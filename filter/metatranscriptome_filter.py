#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
    based on Andrew Tritt's bash script
    Version 1.0.0
    Shijie Yao
    2014-10-10 - initial implementation
    2014-11-20 - perc_low_quality value from 'Low quality discards: 11775164 reads (13.23%) 1766274600 bases (13.23%)'
                 the percentage value following reads
    2015-04-15 - RQC-644 : add read count after rRNA removal (filtered_reads_rna) to rqc_stats.txt
    
    
    Note: normally require large memory node to run (120G)
    CML test:
    qlogin -l exclusive.c
    OR
    qlogin -l mem.c=120G
"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import os
import sys
import logging
import argparse

# append the pipeline lib and tools relative path:
pipe_root = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/' + os.pardir)
sys.path.append(pipe_root + '/lib')   # common
sys.path.append(pipe_root + '/tools') # rqc_fastq

import rqc_fastq as fastqUtil
from common import get_logger, get_status, run_command, checkpoint_step
import os_utility as osUtil

# the following constants needs work - some central location ?
TOOLS_BIN = pipe_root + '/tools'

ADAPTER_DB="/global/projectb/sandbox/gaag/bbtools/jgi-bbtools/resources/nextera.fa.gz,/global/projectb/sandbox/gaag/bbtools/jgi-bbtools/resources/truseq.fa.gz"
ARTIFACT_DB="/global/dna/shared/rqc/ref_databases/qaqc/databases/illumina.artifacts/Illumina.artifacts.2013.12.no_DNA_RNA_spikeins.fa,"\
"/global/dna/shared/rqc/ref_databases/qaqc/databases/illumina.artifacts/RNA_spikeins.artifacts.2012.10.NoPolyA.fa,"\
"/global/dna/shared/rqc/ref_databases/qaqc/databases/illumina.artifacts/DNA_spikeins.artifacts.2012.10.fa,"\
"/global/dna/shared/rqc/ref_databases/qaqc/databases/phix174_ill.ref.fa"

#RRNA_DB="/projectb/sandbox/rqc/qcdb/all.RNA.silva.greengenes.jgi.GtRNAdb.tRNADB-CE/all.RNA.silva.greengenes.jgi.GtRNAdb.tRNADB-CE_fixed.fasta"
RRNA_DB="/global/projectb/sandbox/rqc/qcdb/SILVA_119"

# the following constatns are specific to THIS pipeline
STATUS_LOG = 'status.log';

PIPE_START      = 'start'

RM_ADAPTER_START = 'start rm adapter'
RM_ADAPTER_END   = 'end rm adapter'
RM_RRNA_START      = 'start rm rrna'
RM_RRNA_END        = 'end rm rrna'
COUNT_20MER_START   = 'start count 20mer'
COUNT_20MER_END     = 'end count 20mer'
POST_START = 'start post task'
POST_END = 'end post task'
FILES_STATS_START = 'start pipeline files and stats'
FILES_STATS_END = 'end pipeline files and stats'

PIPE_COMPLETE = 'complete'

DEBUG = False   # for debuging, when T, pass log handle to run_command() etc

STEP_ORDER = {
    PIPE_START      : 0,
    
    RM_ADAPTER_START       : 10,
    RM_ADAPTER_END         : 40,
    
    RM_RRNA_START   : 50,
    RM_RRNA_END     : 60,
    
    COUNT_20MER_START       : 70,
    COUNT_20MER_END       : 80,
    
    POST_START: 90,
    POST_END: 100,
    
    FILES_STATS_START      : 110,
    FILES_STATS_END        : 120,
    
    PIPE_COMPLETE   : 1000  
}
    
    
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## pipeline function definitions

'''
# REMOVE ILLUMINA ADAPTERS AND DO QUALITY TRIMMING AND REMOVE PROCESS ARTIFACTS
@infile    : the raw fastq input file [input of filtering]
@odir       : the output directory
@status     : where the pipeline was at by last run
'''
def rm_adapter(infile, odir, pipeFiles, pipeStats, toDelete, status, log):
    log_and_print('RUN RM ADAPTER : Trimming adapter and low quality bases, and removing artifacts', log)
    checkpoint(RM_ADAPTER_START, status)
   
    fq_base = get_base_name(infile)

    processCleanedFastq = '%s.anqtp.fastq' % fq_base
    adapterStats = '%s.adapter.stats' % fq_base
    artifactStats = '%s.artifact.stats' % fq_base
    
    # t=16 (# threads; don't use so it is auto determined)
    # int=t (interleaved, auto determined if not used [and input from file]; need to use in the receiving end of a pipe)
    # in=INPUT_FASTQ
    # out=stdout.fq (.fq tells the output to stdout is in fastq format)
    # ref=LIST_OF_DATABASE
    # 
    # ktrim=r : search right-end of reads for trimming;
    # k=25 mink = 12 : match kmer length of 12-25 for trimming;
    # tpe=t : trim the paired reads to maintain pairness
    # tbo=t : (trimbyoverlap) Trim adapters based on where paired reads overlap
    # qtrim=r trimq=10: trim low quality bases, with the Q threthold of 10
    # maq=10 : min avg Q
    # maxns=3 : discard reads with >3 Ns (before trimming)
    # minlen=50: discard reads with <50 (after trimming)
    # hdist=1: # of mismatch allowed for a match (def 0)[the larger, the more memory it requires]
    #
    adapterLog = 'adapter.log'
    artifactLog = 'artifact.log'
    cmd1 = 'bbduk.sh in=%s out=stdout.fq ref=%s ktrim=r k=25 mink=12 tpe=t tbo=t qtrim=r trimq=10 maq=10 maxns=3 minlen=50 hdist=1 stats=%s 2>%s|\\' \
                % (infile, ADAPTER_DB, adapterStats, adapterLog)
    cmd2 = 'bbduk.sh int=t in=stdin.fq out=%s ref=%s k=31 hdist=1 stats=%s 2>%s' \
                % (processCleanedFastq, ARTIFACT_DB, artifactStats, artifactLog)

    if STEP_ORDER[status] <= STEP_ORDER[RM_ADAPTER_START]:
        shFileName = os.path.join(odir, 'rm_adapter.sh')
        create_shell(shFileName, (cmd1, cmd2))
        
        stdOut, stdErr, exitCode = exe_command(shFileName, log)    # stdOut of 0 is success
        if exitCode != 0:
            log_and_print(' * failed to run %s: %s' % (shFileName, stdErr), log)
            return None
                        
        log_and_print(' - rm_adapter produced output files [%s, %s, %s] ' % (processCleanedFastq, adapterStats, artifactStats), log)
    else:
        log_and_print(' - no need to perform this step', log)
        
    #TODO: should we check the existance of the expected output files??
    
    log_and_print('RUN RM ADAPTER - completed', log)
    
    checkpoint(RM_ADAPTER_END, status)
    
    #pipeFiles['metatranscriptome_adapter_cleaned'] = '%s/%s' % (odir, processCleanedFastq)
    #pipeFiles['metatranscriptome_adapter_stats'] = '%s/%s' % (odir, adapterStats)
    #pipeFiles['metatranscriptome_artifact_stats'] = '%s/%s' % (odir, artifactStats)
    
    toDelete.append(processCleanedFastq)
            
    with open(adapterLog, 'r') as fh:
        for line in fh:
            if line.startswith('Low quality discards:'):
                tokens = line.split()
                if len(tokens) > 5:
                    pipeStats['perc_low_quality'] = tokens[5][1:-2]
                break
                
    with open(artifactLog, 'r') as fh:
        for line in fh:
            line = line.strip()
            if line.startswith('Contaminants:'):
                tokens = line.split()
                if len(tokens) > 3:
                    pipeStats['perc_artifact'] = tokens[3][1:-2]
                break
                
    return processCleanedFastq
    # - END OF rm_adapter -
    
'''
@infile    : the output file of the rm_adapter step
Remove rRNA and human reads
'''
def rm_rrna(infile, odir, pipeFiles, pipeStats, toDelete, status, log):
    log_and_print('RUN RM rRNA : Removing rRNA and human sequence', log)
    checkpoint(RM_RRNA_START, status)
   
    fq_base = get_base_name(infile)
 
    rrnaCleanedFastq = '%s.hR.fastq.gz' % fq_base
    rRnaFastq = '%s.rRNA.fastq.gz' % fq_base
    rRNAStats = '%s.rRNA.stats' % fq_base
    humanStats = '%s.human.stats' % fq_base
    
    #rRNA_db_dir = os.path.dirname(RRNA_DB)
    
    rRnaLog = 'rRNA.log'
    humanLog = 'human.log'
    # outu=stdout.fq outm=%s : write to both STDOUT and the output file
    # 
    cmd1 = 'bbmap.sh -Xmx30g in=%s outu=stdout.fq outm=%s path=%s scafstats=%s fast=t minid=0.90 local=t ow printunmappedcount=t 2>%s|\\' \
                % (infile, rRnaFastq, RRNA_DB, rRNAStats, rRnaLog)
    cmd2 = 'removehuman.sh -Xmx30g int=t in=stdin.fq outu=%s scafstats=%s gzip ow printunmappedcount=t 2>%s' \
                %(rrnaCleanedFastq, humanStats, humanLog)

    if STEP_ORDER[status] <= STEP_ORDER[RM_RRNA_START]:
        shFileName = os.path.join(odir, 'rm_rrna.sh')
        create_shell(shFileName, (cmd1, cmd2))
        
        stdOut, stdErr, exitCode = exe_command(shFileName, log)    # stdOut of 0 is success
        if exitCode != 0:
            log_and_print(' * failed to run %s [%s] [%s]' % (shFileName, stdErr, exitCode), log)
            return None
                
        log_and_print(' - rm_rrna produced output files [%s, %s, %s, %s] ' % (rrnaCleanedFastq, rRnaFastq, rRNAStats, humanStats), log)
    else:
        log_and_print(' - no need to perform this step', log)
        
    #TODO: should we check the existance of the expected output files??
    
    log_and_print('RUN RM rRNA - completed', log)
    
    checkpoint(RM_RRNA_END, status)
    
    pipeFiles['metatranscriptome_anqtphR_fastq'] = '%s/%s' % (odir, rrnaCleanedFastq)   #all the stats will go with this file to jamo
    pipeFiles['metatranscriptome_rRNA_fastq'] = '%s/%s' % (odir, rRnaFastq)             #perc_low_quality and perc_artifact go with this file to jamo
    
                
    with open(rRnaLog, 'r') as fh:
        for line in fh:
            line = line.strip()
            if line.startswith('unmapped:'):
                tokens = line.split()
                if len(tokens) > 1:
                    unmappedPerc = float(tokens[1][:-1])
                    mappedPerc = 100.0 - unmappedPerc
                    #print('DEBUG %f : %f' % (unmappedPerc, mappedPerc))
                    pipeStats['perc_rrna'] = str(mappedPerc)
                    if len(tokens) > 2:
                        read_count = int(tokens[2]) * 2   # number in human.log is read pairs (confirmed with Brian, 4/15/2015)
                        pipeStats['filtered_reads_rna'] = read_count
                    
                break
                
    with open(humanLog, 'r') as fh:
        for line in fh:
            line = line.strip()
            if line.startswith('unmapped:'):
                tokens = line.split()
                if len(tokens) > 1:
                    unmappedPerc = float(tokens[1][:-1])
                    mappedPerc = 100.0 - unmappedPerc
                    #print('DEBUG %f : %f' % (unmappedPerc, mappedPerc))
                    pipeStats['perc_human'] = str(mappedPerc)
                
                if len(tokens) > 2:
                    read_count = int(tokens[2]) * 2   # number in human.log is read pairs (confirmed with Brian, 4/15/2015)
                    pipeStats['filtered_reads'] = read_count
                break
            
    return rrnaCleanedFastq
    # - END OF rm_rrna -
    
'''
@infile    : the output file of the rm_rrna step
    COUNT 20 MERS
    NOTE: If partial interval allowed at end, interval size will not matter.
'''
def mer_count(infile, odir, pipeFiles, pipeStats, toDelete, status, log):
    log_and_print('RUN 20MER COUNT', log)
    checkpoint(COUNT_20MER_START, status)
   
    fq_base = get_base_name(infile)

    mersampleCounts = '%s.mersamples.txt' % fq_base
    mersampleLog = 'mersample.log'
    cmd = 'bbcountunique.sh -Xmx30g in=%s out=%s cumulative=t interval=1000000 k=20 percent=t count=t plb=t 2>%s' \
                % (infile, mersampleCounts, mersampleLog)

    if STEP_ORDER[status] <= STEP_ORDER[COUNT_20MER_START]:
        shFileName = os.path.join(odir, 'mer_count.sh')
        create_shell(shFileName, (cmd, ))
        
        stdOut, stdErr, exitCode = exe_command(shFileName, log)    # stdOut of 0 is success
        if exitCode != 0:
            log_and_print(' * failed to run %s: %s' % (shFileName, stdErr), log)
            return None
                  
        log_and_print(' - mer count produced output file [%s] ' % mersampleCounts, log)
    else:
        log_and_print(' - no need to perform this step', log)
        
    log_and_print('RUN 20MER COUNT - completed', log)
    
    checkpoint(COUNT_20MER_END, status)
        
    #pipeFiles['metatranscriptome_mersample_counts'] = '%s/%s' % (odir, mersampleCounts)
                
    with open(mersampleCounts, 'r') as fh:
        for line in fh:
            pass
        tokens = line.split()
        if len(tokens) > 2:
            pipeStats['perc_unique_rand_mer'] = tokens[2]
                
    return mersampleCounts

def pipeline_files_stats(odir, pipeFiles, pipeStats, status, log):
    log_and_print('RUN pipeline files and stats', log)
    checkpoint(FILES_STATS_START, status)

    if STEP_ORDER[status] <= STEP_ORDER[FILES_STATS_START]:
        f_pipefiles = 'rqc_files.txt'
        f_pipestats = 'rqc_stats.txt'

        log_and_print(' - write to file %s' % f_pipefiles, log)
        lcnt = 0
        with open(f_pipefiles, 'w') as fh:
            for key in pipeFiles:
                fh.write('%s=%s\n' % (key, pipeFiles[key]))
                lcnt += 1
        log_and_print('    -- %d lines are written to file %s' % (lcnt, f_pipefiles), log)
        
        log_and_print(' - write to file %s' % f_pipestats, log)
        lcnt = 0
        with open(f_pipestats, 'w') as fh:
            for key in pipeStats:
                fh.write('%s=%s\n' % (key, pipeStats[key]))
                lcnt += 1
        log_and_print('    -- %d lines are written to file %s' % (lcnt, f_pipestats), log)
            
        log_and_print(' - pipeline files and stats produced files [%s, %s] ' % (f_pipefiles, f_pipestats), log)
        
    else:
        log_and_print(' - no need to perform this step', log)
        
    log_and_print('RUN pipeline files and stats - completed', log)
    checkpoint(FILES_STATS_END, status)
    return True

# tar and pigz up the files; prep jamo update
def post_task(odir, pipeFiles, pipeStats, status, log):
    log_and_print('Run Tar and pigz', log)
    checkpoint(POST_START, status)
    rtnState = True
    if STEP_ORDER[status] <= STEP_ORDER[FILES_STATS_START]:
        tarfname = 'metatranscript-filter-files.tar'
        zipfname = '%s.gz' % tarfname
        if os.path.exists(tarfname):
            log_and_print(' - delete %s' % tarfname, log)
            os.remove(tarfname)
            
        if os.path.exists(zipfname):
            log_and_print(' - delete %s' % zipfname, log)
            os.remove(zipfname)
            
        # exculde all fastq, fastq.gz other gz files
        cmd = 'tar -cvf %s --exclude=*.fastq*  --exclude=*.gz *' % tarfname
        
        stdOut, stdErr, exitCode = exe_command(cmd, log)
        if exitCode == 0:
            cmd = 'module load pigz; pigz %s' % tarfname
            stdOut, stdErr, exitCode = exe_command(cmd, log)
            if exitCode == 0:
                pipeFiles['metatranscriptome_tar_pigz'] = '%s/%s.gz' % (odir, tarfname)
            else:
                og_and_print(' - failed to pigz the tar file', log)
                rtnState = False
        else:
            log_and_print(' - failed to tar up the result', log)
            rtnState = False
    else:
        log_and_print(' - no need to perform this step', log)
    
    log_and_print('RUN Tar and pigz - completed', log)
    checkpoint(POST_END, status)
    return rtnState

def clean_up(flist):
    log_and_print('RUN Cleanup', log)
    for f in flist:
        if os.path.exists(f):
            log_and_print(' - delete %s' % f, log)
            os.remove(f)
        else:
            log_and_print(' - error : not found %s' % f, log)
        
    log_and_print('RUN Cleanup - completed', log)
    
    
#=================================================================
# Helper functions
    
def create_shell(shName, cmdArray):
    with open(shName, 'w') as fh:
        fh.write('#!/bin/bash\n')
        fh.write('set -e\n')
        fh.write('set -o pipefail\n')
        fh.write('module load bbtools\n')
        for item in cmdArray:
            fh.write('%s\n' % item)
        os.chmod(shName, 0755)  #-rwxr-xr-x
            

# checkpoint logging
def checkpoint(status, fromStatus=PIPE_START):
    if status == PIPE_START or STEP_ORDER[status] > STEP_ORDER[fromStatus]:
        checkpoint_step(STATUS_LOG, status)
    
    
# helper function to construct a formated message string    
def msg_str(key, val):
    return '%15s      %s' % (key, val)

# helpter function to print and log command line arguments
def list_opt(log):
    dash = '-' * 90
    log_and_print(dash, log)
    
    log_and_print(msg_str('fastq', fastq), log)
    log_and_print(msg_str('outputPath', outputPath), log)
    log_and_print(dash, log)

# helper function to print and log a message string
def log_and_print(msg, log):
    if log:
        log.info(msg)
    print(msg)

# helper function to print and log an error message and then exit the program
def err_log_and_print(msg, log):
    if log:
        log.error(msg)
    print(msg)
    exit(2)
    

def exe_command(cmd, log):
    if DEBUG:
        return run_command(cmd, True, log)
    else:
        return run_command(cmd, True)


def get_base_name(fname):

    fq_base = os.path.basename(fname)

    if fq_base.endswith('.gz'):
        fq_base = fq_base[:-3]

    if fq_base.endswith('.fastq'):
        fq_base = fq_base[:-6]

    if fq_base.endswith('.fq'):
        fq_base = fq_base[:-3]

    return fq_base

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## main program

script_name = __file__

if __name__ == "__main__":

    my_name = "Metatranscriptom Filter Pipeline"
    version = "1.0.0"
    
    # Parse options
    usage = "* %s, version %s\n" % (my_name, version)

    oriCmd = ' '.join(sys.argv)

    # command line options
    parser = argparse.ArgumentParser(description=usage, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-f", "--fastq", dest="fastq", help="Fastq file (full path to fastq)", required=True)
    parser.add_argument("-o", "--output-path", dest="outputPath", help = "Output path to write to", required=True)
    parser.add_argument("-v", "--version", action="version", version=version) 
        
    options = parser.parse_args()
    
    logLevel    = "INFO"
    if options.fastq:
        fastq = options.fastq
        
    if options.outputPath:
        outputPath = options.outputPath
    if not outputPath:
        outputPath = os.getcwd()
        
    # create output_directory if it doesn't exist
    if not os.path.isdir(outputPath):
        print("Cannot find %s, creating as new" % outputPath)
        os.makedirs(outputPath)
        
    outputPath = os.path.realpath(outputPath)
        
    # initialize my logger
    logFile = os.path.join(outputPath, "rqc_metatranscriptome_filtering_pipeline.log")
    log = get_logger("metatranscriptome_filtering", logFile, logLevel)
           
    if not os.path.isdir(outputPath):
        err_log_and_print('Cannot work with directory: %s' % outputPath, log)
    
    # check for fastq file
    if fastq:
        if not os.path.isfile(fastq):
            err_log_and_print('%s not found, abort!' % fastq, log)
    else:
        err_log_and_print('No fastq defined, abort!', log)

    fastq = os.path.realpath(fastq)
    
    
    
    #--------------------------------
    # init log
    log_and_print('#' * 50, log)
    log_and_print("Started pipeline, writing log to: %s" % logFile, log)
    log_and_print("CMD=%s" % oriCmd, log)
    list_opt(log)
    
    #--------------------------------
    # the standard pipeline file and stats container
    pipeFiles = {}
    pipeStats = {}
    toDelete = []
    
    
    os.chdir(outputPath)
                      
    cycle = 0
    cycleMax = 1

    status = get_status(STATUS_LOG, log)
    log_and_print('Starting pipeline at [%s]' % status, log)
    
    if status == PIPE_START: 
        checkpoint(PIPE_START)

    # main loop: retry upto cycleMax times
    while cycle < cycleMax:          
        cycle += 1
        log_and_print('ATTEMPT [%d]' % cycle, log)
        sfix = ''
        cmdStr = []
        filesToRemove = []  # list of intermediate files for clean up
        lastFastq = fastq   # lastFastq : fastq produced by each step, init to input
        
        if cycle > 1:
            status = get_status(STATUS_LOG, log)
           
        if status != PIPE_COMPLETE: 
            ofile = rm_adapter(fastq, outputPath, pipeFiles, pipeStats, toDelete, status, log)
            #print('DEBUG f=%s, s=%s' % (str(pipeFiles), str(pipeStats)))
            
            if ofile:
                ofile = rm_rrna(ofile, outputPath, pipeFiles, pipeStats, toDelete, status, log)
                
                if ofile:
                    ofile = mer_count(ofile, outputPath, pipeFiles, pipeStats, toDelete, status, log)
                    
                    if ofile:
                        jobState = post_task(outputPath, pipeFiles, pipeStats, status, log)
                            
                        if jobState:
                            jobState = pipeline_files_stats(outputPath, pipeFiles, pipeStats, status, log)
                                
                            if jobState:
                                    checkpoint(PIPE_COMPLETE)
                                    clean_up(toDelete)
                                    log_and_print('Pipeline Completed', log)
                                    cycle = cycleMax+1
                            else:
                                err_log_and_print('Error: files and stats step failed to complete', log)
                        else:
                            err_log_and_print('Error: tar and pigz failed to complete', log)
                    else:
                        err_log_and_print('Error: 20 mer counting step did not produce the needed output file, OR no meaningful data in output is generated', log)
                else:
                    err_log_and_print('Error: rm rRNA and human step did not produce the needed output file, OR no meaningful data in output is generated', log)                
            else:
                err_log_and_print('Error: rm adaptor step did not produce the needed output file, OR no meaningful data in output is generated', log)
        else:
            cycle = cycleMax+1
            log_and_print('Pipeline Already Complete', log)
            
    sys.exit(0)
            
    