#!/bin/bash -l

#set -e

# Filter test

folder="/global/projectb/sandbox/rqc/analysis/qc_user/filter"
DATE=`date +"%Y%m%d"`


#dev for Bryce
cmd="/global/homes/b/brycef/git/jgi-rqc-pipeline/filter/fastq_filter2.py"


# Nextera frag test ABYBB: 43G (DNA)
#qsub -b yes -j yes -m n -w e -l ram.c=5.25g,h_rt=43199,s_rt=43194 -N filter-ABYBB -P gentech-rqc.p -o $folder/filter-ABYBB.log "$cmd -o $folder/ABYBB-$DATE -f $folder/ABYBB.fastq --prod-type dna"
qsub -b yes -j yes -m n -w e -l h_rt=43200,s_rt=43195,ram.c=3.5g,exclusive.c=1 -pe pe_32 32 -N filter-ABYBB -P gentech-rqc.p -o $folder/filter-ABYBB.log "$cmd -o $folder/ABYBB-$DATE -f $folder/ABYBB.fastq --prod-type dna"

# DNA/frag - ANZTX - 9365.2.130724.GTGAAA.fastq.gz
#qsub -b yes -j yes -m n -w e -l ram.c=5.25g,h_rt=43199,s_rt=43194 -N filter-ANZTX -P gentech-rqc.p -o $folder/filter-ANZTX.log "$cmd -o $folder/ANZTX-$DATE -f $folder/ANZTX.fastq --prod-type dna"
qsub -b yes -j yes -m n -w e -l h_rt=43200,s_rt=43195,ram.c=3.5g,exclusive.c=1 -pe pe_32 32 -N filter-ANZTX -P gentech-rqc.p -o $folder/filter-ANZTX.log "$cmd -o $folder/ANZTX-$DATE -f $folder/ANZTX.fastq --prod-type dna"

# RNA - AONCP - 9365.8.130785.CGAGAA.fastq.gz
#qsub -b yes -j yes -m n -w e -l ram.c=5.25g,h_rt=43199,s_rt=43194 -N filter-AONCP -P gentech-rqc.p -o $folder/filter-AONCP.log "$cmd -o $folder/AONCP-$DATE -f $folder/AONCP.fastq --prod-type rna"
qsub -b yes -j yes -m n -w e -l h_rt=43200,s_rt=43195,ram.c=3.5g,exclusive.c=1 -pe pe_32 32 -N filter-AONCP -P gentech-rqc.p -o $folder/filter-AONCP.log "$cmd -o $folder/AONCP-$DATE -f $folder/AONCP.fastq --prod-type rna"

# CLRS - ACOUA - 9339.6.128879.GTTTCG.fastq.gz
#qsub -b yes -j yes -m n -w e -l ram.c=5.25g,h_rt=43199,s_rt=43194 -N filter-ACOUA -P gentech-rqc.p -o $folder/filter-ACOUA.log "$cmd -o $folder/ACOUA-$DATE -f $folder/ACOUA.fastq --prod-type clrs"
qsub -b yes -j yes -m n -w e -l h_rt=43200,s_rt=43195,ram.c=3.5g,exclusive.c=1 -pe pe_32 32 -N filter-ACOUA -P gentech-rqc.p -o $folder/filter-ACOUA.log "$cmd -o $folder/ACOUA-$DATE -f $folder/ACOUA.fastq --prod-type clrs"

# Jigsaw SAG - AOOAT - 9392.1.130804.TAGCT.fastq.gz
# ISO same params as sag
#qsub -b yes -j yes -m n -w e -l ram.c=5.25g,h_rt=43199,s_rt=43194 -N filter-AOOAT -P gentech-rqc.p -o $folder/filter-AOOAT.log "$cmd -o $folder/AOOAT-$DATE -f $folder/AOOAT.fastq --prod-type sag"
qsub -b yes -j yes -m n -w e -l h_rt=43200,s_rt=43195,ram.c=3.5g,exclusive.c=1 -pe pe_32 32 -N filter-AOOAT -P gentech-rqc.p -o $folder/filter-AOOAT.log "$cmd -o $folder/AOOAT-$DATE -f $folder/AOOAT.fastq --prod-type sag"

# Nextera - AGYYN - 8871.2.114463.CCGTCC.fastq.gz
#qsub -b yes -j yes -m n -w e -l ram.c=5.25g,h_rt=43199,s_rt=43194 -N filter-AGYYN -P gentech-rqc.p -o $folder/filter-AGYYN.log "$cmd -o $folder/AGYYN-$DATE -f $folder/AGYYN.fastq --prod-type nextera"
qsub -b yes -j yes -m n -w e -l h_rt=43200,s_rt=43195,ram.c=3.5g,exclusive.c=1 -pe pe_32 32 -N filter-AGYYN -P gentech-rqc.p -o $folder/filter-AGYYN.log "$cmd -o $folder/AGYYN-$DATE -f $folder/AGYYN.fastq --prod-type nextera"


#unpigz -c $SDM_FASTQ/00/93/65/9365.8.130785.CGAGAA.fastq.gz > AONCP.fastq
#unpigz -c $SDM_FASTQ/00/93/39/9339.6.128879.GTTTCG.fastq.gz > ACOUA.fastq
#unpigz -c $SDM_FASTQ/00/93/92/9392.1.130804.TAGCT.fastq.gz > AOOAT.fastq
#unpigz -c $SDM_FASTQ/00/88/71/8871.2.114463.GTGAAA.fastq.gz > AGYYG.fastq
#unpigz -c $SDM_FASTQ/00/93/65/9365.2.130724.GTGAAA.fastq.gz > ANZTX.fastq