#!/usr/bin/env python
"""
Creates text files from google docs for filtering methods
- looks up SOPs from RQC SOP manager tables
- pulls text version of files from Google docs and writes to the ./filter_desc path

Runnable only by Bryce since he has the virtalenv setup (gapi) and the files for permissions for the google api


- setup google api (gapi)
$ virtualenv gapi
$ . ./gapi/bin/activate
$ pip install --upgrade google-api-python-client
$ pip install --upgrade MySQL

$ ./sop_google.py
$ deactivate

# optional - should be in gapi already
$ pip install --upgrade google-api-python-client

Notes
- included run_command, post_mortem because virtualenv needs logger installed and I'm not interested in creating a huge virtualenv

- uses this api:
https://developers.google.com/drive/v3/reference/files/export

"""

import httplib2
import os
import io
import sys
import re
import time
import MySQLdb
from subprocess import Popen, PIPE

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
from apiclient.http import MediaIoBaseDownload

dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir, '../lib'))
from db_access import jgi_connect_db


import argparse
flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()


# If modifying these scopes, delete your previously saved credentials
SCOPES = 'https://www.googleapis.com/auth/drive'
CLIENT_SECRET_FILE = "/global/homes/b/brycef/client_secret.json" # got from https://developers.google.com/drive/v3/web/quickstart/python, usable between multiple projects

APPLICATION_NAME = 'Drive API Python Quickstart'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir, 'drive-python-quickstart.json')
    #print credential_path

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()

    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print 'Storing credentials to ' + credential_path
    return credentials


def run_command(cmd):
    std_out = None
    std_err = None
    exit_code = None

    if cmd:
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        std_out, std_err = p.communicate()
        exit_code = p.returncode

    else:

        return None, None, -1

    return std_out, std_err, exit_code

def post_mortem_cmd(cmd, exit_code, std_out, std_err):

    if exit_code > 0:
        print "- cmd failed: %s" % cmd
        print "- exit code: %s" % exit_code

    if std_out:
        print "- std_out: %s" % std_out

    if std_err:
        print "- std_err: %s" % std_err



# download without comments?
def download_google_file(file_id, mime_type):

    filename = filename = os.path.join(filter_desc_path, "google.txt") # temp file
    print "- downloading %s to %s" % (file_id, filename)

    request = service.files().export_media(fileId=file_id, mimeType=mime_type)
    fh = fh = io.FileIO(filename, 'wb')
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk() 
        #print "Download %d%%." % int(status.progress() * 100)
    fh.close()

    return filename


# remove comments & user comments from the SOP file
def write_clean_file(tmp_file, out_file):

    # replace [a] with "" ... sag.txt has these in it
    comments_list = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q']
    # don't filter out [1], [2] ... these are actual footnotes

    fh = open(tmp_file, "r")
    fhw = open(out_file, "w")
    for line in fh:
        # would like to download file w/o comments imbedded ...
        if line.strip().startswith("#"):
            continue
        elif line.strip().startswith("["): # comment from google doc
            continue
        elif line.strip().startswith(">"): # email comment
            continue
        if line.strip().startswith("On "): # SOP 1071
            continue

        # clean non-printable chars
        line = re.sub(r'[^\x00-\x7F]+', '', line)


        if line.find(filter_ref_key) > -1:
            line = line.replace(filter_ref_key, filter_ref_file)
        for c in comments_list:
            comment_id = "[%s]" % c
            line = line.replace(comment_id, "")
        fhw.write(line)
    fhw.close()
    fh.close()

    cmd = "rm %s" % (tmp_file)
    std_out, std_err, exit_code = run_command(cmd)
    post_mortem_cmd(cmd, exit_code, std_out, std_err)


def archive_existing_methods():

    zip_file = os.path.join(filter_desc_path, "filter_desc-%s.zip" % time.strftime("%Y%m%d%H%M%S"))
    print "- backing up old files to: %s" % zip_file
    cmd = "zip %s %s/* -x *.zip" % (zip_file, filter_desc_path)
    # -x: exclude existing zip files
    std_out, std_err, exit_code = run_command(cmd)
    post_mortem_cmd(cmd, exit_code, std_out, std_err)


if __name__ == '__main__':

    print "Getting SOPs for Filtering from Google Drive"


    filter_ref_file = "filtering_references-20160921.tar" # JAMO id? 3.1g
    filter_ref_key = "<filter_ref_file>" # replace this in the google docs

    sop_json = {} # sop ids -> sop filenames & links
    filter_desc_path = "/global/homes/b/brycef/git/jgi-rqc-pipeline/filter/filter_desc"

    sop_list = os.path.join(filter_desc_path, "sop_list.txt")
    fh = open(sop_list)
    for line in fh:
        if line.startswith("#"):
            continue

        arr = line.strip().split("=")
        if len(arr) > 1:
            sop_id = "SOP %s" % str(arr[1]).strip()
            file_name = arr[0].strip()

            print "- sop_id: %s = %s" % (sop_id, file_name)
            sop_json[sop_id] = { "file" : file_name }


    # get all of the sop links
    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)

    sql = "select sop_id, sop_doc_link, sop_title from sop where group_id = 28 and sop_status_id = 3"
    # sop_status_id = 3 = approved
    # group_id = 28 = filtering SOPs

    sth.execute(sql)
    row_cnt = int(sth.rowcount)
    print "- found %s SOPs for filtering" % row_cnt

    for _ in range(row_cnt):
        rs = sth.fetchone()

        sop_key = "SOP %s" % rs['sop_id']
        if sop_key in sop_json:
            sop_json[sop_key]['title'] = rs['sop_title']
            sop_json[sop_key]['doc_link'] = rs['sop_doc_link']

    db.close()



    # zip up everything
    archive_existing_methods()


    # log into google and get the new files
    print "- signing into google api"
    credentials = get_credentials()

    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)


    # could do PDF but it might include other comments and things we don't want + we can't merge values into it
    mime_type = 'text/plain'

    # download each SOP and clean it out
    for sop in sop_json:
        if 'doc_link' in sop_json[sop]:
            arr = sop_json[sop]['doc_link'].split("/")
            file_id = ""
            for a in arr:
                if len(a) > len(file_id):
                    file_id = a

            sop_title = sop_json[sop]['title']

            print "- SOP: %s, title: %s, file_id: %s" % (sop, sop_title, file_id)

            if file_id:

                # download to a tmp file
                tmp_file = download_google_file(file_id, mime_type)

                # actual file for filtering SOP (cleaned)
                out_file = os.path.join(filter_desc_path, sop_json[sop]['file'])

                # clean out comments
                write_clean_file(tmp_file, out_file)



    print "Complete"
    sys.exit(0)
