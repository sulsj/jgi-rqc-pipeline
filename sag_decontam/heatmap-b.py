#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Heatmap-b
- produces 2d heatmaps
- replace /usr/common/jgi/qaqc/jigsaw/2.6.5-prod_2.6.5_4-1-g1e9ef4b/bin/heatmap.py
- this is a copy of Vasanth's heatmap.py code with changes made to be more RQC-ish
- runs in 20 seconds

Dependencies:
none

v 1.0: 2018-01-04
- initial version

v 1.1: 2018-03-21
- added x-axis, y-axis and plot title options (don't use plot title, overwrites column labels)

To do:


~~~~~~~~~~~~~~~~~~~[ Test Cases ]

~/git/jgi-rqc-pipeline/sag_decontam/heatmap-b.py -ig -1 -cfs 2 -lfs 2.5 -cl trafficlight -pl -png -sep psv -int \
-i /global/projectb/scratch/brycef/sag/tangy/input/read_level_cross_contam_raw_contigs.txt \
-hf /global/projectb/scratch/brycef/sag/tangy/input/read_level_cross_contam_raw_contigs.pdf \
-gr /global/projectb/scratch/brycef/sag/tangy/heatmap-group.txt \
-o /global/projectb/scratch/brycef/sag/tangy/

~/git/jgi-rqc-pipeline/sag_decontam/heatmap-b.py -ig -1 -png -sep psv -int -min 0 -max 9999 -cl YlOrRd \
-i /global/projectb/sandbox/rqc/analysis/brycef/rqc-dev/pipelines/sag_decontam/archive/00/00/40/00/input/read_level_cross_contam_raw_contigs.txt \
-gr /global/projectb/sandbox/rqc/analysis/brycef/rqc-dev/pipelines/sag_decontam/archive/00/00/40/00/heatmap-group.txt \
-hf /global/projectb/sandbox/rqc/analysis/brycef/rqc-dev/pipelines/sag_decontam/archive/00/00/40/00/input/read_level_cross_contam_raw_contigs2.pdf \
-o /global/projectb/sandbox/rqc/analysis/brycef/rqc-dev/pipelines/sag_decontam/archive/00/00/40/00/input \
-y "Raw Assembly" -x "Input fastq" -t "Read Level Cross Contamination"
"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
#import getpass
import math
import numpy as np
import re

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import textwrap
#import matplotlib.cm as cm
import matplotlib.colors as colors
#from matplotlib.colors import LogNorm
from matplotlib.colors import LinearSegmentedColormap

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, get_colors, get_msg_settings


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
setup new colormap
'''
def get_custom_color(color_name):
    jet_vals = []
    if color_name == "trafficlight": # R,Y,G
        jet_vals = [
           [ 0.236,  0.702,  0.443],
           [ 0.91721133,  0.96514161,  0.63572987 ],
           [ 0.99782135,  0.93246188,  0.63572987 ],
           [ 0.99346405,  0.74771243,  0.43529413 ],
           [ 0.96470588,  0.48409587,  0.2888889  ],
           [ 0.85577343,  0.21481482,  0.16514162 ],
           [ 0.863, 0.078, 0.235 ],
        ]
    newcmap = LinearSegmentedColormap.from_list("newjet", jet_vals)
    return newcmap


'''
max label length (chars)
'''
def get_max_label_length(label_list):
    max_len = 0
    for label in label_list:
        if len(label) > max_len:
            max_len = len(label)
    return max_len

'''
get label font size (chars)
'''
def get_label_font_size(label_list):

    num_rows = len(label_list)
    max_len = get_max_label_length(label_list)

    # determined empirically using datapoints to fit non-linear line
    coeffs = { 5:[-35.5923, 50.9136, 50.9136],
               10:[-26.2424, 42.1579, -0.0761717],
               15:[3.87533, 6.18829, -10.02],
               20:[3.728, 4.94311, -8.12685],
               25:[-26.305, 43.4969, -0.0871735],
               30:[-23.4326, 37.8282, -0.0677802],
             }
    cell_size = min(math.floor((num_rows + 1) / 5.0) * 5 + 5, 30)

    if cell_size <= 5:
        fontsize = max(10 - ((math.floor((max_len) / 10.0)) / 2), 1)
    else:
        a, b, c = coeffs[cell_size]
        fontsize = min(float("%.2f"%(a + b * max_len ** c)), 10.0)

    return fontsize

'''
font size for each cell, depending on number of cells
'''
def get_cell_font_size(label_list):

    # determined empirically using datapoints to fit non-linear line
    num_rows = len(label_list)
    max_len = get_max_label_length(label_list)
    coeffs = { 5:[5.38193, 4.61807, -8.33944e-14],
               10:[6.46667, 4.93457, -10.6071],
               15:[108.522, -100.426, 0.00854362],
               20:[1.12716, 12.4749, -0.463082],
               25:[12.8828, -8.02476, 0.0432271],
    }
    cell_size = min(math.floor((num_rows - 1) / 5.0) * 5 + 5, 30)


    if cell_size >= 50:
        fontsize = 1
    elif cell_size >= 40:
        fontsize = 2
    elif cell_size >=30:
        fontsize = 3
    else:
        a, b, c = coeffs[cell_size]
        fontsize = max("%.2f" % ( a + b * max_len ** c), 1.)

    return fontsize



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "RQC Heatmap"
    version = "1.1"

    #uname = getpass.getuser() # get the user


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)


    # Parse options
    usage = "heatmap.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
Vasanth's heatmap code, modified by Bryce


sep values: (for splitting rows in the data file)
psv = |
csv = ,
wsv = any whitespace
tsv = tabs
# = ignore this line

sets non numeric values to "-" on the heatmap

    """


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-ig", "--ignore-val", action='store', dest='ignore', default=None, help="[Optional] Use this flag to replace given value with a '-'.")
    parser.add_argument("-cl", "--color", action='store', dest='color', default='RdYlGn', help="[Optional] Use cmap colors instead of 'RdYlGn' colors. (only option is trafficlight)")
    parser.add_argument("-gr", "--group_file", action='store', dest='group', default=None, help="[Optional] A text containing group information. One line per group. Row and column information. Example: 2:3::5:7 (where group is from row 2 to 3 and columns 5 to 7)")
    parser.add_argument("-int", "--int", action='store_true', dest='int_flag', default=False, help="[Optional] Use this flag to specify values in integers instead of float.")
    parser.add_argument("-sep", "--sep", dest="sep", help="[Optional] File separator - tsv, psv, csv, wsv (default tsv)" )   
    parser.add_argument("-hf", "--heatmap-file", dest="heatmap_file", default="heatmap.pdf", help = "Filename for output heatmap (default heatmap.pdf)")
    parser.add_argument("-min", "--min-value", action='store', dest='min_norm', default='', help="[Optional] Minimun value for normalizing color range.")
    parser.add_argument("-max", "--max_value", action='store', dest='max_norm', default='', help="[Optional] Minimun value for normalizing color range.")
    parser.add_argument("-cfs", "--cell-fontsize", action='store', dest='cell_fontsize', type=float, default=0., help="[Optional] cell font size; default 2.0px.")
    parser.add_argument("-lfs", "--label-fontsize", action='store', dest='label_fontsize', type=float, default=0., help="[Optional] axis label font size; default 2.5px.")
    parser.add_argument("-png", "--png", action="store_true", dest="png_flag", default = False, help="[Optional] create png file also")
    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-i", "--input-file", dest="input_file", help = "File with a list of library|assembly file")
    parser.add_argument("-x", "--x-axis", dest="xaxis", help = "[Optional] Label for x-axis")
    parser.add_argument("-y", "--y-axis", dest="yaxis", help = "[Optional] Label for y-axis")
    parser.add_argument("-t", "--title", dest="title", help = "[Optional] Plot title")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)

    # defaults
    output_path = None
    cell_fontsize = 2.0
    label_fontsize = 2.5
    igval = -1
    
    xaxis = None
    yaxis = None
    plot_title = None
    
    # parse args
    args = parser.parse_args()
    png_flag = args.png_flag
    print_log = args.print_log

    if args.output_path:
        output_path = args.output_path

    if args.xaxis:
        xaxis = args.xaxis
    if args.yaxis:
        yaxis = args.yaxis
    if args.title:
        plot_title = args.title
        
    png_dpi = 300 # 1000 = 8000px x 6000px, 300 = 2400px x 1800px - good enough
    
    heatmap_file = args.heatmap_file
    infile = args.input_file
    ignore = args.ignore
    color = args.color
    group_file = args.group
    int_flag = args.int_flag
    sep = "\t"
    if args.sep:
        if args.sep == "psv":
            sep = "|"
        elif args.sep == "tsv":
            sep = "\t"
        elif args.sep == "csv":
            sep = ","
        elif args.sep == "wsv":
            sep = "" # whitepace
        else:
            sep = args.sep # ;, ?
            
    min_norm = args.min_norm
    max_norm = args.max_norm
    

    if args.cell_fontsize:
        cell_fontsize = args.cell_fontsize
    if args.label_fontsize:
        label_fontsize = args.label_fontsize

    group_border_color = "purple" # color to group by



    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()

    # if not starting with /  use relative path (GAA-3421)
    if not output_path.startswith("/"):
        output_path = os.path.join(os.getcwd(), output_path)


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    if not infile:
        print "Error: input file missing  %s" % msg_fail
        sys.exit(2)

    if not os.path.isfile(infile):
        print "Error: input file does not exist  %s" % msg_fail
        sys.exit(2)



    # initialize my logger
    log_file = os.path.join(output_path, "heatmap.log")



    # log = logging object
    log_level = "INFO"

    log = get_logger("heatmap", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "input_file", infile)
    if group_file:
        log.info("%25s      %s", "group_file", group_file)
    log.info("%25s      %s", "heatmap_file", heatmap_file)
    if args.sep:
        log.info("%25s      %s", "file separator", args.sep)
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "xaxis", xaxis)
    log.info("%25s      %s", "yaxis", yaxis)
    log.info("%25s      %s", "title", plot_title)
    log.info("%25s      %s", "png_flag", png_flag)
    log.info("%25s      %s", "ignore", ignore)
    log.info("%25s      %s", "color", color)
    log.info("%25s      %s", "int_flag", int_flag)
    log.info("%25s      %s", "min_norm", min_norm)
    log.info("%25s      %s", "max_norm", max_norm)
    log.info("%25s      %s", "cell_fontsize", cell_fontsize)
    log.info("%25s      %s", "label_fontsize", label_fontsize)


    log.info("")




    
    x = 0
    rows = 0
    cols = 0
    row_labels = []
    col_lables = []
    first_line = True
    custom_colors = { 'trafficlight', } # list of available colors


    

    for line in open(infile, 'r'):
        line = line.strip()
        if line.startswith("#"):
            continue
        
        if first_line:
            if sep:
                col_labels = line.split(sep)
            else:
                col_labels = line.split()
            first_line = False
            
        else:
            if sep:
                tokens = line.split(sep)
            else:
                tokens = line.split()
                
            row_labels.append(tokens[0])
            cols = len(tokens) - 1
            rows += 1
    first_line = True

    log.info("- reading input matrix file")


    if int_flag:
        #mat = np.ones(shape=(rows,cols), dtype=np.int)
        mat = np.zeros(shape=(rows,cols), dtype=np.int)
    else:
        #mat = np.ones(shape=(rows,cols))
        mat = np.zeros(shape=(rows,cols))

    for line in open(infile, "r"):
        line = line.strip()
        if line.startswith("#"):
            continue
        
        if first_line:
            if sep:
                tokens = line.split(sep)
            else:
                tokens = line.split()
            first_line = False
            
        else:
            if sep:
                vals = line.split(sep)
            else:
                vals = line.split()
                
            for y in xrange(len(vals)-1):
                # if not a number then don't try to convert it
                v = str(vals[y+1])
                v = re.sub(r'[^0-9\.]+', '', v) 
                if v:
                    if int_flag:
                        mat[x][y] = int(v)
                    else:
                        mat[x][y] = float(v)
                else:
                    mat[x][y] = -1

            x += 1

    log.info("- creating heatmap")

    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='auto')
    ax.get_xaxis().set_tick_params(labeltop=True, labelbottom=True, direction='out')
    ax.get_yaxis().set_tick_params(labelleft=True, labelright=True, direction='out')

    norm = ""
    if color in custom_colors:
        color = get_custom_color(color)
    #else:
    #    log.info("- custom color: %s does not exist  %s", color, msg_warn)

    if min_norm != "" and max_norm != "":
        norm = colors.Normalize(vmin=float(min_norm), vmax=float(max_norm))
    elif min_norm != "":
        norm = colors.Normalize(vmin=float(min_norm))
    elif max_norm != "":
        norm = colors.Normalize(vmax=float(min_norm))

    Z = ax.imshow(mat, aspect='auto', cmap=color, interpolation='nearest', norm=norm) if norm != '' else \
        ax.imshow(mat, aspect='auto', cmap=color, interpolation='nearest')

    
    text_wrap_len = 50
    if not cell_fontsize:
        cell_fontsize = get_cell_font_size(row_labels)
    if not label_fontsize:
        label_fontsize = get_label_font_size(row_labels)

    log.info("- cell fontsize: %s", cell_fontsize)
    log.info("- label fontsize: %s", label_fontsize)


    row_labelsize = max(label_fontsize, 0.1)
    col_labelsize = max(label_fontsize, 0.1)



    if int_flag and ignore != None:
        ignore = int(ignore)
    elif ignore != None:
        ignore = float(ignore)

    log.info("- processing matrix (%s x %s)", mat.shape[0], mat.shape[1])
    for y in range(mat.shape[0]):
        for x in range(mat.shape[1]):

            if ignore != None and mat[y,x] == ignore:
                ax.text(x, y, '-', fontsize=cell_fontsize, horizontalalignment='center', verticalalignment='center')
            else:
                if int_flag:
                    ax.text(x, y, '%d' % mat[y, x], fontsize=cell_fontsize, horizontalalignment='center', verticalalignment='center')
                else:
                    # was .2
                    ax.text(x, y, '%.1f' % mat[y, x], fontsize=cell_fontsize, horizontalalignment='center', verticalalignment='center')


    if group_file != None:
        log.info("- processing group file: %s", group_file)
        for line in open(group_file, "r"):
            rc_info = line.strip().split(":")
            row1 = int(rc_info[0])-1
            row2 = int(rc_info[1])-1
            col1 = int(rc_info[3])-1
            col2 = int(rc_info[4])-1
            ax.vlines(col1-0.5, row1-0.5, row2+0.5, colors=group_border_color, lw=1.2)
            ax.vlines(col2+0.5, row1-0.5, row2+0.5, colors=group_border_color, lw=1.2)
            ax.hlines(row1-0.5, col1-0.5, col2+0.5, colors=group_border_color, lw=1.2)
            ax.hlines(row2+0.5, col1-0.5, col2+0.5, colors=group_border_color, lw=1.2)

    log.info("- creating plot")
    ax.set_xticks(list(xrange(cols)))
    ax.set_yticks(list(xrange(rows)))
   
    ax.xaxis.set_ticks_position('bottom')
    
    if xaxis:
        ax.set_xlabel(xaxis, fontsize=10, alpha=0.5)
    if yaxis:
        ax.set_ylabel(yaxis, fontsize=10, alpha=0.5)
    
    row_labels = [textwrap.fill(label, text_wrap_len) for label in row_labels]
    col_labels = [textwrap.fill(label, text_wrap_len) for label in col_labels]

    ax.set_xticklabels(col_labels, rotation=90, fontsize=math.ceil(col_labelsize))
    ax2 = plt.twiny(ax)
    ax2.set_xlim(ax.get_xlim())
    ax2.get_xaxis().set_tick_params(labeltop=True, labelbottom=True, direction='out')
    ax2.set_xticks(list(xrange(cols)))
    ax2.xaxis.set_ticks_position('top')
    ax2.set_xticklabels(col_labels,rotation=90, fontsize=math.ceil(col_labelsize))
    ax.set_yticklabels(row_labels, fontsize=math.ceil(row_labelsize))

    if plot_title:
        plt.title(plot_title)
        
    plt.tight_layout()
    plt.savefig(heatmap_file)

    if os.path.isfile(heatmap_file):
        log.info("- created: %s  %s", heatmap_file, msg_ok)
    else:
        log.error("- failed to create: %s  %s", heatmap_file, msg_fail)
    
    # write as png also
    if heatmap_file.endswith(".pdf") and png_flag:
        png_file = heatmap_file.replace(".pdf", ".png")
        plt.savefig(png_file, dpi=png_dpi)
        
        if os.path.isfile(png_file):
            log.info("- created: %s  %s", png_file, msg_ok)
        else:
            log.error("- failed to create: %s  %s", png_file, msg_fail)
        
    plt.close()


    sys.exit(0)



"""

                      .-.
         .-._    _.../   `,    _.-.
         |   `'-'    \     \_'`   |
         \            '.__,/ `\_.--,
          /                '._/     |
         /                    '.    /
        ;   _                  _'--;
     '--|- (_)       __       (_) -|--'
     .--|-          (__)          -|--.
      .-\-                        -/-.
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`


"""
