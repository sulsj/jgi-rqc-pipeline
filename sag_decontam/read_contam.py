#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Read-Level Cross Contamination
- for each input fastq to an assembly, how many reads map to another assembly?
- user provides input file with library|input ref|input fastq
- based on Vasanth's code
/usr/common/jgi/qaqc/jgi-rnaseq/prod-7d4296e577d607734760a0eaaee2848227451746/bin/readLevelContamCheck.py

- outputs: read_contam.txt



Dependencies:
bbtools (seal)



v 1.0: 2018-01-02
- initial version

To do:
- how to handle "fake" assembly
- try to use python threads


~~~~~~~~~~~~~~~~~~~[ Test Cases ]

tangy pool: 96 sec to run
~/git/jgi-rqc-pipeline/sag_decontam/read_contam.py -o t1 -pl

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import getpass


# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, get_colors, run_cmd, get_msg_settings


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
For each fastq, run seal against all fastas to see what hit
- we are using the combined references to get a hit to each contig
- in the future might not count contigs < 2000bp ...

* need to use each reference in a csv in order to show the ref stats correctly
* seal should report like this for all.fasta (bbtools)
#name   %unambiguousReads       unambiguousMB   %ambiguousReads ambiguousMB     unambiguousReads        ambiguousReads
CCYUP   0.00002 0.00060 0.00000 0.00000 4       0
CCYWC   0.00001 0.00030 0.00000 0.00000 2       0
CCYWH   0.00003 0.00090 0.00000 0.00000 6       0
CCYWP   0.00002 0.00060 0.00000 0.00000 4       0
CCYWS   0.00001 0.00030 0.00000 0.00000 2       0
CCYWU   92.32801        2974.60815      0.00000 0.00000 19844318        0

$ seal.sh ambig=all mkf=0.6 refnames=f bbsplit nzo=t ow=t \
in=/global/projectb/scratch/brycef/sag/tangy/input/CCYUA-12075.1.235852.TAAGGCG-AGAGGAT.filter-SAG.fastq.gz \
ref=/global/projectb/scratch/brycef/sag/tangy/input/all_contigs.fasta \
refstats=/global/projectb/scratch/brycef/sag/tangy/input/CCYUA_read_contam.txt
* no, this outputs refstats incorrectly

30 seconds per fastq
'''
def do_seal(contam_file):
    log.info("do_seal")


    ref_fasta = None
    ref_fasta_list = []
    fh = open(contam_file, "r")
    for line in fh:
        line = line.strip()
        if line.startswith("#"):
            continue

        arr = line.split("|")
        if len(arr) != 3:
            continue

        lib = str(arr[0]).strip()
        fasta = str(arr[1]).strip()
        fastq = str(arr[2]).strip()


        if line.startswith("ALL"):
            #ref_fasta = fasta
            continue

        ref_fasta_list.append(fasta)

        fq_dict[lib] = fastq



    fh.close()

    ref_fasta = ",".join(ref_fasta_list)

    if not ref_fasta:
        #log.error("- cannot find combined fasta!  %s", msg_fail)
        log.error("- no references!  %s", msg_fail)
        sys.exit(2)

    seal_params = "ambig=all mkf=0.6 refnames=t bbsplit nzo=t ow=t"
    #seal_params = "ambig=al mkf=0.95 qhdist=0 refnames=f bbsplit nzo=t ow=t" # strict - very slow
    #nzo=f = write stats even if ref had 0 hits
    #ambig=all = use all best-matching sequences
    #refnames=t = use file names to count hits
    #bbsplit = write output in bbsplit format (unambig reads %, ambig reads %) - Vasanth request



    for lib in fq_dict:
        log.info("- lib: %s", lib)
        seal_log = os.path.join(output_path, "seal-%s.log" % lib)
        stats_file = os.path.join(output_path, "%s_read_contam.txt" % lib)

        if os.path.isfile(stats_file):
            log.info("- skipping seal, found file: %s", stats_file)
        else:
            #refstats = different format
            cmd = "#bbtools;seal.sh %s in=%s ref=%s refstats=%s > %s 2>&1" % (seal_params, fq_dict[lib], ref_fasta, stats_file, seal_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)


'''
For each *_read_contam.txt file, how many reads from lib %s mapped to an assembly

tokens1 =
ppm = int( (float(tokens[1]) + float(tokens[3]) ) *10000)


#name   %unambiguousReads       unambiguousMB   %ambiguousReads ambiguousMB     unambiguousReads        ambiguousReads
CCYUP   0.00002 0.00060 0.00000 0.00000 4       0

ppm = int(float(%unamib) + float(%ambig)) * 10,000)


'''
def summarize_results():
    log.info("summarize_results")

    summary_file = os.path.join(output_path, "read_contam.txt")
    fhw = open(summary_file, "w")
    fhw.write("#Source Library|Hit Library|PPM\n")

    for lib in fq_dict:
        log.info("- lib: %s", lib)

        stats_file = os.path.join(output_path, "%s_read_contam.txt" % lib)

        other_hit = 0 # hits to libraries that are not "lib"
        hit_dict = {} # lib -> hits in ppm
        if os.path.isfile(stats_file):
            fh = open(stats_file, "r")
            for line in fh:
                line = line.strip()

                if line.startswith("#"):
                    continue

                arr = line.split()
                if len(arr) != 7:
                    continue

                hit_lib = str(arr[0]).strip()[0:5]
                #print hit_lib
                #sys.exit(44)

                unambig_pct = float(arr[1])
                ambig_pct = float(arr[3])
                ppm = int((unambig_pct + ambig_pct) * 10000)
                

                if hit_lib in hit_dict:
                    hit_dict[hit_lib] += ppm
                else:
                    hit_dict[hit_lib] = ppm

                if hit_lib != lib:
                    other_hit += ppm


            fh.close()
            log.info("-- contam hits (ppm): %s", other_hit)

        for hit_lib in hit_dict:
            ppm = hit_dict[hit_lib]
            if ppm > max_ppm:
                ppm = max_ppm
            fhw.write("%s|%s|%s\n" % (lib, hit_lib, ppm))


    fhw.close()



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "Read-Level Cross Contamination"
    version = "1.0"

    uname = getpass.getuser() # get the user


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)


    # Parse options
    usage = "read_contam.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """

    """


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-i", "--input-file", dest="input_file", help = "File with a list of library|assembly file")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)

    output_path = None
    max_ppm = 9999 # could be a param but no one but me uses this
    input_file = None
    
    # parse args
    args = parser.parse_args()

    print_log = args.print_log

    if args.output_path:
        output_path = args.output_path

    if args.input_file:
        input_file = args.input_file

    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/sag"

        if output_path == "t1":
            input_file = os.path.join(test_path, "tangy", "input", "contam.txt")
            output_path = os.path.join("tangy", "input", "read_contam")



        output_path = os.path.join(test_path, output_path)



    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()

    # if not starting with /  use relative path (GAA-3421)
    if not output_path.startswith("/"):
        output_path = os.path.join(os.getcwd(), output_path)


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    if not input_file:
        print "Error: input file missing  %s" % msg_fail
        sys.exit(2)

    if not os.path.isfile(input_file):
        print "Error: input file does not exist  %s" % msg_fail
        sys.exit(2)



    # initialize my logger
    log_file = os.path.join(output_path, "read_contam.log")



    # log = logging object
    log_level = "INFO"

    log = get_logger("read_contam", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "input_file", input_file)
    log.info("%25s      %s", "output_path", output_path)

    log.info("")


    # Lets rock
    fq_dict = {} # populated by do_seal

    do_seal(input_file)

    summarize_results()

    sys.exit(0)



"""

                      .-.
         .-._    _.../   `,    _.-.
         |   `'-'    \     \_'`   |
         \            '.__,/ `\_.--,
          /                '._/     |
         /                    '.    /
        ;   _                  _'--;
     '--|- (_)       __       (_) -|--'
     .--|-          (__)          -|--.
      .-\-                        -/-.
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`

tangy test file:

# Library | Assembly File | Fastq File
CCYUA|/global/projectb/scratch/brycef/sag/tangy/input/CCYUA.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUA-12075.1.235852.TAAGGCG-AGAGGAT.filter-SAG.fastq.gz
CCYUB|/global/projectb/scratch/brycef/sag/tangy/input/CCYUB.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUB-12075.1.235852.TAAGGCG-CTCCTTA.filter-SAG.fastq.gz
CCYUC|/global/projectb/scratch/brycef/sag/tangy/input/CCYUC.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUC-12077.1.236082.TAAGGCG-TATGCAG.filter-SAG.fastq.gz
CCYUG|/global/projectb/scratch/brycef/sag/tangy/input/CCYUG.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUG-12077.1.236082.TAAGGCG-TACTCCT.filter-SAG.fastq.gz
CCYUH|/global/projectb/scratch/brycef/sag/tangy/input/CCYUH.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUH-12075.1.235852.TAAGGCG-AGGCTTA.filter-SAG.fastq.gz
CCYUN|/global/projectb/scratch/brycef/sag/tangy/input/CCYUN.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUN-12075.1.235852.TAAGGCG-ATTAGAC.filter-SAG.fastq.gz
CCYUO|/global/projectb/scratch/brycef/sag/tangy/input/CCYUO.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUO-12075.1.235852.CGTACTA-ATAGAGA.filter-SAG.fastq.gz
CCYUP|/global/projectb/scratch/brycef/sag/tangy/input/CCYUP.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUP-12075.1.235852.CGTACTA-AGAGGAT.filter-SAG.fastq.gz
CCYUS|/global/projectb/scratch/brycef/sag/tangy/input/CCYUS.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUS-12075.1.235852.CGTACTA-CTCCTTA.filter-SAG.fastq.gz
CCYUT|/global/projectb/scratch/brycef/sag/tangy/input/CCYUT.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUT-12075.1.235852.CGTACTA-TATGCAG.filter-SAG.fastq.gz
CCYUU|/global/projectb/scratch/brycef/sag/tangy/input/CCYUU.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUU-12075.1.235852.CGTACTA-TACTCCT.filter-SAG.fastq.gz
CCYUW|/global/projectb/scratch/brycef/sag/tangy/input/CCYUW.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUW-12075.1.235852.CGTACTA-AGGCTTA.filter-SAG.fastq.gz
CCYUX|/global/projectb/scratch/brycef/sag/tangy/input/CCYUX.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUX-12075.1.235852.CGTACTA-ATTAGAC.filter-SAG.fastq.gz
CCYUY|/global/projectb/scratch/brycef/sag/tangy/input/CCYUY.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUY-12075.1.235852.CGTACTA-CGGAGAG.filter-SAG.fastq.gz
CCYUZ|/global/projectb/scratch/brycef/sag/tangy/input/CCYUZ.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUZ-12075.1.235852.AGGCAGA-ATAGAGA.filter-SAG.fastq.gz
CCYWA|/global/projectb/scratch/brycef/sag/tangy/input/CCYWA.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWA-12075.1.235852.AGGCAGA-AGAGGAT.filter-SAG.fastq.gz
CCYWB|/global/projectb/scratch/brycef/sag/tangy/input/CCYWB.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWB-12075.1.235852.AGGCAGA-CTCCTTA.filter-SAG.fastq.gz
CCYWC|/global/projectb/scratch/brycef/sag/tangy/input/CCYWC.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWC-12075.1.235852.AGGCAGA-TATGCAG.filter-SAG.fastq.gz
CCYWG|/global/projectb/scratch/brycef/sag/tangy/input/CCYWG.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWG-12075.1.235852.AGGCAGA-TACTCCT.filter-SAG.fastq.gz
CCYWH|/global/projectb/scratch/brycef/sag/tangy/input/CCYWH.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWH-12075.1.235852.AGGCAGA-AGGCTTA.filter-SAG.fastq.gz
CCYWN|/global/projectb/scratch/brycef/sag/tangy/input/CCYWN.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWN-12075.1.235852.AGGCAGA-ATTAGAC.filter-SAG.fastq.gz
CCYWO|/global/projectb/scratch/brycef/sag/tangy/input/CCYWO.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWO-12075.1.235852.AGGCAGA-CGGAGAG.filter-SAG.fastq.gz
CCYWP|/global/projectb/scratch/brycef/sag/tangy/input/CCYWP.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWP-12075.1.235852.TCCTGAG-ATAGAGA.filter-SAG.fastq.gz
CCYWS|/global/projectb/scratch/brycef/sag/tangy/input/CCYWS.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWS-12075.1.235852.TCCTGAG-AGAGGAT.filter-SAG.fastq.gz
CCYWT|/global/projectb/scratch/brycef/sag/tangy/input/CCYWT.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWT-12075.1.235852.TCCTGAG-CTCCTTA.filter-SAG.fastq.gz
CCYWU|/global/projectb/scratch/brycef/sag/tangy/input/CCYWU.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWU-12075.1.235852.TCCTGAG-TATGCAG.filter-SAG.fastq.gz
CCYWW|/global/projectb/scratch/brycef/sag/tangy/input/CCYWW.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWW-12077.1.236082.TCCTGAG-TACTCCT.filter-SAG.fastq.gz
CCYWX|/global/projectb/scratch/brycef/sag/tangy/input/CCYWX.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWX-12077.1.236082.TCCTGAG-AGGCTTA.filter-SAG.fastq.gz
CCYWY|/global/projectb/scratch/brycef/sag/tangy/input/CCYWY.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWY-12077.1.236082.TCCTGAG-ATTAGAC.filter-SAG.fastq.gz
CCYWZ|/global/projectb/scratch/brycef/sag/tangy/input/CCYWZ.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWZ-12077.1.236082.TCCTGAG-CGGAGAG.filter-SAG.fastq.gz
CCYXA|/global/projectb/scratch/brycef/sag/tangy/input/CCYXA.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXA-12077.1.236082.GGACTCC-ATAGAGA.filter-SAG.fastq.gz
CCYXB|/global/projectb/scratch/brycef/sag/tangy/input/CCYXB.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXB-12077.1.236082.GGACTCC-AGAGGAT.filter-SAG.fastq.gz
CCYXC|/global/projectb/scratch/brycef/sag/tangy/input/CCYXC.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXC-12077.1.236082.GGACTCC-CTCCTTA.filter-SAG.fastq.gz
CCYXG|/global/projectb/scratch/brycef/sag/tangy/input/CCYXG.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXG-12077.1.236082.GGACTCC-TATGCAG.filter-SAG.fastq.gz
CCYXH|/global/projectb/scratch/brycef/sag/tangy/input/CCYXH.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXH-12077.1.236082.GGACTCC-TACTCCT.filter-SAG.fastq.gz
CCYXN|/global/projectb/scratch/brycef/sag/tangy/input/CCYXN.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXN-12077.1.236082.GGACTCC-AGGCTTA.filter-SAG.fastq.gz
CCYXO|/global/projectb/scratch/brycef/sag/tangy/input/CCYXO.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXO-12077.1.236082.GGACTCC-ATTAGAC.filter-SAG.fastq.gz
CCYXP|/global/projectb/scratch/brycef/sag/tangy/input/CCYXP.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXP-12077.1.236082.GGACTCC-CGGAGAG.filter-SAG.fastq.gz
CCYXS|/global/projectb/scratch/brycef/sag/tangy/input/CCYXS.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXS-12077.1.236082.TAGGCAT-ATAGAGA.filter-SAG.fastq.gz
CCYXT|/global/projectb/scratch/brycef/sag/tangy/input/CCYXT.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXT-12077.1.236082.TAGGCAT-AGAGGAT.filter-SAG.fastq.gz
CCYXU|/global/projectb/scratch/brycef/sag/tangy/input/CCYXU.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXU-12077.1.236082.TAGGCAT-CTCCTTA.filter-SAG.fastq.gz
CCYXW|/global/projectb/scratch/brycef/sag/tangy/input/CCYXW.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXW-12077.1.236082.TAGGCAT-TATGCAG.filter-SAG.fastq.gz
CCYXX|/global/projectb/scratch/brycef/sag/tangy/input/CCYXX.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXX-12077.1.236082.TAGGCAT-TACTCCT.filter-SAG.fastq.gz
CCYXY|/global/projectb/scratch/brycef/sag/tangy/input/CCYXY.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXY-12077.1.236082.TAGGCAT-AGGCTTA.filter-SAG.fastq.gz
CCYXZ|/global/projectb/scratch/brycef/sag/tangy/input/CCYXZ.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXZ-12077.1.236082.TAGGCAT-ATTAGAC.filter-SAG.fastq.gz
CCYYA|/global/projectb/scratch/brycef/sag/tangy/input/CCYYA.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYYA-12077.1.236082.TAGGCAT-CGGAGAG.filter-SAG.fastq.gz
CCYYB|/global/projectb/scratch/brycef/sag/tangy/input/CCYYB.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYYB-12077.1.236082.CTCTCTA-ATAGAGA.filter-SAG.fastq.gz
CCYYC|/global/projectb/scratch/brycef/sag/tangy/input/CCYYC.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYYC-12077.1.236082.CTCTCTA-AGAGGAT.filter-SAG.fastq.gz
CCYYG|/global/projectb/scratch/brycef/sag/tangy/input/CCYYG.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYYG-12077.1.236082.CTCTCTA-CTCCTTA.filter-SAG.fastq.gz
CCYYH|/global/projectb/scratch/brycef/sag/tangy/input/CCYYH.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYYH-12077.1.236082.CTCTCTA-TATGCAG.filter-SAG.fastq.gz
#FAKE|/global/projectb/scratch/brycef/sag/tangy/input/FAKE.fasta|/global/projectb/scratch/brycef/sag/tangy/input/
ALL|/global/projectb/scratch/brycef/sag/tangy/input/all_contigs.fasta|/global/projectb/scratch/brycef/sag/tangy/input/

"""
