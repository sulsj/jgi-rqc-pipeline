#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

SAG Decontam Pipeline
- used for single cell assemblies (Single Amplified Genome)
- runs crossblock (bbtools) to remove contigs with high levels of coverage that map to other assemblies
--- Crossblock uses coverage profiles to look for contigs with sections with low coverage that map to other fastqs

Dependencies:
bbtools (crossblock, tadpole, seal)
vsearch (clustering)
ssu_align (openmpi/1.8.7f)  (clustering)
mothur/1.32.1 (clustering)
graphviz
blast


v 1.0 2015-02-20:
- initial version

v 1.1 2015-09-29:
- needed to add plate_id because they're running the same plate name with different libraries ...

v 1.2: 2015-09-30:
- setup to take pool name as the input for the list of libraries to run

v 1.2.1 2015-10-23:
- make_heatmap() needs the size of the clean fasta to be > 0

v 1.2.2 2015-10-30:
- added fake wells

- takes fastq, spades assemblies from Jigsaw (references) and runs 2 different decontam tools from Brian Bushnell (bb) and Shoudan Liang (sl)
- then combine results from bb and sl into one clean fasta (only the contigs that intersect bb and sl)

v 1.3: 2015-11-25
- removed Shoudan's code because it was not very effective (disabled, flag to turn on)
- added tadpole assemblies for any failed assemblies (bbtools), optional - not great assemblies
- input contig count is wrong? no:
ATPZG  = 74, but report says 78 on input? 74 = scaffolds (">"), contigs = 78 (ok)
- blast *dirty.fasta (run_blast.pl) & save
- make_contig_remove_report not reporting correctly - fixed!
- skips steps if found files that indicate the step is done

v 1.3.1: 2015-11-30
- crawl through taxonomy if ncbi_organism_name is null

v 1.3.2: 2015-12-03
- create fake assembly for failures (CAT...CAT)

v 1.3.3: 2015-12-18
- param to use new sag pipeline
- minor pipeline cleanup
- fix for fake assemblies (AUXHS, AUXHP)

v 1.3.4: 2016-03-25
- jigsaw 2.6.2 - moved to global var
- save main commands used for decontam to a file for rqc reports
- renamed pdf files
- setup qsubs for new genepool config
--- RQCSUPPORT-686

v 1.3.5: 2016-04-20
- turned on Shoudan's code

v 1.3.6: 2016-04-27
- pass plate_id to pull libraries (minor updates)

v 1.3.7: 2016-06-02
- include chaff file of dirty contigs, don't qsub the merge_clean() function

v 1.3.8: 2016-06-07
- turned off Shoudan's code
- changes run_blast.pl to run_blastplus.py

v 1.3.9: 2016-06-22
- new heatmap code from Seung-Jin to add with PDFs

v 1.4.0: 2016-07-07
- error correction and k=62 for CrossBlock to reduce inter-species cross contam removal

v 1.4.1: 2016-08-29
- store contigs, etc in from the merge_report into rqc-stats.txt (RQCSUPPORT-793)

v 1.5: 2016-10-07
- https://issues.jgi-psf.org/browse/SEQQC-9399
--- sort taxonomy by 16s similarity, 16s comes from ITS/collaborators
--- 16s dist works, need taxonomic clustering
--- calc_16s_similarity() -- not hooked up but works partially
- fail if creating chaff file fails

v 1.5.1: 2016-10-10
- option to run everything on node to handle java failure on a 1 node queue, need to do this due to a new Java 1.8 option (metadataspace) used by bbtools
- turned on 16s similarity but not reporting on it

v 1.5.2: 2016-10-18
- added bp_removed_(lib_name) to stats output
- upgraded to jigsaw/2.6.4 (shouldn't affect anything)

v 1.6: 2017-01-25
- added 16s similarity computation for heatmaps (SEQQC-9399) - mothur (Rex) and vsearch (Alex)
- fixed naming of yellow (read level ppm contam), green (assembly cross contam) reports (pdf, png)
- re-made read level ppm and assembly cross contam reports using 16s groupings for mothur and vsearch
- updated run_tadpole to create assemblies
- create read_ppm data to create contig report for all sags (SEQQC-10301)
- added config file for commands etc

v 1.6.1: 2017-03-21
- fixed seaborn command using sag_decontam.cfg
- create seaborn map for read level contam (yellow)
- exit with error if crossblock fails to run
- save 16s similarity png maps
- fixed error with all red heatmap for contig level heatmap (BPZBS)

v 1.6.2: 2017-11-27
- changed to jgi_connect_db

v 1.7: 2018-03-21
- updated to work on denovo
-- replaced Vasanth's read contam (ppm/yellow) with read_contam.py
-- replaced Jigsaw asm_contam (pct/green) with asm_contam.py
-- replaced Vasanth's heatmap.py with new heatmap-b.py, include x & y axis titles
- removed qsub option (qsub flag)
- removed Shoudan's code
- refactored how group files are created in create_16s_txt


To do:



Notes...

2018-02-08
- green = contig level heatmaps (all_contigs vs shredded assemblies) - uses blast
- yellow = read leven contam in ppm (reads vs assemblies using seal)
- a "-" value indicates no hit, 0 indicates a hit rounded down to 0, empty center values mean no assembly was created


# crossblock output: library_name, assembly file, reads, grouping, organism name/ncbi tax id (tab delimited) - not yet

To do:
- test tadpole assemblies - worked, but removed all of the tapole assembly as dirt (auxhp) - do trimming on tadpole assemblies!
- check if all assemblies missing
* usually missing the -sag-asm flag

The 2nd part of analysis (SAG-2) is handled by the RQC System
- need to copy old spades directory from SAG-1 and the cleaned contigs to lab_decontam/lab_decontam.fasta
- and copy contam_id and something else too


If an assembly isn't available - bb wants it to be a simple file, sl doesn't work also the heatmap doesn't work
- instead don't include it

Pools 3+ or plates:
SAG-ASM:
runs spades + trims ends
SAG Decontam: (this program)
runs cross-block (BB), Shoudan decontam tools (SL) [SL not run anymore]
SAG-Pool:
ProDeGe (Kristin Tennessen) (if > 3 contigs + 200kbp)
QC Analysis for screened (after prodege) and unscreened (after SAG Decontam)


# needs 2 threads, qsubbing and monitoring stuff (2 threads for java 1.8 issue)

# t1: Ridge.rhizoCC.SAGs.1 (DeL.S164000.SYBR.01 2017-03-14)
#$ qsub -b yes -j yes -m as -now no -w e -N sd-t1 -l h_rt=108000 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sagd-t1.log -js 500 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag_decontam/sag_decontam.py -o t1"
$ qsub -b yes -j yes -m as -now no -w e -N sd-t1 -pe pe_32 32 -l exclusive.c=1 -l ram.c=3.5G,h_vmem=3.5G,h_rt=108000 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sagd-t1.log -js 500 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag_decontam/sag_decontam.py -o t1"

# t2: BAGSY
$ qsub -b yes -j yes -m as -now no -w e -N sd-t2 -l h_rt=108000 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sagd-t2.log -js 500 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag_decontam/sag_decontam.py -o t2"

# t3: --plate Sczyrba.BiogasSAGs.2 -m sag-asm
$ qsub -b yes -j yes -m as -now no -w e -N sd-t3 -pe pe_32 32 -l exclusive.c=1 -l ram.c=3.5G,h_vmem=3.5G,h_rt=108000 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sagd-t3.log -js 500 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag_decontam/sag_decontam.py -o t3"
# new with qsub_flag = 0

# t4: --plate Stew
$ qsub -b yes -j yes -m as -now no -w e -N sd-t4 -pe pe_32 32 -l exclusive.c=1 -l ram.c=3.5G,h_vmem=3.5G,h_rt=108000 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sagd-t4.log -js 500 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag_decontam/sag_decontam.py -o t4"
# new with qsub_flag = 0

# t5: --plate Ridge - dirty & reasm all fastq with tadpole (-m sag-pool pulls from disk)
$ qsub -b yes -j yes -m as -now no -w e -N sd-t5 -pe pe_32 32 -l exclusive.c=1 -l ram.c=3.5G,h_vmem=3.5G,h_rt=108000 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sagd-t5.log -js 500 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag_decontam/sag_decontam.py -o t5"


# stewart test
./sag_decontam.py -o /global/projectb/scratch/brycef/sag/stew2 --plate stew.SAGs.oceanOMZ.1 -m sag-asm

~~~~~~~~~~~~~~~~~~~~~~~
Denovo

$ sbatch /global/homes/b/brycef/git/jgi-rqc-pipeline/sag_decontam/cb-t1d.sh
- 8 hours to run

export PATH=/global/homes/q/qc_user/miniconda/miniconda2/bin:$PATH

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import re
import sys
from argparse import ArgumentParser
import glob
import MySQLdb
import time
import math
import getpass

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))
sys.path.append(os.path.join(my_path, '../sag'))
from common import get_logger, checkpoint_step, append_rqc_file, append_rqc_stats, get_colors, get_msg_settings, run_cmd, convert_cmd
from db_access import jgi_connect_db
from rqc_utility import get_blast_hit_count
from micro_lib import get_ssu, get_bbtools_version, load_config_file, get_the_path

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Symlink the raw fastqs
- might not be online (fail pipeline, pull from jamo)
- delete old symlinks
- link spades files (references)
- references.txt, raw_fastq.txt must match the same ref/lib per line for each file
- combine all references into all_contigs.fasta for SL
'''
def link_input_files(lib_list, ref_file, fq_file, mode):

    log.info("link_raw_fastq: %s", lib_list)


    input_path = get_the_path(INPUT_PATH, output_path)


    all_contigs = os.path.join(input_path, "all_contigs.fasta")
    if os.path.isfile(all_contigs):
        log.info("- %s found, skipping this step", all_contigs)
        return

    lib_dict = {}

    # for testing
    if lib_list[0] == "ABCD":
        lib_list = []


    if ref_file:
        log.info("- reading references from: %s", ref_file)
        fh = open(ref_file, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue
            arr = line.split("=")
            if len(arr) > 1:
                lib_dict[str(arr[0]).strip()] = { "ref" : str(arr[1]).strip() }
        fh.close()


    if fq_file:
        # lib, path, organism name, well
        log.info("- reading fastqs from: %s", fq_file)
        fh = open(fq_file, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue
            arr = line.split("=")
            if len(arr) > 1:
                lib = str(arr[0]).strip()
                fq = str(arr[1]).strip()
                org = str(arr[2]).strip()
                well = str(arr[3]).strip()
                if lib in lib_dict:
                    lib_dict[lib]['well'] = well
                    lib_dict[lib]['org'] = org
                    lib_dict[lib]['fq'] = fq
                else:
                    lib_dict[lib] = { "well" : well, "org" : org, "fq" : fq }

        fh.close()


    if len(lib_list) > 0:

        db = jgi_connect_db("rqc")
        if not db:
            log.error("- no db connection")

            status = "failed"
            checkpoint_step(status_log, status)

            sys.exit(4)

        sth = db.cursor(MySQLdb.cursors.DictCursor)

        # 'COMPRESSED FILTERED FASTQ'
        # 'ILLUMINA COMPRESSED RAW FASTQ'

        # 22 = jigsaw sag-1
        # 28 = sag-asm
        pipeline_type_id = 28
        #mode = "sag-asm"
        if mode == "sag-asm":
            pipeline_type_id = 28
        elif mode == "sag-pool":
            pipeline_type_id = 29
        elif mode == "jigsaw":
            pipeline_type_id = 22


        sql = """
select
    s.seq_unit_id,
    s.library_name,
    concat(sf.fs_location, '/', sf.file_name) as raw_fastq,
    sf.file_name,
    rpq.fs_location as run_folder,
    l.ncbi_organism_name,
    l.tax_kingdom,
    l.tax_phylum,
    l.tax_class,
    l.tax_order,
    l.tax_family,
    l.tax_genus,
    l.tax_species,
    l.well_id
from seq_units s
inner join seq_unit_file sf on s.seq_unit_id = sf.seq_unit_id and sf.file_type = 'COMPRESSED FILTERED FASTQ'
left join rqc_pipeline_queue rpq on s.seq_unit_id = rpq.seq_unit_id
left join library_info l on s.rqc_library_id = l.library_id
where
    rpq.rqc_pipeline_type_id = [ptid]
    and rpq.rqc_pipeline_status_id in (13, 14)
    and s.library_name in (
        """
        # status = 14 = pipeline complete -- ignore the failures (bb-decontam doesn't like failures!)
        #          13 = pipeline failed
        # 22 =  SAG-1: the spades assembly (filtered fastq -> normalization -> spades -> trimming)

        for lib in lib_list:
            sql += "%s,"
        sql = sql[:-1]
        sql = sql + ")"


        sql = sql.replace("[ptid]", str(pipeline_type_id))

        #print sql

        #print pipeline_type_id
        #print lib_list
        # can't print sql % (lib_list)


        sth.execute(sql, (lib_list))
        row_cnt = int(sth.rowcount)
        log.info("- %s seq units", row_cnt)
        if row_cnt == 0:
            sys.exit(20)

        org_list = ['ncbi_organism_name', 'tax_species', 'tax_genus', 'tax_family', 'tax_order', 'tax_class', 'tax_phylum', 'tax_kingdom']

        for _ in range(row_cnt):

            rs = sth.fetchone()

            fq = rs['raw_fastq']
            ref = None

            # crawl through org list to get some organism name
            org = "unknown"
            for o in org_list:
                if rs[o]:
                    org = rs[o]
                    break


            well = rs['well_id']

            # for pooled libs
            if not well:
                well = get_fake_well(_+1)

            if rs['run_folder'] and os.path.isdir(rs['run_folder']):

                # contigs.2k.fasta are contigs > 2kb, Tanja determined that the contigs less than 2k were not useful for the assembly
                #fasta_list = glob.glob(os.path.join(rs['run_folder'], "spades/contigs.2k.fasta"))
                fasta_list = []

                if mode == "sag-asm" or mode == "sag-pool":
                    #print os.path.join(rs['run_folder'], "trim", "scaffolds.trim.fasta")
                    fasta_list = glob.glob(os.path.join(rs['run_folder'], "trim", "scaffolds.trim.fasta"))
                else:
                    fasta_list = glob.glob(os.path.join(rs['run_folder'], "spades/scaffolds.2k.fasta"))



                # should be only 1
                for fasta in fasta_list:
                    ref = fasta

            #print rs['library_name'], ref

            # overwrite values
            if rs['library_name'] in lib_dict:
                if lib_dict[rs['library_name']].get("fq"):
                    pass
                else:
                    lib_dict[rs['library_name']]['fq'] = fq

                if lib_dict[rs['library_name']].get("org"):
                    pass
                else:
                    lib_dict[rs['library_name']]['org'] = org

                if lib_dict[rs['library_name']].get("ref"):
                    pass
                else:
                    lib_dict[rs['library_name']]['ref'] = ref

                if lib_dict[rs['library_name']].get("well"):
                    pass
                else:
                    lib_dict[rs['library_name']]['well'] = well




            else:
                # have nothing add to library dict
                lib_dict[rs['library_name']] = { "well" : well, "org" : org, "fq" : fq, "ref" : ref }

        db.close()



    else:
        # remove the old files to reset the run
        del_list = glob.glob(os.path.join(input_path, "*.fastq.*"))
        del_list += glob.glob(os.path.join(input_path, "*.fasta"))


        for fq in del_list:

            cmd = "rm %s" % (fq)
            std_out, std_err, exit_code = run_cmd(cmd, log)


    # process our library dict

    lib_cnt = 0 # count actual number of libraries we want to do

    offline_cnt = 0 # number of fastqs offline
    offline_ref_cnt = 0 # number of scaffolds.2k.fasta offline

    # these 2 files used for bbdecontam/crossblock, libs must be in the same order
    fastq_file = os.path.join(output_path, "input_fastqs.txt") # input fastqs for assemblies
    ref_file = os.path.join(output_path, "references.txt") # assemblies
    tadpole_file = os.path.join(output_path, "tadpole.txt")
    heatmap = os.path.join(output_path, "heatmap.txt")

    fhfq = open(fastq_file, "w") # input fastqs
    fhfa = open(ref_file, "w") # spades contigs/assemblies

    ftpole = open(tadpole_file, "w") # list of libraries to run tadpole (if option is set)
    fhhm = open(heatmap, "w") # heatmap data - well, organism, lib - used by create_heatmap_files, create_16s_txt


    fhhm.write("#Well Id|Organism|Library|Group\n")

    for lib in lib_dict:
        #print "%s = %s, %s" % (lib, lib_dict[lib]['ref'], lib_dict[lib]['well'])

        fhhm.write("%s|%s|%s|group-0\n" % (lib_dict[lib]['well'], lib_dict[lib]['org'], lib))

        # symlink fastq
        fq = lib_dict[lib]['fq']
        if os.path.isfile(fq):
            new_fastq = os.path.join(input_path, "%s-%s" % (lib, os.path.basename(fq)))

            cmd = "ln -s %s %s" % (fq, new_fastq)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            if exit_code == 0:
                fhfq.write(new_fastq+"\n")


        else:
            log.error("- %s is offline, needs JAMO restore", fq)
            #jamo fetch filename 6513.2.46682.GAGTGG.fastq.gz: raw fastq
            #jamo fetch all filename 6513.2.46682.GAGTGG.filter.fastq.gz: filtered (needs all keyword)
            fhfq.write("jamo fetch all filename %s\n" % os.path.basename(fq))
            offline_cnt += 1



        ref = lib_dict[lib]['ref']
        #print lib,  ref

        if ref and os.path.isfile(ref):



            if os.path.getsize(ref) == 0:

                # create fake assembly, put 18 cats in it
                # "I have a cat named Peppermint and a cat named Dino.
                # I have more cats but they are referred to as 'others' as I cannot think of sixteen more names."
                log.info("- assembly for %s empty, creating a fake one", lib)
                fake_assembly = os.path.join(input_path, "FAKE.fasta")
                fh_fake = open(fake_assembly, "w")
                fh_fake.write(">FAKE_ASSEMBLY_CREATED_BY_SAG_DECONTAM\n")
                fh_fake.write("CAT" * 18 + "\n")
                fh_fake.close()
                ref = fake_assembly

                # redo with tadpole
                if tadpole_flag:
                    log.info("- need to run tadpole on: %s", lib)
                    ftpole.write("%s\t%s\n" % (lib, fq))

            new_fasta = os.path.join(input_path, "%s.fasta" % (lib))
            rename_headers = True # check if headers have >(lib)_ already
            header_key = ">%s_" % lib
            fh = open(ref, "r")
            for line in fh:
                if line.startswith(header_key):
                    rename_headers = False
                    break
            fh.close()

            if rename_headers:
                cmd = "#bbtools;bbrename.sh in=%s out=%s prefix=%s addprefix" % (ref, new_fasta, lib)
                append_cmd_log(cmd)
                std_out, std_err, exit_code = run_cmd(cmd, log)

            else:

                cmd = "cp %s %s" % (ref, new_fasta)
                std_out, std_err, exit_code = run_cmd(cmd, log)


            if exit_code == 0:

                lib_cnt += 1
                fhfa.write(new_fasta + "\n")

        else:

            # write the info
            if tadpole_flag:
                log.info("- need to run tadpole on: %s", lib)
                ftpole.write("%s\t%s\n" % (lib, fq))
                new_fasta = os.path.join(input_path, "%s.fasta" % (lib))
                lib_cnt += 1
                fhfa.write(new_fasta + "\n")

            else:
                #log.info("- skipping %s because no assembly file", lib)
                log.info("- cannot find assembly for %s, creating a fake one", lib)
                fake_assembly = os.path.join(input_path, "%s.fasta" % lib)
                fh_fake = open(fake_assembly, "w")
                fh_fake.write(">%s_FAKE_ASSEMBLY_CREATED_BY_SAG_DECONTAM\n" % lib)
                fh_fake.write("CAT" * 18 + "\n")
                fh_fake.close()
                ref = fake_assembly
                lib_cnt += 1
                fhfa.write(fake_assembly + "\n")

                #offline_ref_cnt += 1

    append_rqc_stats(rqc_stats_log, "library_count", lib_cnt)
    log.info("- using %s/%s libraries", lib_cnt, len(lib_dict))
    if lib_cnt != len(lib_dict):
        log.error("- fastq count != assembly count (%s != %s)  %s", len(lib_dict), lib_cnt, msg_fail)
        sys.exit(10)


    fhfq.close()
    fhfa.close()
    fhhm.close()
    ftpole.close()

    # files for heatmap.py input
    heatmap_group_file = os.path.join(output_path, "heatmap-group.txt")
    setup_heatmap_groups(heatmap) # create heatmap.txt, heatmap-group.txt
    create_grouping_file(heatmap, heatmap_group_file)

    # merge all of the *.fasta into all_contigs.fasta for sl decontam
    # - even though sl is off, still using it

    if os.path.isfile(all_contigs):
        cmd = "rm %s" % (all_contigs)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    cmd = "cat %s >> %s" % (os.path.join(input_path, "*.fasta"), all_contigs)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.getsize(all_contigs) > 10:
        pass
    else:
        log.error("- all_contigs.fasta is too small!  %s", msg_fail)
        sys.exit(2)



    if offline_cnt > 0:
        log.info("- %s fastqs need to be restored from jamo  %s", offline_cnt, msg_fail)
        status = "link input files failed"
        sys.exit(2)


    if offline_ref_cnt > 0:
        log.info("- %s scaffolds.2k.fasta need to be restored from jamo  %s", offline_ref_cnt, msg_fail)
        #status = "link scaffolds.2k.fa files failed"
        sys.exit(2)



'''
re-sort heatmap.txt by organism name and add group-xx
- only for first heatmap created in link_input_files
'''
def setup_heatmap_groups(heatmap_file):
    log.info("setup_heatmap_groups: %s", heatmap_file)

    # resort heatmap, add groups based on organism name
    heatmap_new = heatmap_file.replace(".txt", "-new.txt")
    # sort but leave header at the top, use kolumn 2, delimiter = |
    cmd = "(head -1 %s && tail -n +2 %s | sort -k2 -t '|') > %s" % (heatmap_file, heatmap_file, heatmap_new)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    cmd = "rm %s" % heatmap_file
    std_out, std_err, exit_code = run_cmd(cmd, log)

    fhw = open(heatmap_file, "w")
    fh = open(heatmap_new, "r")
    org_cnt = 0
    last_org = "Iron Maiden" # Somewhere in Time 1986
    for line in fh:
        line = line.strip()
        # keep comment line
        if line.startswith("#"):
            pass
        else:
            arr = line.split("|")
            org = str(arr[1]).strip()

            if org != last_org:
                org_cnt += 1
            last_org = org

            org_group = "group-%s" % org_cnt
            line = line.replace("group-0", org_group)

        fhw.write(line + "\n")

    fh.close()
    fhw.close()
    log.info("- heatmap has %s organism groups", org_cnt)

    cmd = "rm %s" % heatmap_new
    std_out, std_err, exit_code = run_cmd(cmd, log)


'''
create grouping file for heatmap.py using heatmap.txt format
1:12::1:12
13:21::13:21
'''
def create_grouping_file(heatmap_file, group_file):
    log.info("create_grouping_file: %s, %s", heatmap_file, group_file)

    fhw = open(group_file, "w")

    last_group = "Iron Maiden" # Piece of Mind 1984
    row_cnt = 0
    start_row = 1
    end_row = 1

    fh = open(heatmap_file, "r")
    for line in fh:
        line = line.strip()
        if line.startswith("#"):
            continue

        row_cnt += 1
        end_row = row_cnt

        arr = line.split("|")

        my_group = arr[3]

        if my_group != last_group:
            if end_row - 1 > 0:
                log.info("- %s: %s to %s", last_group, start_row, end_row-1)
                fhw.write("%s:%s::%s:%s\n" % (start_row, end_row-1, start_row, end_row-1))
            start_row = end_row

        last_group = my_group

    # last one
    log.info("- %s: %s to %s", last_group, start_row, end_row)
    fhw.write("%s:%s::%s:%s\n" % (start_row, end_row, start_row, end_row))
    fh.close()
    fhw.close()



'''
Some pools don't have wells, so we will create fake ones
well's J,K,L,M,N,O,P,Q indicate FAKE wells
- id = [1..96]
'''
def get_fake_well(well_id):

    fake_well = "X1"
    if well_id < 0:
        well_id = well_id * -1
    if well_id > 96:
        well_id = well_id % 96

    if well_id < 97:
        fake_rows = [ 'J', 'K', 'L', 'M', 'N', 'O', 'P']
        col = well_id % 12
        row = int(well_id / 12)

        fake_well = "%s%s" % (fake_rows[row], col)
        #print id, row, col, fake_well
    return fake_well


'''
Launch tadpole jobs to create assemblies
k = 42 - suggestion from BB
- runs quickly, but assemblies not great
- doesn't run tadpole if the [lib_name].fasta exists
- runs normalization, subsampling and trimming but results might not match
* spades does a better job of assembly than tadpole - 20161212
- not used by default
'''
def run_tadpole():

    log.info("run_tadpole")
    tadpole_file = os.path.join(output_path, "tadpole.txt")

    log.info("- tadpole list: %s", tadpole_file)

    tadpole_path = get_the_path(TADPOLE_PATH, output_path)

    tadpole_cnt = 0

    k = 42 # kmer size for assembly

    if os.path.isfile(tadpole_file):

        # lib name, fastq
        fh = open(tadpole_file, "r")
        for line in fh:
            log.info("- %s", line.strip())


            arr = line.split()
            lib_name = arr[0]
            fq = arr[1]

            if os.path.isfile(fq):

                log_file = os.path.join(tadpole_path, "tadpole-%s.log" % lib_name)
                asm_file = os.path.join(tadpole_path, "%s-raw.fasta" % lib_name)
                asm_trim_file = os.path.join(tadpole_path, "%s.fasta" % lib_name) # to look like normal assembly

                if os.path.isfile(asm_trim_file):
                    log.error("- assembly exists: %s", os.path.basename(asm_trim_file))

                else:

                    norm_log_file = os.path.join(tadpole_path, "norm-%s.log" % lib_name)
                    fq_norm = os.path.join(tadpole_path, "%s-norm.fastq.gz" % lib_name)
                    cmd = "#bbtools;bbnorm.sh in=%s out=%s %s > %s 2>&1" % (fq, fq_norm, config['norm_params'], norm_log_file)

                    append_cmd_log(cmd)
                    std_out, std_err, exit_code = run_cmd(cmd, log)


                    # error correct
                    err_log_file = os.path.join(tadpole_path, "err-%s.log" % lib_name)
                    cmd = "#bbtools;tadpole.sh in=%s %s > %s 2>&1" % (fq_norm, config['err_params'], err_log_file)
                    append_cmd_log(cmd)
                    std_out, std_err, exit_code = run_cmd(cmd, log)


                    subsample_read_count = config['subsample_read_count']
                    subsample_read_count = int(subsample_read_count)/2 # bbtools treats 20M as 20m PAIRED reads = 40m reads
                    fq_sub = os.path.join(tadpole_path, "%s-norm-sub.fastq.gz" % lib_name)
                    sub_log = os.path.join(tadpole_path, "sub-%s.log" % lib_name)
                    cmd = "#bbtools;reformat.sh samplereadstarget=%s in=%s out=%s ow=t > %s 2>&1" % (subsample_read_count, fq_norm, fq_sub, sub_log)
                    append_cmd_log(cmd)
                    std_out, std_err, exit_code = run_cmd(cmd, log)


                    cmd = "#bbtools;tadpole.sh in=%s k=%s out=%s %s > %s 2>&1" % (fq_sub, k, asm_file, config['tadpole_params'], log_file)
                    # shave = remove dead ends
                    # rinse = remove bubbles
                    append_cmd_log(cmd)
                    std_out, std_err, exit_code = run_cmd(cmd, log)


                    # for mapping before trimming
                    cov_contig_stats =  os.path.join(tadpole_path, "cov-%s.log" % lib_name)
                    map_log =  os.path.join(tadpole_path, "map-%s.log" % lib_name)
                    cmd = "#bbtools;bbmap.sh in=%s ref=%s %s covstats=%s > %s 2>&1" % (fq_sub, asm_file, config['trim_map_params'], cov_contig_stats, map_log)
                    append_cmd_log(cmd)
                    std_out, std_err, exit_code = run_cmd(cmd, log)


                    # trimming
                    trim_log =  os.path.join(tadpole_path, "trim-%s.log" % lib_name)
                    cmd = "#bbtools;filterbycoverage.sh in=%s out=%s cov=%s %s > %s 2>&1" % (asm_file, asm_trim_file, cov_contig_stats, config['trim_params'], trim_log)
                    append_cmd_log(cmd)
                    std_out, std_err, exit_code = run_cmd(cmd, log)



                    # need to rename the contigs
                    new_asm_file = os.path.basename(asm_trim_file)
                    new_asm_file = os.path.join(output_path, "input", new_asm_file) # move to input folder too
                    prefix = "%s-tadpole" % (lib_name) # indicate tadpole asm
                    cmd = "#bbtools;bbrename.sh in=%s out=%s prefix=%s addprefix" % (asm_trim_file, new_asm_file, prefix)
                    append_cmd_log(cmd)
                    std_out, std_err, exit_code = run_cmd(cmd, log)

                    tadpole_cnt += 1



            else:

                log.error("- cannot find fq: %s", fq)

        fh.close()


    else:
        log.info("- no tadpole file, nothing to do.  %s", msg_warn)



'''
SEQQC-9399 - Alex's method for similarity
- input for heatmap and for crossblock (not crossblock yet)
'''
def calc_16s_similarity_vsearch():
    log.info("calc_16s_similarity")

    s_path = get_the_path(VSEARCH_PATH, output_path)


    # get the 16s for everything

    bbrename_log = os.path.join(s_path, "bbrename.log")
    lib_list = [] # array of library names to use later...
    heatmap = os.path.join(output_path, "heatmap.txt")
    fh = open(heatmap, "r")
    for line in fh:
        line = line.strip()
        if line.startswith("#"):
            continue
        arr = line.split("|")

        lib_name = str(arr[2]).strip()
        lib_list.append(lib_name)

        file_16s_org = os.path.join(s_path, "%s-16s-org.fasta" % lib_name)
        if not os.path.isfile(file_16s_org):
            get_ssu(lib_name, file_16s_org, log)

            # prepend with library name
            file_16s = os.path.join(s_path, "%s-16s.fasta" % lib_name)
            cmd = "#bbtools;bbrename.sh in=%s out=%s prefix=%s addprefix ow=t >> %s 2>&1" % (file_16s_org, file_16s, lib_name, bbrename_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

    fh.close()


    # append all 16s into 1 file
    all_contigs = os.path.join(s_path, "all_16s.fasta")
    if os.path.isfile(all_contigs):
        cmd = "rm %s" % (all_contigs)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    cmd = "cat %s >> %s" % (os.path.join(s_path, "*16s.fasta"), all_contigs)
    std_out, std_err, exit_code = run_cmd(cmd, log)



    vsearch_log = os.path.join(s_path, "vsearch_version.log")
    cmd = "#%s;vsearch -v > %s 2>&1" % (config['vsearch_module'], vsearch_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)


    vsearch_out = os.path.join(s_path, "vsearch.16s.out")
    vsearch_log = os.path.join(s_path, "vsearch.log")

    #cmd = "vsearch --threads 8 --notrunclabels --acceptall --iddef 4 --self --query_cov 0.1 --strand both --allpairs_global %s --userfields query+target+ql+tl+id4 --userout %s > %s 2>&1"
    cmd = "#%s;vsearch %s --userout %s > %s 2>&1" % (config['vsearch_module'], config['vsearch_params'], vsearch_out, vsearch_log)
    cmd = cmd.replace("[fasta]", all_contigs)

    #awk '{$5>=0.97}' vsearch.16S.out

    # save file
    std_out, std_err, exit_code = run_cmd(cmd, log)


    create_16s_txt(s_path, vsearch_out, "vsearch")



'''
read libs from HEATMAP.txt
- uses Rex's method
dist -> similarity = sqrt(1 - dist/100)
'''
def calc_16s_similarity_mothur():
    log.info("calc_16s_similarity_mothur")

    s_path = get_the_path(MOTHUR_PATH, output_path)


    # get the 16s for everything

    bbrename_log = os.path.join(s_path, "bbrename.log")
    lib_list = [] # array of library names to use later...
    heatmap = os.path.join(output_path, "heatmap.txt")
    fh = open(heatmap, "r")
    for line in fh:
        line = line.strip()
        if line.startswith("#"):
            continue

        arr = line.split("|")
        lib_name = str(arr[2]).strip()
        lib_list.append(lib_name)

        file_16s_org = os.path.join(s_path, "%s-16s-org.fasta" % lib_name)
        if not os.path.isfile(file_16s_org):
            get_ssu(lib_name, file_16s_org, log)

            # prepend with library name
            file_16s = os.path.join(s_path, "%s-16s.fasta" % lib_name)
            cmd = "#bbtools;bbrename.sh in=%s out=%s prefix=%s addprefix ow=t >> %s 2>&1" % (file_16s_org, file_16s, lib_name, bbrename_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

    fh.close()


    # append all 16s into 1 file
    all_contigs = os.path.join(s_path, "all_16s.fasta")
    if os.path.isfile(all_contigs):
        cmd = "rm %s" % (all_contigs)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    cmd = "cat %s >> %s" % (os.path.join(s_path, "*16s.fasta"), all_contigs)
    std_out, std_err, exit_code = run_cmd(cmd, log)


    # afa files = aligned fasta
    archaea_afa = os.path.join(s_path, "ssu_output", "ssu_output.archaea.afa")
    bacteria_afa = os.path.join(s_path, "ssu_output", "ssu_output.bacteria.afa")

    afa_list = [archaea_afa, bacteria_afa]

    afa_cnt = 0
    for afa_file in afa_list:
        if os.path.isfile(afa_file):
            afa_cnt += 1

    if afa_cnt > 0:
        log.info("- skipping ssu-align step, found afa files")

    else:

        cluster = os.environ.get('NERSC_HOST', 'unknown')
        cluster = cluster.lower()
        cluster_type = "uge"
        if cluster in ("denovo", "cori"):
            cluster_type = "slurm"

        '''
Note for genepool:
I would do through run_command but get an error which is resolved by creating bash script ...
ERROR, the required executable ssu-esl-seqstat is not in your PATH environment
variable or can't run on this system. See the User's Guide Installation section.

fix by adding to path:
$ PATH=$PATH:/usr/common/jgi/aligners/ssu-align/0.1.1/bin/ssu-esl-seqstat

2017-12-15: keeping ssu.sh because it will still be genepool & denovo compatible
        '''


        ssu_script = os.path.join(s_path, "ssu.sh")
        fh = open(ssu_script, "w")
        fh.write("#!/bin/bash -l\n")
        fh.write("\n")
        fh.write("# path append\n")


        fh.write("PATH=$PATH:%s\n" % config['ssu_path'])
        fh.write("\n")
        if cluster_type == "uge":
            fh.write("module load %s %s\n" % (config['ssu_align_module'], config['openmpi_module']))
            fh.write("\n")
        fh.write("cd %s\n" % s_path)
        fh.write("\n")

        ssu_log = os.path.join(s_path, "ssu.log")
        cmd = "ssu-align --dna -f %s ssu_output > %s 2>&1" % (os.path.basename(all_contigs), ssu_log)
        if cluster_type == "slurm":
            cmd = "#%s;ssu-align --dna -f %s ssu_output > %s 2>&1" % (config['ssu_align_module'], os.path.basename(all_contigs), ssu_log)
            cmd = convert_cmd(cmd)
        fh.write(cmd + "\n")
        fh.write("\n")

        ssu_mask_log = os.path.join(s_path, "ssu_mask.log")
        cmd = "ssu-mask ssu_output > %s 2>&1\n" % ssu_mask_log
        if cluster_type == "slurm":
            cmd = "#%s;ssu-mask ssu_output > %s 2>&1\n" % (config['ssu_align_module'], ssu_mask_log)
            cmd = convert_cmd(cmd)
        fh.write(cmd + "\n")
        fh.write("\n")

        ssu_mask_log2 = os.path.join(s_path, "ssu_mask-stk2afa.log")
        cmd = "ssu-mask ssu_output --stk2afa > %s 2>&1\n" % ssu_mask_log2
        if cluster_type == "slurm":
            cmd = "#%s;ssu-mask ssu_output --stk2afa > %s 2>&1\n" % (config['ssu_align_module'], ssu_mask_log2)
            cmd = convert_cmd(cmd)
        fh.write(cmd + "\n")
        fh.write("\n")

        fh.close()

        log.info("- running ssu_align commands")
        ssu_script_log = os.path.join(s_path, "ssu_script.log")

        cmd = "bash %s > %s 2>&1" % (ssu_script, ssu_script_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)



    # reformat *.afa files and run mothur for distance matrix
    # the .afa files generated by ssu-align need to be reformated by changing lowercase bases to uppercase and replacing gap symbol “.” with “-“ so that mothur can process files.


    for afa_file in afa_list:

        dist_file = os.path.join(s_path, "ssu_output", afa_file.replace(".afa", ".dist"))
        if os.path.isfile(dist_file):
            log.info("- skipping reformat and mothur, found: %s", dist_file)
        else:

            if os.path.isfile(afa_file):
                log.info("- reformatting file: %s", afa_file)
                afa_file_old = afa_file.replace(".afa", ".afa_old")
                cmd = "cp %s %s" % (afa_file, afa_file_old)
                std_out, std_err, exit_code = run_cmd(cmd, log)


                fh = open(afa_file_old)
                fhw = open(afa_file, "w")
                for line in fh:
                    if line.startswith(">"):
                        pass
                    else:
                        line = line.replace(".","-").upper()
                    fhw.write(line)

                fhw.close()
                fh.close()

                log.info("- calculating similarity matrix with mothur")
                mothur_log = afa_file.replace(".afa", ".mothur.log")

                # mothur is quick
                cmd = "#%s;mothur \"#dist.seqs(fasta=%s, countends=F);quit()\" > %s 2>&1" % (config['mothur_module'], afa_file, mothur_log)
                std_out, std_err, exit_code = run_cmd(cmd, log)


            else:
                log.warn("- could not find afa file: %s  %s", afa_file, msg_warn)

        if os.path.isfile(dist_file):
            log.info("- found dist_file: %s", dist_file)

        else:
            log.error("- No dist file created from: %s  %s", afa_file, msg_fail)



    # create similarity file
    for afa_file in afa_list:
        dist_file = os.path.join(s_path, "ssu_output", afa_file.replace(".afa", ".dist"))
        if os.path.isfile(dist_file):
            sim_file = dist_file.replace(".dist", ".sim")
            fhw = open(sim_file, "w")
            fhw.write("#lib_from lib_to distance similarity\n")
            fh = open(dist_file, "r")
            for line in fh:
                arr = line.split()
                if len(arr) > 2:
                    lib_from = str(arr[0]).split("_")[0]
                    lib_to = str(arr[1]).split("_")[0]
                    dist = float(arr[2])
                    similarity = 0.0
                    if dist > 0:
                        similarity = math.sqrt(1 - dist) * 100.0
                    fhw.write("%s %s %s %s\n" % (lib_from, lib_to, dist, similarity))

            fh.close()
            fhw.close()


        # only do this for bacteria
        if "bacteria" in dist_file:
            create_16s_txt(s_path, sim_file, "mothur")


    # UPGMA = Unweighted Pair Group Method with Arithmetic Mean



'''
create heatmap-16s-*.txt
- based on the pathname, we will figure out the input file
- creates png dot file of libs connected to each other

'''
def create_16s_txt(path_16s, input_file, mode):
    log.info("create_16s_txt: %s", input_file)



    if os.path.isfile(input_file):

        group_dict = {} # [group_id] = csv of members?
        group_cnt = 0
        group_key = ""


        dot_file = os.path.join(path_16s, "16s_%s.gv" % mode)
        fhw = open(dot_file, "w")
        fhw.write("digraph similarity_%s {\n" % mode)
        #fhw.write("  size=\"20,20\"\n")
        fhw.write("  rankdir=LR;\n")
        fhw.write("  node [ shape = circle ];\n")

        #sim_id = 4 # array position 4 in vsearch
        #if mode == "mothur":
        #    sim_id = 3

        threshold = 97.0 # based on illumina error rate
        #threshold = 80.8 # testing - makes more groups

        fh = open(input_file, "r")

        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue

            arr = line.split()

            #similarity = float(arr[sim_id])
            similarity = float(arr.pop())
            if similarity >= threshold:
                lib1 = str(arr[0]).split("_")[0]
                lib2 = str(arr[1]).split("_")[0]
                #print lib1, lib2, similarity, group_cnt
                fhw.write("  %s -> %s [ label = \"%s\" ];\n" % (lib1, lib2, "{:0.2f}".format(similarity)) )

                # look for the group_key for lib1, if found then add lib2 to group_dict[group_key]
                # also reverse lib1 & lib2 - if found lib2 then add lib1 to group_dict[group_key]
                # if not found in any group then create a new group
                found = False
                group_key = ""
                for g in group_dict:
                    arr = str(group_dict[g]).split(",")
                    if lib1 in arr:
                        group_key = g
                        found = True
                        continue

                    if lib2 in arr:
                        group_key = g
                        found = True
                        lib_tmp = lib2
                        lib2 = lib1
                        lib1 = lib_tmp
                        continue


                if not found:
                    group_cnt += 1
                    group_key = "group-%s" % group_cnt
                    group_dict[group_key] = lib1 + ","

                # okay if same lib added to the group
                group_dict[group_key] += lib2 + ","



        fh.close()
        fhw.write("}\n")
        fhw.close()



        # merge group_dict keys
        # look for each library in the other groups to figure out what to merge & add to the merge_dict
        merge_dict = {}
        for g in sorted(group_dict.iterkeys()):
            arr = group_dict[g].split(",")
            for a in arr:
                if a == "":
                    continue

                for g2 in sorted(group_dict.iterkeys()):
                    if g2 == g:
                        continue
                    arr2 = group_dict[g2].split(",")
                    if a in arr2:

                        #print "found %s from group %s in group %s" % (a, g, g2)
                        mkey = "%s|%s" % (g, g2)
                        mkey2 = "%s|%s" % (g2, g)
                        if mkey2 in merge_dict:
                            pass
                        else:
                            merge_dict[mkey] = 1


        for m in merge_dict:
            arr = m.split("|")
            group_dict[arr[0]] += group_dict[arr[1]]
            #del group_dict[arr[1]]
            group_dict[arr[1]] = ""


        # just for debugging output
        for g in sorted(group_dict.iterkeys()):
            arr = group_dict[g].split(",")
            arr_set = set(arr)
            arr_list = list(arr_set)
            arr_list.sort()
            #print g, arr_list
            log.info("- %s: %s", g, arr_list)
            #BACZC BACZY 98.7 1


        lib_group = {}
        for g in group_dict:
            arr = group_dict[g].split(",")
            for a in arr:
                if a != "":
                    lib_group[a] = g

        # create a graph of the ones in the same group (for debugging)
        png_file = dot_file.replace(".gv", ".png")
        cmd = "#graphviz;dot -T png %s > %s" % (dot_file, png_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        append_rqc_file(rqc_file_log, "16s_similarity_%s_png" % mode, png_file)

        heatmap = os.path.join(output_path, "heatmap.txt") # original heatmap, use this to create new order
        heatmap_new = heatmap.replace(".txt", "-new.txt") # temp file name, need to resort by group
        fh = open(heatmap, "r")
        fhw = open(heatmap_new, "w")
        fhw.write("#well id|organism name|library|group\n")

        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue
            # well|org name|lib|group
            arr = line.split("|")
            lib = str(arr[2]).strip()
            if lib in lib_group:
                group_id = lib_group[lib]
            else:
                group_cnt += 1
                group_id = "group-%s" % group_cnt

            fhw.write("%s|%s|%s|%s\n" % (arr[0], arr[1], lib, group_id))

        fh.close()
        fhw.close()


        heatmap2 = heatmap.replace(".txt", "-16s-%s.txt" % mode) # temp name - depends on heatmap codes and how they will use it
        cmd = "(head -1 %s && tail -n +2 %s | sort -k4 -V -t '|') > %s" % (heatmap_new, heatmap_new, heatmap2)
        # -V = version string sorting, -k4 = kolumn 4
        std_out, std_err, exit_code = run_cmd(cmd, log)

        cmd = "rm %s" % heatmap_new
        std_out, std_err, exit_code = run_cmd(cmd, log)

        heatmap_group = heatmap2.replace("heatmap-", "heatmap-group-")
        create_grouping_file(heatmap2, heatmap_group)


    else:
        log.error("- cannot find file: %s  %s", input_file, msg_fail)



'''
write a report with the bbstats for each reference in the path
- need _clean, dirty pattern for both SL & BB

'''
def run_bbstats(my_path, pattern):

    log.info("run_bbstats")

    if not pattern:
        pattern = "*.fasta"


    report_file = os.path.join(my_path, "report.txt")
    if os.path.isfile(report_file):
        log.info("- skipping step, found file: %s", report_file)
        return


    ref_list = glob.glob(os.path.join(my_path, pattern))

    # get the well name
    heatmap_dict = {}
    heatmap_file = os.path.join(output_path, "heatmap.txt")
    fh = open(heatmap_file, "r")
    for line in fh:
        if line.startswith("#"):
            continue
        arr = line.split("|")
        # well, ncbi name, library name
        heatmap_dict[arr[2]] = arr[0]

    fh.close()


    fh = open(report_file, "w")

    my_header = ""
    my_buffer = ""

    ref_cnt = 0
    for ref in ref_list:

        ref_cnt += 1

        # format=3 = row format
        cmd = "#bbtools;stats.sh format=3 in=%s" % ref

        lib_name = os.path.basename(ref).split("-")[0].replace("_clean","").replace(".fasta", "")
        std_out, std_err, exit_code = run_cmd(cmd, log)

        #print std_out
        arr = std_out.split("\n")

        if ref_cnt == 1:
            my_header = "Library   Well   %s" % (arr[0])

        well_id = "?"
        if lib_name in heatmap_dict:
            well_id = heatmap_dict[lib_name]

        my_buffer += "%s   %s   %s\n" % (lib_name, well_id, arr[1])

    fh.write(my_header + "\n")
    fh.write(my_buffer + "\n")
    fh.close()


    log.info("- wrote %s, %s references", report_file, ref_cnt)


    return report_file



"""
bbtools decontamination: only 1 command (CrossBlock)
module load bbtools; decontaminate.sh readnamefile=readlist.txt refnamefile=reflist.txt out=decon/ pigz unpigz passes=1 mapraw log=dclog.txt cecc=t prefilter=t prebits=2
- takes 4-6 hours
readlist.txt is a list of read file locations, one per line; and reflist.txt is a list of assembly locations, also one per line.  Each file must have a unique filename (regardless of path), so I first make symlinks to them,resulting in list files that look like this:

readlist.txt:
XQEA.fq.gz
XQEB.fq.gz
XQEC.fq.gz
...etc

reflist.txt:
XQEA.fasta
XQEB.fasta
XQEC.fasta
...etc

* crossblock k=62 is good for not removing too much of same-taxonomy cross contamination

"""
def run_bbdecontam():
    log.info("run_bbdecontam")

    bb_path = get_the_path(BB_DECONTAM_PATH, output_path)

    results_file = os.path.join(bb_path, "results.txt")
    if os.path.isfile(results_file):
        log.info("- skipping step, found %s", results_file)
        return 0


    raw_fastq = os.path.join(output_path, "input_fastqs.txt")
    ref_file = os.path.join(output_path, "references.txt")
    crossblock_log = os.path.join(bb_path, "crossblock.log")


    # params per BB, 2015-02-10
    #cmd = "module load bbtools;decontaminate.sh readnamefile=%s refnamefile=%s out=%s pigz unpigz passes=1 mapraw log=%s/dclog.txt cecc=t prefilter=t prebits=2"
    # params per BB, 2016-07-07

    os.chdir(bb_path) # change to bbpath to run because it tries to write temp files to ~brycef otherwise using my quota up!
    cmd = "#bbtools;decontaminate.sh readnamefile=%s refnamefile=%s out=%s log=%s/dclog.txt %s > %s 2>&1"
    cmd = cmd % (raw_fastq, ref_file, bb_path, bb_path, config['crossblock_params'], crossblock_log)
    append_cmd_log(cmd)
    std_out, std_err, exit_code = run_cmd(cmd, log)


    if exit_code > 0:
        log.error("- fatal crossblock error")
        status = "failed"
        checkpoint_step(status_log, status)
        sys.exit(exit_code)

    # remove *_demuxed.fq.gz
    cmd = "cd %s;rm *.fq.gz" % (bb_path)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    return 0


'''
merge the cleaned contigs from bb into clean
$ filterbyname.sh ow=t in=../sl/TNHG.fasta-clean.fasta in=../bb/TNHG-7667.7.80862.AAAGCA_clean.fasta out=TNHG_clean.fasta
- create a script to run them serially (clean.sh)
- not needed?
'''
def merge_clean():
    log.info("merge_clean")

    clean_path = get_the_path(CLEAN_PATH, output_path)

    bb_path = os.path.join(output_path, BB_DECONTAM_PATH)
    input_path = os.path.join(output_path, INPUT_PATH)

    bb_list = glob.glob(os.path.join(bb_path, "*_clean.fasta")) # TNHG-7667.7.80862.AAAGCA_clean.fasta


    # get the cleaned bb files
    bb_dict = {} # lib = file
    for bb in bb_list:
        bb_lib = os.path.basename(bb).split("-")[0]
        bb_dict[bb_lib] = bb


    log.info("- copying bb cleaned fastas to clean folder")


    for lib in bb_dict:
        clean_fasta = os.path.join(clean_path, "%s_clean.fasta" % lib)

        cmd = "cp %s %s" % (bb_dict[lib], clean_fasta)
        std_out, std_err, exit_code = run_cmd(cmd, log)



    # save chaff (removed contigs) in *_chaff.fasta
    for lib in bb_dict:

        input_fasta = os.path.join(input_path, "%s.fasta" % lib)
        clean_fasta = os.path.join(clean_path, "%s_clean.fasta" % lib)
        chaff_fasta = os.path.join(clean_path, "%s_chaff.fasta" % lib)
        chaff_log = os.path.join(clean_path, "%s_chaff.log" % lib)

        cmd = "#bbtools;filterbyname.sh in=%s names=%s out=%s ow=t include=f > %s 2>&1"  % (input_fasta, clean_fasta, chaff_fasta, chaff_log)
        # ow=t - overwrite = true otherwise it crashes
        # include=f = add to chaff those contigs not in "names" file
        # names = names of contigs from cleaned fasta

        append_cmd_log(cmd)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        if exit_code > 0:
            status = "failed"
            checkpoint_step(status_log, status)

            sys.exit(exit_code)


    all_contigs = os.path.join(clean_path, "all_contigs.fasta")
    if os.path.isfile(all_contigs):
        cmd = "rm %s" % (all_contigs)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    cmd = "cat %s >> %s" % (os.path.join(clean_path, "*_clean.fasta"), all_contigs)
    std_out, std_err, exit_code = run_cmd(cmd, log)


    return 0

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## reporting functions

'''
create a text report showing the changes from input to cleaned
- creates ./merge_report.txt

'''
def make_stats_report():

    log.info("make_stats_report")

    report_file = os.path.join(output_path, "merge_report.txt")
    if os.path.isfile(report_file):
        log.info("- skipping step, found file: %s", report_file)
        return

    input_path = os.path.join(output_path, INPUT_PATH)
    bb_path = os.path.join(output_path, BB_DECONTAM_PATH)
    clean_path = os.path.join(output_path, CLEAN_PATH)

    my_json = {}
    total_key = "xxxttl"
    my_json[total_key] = {}

    report_dict = {
        "input" : input_path,
        "bb" : bb_path,
        "clean" : clean_path,
    }

    for r in ["input", "bb", "clean"]:

        # read each report.txt and merge the results into master report
        my_report = os.path.join(report_dict[r], "report.txt")

        contig_key = "%s_n_contigs" % r
        bp_key = "%s_contig_bp" % r

        if os.path.isfile(my_report):
            log.info("- processing: %s", my_report)

            fh = open(my_report, "r")
            for line in fh:

                line = line.strip()
                if line.startswith("Library"):
                    continue
                elif line.startswith("#"):
                    continue


                arr = line.split()

                lib = None
                if len(arr) > 5:

                    lib = arr[0]

                    if lib != "all_contigs":

                        if lib not in my_json:
                            my_json[lib] = {}

                        my_json[lib][contig_key] = arr[3]
                        my_json[lib][bp_key] = arr[5]
                        my_json[lib]['well_id'] = arr[1]

                        # save totals
                        if total_key not in my_json:
                            my_json[total_key] = { contig_key: 0, bp_key: 0 }

                        if contig_key in my_json[total_key]:
                            my_json[total_key][contig_key] += int(arr[2])
                        else:
                            my_json[total_key][contig_key] = int(arr[2])

                        if bp_key in my_json[total_key]:
                            my_json[total_key][bp_key] += int(arr[4])
                        else:
                            my_json[total_key][bp_key] = int(arr[4])


            fh.close()

        else:
            log.error("- missing file: %s", my_report)


    my_json[total_key]['well_id'] = "--"


    remove_pct_ttl = 0.0
    contig_diff_ttl = 0
    fh = open(report_file, "w")

    header = "Library,Well,Input Contigs,Input BP,BB Contigs,BB BP,Clean Contigs,Clean BP,Contigs Removed,BP Removed Ratio"

    fh.write(header + "\n")
    check_list = ['bb_n_contigs', 'bb_contig_bp']

    for lib in sorted(my_json):

        if lib.lower() == "fake":
            continue


        # difference in starting vs ending contigs

        # for FAKE libraries - skip it instead

        if 'clean_n_contigs' not in my_json[lib]:
            my_json[lib]['clean_n_contigs'] = my_json[lib]['input_n_contigs']

        if 'clean_contig_bp' not in my_json[lib]:
            my_json[lib]['clean_contig_bp'] = my_json[lib]['input_contig_bp']


        contig_diff = 0

        contig_diff = int(my_json[lib]['input_n_contigs']) - int(my_json[lib]['clean_n_contigs'])

        # pct of bases removed for contam
        remove_pct = 0.0
        if 'clean_contig_bp' in my_json[lib]:
            if int(my_json[lib]['input_contig_bp']) > 0:
                remove_pct = 1 - int(my_json[lib]['clean_contig_bp']) / float(my_json[lib]['input_contig_bp'])

        lib_name = lib


        # Treat total special
        if lib == total_key:
            lib_name = "Total"
            contig_diff_ttl = contig_diff
            remove_pct_ttl = remove_pct


        # zero out missing values
        for check in check_list:
            if not check in my_json[lib]:
                my_json[lib][check] = 0


        my_buffer = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (lib_name, my_json[lib]['well_id'],
                                                             my_json[lib]['input_n_contigs'], my_json[lib]['input_contig_bp'],
                                                             my_json[lib]['bb_n_contigs'], my_json[lib]['bb_contig_bp'],
                                                             my_json[lib]['clean_n_contigs'], my_json[lib]['clean_contig_bp'],
                                                             contig_diff, remove_pct)
        fh.write(my_buffer + "\n")
        log.info("- %s = %s removed", lib, "{:.2%}".format(remove_pct))

    fh.close()


    # save stats
    append_rqc_stats(rqc_stats_log, "total_input_contigs", my_json[total_key]['input_n_contigs'])
    append_rqc_stats(rqc_stats_log, "total_input_bp", my_json[total_key]['input_contig_bp'])
    append_rqc_stats(rqc_stats_log, "total_clean_contigs", my_json[total_key]['clean_n_contigs'])
    append_rqc_stats(rqc_stats_log, "total_clean_bp", my_json[total_key]['clean_contig_bp'])
    append_rqc_stats(rqc_stats_log, "total_contig_removed", contig_diff_ttl)
    append_rqc_stats(rqc_stats_log, "total_bp_removed_ratio", remove_pct_ttl)

    log.info("- created %s", report_file)


'''
Save stats for control chart reporting (RQCSUPPORT-829)
'''
def save_merge_report_stats():
    log.info("save_merge_report_stats")

    merge_report = os.path.join(output_path, "merge_report.txt")

    # field def, field number in arr
    merge_def = {
        "input_contigs" : 2,
        "input_bp" : 3,
        "crossblock_contigs" : 4,
        "crossblock_bp" : 5,
        "clean_contigs" : 8,
        "clean_bp" : 9,
        "contigs_removed" : 10,
        "removed_bp_pct" : 11,
    }

    fh = open(merge_report, "r")
    for line in fh:
        # skip header, keep "Total" line
        if line.startswith("Library"):
            continue


        arr = line.strip().split(",")
        if len(arr) >= 11:

            lib = arr[0]
            # for each field save the value ...
            for k in merge_def:
                my_key = "%s_%s" % (k, lib)
                my_val = arr[merge_def[k]]

                append_rqc_stats(rqc_stats_log, my_key, my_val)

            my_val = int(arr[3]) - int(arr[9])
            append_rqc_stats(rqc_stats_log, "bp_removed_%s" % lib, my_val)

    fh.close()


'''
Run blast on each *dirty.fasta where file size > 0 - in bb decontam
- parallelize each run?
'''
def blast_dirty(sub_path):
    log.info("blast_dirty")

    blast_path = os.path.join(output_path, sub_path)

    dirty_list = glob.glob(os.path.join(blast_path, "*_dirty.fasta"))

    status = "blast dirty contigs in progress"
    checkpoint_step(status_log, status)

    blast_cnt = 0 # number of dirty files to blast

    #nt_db = RQCReferenceDatabases.NT
    #blast_param = "'-perc_identity 90 -evalue 1e-30 -dust \"yes\" -num_threads 8'"

    blast_done = os.path.join(blast_path, "done.txt")

    if os.path.isfile(blast_done):
        log.info("- skipping blast, %s exists", blast_done)

    else:

        fh = open(blast_done, "w")

        for dirty_file in dirty_list:

            if os.path.getsize(dirty_file) > 0:
                blast_cnt += 1

                lib_name = os.path.basename(dirty_file).split("-")[0]
                lib_name = "%s-%s-dirty" % (lib_name, sub_path)
                log_file = os.path.join(blast_path, "%s.blast.log" % lib_name)

                blastplus_cmd = os.path.realpath(str(config['run_blast_cmd']).replace("[pipeline_path]", os.path.join(my_path, '../tools')))

                fh.write("# %s\n" % dirty_file)
                timestamp = time.strftime("%Y-%m-%d %H:%M:%S")
                fh.write("# %s\n" % timestamp)

                cmd = "%s -s -o %s -q %s -d nt > %s 2>&1" % (blastplus_cmd, blast_path, dirty_file, log_file)
                fh.write(cmd + "\n")
                append_cmd_log(cmd)
                std_out, std_err, exit_code = run_cmd(cmd, log)

                fh.write("# blast exit code: %s\n" % exit_code)

                fh.write("\n")

                #~/git/jgi-rqc-pipeline/tools/run_blastplus.py -o /global/projectb/scratch/brycef/sag/BAGHS/unscreen/megablast -q /global/projectb/scratch/brycef/sag/BAGHS/pool_asm/pool_decontam.fasta \
                #-d /scratch/rqc/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted

        fh.close()


    if os.path.isfile(blast_done):
        log.info("- processing blast files")

        ext_list = [".parsed", ".parsed.taxlist", ".parsed.tophit"]
        b_cnt = 0


        blast_list = glob.glob(os.path.join(blast_path, "*.vs.nt"))

        for blast_file in blast_list:

            blast_name = os.path.basename(blast_file)
            log.info("- %s", blast_name)

            if os.path.getsize(blast_file) > 0:
                append_rqc_file(rqc_file_log, blast_name, blast_file)


                b_cnt += 1


                for e in ext_list:
                    blast_file_new = blast_file + e
                    if os.path.isfile(blast_file_new):

                        hit_cnt = get_blast_hit_count(blast_file_new)
                        append_rqc_stats(rqc_stats_log, os.path.basename(blast_file_new) + "_cnt", hit_cnt)

                        append_rqc_file(rqc_file_log, os.path.basename(blast_file_new), blast_file_new)
                        log.info("- %s (%s hits)", os.path.basename(blast_file_new), hit_cnt)
                        #b_cnt += 1


                    else:
                        append_rqc_stats(rqc_stats_log, os.path.basename(blast_file_new) + "_cnt", 0)
                        log.info("- %s   MISSING", os.path.basename(blast_file_new))
            else:
                log.info("- no blast hits for: %s", blast_name)


        append_rqc_stats(rqc_stats_log, "blast_%s_cnt" % sub_path, b_cnt)

    status = "blast dirty contigs complete"
    checkpoint_step(status_log, status)


'''
For each reference, make a list of contigs from the original that are not in the new file
- writes to ./clean/contigs_removed.txt
'''
def make_contig_remove_report():
    log.info("make_contig_remove_report")


    clean_path = os.path.join(output_path, CLEAN_PATH)

    ref_list = []

    fh = open(os.path.join(output_path, "references.txt"), "r")

    for line in fh:
        line = line.strip()
        if line.startswith("#"):
            continue

        ref_list.append(line)
    fh.close()


    # store headers for each removed contig in one file
    ttl_rmv = 0
    rmv_file = os.path.join(clean_path, "contigs_removed.txt")

    fh = open(rmv_file, "w")

    for ref in ref_list:
        scaffold_dict = {} # list of scaffolds in dirty file

        #print ref
        fh_ref = open(ref, "r")
        # >ATSAN_NODE_14_length_5532_cov_3.59211_ID_27
        for line in fh_ref:
            line = line.strip()
            if line.startswith(">"):
                scaffold_dict[line] = 0
        fh_ref.close()


        clean_ref = ref.replace("/input/", "/clean/").replace(".fasta", "_clean.fasta")

        if os.path.isfile(clean_ref):
            fh_clean = open(clean_ref, "r")
            for line in fh_clean:
                line = line.strip()
                if line.startswith(">"):
                    if line in scaffold_dict:
                        scaffold_dict[line] += 1
            fh_clean.close()


            rmv_cnt = 0


            for scaffold in scaffold_dict:
                if scaffold_dict[scaffold] > 0:
                    pass
                else:
                    fh.write(scaffold+"\n")
                    rmv_cnt += 1


            log.info("- %s: %s scaffolds removed", os.path.basename(ref), rmv_cnt)

            ttl_rmv += rmv_cnt

        else:
            log.info("- cannot find clean ref: %s", clean_ref)

    fh.close()

    append_rqc_file(rqc_file_log, "contigs_removed", rmv_file)
    log.info("- removed %s scaffolds total", ttl_rmv)


'''
Create contam input file (contam.txt)
- used for do_asm_contam, do_read_contam
- assume fasta files are prefixed with library names
# Library | Assembly File | Fastq File

return contam.txt file
'''
def make_contam_input_file(sub_path, fasta_pattern, fastq_pattern):
    log.info("make_contam_input_file: %s, %s, %s", sub_path, fasta_pattern, fastq_pattern)


    the_path = os.path.join(output_path, sub_path)

    fasta_list = glob.glob(os.path.join(output_path, fasta_pattern))
    fasta_list.append(os.path.join(output_path, sub_path, "all_contigs.fasta"))

    contam_file = os.path.join(the_path, "contam.txt")
    fh = open(contam_file, "w")
    fh.write("# Library| Assembly File | Fastq File\n")

    for my_fasta in fasta_list:

        lib = os.path.basename(my_fasta).split(".")[0]
        if "clean" in fasta_pattern:
            lib = lib.replace("_clean", "")

        fq_pattern = fastq_pattern.replace("[lib]", lib)
        my_fastq_list = glob.glob(os.path.join(output_path, fq_pattern))

        my_fastq = os.path.dirname(my_fasta)
        if len(my_fastq_list) > 0:
            my_fastq = my_fastq_list[0]

        if lib == "all_contigs":
            lib = "ALL"
        if lib == "FAKE":
            lib = "#FAKE" # don't include

        log.info("-- %s: %s, %s", lib, os.path.basename(my_fasta), os.path.basename(my_fastq))

        fh.write("%s|%s|%s\n" % (lib, my_fasta, my_fastq))
    fh.close()

    log.info("- created: %s", contam_file)

    return contam_file


'''
Assembly vs assembly contamination ~ 5min
- concats all assemblies into all_contigs.fasta
- shred each assembly into 2000bp pieces
- for each shreded assembly, blast against all_contigs.fasta
- report on percent of hits
(green background)

'''
def do_asm_contam(sub_path, contam_file):
    log.info("do_asm_contam: %s, %s", sub_path, contam_file)

    the_path = get_the_path(os.path.join(sub_path, ASM_CONTAM_PATH), output_path)
    asm_contam = os.path.join(the_path, "asm_contam.txt")

    if os.path.isfile(asm_contam):
        log.info("- skipping asm_contam, found file: %s  %s", asm_contam, msg_warn)

    else:
        asm_contam_cmd = os.path.realpath(str(config['asm_contam_cmd']).replace("[pipeline_path]", my_path))
        asm_contam_log = os.path.join(the_path, "asm_contam2.log")
        cmd = "%s -i %s -o %s > %s 2>&1" % (asm_contam_cmd, contam_file, the_path, asm_contam_log)
        append_cmd_log(cmd)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    if not os.path.isfile(asm_contam):
        log.error("- asm_contam failed!  %s", msg_fail)
        sys.exit(2)

    return asm_contam


'''
Reads vs assembly contamination ~ 30 min
- uses seal to compare kmers in the references (assemblies) to each fastq
- report on parts-per-million (ppm) for each hit, max 9999
(yellow background)
'''
def do_read_contam(sub_path, contam_file):
    log.info("do_read_contam: %s, %s", sub_path, contam_file)

    the_path = get_the_path(os.path.join(sub_path, READ_CONTAM_PATH), output_path)
    read_contam = os.path.join(the_path, "read_contam.txt")

    if os.path.isfile(read_contam):
        log.info("- skipping read_contam, found file: %s  %s", read_contam, msg_warn)
    else:

        read_contam_cmd = os.path.realpath(str(config['read_contam_cmd']).replace("[pipeline_path]", my_path))
        read_contam_log = os.path.join(the_path, "read_contam2.log")
        cmd = "%s -i %s -o %s > %s 2>&1" % (read_contam_cmd, contam_file, the_path, read_contam_log)
        append_cmd_log(cmd)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    if not os.path.isfile(read_contam):
        log.error("- read_contam failed!  %s", msg_fail)
        sys.exit(2)


    return read_contam


'''
Take the read_contam.txt or asm_contam.txt and order it according to the mode
- output = group file and txt file for heatmap.py

create_heatmap_files("org", "input", read_contam_file, "read_level_cross_contam_raw_contigs.pdf", "read")
- converts output_file from *.pdf to *.txt for heatmap_file for create_heatmap

col_id = column number to use from input_file.  asm_contam = 4, read_contam = 2

'''
def create_heatmap_files(mode, sub_path, input_file, col_id, output_file):
    log.info("create_heatmap_files: %s, %s, %s, %s", mode, sub_path, input_file, output_file)

    # default group by "org" (organism)
    # files in the "output_path" folder
    order_file = "heatmap.txt"
    group_file = "heatmap-group.txt"
    if mode == "vsearch":
        order_file = "heatmap-16s-vsearch.txt"
        group_file = "heatmap-group-16s-vsearch.txt"
    elif mode == "mothur":
        order_file = "heatmap-16s-mothur.txt"
        group_file = "heatmap-group-16s-mothur.txt"


    xaxis = ""
    yaxis = ""
    asm_desc = "Raw"
    if sub_path == "clean":
        asm_desc = "Cleaned"
    if output_file.startswith("contig_level"):
        xaxis = "%s Shredded Assembly" % (asm_desc)
        yaxis = "%s Assembly" % (asm_desc)
    if output_file.startswith("read_level"):
        xaxis = "Input Fastq"
        yaxis = "%s Assembly" % (asm_desc)
    if mode == "vsearch":
        xaxis += ", grouped by 16s Vsearch"
    elif mode == "mothur":
        xaxis += ", grouped by 16s Mothur"

    order_file = os.path.join(output_path, order_file)
    group_file = os.path.join(output_path, group_file)

    the_path = get_the_path(sub_path, output_path)

    if output_file.startswith("/"):
        pass
    else:
        output_file = os.path.join(the_path, output_file)

    if os.path.isfile(order_file):

        lib_dict = {} # info for the library
        lib_order = [] # order for rows and columns

        # get info for libraries
        fh = open(order_file, "r")
        # well | organism | library | group
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue
            arr = line.split("|")

            if len(arr) < 3:
                continue

            well_id = str(arr[0]).strip()
            organism = str(arr[1]).strip()
            organism = re.sub(r'[^A-Za-z0-9]+', '_', organism)
            library = str(arr[2]).strip()
            #print well_id, organism, library
            group = ""
            if len(arr) > 3:
                group = str(arr[3]).strip()
            col_title = "%s_%s" % (library, organism)
            lib_dict[library] = { "well" : well_id, "org" : organism, "group" : group, "column" : col_title }
            lib_order.append(library)
        fh.close()

        # get data for matrix
        matrix_data = {} # lib_from:lib_to = val
        fh = open(input_file, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue

            arr = line.split("|")
            lib_from = arr[0]
            lib_to = arr[1]
            val = arr[col_id]
            my_key = "%s:%s" % (lib_from, lib_to)

            matrix_data[my_key] = val # should be formatted by asm_contam or read_contam

        fh.close()



        # create input matrix for heatmap
        heatmap_file = output_file.replace(".pdf", ".txt")
        fhw = open(heatmap_file, "w")
        my_list = []
        for libc in lib_order:
            col_title = libc
            if libc in lib_dict:
                col_title = lib_dict[libc]['column']

            my_list.append(col_title)

        my_buffer = "|".join(my_list)
        #my_buffer = "|" + my_buffer # leading | to header
        fhw.write(my_buffer + "\n")


        for libc in lib_order:
            my_list = []
            my_list.append(libc)
            for libr in lib_order:

                val = "-"
                my_key = "%s:%s" % (libr, libc) # Bryce's way
                #my_key = "%s:%s" % (libc, libr) # Kecia thinks this is correct (RQC-1092)??, need to change x,y axis for plotting
                val = matrix_data.get(my_key, "-")

                my_list.append(val)
            my_buffer = "|".join(my_list)
            fhw.write(my_buffer + "\n")
            #print my_buffer
        fhw.close()
        log.info("- created file: %s", heatmap_file)



        create_heatmap(sub_path, heatmap_file, group_file, xaxis, yaxis, output_file)



'''
Recreate pdf and png heatmaps

~/git/jgi-rqc-pipeline/sag_decontam/heatmap-b.py \
-ig -1 -pl -png -sep psv -int -min 0 -max 9999 -cl YlOrRd \
-i /global/projectb/scratch/brycef/sag/tangy/input/read_level_cross_contam_raw_contigs.txt \
-hf /global/projectb/scratch/brycef/sag/tangy/input/read_level_cross_contam_raw_contigs.pdf \
-gr /global/projectb/scratch/brycef/sag/tangy/heatmap-group.txt \
-o /global/projectb/scratch/brycef/sag/tangy/

- creates a pdf and png and saves to rqc-files.txt

keys:
contig_level_contamination_raw_contigs_pdf
contig_level_contamination_raw-16s-vsearch_pdf
contig_level_contamination_raw-16s-mothur_pdf

read_level_cross_contam_raw_contigs_pdf
read_level_contamination_raw-16s-vsearch_pdf
read_level_contamination_raw-16s-mothur_pdf

* read_level = ppm/yellow
* contig_level = pct/green

'''
def create_heatmap(sub_path, heatmap_file, heatmap_group, xaxis, yaxis, output_heatmap):
    log.info("create_heatmap: %s, %s, %s, %s", sub_path, heatmap_file, heatmap_group, output_heatmap)
    log.info("- x-axis: %s, y-axis: %s", xaxis, yaxis)

    the_path = get_the_path(sub_path, output_path)

    # contig level = pct/green
    heatmap_params = config['heatmap_asm_contam_params'] # -ig -1 -png -sep psv -cl trafficlight
    if os.path.basename(output_heatmap).startswith("read"):
        # read level = ppm/yellow
        heatmap_params = config['heatmap_read_contam_params'] # -ig -1 -png -sep psv -int -min 0 -max 9999 -cl YlOrRd

    if os.path.isfile(output_heatmap) and 1==2:
        # always create a heatmap
        log.info("- skipping heatmap creation, found file: %s  %s", heatmap_file, msg_warn)

    else:

        heatmap_cmd = os.path.realpath(str(config['heatmap_cmd']).replace("[pipeline_path]", my_path))
        heatmap_log = os.path.join(the_path, "heatmap2.log")



        cmd = "%s %s -i %s -gr %s -hf %s -o %s -x '%s' -y '%s' > %s 2>&1" % (heatmap_cmd, heatmap_params, heatmap_file, heatmap_group, output_heatmap, the_path, xaxis, yaxis, heatmap_log)
        append_cmd_log(cmd)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(output_heatmap):
        log.info("- created: %s  %s", output_heatmap, msg_ok)

        # save heatmaps
        my_key = os.path.basename(output_heatmap)
        my_key = my_key.replace(".pdf", "_pdf")
        append_rqc_file(rqc_file_log, my_key, output_heatmap)

        png_file = output_heatmap.replace(".pdf", ".png")
        if os.path.isfile(png_file):
            my_key = my_key.replace("_pdf", "_png")
            append_rqc_file(rqc_file_log, my_key, png_file)

    else:
        log.info("- failed to create heatmap: %s  %s", output_heatmap, msg_fail)
        sys.exit(2)


'''
Run seal:
seal.sh in=fastq ref=all_contigs.fasta stats=fq-seal-lib.txt mkf=0.75 cz=100 ambig=all ss=f ow=t

dict of all contigs in all_contigs.fa:
- [contig] = list o' libs that it matches where seal base count > 10kbp

all_contigs_sum.txt:BAGOX_NODE_180_length_2646_cov_106.036_ID_17070|% bases|BAGOH|16s similarity mothur|16s similarity vsearch
all_contigs_sum.txt:BAGOX_NODE_180_length_2646_cov_106.036_ID_17070|% bases|BAGOU
all_contigs_sum.txt:BAGOX_NODE_180_length_2646_cov_106.036_ID_17070|% bases|BAGOW

File that shows for each assembly:
Contig,library with reads mapping to it/16s similarity (vsearch)
Contig-1,BAGOW/91.7% BAGOT/55.5%

pass this file onto sag.py for pooled assembly step
- convert report to json? no - currently it works as a report and it keeps the contig order
- keep format in sync with sag.py

'''
def read_ppm_report():

    log.info("read_ppm_report")

    status = "read ppm report in progress"
    checkpoint_step(status_log, status)

    the_path = os.path.join(output_path, READ_PPM_PATH)
    if not os.path.isdir(the_path):
        os.makedirs(the_path)

    # get all libraries, setup seal to run - 20 minutes for 46 libs


    lib_list = []
    input_fq_file = os.path.join(output_path, "input_fastqs.txt")
    all_fasta = os.path.join(output_path, "input", "all_contigs.fasta") # all contigs from input combined into one

    fh = open(input_fq_file, "r")
    for line in fh:
        lib = os.path.basename(line)[0:5]
        lib_list.append(lib)
        stats_file = os.path.join(the_path, "fq-%s-seal.txt" % lib)
        log_file = stats_file.replace(".txt", ".log")


        if os.path.isfile(stats_file):
            log.info("- skipping seal for %s", lib)

        else:
            cmd = "#bbtools;seal.sh in=%s ref=%s stats=%s %s > %s 2>&1" % (line.strip(), all_fasta, stats_file, config['read_seal_ppm_params'], log_file)
            std_out, std_err, exit_code = run_cmd(cmd, log)


    fh.close()



    # get vsearch similarities
    vsim_dict = {}
    # get vsearch similarity
    vsearch_sim = os.path.join(output_path, "16s_vsearch", "vsearch.16s.out")
    fh = open(vsearch_sim, "r")
    for line in fh:
        if line.startswith("#"):
            continue
        arr = line.strip().split()
        lib1 = str(arr[0]).split("_")[0]
        lib2 = str(arr[1]).split("_")[0]
        sim = arr[4] # similarity score
        my_key = "%s|%s" % (lib1, lib2)
        vsim_dict[my_key] = "%s%%" % sim
        my_key = "%s|%s" % (lib2, lib1)
        vsim_dict[my_key] = "%s%%" % sim
    fh.close()

    # get mothur similarities
    msim_dict = {}
    mothur_sim = os.path.join(output_path, "16s_mothur", "ssu_output", "ssu_output.bacteria.sim")
    fh = open(mothur_sim, "r")
    for line in fh:
        if line.startswith("#"):
            continue
        arr = line.strip().split()
        lib1 = str(arr[0]).split("_")[0]
        lib2 = str(arr[1]).split("_")[0]
        sim = arr[3] # similarity score
        my_key = "%s|%s" % (lib1, lib2)

        msim_dict[my_key] = "{:.1%}".format(float(sim)/100.0)
        my_key = "%s|%s" % (lib2, lib1)
        msim_dict[my_key] = "{:.1%}".format(float(sim)/100.0)
    fh.close()


    contig_dict = {}
    all_contigs_file = os.path.join(the_path, "all_contigs_list.txt")
    fhw = open(all_contigs_file, "w")
    fhw.write("#Contig Name|Matching Fastq Library|Fastq Bases|Fastq Bases Percent|Vsearch 16s Similarity|Mothur 16s Similarity\n")
    for lib in lib_list:

        seal_file = os.path.join(the_path, "fq-%s-seal.txt" % lib)

        if os.path.isfile(seal_file):

            fh = open(seal_file, "r")
            log.info("- scanning %s", seal_file)
            for line in fh:

                # seal comments
                if line.startswith("#"):
                    continue
                # skip if we matched our own library
                if line.startswith(lib):
                    continue

                arr = line.split()
                #print arr
                # only include those with 10kbp or more - coverage could cause this to be very high

                sk = "%s|%s" % (lib, arr[0][0:5]) # similarity key
                v = "0.0%"
                m = "0.0%"
                if sk in vsim_dict:
                    v = vsim_dict[sk]
                if sk in msim_dict:
                    m = msim_dict[sk]


                # 16s-v = vsearch 16s similarity, 16s-m = mothur 16s similarity
                log.info("--- contig %s: %s, %s bp (%s), 16s-v: %s, 16s-m: %s", arr[0], lib, arr[3], arr[4], v, m)
                out = "%s|%s|%s|%s|%s|%s|%s|%s\n" % (arr[0], lib, arr[1], arr[2], arr[3], arr[4], v, m)
                fhw.write(out)


                if int(arr[3]) > 0:  # filter this in sag.py - make_contig_report

                    # arr4 = pct of fq bases hitting that contig, arr3 = bases that hit contig

                    # format: contaminating lib, vsearch sim, mothur sim, reads, reads %, bp, bp %
                    b = "%s/%s/%s/%s/%s/%s/%s" % (lib, v, m, arr[1], arr[2], arr[3], arr[4]) # only library & vsearch similarity

                    key = arr[0] # contig name w/o leading >
                    if key in contig_dict:
                        contig_dict[key]['contam'] += ",%s" % b
                    else:
                        contig_dict[key] = {'contam' : b }

            fh.close()
    fhw.close()

    # create contigs-%s.txt which shows the scaffolds with the contaminating libraries
    # - read all headers in fa, look up in contig_dict and report for contig_dict
    for lib in lib_list:
        fa = os.path.join(output_path, "input", "%s.fasta" % lib)
        fa_o = os.path.join(the_path, "contigs-%s.txt" % lib)
        log.info("- creating file: %s", fa_o)
        fhw = open(fa_o, "w")

        fhw.write("#Contig|Contaminating Libraries/16s Sim Vsearch/16s Sim Mothur/BP Match from FQ/Pct of Bases from FQ\n")
        fh = open(fa, "r")
        for line in fh:
            line = line.strip()
            if line.startswith(">"):
                line = line[1:] # strip > from header header
                contam_libs = ""
                if line in contig_dict:
                    contam_libs = contig_dict[line]['contam'] #.replace(",", ", ")

                fhw.write("%s|%s\n" % (line, contam_libs))

        fh.close()
        fhw.close()

    status = "read ppm report complete"
    checkpoint_step(status_log, status)



'''
Add files to the files and stats logs
** to edit
'''
def save_stats():
    log.info("save_stats")

    input_path = os.path.join(output_path, INPUT_PATH)
    bb_path = os.path.join(output_path, BB_DECONTAM_PATH)
    clean_path = os.path.join(output_path, CLEAN_PATH)

    report_dict = {
        "input" : { 'path' : input_path, 'file_type' : "raw_contigs" },
        "bb" : { 'path' : bb_path, 'file_type' : "bbdecontam" },
        "clean" : { 'path' : clean_path, 'file_type' : "cleaned_contigs" },
    }


    for r in ["input", "bb", "clean"]:

        if not os.path.isdir(report_dict[r]['path']):
            continue

        """
        # taxmap = contig level contam - green
        taxmap_file = os.path.join(report_dict[r]['path'], "taxmap_pct_10.pdf")

        #taxmap_name = "%s_taxmap_pct_10" % r
        taxmap_name = "contig_level_contamination_%s" % report_dict[r]['file_type']

        # contig level = green = shread and blast assemblies to each other
        new_taxmap_file = os.path.join(report_dict[r]['path'], "contig_level_contamination_%s.pdf" % (report_dict[r]['file_type']))
        if os.path.isfile(taxmap_file):
            cmd = "cp %s %s" % (taxmap_file, new_taxmap_file)
            std_out, std_err, exit_code = run_cmd(cmd, log)


        if os.path.isfile(new_taxmap_file):
            append_rqc_file(rqc_file_log, taxmap_name, new_taxmap_file)
        """

        # stats.sh for each *clean.fasta
        fasta_report = os.path.join(report_dict[r]['path'], "report.txt")
        new_fasta_report = os.path.join(report_dict[r]['path'], "%s_report.txt" % (report_dict[r]['file_type']))
        cmd = "cp %s %s" % (fasta_report, new_fasta_report)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        fasta_report_name = "ref_stats_%s" % report_dict[r]['file_type']

        if os.path.isfile(fasta_report):
            append_rqc_file(rqc_file_log, fasta_report_name, new_fasta_report)

        """
        # ppm = yellow -> use seal to map contigs to input fastqs
        ppm_file = os.path.join(report_dict[r]['path'], "heatmap.pdf")
        new_ppm_file = os.path.join(report_dict[r]['path'], "read_level_cross_contam_%s.pdf" % report_dict[r]['file_type'])
        if os.path.isfile(ppm_file):
            cmd = "cp %s %s" % (ppm_file, new_ppm_file)
            std_out, std_err, exit_code = run_cmd(cmd, log)


        ppm_name = "read_level_cross_contam_%s" % report_dict[r]['file_type'] # report_dict[r]['file_type']

        if os.path.isfile(new_ppm_file):
            append_rqc_file(rqc_file_log, ppm_name, new_ppm_file)
        """


    merge_report = os.path.join(output_path, "merge_report.txt")
    if os.path.isfile(merge_report):
        append_rqc_file(rqc_file_log, "merge_report", merge_report)

    heatmap = os.path.join(output_path, "heatmap.txt")
    if os.path.isfile(heatmap):
        append_rqc_file(rqc_file_log, "heatmap", heatmap)



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## look up functions

'''
returns all of the library names for a plate
#e.g. fred.SAGs.HotLakes.1
need plate_id now because they're running the same plate_name with new libraries ...
'''
def get_library_list_from_plate(plate_name, plate_id):

    log.info("get_library_list_from_plate: %s, plate_id: %s", plate_name, plate_id)

    library_list = []

    db = None
    if uname == "brycef":
        db = jgi_connect_db("draw")
    else:
        db = jgi_connect_db("rqc")

    #db = jgi_connect_db("rqc-dev")

    if not db:
        status = "failed"
        checkpoint_step(status_log, status)

        log.error("- no db connection")
        sys.exit(4)

    sth = db.cursor(MySQLdb.cursors.DictCursor)

    sql_val = None

    sql = "select library_name from library_info where"

    if plate_name and plate_id:
        sql += " plate_name = %s and plate_id = %s"
        sql_val = (plate_name, plate_id)
    elif plate_name:
        sql += " plate_name = %s"
        sql_val = plate_name
    elif plate_id:
        sql += " plate_id = %s"
        sql_val = plate_id
    else:
        log.error("- no plate_name or plate_id!")
        return library_list

    sth.execute(sql, (sql_val,))
    row_cnt = int(sth.rowcount)
    for _ in range(row_cnt):
        rs = sth.fetchone()
        library_list.append(rs['library_name'])

    log.info("- added %s libraries", len(library_list))

    db.close()

    return library_list


'''
Get the list of libraries based on the pooled seq unit name
- could link to library_info and verify the seq_prod_name = SAG but skipping for now
- different code decides to use the library or not based on if the library passed or failed SAG-1
- could also do a web service ...
'''
def get_library_list_from_pool(pool_name):
    log.info("get_library_list_from_pool: %s", pool_name)
    library_list = []

    db = None
    if uname == "brycef":
        db = jgi_connect_db("draw")
    else:
        db = jgi_connect_db("rqc")

    #db = jgi_connect_db("rqc-dev")

    if not db:
        status = "failed"
        checkpoint_step(status_log, status)


        log.error("- no db connection")
        sys.exit(4)

    sth = db.cursor(MySQLdb.cursors.DictCursor)

    run_id = 0
    lane_id = 0

    sql = "select run_id, run_section from seq_units where library_name = %s"
    sth.execute(sql, (pool_name,))
    rs = sth.fetchone()
    if rs:
        run_id = rs['run_id']
        lane_id = rs['run_section']

    sql = "select library_name from seq_units where run_id = %s and run_section = %s and is_multiplex = 0"

    sth.execute(sql, (run_id, lane_id))
    row_cnt = int(sth.rowcount)
    for _ in range(row_cnt):
        rs = sth.fetchone()
        if rs['library_name'].lower() != "unknown":
            library_list.append(rs['library_name'])


    log.info("- added %s libraries", len(library_list))

    return library_list


# save commands run for Kecia
def append_cmd_log(cmd):

    fh = open(cmd_log, "a")
    fh.write(cmd + "\n")
    fh.close()



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "SAG Decontam"
    version = "1.7"


    DEFAULT_CONFIG_FILE = os.path.realpath(os.path.join(my_path, "../sag_decontam/", "sag_decontam.cfg"))
    INPUT_PATH = "input"
    CLEAN_PATH = "clean"
    TADPOLE_PATH = "tadpole"
    VSEARCH_PATH = "16s_vsearch"
    MOTHUR_PATH = "16s_mothur"
    BB_DECONTAM_PATH = "bb" # crossblock
    ASM_CONTAM_PATH = "asm_contam"
    READ_CONTAM_PATH = "read_contam"
    READ_PPM_PATH = "read_ppm"


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)
    uname = getpass.getuser() # get the user: brycef for testing

    # Parse options
    usage = "prog [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-l", "--lib", dest="lib_list", type=str, help="list of library names")
    parser.add_argument("-pt", "--plate", dest="plate_name", type=str, help="plate name")
    parser.add_argument("-ptid", "--plate-id", dest="plate_id", type=str, help="plate id")
    parser.add_argument("-p", "--pool", dest="pool_name", type=str, help="Pooled library name")
    parser.add_argument("-t", "--tadpole", dest="tadpole", default=False, action="store_true", help="Run tadpole to assemble fastqs (norm, subsample, tadpole, trim)")
    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-r", "--ref-file", dest="ref_file", help = "File containing library=path/to/reference (optional, if run folder is missing)")
    parser.add_argument("-fq", "--fastq-file", dest="fq_file", help = "File containing library=path/to/fastq (optional, used for testing)")
    parser.add_argument("-c", "--config", dest="config_file", help = "Location of sag_decontam.cfg file to use")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)


    lib_list = []
    pool_name = None
    plate_name = None
    plate_id = None
    output_path = None
    ref_file = None # file with library=path/to/scaffolds.2k.fasta - for when the run folder is gone ... only needs to contain those references that we can't normally find
    fq_file = None # file with library=path/to/fastq - for mock community
    tadpole_flag = False
    mode = "sag-asm" # default to new sag pipeline




    # parse args
    args = parser.parse_args()

    print_log = args.print_log

    if args.tadpole:
        tadpole_flag = args.tadpole

    if args.output_path:
        output_path = args.output_path


    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/sag"
        test_name = output_path
        if output_path == "t1":

            # new test
            #args.plate_id = "27-159439-67" # Ridge.rhizoCC.SAGs.1
            #args.plate_name = "DeL.S164000.SYBR.01"
            #output_path = os.path.join(test_path, "t1")
            # new test 2017-12-15
            args.plate_name = "tangany_SAGs"
            test_name = "tangy" # mmmmm tangy

        elif output_path == "t2":
            args.pool_name = "BAGSY"
            output_path = os.path.join(test_path, "t2")

        elif output_path == "t3":
            args.plate_name = "Sczyrba.BiogasSAGs.2" # or .2
            output_path = os.path.join(test_path, "t3")

        elif output_path == "t4":
            args.plate_name = "stew.SAGs.oceanOMZ.1" # older plate
            output_path = os.path.join(test_path, "t4")


        elif output_path == "t5":
            args.plate_name = "Ridge.rhizoCC.SAGs.1"
            output_path = os.path.join(test_path, "t5")
            mode = "sag-pool"
            tadpole_flag = True
            #plate_name = "Mock.Community.6"
            #plate_id = "Mock"
            #output_path = "/global/projectb/scratch/brycef/sag/mock6/"
            #output_path = os.path.join(test_path, "mock6")
            #args.ref_file = os.path.join(output_path, "ref.txt")
            #args.fq_file = os.path.join(output_path, "fq.txt")

            #lib_list = ['AAAA','AAAB','AAAC'] # there are 25 actually

        output_path = os.path.join(test_path, test_name)


    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    # initialize my logger
    log_file = os.path.join(output_path, "sag_decontam.log")



    # log = logging object
    log_level = "INFO"
    log = get_logger("sag-d", log_file, log_level, print_log)


    log.info("%s", 80 * "~")
    log.info("Starting %s", script_name)

    log.info("")


    if args.pool_name:
        pool_name = args.pool_name
        lib_list = get_library_list_from_pool(pool_name)


    if args.plate_name or args.plate_id:
        plate_name = args.plate_name
        plate_id = args.plate_id
        lib_list = get_library_list_from_plate(plate_name, plate_id)
        #print lib_list, len(lib_list)
        #sys.exit(13)

    if args.lib_list:
        lib_list = tuple(re.split(r'[,;\s]+', args.lib_list))

    if args.ref_file:
        ref_file = args.ref_file
        if not os.path.isfile(ref_file):
            print "* Error: cannot find ref-file: %s" % (ref_file)
            sys.exit(8)

    if args.fq_file:
        fq_file = args.fq_file
        if not os.path.isfile(fq_file):
            print "* Error: cannot find fq-file: %s" % (fq_file)
            sys.exit(8)


    config_file = os.path.join(output_path, "sag_decontam.cfg")

    if args.config_file:
        config_file = args.config_file


    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "pool_name", pool_name)
    log.info("%25s      %s", "plate_name", plate_name)
    log.info("%25s      %s", "plate_id", plate_id)
    log.info("%25s      %s", "library_list", lib_list)
    log.info("%25s      %s", "ref_file", ref_file)
    log.info("%25s      %s", "fq_file", fq_file)
    log.info("%25s      %s", "tadpole_flag", tadpole_flag)
    log.info("%25s      %s", "config_file", config_file)

    log.info("")


    status_log = os.path.join(output_path, "status.log")
    rqc_file_log = os.path.join(output_path, "rqc-files.tmp")
    rqc_stats_log = os.path.join(output_path, "rqc-stats.tmp")
    cmd_log = os.path.join(output_path, "sag_decontam_commands.log")
    if os.path.isfile(cmd_log):
        fh = open(cmd_log, "a")
        fh.write("# New Run\n")
        fh.close()
    else:
        fh = open(cmd_log, "w")
        fh.write("# New run\n")
        fh.close()

    if len(lib_list) == 0:

        status = "failed"
        checkpoint_step(status_log, status)

        log.error("- no libraries to run, exiting")
        sys.exit(8)


    if not os.path.isfile(config_file):
        log.info("- cannot find config file: %s", config_file)
        log.info("- using default config file.")
        cmd = "cp %s %s" % (DEFAULT_CONFIG_FILE, config_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    # set config dict with values from sag_decontam.cfg
    config = load_config_file(config_file, log)


    get_bbtools_version(rqc_stats_log, output_path, log)


    # test area

    #save_stats()
    #make_contig_remove_report()
    #save_merge_report_stats()

    #create_16s_txt(path_16s, input_file, mode):
    #calc_16s_similarity_vsearch() # Alex's method
    #calc_16s_similarity_mothur() # Rex's method
    #read_ppm_report()
    #make_contam_input_file("input", "input/*.fasta", "input/[lib]-*.fastq.gz")
    #create_heatmap_files("org", "input", "/global/projectb/scratch/brycef/sag/tangy/input/read_contam/read_contam.txt", "/global/projectb/scratch/brycef/sag/tangy/input/read_level_cross_contam_raw_contigs.pdf", "read")

    #sys.exit(44)




    status = "link input files in progress"
    checkpoint_step(status_log, status)

    # get the raw fastq and assemblies
    link_input_files(lib_list, ref_file, fq_file, mode)

    status = "link input files complete"
    checkpoint_step(status_log, status)



    # run tadpole assemblies (norm, subsample, tadpole, trim) - untested
    if tadpole_flag:
        status = "tadpole in progress"
        checkpoint_step(status_log, status)

        run_tadpole()

        status = "tadpole complete"
        checkpoint_step(status_log, status)


    status = "16s similarity in progress"
    checkpoint_step(status_log, status)

    # to group by 16s similarity
    calc_16s_similarity_vsearch() # Alex's method
    calc_16s_similarity_mothur() # Rex's method

    status = "16s similarity complete"
    checkpoint_step(status_log, status)

    ## ------------------------------------------------------------[ Crossblock ]

    status = "crossblock in progress"
    checkpoint_step(status_log, status)

    # run bbtools command
    run_bbdecontam()

    status = "crossblock complete"
    checkpoint_step(status_log, status)

    ##-------------------------------------------------------------[ Reporting ]

    status = "decontam reporting in progress"
    checkpoint_step(status_log, status)


    # merge into cleaned (filterbyname.sh)
    merge_clean()

    # bb stats.sh on all of the fastas
    run_bbstats(os.path.join(output_path, INPUT_PATH), "*.fasta")
    run_bbstats(os.path.join(output_path, BB_DECONTAM_PATH), "*_clean.fasta")
    run_bbstats(os.path.join(output_path, CLEAN_PATH), "*_clean.fasta")

    # report on contig, bases difference between input, cleaned (merge_report)
    make_stats_report()

    # report on contigs removed from each reference
    make_contig_remove_report()
    save_merge_report_stats()

    status = "decontam reporting complete"
    checkpoint_step(status_log, status)

    ## -----------

    # blast results for dirty contigs

    blast_dirty("bb")


    ## -----------
    status = "contig cross contam raw in progress"
    checkpoint_step(status_log, status)

    contam_file = make_contam_input_file(INPUT_PATH, "input/*.fasta", "input/[lib]-*.fastq.gz")
    asm_contam_file = do_asm_contam(INPUT_PATH, contam_file)

    # org, vsearch, mothur
    create_heatmap_files("org", INPUT_PATH, asm_contam_file, 4, "contig_level_contamination_raw_contigs.pdf")
    create_heatmap_files("vsearch", INPUT_PATH, asm_contam_file, 4, "contig_level_contamination_raw-16s-vsearch.pdf")
    create_heatmap_files("mothur", INPUT_PATH, asm_contam_file, 4, "contig_level_contamination_raw-16s-mothur.pdf")

    status = "contig cross contam raw complete"
    checkpoint_step(status_log, status)

    status = "read level contam raw in progress"
    checkpoint_step(status_log, status)

    read_contam_file = do_read_contam(INPUT_PATH, contam_file)

    # org, vsearch, mothur
    create_heatmap_files("org", INPUT_PATH, read_contam_file, 2, "read_level_cross_contam_raw_contigs.pdf")
    create_heatmap_files("vsearch", INPUT_PATH, read_contam_file, 2, "read_level_cross_contamination_raw-16s-vsearch.pdf")
    create_heatmap_files("mothur", INPUT_PATH, read_contam_file, 2, "read_level_cross_contamination_raw-16s-mothur.pdf")

    status = "read level contam raw complete"
    checkpoint_step(status_log, status)


    ## -----------
    status = "contig cross contam clean in progress"
    checkpoint_step(status_log, status)

    contam_file = make_contam_input_file(CLEAN_PATH, "clean/*_clean.fasta", "input/[lib]-*.fastq.gz")
    asm_contam_file = do_asm_contam(CLEAN_PATH, contam_file)

    create_heatmap_files("org", CLEAN_PATH, asm_contam_file, 4, "contig_level_contamination_cleaned_contigs.pdf")
    create_heatmap_files("vsearch", CLEAN_PATH, asm_contam_file, 4, "contig_level_contamination_clean-16s-vsearch.pdf")
    create_heatmap_files("mothur", CLEAN_PATH, asm_contam_file, 4, "contig_level_contamination_clean-16s-mothur.pdf")

    status = "contig cross contam clean complete"
    checkpoint_step(status_log, status)


    status = "read level contam clean in progress"
    checkpoint_step(status_log, status)

    read_contam_file = do_read_contam(CLEAN_PATH, contam_file)

    # org, vsearch, mothur
    create_heatmap_files("org", CLEAN_PATH, read_contam_file, 2, "read_level_cross_contam_cleaned_contigs.pdf")
    create_heatmap_files("vsearch", CLEAN_PATH, read_contam_file, 2, "read_level_cross_contamination_clean-16s-vsearch.pdf")
    create_heatmap_files("mothur", CLEAN_PATH, read_contam_file, 2, "read_level_cross_contamination_clean-16s-mothur.pdf")

    status = "read level contam clean complete"
    checkpoint_step(status_log, status)


    # create read_ppm report to import into sag.py
    read_ppm_report()



    # save for rqc system to pick up
    save_stats()


    status = "complete"
    checkpoint_step(status_log, status)

    if status == "complete":

        append_rqc_file(rqc_file_log, "decontam_commands", cmd_log)

        # move rqc-files.tmp to rqc-files.txt
        rqc_new_file_log = os.path.join(output_path, "rqc-files.txt")

        cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        rqc_new_stats_log = os.path.join(output_path, "rqc-stats.txt")

        cmd = "mv %s %s" % (rqc_stats_log, rqc_new_stats_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    log.info("Completed %s", script_name)

    sys.exit(0)



"""

        YYY
      YYYYYYY
     YYYYYYYYY
    Y..YYY..YYY
    ....Y....YY     CHARACTER / NICKNAME
    ##..Y##..YY     - SHADOW   "BLINKY"
   Y##..Y##..YYY    - SPEEDY   "PINKY"
   YY..YYY..YYYY    - BASHFUL  "INKY"
   YYYYYYYYYYYYY    - POKEY    "CLIDE"
   YYYYYYYYYYYYY
   YYYYYYYYYYYYY
   YYYYYYYYYYYYY
   YYYYYYYYYYYYY
   YY YY   YY YY
   Y   Y   Y   Y

      1980 Midway


"""
