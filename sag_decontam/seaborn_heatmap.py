#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

Creates readable heatmap for contig level cross contam data which comes from Vasanth' heatmap code
- needs seaborn installed (nice plotting package that works with matplotlib)

taxmap from
$ ./seaborn_heatmap.py -i ~/git/jgi-rqc-pipeline/sag_decontam/taxmap_ct.csv -o ~/git/jgi-rqc-pipeline/sag_decontam/dirty_heatmap.png
~ 14 sec

group by file
$ ./seaborn_heatmap.py -i /global/projectb/scratch/brycef/sag/t1/input/taxmap_pct-16s-vsearch.csv -g /global/projectb/scratch/brycef/sag/t1/input/taxmap_group-16s-vsearch.txt -o /global/projectb/scratch/brycef/sag/t1/input/contig_level_contamination_raw-16s-vsearch-b.png

To do:
- some of the labels on the rows & columns are still cut off

versions
1.1: 2017-03-20
- added group by file for custom grouping

* 2018-01-04: code is deprecated

"""

import os
import sys
import numpy as np
import matplotlib
matplotlib.use('Agg') ## This needs to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
import seaborn as sns
from argparse import ArgumentParser

if __name__ == "__main__":

    label = None
    data2 = []
    
    version = "v 1.1"
    
    usage = "prog [options]\n"
    #usage += "* %s, version %s\n" % (my_name, version)


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-i", "--input-file", dest="input", type=str, help="input csv")
    parser.add_argument("-g", "--group-file", dest="group", type=str, help="input csv")
    parser.add_argument("-o", "--output-png", dest="output_png", type=str, help="output png")

    args = parser.parse_args()

    input_file = None
    group_file = None
    output_png = None
    
    
    if args.input:
        input_file = args.input

    if args.group:
        group_file = args.group

    if args.output_png:
        output_png = args.output_png

    if not input_file:
        print "Error: no input file!"
        sys.exit(2)

    if not os.path.isfile(input_file):
        print "Error: cannot find file %s" % input_file
        sys.exit(2)

    if not output_png:
        print "Error: no output file!"
        sys.exit(2)

    ## Just collect the labels
    with open(input_file, "r") as f:
        for l in f:
            label = l.strip().split('\t')
            break

    # print label

    s = 0
    sl = ""
    loc = []

    ## Collect the label name changing index
    for i in label:
        nl = ""
        try:
            nl = i.split('_')[1]
        except IndexError:
            nl = "None"

        if nl != sl:
            sl = nl
            loc.append(s)

        s += 1

    # print loc

    ## Load data
    ncols = len(label)
    loc.append(ncols)
    data = np.loadtxt(input_file, delimiter="\t", comments='#', skiprows=1, usecols=range(1, ncols+1), dtype=None)

    ## set appropriate font and dpi
    sns.set(font_scale=1.2)
    #sns.set_style({"savefig.dpi": 150})
    sns.set_style({"savefig.dpi": 200})

    ## plot it out
    ax = sns.heatmap(data, linewidths=.1, annot=True, annot_kws={"size":8}, fmt='g', cmap="RdYlGn_r", vmin=0, mask=data < 0, xticklabels=label, yticklabels=label)

    ## set the x-axis labels on the top
    ax.xaxis.tick_top()

    ## rotate the x/y-axis labels
    plt.xticks(rotation=90)
    plt.yticks(rotation=360)

    x1 = 0
    x2 = 0
    y1 = ncols
    y2 = ncols

    # does the boxes around each area - assumes its sorted by name
    if not group_file:

        for i in range(1, len(loc)):
            x1 = loc[i-1]
            x2 = loc[i]
            ax.plot([x1, x2], [y1, y2], 'k-', lw=2)
            ax.plot([x1, x2], [ncols-x2, ncols-x2], 'k-', lw=2)
            ax.plot([ncols-y1, ncols-y2], [ncols-x1, ncols-x2], 'k-', lw=2)
            ax.plot([x2, x2], [ncols-x1, ncols-x2], 'k-', lw=2)
            y1 = ncols - x2
            y2 = ncols - x2
    else:
        # custom grouping (Vsearch-16s, etc)
        fh  = open(group_file, "r")
        for line in fh:
            #print line
            arr = line.split(":") # 3:6::3:6 - always assume square shape so only look at 1st 2 values
            x1 = int(arr[0])-1 # starting group
            x2 = int(arr[1]) # ending group
            ax.plot([x1, x2], [y1, y2], 'k-', lw=2)
            ax.plot([x1, x2], [ncols-x2, ncols-x2], 'k-', lw=2)
            ax.plot([ncols-y1, ncols-y2], [ncols-x1, ncols-x2], 'k-', lw=2)
            ax.plot([x2, x2], [ncols-x1, ncols-x2], 'k-', lw=2)
            y1 = ncols - x2
            y2 = ncols - x2            
            
            
        fh.close()
        

    ## get figure (usually obtained via "fig,ax=plt.subplots()" with matplotlib)
    fig = ax.get_figure()


    ## specify dimensions and save
    fig.set_size_inches(30, 20)
    #fig.set_size_inches(40, 30)
    fig.savefig(output_png)

    sys.exit(0)
