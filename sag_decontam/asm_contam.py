#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Assembly Cross Contamination
- for each assembly from a plate, how much maps to other libraries?
- user provides input file with library|input file
- outputs assembly_contam.txt
- mean contam is not a good measure of contamination, it just is a measure of shared contigs, the assemblies might be from the same genus or species
replaces /usr/common/jgi/qaqc/jigsaw/2.6.5-prod_2.6.5_4-1-g1e9ef4b/bin/contamCheckByPlateId.py
- does produce different results, heatmap hits are same or different?

Dependencies:
bbtools (shred)
blast

For each assembly:
- combine everything into one big assembly(1)
- for each assembly, shred into 2kb pieces
- blast each assembly vs (1)
- output how much hits another library / total hits

Heatmap created with different code

v 1.0: 2017-12-20
- initial version

v 1.1: 2018-03-20
- fix calculation for hits, use # shreds

To do:
- try to use python threads

~~~~~~~~~~~~~~~~~~~[ Test Cases ]

tangy pool: 96 sec to run
~/git/jgi-rqc-pipeline/sag_decontam/asm_contam.py -o t1 -pl

~/git/jgi-rqc-pipeline/sag_decontam/asm_contam.py -pl \
-i /global/projectb/scratch/brycef/sag/tangy/input/contam.txt \
-o /global/projectb/scratch/brycef/sag/tangy/input/asm_contam

~/git/jgi-rqc-pipeline/sag_decontam/asm_contam.py -pl \
-o /global/projectb/sandbox/rqc/analysis/brycef/rqc-dev/pipelines/sag_decontam/archive/00/00/40/00/input/asm_contam \
-i /global/projectb/sandbox/rqc/analysis/brycef/rqc-dev/pipelines/sag_decontam/archive/00/00/40/00/input/contam3.txt

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import getpass
import glob

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, get_colors, run_cmd, get_msg_settings


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Shred each assembly into 2000bp chunks
- discard the last shred since its likely < 2000bp and won't meet the minlength hit criteria anyhow
'''
def shred_assemblies(shred_length, min_length):
    log.info("shred_assemblies: %s bp, min: %s bp", shred_length, min_length)

    all_fasta = None

    fh = open(input_file, "r")
    for line in fh:
        line = line.strip()
        if line.startswith("#"):
            continue

        arr = line.split("|")
        if len(arr) != 3:
            continue

        lib = str(arr[0]).strip()
        fasta = str(arr[1]).strip()

        # special case, all = everything combined together
        if lib.lower() == "all":
            all_fasta = fasta

            continue

        lib_path = os.path.join(output_path, lib)
        if not os.path.isdir(lib_path):
            os.makedirs(lib_path)

        shred_fasta = os.path.join(lib_path, "%s.shred.fasta" % lib)
        # shred with bbtools
        if os.path.isfile(shred_fasta):
            log.info("- skipping shred, found file: %s  %s", shred_fasta, msg_warn)
        else:
            shred_log = os.path.join(lib_path, "shred.log")
            cmd = "#bbtools;shred.sh in=%s out=%s length=%s minlength=%s ow=t > %s 2>&1" % (fasta, shred_fasta, shred_length, min_length, shred_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)


    fh.close()

    return all_fasta

'''
Blast all shreded fastas vs the combined fasta (all_fasta)
'''
def blast_assemblies(all_fasta):
    log.info("blast_assemblies: %s", all_fasta)


    makedb_log = os.path.join(output_path, "make_db.log")
    blast_db = os.path.join(output_path, os.path.basename(all_fasta).replace(".fasta", ".blast"))
    if os.path.isfile(blast_db):
        log.info("- skipping makeblastdb for: %s  %s", all_fasta, msg_warn)
    else:
        cmd = "#blast;makeblastdb -dbtype nucl -in %s -out %s > %s 2>&1" % (all_fasta, blast_db, makedb_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)



    fh = open(input_file, "r")
    for line in fh:
        line = line.strip()
        if line.startswith("#"):
            continue

        arr = line.split("|")
        if len(arr) != 3:
            continue

        lib = str(arr[0]).strip()

        # special case, all = everything combined together
        if lib.lower() == "all":
            continue


        lib_path = os.path.join(output_path, lib)
        shred_fasta = os.path.join(lib_path, "%s.shred.fasta" % lib)

        # queue up shred_fasta?
        blast_me(lib, lib_path, shred_fasta, blast_db)

    fh.close()


'''
Align the shreded assembly vs all assemblies
- 2-5 seconds for each library
'''
def blast_me(lib, lib_path, fasta, blast_db):
    log.info("blast_me: %s", lib)
    log.info("- q=%s", fasta)
    log.info("- db=%s", blast_db)

    min_blast_pct_id = 99

    done_file = os.path.join(lib_path, "done.txt")
    if os.path.isfile(done_file):
        log.info("- skipping blast, found file %s  %s", done_file, msg_warn)
    else:


        blast_log = os.path.join(lib_path, "blast.log")
        #blastn -query /global/projectb/scratch/brycef/sag/tangy/input/asm_contam/CCYYB/CCYYB.shred.fasta -db /global/projectb/scratch/brycef/sag/tangy/input/asm_contam/all_contigs.fasta -perc_identity 99 -num_threads 8 -evalue 1e-30 -task megablast -outfmt '6 qseqid sseqid qstart qend qlen sstart send slen length'
        blast_params = "-perc_identity %s -dust yes -num_threads 8 -evalue 1e-30 -task megablast -outfmt '6 qseqid sseqid qstart qend qlen sstart send slen length pident'" % min_blast_pct_id
        cmd = "#blast;blastn -query %s -db %s %s > %s 2>&1" % (fasta, blast_db, blast_params, blast_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        # -dust yes = Filter query sequence with DUST (Format: 'yes', 'level window linker', or 'no' to disable)??
        
        #blast_cmd = "%s/run_blastplus.py" % (os.path.realpath(os.path.join(my_path, '../tools')))
        #cmd = "%s -l -q %s -d %s -o %s -t 8 > %s 2>&1" % (blast_cmd, fasta, blast_db, lib_path, blast_log)
        #std_out, std_err, exit_code = run_cmd(cmd, log)

        # Jigsaw blast params
        #-perc_identity 99 -evalue 1e-30 -dust yes -num_threads 8

        if exit_code == 0:
            fh = open(done_file, "w")
            fh.write("blast %s = done\n" % lib)
            fh.close()

            # sed -i -- add header line to the blast file inline
            cmd = "sed -i '1s/^/#qseqid sseqid qstart qend qlen sstart send slen length pident\\n/' %s" % blast_log
            std_out, std_err, exit_code = run_cmd(cmd, log)


'''
Summarize blast hits for each library
- if query lib == subject lib then its not a hit ... min_length
- percent of subject = number of hits relative to what the expected library is, e.g. 45 hits to AAAAA, 5 to AAAAB = pct2 = 45/45 for AAAAA, 5/45 for AAAAB
'''
def sum_blast_hits(contam_summary, min_length):
    log.info("sum_blast_hits: %s", contam_summary)
    err_cnt = 0
    ttl_good_hit = 0
    ttl_contam_hit = 0


    fh = open(contam_summary, "w")

    fh.write("# Library|Contam Library|Number of hits|Pct total hits|Pct of subject\n")
    fh.close()

    fh = open(input_file, "r")
    for line in fh:
        line = line.strip()
        if line.startswith("#"):
            continue

        arr = line.split("|")
        if len(arr) != 3:
            continue

        lib = str(arr[0]).strip()

        # special case, all = everything combined together
        if lib.lower() == "all":
            continue

        log.info("- lib: %s", lib)

        lib_path = os.path.join(output_path, lib)
        blast_file = os.path.join(lib_path, "blast.log")
        
        shred_cnt = 0
        shred_fa = os.path.join(lib_path, "%s.shred.fasta" % lib)
        if os.path.isfile(shred_fa) and os.path.getsize(shred_fa) > 0:
            cmd = "grep -c '>' %s" % (shred_fa)
            std_out, std_err, exit_code = run_cmd(cmd, log)
            shred_cnt = int(std_out.strip())
        
        
        #blast_list = glob.glob(os.path.join(lib_path, "*.parsed"))
        #if len(blast_list) > 0:
        #    blast_file = blast_list[0]
        #else:
        #    log.error("- missing blast file for %s!  %s", lib, msg_warn)

        if shred_cnt > 0:
            good_hit, contam_hit = get_hit_count(lib, blast_file, contam_summary, min_length, shred_cnt)
            if good_hit < 0:
                err_cnt += 1
            else:
                ttl_good_hit += good_hit
                ttl_contam_hit += contam_hit


    fh.close()

    if err_cnt > 0:
        log.error("- missed blast for %s libraries!  %s", err_cnt, msg_fail)
        sys.exit(2)

    log.info("- total good hits: %s, total contam hits: %s", ttl_good_hit, ttl_contam_hit)
    log.info("- mean contam: %s", "{:.2%}".format(ttl_contam_hit / float(ttl_good_hit + ttl_contam_hit)))


'''
Summarize hits from blast.log and append to contam_summary file
If hit same node 2x, count only one time
CAYWP_NODE_69_length_7479_cov_6.51571_2000-3999 CAYZA_NODE_92_length_3173_cov_2.17251   797     0.0             431     100.000 1343    1773    2000    1       431     2773    N/A     CAYZA_NODE_92_length_3173_cov_2.17251
CAYWP_NODE_69_length_7479_cov_6.51571_2000-3999 CAYZA_NODE_92_length_3173_cov_2.17251   370     5.75e-101       200     100.000 1801    2000    2000    442     641     2773    N/A     CAYZA_NODE_92_length_3173_cov_2.17251
'''
def get_hit_count(lib, blast_file, contam_summary, min_length, shred_cnt):
    log.info("get_hit_count: %s, %s", lib, blast_file)
    log.info("- shred cnt: %s", shred_cnt)
    
    good_hit = 0
    contam_hit = 0
    min_pct_id = 95.0 # guessed this number

    # how many shreds?
    

    hit_dict = {} # library -> hits
    seen_dict = {} # key = q_name + s_name, if exists skip
    
    if os.path.isfile(blast_file):
        fh = open(blast_file, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue

            arr = line.split()

            #if len(arr) != 14:
            #    continue

            q_name = arr[0]
            s_name = arr[1]
            #print arr
            #print len(arr)
            #sys.exit(44)
            
            
            qlen = int(arr[8])
            pct_id = float(arr[9])
            
            qlib = str(q_name.split("_")[0]).lower() # query lib
            slib = str(s_name.split("_")[0]).lower() # source lib
            
            if qlen >= min_length and pct_id >= min_pct_id:
                
                my_key = "%s+%s" % (q_name, s_name)
                if my_key in seen_dict:
                    continue
                
                seen_dict[my_key] = 1
                
                if qlib == slib:
                    good_hit += 1
                else:
                    contam_hit += 1

                if slib in hit_dict:
                    hit_dict[slib] += 1
                else:
                    hit_dict[slib] = 1

        fh.close()


        total_hit = good_hit + contam_hit

        # how many hits to the library (itself)
        subject_hit = hit_dict.get(lib.lower(), 0)


        fh = open(contam_summary, "a")
        for my_lib in hit_dict:
            pct = 100 *  hit_dict[my_lib] / float(total_hit)
            pct = "{:.1f}".format(pct) # 1 digit precision

            pct2 = 0.0
            if subject_hit > 0:
                pct2 = 100 * hit_dict[my_lib] / float(shred_cnt) #float(subject_hit)
                pct2 = "{:.1f}".format(pct2) # 1 digit precision

            fh.write("%s|%s|%s|%s|%s\n" % (lib, my_lib.upper(), hit_dict[my_lib], pct, pct2))
        fh.close()

    else:
        log.error("- missing blast file: %s  %s", blast_file, msg_fail)
        good_hit = -1

    log.info("- good hit: %s, contam_hit: %s", good_hit, contam_hit)

    return good_hit, contam_hit


"""
may look into this if plate size > 200 libs

from checkm - consider multi-threading the do_blasts - put in a queue
from threading import Thread
import threading

#def run_thread(fasta, rtype, odir, verbose):
#    t = Thread(name=rtype, target=do_checkm, args=(fasta, rtype, odir, verbose))
#    t.start()
#    return t

    while True:
        tcnt = threading.active_count()
        if tcnt == 1: # main thread
            break
        if verbose:
            main_thread = threading.currentThread()
            for t in threading.enumerate():
                if t != main_thread:
#                    print('  - wait on thread %s to complete' % t.getName())
#            print('\n')
#        time.sleep(5)
"""
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "Assembly Cross Contamination"
    version = "1.1"

    uname = getpass.getuser() # get the user


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)


    # Parse options
    usage = "asm_contam.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """

    """


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-i", "--input-file", dest="input_file", help = "File with a list of library|assembly file")
    parser.add_argument("-s", "--shred-length", dest="shred_length", type=int, help = "Shred assemblies into this many base pairs (default 2000)")
    parser.add_argument("-m", "--min-length", dest="min_length", type=int, help = "Minimum blast hit length (default 1900)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)

    output_path = None
    shred_length = 2000 # 2000bp
    min_length = 1900 # minimum length for blast alignment
    input_file = None

    # parse args

    args = parser.parse_args()

    print_log = args.print_log

    if args.output_path:
        output_path = args.output_path

    if args.input_file:
        input_file = args.input_file

    if args.shred_length:
        shred_length = args.shred_length

    if args.min_length:
        min_length = args.min_length

    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/sag"

        if output_path == "t1":
            input_file = os.path.join(test_path, "tangy", "input", "contam.txt")
            output_path = os.path.join("tangy", "input", "asm_contam")



        output_path = os.path.join(test_path, output_path)



    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()

    # if not starting with /  use relative path (GAA-3421)
    if not output_path.startswith("/"):
        output_path = os.path.join(os.getcwd(), output_path)


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    if not input_file:
        print "Error: input file missing  %s" % msg_fail
        sys.exit(2)

    if not os.path.isfile(input_file):
        print "Error: input file does not exist  %s" % msg_fail
        sys.exit(2)



    # initialize my logger
    log_file = os.path.join(output_path, "asm_contam.log")



    # log = logging object
    log_level = "INFO"

    log = get_logger("asm_contam", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "input_file", input_file)
    log.info("%25s      %s bp", "shred_length", shred_length)
    log.info("%25s      %s bp", "min_length", min_length)
    log.info("%25s      %s", "output_path", output_path)

    log.info("")




    # Here we go!
    combo_fasta = shred_assemblies(shred_length, min_length)
    contam_summary = os.path.join(output_path, "asm_contam.txt")

    if not combo_fasta:
        log.error("no combined fasta, need to do this code...")
        sys.exit(44)
    else:
        symlink_combo_fasta = os.path.join(output_path, "all_contigs.fasta")
        cmd = "ln -s -f %s %s" % (combo_fasta, symlink_combo_fasta)
        # -f remove existing destination file
        std_out, std_err, exit_code = run_cmd(cmd, log)

        combo_fasta = symlink_combo_fasta

    blast_assemblies(combo_fasta)

    sum_blast_hits(contam_summary, min_length)

    sys.exit(0)



"""

                      .-.
         .-._    _.../   `,    _.-.
         |   `'-'    \     \_'`   |
         \            '.__,/ `\_.--,
          /                '._/     |
         /                    '.    /
        ;   _                  _'--;
     '--|- (_)       __       (_) -|--'
     .--|-          (__)          -|--.
      .-\-                        -/-.
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`

tangy test file: contam.txt (same as read_contam's input)
# Library | Assembly File | Fastq File
CCYUA|/global/projectb/scratch/brycef/sag/tangy/input/CCYUA.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUA-12075.1.235852.TAAGGCG-AGAGGAT.filter-SAG.fastq.gz
CCYUB|/global/projectb/scratch/brycef/sag/tangy/input/CCYUB.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUB-12075.1.235852.TAAGGCG-CTCCTTA.filter-SAG.fastq.gz
CCYUC|/global/projectb/scratch/brycef/sag/tangy/input/CCYUC.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUC-12077.1.236082.TAAGGCG-TATGCAG.filter-SAG.fastq.gz
CCYUG|/global/projectb/scratch/brycef/sag/tangy/input/CCYUG.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUG-12077.1.236082.TAAGGCG-TACTCCT.filter-SAG.fastq.gz
CCYUH|/global/projectb/scratch/brycef/sag/tangy/input/CCYUH.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUH-12075.1.235852.TAAGGCG-AGGCTTA.filter-SAG.fastq.gz
CCYUN|/global/projectb/scratch/brycef/sag/tangy/input/CCYUN.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUN-12075.1.235852.TAAGGCG-ATTAGAC.filter-SAG.fastq.gz
CCYUO|/global/projectb/scratch/brycef/sag/tangy/input/CCYUO.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUO-12075.1.235852.CGTACTA-ATAGAGA.filter-SAG.fastq.gz
CCYUP|/global/projectb/scratch/brycef/sag/tangy/input/CCYUP.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUP-12075.1.235852.CGTACTA-AGAGGAT.filter-SAG.fastq.gz
CCYUS|/global/projectb/scratch/brycef/sag/tangy/input/CCYUS.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUS-12075.1.235852.CGTACTA-CTCCTTA.filter-SAG.fastq.gz
CCYUT|/global/projectb/scratch/brycef/sag/tangy/input/CCYUT.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUT-12075.1.235852.CGTACTA-TATGCAG.filter-SAG.fastq.gz
CCYUU|/global/projectb/scratch/brycef/sag/tangy/input/CCYUU.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUU-12075.1.235852.CGTACTA-TACTCCT.filter-SAG.fastq.gz
CCYUW|/global/projectb/scratch/brycef/sag/tangy/input/CCYUW.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUW-12075.1.235852.CGTACTA-AGGCTTA.filter-SAG.fastq.gz
CCYUX|/global/projectb/scratch/brycef/sag/tangy/input/CCYUX.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUX-12075.1.235852.CGTACTA-ATTAGAC.filter-SAG.fastq.gz
CCYUY|/global/projectb/scratch/brycef/sag/tangy/input/CCYUY.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUY-12075.1.235852.CGTACTA-CGGAGAG.filter-SAG.fastq.gz
CCYUZ|/global/projectb/scratch/brycef/sag/tangy/input/CCYUZ.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYUZ-12075.1.235852.AGGCAGA-ATAGAGA.filter-SAG.fastq.gz
CCYWA|/global/projectb/scratch/brycef/sag/tangy/input/CCYWA.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWA-12075.1.235852.AGGCAGA-AGAGGAT.filter-SAG.fastq.gz
CCYWB|/global/projectb/scratch/brycef/sag/tangy/input/CCYWB.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWB-12075.1.235852.AGGCAGA-CTCCTTA.filter-SAG.fastq.gz
CCYWC|/global/projectb/scratch/brycef/sag/tangy/input/CCYWC.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWC-12075.1.235852.AGGCAGA-TATGCAG.filter-SAG.fastq.gz
CCYWG|/global/projectb/scratch/brycef/sag/tangy/input/CCYWG.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWG-12075.1.235852.AGGCAGA-TACTCCT.filter-SAG.fastq.gz
CCYWH|/global/projectb/scratch/brycef/sag/tangy/input/CCYWH.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWH-12075.1.235852.AGGCAGA-AGGCTTA.filter-SAG.fastq.gz
CCYWN|/global/projectb/scratch/brycef/sag/tangy/input/CCYWN.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWN-12075.1.235852.AGGCAGA-ATTAGAC.filter-SAG.fastq.gz
CCYWO|/global/projectb/scratch/brycef/sag/tangy/input/CCYWO.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWO-12075.1.235852.AGGCAGA-CGGAGAG.filter-SAG.fastq.gz
CCYWP|/global/projectb/scratch/brycef/sag/tangy/input/CCYWP.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWP-12075.1.235852.TCCTGAG-ATAGAGA.filter-SAG.fastq.gz
CCYWS|/global/projectb/scratch/brycef/sag/tangy/input/CCYWS.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWS-12075.1.235852.TCCTGAG-AGAGGAT.filter-SAG.fastq.gz
CCYWT|/global/projectb/scratch/brycef/sag/tangy/input/CCYWT.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWT-12075.1.235852.TCCTGAG-CTCCTTA.filter-SAG.fastq.gz
CCYWU|/global/projectb/scratch/brycef/sag/tangy/input/CCYWU.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWU-12075.1.235852.TCCTGAG-TATGCAG.filter-SAG.fastq.gz
CCYWW|/global/projectb/scratch/brycef/sag/tangy/input/CCYWW.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWW-12077.1.236082.TCCTGAG-TACTCCT.filter-SAG.fastq.gz
CCYWX|/global/projectb/scratch/brycef/sag/tangy/input/CCYWX.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWX-12077.1.236082.TCCTGAG-AGGCTTA.filter-SAG.fastq.gz
CCYWY|/global/projectb/scratch/brycef/sag/tangy/input/CCYWY.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWY-12077.1.236082.TCCTGAG-ATTAGAC.filter-SAG.fastq.gz
CCYWZ|/global/projectb/scratch/brycef/sag/tangy/input/CCYWZ.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYWZ-12077.1.236082.TCCTGAG-CGGAGAG.filter-SAG.fastq.gz
CCYXA|/global/projectb/scratch/brycef/sag/tangy/input/CCYXA.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXA-12077.1.236082.GGACTCC-ATAGAGA.filter-SAG.fastq.gz
CCYXB|/global/projectb/scratch/brycef/sag/tangy/input/CCYXB.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXB-12077.1.236082.GGACTCC-AGAGGAT.filter-SAG.fastq.gz
CCYXC|/global/projectb/scratch/brycef/sag/tangy/input/CCYXC.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXC-12077.1.236082.GGACTCC-CTCCTTA.filter-SAG.fastq.gz
CCYXG|/global/projectb/scratch/brycef/sag/tangy/input/CCYXG.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXG-12077.1.236082.GGACTCC-TATGCAG.filter-SAG.fastq.gz
CCYXH|/global/projectb/scratch/brycef/sag/tangy/input/CCYXH.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXH-12077.1.236082.GGACTCC-TACTCCT.filter-SAG.fastq.gz
CCYXN|/global/projectb/scratch/brycef/sag/tangy/input/CCYXN.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXN-12077.1.236082.GGACTCC-AGGCTTA.filter-SAG.fastq.gz
CCYXO|/global/projectb/scratch/brycef/sag/tangy/input/CCYXO.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXO-12077.1.236082.GGACTCC-ATTAGAC.filter-SAG.fastq.gz
CCYXP|/global/projectb/scratch/brycef/sag/tangy/input/CCYXP.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXP-12077.1.236082.GGACTCC-CGGAGAG.filter-SAG.fastq.gz
CCYXS|/global/projectb/scratch/brycef/sag/tangy/input/CCYXS.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXS-12077.1.236082.TAGGCAT-ATAGAGA.filter-SAG.fastq.gz
CCYXT|/global/projectb/scratch/brycef/sag/tangy/input/CCYXT.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXT-12077.1.236082.TAGGCAT-AGAGGAT.filter-SAG.fastq.gz
CCYXU|/global/projectb/scratch/brycef/sag/tangy/input/CCYXU.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXU-12077.1.236082.TAGGCAT-CTCCTTA.filter-SAG.fastq.gz
CCYXW|/global/projectb/scratch/brycef/sag/tangy/input/CCYXW.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXW-12077.1.236082.TAGGCAT-TATGCAG.filter-SAG.fastq.gz
CCYXX|/global/projectb/scratch/brycef/sag/tangy/input/CCYXX.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXX-12077.1.236082.TAGGCAT-TACTCCT.filter-SAG.fastq.gz
CCYXY|/global/projectb/scratch/brycef/sag/tangy/input/CCYXY.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXY-12077.1.236082.TAGGCAT-AGGCTTA.filter-SAG.fastq.gz
CCYXZ|/global/projectb/scratch/brycef/sag/tangy/input/CCYXZ.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYXZ-12077.1.236082.TAGGCAT-ATTAGAC.filter-SAG.fastq.gz
CCYYA|/global/projectb/scratch/brycef/sag/tangy/input/CCYYA.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYYA-12077.1.236082.TAGGCAT-CGGAGAG.filter-SAG.fastq.gz
CCYYB|/global/projectb/scratch/brycef/sag/tangy/input/CCYYB.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYYB-12077.1.236082.CTCTCTA-ATAGAGA.filter-SAG.fastq.gz
CCYYC|/global/projectb/scratch/brycef/sag/tangy/input/CCYYC.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYYC-12077.1.236082.CTCTCTA-AGAGGAT.filter-SAG.fastq.gz
CCYYG|/global/projectb/scratch/brycef/sag/tangy/input/CCYYG.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYYG-12077.1.236082.CTCTCTA-CTCCTTA.filter-SAG.fastq.gz
CCYYH|/global/projectb/scratch/brycef/sag/tangy/input/CCYYH.fasta|/global/projectb/scratch/brycef/sag/tangy/input/CCYYH-12077.1.236082.CTCTCTA-TATGCAG.filter-SAG.fastq.gz
#FAKE|/global/projectb/scratch/brycef/sag/tangy/input/FAKE.fasta|/global/projectb/scratch/brycef/sag/tangy/input/
ALL|/global/projectb/scratch/brycef/sag/tangy/input/all_contigs.fasta|/global/projectb/scratch/brycef/sag/tangy/input/


"""
