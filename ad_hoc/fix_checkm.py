#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Some checkm jobs in sag_pool pipeline failed due to the division by zero error and the png files need to be regenerated.
    Need to use qc_user account to run

    fix_checkm.py -l LIBNAME -t [s|u|a]
        s - fix checkm of the screened contigs
        u - fix checkm of the unscreend contigs
        a - fix both screened and unscreed

    3/23/2017
    Shijie Yao

"""

import os
import sys
from argparse import ArgumentParser
import MySQLdb
import glob
import shutil


ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from db_access import jgi_connect_db
from os_utility import run_sh_command

db = jgi_connect_db("rqc")
sth = db.cursor(MySQLdb.cursors.DictCursor)

def job_dir(lib):
    sql = 'select rqc_pipeline_queue_id, fs_location from rqc_pipeline_queue q inner join seq_units s on q.seq_unit_id=s.seq_unit_id where rqc_pipeline_type_id=29 and library_name = %s'
    sth.execute(sql, (lib, ))
    rows = sth.fetchall()
    if rows:
        return rows[0]['rqc_pipeline_queue_id'], rows[0]['fs_location']
    else:
        return None, None

def missing_png_file_in(adir):
    missing = False
    ckmdir = os.path.join(adir, 'checkm/arc/*.png')
    files = glob.glob(ckmdir)
    if len(files) < 3:
        missing = True

    if not missing:
        ckmdir = os.path.join(adir, 'checkm/bac/*.png')
        if len(files) < 3:
            missing = True

    if not missing:
        ckmdir = os.path.join(adir, 'checkm/lwf/*.png')
        if len(files) < 3:
            missing = True

    return missing

def get_file_path(kname, qid):
    sql = 'select concat(fs_location, file_name) path from rqc_pipeline_files where rqc_pipeline_queue_id=%s and is_production=1 and file_type = %s'
    sth.execute(sql, (qid, kname))
    rows = sth.fetchall()
    if rows:
        return str(rows[0]['path'])
    else:
        print('  - warning : no db path for %s' % kname)
        return None

def get_sag_pool_path(qid):
    sql = 'select fs_location from rqc_pipeline_files where rqc_pipeline_queue_id=%s and is_production=1 limit 1'
    sth.execute(sql, (qid,))
    row = sth.fetchone()
    if row:
        return str(row['fs_location'])
    else:
        print('  - error : no db path for qid=%d' % qid)
        return None

def add_db_entry(fs, kname, fname, qid):
    sql = 'insert into rqc_pipeline_files (rqc_pipeline_queue_id, fs_location, file_name, file_type, is_production) values(%s, %s, %s, %s, 1)'
    opts= (qid, fs, fname, kname)
    print('add to db : sql=%s; val=%s' % (sql, opts))

    sth.execute(sql, opts)
    lid = int(sth.lastrowid)
    nfname = '%d-%s' % (lid, os.path.basename(fname))
    sql = 'update rqc_pipeline_files set file_name = %s where rqc_pipeline_file_id = %s'
    opts = (nfname, lid)
    print('add to db : sql=%s; val=%s' % (sql, opts))
    sth.execute(sql, opts)
    print('new file name = %s' % nfname)
    return nfname

def copy_file(src, des):
    if des:
        print('  - copy %s to %s' % (os.path.basename(src), des))
        shutil.copyfile(src, des)

def update_files(adir, typedir, ftype, qid):
    pngfilter = os.path.join(adir, '*.png')
    pnglist = glob.glob(pngfilter)
    if len(pnglist) == 3:
        for png in pnglist:
            kname = '%s_%s' % (typedir, ftype[os.path.basename(png)])
            tofile = get_file_path(kname, qid)
            if not tofile:
                pdir = get_sag_pool_path(qid)
                if pdir:
                    tofile = add_db_entry(pdir, kname, png, qid)
                    if tofile:
                        tofile = os.path.join(pdir, tofile)
            if tofile:
                copy_file(png, tofile)


def do_repair(adir, fasta, typedir, qid):
    FTYPE = {   'arc' : { 'checkm_chart_d-arc.png' : 'checkm_chart_d-arc_image',
                            'checkm_chart_d-arc_nsc.png' : 'checkm_chart_d-arc_nsc_image',
                            'checkm_histogram_d-arc.png' : 'checkm_histogram_d-arc_image'},
                'bac' : { 'checkm_chart_d-bac.png' : 'checkm_chart_d-bac_image',
                            'checkm_chart_d-bac_nsc.png' : 'checkm_chart_d-bac_nsc_image',
                            'checkm_histogram_d-bac.png' : 'checkm_histogram_d-bac_image'},
                'lwf' : { 'checkm_chart_lwf.png' : 'checkm_chart_lwf_image',
                            'checkm_chart_lwf_nsc.png' : 'checkm_chart_lwf_nsc_image',
                            'checkm_histogram_lwf.png' : 'checkm_histogram_lwf_image'}
    }

    cmd = 'module load jgi-rqc; checkm_helper.py -i %s' % fasta
    os.chdir(adir)
    ckmdir = os.path.abspath('./checkm')

    if os.path.isdir(ckmdir):
        tmp = '%s_old' % ckmdir
        print('  - rename %s to %s' % (ckmdir, tmp))
        os.rename(ckmdir, tmp)

    print('  - run %s' % cmd)
    sout, serr, ecode = run_sh_command(cmd, True)

    if ecode == 0:
        if os.path.isdir(ckmdir):
            arcdir = os.path.join(ckmdir, 'arc')
            bacdir = os.path.join(ckmdir, 'bac')
            lwfdir = os.path.join(ckmdir, 'lwf')
            update_files(arcdir, typedir, FTYPE['arc'], qid)
            update_files(bacdir, typedir, FTYPE['bac'], qid)
            update_files(lwfdir, typedir, FTYPE['lwf'], qid)
    else:
        print('   ERROR: %s' % serr)

def fix_one(jdir, atype, qid):
    # check if any png files are missing from jdir/screen
    if atype in ('s', 'a'):
        typedir = 'screen'
        fasta = os.path.join(jdir, 'prodege/sag_decontam/sag_decontam_output_clean.fna')
        if os.path.isfile(fasta):
            print('  - find fasta : %s' % fasta)
            adir = os.path.join(jdir, typedir)
            print('  - need fix %s' % adir)
            do_repair(adir, fasta, typedir, qid)
        else:
            print('  - error : no fasta file found %s' % fasta)

    if atype in ('u', 'a'):
        typedir = 'unscreen'
        fasta = os.path.join(jdir, 'trim/scaffolds.trim.fasta')
        if os.path.isfile(fasta):
            print('  - find fasta : %s' % fasta)
            adir = os.path.join(jdir, typedir)
            print('  - need fix %s' % adir)
            do_repair(adir, fasta, typedir, qid)
        else:
            print('  - error : no fasta file found %s' % fasta)


def fix_libs(libs, atype):
    for lib in libs:
        print('Fixing %s' % lib)
        qid, jdir = job_dir(lib)
        if jdir:
            print('  - find job dir : %s' % jdir)
            fix_one(jdir, atype, qid)
        else:
            print('  - error : no sag_pool job found in RQC for library %s' % lib)

if __name__ == '__main__':
    print('Run fix checkm')

    NAME = 'Rerun checkm on sag_pool libs'
    VERSION = '1.0.0'

    USAGE = 'prog [options]\n'
    USAGE += '* %s, version %s\n' % (NAME, VERSION)

    JOB_CMD = ' '.join(sys.argv)
    PARSER = ArgumentParser(usage=USAGE)
    PARSER.add_argument('-l', '--libs', dest='libs', help='lib names', required=True)
    PARSER.add_argument('-t', '--type', dest='type', help='type screen[s], unscreen[u], all[a]', required=True)
    PARSER.add_argument('-v', '--version', action='version', version=VERSION)

    ARGS = PARSER.parse_args()

    if os.getenv('USER') != 'qc_user':
        print('Error : need to use qc_user account to run!')
        sys.exit(1)

    libs = None
    if ARGS.libs:
        libs = ARGS.libs.split(',')
    TYPES = ('s', 'u', 'a')
    if ARGS.type not in TYPES:
        print('Error : type need to be in %s' % str(TYPES))
        sys.exit(1)


    if not libs:
        print('Error : need libs')
    else:
        fix_libs(libs, ARGS.type)
