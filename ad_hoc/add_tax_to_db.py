#!/usr/bin/env python
'''
	add new ncbi tax names to taxtonames table
	Update History:
	7/2/2014: init implementation

	Shijie Yao 
'''

import os
import sys
import argparse
import time
import MySQLdb

sys.path.append('../lib')
from db_access import db_connect
from common import run_command 

def is_in_db(sth, aid):
	sql = 'SELECT name from taxtonames WHERE taxid = %s'
	sth.execute(sql, (aid, ))
	return sth.rowcount == 1

def add_to_db(sth, fname):
	sql = 'INSERT INTO taxtonames (taxid, name, name_lc) VALUES(%s, %s, %s)'
	with open(fname) as fh:
		for line in fh:
			line = line.strip()
			if line.startswith('#'):
				continue

			tok = line.split()
			if len(tok) > 1:
				aid = tok[0]
				name = ' '.join(tok[1:])
				if is_in_db(sth, aid):
					print('  - Already in DB : %s' % aid)
				else:
					print('  - Inserting (%s, %s)' % (aid, name))
					sth.execute(sql, (aid, name, name.lower()))
	

def dbinfo(sth):
	sql = 'SELECT @@hostname AS hostname, database() AS dbname, user() AS uname'
	sth.execute(sql)
	rows = sth.fetchone()
	return 'DB host=%s, name=%s, user=%s' % (rows['hostname'], rows['dbname'], rows['uname'])
	
#===============================
# The main file
if __name__ == "__main__":

	my_name = "Ad hoc script to clean up seq_units db table"
	version = "1.0.0"
        
	if len(sys.argv) < 2:
		print('need a input file of (taxid taxname) as the sole cml argument')
		exit(1)

	ifile = sys.argv[1]
	print('Input file name = %s' % ifile)

	if not os.path.isfile(ifile):
		print('    - invalid file name')
		exit(1)
	

        db = db_connect(db_server='production', db_name='rqc_tax', db_user='rqc_prod', db_pwd='dHJvd3QudGFYZXMub0Jsb25nLjIyMzs=')
	#db = db_connect(db_server='draw.jgi-psf.org', db_name='rqc_tax', db_user='rqc_dev', db_pwd='b1Jpb24uVGhyeWxsLnJhdDo5OQ==')
	
	if db == None:
		print('Error : failed to connect to RQC database');
		sys.exit(1)
		
	sth = db.cursor(MySQLdb.cursors.DictCursor)
	dbparam = dbinfo(sth);
	print(dbparam)

	add_to_db(sth, ifile)

	sys.exit(0)
	
