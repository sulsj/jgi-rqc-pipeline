#!/usr/bin/env python
'''
    check completed jigsaw job the existance of jigsaw_stats.yaml (yml) file and if rqc.txt contains
    an entry for it. append the missing entry

    need to logon to
      qc_user@genepool10:/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/ad_hoc
    and run >python add_stats_to_rqc_txt.py

   Update History:
   2/12/2014: init implementation

   Shijie Yao
'''

import os
import sys
import argparse

import MySQLdb
import glob

sys.path.append('../lib')
from db_access import jgi_connect_db
from common import run_command

#all completed
def jigsaw_completed(sth):
    dateMax = '2014-02-12 00:00:00'
    sql = '''
            SELECT rqc_pipeline_queue_id, fs_location, dt_created
            FROM rqc_pipeline_queue
            WHERE rqc_pipeline_type_id in (8, 9) AND rqc_pipeline_status_id=14 AND dt_created < %s
            ORDER by dt_created
        '''

    sth.execute(sql, dateMax)
    cnt = sth.rowcount

    rtn = []
    for n in range(cnt):
        row = sth.fetchone()
        rtn.append((row['fs_location'], row['dt_created'], row['rqc_pipeline_queue_id']));

    return rtn


def contains_stats(txt, fName):
    cmd = 'grep %s %s' % (txt, fName)

    stdOut, stdErr, exitCode = run_command(cmd, True)
    if exitCode == 0:   # for grep, when it find the string in file, the exitCode = 0
        return True
    elif exitCode == 1: # for grep, when it failed to find the string in file, the exitCode = 1
        return False
    else:   # eg 2 if the given file not exist
        print( 'ERROR : failed to execute cmd [%s][%s]' % (cmd, exitCode) )
        return False

# return rqc_files.txt or any file of with this prefix in the given location. return None if no such file
def find_rqc_files_txt(fs_location):
    for afile in glob.glob(os.path.join(fs_location, "rqc_files.txt*")):
        if afile:
            return afile
    return None

def is_rqc_files_txt_contain_stats(txt, fName):
    cmd = 'grep %s %s' % (txt, fName)
    stdOut, stdErr, exitCode = run_command(cmd, True)
    if exitCode == 0:   # for grep, when it find the string in file, the exitCode = 0
        return True
    elif exitCode == 1: # for grep, when it failed to find the string in file, the exitCode = 1
        return False
    else:   # eg 2 if the given file not exist
        print( 'ERROR : failed to execute cmd [%s][%s]' % (cmd, exitCode) )
        return False

def append_to(line, toFile, exe):
    #cmd = 'echo \"\n%s\" >> %s' % (line, toFile)
    cmd = 'echo %s >> %s' % (line, toFile)
    if not exe:
        print('CMD=%s' % cmd)
    else:
        stdOut, stdErr, exitCode = run_command(cmd, True)
        if exitCode != 0:
            print('Failed to execute the command [%s][%s]' % (cmd, exitCode) )


def process(sth, exe):
    jobList = jigsaw_completed(sth)

    for pair in jobList:
        fs_location, run_date, qid = pair
        statFile1 = os.path.join(fs_location, 'jigsaw_stats.yaml')
        statFile2 = os.path.join(fs_location, 'jigsaw_stats.yml')
        statFile3 = os.path.join(fs_location, 'jigsaw_stats.txt')

        #print('[%s][%s]' % (statFile1, statFile2))
        statFile = None
        if os.path.isfile(statFile1):
            statFile = 'jigsaw_stats.yaml'
        elif os.path.isfile(statFile2):
            statFile = 'jigsaw_stats.yml'
        elif os.path.isfile(statFile3):
            statFile = 'jigsaw_stats.txt'
            #print("%s\t%s" % (pair[0], pair[1]))


        if statFile:    # the jigsaw_stats file exits
            rqc_files =  find_rqc_files_txt(fs_location)
            #print('find [%s]' % rqc_files)
            rqcOK = contains_stats(statFile, rqc_files)
            if not rqcOK:
                rqc_entry = 'jigsawStatsFile=%s' % os.path.join(fs_location, statFile)
                print( 'fixed [qid=%s\t%s\t%s]' % (qid, fs_location, run_date) )
                append_to(rqc_entry, rqc_files, exe)

                sys.exit(1)

        else:
            print( ' no stat file [qid=%s\t%s\t%s]' % (pair[2], pair[0], pair[1]) )



def dbinfo(sth):
    sql = 'SELECT @@hostname AS hostname, database() AS dbname, user() AS uname'
    sth.execute(sql)
    rows = sth.fetchone()
    return 'DB host=%s, name=%s, user=%s' % (rows['hostname'], rows['dbname'], rows['uname'])



#===============================
# The main file
if __name__ == "__main__":

    my_name = "Ad hoc script to fix rqc.txt missing jigsaw_stats.yaml entry"
    version = "1.0.0"

    # Parse options
    usage = "*%s, version %s\n" % (my_name, version)

    # command line options
    parser = argparse.ArgumentParser(description=usage, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-e", "--exe", dest="exe", action='store_true', help="to execute the action. otherwise, only dry-run")
    parser.add_argument("-v", "--version", action="version", version=version)

    options = parser.parse_args()


    # global shared objects
    db = jgi_connect_db("rqc")

    if db == None:
        print('Error : failed to connect to RQC database');
        sys.exit(1)

    sth = db.cursor(MySQLdb.cursors.DictCursor)

    dbparam = dbinfo(sth);

    print(dbparam)


    process(sth, options.exe)

    db.close()
    sys.exit(0)
