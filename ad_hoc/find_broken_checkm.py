#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Search sag-pool jobs for broken checkm (missing png files), and report

    3/24/2017
    Shijie Yao

"""

import os
import sys
from argparse import ArgumentParser
import MySQLdb
import glob
import shutil


ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from db_access import db_connect

db = db_connect(db_server='scidb1.nersc.gov', db_user='rqc_prod', db_name='rqc', db_pwd='dHJvd3QudGFYZXMub0Jsb25nLjIyMzs=')
sth = db.cursor(MySQLdb.cursors.DictCursor)

def missing_png_file_in(adir):
    missing = False
    ckmdir = os.path.join(adir, 'checkm/arc/*.png')
    files = glob.glob(ckmdir)
    if len(files) < 3:
        missing = True

    if not missing:
        ckmdir = os.path.join(adir, 'checkm/bac/*.png')
        if len(files) < 3:
            missing = True

    if not missing:
        ckmdir = os.path.join(adir, 'checkm/lwf/*.png')
        if len(files) < 3:
            missing = True

    return missing


def do_search():
    sql = '''select s.library_name, rqc_pipeline_queue_id, fs_location
            from rqc_pipeline_queue q inner join seq_units s on q.seq_unit_id=s.seq_unit_id
            where rqc_pipeline_type_id=29 and rqc_pipeline_status_id=14
            '''
    sth.execute(sql)
    rows = sth.fetchall()
    msg = ''
    goodlist = []
    dcnt = len(rows)
    fcnt = 0
    for row in rows:
        #print('pipeline dir : %s' % row['fs_location'])
        lib = row['library_name']
        adir = row['fs_location']

        # TODO : debug
        # if lib != 'BONWB':
        #     continue

        if os.path.isdir(adir):
            title = 'pipeline dir : %s\n%s' % (adir, lib)
            sdir = os.path.join(row['fs_location'], 'screen')
            if os.path.isdir(sdir) and os.path.isdir(os.path.join(sdir, 'checkm')):
                fasta = os.path.join(adir, 'prodege/sag_decontam/sag_decontam_output_clean.fna')
                if os.path.isfile(fasta) and os.stat(fasta).st_size > 0:
                    tofix = missing_png_file_in(sdir)
                    if tofix:
                        msg = '%s\n%s' % (title, '  - need to fix screen')

            usdir = os.path.join(row['fs_location'], 'unscreen')
            if os.path.isdir(usdir) and os.path.isdir(os.path.join(usdir, 'checkm')):
                fasta = os.path.join(adir, 'trim/scaffolds.trim.fasta')
                if os.path.isfile(fasta) and os.stat(fasta).st_size > 0:
                    tofix = missing_png_file_in(usdir)
                    if tofix:
                        if msg == '':
                            msg = title
                        msg += '\n' + '  - need to fix unscreen'

            if msg:
                print('%s\n' % msg)
                fcnt += 1
            else:
                goodlist.append(adir)

        msg = ''

    print('Total data : %d' % dcnt)
    print('Bad data : %d' % fcnt)

    # print('Good data:')
    # print('%s' % '\n'.join(goodlist))


if __name__ == '__main__':
    print('search checkm in sag-pool')

    NAME = 'Check checkm in sag_pool pipeline'
    VERSION = '1.0.0'

    USAGE = 'prog [options]\n'
    USAGE += '* %s, version %s\n' % (NAME, VERSION)

    JOB_CMD = ' '.join(sys.argv)
    PARSER = ArgumentParser(usage=USAGE)
    PARSER.add_argument('-v', '--version', action='version', version=VERSION)

    ARGS = PARSER.parse_args()

    do_search()
