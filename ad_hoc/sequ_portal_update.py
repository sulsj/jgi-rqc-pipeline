#!/usr/bin/env python
'''
    RQCSUPPORT-289 : some segmentQCed data have their seq units in unQCed status.
    find those seq units, and set QC status to 1 (Data Usable) and update JAMO.

   Update History:
   4/24/2014: init implementation

   Shijie Yao
'''

import os
import sys
import argparse
import json

import MySQLdb

sys.path.append('../lib')
from curl import Curl, CurlHttpException
from db_access import jgi_connect_db



PRODUCTION = True

#-----------------------------------------------------------------
# functions

'''
     if filtered fastq is available for portal, should have
     {
       "metadata" : {
            "portal":{
                "display_location":[
                    "Raw Data"
                ]
            },
       },

     }
'''
def filtered_fastq_in_jamo(jamodat):
    'check if the jamo json object contains the filtered fastq info'
    if 'metadata' in jamodat:
        metadata = jamodat['metadata']
        if 'portal' in metadata:
            if 'display_location' in metadata['portal'] and len(metadata['portal']['display_location']) > 0 and metadata['portal']['display_location'][0]:
                return True

'''

     if the raw fastq is available for portal, should have
     {
        "metadata" : {
             "rqc":{
                "update_date":"2014-05-07 11:06:49",
                "usable":true,
                "analyst_name":"RQC FIX",
                "comments":"Sync RQC with portal"
            },
        },
     }
'''
def raw_fastq_in_jamo(jamodat):
    'check if the jamo json object contains the seq unit qc info'
    if 'metadata' in jamodat:
        metadata = jamodat['metadata']
        if 'rqc' in metadata:
            if 'usable' in metadata['rqc'] and metadata['rqc']['usable'] == True:
                return True

    return False

def jamo_record(metaid):
    url = 'https://sdm2.jgi-psf.org'
    api = 'api/metadata/file/%s' % metaid
    curl = Curl(url)
    return curl.get(api)    # dict


def has_been_seq_qced(sun):
    sql = 'SELECT qc_state FROM seq_unit_qc WHERE seq_unit_name = %s'
    sth.execute(sql, (sun))
    row = sth.fetchone()
    if row and row['qc_state'] == 1:
        return True
    else:
        return False



def get_metaid(suid):
    sql = 'SELECT metadata_id FROM seq_unit_file WHERE seq_unit_id = %s AND file_type = %s'
    #print('DEBUG get_metaid: sql=%s; id=%s' % (sql, suid))
    sth.execute(sql, (suid, 'ILLUMINA COMPRESSED RAW FASTQ'))
    row = sth.fetchone()
    if row:
        return row['metadata_id']
    else:
        return None


def do_fix(sun, live=False):
    ws_host = 'rqc-9'  # Shijie's dev web server

    if PRODUCTION:
        ws_host = 'rqc-legacy.jgi-psf.org'  # the new legacy production server


    seq_qc_cmd = "curl -d \"state=1&comments=Sync RQC with portal&analyst=%s&seqUnit=%s&qc_html=true\""
    seq_qc_cmd += " https://%s/cgi-bin/display_report_illumina.cgi" % ws_host
    seq_qc_cmd = seq_qc_cmd % ('RQC FIX', sun)

    if live:
        import commands as exe
        write_log('Command: %s\n' % seq_qc_cmd)
        out = exe.getoutput(seq_qc_cmd)

        out = exe.getoutput(seq_qc_cmd)
        write_log(out)
        write_log('       ---------------\n')

    else:
        write_log('----------------------')
        write_log('To submit, repeat previous command with -exe option: \n%s' % seq_qc_cmd)
        write_log('----------------------')

    return True


def do_lib(sth, lname, live=False):
    DO_OK = True
    print('do_lib - work on : %s' % lname)

    sql = 'SELECT seq_unit_id, seq_unit_name FROM seq_units WHERE library_name = %s'
    sth.execute(sql, (lname, ))
    cnt = sth.rowcount
    rows = sth.fetchall()
    for row in rows:
        suid = row['seq_unit_id']
        sun = row['seq_unit_name']
        metaid = get_metaid(suid)

        if not metaid:
            print('      ??? no meta data id in RQC for %s ???' % sun)
        else:
            print('      JAMO id = %s' % metaid)
            jobj = jamo_record(metaid)
            rawInJamo = raw_fastq_in_jamo(jobj)
            filteredInJamo = filtered_fastq_in_jamo(jobj)
            qcInRQC = has_been_seq_qced(sun)

            if rawInJamo:
                print('      raw fastq already in JAMO')
            if filteredInJamo:
                print('      filtered fastq already in JAMO')
            if qcInRQC:
                print('      Has been QCed in RQC')

            if not qcInRQC or not filteredInJamo:
                print('      --- %s not been QCed' % sun)
                rtn = do_fix(sun, live)

            print('')

    return DO_OK


def do_sun(sth, sun, live=False):
    DO_OK = True
    print('do_sun - work on : %s' % sun)

    return DO_OK



def do_value(sth, iname, live=False):
    DO_OK = True
    if not iname or iname.startswith('#'):
        print('do_value - skip : %s' % iname)
    else:
        if iname.endswith('.fastq.gz') or iname.endswith('.srf'):
            st = do_sun(sth, iname, live)
        else:
            st = do_lib(sth, iname, live)
            print('')
            print('')


    return DO_OK


def do_file(sth, fpath, live=False):
    DO_OK = True
    if os.path.isfile(fpath):
        print('infile = %s' % os.path.realpath(fpath))
        with open(fpath, 'r') as fh:
            for line in fh:
                line = line.strip()
                st = do_value(sth, line.strip(), live)
                if not st:
                    DO_OK = False
    else:
        print('unexist file = %s' % fpath)
        DO_OK = False

    return DO_OK


#helper for logging
def write_log( msg, logfile='./%s.log' % __file__[:-3]):
    fout = None
    if(logfile):
        fout = open(logfile, 'a'); # append to

    print(msg);
    if fout:
        fout.write(msg + "\n");

def dbinfo(sth):
    sql = 'SELECT @@hostname AS hostname, database() AS dbname, user() AS uname'
    sth.execute(sql)
    rows = sth.fetchone()
    return 'DB host=%s, name=%s, user=%s' % (rows['hostname'], rows['dbname'], rows['uname'])



#===============================
# The main file
if __name__ == "__main__":

    my_name = "Ad hoc script to update seq_unit_qc and portal"
    version = "1.0.0"

    # Parse options
    usage = "*%s, version %s\n" % (my_name, version)

    # command line options
    parser = argparse.ArgumentParser(description=usage, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--file", dest="ifile", help="provided input file of libraries or seq units")
    parser.add_argument("-d", "--data", dest="idata", help="provided a single seq unit name, or library name")
    parser.add_argument("-e", "--exe", dest="exe", action='store_true', help="to execute the action. otherwise, only dry-run")
    parser.add_argument("-v", "--version", action="version", version=version)

    options = parser.parse_args()


    # global shared objects
    db = jgi_connect_db("rqc")


    if db == None:
        print('Error : failed to connect to RQC database');
        sys.exit(1)

    sth = db.cursor(MySQLdb.cursors.DictCursor)

    dbparam = dbinfo(sth);

    print(dbparam)

    status = False

    if options.ifile:
        status = do_file(sth, options.ifile, options.exe)

    elif options.idata:
        status = do_value(sth, options.idata, options.exe)

    if status:
        print('Process OK')
    else:
        print('Process encountered issues')





    db.close()
    sys.exit(0)
