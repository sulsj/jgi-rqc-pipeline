#! /usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys

## custom libs in "../lib/"
# ROOT_DIR = os.path.dirname(os.path.realpath(__file__))
# print ROOT_DIR
# sys.path.append(os.path.join(ROOT_DIR, ".."))
# sys.path.append(os.path.join(ROOT_DIR, 'tools'))     ## ./tools
# sys.path.append(os.path.join(ROOT_DIR, '../lib'))    ## rqc-pipeline/lib
# sys.path.append(os.path.join(ROOT_DIR, '../tools'))  ## rqc-pipeline/tools
#
# from readqc_constants import RQCReadQcConfig, ReadqcStats
# from rqc_constants import RQCExitCodes
# from os_utility import run_sh_command
# from common import append_rqc_stats, append_rqc_file

class RQCExitCodes:
    JGI_SUCCESS = 0
    JGI_FAILURE = 1

class ReadqcStats:
    ILLUMINA_BASE_Q30 = 'base Q30'
    ILLUMINA_BASE_Q25 = 'base Q25'
    ILLUMINA_BASE_Q20 = 'base Q20'
    ILLUMINA_BASE_Q15 = 'base Q15'
    ILLUMINA_BASE_Q10 = 'base Q10'
    ILLUMINA_BASE_Q5 = 'base Q5'

    ILLUMINA_BASE_C30 = 'base C30'
    ILLUMINA_BASE_C25 = 'base C25'
    ILLUMINA_BASE_C20 = 'base C20'
    ILLUMINA_BASE_C15 = 'base C15'
    ILLUMINA_BASE_C10 = 'base C10'
    ILLUMINA_BASE_C5 = 'base C5'

    ILLUMINA_READ_Q30 = 'read Q30'
    ILLUMINA_READ_Q25 = 'read Q25'
    ILLUMINA_READ_Q20 = 'read Q20'
    ILLUMINA_READ_Q15 = 'read Q15'
    ILLUMINA_READ_Q10 = 'read Q10'
    ILLUMINA_READ_Q5 = 'read Q5'

    ILLUMINA_BASE_Q30_SCORE_MEAN = 'Q30 bases Q score mean'
    ILLUMINA_BASE_Q30_SCORE_STD = 'Q30 bases Q score std'
    ILLUMINA_BASE_OVERALL_BASES_Q_SCORE_MEAN = 'overall bases Q score mean'
    ILLUMINA_BASE_OVERALL_BASES_Q_SCORE_STD = 'overall bases Q score std'


def base_level_qual_stats(dataToRecordDict, reformatObqhistFile):
    cummlatPer = 0
    cummlatBase = 0

    statsPerc = {30:0, 25:0, 20:0, 15:0, 10:0, 5:0}
    statsBase = {30:0, 25:0, 20:0, 15:0, 10:0, 5:0}

    Q30_seen = 0
    Q25_seen = 0
    Q20_seen = 0
    Q15_seen = 0
    Q10_seen = 0
    Q5_seen = 0

    ## New format
    ##Median    38
    ##Mean  37.061
    ##STDev 4.631
    ##Mean_30   37.823
    ##STDev_30  1.699
    ##Quality   bases   fraction
    #0  159 0.00008
    #1  0   0.00000
    #2  12175   0.00593
    #3  0   0.00000
    #4  0   0.00000
    #5  0   0.00000
    #6  0   0.00000

    allLines = open(reformatObqhistFile).readlines()
    for l in allLines[::-1]:
        l = l.strip()
        
        ##
        ## obqhist file format example
        ##
        # #Median	36
        # #Mean	33.298
        # #STDev	5.890
        # #Mean_30	35.303
        # #STDev_30	1.517
        # #Quality	bases	fraction
        # 0	77098	0.00043
        # 1	0	0.00000
        # 2	0	0.00000
        # 3	0	0.00000
        # 4	0	0.00000
        # 5	0	0.00000
        # 6	0	0.00000

        if len(l) > 0:
            if l.startswith("#"):
                if l.startswith("#Mean_30"):
                    dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q30_SCORE_MEAN] = l.split('\t')[1]

                elif l.startswith("#STDev_30"):
                    dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q30_SCORE_STD] = l.split('\t')[1]

                elif l.startswith("#Mean"):
                    dataToRecordDict[ReadqcStats.ILLUMINA_BASE_OVERALL_BASES_Q_SCORE_MEAN] = l.split('\t')[1]

                elif l.startswith("#STDev"):
                    dataToRecordDict[ReadqcStats.ILLUMINA_BASE_OVERALL_BASES_Q_SCORE_STD] = l.split('\t')[1]

                continue


            qavg = None
            nbase = None
            percent = None

            t = l.split()

            try:
                qavg = int(t[0])
                nbase = int(t[1])
                percent = float(t[2])
            except IndexError:
                print("parse error in base_level_qual_stats: %s %s %s %s" % (l, qavg, nbase, percent))
                continue

            print("base_level_qual_stats(): qavg and nbase and percent: %s %s %s" % (qavg, nbase, percent))
            # if not (qavg and nbase and percent): continue
            # if nbase == 0 and percent == 0.0: continue

            cummlatPer += percent * 100.0
            cummlatPer = float("%.f" % (cummlatPer))
            if cummlatPer > 100: cummlatPer = 100.0 ## RQC-621
            cummlatBase += nbase

            if qavg == 30:
                Q30_seen = 1
                statsPerc[30] = cummlatPer
                statsBase[30] = cummlatBase

                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q30] = cummlatPer
                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_C30] = cummlatBase

            elif qavg == 25:
                Q25_seen = 1
                statsPerc[25] = cummlatPer
                statsBase[25] = cummlatBase

                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q25] = cummlatPer
                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_C25] = cummlatBase

            elif qavg == 20:
                Q20_seen = 1
                statsPerc[20] = cummlatPer
                statsBase[20] = cummlatBase

                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q20] = cummlatPer
                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_C20] = cummlatBase

            elif qavg == 15:
                Q15_seen = 1
                statsPerc[15] = cummlatPer
                statsBase[15] = cummlatBase

                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q15] = cummlatPer
                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_C15] = cummlatBase

            elif qavg == 10:
                Q10_seen = 1
                statsPerc[10] = cummlatPer
                statsBase[10] = cummlatBase

                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q10] = cummlatPer
                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_C10] = cummlatBase

            elif qavg == 5:
                Q5_seen = 1
                statsPerc[5] = cummlatPer
                statsBase[5] = cummlatBase

                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q5] = cummlatPer
                dataToRecordDict[ReadqcStats.ILLUMINA_BASE_C5] = cummlatBase

    ## Double check that no value is missing.
    if Q25_seen == 0 and Q30_seen != 0:
        Q25_seen = 1
        statsPerc[25] = statsPerc[30]
        statsBase[25] = statsBase[30]
        dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q25] = cummlatPer
        dataToRecordDict[ReadqcStats.ILLUMINA_BASE_C25] = cummlatBase

    if Q20_seen == 0 and Q25_seen != 0:
        Q20_seen = 1
        statsPerc[20] = statsPerc[25]
        statsBase[20] = statsBase[25]
        dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q20] = cummlatPer
        dataToRecordDict[ReadqcStats.ILLUMINA_BASE_C20] = cummlatBase

    if Q15_seen == 0 and Q20_seen != 0:
        Q15_seen = 1
        statsPerc[15] = statsPerc[20]
        statsBase[15] = statsBase[20]
        dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q15] = cummlatPer
        dataToRecordDict[ReadqcStats.ILLUMINA_BASE_C15] = cummlatBase

    if Q10_seen == 0 and Q15_seen != 0:
        Q10_seen = 1
        statsPerc[10] = statsPerc[15]
        statsBase[10] = statsBase[15]
        dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q10] = cummlatPer
        dataToRecordDict[ReadqcStats.ILLUMINA_BASE_C10] = cummlatBase

    if Q5_seen == 0 and Q10_seen != 0:
        Q5_seen = 1
        statsPerc[5] = statsPerc[10]
        statsBase[5] = statsBase[10]
        dataToRecordDict[ReadqcStats.ILLUMINA_BASE_Q5] = cummlatPer
        dataToRecordDict[ReadqcStats.ILLUMINA_BASE_C5] = cummlatBase

    if Q30_seen == 0:
        print("Q30 is 0. Base quality values are ZERO.")

    print("Q and C values: %s" % (dataToRecordDict))


    return RQCExitCodes.JGI_SUCCESS
    
    
if __name__ == "__main__":

    # inFile = sys.argv[1]
    inFile = "obqhist.txt"
    outDict = {}
    
    ret = base_level_qual_stats(outDict, inFile)
    
    print ret, outDict
    
    
    
## EOF