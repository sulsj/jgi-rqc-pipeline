#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import sys
import argparse
import logging
import platform
import gzip
import cPickle as pickle
# from collections import OrderedDict
import sqlite3 as lite
from cogent.parse.ncbi_taxonomy import NcbiTaxonomyFromFiles
from cogent.db.ncbi import ids_to_taxon_ids, taxon_ids_to_lineages, taxon_ids_to_names_and_lineages


from os_utility import get_tool_path, run_sh_command, make_dir_p, change_mod, back_ticks


NCBI_TAXONOMY_DB_PATH = "/global/projectb/sandbox/rqc/qcdb/taxonomy/tax/DEFAULT/"
#NCBI_TAXONOMY_DB_PATH = "/global/projectb/scratch/sulsj/2016.02.03-run_blastplus_py-test/gi2taxid-db"

"""
Returns lineage information in my_ranks order
"""
def get_lineage(node):
    ranks = ['superkingdom','phylum','class','order','family','genus','species']
    ranksLookup = dict([(r,idx) for idx,r in enumerate(ranks)])
    lineage = [""] * len(ranks)
    curr = node
    while curr.Parent is not None:
        if curr.Rank in ranksLookup:
            lineage[ranksLookup[curr.Rank]] = curr.Name
            #print curr.Name, curr.Rank
        curr = curr.Parent

    return lineage
    

if __name__ == '__main__':

    blastOutputFile = sys.argv[1]

    ##
    ## Collect unique gi
    ##
    print("Creating gi list...")
    taxonomyInfoDict = {} ## unique gi, cnt, lineage
    totalTophits = 0
    with open(blastOutputFile + ".tophit", 'r') as TOPFH:
        for l in TOPFH:
            if l.startswith('#'): continue
            # (query, subject, expect, length, percId, qLength, sLength, staxid) = l.rstrip().split() ## staxid is not used for now.
            
            #query	subject	score	expect	length	%id	q_start	q_stop	q_length	qstrand	s_start	s_stop	s_length	s_strand            
            toks = l.rstrip().split()            
            subject = toks[1]
            gi = 0
            try:
                gi = int(subject.split('|')[1]) ## gi|851289456|ref|NZ_AXDV01000017.1| or gi|472256744| gi|461490773| gi|71143482| gi|461490773|
            except:
                print("Non digit gi number found")
                continue

            ## Count the number of reads with identical gi
            if gi not in taxonomyInfoDict:
                taxonomyInfoDict[gi] = {} ## new entry
                taxonomyInfoDict[gi]['cnt'] = 1
            else:
                taxonomyInfoDict[gi]['cnt'] += 1

            totalTophits += 1

    print("Number of gis collected: %s" % (len(taxonomyInfoDict)))
    print taxonomyInfoDict


    ##
    ## Generate taxonomy lineage
    ##

    ## Load ncbi taxonomy tree and gi2taxid
    print("Loading taxonomy tree...")
    tree = None
    ncbiTaxTreePickled = os.path.join(NCBI_TAXONOMY_DB_PATH, "ncbitaxonomy.pickled")
    ncbiGi2TaxidPickled = os.path.join(NCBI_TAXONOMY_DB_PATH, "gi2taxid_nucl.pickled")
    ncbiGi2TaxidSqlite = os.path.join(NCBI_TAXONOMY_DB_PATH, "gi2taxid_nucl.db")
    ncbiNodesDmpTaxTreeDump = os.path.join(NCBI_TAXONOMY_DB_PATH, "nodes.dmp")
    ncbiNamesDmpTaxTreeDump = os.path.join(NCBI_TAXONOMY_DB_PATH, "names.dmp")
    ncbiGiTaxidNuclDump = os.path.join(NCBI_TAXONOMY_DB_PATH, "gi_taxid_nucl.dmp.gz")
    assert os.path.isfile(ncbiNodesDmpTaxTreeDump), "Cannot find %s" % ncbiNodesDmpTaxTreeDump
    assert os.path.isfile(ncbiNamesDmpTaxTreeDump), "Cannot find %s" % ncbiNamesDmpTaxTreeDump
    assert os.path.isfile(ncbiGiTaxidNuclDump), "Cannot find %s" % ncbiGiTaxidNuclDump


    ## Load ncbi taxonomy tree. If the pickle found, load it. Or create a pickle.
    if not os.path.isfile(ncbiTaxTreePickled):
        tree = NcbiTaxonomyFromFiles(open(ncbiNodesDmpTaxTreeDump), open(ncbiNamesDmpTaxTreeDump))
        pickle.dump(tree, open(ncbiTaxTreePickled  + ".tmp", "wb"))
        change_mod(ncbiTaxTreePickled + ".tmp", "0644")
        back_ticks(['mv', ncbiTaxTreePickled + ".tmp", ncbiTaxTreePickled])
        print("Created pickled taxonomy tree: %s" % (ncbiTaxTreePickled))
    else:
        tree = pickle.load(open(ncbiTaxTreePickled, "rb"))
        print("Found and loaded pickled taxonomy tree from %s" % (ncbiTaxTreePickled))

    assert tree != None
    print("Taxonomy tree loaded")

    
    con = None
    cur = None

    if not os.path.isfile(ncbiGi2TaxidSqlite):
        con = lite.connect(ncbiGi2TaxidSqlite)
        assert con
        cur = con.cursor()
        cur.execute("PRAGMA cache_size = 1000000")  #1GB
        # cur.execute("PRAGMA journal_mode = OFF")
        # cur.execute("PRAGMA synchronous = OFF")
        # cur.execute("PRAGMA temp_store = MEMORY")

        print("Creating gi2taxid db...")
        
        with gzip.open(ncbiGiTaxidNuclDump, 'r') as F:
            cur.execute("DROP TABLE IF EXISTS gi2taxid;")
            cur.execute("CREATE TABLE gi2taxid(gi INT PRIMARY KEY, taxid INT);")

            ##https://github.com/darcyabjones/gi-to-tax/blob/master/gi2tax.py
            def gi_taxid_generator(fHandle):
                ## returns an iterable generator to write nodes with.
                for l in fHandle:
                    toks = l.rstrip().split()
                    gi = int(toks[0])
                    taxId = int(toks[1])
                    yield (gi, taxId)                
                return

            cur.executemany(("INSERT INTO gi2taxid(gi, taxid) VALUES (?, ?)"), gi_taxid_generator(F))                
            con.commit()

        print("Creating index...")
        cur.execute("CREATE INDEX gi_index ON gi2taxid (gi);")


    ## Get the lineage
    print("Finding lineage...")
    assert os.path.isfile(ncbiGi2TaxidSqlite)
    con = lite.connect(ncbiGi2TaxidSqlite)
    assert con
    cur = con.cursor()
    ranks = ['superkingdom','phylum','class','order','family','genus','species']
    ranksLookup = dict([(r,idx) for idx,r in enumerate(ranks)])
    
    for gi,v in taxonomyInfoDict.iteritems():
        node = None
        try:
            # node = tree.ById[gi2taxid[gi]]            
            cur.execute("SELECT taxid FROM gi2taxid WHERE gi=:gi", {"gi": int(gi)})
            row = cur.fetchone()
            if row is None: print "tax id not found for gi = ", gi
            else: 
                node = tree.ById[int(row[0])]

                if node is None:                     
                    log.error("Cannot find tax tree node for gi=%s, taxid=%s" % (gi, row[0]))
                    # print "gi, taxid = ", gi, row[0]
                else:
                    ##print gi, ids_to_taxon_ids([str(gi)])
                    ##if ids_to_taxon_ids([str(gi)]): print list(taxon_ids_to_names_and_lineages(ids_to_taxon_ids([str(gi)])))
                    
                    lineage = [""] * len(ranks)
                    curr = node
                    while curr.Parent is not None:
                        if curr.Rank in ranks: 
                            # print curr.Name, curr.Rank
                            lineage[ranksLookup[curr.Rank]] = curr.Name
                        curr = curr.Parent

                    v['lineage'] = ";".join(lineage)
                    v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0 ## compute percentage of tophits
                    
                    print gi, row[0], lineage, a
                
        except KeyError:
            v['lineage'] = ""
            v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0 ## compute percentage of tophits
            # continue

    if con: con.close()

    ## Create taxlist file
    ## ex
    ## #name    numHits pctHit  pctQuery    taxonomy
    ## Methanolacinia paynteri  540 94.24   0.00    Archaea;Euryarchaeota;Methanomicrobia;Methanomicrobiales;Methanomi crobiaceae;Methanolacinia;Methanolacinia paynteri
    ## Methanosarcina mazei 28  4.89    0.00    Archaea;Euryarchaeota;Methanomicrobia;Methanosarcinales;Methanosar cinaceae;Methanosarcina;Methanosarcina mazei
    ## Methanobrevibacter smithii   3   0.52    0.00    Archaea;Euryarchaeota;Methanobacteria;Methanobacteriales;Methanobacteriaceae;Methanobrevibacter;Methanobrevibacter smithii
    ## Methanobrevibacter smithii   1   0.17    0.00    Archaea;Euryarchaeota;Methanobacteria;Methanobacteriales;Methanobacteriaceae;Methanobrevibacter;Methanobrevibacter smithii
    with open(blastOutputFile + ".taxlist", 'w') as TLFH:
        TLFH.write("#name\tnumHits\tpctHit\tpctQuery\ttaxonomy\n")
        for gi in sorted(taxonomyInfoDict.keys(), key=lambda y: (taxonomyInfoDict[y]['cnt']), reverse=True): ## sort by cnt
            if 'lineage' in taxonomyInfoDict[gi] and taxonomyInfoDict[gi]['lineage'] != "":
                ## For backward compatibility, add unused field, pctQuery
                # name  numHits pctHit  pctQuery    taxonomy
                TLFH.write("%s\t%s\t%.2f\t0.00\t%s\n" % (taxonomyInfoDict[gi]['lineage'].split(';')[-1], 
                                                         taxonomyInfoDict[gi]['cnt'], 
                                                         taxonomyInfoDict[gi]['perc'], 
                                                         taxonomyInfoDict[gi]['lineage']))


    print("Taxlist created.")
    print("run_blastplus.py completed.")
    
    
## EOF