#!/usr/bin/env python

import os
import gzip
from cogent.parse.ncbi_taxonomy import NcbiTaxonomyFromFiles
import cPickle as pickle

#NCBI_TAXDB = "/global/projectb/sandbox/rqc/qcdb/taxonomy/tax/DEFAULT"
#NCBI_TAXDB = "./"
tree = None
if not os.path.isfile("ncbitaxonomy.pickled"):
    tree = NcbiTaxonomyFromFiles(open('/global/projectb/sandbox/rqc/qcdb/taxonomy/tax/DEFAULT/nodes.dmp'),     open('/global/projectb/sandbox/rqc/qcdb/taxonomy/tax/DEFAULT/names.dmp'))

    pickle.dump(tree, open("ncbitaxonomy.pickled", "wb"))
else:
    print 'Found pickle1'
    tree = pickle.load(open("ncbitaxonomy.pickled", "rb"))

assert tree != None
print 'Tax tree loaded'

root = tree.Root

ranks = ['superkingdom','phylum','class','order','family','genus','species']

def get_lineage(node, my_ranks):
    """returns lineage information in my_ranks order"""
    ranks_lookup = dict([(r,idx) for idx,r in enumerate(my_ranks)])
    lineage = [None] * len(my_ranks)
    curr = node
    while curr.Parent is not None:
        if curr.Rank in ranks_lookup:
            lineage[ranks_lookup[curr.Rank]] = curr.Name
        curr = curr.Parent

    return lineage

gi2taxid = None    
if not os.path.isfile("gi2taxid.pickled"):
    gi2taxid = {}
    with gzip.open("/global/projectb/sandbox/rqc/qcdb/taxonomy/tax/DEFAULT/gi_taxid_nucl.dmp.gz", 'r') as F:
        for l in F:
            (key, val) = l.rstrip().split()
            gi2taxid[int(key)] = int(val)
    pickle.dump(gi2taxid, open("gi2taxid.pickled", "wb"))
else:
    print 'Found pickle2'
    gi2taxid = pickle.load(open("gi2taxid.pickled", "rb"))

assert gi2taxid != None
print 'gi2taxid loaded'


            
node = tree.ById[192150] # some taxon id...
print get_lineage(node, ranks)

print gi2taxid[851289456]
node = tree.ById[gi2taxid[851289456]] # some taxon id...
print get_lineage(node, ranks)