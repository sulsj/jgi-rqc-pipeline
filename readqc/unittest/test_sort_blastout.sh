#!/bin/bash

cat $1 | sort -t$'\t' -k1,1 -k3,3n | 
gawk -F'[\t]' '!/^#/{
s=gensub(" ","  ",1,$2); # creates new column:=3
t=gensub(" ","_","g",s)
a[$1]=t" "$4" "$5" "$6" "$9" "$13}
END{ for(e in a) print e,a[e] }' | sort -k 4,4n -k 6,6n