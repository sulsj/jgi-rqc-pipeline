#!/bin/bash -l

#set -e

# Read qc pipeline tests

# How to compare the results: jgi-rqc/rqc_system/test/compare.py -rf [list of output folders] -p [pacbio|raed|filter|assemblyqc]

OUTDIR="/global/projectb/sandbox/rqc/analysis/qc_user/readqc"
DATE=`date +"%Y%m%d"`

#dev for sulsj
cmd="/global/homes/s/sulsj/work/bitbucket-repo/jgi-rqc-pipeline/readqc/readqc.py"

# DNA
SEQUNIT="7257.1.64419.CACATTGTGAG"
qsub -cwd -b yes -j yes -m as -now no -w e -N readqc-test-$SEQUNIT -l h_rt=43195 -P gentech-rqc.p -o $OUTDIR/readqc-$SEQUNIT-$DATE.log "$cmd --output-path $OUTDIR/$SEQUNIT-$DATE --fastq $OUTDIR/data/$SEQUNIT.fastq.gz --skip-cleanup"

# DNA
SEQUNIT="7378.1.69281.CGATG"
qsub -cwd -b yes -j yes -m as -now no -w e -N readqc-test-$SEQUNIT -l h_rt=43195 -P gentech-rqc.p -o $OUTDIR/readqc-$SEQUNIT-$DATE.log "$cmd --output-path $OUTDIR/$SEQUNIT-$DATE --fastq $OUTDIR/data/$SEQUNIT.fastq.gz --skip-cleanup"

# RNA
SEQUNIT="7365.2.69553.AGTTCC"
qsub -cwd -b yes -j yes -m as -now no -w e -N readqc-test-$SEQUNIT -l h_rt=43195 -P gentech-rqc.p -o $OUTDIR/readqc-$SEQUNIT-$DATE.log "$cmd --output-path $OUTDIR/$SEQUNIT-$DATE --fastq $OUTDIR/data/$SEQUNIT.fastq.gz --skip-cleanup"

# Pooled
SEQUNIT="7616.1.77799"
qsub -cwd -b yes -j yes -m as -now no -w e -N readqc-test-$SEQUNIT -l h_rt=43195 -P gentech-rqc.p -o $OUTDIR/readqc-$SEQUNIT-$DATE.log "$cmd --output-path $OUTDIR/$SEQUNIT-$DATE --fastq $OUTDIR/data/$SEQUNIT.fastq.gz --skip-cleanup"

# dual index
SEQUNIT="8339.1.98499.AGGCAGA-TATCCTC"
qsub -cwd -b yes -j yes -m as -now no -w e -N readqc-test-$SEQUNIT -l h_rt=43195 -P gentech-rqc.p -o $OUTDIR/readqc-$SEQUNIT-$DATE.log "$cmd --output-path $OUTDIR/$SEQUNIT-$DATE --fastq $OUTDIR/data/$SEQUNIT.fastq.gz --skip-cleanup"

# single
SEQUNIT="9180.1.123886.ACTCT"
qsub -cwd -b yes -j yes -m as -now no -w e -N readqc-test-$SEQUNIT -l h_rt=43195 -P gentech-rqc.p -o $OUTDIR/readqc-$SEQUNIT-$DATE.log "$cmd --output-path $OUTDIR/$SEQUNIT-$DATE --fastq $OUTDIR/data/$SEQUNIT.fastq.gz --skip-cleanup"

# nextera
SEQUNIT="8871.2.114463.CCGTCC"
qsub -cwd -b yes -j yes -m as -now no -w e -N readqc-test-$SEQUNIT -l h_rt=43195 -P gentech-rqc.p -o $OUTDIR/readqc-$SEQUNIT-$DATE.log "$cmd --output-path $OUTDIR/$SEQUNIT-$DATE --fastq $OUTDIR/data/$SEQUNIT.fastq.gz --skip-cleanup"

# smRNA
SEQUNIT="8520.1.103218.GTTTC"
qsub -cwd -b yes -j yes -m as -now no -w e -N readqc-test-$SEQUNIT -l h_rt=43195 -P gentech-rqc.p -o $OUTDIR/readqc-$SEQUNIT-$DATE.log "$cmd --output-path $OUTDIR/$SEQUNIT-$DATE --fastq $OUTDIR/data/$SEQUNIT.fastq.gz --skip-cleanup --cut 20"
