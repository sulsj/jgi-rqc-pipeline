#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
read qc pipeline

Original command ex
     $ bin/run_rqc_batch.pl -force -read -i seq_units.queue -mem 40G -run-type sge
     -max-job 50 -email "ssul@lbl.gov" -no-track -time-limit-hrs 12:00:00

     (cluster run)
     $ bin/run_rqc_illumina_pipeline.pl -run-type sge -seq-unit 7216.1.62965.GTATCCATGCG.srf -queue normal.c -read

     (local run)
     $ bin/run_rqc_illumina_pipeline.pl -run-type local -seq-unit 7216.1.62965.GTATCCATGCG.srf -read


Command
    $ readqc.py --fastq 6145.5.40009.fastq --output-path in-progress/00/00/00/05/

Outputs
  - stats_log = readqc_stats.txt
  - file_log = readqc_files.txt
  - status_log = readqc_status.log


Created: Jul 24 2013

sulsj (ssul@lbl.gov)


Revision:

    20131025 5.0.0: Add multiplex stat analysis
    20131101 5.0.1: Add Percentages of Indexing Tags, index detection plot and stats
    20131105 5.0.1: Released!
    20131106 5.0.2: Add do_end_of_read_illumina_adapter_check() step (STEP15)
    20131203 5.0.3: RQC-319: reverted to no-cut version for common comtam analysis and added 50bp cutting only to artifacts analysis
    20130404 5.0.4: RQC-383 note for the new Illumina adapter check script
    20130410 5.0.5: Add optional parameters: libName, isRna, isMultiplex
    20130414 5.0.6: Add blastDbPath option for localizing files
    20130414 5.0.7: Use get_tool_path for bwa and gnuplot
    20130415 5.0.8: Cleanup; os_utility2.py
    20130415 5.0.8: Released!

    20140519 5.1.0: Separating computation parts from report to readqc pipeline

    20140610 5.2.0: Add insert size analysis by bbmerge.sh in bbtools
    20140618 5.2.1: Test sciclone analysis by bbduk.sh in bbtools
    20140625 5.2.2: Add option to skip the blast search step
    20140722 5.2.2: Released!

    20140722 5.3.0: Add bbcountunique for 20mer counting

    20140723 5.4.0: Add blast database localization and option to skip localization

    20140723 5.4.1: Remove read num counting in step 13

    20140724 5.5.0: Replace megablast with Blastn

    20140814 5.6.0: Step 1 reformat.sh subsampling; Step 4 updated using the outputs from reformat.sh

    20140821 5.7.0: Step 3 updated using the outputs from reformat.sh (gchist)

    20140821 5.8.0: Step 5 updated using the outputs from reformat.sh (obqhist)

    20140826 5.9.0: Step 6 updated using the outputs from reformat.sh (obqhist); disable stat file gen from reformat.sh in step 13

    20140917 5.10.0: Divided Blast step into three separate steps (Step13 --> Step13, 14, 15), Updated Step17 to generate D3 plot in the pipeline;

    20140929 5.11.0: Replaced zcat with pigz -d -c; Replaced separate_paired_end_fq() in readqc_util.py with reformat.sh; Removed "read.qual" generation in Step14,15; Removed gnuplot in Step 16.

    20141001 6.0.0: Released!
    20141002 6.0.1: Removed fq2fa.pl in step4 and step12;

    20141002 6.1.0: Updated sciclone analysis using bbduk;
    20141006 6.1.1: Updated insert size analysis to cope with the case that bbmerge found no solution;

    20141009 6.2.0: Clean up tools and constants;

    20141009 6.3.0: Replaced findDedupes.sh with dedupe.sh in bbtools;
    20141014 6.3.0: Released!
    20141014 6.3.1: Replaced bwa with "dedupe.sh out=null qin=33 ow=t s=0 ftr=49 ac=f int=f";
    20141103 6.3.2: Updated bbduk option to have "statscolumns=3";
    20141104 6.3.3: Updated gc stats gen to parse mode and med values from *.gchist file;
    20141106 6.3.4: Removed os_utility2.py;
    20141211 6.3.5: Added synbio pipeline files; Added fastq_filter2.py; Added "gcplot" flag for reformat.sh; Added cumulative read % for gchist output;
    20150223 6.4.6: Added timeout for megan (timeout -v -t 1800s megan.... ); MySql host was set to gpdb09;
    20150226 6.3.6: Released!
    20152027 6.3.7: Added read%, GC%, and read count tooltip labels in the readqc gc histogram plot (RQCSUPPORT-375);
    20150303 6.3.8: Replaced duk with bbduk for Step11 read contam analysis; Megan removed;
    20150304 6.3.8: Released!

    20150317 6.4.0: Added localize_file() in rqc_utility.py; Localize ref DBs in Step 11, 12, and 17;
    20150327 6.4.1: Fixed bbduk contam detection (RQCSUPPORT-467)

    20150429 6.5.0: Added GC divergence analysis (step19)
    20150616 6.5.1: Updated GC divergence with "module load jgi-fastq-signal-processing/1.x"
    20150625 6.5.2: Added mem usage stat to run_sh_command()
    20150701 6.5.2: Released!

    20150720 6.6.0: Added timeout for blast (timeout -v -t 21600s)
    20150722 6.6.1: Removed kmer option (--kmer 63)
    20150724 6.6.1: Released!
    20150728 6.7.0: Added option to skip the Blast against Refseq/NT step (Step 14/15): --skip-blast-refseq/--skip-blast-nt

    20150916 7.0.0: Cleaned up tools; Updated *_constants.py
    20151005 7.0.1: Skipped the step 16 illumina_generate_index_sequence_detection_plot if there is no index seq
    20151005 7.0.1: Released!
    20151008 7.0.2: Replaced run_blastn() with run_blast.pl using Jigsaw
    20151016 7.0.3: Added module load jigsaw for run_blast.pl
    20151021 7.0.3: Released! c52e0b8
    20151022 7.0.4: Updated blast step
    20151028 7.0.5: Changed ref db locations from /dna/shared/rqc/ref_databases to /global/dna/shared/rqc/ref_databases for Mendel+

    20151103 7.1.0: Updated to do the Blast search against refseq.archaea and refseq.bacteria instead of refseq.microbial
    20151103 7.1.0: Released! b237c34
    20151109 7.1.1: Updated readqc for dealing 20bp cutting for smRNA

    20151111 7.2.0: Cleanup; Addded 20bp contamination stat for smRNA
    20151111 7.2.0: Released! 0585e15
    20151207 7.2.1: Updated readqc for Seal test instead of bbduk
    20160203 7.2.2: Updated to use /scratch/blastdb

    20160210 7.3.0: Replaced Jigsaw's run_blast.pl with run_blastplus.py
    20160316 7.3.1: Fixed base qual analysis
    20160405 7.3.2: Updated blast parsed output line counting
    20160414 7.3.3: Added pooled analysis option (-p)
    20160420 7.3.4: Added top100 blast hit file
    20160525 7.3.5: Added non-synthetic contamination analysis; Removed bbduk for contamination analysis;
    20160601 7.3.6: Adjusted seal.sh params for non-synthetic contamination analysis
    20160603 7.3.7: Added synthetic contam and adapter contamination analysis

    20160711 7.3.8: Removed e.coli from contamination analysis
    20160722 7.3.9: Added contamination stats to readqc_stats.txt per contam db
    20160729 7.3.10: Updated contam stat name by adding 'contam:' as prefix
    20160810 7.3.11: Chnaged to k=25 for unique 20mer analysis
    20160815 7.3.12: Updated mer uniqueness plotting for 20/25 mer setting
    20160823 7.3.13: Added shuffling for mer uniqueness analysis; Released 399fb8e, 2909dbc
    20160825 7.3.14: Split suffling and bbcountunique due to memory issue; Released 71148d9
    20160829 7.3.15: Reverted not to run shuffling for mer uniqueness analysis;
    20160831 7.3.16: Updated with new localize_dir2()
    20160907 7.3.17: uniqueness failed due to assertion (line 315, 413 in readqc_utils.py; 199 in readqc_report.py)
    20160920 7.3.18: Subsampling by get_subsample_rate() in common.py

    20160923 7.4.0: Updated uniq mer analysis step
    20161121 7.4.1: Updated to correctly parse reformat.sh log file
    20170119 7.4.2: Updated to save only <10 contam stats with "gi" info only

    20170407 7.5.0: Added jgi-fastq-signal-processing scripts for gc divergence analysis (only for Denovo)

    20170427 8.0.0: Updated to run on Denovo; Updated RQCReadQcCommands in readqc_constants.py; Updated run_blastplus.py with checking hostname;
                    Updated get_tool_path() with checking hostname;
    20170511 8.0.1: Added Cori support;

    20170531 8.1.0: Updated to use run_blastplus_taxserver.py
    20170705 8.2.0: Updated to use max 10,000 index sequences to create demultiplex plot
    20170906 8.2.1: Fixed skip blast option

    20170908 8.3.0: Added sketch vs refseq, nt, silva
    20170911 8.3.1: Manually add -Xmx23G to seal.sh in contam detection and dedupe
    20171011 8.3.2: Added printtaxa=t to sendsketch.sh
    20171020 8.3.3: Added depth depth2 to sendsketch.sh
    20171106 8.3.4: Added unique2 and merge to sendsketch.sh

    20180105 8.3.5: Updated contam db (ARTIFACT_FILE_NO_SPIKEIN)
    20180223 8.3.6: Fixed the stat value, "q20 read 1" detection
    20180404 8.3.7: Added read_count2 to count reads from hudson alpha data
    20180430 8.3.8: Manually set the permission to ldpaths

    20180501 8.4.0: Updated to use sulsj/rocker:latest Rocker container for Rscript
    20180605 8.4.1: Dockerized!
    20180614 8.4.2: Updated get_working_read_length() @ rqc_fastq.py for Hudson data
    20180626 8.4.3: Exit with 128 if there is out-of-mem; Updated to record java.lang.OutOfMemoryError to GLOBAL_LOG;
    20180706 8.4.4: add kapa spikein detetion (RQC-1117)

    20180711 8.5.0: Separate kapa step



"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use
import os
import sys
import argparse

## custom libs in "../lib/"
SRC_DIR = os.path.dirname(__file__)
sys.path.append(os.path.join(SRC_DIR, "tools"))    ## rqc-pipeline/readqc/tools
sys.path.append(os.path.join(SRC_DIR, "../lib"))   ## rqc-pipeline/lib
sys.path.append(os.path.join(SRC_DIR, "../tools")) ## rqc-pipeline/tools

from readqc_constants import RQCReadQcConfig, RQCReadQc, ReadqcStats, RQCReadQcReferenceDatabases
from common import get_logger, get_status, append_rqc_stats, append_rqc_file, set_colors, get_subsample_rate
from readqc_utils import *
from rqc_utility import safe_basename
from rqc_fastq import get_working_read_length, read_count2
from readqc_report import *
from os_utility import get_tool_path
from db_access import jgi_connect_db    # for well_id look up when doing kapa spikein

VERSION = "8.5.0"
LOG_LEVEL = "DEBUG"
SCRIPT_NAME = __file__

color = {}
color = set_colors(color, True)


"""
STEP1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_fast_subsample_fastq_sequences(fastq, skipSubsampling, log):
    log.info("\n\n%sSTEP1 - Subsampling reads <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    statsFile = RQCReadQcConfig.CFG["stats_file"]

    status = "1_illumina_readqc_subsampling in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    inputReadNum = 0
    sampleRate = 0.0

    if not skipSubsampling:
        # sampleRate = RQCReadQc.ILLUMINA_SAMPLE_PERCENTAGE    ## 0.01
        # inputReadNum = read_count(fastq) ## read count from the original fastq
        inputReadNum = read_count2(fastq, log) ## read count from the original fastq
        assert inputReadNum > 0, "ERROR: invalid input fastq"
        sampleRate = get_subsample_rate(inputReadNum)
        log.info("Subsampling rate = %s", sampleRate)
    else:
        log.info("1_illumina_readqc_subsampling: skip subsampling. Use all the reads.")
        sampleRate = 1.0

    retCode = None
    totalReadNum = 0
    firstSubsampledFastqFileName = ""

    sequnitFileName, _ = safe_basename(fastq, log)
    sequnitFileName = sequnitFileName.replace(".fastq", "").replace(".gz", "")

    firstSubsampledFastqFileName = sequnitFileName + ".s" + str(sampleRate) + ".fastq"

    retCode, firstSubsampledFastqFileName, totalBaseCount, totalReadNum, subsampledReadNum, bIsPaired, readLength = fast_subsample_fastq_sequences(fastq, firstSubsampledFastqFileName, sampleRate, True, log)

    append_rqc_stats(statsFile, "SUBSAMPLE_RATE", sampleRate, log)

    if retCode in (RQCExitCodes.JGI_FAILURE, -2):
        status = "1_illumina_readqc_subsampling failed"
        log.info(status)
        checkpoint_step_wrapper(status)

    else:
        append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_BASE_COUNT, totalBaseCount, log)
        append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_COUNT, totalReadNum, log)

        status = "1_illumina_readqc_subsampling complete"
        log.info(status)
        checkpoint_step_wrapper(status)


    return status, firstSubsampledFastqFileName, totalReadNum, subsampledReadNum, bIsPaired, readLength


"""
STEP2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_write_unique_20_mers(fastq, totalReadCount, log):
    log.info("\n\n%sSTEP2 - Sampling unique 25 mers <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    if totalReadCount >= RQCReadQc.ILLUMINA_MER_SAMPLE_REPORT_FRQ * 2: ## 25000
        log.debug("read count total in step2 = %s", totalReadCount)
        filesFile = RQCReadQcConfig.CFG["files_file"]
        statsFile = RQCReadQcConfig.CFG["stats_file"]

        status = "2_unique_mers_sampling in progress"
        log.info(status)
        checkpoint_step_wrapper(status)

        retCode, newDataFile, newPngPlotFile, newHtmlPlotFile = write_unique_20_mers(fastq, log)

        if retCode != RQCExitCodes.JGI_SUCCESS:
            status = "2_unique_mers_sampling failed"
            log.error(status)
            checkpoint_step_wrapper(status)

        else:
            ## if no output files, skip this step.
            if newDataFile is not None:
                statsDict = {}

                ## in readqc_report.py
                ## 2014.07.23 read_level_mer_sampling is updated to process new output file format from bbcountunique
                log.info("2_unique_mers_sampling: post-processing the bbcountunique output file.")
                read_level_mer_sampling(statsDict, newDataFile, log)

                for k, v in statsDict.items():
                    append_rqc_stats(statsFile, k, str(v), log)

                ## outputs from bbcountunique
                append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_20MER_UNIQUENESS_TEXT, newDataFile, log)
                append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_20MER_UNIQUENESS_PLOT, newPngPlotFile, log)
                append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_20MER_UNIQUENESS_D3_HTML_PLOT, newHtmlPlotFile, log)

            status = "2_unique_mers_sampling complete"
            log.info(status)
            checkpoint_step_wrapper(status)

    else:
        ## if num reads < RQCReadQc.ILLUMINA_MER_SAMPLE_REPORT_FRQ = 25000
        ## just proceed to the next step
        log.warning("2_unique_mers_sampling can't run it because the number of reads < %s.", RQCReadQc.ILLUMINA_MER_SAMPLE_REPORT_FRQ * 2)
        status = "2_unique_mers_sampling complete"
        checkpoint_step_wrapper(status)


    return status


"""
STEP3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_illumina_read_gc(fastq, log):
    log.info("\n\n%sSTEP3 - Making read GC histograms <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCReadQcConfig.CFG["files_file"]
    statsFile = RQCReadQcConfig.CFG["stats_file"]

    status = "3_illumina_read_gc in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    reformat_gchist_file, png_file, htmlFile, mean_val, stdev_val, med_val, mode_val = illumina_read_gc(fastq, log)

    if not reformat_gchist_file:
        status = "3_illumina_read_gc failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_GC_MEAN, mean_val, log)
        append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_GC_STD, stdev_val, log)
        append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_GC_MED, med_val, log)
        append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_GC_MODE, mode_val, log)

        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_GC_TEXT, reformat_gchist_file, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_GC_PLOT, png_file, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_GC_D3_HTML_PLOT, htmlFile, log)

        status = "3_illumina_read_gc complete"
        log.info(status)
        checkpoint_step_wrapper(status)


    return status


"""
STEP4 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_read_quality_stats(fastq, log):
    log.info("\n\n%sSTEP4 - Analyzing read quality <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCReadQcConfig.CFG["files_file"]
    statsFile = RQCReadQcConfig.CFG["stats_file"]

    status = "4_illumina_read_quality_stats in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    readLength = 0
    readLenR1 = 0
    readLenR2 = 0
    isPairedEnd = None

    if not os.path.isfile(fastq):
        status = "4_illumina_read_quality_stats failed"
        log.error("4_illumina_read_quality_stats failed. Cannot find the input fastq file")
        checkpoint_step_wrapper(status)
        return status

    ## First figure out if it's pair-ended or not
    ## NOTE: ssize=10000 is recommended!
    readLength, readLenR1, readLenR2, isPairedEnd = get_working_read_length(fastq, log)
    log.info("Read length = %s, and is_pair_ended = %s", readLength, isPairedEnd)

    if readLength == 0:
        log.error("Failed to run get_working_read_length.")
        status = "4_illumina_read_quality_stats failed"
        checkpoint_step_wrapper(status)

    else:
        ## Pair-ended
        r1_r2_baseposqual_png = None   ## Average Base Position Quality Plot (*.qrpt.png)
        r1_r2_baseposqual_html = None  ## Average Base Position Quality D3 Plot (*.qrpt.html)
        r1_r2_baseposqual_txt = None   ## Read 1/2 Average Base Position Quality Text (*.qhist.txt)

        r1_baseposqual_box_png = None  ## Read 1 Average Base Position Quality Boxplot (*.r1.png)
        r2_baseposqual_box_png = None  ## Read 2 Average Base Position Quality Boxplot (*.r2.png)
        r1_baseposqual_box_html = None ## Read 1 Average Base Position Quality D3 Boxplot (*.r1.html)
        r2_baseposqual_box_html = None ## Read 2 Average Base Position Quality D3 Boxplot (*.r2.html)
        r1_r2_baseposqual_box_txt = None  ## Average Base Position Quality text

        r1_cyclenbase_png = None       ## Read 1 Percent N by Read Position (*.r1.fastq.base.stats.Npercent.png) --> Read 1 Cycle N Base Percent plot
        r2_cyclenbase_png = None       ## Read 2 Percent N by Read Position (*.r2.fastq.base.stats.Npercent.png) --> Read 2 Cycle N Base Percent plot

        r1_cyclenbase_txt = None       ## Read 1 Percent N by Read Position Text (*.r1.fastq.base.stats) --> Read 1 Cycle N Base Percent text
        #r2_cyclenbase_txt = None       ## Read 2 Percent N by Read Position Text (*.r2.fastq.base.stats) --> Read 2 Cycle N Base Percent text
        r1_r2_cyclenbase_txt = None    ## Merged Percent N by Read Position Text (*.r2.fastq.base.stats) --> Merged Cycle N Base Percent text

        r1_cyclenbase_html = None
        r2_cyclenbase_html = None

        r1_nuclcompfreq_png = None     ## Read 1 Nucleotide Composition Frequency Plot (*.r1.stats.png)
        r2_nuclcompfreq_png = None     ## Read 2 Nucleotide Composition Frequency Plot (*.r2.stats.png)
        r1_nuclcompfreq_html = None
        r2_nuclcompfreq_html = None

        ## Single-ended
        #se_baseposqual_txt = None      ## Average Base Position Quality Text (*.qrpt)
        #se_nuclcompfreq_png = None     ## Cycle Nucleotide Composition (*.stats.png)

        if isPairedEnd:
            append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_LENGTH_1, readLenR1, log)
            append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_LENGTH_2, readLenR2, log)

        else:
            append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_LENGTH_1, readLength, log)

        ## Average Base Position Quality Plot/Text using qhist.txt
        r1_r2_baseposqual_txt, r1_r2_baseposqual_png, r1_r2_baseposqual_html = gen_average_base_position_quality_plot(fastq, isPairedEnd, log) ## .reformat.qhist.txt

        log.debug("Outputs: %s %s %s", r1_r2_baseposqual_png, r1_r2_baseposqual_html, r1_r2_baseposqual_txt)

        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QUAL_POS_PLOT_MERGED, r1_r2_baseposqual_png, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QUAL_POS_PLOT_MERGED_D3_HTML_PLOT, r1_r2_baseposqual_html, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QUAL_POS_QRPT_1, r1_r2_baseposqual_txt, log) ## for backward compatibility
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QUAL_POS_QRPT_2, r1_r2_baseposqual_txt, log) ## for backward compatibility
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QUAL_POS_QRPT_MERGED, r1_r2_baseposqual_txt, log)

        ## Average Base Position Quality Plot/Text using bqhist.txt
        r1_r2_baseposqual_box_txt, r1_baseposqual_box_png, r2_baseposqual_box_png, r1_baseposqual_box_html, r2_baseposqual_box_html = gen_average_base_position_quality_boxplot(fastq, log)

        log.debug("Read qual outputs: %s %s %s %s %s", r1_r2_baseposqual_box_txt, r1_baseposqual_box_png, r1_baseposqual_box_html, r2_baseposqual_box_png, r2_baseposqual_box_html)

        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QUAL_POS_QRPT_BOXPLOT_1, r1_baseposqual_box_png, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QUAL_POS_QRPT_D3_HTML_BOXPLOT_1, r1_baseposqual_box_html, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QUAL_POS_QRPT_BOXPLOT_2, r2_baseposqual_box_png, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QUAL_POS_QRPT_D3_HTML_BOXPLOT_2, r2_baseposqual_box_html, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QUAL_POS_QRPT_BOXPLOT_TEXT, r1_r2_baseposqual_box_txt, log)

        ## ----------------------------------------------------------------------------------------------------
        ## compute Q20 of the two reads
        q20Read1 = None
        q20Read2 = None

        ## using bqhist.txt
        if r1_r2_baseposqual_box_txt:
            q20Read1 = q20_score_new(r1_r2_baseposqual_box_txt, 1, log)
            if isPairedEnd:
                q20Read2 = q20_score_new(r1_r2_baseposqual_box_txt, 2, log)

        log.debug("q20 for read 1 = %s", q20Read1)
        log.debug("q20 for read 2 = %s", q20Read2)

        if q20Read1 is not None:
            append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_Q20_READ1, q20Read1, log)
        else:
            log.error("Failed to get q20 read 1 from %s", r1_r2_baseposqual_box_txt)
            status = "4_illumina_read_quality_stats failed"
            checkpoint_step_wrapper(status)
            return status

        if isPairedEnd:
            if q20Read2 is not None:
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_Q20_READ2, q20Read2, log)
            else:
                log.error("Failed to get q20 read 2 from %s", r1_r2_baseposqual_box_txt)
                status = "4_illumina_read_quality_stats failed"
                checkpoint_step_wrapper(status)
                return status

        r1_r2_cyclenbase_txt, r1_nuclcompfreq_png, r1_nuclcompfreq_html, r2_nuclcompfreq_png, r2_nuclcompfreq_html = gen_cycle_nucleotide_composition_plot(fastq, readLength, isPairedEnd, log)

        log.debug("gen_cycle_nucleotide_composition_plot() ==> %s %s %s %s %s", r1_cyclenbase_txt, r1_nuclcompfreq_png, r1_nuclcompfreq_html, r2_nuclcompfreq_png, r2_nuclcompfreq_html)

        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_COUNT_TEXT_1, r1_r2_cyclenbase_txt, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_COUNT_TEXT_2, r1_r2_cyclenbase_txt, log) # reformat.sh generates a single merged output file.

        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_COUNT_PLOT_1, r1_nuclcompfreq_png, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_COUNT_D3_HTML_PLOT_1, r1_nuclcompfreq_html, log)

        if r2_nuclcompfreq_png:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_COUNT_PLOT_2, r2_nuclcompfreq_png, log)
        if r2_nuclcompfreq_html:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_COUNT_D3_HTML_PLOT_2, r2_nuclcompfreq_html, log)

        ### ---------------------------------------------------------------------------------------------------
        ## using bhist.txt
        r1_cyclenbase_txt, r1_cyclenbase_png, r1_cyclenbase_html, r2_cyclenbase_png, r2_cyclenbase_html = gen_cycle_n_base_percent_plot(fastq, readLength, isPairedEnd, log)

        log.debug("Outputs: %s %s %s", r1_cyclenbase_txt, r1_cyclenbase_png, r1_cyclenbase_html)

        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_PERCENTAGE_TEXT_1, r1_cyclenbase_txt, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_PERCENTAGE_TEXT_2, r1_cyclenbase_txt, log)

        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_PERCENTAGE_PLOT_1, r1_cyclenbase_png, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_PERCENTAGE_D3_HTML_PLOT_1, r1_cyclenbase_html, log)

        if r2_cyclenbase_png:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_PERCENTAGE_PLOT_2, r2_cyclenbase_png, log)
        if r2_cyclenbase_html:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_PERCENTAGE_D3_HTML_PLOT_2, r2_cyclenbase_html, log)

        status = "4_illumina_read_quality_stats complete"
        log.info(status)
        checkpoint_step_wrapper(status)


    return status



"""
STEP5 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_write_base_quality_stats(fastq, log):
    log.info("\n\n%sSTEP5 - Calculating base quality statistics for reads <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCReadQcConfig.CFG["files_file"]
    statsFile = RQCReadQcConfig.CFG["stats_file"]

    status = "5_illumina_read_quality_stats in progress"
    checkpoint_step_wrapper(status)
    log.info(status)

    reformatObqhistFile = write_avg_base_quality_stats(fastq, log) ## *.reformat.obqhist.txt

    if not reformatObqhistFile:
        status = "5_illumina_read_quality_stats failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        ## Generate qual scores and plots of read level QC
        statsDict = {}

        retCode = base_level_qual_stats(statsDict, reformatObqhistFile, log)

        if retCode != RQCExitCodes.JGI_SUCCESS:
            status = "5_illumina_read_quality_stats failed"
            log.error(status)
            checkpoint_step_wrapper(status)

        else:
            for k, v in statsDict.items():
                append_rqc_stats(statsFile, k, str(v), log)

            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_BASE_QUALITY_STATS, reformatObqhistFile, log)

            status = "5_illumina_read_quality_stats complete"
            log.info(status)
            checkpoint_step_wrapper(status)


    return status



"""
STEP6 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_illumina_count_q_score(fastq, log):
    log.info("\n\n%sSTEP6 - Generating quality score histogram <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCReadQcConfig.CFG["files_file"]
    statsFile = RQCReadQcConfig.CFG["stats_file"]

    status = "6_illumina_count_q_score in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    qhistTxtFile, qhistPngFile, qhistHtmlPlotFile = illumina_count_q_score(fastq, log) ## *.obqhist.txt

    if not qhistTxtFile:
        status = "6_illumina_count_q_score failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        ## save qscores in statsFile
        qscore = {}
        read_level_qual_stats(qscore, qhistTxtFile, log)

        for k, v in qscore.items():
            append_rqc_stats(statsFile, k, str(v), log)

        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QHIST_TEXT, qhistTxtFile, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QHIST_PLOT, qhistPngFile, log)
        append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_QHIST_D3_HTML_PLOT, qhistHtmlPlotFile, log)

        status = "6_illumina_count_q_score complete"
        log.info(status)
        checkpoint_step_wrapper(status)


    return status



"""
STEP7 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
## 20140903 removed
##def do_illumina_calculate_average_quality(fastq, log):



"""
STEP8 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_illumina_find_common_motifs(fastq, log):
    log.info("\n\n%sSTEP8 - Locating N stutter motifs in sequence reads <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCReadQcConfig.CFG["files_file"]
    statsFile = RQCReadQcConfig.CFG["stats_file"]

    status = "8_illumina_find_common_motifs in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    retCode, statDataFile = illumina_find_common_motifs(fastq, log)

    log.info("nstutter statDataFile name = %s", statDataFile)

    if retCode != RQCExitCodes.JGI_SUCCESS:
        status = "8_illumina_find_common_motifs failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        ## read_level_stutter
        ## ex)
        ##688   N----------------------------------------------------------------------------------------------------------------------------
        ##-------------------------
        ##346   NNNNNNNNNNNNN----------------------------------------------------------------------------------------------------------------
        ##-------------------------
        ##53924 ------------N----------------------------------------------------------------------------------------------------------------
        ##-------------------------
        ##sum pct patterNs past 0.1 == 15.9245930330268 ( 54958 / 345114 * 100 )

        with open(statDataFile, "r") as stutFH:
            lines = stutFH.readlines()

            ## if no motifs are detected the file is empty
            if not lines:
                log.warning("The *.nstutter.stat file is not available in function read_level_stutter(). The function still returns JGI_SUCCESS.")

            else:
                assert lines[-1].find("patterNs") != -1
                t = lines[-1].split()
                ## ["sum", "pct", "patterNs", "past", "0.1", "==", "15.9245930330268", "(", "54958", "/", "345114", "*", "100", ")"]
                percent = "%.2f" % float(t[6])

                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_N_FREQUENCE, percent, log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_N_PATTERN, str("".join(lines[:-1])), log)

        ## NOTE: ???
        append_rqc_file(filesFile, "find_common_motifs.dataFile", statDataFile, log)

        status = "8_illumina_find_common_motifs complete"
        log.info(status)
        checkpoint_step_wrapper(status)


    return status



"""
STEP9 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
## REMOVED!
##def do_illumina_run_bwa(fastq, log):

def do_illumina_run_dedupe(fastq, log):
    log.info("\n\n%sSTEP9 - Aligning reads against each other using dedupe.sh <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    statsFile = RQCReadQcConfig.CFG["stats_file"]

    status = "9_illumina_run_dedupe in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    retCode, bwa_file = illumina_run_dedupe(fastq, log) ## bbdedupe.sh version

    if retCode != RQCExitCodes.JGI_SUCCESS:
        status = "9_illumina_run_dedupe failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        ## NEW
        ##$ dedupe.sh in=../2014.10.07-bbduk-contam-test/8370.6.99451.s0.01.fastq out=dedupe.out overwrite=t
        ##java -Djava.library.path=/usr/common/jgi/utilities/bbtools/prod-v33.60/lib -ea -Xmx93347m -Xms93347m -cp /usr/common/jgi/utilities/bbtools/prod-v33.60/lib/BBTools.jar jgi.Dedupe in=../2014.10.07-bbduk-contam-test/8370.6.99451.s0.01.fastq out=dedupe.out overwrite=t
        ##Executing jgi.Dedupe [in=../2014.10.07-bbduk-contam-test/8370.6.99451.s0.01.fastq, out=dedupe.out, overwrite=t]
        ##
        ##Initial:
        ##Memory: free=93314m, used=489m
        ##
        ##Found 844861 duplicates.
        ##Finished exact matches.    Time: 5.720 seconds.
        ##Memory: free=72758m, used=21045m
        ##
        ##Found 0 contained sequences.
        ##Finished containment.      Time: 7.886 seconds.
        ##Memory: free=78830m, used=14973m
        ##
        ##Removed 0 invalid entries.
        ##Finished invalid removal.  Time: 0.178 seconds.
        ##Memory: free=78830m, used=14973m
        ##
        ##Input:                    4767578 reads       476757800 bases.
        ##Duplicates:               844861 reads (17.72%)   84486100 bases (17.72%)         0 collisions.
        ##Containments:             0 reads (0.00%)     0 bases (0.00%)     292068110 collisions.
        ##Result:                   3922717 reads (82.28%)  392271700 bases (82.28%)
        ##
        ##Printed output.            Time: 4.879 seconds.
        ##Memory: free=73668m, used=20135m
        ##
        ##Time:             18.679 seconds.
        ##Reads Processed:       4767k  255.24k reads/sec
        ##Bases Processed:        476m  25.52m bases/sec

        numTotal = None
        numDup = None

        with open(bwa_file, "r") as bwaFH:
            lines = bwaFH.readlines()

            for l in lines:
                if l.startswith("Input"):
                    numTotal = int(l.strip().split()[1])
                elif l.startswith("Duplicates"):
                    numDup = int(l.strip().split()[1])

            if numTotal and numDup:
                percentDup = float(numDup) / numTotal * 100.0
                percentDup = float("%.1f" % (percentDup))

                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_BWA_ALIGNED, numTotal, log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_BWA_ALIGNED_DUPLICATE, numDup, log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_BWA_ALIGNED_DUPLICATE_PERCENT, percentDup, log)

        ## For RQC2.0
        ## Read readqc_stats for getting ILLUMINA_READ_N_PATTERN
        ## To deal with restarting feature of rqc pipeline, it's safe to read the statsFile contents to get the info needed.

        npattern = None

        if statsFile:
            contents = get_analysis_file_contents(statsFile)

            for l in contents:
                if l.startswith("read N pattern"):
                    t = l.split("=")
                    assert len(t) == 2
                    npattern = t[1].strip()
                    log.debug("nstutter pattern: %s", l)
                    break

        else:
            log.warning("nstutter file can not be found in readqc_files.txt.")

        ## Prepare report data
        serialNum = 0

        if npattern and numTotal != 0:
            motif_freq = {}
            patterns = npattern.split("\n")

            for p in patterns:
                t = p.strip().split()
                assert len(t) == 2
                count = t[0]
                motif = t[1]
                motif_freq[motif] = count

            for w in sorted(motif_freq, key=motif_freq.get, reverse=True):
                serialNum += 1
                subper = float(motif_freq[w]) / numTotal * 100
                append_rqc_stats(statsFile, "motif_freq_patt_" + str(serialNum), w, log)
                append_rqc_stats(statsFile, "motif_freq_cnt_" + str(serialNum), motif_freq[w], log)
                append_rqc_stats(statsFile, "motif_freq_perc_" + str(serialNum), subper, log)

        else:
            log.warning("nstutter pattern(s) can not be found.")

        ## save motif freq count for reporting in rqc 2.0
        append_rqc_stats(statsFile, "motif_freq_num", serialNum, log)

        ##
        ## This will create lines in statsFile like
        ## motif_freq_1 = N--------------------------------------------------------------------------------------------------- 12 0.11
        ## motif_freq_2 = N--------------------------------------------------------------------------------------------------- 4 0.15
        ## motif_freq_3 = N--------------------------------------------------------------------------------------------------- 3 0.16
        ## motif_freq_number = 3

        status = "9_illumina_run_dedupe complete"
        log.info(status)
        checkpoint_step_wrapper(status)


    return status



"""
STEP10 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
## Removed!
##def do_illumina_run_tagdust(fastq, log):



"""
STEP11 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_illumina_detect_read_contam(fastq, bpToCut, log):
    log.info("\n\n%sSTEP11 - Detect read contam <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCReadQcConfig.CFG["files_file"]
    statsFile = RQCReadQcConfig.CFG["stats_file"]

    status = "11_illumina_detect_read_contam in progress"
    log.info(status)
    checkpoint_step_wrapper(status)


    #########
    ## seal
    #########
    retCode2, outFileDict2, ratioResultDict2, contamStatDict = illumina_detect_read_contam3(fastq, bpToCut, log) ## seal version

    if retCode2 != RQCExitCodes.JGI_SUCCESS:
        log.error("11_illumina_detect_read_contam seal version failed.")
        status = "11_illumina_detect_read_contam failed"
        checkpoint_step_wrapper(status)

    else:
        for k, v in outFileDict2.items():
            append_rqc_file(filesFile, k + " seal", str(v), log)
            append_rqc_file(filesFile, k, str(v), log)

        for k, v in ratioResultDict2.items():
            append_rqc_stats(statsFile, k + " seal", str(v), log)
            append_rqc_stats(statsFile, k, str(v), log)

        ## contamination stat
        for k, v in contamStatDict.items():
            append_rqc_stats(statsFile, k, str(v), log)

        log.info("11_illumina_detect_read_contam seal version complete.")
        status = "11_illumina_detect_read_contam complete"
        checkpoint_step_wrapper(status)


    return status



"""
STEP12 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_illumina_sciclone_analysis(fastq, origFastq, log, libName=None, isRna=None):
    log.info("\n\n%sSTEP12 - Sciclone analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCReadQcConfig.CFG["files_file"]
    statsFile = RQCReadQcConfig.CFG["stats_file"]

    status = "12_illumina_sciclone_analysis in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    if not os.path.isfile(fastq):
        log.error("Fastq file not found.")
        return RQCExitCodes.JGI_FAILURE

    ## First figure out if it's pair-ended or not
    ## ssize=10000 is recommended!
    readLength, _, _, isPairedEnd = get_working_read_length(fastq, log)

    if readLength == 0:
        status = "12_illumina_sciclone_analysis failed"
        log.error("Failed to run get_working_read_length.")
        checkpoint_step_wrapper(status)

    else:
        retCode, dnaCountFile, rnaCountFile = illumina_sciclone_analysis2(origFastq, isPairedEnd, log, libName=libName, isRna=isRna)

        if retCode != RQCExitCodes.JGI_SUCCESS:
            ###if strandedness_output_log == -1: ## no lib info available
            if dnaCountFile == -1 and rnaCountFile == -1: ## no li10.24b info available
                log.warning("No library info found for %s", origFastq)
                log.warning("Skip the sciclone analysis.")
                log.warning("12_illumina_sciclone_analysis skipped. No lib info found")
                status = "12_illumina_sciclone_analysis complete"
                checkpoint_step_wrapper(status)

            else:
                status = "12_illumina_sciclone_analysis failed"
                log.error(status)
                checkpoint_step_wrapper(status)

        else:
            if dnaCountFile:
                append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_SCICLONE_DNA_COUNT_FILE, dnaCountFile, log)

                ## For RQC 2.0
                ## Define a constant hash for the 24 well IDs as reference
                WELL_KEYS = {
                    "A01":1, "A03":1, "A05":1,
                    "B02":1, "B04":1, "B06":1,
                    "C01":1, "C03":1, "C05":1,
                    "D02":1, "D04":1, "D06":1,
                    "E01":1, "E03":1, "E05":1,
                    "F02":1, "F04":1, "F06":1,
                    "G01":1, "G03":1, "G05":1,
                    "H02":1, "H04":1, "H06":1}

                scicloneDnaDataDict = {}

                ## Get the file contents
                scicloneFileContents = get_analysis_file_contents(dnaCountFile)

                log.debug("scicloneFileContents = %s", scicloneFileContents)

                if scicloneFileContents != "file not found":
                    totalCount = 0
                    matchedCount = 0
                    matchedPerc = 0.0

                    for l in scicloneFileContents:
                        if l.startswith("#Total"):
                            totalCount = int(l.split()[1])

                        elif l.startswith("#Matched"):
                            ##
                            ## RQCSUPPORT-467
                            ##
                            ##$ more ./qual/8808.2.112672.CCCATG.s0.01.DNA_spikein.bbduk.stats
                            ###File /global/projectb/scratch/qc_user/rqc/prod/pipelines/read_qc/in-progress/02/85/85/51/subsample/8808.2.112672.CCCATG.s0.01.fastq
                            ###Total    90256
                            ###Matched  2188    2.42422%
                            ###Name Reads   ReadsPct
                            ##Nextera_LMP_read1_external_adapter    1062    1.17665%   ===> SHOULD NOT BE COUNTED!!
                            ##Nextera_LMP_read2_external_adapter    1030    1.14120%   ===> SHOULD NOT BE COUNTED!!
                            ##I7_Primer_Nextera_XT_and_Nextera_Enrichment_N706  52  0.05761%   ===> SHOULD NOT BE COUNTED!!
                            ##I5_Primer_Nextera_XT_and_Nextera_Enrichment_[N/S/E]501    35  0.03878%   ===> SHOULD NOT BE COUNTED!!
                            ##gi|149999561|gb|EF694056.1| Cloning vector pJET1.2, complete sequence 9   0.00997%

                            matchedCount = int(l.split()[1])
                            matchedPerc = float(l.split()[2].replace("%", ""))

                        ## Ignore comments and it is not an empty line
                        if l != "" and not l.startswith("#"):
                            ## Split the info by whitespace
                            scicloneLineFields = l.split()
                            assert len(scicloneLineFields) == 3

                            wellId = scicloneLineFields[0].split("_")[0]
                            readCnt = scicloneLineFields[1]
                            readPerc = scicloneLineFields[2].replace("%", "")

                            ## New output from bbduk
                            ##
                            ## #File    8076.3.89390.AAATGC.fastq.gz
                            ## #Total   75578216
                            ## #Matched 6845752 9.05784%
                            ## C01_RandFrag_2_25b   6841939 9.05279%
                            ## A01_RandFrag_1_25a   3543    0.00469%
                            ## D02_RandFrag_6_35b   71  0.00009%
                            ## A03_RandFrag_9_45a   57  0.00008%
                            ## B02_RandFrag_5_35a   31  0.00004%
                            ## F04_RandFrag_15_55c  29  0.00004%
                            ## C03_RandFrag_10_45b  22  0.00003%
                            ## H02_RandFrag_8_35d   18  0.00002%
                            ## B06_RandFrag_21_75a  14  0.00002%
                            ## E01_RandFrag_3_25c   8   0.00001%
                            ## E03_RandFrag_11_45c  6   0.00001%
                            ## F06_RandFrag_23_75c  5   0.00001%
                            ## D04_RandFrag_14_55b  4   0.00001%
                            ## H06_RandFrag_24_75d  3   0.00000%
                            ## D06_RandFrag_22_75b  2   0.00000%

                            if wellId in WELL_KEYS:
                                ## Set the read count.
                                scicloneDnaDataDict[wellId] = {}
                                scicloneDnaDataDict[wellId]["READ_CT"] = readCnt

                                ## Set the read percent. Round to 2 decimal places.
                                append_rqc_stats(statsFile, wellId, readPerc, log)

                    append_rqc_stats(statsFile, "ILLUMINA_READ_SCICLONE_DNA_COUNT_TOTAL", totalCount, log)
                    append_rqc_stats(statsFile, "ILLUMINA_READ_SCICLONE_DNA_COUNT_MATCHED", matchedCount, log)
                    append_rqc_stats(statsFile, "ILLUMINA_READ_SCICLONE_DNA_COUNT_MATCHED_PERC", matchedPerc, log)

                    ## Check if the hash has any values
                    for k, _ in WELL_KEYS.iteritems():
                        if k in scicloneDnaDataDict:
                            continue
                        else:
                            append_rqc_stats(statsFile, k, "0.00", log)

                    log.debug("scicloneDnaDataDict = %s", str(scicloneDnaDataDict))

            elif rnaCountFile:
                append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_SCICLONE_RNA_COUNT_FILE, rnaCountFile, log)

            status = "12_illumina_sciclone_analysis complete"
            log.info(status)
            checkpoint_step_wrapper(status)


    return status



"""
STEP13 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
## Removed!!
##def do_illumina_read_megablast(firstSubsampledFastqFileName, skipSubsampling, subsampledReadNum, log, blastDbPath=None):



"""
New STEP13 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_illumina_subsampling_read_blastn(firstSubsampledFastqFileName, skipSubsampling, subsampledReadNum, log):
    log.info("\n\n%sSTEP13 - Run subsampling for Blast search <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    subsampeldFastqFile = None
    totalReadNum = 0
    readNumToReturn = 0

    status = "13_illumina_subsampling_read_megablast in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    if subsampledReadNum == 0:
        cmd = " ".join(["grep", "-c", "'^+'", firstSubsampledFastqFileName])

        stdOut, _, exitCode = run_sh_command(cmd, True, log)

        if exitCode != 0:
            log.error("Failed to run grep cmd")
            return RQCExitCodes.JGI_FAILURE, None, None, None

        else:
            readNum = int(stdOut)

    else:
        readNum = subsampledReadNum

    log.info("Subsampled read number = %d", readNum)

    sampl_per = RQCReadQc.ILLUMINA_SAMPLE_PERCENTAGE    ## 0.01
    max_count = RQCReadQc.ILLUMINA_SAMPLE_COUNT         ## 50000

    ## TODO
    ## Use "samplereadstarget" option in reformat.sh

    if skipSubsampling:
        log.debug("No subsampling for megablast. Use fastq file, %s (readnum = %s) as query for megablast.", firstSubsampledFastqFileName, readNum)
        subsampeldFastqFile = firstSubsampledFastqFileName
        readNumToReturn = readNum

    else:
        if readNum > max_count:
            log.debug("Run the 2nd subsampling for running megablast.")
            secondSubsamplingRate = float(max_count) / readNum
            log.debug("SecondSubsamplingRate=%s, max_count=%s, readNum=%s", secondSubsamplingRate, max_count, readNum)
            log.info("Second subsampling of Reads after Percent Subsampling reads = %s with new percent subsampling %f.", readNum, secondSubsamplingRate)

            secondSubsampledFastqFile = ""
            # dataFile = ""

            sequnitFileName, exitCode = safe_basename(firstSubsampledFastqFileName, log)
            sequnitFileName = sequnitFileName.replace(".fastq", "").replace(".gz", "")

            secondSubsampledFastqFile = sequnitFileName + ".s" + str(sampl_per) + ".s" + str(secondSubsamplingRate) + ".n" + str(max_count) + ".fastq"

            ## ex) fq_sub_sample.pl -f .../7601.1.77813.CTTGTA.s0.01.fastq -o .../7601.1.77813.CTTGTA.s0.01.stats -r 0.0588142575171 > .../7601.1.77813.CTTGTA.s0.01.s0.01.s0.0588142575171.n50000.fastq
            ## ex) reformat.sh
            retCode, secondSubsampledFastqFile, totalBaseCount, totalReadNum, subsampledReadNum, _, _ = fast_subsample_fastq_sequences(firstSubsampledFastqFileName, secondSubsampledFastqFile, secondSubsamplingRate, False, log)

            if retCode != RQCExitCodes.JGI_SUCCESS:
                log.error("Second subsampling failed.")
                return RQCExitCodes.JGI_FAILURE, None, None, None

            else:
                log.info("Second subsampling complete.")
                log.info("Second Subsampling Total Base Count = %s.", totalBaseCount)
                log.info("Second Subsampling Total Reads = %s.", totalReadNum)
                log.info("Second Subsampling Sampled Reads = %s.", subsampledReadNum)

                if subsampledReadNum == 0:
                    log.warning("Too small first subsampled fastq file. Skip the 2nd sampling.")
                    secondSubsampledFastqFile = firstSubsampledFastqFileName

            subsampeldFastqFile = secondSubsampledFastqFile
            readNumToReturn = subsampledReadNum

        else:
            log.debug("The readNum is smaller than max_count=%s. The 2nd sampling skipped.", str(max_count))
            subsampeldFastqFile = firstSubsampledFastqFileName
            totalReadNum = readNum
            readNumToReturn = readNum

    status = "13_illumina_subsampling_read_megablast complete"
    log.info(status)
    checkpoint_step_wrapper(status)


    return status, subsampeldFastqFile, totalReadNum, readNumToReturn



##"""
##STEP14 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##
##"""
##def do_illumina_read_blastn_refseq_microbial(subsampeldFastqFile, subsampledReadNum, log, blastDbPath=None):
## 12212015 sulsj REMOVED!


"""
New STEP14 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_illumina_read_blastn_refseq_archaea(subsampeldFastqFile, subsampledReadNum, log):
    log.info("\n\n%sSTEP14 - Run read blastn against refseq archaea <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    statsFile = RQCReadQcConfig.CFG["stats_file"]
    retCode = None

    status = "14_illumina_read_blastn_refseq_archaea in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    retCode = illumina_read_blastn_refseq_archaea(subsampeldFastqFile, log)

    if retCode == RQCExitCodes.JGI_FAILURE:
        status = "14_illumina_read_blastn_refseq_archaea failed"
        log.error(status)

    elif retCode == -143: ## timeout
        log.warning("14_illumina_read_blastn_refseq_archaea timeout.")
        status = "14_illumina_read_blastn_refseq_archaea complete"

    else:
        ## read number used in blast search?
        append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READS_NUMBER, subsampledReadNum, log)

        ret2 = read_megablast_hits("refseq.archaea", log)

        if ret2 != RQCExitCodes.JGI_SUCCESS:
            log.error("Errors in read_megablast_hits() of refseq.microbial")
            status = "14_illumina_read_blastn_refseq_archaea failed"
            log.error(status)

        else:
            status = "14_illumina_read_blastn_refseq_archaea complete"
            log.info(status)


    return status


"""
STEP15 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_illumina_read_blastn_refseq_bacteria(subsampeldFastqFile, log):
    log.info("\n\n%sSTEP15 - Run read blastn against refseq bacteria <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    status = "15_illumina_read_blastn_refseq_bacteria in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    retCode = illumina_read_blastn_refseq_bacteria(subsampeldFastqFile, log)

    if retCode == RQCExitCodes.JGI_FAILURE:
        status = "15_illumina_read_blastn_refseq_bacteria failed"
        log.error(status)

    elif retCode == -143: ## timeout
        log.warning("15_illumina_read_blastn_refseq_bacteria timeout.")
        status = "15_illumina_read_blastn_refseq_bacteria complete"

    else:
        ret2 = read_megablast_hits("refseq.bacteria", log)

        if ret2 != RQCExitCodes.JGI_SUCCESS:
            log.error("Errors in read_megablast_hits() of refseq.microbial")
            log.error("15_illumina_read_blastn_refseq_bacteria reporting failed.")
            status = "15_illumina_read_blastn_refseq_bacteria failed"

        else:
            status = "15_illumina_read_blastn_refseq_bacteria complete"
            log.info(status)


    return status



"""
STEP16 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_illumina_read_blastn_nt(subsampeldFastqFile, log):
    log.info("\n\n%sSTEP16 - Run read blastn against nt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    retCode = None

    status = "16_illumina_read_blastn_nt in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    retCode = illumina_read_blastn_nt(subsampeldFastqFile, log)

    if retCode == RQCExitCodes.JGI_FAILURE:
        status = "16_illumina_read_blastn_nt failed"
        log.error(status)

    elif retCode == -143: ## timeout
        log.warning("16_illumina_read_blastn_nt timeout.")
        status = "16_illumina_read_blastn_nt complete"

    else:
        ret2 = read_megablast_hits("nt", log)

        if ret2 != RQCExitCodes.JGI_SUCCESS:
            log.error("Errors in read_megablast_hits() of nt")
            log.error("16_illumina_read_blastn_nt reporting failed.")
            status = "16_illumina_read_blastn_nt failed"

        else:
            status = "16_illumina_read_blastn_nt complete"
            log.info(status)


    return status



"""
STEP17 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_illumina_multiplex_statistics

Demultiplexing analysis for pooled lib

"""
def do_illumina_multiplex_statistics(fastq, log, isMultiplexed=None):
    log.info("\n\n%sSTEP17 - Run Multiplex statistics analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCReadQcConfig.CFG["files_file"]

    status = "17_multiplex_statistics in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    log.debug("fastq file: %s", fastq)

    retCode, demultiplexStatsFile, detectionPngPlotFile, detectionHtmlPlotFile = illumina_generate_index_sequence_detection_plot(fastq, log, isMultiplexed=isMultiplexed)

    if retCode != RQCExitCodes.JGI_SUCCESS:
        status = "17_multiplex_statistics failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        status = "17_multiplex_statistics complete"
        log.info(status)
        checkpoint_step_wrapper(status)

        if detectionPngPlotFile is not None:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_DEMULTIPLEX_STATS_PLOT, detectionPngPlotFile, log)

        if detectionHtmlPlotFile is not None:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_DEMULTIPLEX_STATS_D3_HTML_PLOT, detectionHtmlPlotFile, log)

        if demultiplexStatsFile is not None:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_DEMULTIPLEX_STATS, demultiplexStatsFile, log)


    return status



"""
STEP18 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_end_of_read_illumina_adapter_check

"""
def do_end_of_read_illumina_adapter_check(firstSubsampledFastqFileName, log):
    log.info("\n\n%sSTEP18 - Run end_of_read_illumina_adapter_check analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCReadQcConfig.CFG["files_file"]
    plotFile = None
    dataFile = None

    status = "18_end_of_read_illumina_adapter_check in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    log.debug("sampled fastq file: %s", firstSubsampledFastqFileName)

    retCode, dataFile, plotFile, htmlFile = end_of_read_illumina_adapter_check(firstSubsampledFastqFileName, log)

    if retCode != RQCExitCodes.JGI_SUCCESS:
        status = "18_end_of_read_illumina_adapter_check failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        status = "18_end_of_read_illumina_adapter_check complete"
        log.info(status)
        checkpoint_step_wrapper(status)

        if plotFile is not None:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_END_OF_READ_ADAPTER_CHECK_PLOT, plotFile, log)
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_END_OF_READ_ADAPTER_CHECK_D3_HTML_PLOT, htmlFile, log)

        if dataFile is not None:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_END_OF_READ_ADAPTER_CHECK_DATA, dataFile, log)


    return status



"""
STEP19 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_insert_size_analysis

"""
def do_insert_size_analysis(fastq, log):
    log.info("\n\n%sSTEP19 - Run insert size analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCReadQcConfig.CFG["files_file"]
    statsFile = RQCReadQcConfig.CFG["stats_file"]

    plotFile = None
    dataFile = None

    status = "19_insert_size_analysis in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    log.debug("fastq file used: %s", fastq)

    retCode, dataFile, plotFile, htmlFile, statsDict = insert_size_analysis(fastq, log) ## by bbmerge.sh in bbtools

    if retCode != RQCExitCodes.JGI_SUCCESS:
        status = "19_insert_size_analysis failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        if plotFile is not None:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_HISTO_PLOT, plotFile, log)

        if dataFile is not None:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_HISTO_DATA, dataFile, log)

        if htmlFile is not None:
            append_rqc_file(filesFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_HISTO_D3_HTML_PLOT, htmlFile, log)

        if statsDict:
            try:
                ## --------------------------------------------------------------------------------------------------------
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_AVG_INSERT, statsDict["avg_insert"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_STD_INSERT, statsDict["std_insert"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_MODE_INSERT, statsDict["mode_insert"], log)
                ## --------------------------------------------------------------------------------------------------------

                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_TOTAL_TIME, statsDict["total_time"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_NUM_READS, statsDict["num_reads"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_JOINED_NUM, statsDict["joined_num"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_JOINED_PERC, statsDict["joined_perc"], log)

                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_AMBIGUOUS_NUM, statsDict["ambiguous_num"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_AMBIGUOUS_PERC, statsDict["ambiguous_perc"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_NO_SOLUTION_NUM, statsDict["no_solution_num"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_NO_SOLUTION_PERC, statsDict["no_solution_perc"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_TOO_SHORT_NUM, statsDict["too_short_num"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_TOO_SHORT_PERC, statsDict["too_short_perc"], log)

                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_INSERT_RANGE_START, statsDict["insert_range_start"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_INSERT_RANGE_END, statsDict["insert_range_end"], log)

                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_90TH_PERC, statsDict["perc_90th"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_50TH_PERC, statsDict["perc_50th"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_10TH_PERC, statsDict["perc_10th"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_75TH_PERC, statsDict["perc_75th"], log)
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_READ_INSERT_SIZE_25TH_PERC, statsDict["perc_25th"], log)

            except KeyError:
                log.error("19_insert_size_analysis failed (KeyError).")
                status = "19_insert_size_analysis failed"
                checkpoint_step_wrapper(status)
                return status

        status = "19_insert_size_analysis complete"
        log.info(status)
        checkpoint_step_wrapper(status)


    return status



"""
STEP20 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_gc_divergence


"""
def do_gc_divergence_analysis(fastq, log):
    log.info("\n\n%sSTEP20 - Run GC divergence analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    
    if os.environ['NERSC_HOST'] == "docker":
        log.info("GC divergence analysis will be skipped in Docker environment.")
        status = "20_gc_divergence_analysis complete"
        log.info(status)
        checkpoint_step_wrapper(status)
        return status
    
    filesFile = RQCReadQcConfig.CFG["files_file"]
    statsFile = RQCReadQcConfig.CFG["stats_file"]

    status = "20_gc_divergence_analysis in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    readLength, _, _, isPairedEnd = get_working_read_length(fastq, log)
    log.info("Read length = %s, and is_pair_ended = %s", readLength, isPairedEnd)

    retCode, transformedCsvFile, plotFile, coefficientsCsvFile, coeffList = gc_divergence_analysis(fastq, isPairedEnd, SRC_DIR, log)

    if retCode != RQCExitCodes.JGI_SUCCESS:
        status = "20_gc_divergence_analysis failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        for v in coeffList:
            readNum = v["read"].replace("\"", "")
            varVal = v["variable"].replace("\"", "")

            if readNum == "Read 1":
                if varVal == "AT":
                    append_rqc_stats(statsFile, ReadqcStats.GC_DIVERGENCE_COEFF_R1_AT, v["coefficient"], log)
                elif varVal == "AT+CG":
                    append_rqc_stats(statsFile, ReadqcStats.GC_DIVERGENCE_COEFF_R1_ATCG, v["coefficient"], log)
                elif varVal == "CG":
                    append_rqc_stats(statsFile, ReadqcStats.GC_DIVERGENCE_COEFF_R1_CG, v["coefficient"], log)
            elif readNum == "Read 2":
                if varVal == "AT":
                    append_rqc_stats(statsFile, ReadqcStats.GC_DIVERGENCE_COEFF_R2_AT, v["coefficient"], log)
                elif varVal == "AT+CG":
                    append_rqc_stats(statsFile, ReadqcStats.GC_DIVERGENCE_COEFF_R2_ATCG, v["coefficient"], log)
                elif varVal == "CG":
                    append_rqc_stats(statsFile, ReadqcStats.GC_DIVERGENCE_COEFF_R2_CG, v["coefficient"], log)
            else:
                log.error("Unexpected coefficient value: %s", str(coeffList))
                status = "20_gc_divergence_analysis failed"
                log.error(status)
                checkpoint_step_wrapper(status)
                return status

        append_rqc_file(filesFile, ReadqcStats.GC_DIVERGENCE_CSV_FILE, transformedCsvFile, log)
        append_rqc_file(filesFile, ReadqcStats.GC_DIVERGENCE_PLOT_FILE, plotFile, log)
        append_rqc_file(filesFile, ReadqcStats.GC_DIVERGENCE_COEFFICIENTS_CSV_FILE, coefficientsCsvFile, log)

        status = "20_gc_divergence_analysis complete"
        log.info(status)
        checkpoint_step_wrapper(status)


    return status



"""
STEP21 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_sketch_vs_nt_refseq_silva

"""
def do_sketch_vs_nt_refseq_silva(fasta, log):
    log.info("\n\n%sSTEP21 - Run sketch vs nt, refseq, silva <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "21_sketch_vs_nt_refseq_silva in progress"
    checkpoint_step_wrapper(status)

    sketchOutDir = "sketch"
    sketchOutPath = os.path.join(outputPath, sketchOutDir)
    make_dir(sketchOutPath)
    change_mod(sketchOutPath, "0755")

    seqUnitName, exitCode = safe_basename(fasta, log)
    seqUnitName = file_name_trim(seqUnitName)

    filesFile = RQCReadQcConfig.CFG["files_file"]

    ## sendsketch examples
    ## sendsketch.sh in=file.fa out=results.txt colors=f nt
    ## sendsketch.sh in=file.fa out=results.txt colors=f refseq
    ## sendsketch.sh in=file.fa out=results.txt colors=f silva

    comOptions = "ow=t colors=f printtaxa=t depth depth2 unique2 merge"

    ## NT ##########################
    sketchOutFile = os.path.join(sketchOutPath, seqUnitName + ".sketch_vs_nt.txt")
    sendSketchShCmd = get_tool_path("sendsketch.sh", "bbtools")
    cmd = "%s in=%s out=%s %s nt" % (sendSketchShCmd, fasta, sketchOutFile, comOptions)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)
    if exitCode != 0:
        log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
        status = "21_sketch_vs_nt_refseq_silva failed"
        checkpoint_step_wrapper(status)
        return status

    append_rqc_file(filesFile, "sketch_vs_nt_output", sketchOutFile, log)

    ## Refseq ##########################
    sketchOutFile = os.path.join(sketchOutPath, seqUnitName + ".sketch_vs_refseq.txt")
    cmd = "%s in=%s out=%s %s refseq" % (sendSketchShCmd, fasta, sketchOutFile, comOptions)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)
    if exitCode != 0:
        log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
        status = "21_sketch_vs_refseq failed"
        checkpoint_step_wrapper(status)
        return status

    append_rqc_file(filesFile, "sketch_vs_refseq_output", sketchOutFile, log)

    ## Silva ##########################
    sketchOutFile = os.path.join(sketchOutPath, seqUnitName + ".sketch_vs_silva.txt")
    cmd = "%s in=%s out=%s %s silva" % (sendSketchShCmd, fasta, sketchOutFile, comOptions)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)
    if exitCode != 0:
        log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
        status = "21_sketch_vs_silva failed"
        checkpoint_step_wrapper(status)
        return status

    append_rqc_file(filesFile, "sketch_vs_silva_output", sketchOutFile, log)

    status = "21_sketch_vs_nt_refseq_silva complete"
    log.info(status)
    checkpoint_step_wrapper(status)

    return status

##
## Shijie : add kapa spikein detection: (RQC-1117)
##
def do_kapa(fastq, log):
    log.info("\n\n%sSTEP22 - kapa spikein detection using seal.sh <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    if os.environ['NERSC_HOST'] == "docker":
        log.info("Kapa spikein analysis will be skipped in Docker environment.")
        return RQCExitCodes.JGI_SUCCESS

    ## kapa index to well id mapping, from https://docs.google.com/spreadsheets/d/1AG6iTfE3_MZPU73mIh3WsOoo_GrTIfC7uaE7mG2WAgs/edit#gid=327238341
    kapamap = {
        'A1': 'tag001',
        'B1': 'tag002',
        'C1': 'tag003',
        'D1': 'tag004',
        'E1': 'tag005',
        'F1': 'tag006',
        'G1': 'tag007',
        'H1': 'tag008',
        'A2': 'tag009',
        'B2': 'tag010',
        'C2': 'tag011',
        'D2': 'tag012',
        'E2': 'tag013',
        'F2': 'tag014',
        'G2': 'tag015',
        'H2': 'tag016',
        'A3': 'tag017',
        'B3': 'tag018',
        'C3': 'tag019',
        'D3': 'tag020',
        'E3': 'tag021',
        'F3': 'tag022',
        'G3': 'tag023',
        'H3': 'tag027',
        'A4': 'tag029',
        'B4': 'tag030',
        'C4': 'tag031',
        'D4': 'tag032',
        'E4': 'tag033',
        'F4': 'tag034',
        'G4': 'tag035',
        'H4': 'tag036',
        'A5': 'tag037',
        'B5': 'tag038',
        'C5': 'tag039',
        'D5': 'tag040',
        'E5': 'tag041',
        'F5': 'tag042',
        'G5': 'tag043',
        'H5': 'tag044',
        'A6': 'tag045',
        'B6': 'tag046',
        'C6': 'tag047',
        'D6': 'tag102',
        'E6': 'tag049',
        'F6': 'tag050',
        'G6': 'tag051',
        'H6': 'tag052',
        'A7': 'tag053',
        'B7': 'tag054',
        'C7': 'tag055',
        'D7': 'tag056',
        'E7': 'tag057',
        'F7': 'tag058',
        'G7': 'tag059',
        'H7': 'tag060',
        'A8': 'tag061',
        'B8': 'tag062',
        'C8': 'tag063',
        'D8': 'tag064',
        'E8': 'tag065',
        'F8': 'tag066',
        'G8': 'tag067',
        'H8': 'tag068',
        'A9': 'tag069',
        'B9': 'tag070',
        'C9': 'tag071',
        'D9': 'tag072',
        'E9': 'tag073',
        'F9': 'tag074',
        'G9': 'tag075',
        'H9': 'tag076',
        'A10': 'tag077',
        'B10': 'tag078',
        'C10': 'tag079',
        'D10': 'tag080',
        'E10': 'tag081',
        'F10': 'tag082',
        'G10': 'tag083',
        'H10': 'tag084',
        'A11': 'tag085',
        'B11': 'tag086',
        'C11': 'tag087',
        'D11': 'tag088',
        'E11': 'tag089',
        'F11': 'tag090',
        'G11': 'tag091',
        'H11': 'tag092',
        'A12': 'tag093',
        'B12': 'tag094',
        'C12': 'tag095',
        'D12': 'tag096',
        'E12': 'tag097',
        'F12': 'tag099',
        'G12': 'tag104',
        'H12': 'tag101'
    }

    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)
    sun = os.path.basename(fastq)
    sql = 'select well_id from library_info l inner join seq_units s on l.library_name=s.library_name where seq_unit_name=%s'
    sth.execute(sql, (sun,))
    rs = sth.fetchone()
    if not rs:
        log.warning("Query for well id (%s) returned empty in library_info." % sql%sun)
        return RQCExitCodes.JGI_FAILURE

    wellId = rs['well_id']
    if not wellId:
        log.warning("well id is NULL in library_info [%s]" % sun)
        return RQCExitCodes.JGI_FAILURE

    kapaIndex = kapamap[wellId]
    if not kapaIndex:
        log.warning("No kapa spikein index defined for well id [%s]" % wellId)
        return RQCExitCodes.JGI_FAILURE

    sealShCmd = RQCReadQcCommands.SEAL_SH_CMD

    sequnitFileName, exitCode = safe_basename(fastq, log)
    READ_OUTPUT_PATH = RQCReadQcConfig.CFG["output_path"]

    kapaDir = "kapa"
    kapaPath = os.path.join(READ_OUTPUT_PATH, kapaDir)
    make_dir(kapaPath)
    change_mod(kapaPath, "0755")

    kapaStats = os.path.join(kapaPath, "kapa.stats")
    kapaOut = os.path.join(kapaPath, "kapaReads.fa")

    ## Moved to readqc_constants.py
    # kapaDB = '/global/projectb/sandbox/gaag/bbtools/data/kapatags.L40.fa'
    # sealShCmd = "%s in=%s stats=%s out=%s ow=t ref=%s" % (sealShCmd, fastq, kapaStats, kapaOut, kapaDB)
    sealShCmd = "%s in=%s stats=%s out=%s ow=t ref=%s" % (sealShCmd, fastq, kapaStats, kapaOut, RQCReadQcReferenceDatabases.kapaDB)
    log.info('RUN %s' % sealShCmd)
    _, _, exitCode = run_sh_command(sealShCmd, True, log, True)

    if exitCode != 0:
        log.error("Failed to run %s" % sealShCmd)
        return RQCExitCodes.JGI_FAILURE
    else:

        if os.path.isfile(kapaStats) and os.path.isfile(kapaOut):
            # topName = 'unknown'
            expectedReadCnt = 0
            expectedReadPct = 0
            expectedBaseCnt = 0
            expectedBasePct = 0
            otherReadCnt = 0
            otherReadPct = 0
            otherBaseCnt = 0
            otherBasePct = 0

            with open(kapaStats) as fh:
                for line in fh:
                    if line.startswith('#'):
                        continue

                    line = line.strip()
                    toks = line.split()
                    name = toks[0]
                    readCnt = int(toks[1])
                    readPct = float(toks[2][:-1])
                    baseCnt = int(toks[3])
                    basePct = float(toks[4][:-1])

                    if name == kapaIndex:
                        expectedReadCnt = readCnt
                        expectedReadPct = readPct
                        expectedBaseCnt = baseCnt
                        expectedBasePct = basePct
                    else:
                        otherReadCnt += readCnt
                        otherReadPct += readPct
                        otherBaseCnt += baseCnt
                        otherBasePct += basePct

            filesFile = RQCReadQcConfig.CFG["files_file"]
            # append_rqc_stats(filesFile, 'kapa spikein file', kapaOut, log)
            append_rqc_stats(filesFile, 'kapa spikein stats', kapaStats, log)

            statsFile = RQCReadQcConfig.CFG["stats_file"]
            append_rqc_stats(statsFile, 'kapa expected index name', kapaIndex, log)
            append_rqc_stats(statsFile, 'kapa expected index read count', expectedReadCnt, log)
            append_rqc_stats(statsFile, 'kapa expected index read percentage', expectedReadPct, log)
            append_rqc_stats(statsFile, 'kapa expected index base count', expectedBaseCnt, log)
            append_rqc_stats(statsFile, 'kapa expected index base percentage', expectedBasePct, log)

            append_rqc_stats(statsFile, 'kapa other index read count', otherReadCnt, log)
            append_rqc_stats(statsFile, 'kapa other index read ppm', otherReadPct * 1000, log)
            append_rqc_stats(statsFile, 'kapa other index base count', otherBaseCnt, log)
            append_rqc_stats(statsFile, 'kapa other index base ppm', otherBasePct * 1000, log)
        else:
            log.warning('kapa spike in detection cmd [%s] did not produced output files [%s][%s].'%(sealShCmd, kapaStats, kapaOut))

        return RQCExitCodes.JGI_SUCCESS


"""
STEP24 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_cleanup_readqc

"""
def do_cleanup_readqc(log):
    log.info("\n\n%sSTEP24 - Cleanup <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "24_cleanup_readqc in progress"
    checkpoint_step_wrapper(status)

    ## Get option
    skipCleanup = RQCReadQcConfig.CFG["skip_cleanup"]

    if not skipCleanup:
        retCode = cleanup_readqc(log)
    else:
        log.warning("File cleaning is skipped.")
        retCode = RQCExitCodes.JGI_SUCCESS

    if retCode != RQCExitCodes.JGI_SUCCESS:
        status = "24_cleanup_readqc failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        status = "24_cleanup_readqc complete"
        log.info(status)
        checkpoint_step_wrapper(status)


    return status


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program
if __name__ == "__main__":
    desc = "RQC ReadQC Pipeline"

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-f", "--fastq", dest="fastq", help="Set input fastq file (full path to fastq)", required=True)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Set output path to write to")
    parser.add_argument("-x", "--cut", dest="cutLenOfFirstBases", help="Set Read cut length (bp) for read contamination detection")
    parser.add_argument("-v", "--version", action="version", version=VERSION)

    ## For running on Crays
    parser.add_argument("-l", "--lib-name", dest="libName", help="Set library name", required=False)
    parser.add_argument("-m", "--is-multiplexed", dest="isMultiplexed", help="Set multiplexed data", required=False)
    parser.add_argument("-r", "--is-rna", dest="isRna", help="Set RNA data", required=False)

    ## Toggle options
    parser.add_argument("-b", "--skip-blast", action="store_true", help="Skip the blast run", dest="skipBlast", default=False, required=False)
    parser.add_argument("-bn", "--skip-blast-nt", action="store_true", help="Skip Blast run against nt", dest="skipBlastNt", default=False, required=False)
    parser.add_argument("-br", "--skip-blast-refseq", action="store_true", help="Skip Blast run against refseq.archaea and refseq.bateria", dest="skipBlastRefseq", default=False, required=False)
    parser.add_argument("-c", "--skip-cleanup", action="store_true", help="Skip temporary file cleanup", dest="skipCleanup", default=False, required=False)
    parser.add_argument("-p", "--pooled-analysis", action="store_true", help="Enable pooled analysis (demultiplexing)", dest="pooledAnalysis", default=False, required=False)
    parser.add_argument("-s", "--skip-subsample", action="store_true", help="Skip subsampling.", dest="skipSubsample", default=False, required=False)
    parser.add_argument("-z", "--skip-localization", action="store_true", help="Skip database localization", dest="skipDbLocalization", default=False, required=False)

    options = parser.parse_args()

    outputPath = None # output path, defaults to current working directory
    fastq = None # full path to input fastq.gz

    status = ""
    nextStepToDo = 0

    if options.outputPath:
        outputPath = os.path.abspath(options.outputPath)
    else:
        outputPath = os.getcwd()

    if options.fastq:
        fastq = options.fastq

    ## create output_directory if it doesn't exist
    if not os.path.isdir(outputPath):
        os.makedirs(outputPath)

    libName = None ## for illumina_sciclone_analysis()
    isRna = None ## for illumina_sciclone_analysis()
    isMultiplexed = None ## for illumina_generate_index_sequence_detection_plot()

    bSkipBlast = False
    bSkipBlastRefseq = False ## refseq.archaea and refseq.bacteria
    bSkipBlastNt = False
    bPooledAnalysis = False

    ## RQC-743 Need to specify the first cutting bp for read contam detection (eps. for smRNA )
    firstBptoCut = 50
    if options.cutLenOfFirstBases:
        firstBptoCut = options.cutLenOfFirstBases

    if options.libName:
        libName = options.libName


    ## switches
    if options.isRna:
        isRna = options.isRna
    if options.isMultiplexed:
        isMultiplexed = options.isMultiplexed
    if options.skipBlast:
        bSkipBlast = options.skipBlast
    if options.skipBlastRefseq:
        bSkipBlastRefseq = options.skipBlastRefseq
    if options.skipBlastNt:
        bSkipBlastNt = options.skipBlastNt
    if options.pooledAnalysis:
        bPooledAnalysis = options.pooledAnalysis


    skipSubsampling = options.skipSubsample

    ## Set readqc config
    RQCReadQcConfig.CFG["status_file"] = os.path.join(outputPath, "readqc_status.log")
    RQCReadQcConfig.CFG["files_file"] = os.path.join(outputPath, "readqc_files.tmp")
    RQCReadQcConfig.CFG["stats_file"] = os.path.join(outputPath, "readqc_stats.tmp")
    RQCReadQcConfig.CFG["output_path"] = outputPath
    RQCReadQcConfig.CFG["skip_cleanup"] = options.skipCleanup
    RQCReadQcConfig.CFG["skip_localization"] = options.skipDbLocalization
    RQCReadQcConfig.CFG["log_file"] = os.path.join(outputPath, "readqc.log")

    print "Started readqc pipeline, writing log to: %s" % (RQCReadQcConfig.CFG["log_file"])

    log = get_logger("readqc", RQCReadQcConfig.CFG["log_file"], LOG_LEVEL, False, True)
    log.info("=================================================================")
    log.info("   Read Qc Analysis (version %s)", VERSION)
    log.info("=================================================================")

    log.info("Starting %s with %s", SCRIPT_NAME, fastq)

    ## check for fastq file
    if os.path.isfile(fastq):
        log.info("Found %s, starting processing.", fastq)
        if os.path.isfile(RQCReadQcConfig.CFG["status_file"]):
            status = get_status(RQCReadQcConfig.CFG["status_file"], log)
            log.info("Latest status = %s", status)

            if status == "start":
                pass

            elif status != "complete":
                nextStepToDo = int(status.split("_")[0])
                if status.find("complete") != -1:
                    nextStepToDo += 1
                log.info("Next step to do = %s", nextStepToDo)

            else:
                ## already bDone. just exit
                log.info("Completed %s: %s", SCRIPT_NAME, fastq)
                exit(0)
        else:
            checkpoint_step_wrapper("start")
            status = get_status(RQCReadQcConfig.CFG["status_file"], log)
            log.info("Latest status = %s", status)

    else:
        log.error("%s not found, aborting!", fastq)
        exit(2)

    bDone = False
    cycle = 0
    totalReadNum = 0
    firstSubsampledFastqFileName = ""
    secondSubsampledFastqFile = ""
    totalReadCount = 0
    subsampledReadNum = 0
    bIsPaired = False
    readLength = 0
    firstSubsampledLogFile = os.path.join(outputPath, "subsample", "first_subsampled.txt")
    secondSubsampledLogFile = os.path.join(outputPath, "subsample", "second_subsampled.txt")


    if status == "complete":
        log.info("Status is complete, not processing.")
        bDone = True

    while not bDone:

        cycle += 1

        if bPooledAnalysis:
            nextStepToDo = 17
            status = "16_illumina_read_blastn_nt complete"

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 1. fast_subsample_fastq_sequences
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 1 or status == "start":
            status, firstSubsampledFastqFileName, totalReadCount, subsampledReadNum, bIsPaired, readLength = do_fast_subsample_fastq_sequences(fastq, skipSubsampling, log)

            if status.endswith("failed"):
                log.error("Subsampling failed.")
                sys.exit(-1)

            ## if not skipSubsampling and subsampledReadNum == 0: ## too small input file
            if not skipSubsampling and subsampledReadNum < RQCReadQc.ILLUMINA_MIN_NUM_READS: ## min=1000
                log.info("Too small input fastq file. Skip subsampling: total number of reads = %s, sampled number of reads = %s", totalReadCount, subsampledReadNum)
                skipSubsampling = True
                status, firstSubsampledFastqFileName, totalReadCount, subsampledReadNum, bIsPaired, readLength = do_fast_subsample_fastq_sequences(fastq, skipSubsampling, log)

                if status.endswith("failed"):
                    log.error("Subsampling failed.")
                    sys.exit(-1)

                subsampledReadNum = totalReadCount
                if subsampledReadNum >= RQCReadQc.ILLUMINA_MIN_NUM_READS:
                    status = "1_illumina_readqc_subsampling complete"

            ## Still too low number of reads -> record ILLUMINA_TOO_SMALL_NUM_READS and quit
            ##
            if subsampledReadNum == 0 or subsampledReadNum < RQCReadQc.ILLUMINA_MIN_NUM_READS: ## min=1000
                log.info("Too small number of reads (< %s). Stop processing.", RQCReadQc.ILLUMINA_MIN_NUM_READS)
                log.info("Completed %s: %s", SCRIPT_NAME, fastq)
                checkpoint_step_wrapper("complete")

                ## Add ILLUMINA_TOO_SMALL_NUM_READS=1 to stats to be used in reporting
                statsFile = RQCReadQcConfig.CFG["stats_file"]
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_TOO_SMALL_NUM_READS, "1", log)

                ## move rqc-files.tmp to rqc-files.txt
                newFilesFile = os.path.join(outputPath, "readqc_files.txt")
                newStatsFile = os.path.join(outputPath, "readqc_stats.txt")

                cmd = "mv %s %s " % (RQCReadQcConfig.CFG["files_file"], newFilesFile)
                log.info("mv cmd: %s", cmd)
                run_sh_command(cmd, True, log)

                cmd = "mv %s %s " % (RQCReadQcConfig.CFG["stats_file"], newStatsFile)
                log.info("mv cmd: %s", cmd)
                run_sh_command(cmd, True, log)

                exit(0)

        if status == "1_illumina_readqc_subsampling failed":
            bDone = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 2. write_unique_20_mers
        ## NOTE: fastq = orig input fastq,
        ##       totalReadCount = total reads in the orig input fastq
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 2 or status == "1_illumina_readqc_subsampling complete":
            ## Cope with restarting
            ## save subsamples file in "first_subsampled.txt" so that the file
            ## name can be read when started
            if not os.path.isfile(firstSubsampledLogFile):
                make_dir(os.path.join(outputPath, "subsample"))
                with open(firstSubsampledLogFile, "w") as SAMP_FH:
                    SAMP_FH.write(firstSubsampledFastqFileName + " " + str(totalReadCount) + " " + str(subsampledReadNum) + "\n")

            if firstSubsampledFastqFileName == "" and options.skipSubsample:
                if firstSubsampledFastqFileName == "" and os.path.isfile(firstSubsampledLogFile):
                    with open(firstSubsampledLogFile, "r") as SAMP_FH:
                        l = SAMP_FH.readline().strip()
                        t = l.split()
                        assert len(t) == 2 or len(t) == 3
                        firstSubsampledFastqFileName = t[0]
                        totalReadCount = t[1]
                        subsampledReadNum = t[2]
                        log.debug("firstSubsampledFastqFileName=%s, totalReadCount=%s, subsampledReadNum=%s", firstSubsampledFastqFileName, totalReadCount, subsampledReadNum)

                else:
                    nextStepToDo = 1
                    continue

            ## TODO: move getting totalReadNum in the func ???
            ##
            log.debug("firstSubsampledFastqFileName=%s, totalReadCount=%s, subsampledReadNum=%s", firstSubsampledFastqFileName, totalReadCount, subsampledReadNum)

            if totalReadCount is None or totalReadCount == 0:
                nextStepToDo = 1
                continue
            else:
                status = do_write_unique_20_mers(fastq, totalReadCount, log)

        if status == "2_unique_mers_sampling failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 3. generate read GC histograms: Illumina_read_gc
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 3 or status == "2_unique_mers_sampling complete":
            ## Read the subsampled file name
            if firstSubsampledFastqFileName == "" and os.path.isfile(firstSubsampledLogFile):
                with open(firstSubsampledLogFile, "r") as SAMP_FH:
                    firstSubsampledFastqFileName = SAMP_FH.readline().strip().split()[0]
            status = do_illumina_read_gc(firstSubsampledFastqFileName, log)

        if status == "3_illumina_read_gc failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 4. read_quality_stats
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 4 or status == "3_illumina_read_gc complete":
            if firstSubsampledFastqFileName == "" and os.path.isfile(firstSubsampledLogFile):
                with open(firstSubsampledLogFile, "r") as SAMP_FH:
                    firstSubsampledFastqFileName = SAMP_FH.readline().strip().split()[0]
            status = do_read_quality_stats(firstSubsampledFastqFileName, log)

        if status == "4_illumina_read_quality_stats failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 5. write_base_quality_stats
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 5 or status == "4_illumina_read_quality_stats complete":
            if firstSubsampledFastqFileName == "" and os.path.isfile(firstSubsampledLogFile):
                with open(firstSubsampledLogFile, "r") as SAMP_FH:
                    firstSubsampledFastqFileName = SAMP_FH.readline().strip().split()[0]
            status = do_write_base_quality_stats(firstSubsampledFastqFileName, log)

        if status == "5_illumina_read_quality_stats failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 6. illumina_count_q_score
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 6 or status == "5_illumina_read_quality_stats complete":
            if firstSubsampledFastqFileName == "" and os.path.isfile(firstSubsampledLogFile):
                with open(firstSubsampledLogFile, "r") as SAMP_FH:
                    firstSubsampledFastqFileName = SAMP_FH.readline().strip().split()[0]
            status = do_illumina_count_q_score(firstSubsampledFastqFileName, log)

        if status == "6_illumina_count_q_score failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 7. illumina_calculate_average_quality
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 7 or status == "6_illumina_count_q_score complete":
            ## Let's skip this step. (20140902)
            log.info("\n\n%sSTEP7 - Skipping 21mer analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

            status = "7_illumina_calculate_average_quality in progress"
            checkpoint_step_wrapper(status)

            status = "7_illumina_calculate_average_quality complete"
            checkpoint_step_wrapper(status)

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 8. illumina_find_common_motifs
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 8 or status == "7_illumina_calculate_average_quality complete":
            if firstSubsampledFastqFileName == "" and os.path.isfile(firstSubsampledLogFile):
                with open(firstSubsampledLogFile, "r") as SAMP_FH:
                    firstSubsampledFastqFileName = SAMP_FH.readline().strip().split()[0]
            status = do_illumina_find_common_motifs(firstSubsampledFastqFileName, log)

        if status == "8_illumina_find_common_motifs failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 9. illumina_run_dedupe
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 9 or status == "8_illumina_find_common_motifs complete":
            if firstSubsampledFastqFileName == "" and os.path.isfile(firstSubsampledLogFile):
                with open(firstSubsampledLogFile, "r") as SAMP_FH:
                    firstSubsampledFastqFileName = SAMP_FH.readline().strip().split()[0]
            status = do_illumina_run_dedupe(firstSubsampledFastqFileName, log) ## bbdedupe.sh version

        if status == "9_illumina_run_dedupe failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 10. illumina_run_tagdust
        ##
        ## NOTE: This step will be skipped. No need to run.
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 10 or status == "9_illumina_run_dedupe complete":
            ## 20131023 skip this step
            log.info("\n\n%sSTEP10 - Skipping tag dust <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
            status = "10_illumina_run_tagdust in progress"
            checkpoint_step_wrapper(status)

            status = "10_illumina_run_tagdust complete"
            checkpoint_step_wrapper(status)

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 11. illumina_detect_read_contam
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 11 or status == "10_illumina_run_tagdust complete":
            if firstSubsampledFastqFileName == "" and os.path.isfile(firstSubsampledLogFile):
                with open(firstSubsampledLogFile, "r") as SAMP_FH:
                    firstSubsampledFastqFileName = SAMP_FH.readline().strip().split()[0]
            status = do_illumina_detect_read_contam(firstSubsampledFastqFileName, firstBptoCut, log)

        if status == "11_illumina_detect_read_contam failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 12. illumina_sciclone_analysis
        ##
        ## Perform the sciclone analysis.  Eventually we will need a trigger
        ## to only perform this on sciclone-specific libraries.
        ## Uses entire fastq file rather than subset.
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 12 or status == "11_illumina_detect_read_contam complete":
            if firstSubsampledFastqFileName == "" and os.path.isfile(firstSubsampledLogFile):
                with open(firstSubsampledLogFile, "r") as SAMP_FH:
                    firstSubsampledFastqFileName = SAMP_FH.readline().strip().split()[0]

            status = do_illumina_sciclone_analysis(firstSubsampledFastqFileName, fastq, log, libName=libName, isRna=isRna)

        if status == "12_illumina_sciclone_analysis failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 13. illumina_subsampling_read_megablast
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 13 or status == "12_illumina_sciclone_analysis complete":
            if not bSkipBlast:
                if firstSubsampledFastqFileName == "" and os.path.isfile(firstSubsampledLogFile):
                    with open(firstSubsampledLogFile, "r") as SAMP_FH:
                        l = SAMP_FH.readline().strip()
                        firstSubsampledFastqFileName = l.split()[0]
                        totalReadCount = int(l.split()[1])
                        subsampledReadNum = int(l.split()[2])

                status, secondSubsampledFastqFile, second_read_cnt_total, second_read_cnt_sampled = do_illumina_subsampling_read_blastn(firstSubsampledFastqFileName, skipSubsampling, subsampledReadNum, log)
                log.debug("status=%s, secondSubsampledFastqFile=%s, second_read_cnt_total=%s, second_read_cnt_sampled=%s", status, secondSubsampledFastqFile, second_read_cnt_total, second_read_cnt_sampled)

            else:
                statsFile = RQCReadQcConfig.CFG["stats_file"]
                append_rqc_stats(statsFile, ReadqcStats.ILLUMINA_SKIP_BLAST, "1", log)
                log.info("\n\n%sSTEP13 - Run subsampling for Blast search <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
                log.info("13_illumina_subsampling_read_megablast skipped.\n")
                status = "13_illumina_subsampling_read_megablast complete"
                checkpoint_step_wrapper(status)

        if status == "13_illumina_subsampling_read_megablast failed":
            bDone = True

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # 14. illumina_read_blastn_refseq_archaea
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 14 or status == "13_illumina_subsampling_read_megablast complete":
            if not bSkipBlast and not bSkipBlastRefseq:
                if secondSubsampledFastqFile == "" and not os.path.isfile(secondSubsampledLogFile):
                    nextStepToDo = 13
                    continue

                if secondSubsampledFastqFile != "" and not os.path.isfile(secondSubsampledLogFile):
                    make_dir(os.path.join(outputPath, "subsample"))
                    with open(secondSubsampledLogFile, "w") as SAMP_FH:
                        SAMP_FH.write(secondSubsampledFastqFile + " " + str(second_read_cnt_total) + " " + str(second_read_cnt_sampled) + "\n")
                else:
                    with open(secondSubsampledLogFile, "r") as SAMP_FH:
                        l = SAMP_FH.readline().strip()
                        secondSubsampledFastqFile = l.split()[0]
                        second_read_cnt_total = int(l.split()[1])
                        second_read_cnt_sampled = int(l.split()[2])

                status = do_illumina_read_blastn_refseq_archaea(secondSubsampledFastqFile, second_read_cnt_sampled, log)
                checkpoint_step_wrapper(status)

            else:
                log.info("\n\n%sSTEP14 - Run illumina_read_blastn_refseq_archaea analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
                log.info("14_illumina_read_blastn_refseq_archaea skipped.\n")
                status = "14_illumina_read_blastn_refseq_archaea in progress"
                checkpoint_step_wrapper(status)
                status = "14_illumina_read_blastn_refseq_archaea complete"
                checkpoint_step_wrapper(status)

        if status == "14_illumina_read_blastn_refseq_archaea failed":
            bDone = True

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # 15. illumina_read_blastn_refseq_bacteria
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 15 or status == "14_illumina_read_blastn_refseq_archaea complete":
            if not bSkipBlast and not bSkipBlastRefseq:
                if secondSubsampledFastqFile == "" and not os.path.isfile(secondSubsampledLogFile):
                    nextStepToDo = 13
                    continue

                if secondSubsampledFastqFile != "" and not os.path.isfile(secondSubsampledLogFile):
                    make_dir(os.path.join(outputPath, "subsample"))
                    with open(secondSubsampledLogFile, "w") as SAMP_FH:
                        SAMP_FH.write(secondSubsampledFastqFile + " " + str(second_read_cnt_total) + " " + str(second_read_cnt_sampled) + "\n")
                else:
                    with open(secondSubsampledLogFile, "r") as SAMP_FH:
                        l = SAMP_FH.readline().strip()
                        secondSubsampledFastqFile = l.split()[0]
                        second_read_cnt_total = int(l.split()[1])
                        second_read_cnt_sampled = int(l.split()[2])

                status = do_illumina_read_blastn_refseq_bacteria(secondSubsampledFastqFile, log)
                checkpoint_step_wrapper(status)

            else:
                log.info("\n\n%sSTEP15 - Run illumina_read_blastn_refseq_bacteria analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
                log.info("15_illumina_read_blastn_refseq_bacteria skipped.\n")
                status = "15_illumina_read_blastn_refseq_bacteria in progress"
                checkpoint_step_wrapper(status)
                status = "15_illumina_read_blastn_refseq_bacteria complete"
                checkpoint_step_wrapper(status)

        if status == "15_illumina_read_blastn_refseq_bacteria failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 16. illumina_read_blastn_nt
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 16 or status == "15_illumina_read_blastn_refseq_bacteria complete":
            if not bSkipBlast and not bSkipBlastNt:
                if secondSubsampledFastqFile == "" and not os.path.isfile(secondSubsampledLogFile):
                    nextStepToDo = 13
                    continue

                if secondSubsampledFastqFile == "" and os.path.isfile(secondSubsampledLogFile):
                    with open(secondSubsampledLogFile, "r") as SAMP_FH:
                        l = SAMP_FH.readline().strip()
                        secondSubsampledFastqFile = l.split()[0]
                        second_read_cnt_total = int(l.split()[1])
                        second_read_cnt_sampled = int(l.split()[2])

                status = do_illumina_read_blastn_nt(secondSubsampledFastqFile, log)
                checkpoint_step_wrapper(status)

            else:
                log.info("\n\n%sSTEP16 - Run illumina_read_blastn_nt analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
                log.info("16_illumina_read_blastn_nt skipped.\n")
                status = "16_illumina_read_blastn_nt in progress"
                checkpoint_step_wrapper(status)
                status = "16_illumina_read_blastn_nt complete"
                checkpoint_step_wrapper(status)

        if status == "16_illumina_read_blastn_nt failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 17. multiplex_statistics
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 17 or status == "16_illumina_read_blastn_nt complete":
            status = do_illumina_multiplex_statistics(fastq, log, isMultiplexed=isMultiplexed)

        if status == "17_multiplex_statistics failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 18. end_of_read_illumina_adapter_check
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 18 or status == "17_multiplex_statistics complete":
            if firstSubsampledFastqFileName == "" and os.path.isfile(firstSubsampledLogFile):
                with open(firstSubsampledLogFile, "r") as SAMP_FH:
                    firstSubsampledFastqFileName = SAMP_FH.readline().strip().split()[0]
            status = do_end_of_read_illumina_adapter_check(firstSubsampledFastqFileName, log)

        if status == "18_end_of_read_illumina_adapter_check failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 19. insert size analysis (bbmerge.sh)
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 19 or status == "18_end_of_read_illumina_adapter_check complete":
            status = do_insert_size_analysis(fastq, log)

        if status == "19_insert_size_analysis failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 20. GC divergence analysis
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 20 or status == "19_insert_size_analysis complete":
            status = do_gc_divergence_analysis(fastq, log)

        if status == "20_gc_divergence_analysis failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 21. sketch vs nt, refseq, silva
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 21 or status == "20_gc_divergence_analysis complete":
            status = do_sketch_vs_nt_refseq_silva(fastq, log)

        if status == "21_sketch_vs_nt_refseq_silva failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 22. kapa
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 22 or status == "21_sketch_vs_nt_refseq_silva complete":
            if True: # TODO : should check ITS whether there are spikein in library
                ret = do_kapa(fastq, log)
                # if ret != RQCExitCodes.JGI_FAILURE:
                #     status = "22_kapa complete"
                # else:
                #     status = "22_kapa failed"
                status = "22_kapa complete"
            else:
                log.info("22_kapa skipped.\n")
                status = "22_kapa complete"

            checkpoint_step_wrapper(status)

        if status == "22_kapa failed":
            bDone = True

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 23. postprocessing & reporting
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 23 or status == "22_kapa complete":

            log.info("\n\n%sSTEP23 - Run illumina_readqc_report_postprocess: mv rqc-*.tmp to rqc-*.txt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
            status = "23_illumina_readqc_report_postprocess in progress"
            checkpoint_step_wrapper(status)

            ## move rqc-files.tmp to rqc-files.txt
            newFilesFile = os.path.join(outputPath, "readqc_files.txt")
            newStatsFile = os.path.join(outputPath, "readqc_stats.txt")

            cmd = "mv %s %s " % (RQCReadQcConfig.CFG["files_file"], newFilesFile)
            log.info("mv cmd: %s", cmd)
            run_sh_command(cmd, True, log)

            cmd = "mv %s %s " % (RQCReadQcConfig.CFG["stats_file"], newStatsFile)
            log.info("mv cmd: %s", cmd)
            run_sh_command(cmd, True, log)

            status = "23_illumina_readqc_report_postprocess complete"
            log.info(status)
            checkpoint_step_wrapper(status)

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 24. Cleanup
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 24 or status == "23_illumina_readqc_report_postprocess complete":
            status = do_cleanup_readqc(log)

        if status == "24_cleanup_readqc failed":
            bDone = True

        if status == "24_cleanup_readqc complete":
            status = "complete" ## FINAL COMPLETE!
            bDone = True

        ## don't cycle more than 10 times ...
        if cycle > 10:
            bDone = True

    if status != "complete":
        log.info("Status %s", status)

    else:
        log.info("\n\nCompleted %s: %s", SCRIPT_NAME, fastq)
        checkpoint_step_wrapper("complete")
        log.info("Done.")
        print "Done."


    exit(0)


## EOF
