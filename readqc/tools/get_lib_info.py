#!/usr/bin/env python

import sys
import os
import MySQLdb

# custom libs in "../lib/"
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir, '../../lib'))
from db_access import jgi_connect_db

def get_multiplex_info(seq_unit_name, log=None):

    #log.info("get_multiplex_info: %s", seq_unit_name)
    is_multiplexed = -1

    if seq_unit_name:
        seq_unit_name2 = seq_unit_name
        if seq_unit_name2.endswith(".srf"):
            seq_unit_name2 = seq_unit_name2.replace(".srf", ".fastq.gz")

        sql = "select is_multiplex from seq_units where seq_unit_name = %s"


        #log.debug("sql= %s. seq_unit_name = %s" % (sql, seq_unit_name2))
        sth.execute(sql, (seq_unit_name))
        rs = sth.fetchone()

        ## Try to get lib info again using *.fastq.gz
        if not rs:
            #log.debug("sql = %s. seq_unit_name = %s" % (sql, seq_unit_name1))
            sth.execute(sql, (seq_unit_name2))
            rs = sth.fetchone()

        if rs:
            if rs['is_multiplex']:
                is_multiplexed = 1 if rs['is_multiplex'] == 1 else 0
            else:
                is_multiplexed = 0
        else:
            #log.error("- cannot find is_multiplex information from the seq_units table for %s", seq_unit_name)
            is_multiplexed = "N/A"


    return is_multiplexed


def get_lib_info(seq_unit_name, log=None):

    #log.info("get_lib_info: %s", seq_unit_name)

    ## these fields are neeeded to look up the reference files in the seq_location_repos db
    seq_project_id = 0
    ncbi_organism_name = None
    library_name = None
    is_rna = None

    if seq_unit_name: ## ex) seq_unit_name = "7146.1.62471.CCACGC"

        ## 20131028 To fix sciclone report error
        #seq_unit_name1 = seq_unit_name
        #seq_unit_name2 = seq_unit_name
        #if seq_unit_name.endswith(".srf"):

        seq_unit_name2 = seq_unit_name
        if seq_unit_name2.endswith(".srf"):
            seq_unit_name2 = seq_unit_name2.replace(".srf", ".fastq.gz")


        sql = """
select
    su.seq_unit_id,
    lib.ncbi_organism_name,
    lib.seq_proj_id,
    lib.library_name,
    lib.sow_item_type
from seq_units su
inner join library_info lib on su.rqc_library_id = lib.library_id
where su.seq_unit_name = %s
	"""

        #log.debug("sql = %s. seq_unit_name = %s" % (sql, seq_unit_name1))

        sth.execute(sql, (seq_unit_name))
        rs = sth.fetchone()


        ## Try to get lib info again using *.fastq.gz
        if not rs:
            #log.debug("sql = %s. seq_unit_name = %s" % (sql, seq_unit_name2))
            sth.execute(sql, (seq_unit_name2))
            rs = sth.fetchone()


        if rs:
            seq_project_id = rs['seq_proj_id']
            ncbi_organism_name = rs['ncbi_organism_name']
            library_name = rs['library_name']


            if rs['sow_item_type'] and rs['sow_item_type'] != "NULL":
                if str(rs['sow_item_type']).lower() == "rna":
                    #is_rna = True
                    is_rna = 1
                else:
                    #is_rna = False
                    is_rna = 0

            else:
                if library_name.startswith('C') and len(library_name) == 4:
                    #is_rna = True
                    is_rna = 1
                else:
                    #is_rna = False
                    is_rna = 0

                #log.info("- seq_proj_id: %s, ncbi: %s, lib_name: %s, is_rna: %s", seq_project_id, ncbi_organism_name, library_name, is_rna)

        else:
            #log.error("- cannot find library information for the sequnit: %s", seq_unit_name)
            is_rna = "N/A"


    return seq_project_id, ncbi_organism_name, library_name, is_rna


if __name__ == "__main__":

    #seq_unit_name1 = "7257.1.64419.CACATTGTGAG"
    #seq_unit_name1 = "6872.5.56313.CGATGT"

    assert (len(sys.argv) == 2)
    seq_unit_name = sys.argv[1]

    if seq_unit_name.endswith(".fastq.gz") or seq_unit_name.endswith(".srf") or seq_unit_name.endswith(".fastq"):
        pass
    else:
        seq_unit_name += ".fastq.gz"

    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)

    seq_project_id, ncbi_organism_name, lib_name, is_rna = get_lib_info(seq_unit_name)
    is_multiplexed = get_multiplex_info(seq_unit_name)

    ret = "%s,%s,%s" % (lib_name, is_rna, is_multiplexed)

    print ret

    db.close()

    sys.exit(0)


