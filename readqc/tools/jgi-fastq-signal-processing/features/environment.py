from scripttest import TestFileEnvironment
import logging
logging.getLogger("requests").setLevel(logging.WARNING)

def before_scenario(context, scenario):
    context.env = TestFileEnvironment('./tmp')
