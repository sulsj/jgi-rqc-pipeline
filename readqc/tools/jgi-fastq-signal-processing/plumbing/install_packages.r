#!/usr/bin/env Rscript

if (Sys.info()['sysname'] == "Darwin"){
  options(pkgType = "binary")
}

library('packrat')
packrat::init("./vendor/r")

args <- commandArgs(trailingOnly = TRUE)
packages = read.table(args[1])
install.packages(t(packages))
