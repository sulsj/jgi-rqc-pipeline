parse_command_args <- function(){
  'usage:
    format_signal_data --input <file> --output <file> --type <type> --read <type>

  options:
    --help         Show this screen
    --input FILE   RQC generated data file
    --output FILE  Destination to write formatted metrics to
    --read READ    The input read: 1, 2 or both
    --type TYPE    The type of input data' -> doc
  docopt(doc)
}

check_args <- function(args){
  if (! is.element(args$read, c("1", "2", "both"))) {
    write(paste("Unknown input to --read option:", args$read), stderr())
    quit(status = 1)
  }
  if (! is.element(args$type, c("composition", "quality"))) {
    write(paste("Unknown input to --type option:", args$type), stderr())
    quit(status = 1)
  }
}

percent <- function(v){
  abs(round(v * 100, 2))
}

check_data_length <- function(data, args){
  if ((args$read == "both") & (dim(data)[1] %% 2 != 0)){
    write("Error: R1 and R2 do not have the same number of bases. This data likely contains errors.", stderr())
    quit(status = 1)
  }
}

format_signal <- function(data, read, type){
  f = switch(type,
        quality     = format_quality,
        composition = format_composition)
  f(data, read)
}

format_quality <- function(data, read){
  if (read == "both") {
    data <- within(data,{
      Difference_linear <- Read1_linear - Read2_linear
    })
  }

  data %>%
    mutate(position = X.BaseNum) %>%
    select(-X.BaseNum) %>%
    gather(key, value, -position)  %>%
    separate(key, into = c("read", "variable"), sep = "_") %>%
    filter(variable != "log") %>%
    mutate(read = sub("Read", "Read ", read)) %>%
    mutate(variable = sub("linear", "Quality Score", variable)) %>%
    mutate(value = round(value, 2))
}

format_composition <- function(data, read){
  if (read == "both") {
    read_length <- dim(data)[1] / 2
    var         <- c("Read 1","Read 2")
    position    <- c(1:read_length, 1:read_length)
  } else {
    read_length <- dim(data)[1]
    var         <- c(paste("Read", read))
    position    <- c(1:read_length)
  }

  data %>%
    mutate(CG   = percent(C - G), AT = percent(A - T)) %>%
    mutate(ATCG = AT + CG) %>%
    mutate(read = rep(factor(var), each = read_length)) %>%
    mutate(position = position) %>%
    select(position, read, CG, AT, ATCG) %>%
    gather(variable, value, CG, AT, ATCG) %>%
    mutate(variable = sub("ATCG", "AT+CG", variable))
}
