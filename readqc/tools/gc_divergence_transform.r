#!/usr/bin/env Rscript

library(dplyr)
library(tidyr)

args <- commandArgs(trailingOnly = TRUE)
data <- read.csv(args[1], sep = "\t")

read_length <- dim(data)[1] / 2

output <- data %>%
  mutate(CG       = (C - G) * 100, AT = (A - T) * 100) %>%
  mutate(read     = rep(factor(c("Read 1","Read 2")), each = read_length)) %>%
  mutate(position = c(1:read_length, 1:read_length)) %>%
  filter(position != read_length) %>%
  select(position, read, CG, AT) %>%
  gather(composition, value, CG:AT)

write.csv(output, row.names = FALSE)
