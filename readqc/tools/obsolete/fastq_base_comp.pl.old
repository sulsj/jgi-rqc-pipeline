#!/usr/bin/env perl


# takes fastq file and generates nucleotide base count by position and 
# N count by position plots
# 2010.09

use strict;
use warnings;

use File::Basename;

use FindBin '$RealBin';
use lib "$RealBin/../lib";


my $CAT_CMD = "cat";
my $CUT_CMD = "cut";
my $FASTX_QUALITY_STATS =  $RealBin."/fastx_quality_stats";
my $FASTX_QUALITY_STATS_PARSER = $RealBin."/fastx_quality_stats_parser.pl";
#my $GNUPLOT = "/usr/common/jgi/math/gnuplot/4.6.2/bin/gnuplot";
my $GNUPLOT = "gnuplot";
my $GREP_CMD = "grep";
my $WC_CMD = "wc";


my $in_file = $ARGV[0];

unless ( $in_file && -f $in_file ) {
    print STDERR <<END;
	usage: $0 <fastq_file>
	takes fastq file and generates a base per cycle base composition plot
 	bz2 zipped or txt file ok
END
    exit;
}

chomp $in_file;

my $stats_file = $in_file . '.base.stats';
my $gnuplot_png_file = $stats_file . '.png';

my @n_seq = `$GREP_CMD -c '\@' $in_file`;
my $ymax = $n_seq[0] * 1.1;

my $cmd .=  "$CAT_CMD $in_file"
    . " | $FASTX_QUALITY_STATS - " 
    . " | $GREP_CMD -v column "
    . " | $FASTX_QUALITY_STATS_PARSER - > $stats_file;";

system ($cmd);
my @rl = `$WC_CMD -l $stats_file | $CUT_CMD -d' ' -f1`;
my $xmax = $rl[0] + 14;

my $graph_title = basename($in_file);

my $gnuplot_cmd = qq( echo "set terminal png; set output '$gnuplot_png_file'; )
    . qq( set title 'Base Counts by Read Position: $graph_title' ; set xlabel 'Read Position'; )
    . qq( set xrange [0:$xmax]; set yrange [0:$ymax]; set ylabel 'Count'; )
    . qq( set grid; plot '$stats_file' u 1:3 w lp title 'A', )
    . qq( '$stats_file' u 1:5 w lp  title 'T', )
    . qq( '$stats_file' u 1:7 w lp  title 'G', )
    . qq( '$stats_file' u 1:9 w lp title 'C', )
    . qq( '$stats_file' u 1:11 w lp title 'N', )
    . qq( '$stats_file' u 1:2 w lp title 'All'; )
    . qq( set terminal png; replot;" | $GNUPLOT; );

my $n_gnuplot_png_file = $stats_file . '.Npercent.png';

my $n_gnuplot_cmd = qq( echo "set terminal png; set output '$n_gnuplot_png_file'; )
    . qq( set title 'Percent N by Read Position: $graph_title' ; set xlabel 'Read Position'; )
    . qq( set grid; set xrange [0:*]; set yrange [0:*]; set ylabel 'Percent'; )
    . qq( plot '$stats_file' u 1:12 w lp title 'Percent N'; )
    . qq( set terminal png; replot;" | $GNUPLOT; );

system ($gnuplot_cmd);
system ($n_gnuplot_cmd);
