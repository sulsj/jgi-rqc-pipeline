#!/usr/bin/env perl

# parses histogram2.pl output
# outputs bin_min bin_max count fraction cum_fraction

use strict;
use warnings;

my $line;
my $tot_values;
while ($line = <>) {
  if ( $line =~ /^\|/  ) {
    # it's a data line
    $line =~ /\s+([\d\.]+)\s-\s([\d\.]+)\s:\s\[\s([\d]+)\s([\d\.e\+\-]+)\s([\d\.e\+\-]+)/;
    my $bin_min = $1;
    my $bin_max = $2;
    my $count = $3;
    my $fract = $4;
    my $c_fract = $5;
    print "$bin_min\t$bin_max\t$count\t$fract\t$c_fract\n";
  }
}

