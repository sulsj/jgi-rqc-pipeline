#!/usr/bin/env perl

use strict;
use warnings;

my $n_args = @ARGV;
if (($n_args != 2) && ($n_args != 3) && ($n_args != 4)) {
    die "Usage: GCcontent.pl <input fasta> <mer size (1,2,3)> <<window size>> <<slide>>\n";
}

open(F,$ARGV[0]);
my $use_window = 0;
if ($n_args > 2) {
    $use_window = $ARGV[2];
    if ($use_window < 50) {
	die "window size must be at least 50 bp\n";
    }
}

my $slide_by = 1;
if ($n_args == 4) {
    $slide_by = $ARGV[3];
    if ($slide_by < 1) {
	die "slide value must be at least 1 bp\n";
    }
}

my $mersize = $ARGV[1];
if (($mersize != 1) && ($mersize != 2) && ($mersize != 3) && ($mersize != 4)) {
    die "mersize must be 1,2,3 or 4.\n";
}

my @bases = ("A","C","G","T");

my @mers = @bases;
my @tempmers = ();

for (my $i=1;$i<$mersize;$i++) {
    foreach(@mers) {
	my $a = $_;
	foreach(@bases) {
	    my $b = $_;
	    push(@tempmers,$a.$b);
	}
    }
    @mers = @tempmers;
    @tempmers = ();
}

my $seq = "";
my $current_entry = "NO_CURRENT_ENTRY";

while (my $i = <F>) {
    chomp $i;
    if ($i =~ /^>/) {
	if (!($current_entry eq  "NO_CURRENT_ENTRY")) {
	    
	    $seq =~ tr/acgt/ACGT/;
	    my $seq_length = length($seq);
	    my $window_size = $seq_length;
	    if ($use_window) {
		$window_size = $use_window;
	    }

	    for (my $windex = 0;
		 $windex <= $seq_length-$window_size;
		 $windex += $slide_by) {
		
		my $subseq = substr($seq,$windex,$window_size);

		my @mercounts = ();
		my $total = 0;
		foreach(@mers) {
		    my $mer = $_;
		    my $mercount = 0;
		    while ($subseq =~ /$_/g) {
			$mercount++;
		    }
#		    print "($_) $mercount ($subseq)\n";
		    push (@mercounts,$mercount);
		    $total += $mercount;
		}

		my @p = map(0,@mercounts);
		if ($total > 0) {
		    @p = map { $_/$total } @mercounts;
		}

		print "$current_entry\t$windex\t$total";
		my $nmers = @mers;
		for (my $j = 0; $j < $nmers; $j++) {
#		    printf ("\t$mers[$j]\t$mercounts[$j]\t%.4f",$p[$j]);
		    printf ("\t%.4f",$p[$j]);
		}
		print "\n";
	    }

	}
	$seq = "";
	($current_entry) = $i =~ /^>(\S+)/;
    } else {
	$seq .= $i;
    }
}

$seq =~ tr/acgt/ACGT/;
my $seq_length = length($seq);
my $window_size = $seq_length;
if ($use_window) {
    $window_size = $use_window;
}

for (my $windex = 0;
     $windex <= $seq_length-$window_size;
     $windex += $slide_by) {
    
    my $subseq = substr($seq,$windex,$window_size);

    my @mercounts = ();
    my $total = 0;
    foreach(@mers) {
	my $mercount = 0;
	while ($subseq =~ /$_/g) {
	    $mercount++;
	}
#	print "($_) $mercount ($subseq)\n";
	push (@mercounts,$mercount);
	$total += $mercount;
    }
    
    my @p = map(0,@mercounts);
    if ($total > 0) {
	@p = map { $_/$total } @mercounts;
    }
    
    print "$current_entry\t$windex\t$total";
    my $nmers = @mers;
    for (my $j = 0; $j < $nmers; $j++) {
#	printf ("\t$mers[$j]\t$mercounts[$j]\t%.4f",$p[$j]);
	printf ("\t%.4f",$p[$j]);
    }
    print "\n";
    
}

close F;
