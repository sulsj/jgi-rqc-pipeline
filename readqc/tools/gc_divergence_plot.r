#!/usr/bin/env Rscript

library(ggplot2)
library(MASS)

args <- commandArgs(trailingOnly = TRUE)
data <- read.csv(args[1])

weights       <- rep(1, dim(data)[1] / 4)
weights[1:50] <- 0

ggplot(aes(x = position, y = value, col = composition, group = composition), data = data) +
  geom_point(alpha = 0.7, size = 1) +
  geom_smooth(formula = y ~ x + I(x^2), method = rlm, col = "grey20", lty = 2, weights = weights) +
  facet_wrap(~ read) +
  scale_x_continuous("Position in read") +
  scale_y_continuous("Percentage point difference in composition", limits = c(-5, 5)) +
  theme_bw()

ggsave(args[2], width = 8, height = 5)
