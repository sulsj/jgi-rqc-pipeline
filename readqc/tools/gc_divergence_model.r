#!/usr/bin/env Rscript

args <- commandArgs(trailingOnly = TRUE)
data <- read.csv(args[1])

data <- subset(data, composition = "CG")

calculate_coef <- function(value, position){
  model          <- lm(value ~ position + I(position^2))
  quadratic_coef <- coef(model)[3]
  quadratic_coef
}

with(data, {
  calculate_coef(value, position)
})
