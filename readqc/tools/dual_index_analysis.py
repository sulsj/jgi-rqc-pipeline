#!/usr/bin/python

# -*- coding: utf-8 -*-

#
# Dual index analysis with Chee Hong's index
#
# Created: 2015.07.14
#
# sulsj (ssul@lbl.gov)
#

import sys
import getopt
import subprocess
import os
import glob

CHEE_INDEX_DICT = {"TCCGCCTT"   :   "DJ501",
"CTTCTGCC"  :   "DJ502",
"CAAGGAAC"  :   "DJ503",
"GTTCATGG"  :   "DJ504",
"TTACGGTA"  :   "DJ505",
"AAGTACCT"  :   "DJ506",
"CGCCAATT"  :   "DJ507",
"TATTGGCC"  :   "DJ508",
"AGAACGAT"  :   "DJ717",
"GCTTGCGA"  :   "DJ718",
"TCTACGCG"  :   "DJ719",
"ATCTGATA"  :   "DJ720",
"TTGAATCC"  :   "DJ701",
"AACGGATT"  :   "DJ702",
"CTCCAGCC"  :   "DJ703",
"TCTTGCTT"  :   "DJ704",
"TGGATATG"  :   "DJ705",
"CCAGATCC"  :   "DJ706",
"ATCATCTT"  :   "DJ707",
"TATGATCA"  :   "DJ708"}

def main(argv):
    inputDir = ''
    inputFile = ''
    outputFile = ''
    inputFileList = []
    
    try:
        opts, args = getopt.getopt(argv,"hd:i:o:",["idir=","ifile=","ofile="])
    except getopt.GetoptError:
        print 'dual_index_analysis.py [-d <inputDir> | -i <inputFile>] -o <outputFile>'
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print 'dual_index_analysis.py [-d <inputDir> | -i <inputFile>]-o <outputFile>'
            sys.exit()
        elif opt in ("-d", "--idir"):
            inputDir = arg
            inputFileList = glob.glob(os.path.join(inputDir, '*.gz'))
        elif opt in ("-i", "--ifile"):
            inputFile = arg
            inputFileList.append(inputFile)    
        elif opt in ("-o", "--ofile"):
            outputFile = arg

    print "Input dir file = ", inputDir, inputFileList
    print "Input fastq file = ", inputFile
    print "Demultiplex_stats_file = ", outputFile

    index_sequence_counter = {}
    cat_cmd = "zcat"
    sequence_ct = 0
 
    for anInputFile in inputFileList:
        print anInputFile
        try:
            proc = subprocess.Popen([cat_cmd, anInputFile], bufsize=2**16, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    
            while 1:
                line = proc.stdout.readline()
                if not line: break
    
                ## First line is header
                line.strip()
                header = line
    
                ## Get second line of record - sequence
                line = proc.stdout.readline()
                line.strip()
                sequence = line
    
                ## Get third line - junk
                line = proc.stdout.readline()
                line.strip()
                junk = line
    
                ## Get the final line (4th line) - quality
                line = proc.stdout.readline()
                line.strip()
                qual = line
    
                ## Parse the header
                header_fields = header.split(":")
    
                ## The last index is the index
                index_sequence = header_fields[-1].strip()
                assert(index_sequence)
    
                ## Increment the counts and store the index
                if index_sequence in index_sequence_counter:
                    index_sequence_counter[index_sequence] += 1
                else:
                    index_sequence_counter[index_sequence] = 1
    
                sequence_ct += 1
            
            print "  %d" % sequence_ct
    
        except Exception as e:
            print >>stderr, "Exception in file reading: %s" % e
            print >>stderr, "Failed to read the given fastq file [%s]" % inputFile



    STATS = open(outputFile, "w")
    ## Count the total number of indexes found
    num_indexes_found = len(index_sequence_counter)

    ## Store the data header information for printing
    report_header = """# Demultiplexing Summary
#
# Seq unit name: %s
# Total sequences: %s
# Total indexes found: %s
# 1=index_sequence 2=index_sequence_count 3=percent_of_total
#
""" % (inputFile, sequence_ct, num_indexes_found)

    STATS.write(report_header)


    ## Sort by value, descending
    for index_sequence in sorted(index_sequence_counter, key=index_sequence_counter.get, reverse=True):
        perc = float(index_sequence_counter[index_sequence]) / float(sequence_ct) * 100
        #try:
        #    l = "%s\t%s+%s\t%s\t%.6f\n" % (index_sequence, CHEE_INDEX_DICT[index_sequence[:8]], CHEE_INDEX_DICT[index_sequence[8:]], index_sequence_counter[index_sequence], perc)
        #except KeyError:
        #    print "The index, %s or %s cannot be found." % (index_sequence[:8], index_sequence[8:])
        #    continue
        l = "%s\t%s\t%.6f\n" % (index_sequence, index_sequence_counter[index_sequence], perc)

        STATS.write(l)

    STATS.close()


if __name__ == "__main__":
   main(sys.argv[1:])