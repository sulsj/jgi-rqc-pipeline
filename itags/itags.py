#!/usr/bin/env python
# -*- coding: utf-8 -*-

## DEPRECATED!!!!

#
# iTags pipeline
# (converted from Michaels' bash script)
#
# Barton's itag shell scriptola
# itag.sh [pooled lib] [spid] [primer id] [gls pru] [output dir]
# ~/code/itag.sh ABTTW 1063664 2 2-910674 /global/projectb/scratch/sulsj/itags/ABTTW
#
#    Run_id: 9086
#    GLS_physical_run_unit_id: 2-910674 /global/projectb/scratch/brycef
#    iTag ID: 2
#
# repurpose with a python script?
# - $PRIMER -> REGION provied by RQC as input
# no outputs or web page
#
# * NOTE:
#     ~/.jamo/report.py and ~/.jamo/token should be created to use "module load itagqc; jamo report itags"
#
# Created: 2015.08.20
#
# sulsj (ssul@lbl.gov)
#
# History
#
#     20140820 0.0.9: init
#
#     20151111 1.0.1: Removed unexpected characters from the library file, e.q. Cistern_spring_(boardwalk) ==> Cistern_spring_boardwalk
#
#     20151111 1.0.1: Released! 0585e15
#

import os
import sys
import time
import re
from argparse import ArgumentParser

## custom libs in "../lib/"
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir, '../lib'))    ## rqc-pipeline/lib
sys.path.append(os.path.join(dir, '../tools'))  ## rqc-pipeline/tools

from os_utility import make_dir, back_ticks, run_sh_command
from common import get_logger, checkpoint_step, append_rqc_file, append_rqc_stats, post_mortem_cmd


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## qsub functions

# module load uge
#def submit_qsub(qsub_opt, qsub_cmd, qsub_name, output_path):
#
#    #log.info("submit_qsub")
#
#    job_id = 0
#    job_share = 150
#
#    cluster_project = "gentech-rqc.p"
#    qsub_path = os.path.join(output_path, "qsub")
#
#    if not os.path.isdir(qsub_path):
#        os.makedirs(qsub_path)
#
#    output_log = os.path.join(qsub_path, "qsub-%s.log" % qsub_name)
#
#    my_qsub = "qsub -b y -j y -m n -w e -terse -N %s -P %s -o %s -js %s %s '%s'" % (qsub_name, cluster_project, output_log, job_share, qsub_opt, qsub_cmd)
#    # append to qsub.txt
#
#    cmd = "module load uge;%s" % my_qsub
#    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)
#    post_mortem_cmd(cmd, exitCode, stdOut, stdErr, log)
#
#
#    if exitCode == 0:
#        job_id = int(stdOut.strip())
#
#    log.info("- cluster job id: %s", job_id)
#
#    if job_id > 0:
#        qsub_log = os.path.join(output_path, "qsub_list.txt")
#        fh = open(qsub_log, "a")
#        fh.write("%s,%s\n" % (job_id, "submitted"))
#        fh.close()
#
#    return job_id


# watch jobs running on the cluster
def watch_cluster(output_path, log):
    log.info("watch_cluster")

    is_job_complete = "/usr/common/usg/bin/isjobcomplete.new" # + job_id = nice way to ask if job is done
    qsub_log = os.path.join(output_path, "qsub_list.txt")

    sleep_time = 300 # check isjobrunning every xx seconds

    hb_max = (180 * 3600) / sleep_time # total number of heartbeats before we give up, 180 hours worth of run time
    #hb_max = 5 # temp

    done = 0
    hb_cnt = 0 # heartbeat count

    if not os.path.isfile(qsub_log):
        done = 1

    while done == 0:

        hb_cnt += 1
        log.info("- heartbeat: %s", hb_cnt)

        qsub_list = []


        qsub_cnt = 0
        qsub_complete = 0
        qsub_err = 0
        fh = open(qsub_log, "r")
        for line in fh:

            qsub_list.append(line.strip())
            qsub_cnt += 1
        fh.close()

        fh = open(qsub_log, "w")


        for qsub in qsub_list:

            job_id, status = qsub.split(",")
            new_status = status

            if status in ("complete", "fail"):
                continue
            else:
                cmd = "%s %s" % (is_job_complete, job_id)
                stdOut, _, _ = run_sh_command(cmd, True, log)
                #post_mortem_cmd(cmd, exitCode, stdOut, stdErr, log)

                running = "%s queued/running" % job_id
                not_running = "%s not queued/running" % job_id
                error_qw = "%s queued/running/error" % job_id



                if stdOut.strip == running:
                    new_status = "running"

                elif stdOut.strip() == not_running:
                    new_status = "complete" # might have failed
                    qsub_complete += 1

                elif stdOut.strip() == error_qw:
                    new_status = "error"
                    qsub_err += 1

                if stdOut.strip() == "not queued/running":
                    new_status = "complete"

            fh.write("%s,%s\n" % (job_id, new_status))
            log.info("- job_id: %s, status: %s", job_id, new_status)



        fh.close()

        qsub_running = qsub_cnt - (qsub_complete + qsub_err)
        log.info("- job count: %s, running: %s, err: %s", qsub_cnt, qsub_running, qsub_err)

        if qsub_cnt == (qsub_complete + qsub_err):
            done = 1




        if hb_cnt >= hb_max:
            done = 1

        if done == 0:
            time.sleep(sleep_time)




## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

scriptName = __file__

if __name__ == "__main__":
    my_name = "iTags Pipeline"
    version = "1.0.1"

    ## itag.sh [pooled lib] [spid] [primer id] [gls pru] [output dir]
    ## ex) ~/code/itag.sh ABTTW 1063664 2 2-910674 /global/projectb/scratch/brycef/itag/ABTTW

    usage = "prog [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)

    desc = 'itags'
    parser = ArgumentParser(description=desc, usage = usage)
    parser.add_argument("-p", "--pooled-lib", dest="pooledLib", help="Pooled lib", required=True)
    parser.add_argument("-s", "--spid", dest="spid", help="spid", required=True)
    parser.add_argument("-r", "--primer-id", dest="primerId", help = "Primer ID", required=True)
    parser.add_argument("-g", "--gls-pru", dest="glsPru", help = "gls pru", required=True)
    parser.add_argument("-o", "--output-dir", dest="outputDir", help = "Output directory", required=False)
    parser.add_argument('-v', '--version', action='version', version=version)

    options = parser.parse_args()

    outputDir = None
    pooledLib = options.pooledLib
    spid = options.spid
    primerId = options.primerId
    glsPru = options.glsPru

    if options.outputDir:
        outputDir = options.outputDir
        make_dir(outputDir)
    else:
        outputDir = os.getcwd()

    # initialize my logger
    logFile = os.path.join(outputDir, "itags.log")

    # log = logging object
    logLevel = "INFO"
    log = get_logger("itags", logFile, logLevel, False, True)

    log.info("%s", 80 * "~")
    log.info("Starting %s", scriptName)

    log.info("")

    libraryFile = None
    statusFile = None
    statsFile = None
    filesFIle = None

    libraryFile = os.path.join(outputDir, "pooledLib.txt")
    statusFile = os.path.join(outputDir, "itag_status.txt")
    filesFile = os.path.join(outputDir, "rqc-files.tmp")
    statsFile = os.path.join(outputDir, "rqc-stats.tmp")

    back_ticks(["touch", filesFile])
    back_ticks(["touch", statsFile])

    #EnvironmentModules.module(['load', "jamo"])
    #EnvironmentModules.module(['load', "itagqc"])

    #status = "itags start"
    #checkpoint_step(statusFile, status)

    step = 1

    ## Step 1: jamo report itags
    if step == 1:
        log.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        log.info("STAGE 1")

        status = "jamo report itags in progress"
        checkpoint_step(statusFile, status)

        cmd = """module load jamo itagqc; jamo report itags \
custom \
metadata.fastq_type = sdm_normal and \
metadata.sow_segment.sequencing_project_id = %s and \
metadata.sow_segment.itag_primer_set_id = %s and \
metadata.physical_run_unit.gls_physical_run_unit_id = %s \
| sort \
> %s""" % (spid, primerId, glsPru, libraryFile)

        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)

        if exitCode != 0:
            log.error("jamo report itags command execution failed")
            status = "jamo report itags failed"
            checkpoint_step(statusFile, status)
            sys.exit(1)

        ## RQCSUPPORT-593
        ## Remove unexpected characters from the library file, e.q. Cistern_spring_(boardwalk) ==> Cistern_spring_boardwalk
        ##
        with open(libraryFile, "r") as inF:
            with open(libraryFile+".new", "w") as outF:
                for line in inF.readlines():
                    if line:
                        newHeader = re.sub('[^A-Za-z0-9_]', '', line.split()[0])
                        outF.write(newHeader + "\t" + line.split()[1] + '\n')

        cmd = "cp %s %s && rm -rf %s" % (libraryFile+".new", libraryFile, libraryFile+".new")

        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)

        if exitCode != 0:
            log.error("sed: library file cleaning failed")
            status = "jamo report itags failed"
            checkpoint_step(statusFile, status)
            sys.exit(1)

        status = "jamo report itags complete"
        checkpoint_step(statusFile, status)

        step = 2

    ## Step 2: itag setup
    if step == 2:

        log.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        log.info("STAGE 2")

        status = "itag setup in progress"
        checkpoint_step(statusFile, status)

        region = None
        if primerId == '1':
            region = "16S-V4"
        elif primerId == '2':
            region = "16S-V4"
        elif primerId == '4':
            region = "18S-V4"
        elif primerId == '5':
            region = "ITS2"

        cmd = """module load jamo itagqc; itagqc-init \
--library_file %s \
--identifier %s \
--directory %s \
--email ssul@lbl.gov \
--project_type=%s""" % (libraryFile, glsPru, os.path.join(outputDir, "setup"), region)

        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)

        if exitCode != 0:
            log.error("itag setup command execution failed")
            status = "itag setup failed"
            checkpoint_step(statusFile, status)
            sys.exit(1)

        status = "itag setup complete"
        checkpoint_step(statusFile, status)

        step = 3

    ## Step 3: jamo report itags_project_metadata custom
    if step == 3:
        log.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        log.info("STAGE 3")

        status = "jamo report itags_project_metadata custom in progress"
        checkpoint_step(statusFile, status)

        cmd = """module load jamo itagqc; jamo report itags_project_metadata custom \
metadata.fastq_type=sdm_normal and \
metadata.sequencing_project.sequencing_project_id=%s and \
metadata.physical_run_unit.gls_physical_run_unit_id=%s and \
metadata.sow_segment.itag_primer_set_id=%s \
> %s/project.yaml""" % (spid, glsPru, primerId, os.path.join(outputDir, "setup"))

        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)

        if exitCode != 0:
            log.error("jamo report itags_project_metadata custom command execution failed")
            status = "jamo report itags_project_metadata custom failed"
            checkpoint_step(statusFile, status)
            sys.exit(1)

        status = "jamo report itags_project_metadata custom complete"
        checkpoint_step(statusFile, status)

        step = 4

    ## Step 4: qsub
    if step == 4:
        log.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        log.info("STAGE 4")

        status = "qsub in progress"
        checkpoint_step(statusFile, status)

        ## qsub bin/itagger
        ## NOTE: qsub log will be created under setup/log
        cmd = "qsub %s" % (os.path.join(outputDir, "setup/bin/itagger"))
        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)

        if exitCode != 0:
            log.error("qsub itager command execution failed")
            status = "qsub itager failed"
            checkpoint_step(statusFile, status)
            sys.exit(1)
        else:
            job_id = int(stdOut.strip().split()[2]) ## To parse out job id from 'Your job 12894715 ("itagger.2-1027998") has been submitted'
            if job_id > 0:
                log.info("- cluster job id: %s", job_id)
                qsub_log = os.path.join(outputDir, "qsub_list.txt")
                fh = open(qsub_log, "a")
                fh.write("%s,%s\n" % (job_id, "submitted"))
                fh.close()
            else:
                log.error("- invalid itagger job id: %s", job_id)
                sys.exit(1)


        ## don't run this unless we want to release it: Seung-Jin, include this step but don't run it during your tests
        ## qsub -hold_jid itagger.$UNIT_ID bin/package
        ## NOTE: qsub log will be created under setup/log
        cmd = "qsub -hold_jid itagger.%s %s" % (glsPru, os.path.join(outputDir, "setup/bin/package"))

        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)

        if exitCode != 0:
            log.error("qsub package command execution failed")
            status = "qsub package failed"
            checkpoint_step(statusFile, status)
            sys.exit(1)
        else:
            job_id = int(stdOut.strip().split()[2])
            if job_id > 0:
                log.info("- cluster job id: %s", job_id)
                qsub_log = os.path.join(outputDir, "qsub_list.txt")
                fh = open(qsub_log, "a")
                fh.write("%s,%s\n" % (job_id, "submitted"))
                fh.close()
            else:
                log.error("- invalid package job id: %s", job_id)
                sys.exit(1)


        ## wait for all jobs to finish
        watch_cluster(outputDir, log)

        status = "qsub complete"
        checkpoint_step(statusFile, status)



    status = "complete"
    checkpoint_step(statusFile, status)

    if status == "complete":
        ## move rqc-files.tmp to rqc-files.txt
        newFilesFile = os.path.join(outputDir, "rqc-files.txt")
        cmd = "mv %s %s" % (filesFile, newFilesFile)
        log.info("- cmd: %s", cmd)
        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)
        post_mortem_cmd(cmd, exitCode, stdOut, stdErr, log)

        newStatsFile = os.path.join(outputDir, "rqc-stats.txt")
        cmd = "mv %s %s" % (statsFile, newStatsFile)
        log.info("- cmd: %s", cmd)
        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)
        post_mortem_cmd(cmd, exitCode, stdOut, stdErr, log)


    log.info("Completed %s", scriptName)

    sys.exit(0)


## EOF