#!/bin/bash
# Barton's itag shell scriptola
# itag.sh [pooled lib] [spid] [primer id] [gls pru] [output dir]
# ~/code/itag.sh ABTTW 1063664 2 2-910674 /global/projectb/scratch/brycef/itag/ABTTW
#
#Run_id: 9086
#GLS_physical_run_unit_id: 2-910674 /global/projectb/scratch/brycef
#iTag ID: 2

# repurpose with a python script?
# - $PRIMER -> REGION provied by RQC as input
# no outputs or web page

set -o nounset
set -o errexit

readonly LIBRARY=$1
readonly SPID=$2
readonly PRIMER=$3
readonly GLS_RUN_UNIT=$4
readonly OUTDIR=$5
#readonly LIBRARY_FILE=`mktemp`
readonly LIBRARY_FILE="${OUTDIR}/${LIBRARY}.txt"

#readonly DIR="${OUTDIR}/${LIBRARY}_${SPID}_${PRIMER}_${GLS_RUN_UNIT}"
readonly DIR=$OUTDIR
mkdir -p $DIR

STATUS_FILE="${OUTDIR}/itag_status.txt"




touch "${OUTDIR}/rqc-stats.txt"
touch "${OUTDIR}/rqc-files.txt"


module load jamo itagqc





jamo report itags \
  custom \
  metadata.fastq_type = sdm_normal and \
  metadata.sow_segment.sequencing_project_id = ${SPID} and \
  metadata.sow_segment.itag_primer_set_id = ${PRIMER} and \
  metadata.physical_run_unit.gls_physical_run_unit_id = ${GLS_RUN_UNIT} \
  | sort \
  > ${LIBRARY_FILE}

case $PRIMER in
  1) REGION="16S-V4";;
  2) REGION="16S-V4";;
  4) REGION="18S-V4";;
  5) REGION="ITS2";;
esac

# itag setup

itagqc-init \
  --library_file $LIBRARY_FILE \
  --identifier $GLS_RUN_UNIT \
  --directory $DIR \
  --email brycefoster@lbl.gov \
  --project_type=$REGION

#rm ${LIBRARY_FILE}



jamo report itags_project_metadata custom \
  metadata.fastq_type=sdm_normal and \
  metadata.sequencing_project.sequencing_project_id=${SPID} and \
  metadata.physical_run_unit.gls_physical_run_unit_id=${GLS_RUN_UNIT} and \
  metadata.sow_segment.itag_primer_set_id=${PRIMER} \
  > $DIR/project.yaml

cd $DIR


# qsub params in the itagger shell script created by itagqc-init
qsub bin/itagger

# don't run this unless we want to release it: Seung-Jin, include this step but don't run it during your tests
#qsub -hold_jid itagger.$UNIT_ID bin/package


exit 0

