#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Itagger 2.0 pipeline wrapper
* uses usearch license which allows only 1 instance to be running at a time!
- Alex looking to fix this, Ed would need to do a lot of work

Dependencies:
    ITAGGER_MODULE = "itagqc/3.x" # Michael's code
    ITAG_MODULE = "itagger/2.2" # Ed Kirton's code - has other dependencies
    JAMO_MODULE = "jamo"
    JQ_MODULE = "jq"


2017-04-12: Amplicon sequencing being phased out at JGI

v 1.0: 2016-08-31
- initial version

v 1.0.1: 2016-09-06
- added itagger/2.0 module per Michael

v 1.0.2: 2016-09-16
- reporting on merged reads, check for files for clustering and jamo submit to not run them again

v 1.0.3: 2016-09-29
- added separate step to do the qc submission and the jat submission
- added check for seqobs files

v 1.0.4: 2016-10-03
- added step for prepare and setup a check to run the complete step, only runs complete if user == qc_user

v 1.0.5: 2016-10-17
- added pru_id to checking seq obs (run 10889 & run 10373 - different pru_ids but same primer and spid)

v 1.0.6: 2016-11-21
- changed to itagger/2.1 module
- 8 threads for "prepare" step

v 1.0.7: 2016-12-14
- took off 8 threads for "prepare" step - messing up prepares?

v 1.0.8: 2016-12-15
- README.txt was missing sample details section, added "CLEAN" step before PREPARE so we can re-run itagger to do JAMO without re-running itagger

v 1.1: 2016-12-22
- work with multiple spids/gls_ids/du_ids for custom runs

v 1.1.1: 2017-03-14
- added "ns" flag to prevent jat submission if R&D run

v 1.1.2: 2017-04-04
- added check if seqobs file is PURGED
- changed to itagger/2.2 - usearch fix for lanes 1,5,9

v 1.1.3: 2017-08-07
- RQCSUPPORT-1421 - add at_id to metadata.json

v 1.1.4: 2017-09-11
- close SOW items through web service call
- use run_cmd

v 1.2: 2018-01-18
- denovo updates
- rewrote Michael's stupid make file into itag_prepare_jamo_new and itag_submit_jamo_new

v 1.2.1: 2018-02-22
- create empty contam.otu.fasta.* files if missing

******************************


If failure, (du = 2645,2 seq obs) its okay, the collabs probably just want the otu (taxonomic classifications) which can be sent
- leave as failed in RQC, when run as combined it should work


Will get a "threads" error if running on gpint2* because Michael's script looks for the genepool $(NSLOTS) var.
- qlogin instead and run

Tests (run on qlogin or cluster, not gpint*)
t1 = BAOZU
t2 = 1095844
* can run tests w/o JAMO submission if not qc_user.  Qc_user gets "live" set automatically which does the JAT submission
t1 = 2614
t2 = 2644
To do:
- tests to check the output (Behave/girkin?)
- failure check for clustering, jamo steps
- run without providing the du_id, doing pru-id etc fails

$ qsub -b yes -j yes -m as -now no -w e -N itag-t1 -l exclusive.c=1 -l ram.c=3.5G,h_vmem=3.5G,h_rt=43200,s_rt=43195 -o /global/projectb/scratch/brycef/itag2/itag-t1.log -js 508 "/global/homes/b/brycef/git/jgi-rqc-pipeline/itags/itagger2.py -o t1"
$ qsub -b yes -j yes -m as -now no -w e -N itag-t2 -l exclusive.c=1 -l ram.c=3.5G,h_vmem=3.5G,h_rt=43200,s_rt=43195 -o /global/projectb/scratch/brycef/itag2/itag-t2.log -js 507 "/global/homes/b/brycef/git/jgi-rqc-pipeline/itags/itagger2.py -o t2"

* everything in the make file could be in this pipeline ...

Using
usearch v8.1.1861_i86linux64, 264Gb RAM, 32 cores
- 9.1 is out but Ed hasn't upgraded - more significant upgrade (2017-01-05)

- clustering calls itag_calculate_sequence_observations.sh
--- itagger/2.1 itaggerReadQc.pl (/usr/common/jgi/pkmeco/itagger/2.0/bin/itaggerReadQc.pl)
--- Ed Kirton's script
--- Ed could split the itagger into 2 parts so we only needed to run one part on one node that uses usearch and the other could run in parallel for multiple plates
------ not worth it right now, a plate takes at most 1 hr to process
- Ed uses usearch for its clustering ability, not so much for alignment, and its published

SSU metagenomics, next-generation reads are clustered into Operational Taxonomic Units (OTUs).
more info: http://www.drive5.com/usearch/manual/

clustering: calls itagger.pl
itagger.pl - Ed's script which calls usearch on seqobs.txt

- itag_import_sequence_observations.sh
--- creates metadata.json file and then does jat import

Can resubmit submissions (sequnits/submissions.json) using:

$ curl --header "Content-Type: application/json" --data-binary "@submission2.json" https://rqc.jgi-psf.org/api/sow_qc/submit
* submission2 has only those not submitted the first time, otherwise doesn't work
- use query to write sed command to update submission2.json, then edit submission2 to remove the COMPLETEOCMPLETE... records
$ cp submission.json submission2.json
select s.seq_unit_name, s.library_name, qc.qc_state, l.itag_primer_set, l.seq_proj_id
from seq_units s
left join seq_unit_qc qc on qc.seq_unit_name = s.seq_unit_name
left join library_info l on s.rqc_library_id = l.library_id
where l.seq_proj_id = 1112832 and l.itag_primer_set = 'its2'
sed -i s/11047.1.188351.TAAGGCCTATC.fastq.gz/COMPLETECOMPLETECOMPLETECOMPLETE/ submission2.json

# good info
$ cat  /usr/common/jgi/pkmeco/itagger/2.0/bin/README.txt

cd to itag path:
$ module load itagger/2.1
$ itagger.pl --jgi seqobs.json --out clustering --region 16S-V4-V5 --threads 32

Itagger2 also runs the QC script for everything in the pool

To test clustering.mk:
cd to the path then ...
~/git/itag-qc/bin/clustering.mk PARENT_LIBRARY=BNNGW PRU_ID=2-1679665 SPID=1118223 PRIMER_SET=16S-V4-V5 clustering
~/git/itag-qc/bin/clustering.mk PARENT_LIBRARY=BNNGW PRU_ID=2-1679665 SPID=1118223 PRIMER_SET=16S-V4-V5 prepare

# remove files to try to make README again ...
~/git/itag-qc/bin/clustering.mk PARENT_LIBRARY=BNNGW PRU_ID=2-1679665 SPID=1118223 PRIMER_SET=16S-V4-V5 clean


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

itag ap: https://issues.jgi-psf.org/browse/RQCSUPPORT-963
qlogin
$ cd ~/git/jgi-rqc-pipeline/itags
$ ./itagger2.py -o $SCRATCH/itag2/1134744 -du 1991,2600
$ ./itagger2.py -o $SCRATCH/itag2/1134748 -du 2045,2646
$ ./itagger2.py -o $SCRATCH/itag2/1134746 -du 1981,2669
$ cd jamo; jat import itags metadata.json > submission.txt



~~~~~~~~~~~~~~~~~~~[ Test Cases ]
-o t1, -o t2

$ jat import itags metadata.json
- 2018-01-18 - worked!

denovo
$ sbatch /global/homes/b/brycef/git/jgi-rqc-pipeline/itags/itag-t1d.sh

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
import glob
import json
from argparse import ArgumentParser

import collections
import getpass

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))
sys.path.append(os.path.join(my_path, '../sag'))
from common import get_logger, checkpoint_step, append_rqc_file, append_rqc_stats, get_colors, get_msg_settings, get_analysis_project_id, run_cmd
from micro_lib import get_web_page


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Count number of libraries, number of filtered pipeline complete, qc_failures,
Verify primer set, product type
* rqc_setup also checks this (check_itag_files)
https://rqc.jgi-psf.org/api/rqcws/itag_filter_check/1991
'''
def filter_pipeline_status_check():
    log.info("filter_pipeline_status_check: %s", output_path)

    step_name = "filter_pipeline_status_check"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    #curl = Curl(RQC_URL)

    filter_cnt = 0
    filter_complete = 0
    filter_fail = 0
    qc_fail = 0

    du_list = str(du_id).split(",")

    for my_du_id in du_list:
        rqc_itag_filter_api = "%s/%s" % ("api/rqcws/itag_filter_check", my_du_id)
        log.info("- calling %s/%s", RQC_URL, rqc_itag_filter_api)
        
        response = get_web_page(RQC_URL, rqc_itag_filter_api, log)

        if response:
            # check if the number of seq units filtered == number of filter pipelines complete + qc failures && filter_cnt > 0
            if 'filter_cnt' in response:

                filter_cnt += response['filter_cnt']
                filter_complete += response['filter_complete']
                filter_fail += response['filter_failed']
                qc_fail += response['qc_fail']



    status = "%s failed" % step_name
    m = msg_fail

    if filter_cnt > 0:
        if filter_complete + qc_fail >= filter_cnt:
            status = "%s complete" % step_name
            m = msg_ok

    log.info("- filter total: %s, filter complete: %s, filter failed: %s, qc fail: %s  %s", filter_cnt, filter_complete, filter_fail, qc_fail, m)
    checkpoint_step(status_log, status)

    return status, filter_complete


'''
Run check that we have the correct number of seqobs files in jamo (and no duplicates)
- filter_cnt = number of filtered files created (some might be qc fail and not get a seqobs)
- need gls_pru_id also as a unique_id (spid 1095844, 16s-v4-v5-pna, run 10889, 10373 = different run)
'''
def seqobs_check(spid, primer_set, pru_id, filter_cnt):
    log.info("seqobs_check: spid = %s, itag_primer = %s, pru_id = %s, filter_cnt = %s", spid, primer_set,  pru_id, filter_cnt)
    step_name = "seqobs_check"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    seqobs_cnt = 0
    seqobs_coll = collections.Counter()

    # convert vars "1092708,1125756" to jamo vars (1092708 1125756)
    primer_set_jamo = "(%s)" % primer_set.replace(",", " ")
    spid_jamo = "(%s)" % str(spid).replace(",", " ")
    pru_id_jamo = "(%s)" % pru_id.replace(",", " ")

    cmd = "#jamo;jamo report select _id, file_name, file_path, file_status where metadata.jat_label = 'sequence_observations' and metadata.itag_primer_set in '%s' and metadata.sequencing_project_id in '%s' and metadata.gls_physical_run_unit_id in '%s' as json" % (primer_set_jamo, spid_jamo, pru_id_jamo)
    #jamo report select _id, file_name where metadata.jat_label = 'sequence_observations' and metadata.itag_primer_set = 16S-V4 and metadata.sequencing_project_id in '(1092708 1125756)' and metadata.gls_physical_run_unit_id in '(2-1282868 2-1655283)' as json
    std_out, std_err, exit_code = run_cmd(cmd, log)

    seqobs_json = json.loads(std_out)
    for seqob in seqobs_json:
        seqob_file = os.path.join(seqob['file_path'], seqob['file_name'])
        if os.path.isfile(seqob_file):
            seqobs_cnt += 1
            seqobs_coll[seqob['file_name']] += 1
        else:
            log.error("- seqobs file PURGED: %s  %s", seqob['file_name'], msg_fail)



    status = "%s complete" % step_name

    msg = msg_ok
    if filter_cnt != seqobs_cnt:
        msg = msg_fail
        status = "%s failed" % step_name

    log.info("- filter files: %s, seqobs: %s  %s", filter_cnt, seqobs_cnt, msg)

    msg = msg_ok
    duplicate_cnt = 0
    for seqob in seqobs_coll:
        if seqobs_coll[seqob] > 1:
            log.info("- duplicate seqob: %s  %s", seqob, msg_fail)
            duplicate_cnt += 1

    if duplicate_cnt > 0:
        msg = msg_fail
        status = "%s failed" % step_name

    log.info("- duplicate seq obs: %s  %s", duplicate_cnt, msg)

    checkpoint_step(status_log, status)

    return status


'''
Fucking michael's shit make script doesn't fucking work

'''
def itag_cluster_new(pool_lib, pru_id, spid, primer_set):
    log.info("itag_cluster_new: pool = %s, pru_id = %s, spid = %s, primer_set = %s", pool_lib, pru_id, spid, primer_set)

    step_name = "itag_cluster"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    itag_status = "failed"

    itagger_file = os.path.join(output_path, "clustering", "otu", "align.txt")
    if os.path.isfile(itagger_file):
        log.info("- itagger ran, not re-running")
        itag_status = "complete"

    else:

        os.chdir(output_path)

        # create input file for itagger
        seqobs_json = os.path.join(output_path, "seqobs.json")
        cmd = "#jamo; jamo report select metadata.seq_unit_name, metadata.sample_name, metadata.sample_id, file_path, file_name"
        cmd += " where metadata.itag_primer_set = '%s' and metadata.sequencing_project_id = '%s' and metadata.gls_physical_run_unit_id = '%s'"
        cmd += " and metadata.parent_library_name = '%s' and metadata.jat_label = 'sequence_observations' as json"
        cmd += " | iconv -f utf-8 -t ascii//translit -c > %s"

        cmd = cmd % (primer_set, spid, pru_id, pool_lib, seqobs_json)
        std_out, std_err, exit_code = run_cmd(cmd, log)



        # run itagger.pl - does clustering
        #itagger.pl --jgi $1 --out $2 --region $3 --threads $4
        itagger_log = os.path.join(output_path, "itag-clustering.log")
        threads = 32
        #/global/homes/b/brycef/git/itag-qc/bin/cluster.sh /global/projectb/scratch/brycef/itag2/t1/seqobs.json clustering 16S-V4-V5 32
        cmd = "%s/bin/cluster.sh %s %s %s %s > %s 2>&1" % (itag_folder, seqobs_json, "clustering", primer_set, threads, itagger_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        
        
        if exit_code == 0:
            itag_status = "complete"

        cmd = "touch clustering/otu/unk.otu.fasta clustering/otu/contam.otu.fasta clustering/otu/unk.otu.fasta.obs clustering/otu/contam.otu.fasta.obs"
        std_out, std_err, exit_code = run_cmd(cmd, log)


    status = "%s %s" % (step_name, itag_status)
    checkpoint_step(status_log, status)


    return status

'''
Setup files for jamo
* converted from itag-qc/bin/cluster.mk
'''
def itag_prepare_jamo_new(pool_lib, pru_id, spid, primer_set):
    log.info("itag_prepare_jamo_new: pool = %s, pru_id = %s, spid = %s, primer_set = %s", pool_lib, pru_id, spid, primer_set)


    step_name = "itag_prepare_jamo"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)



    # create clean folders
    jamo_path = os.path.join(output_path, JAMO_PATH)
    if os.path.isdir(jamo_path):
        cmd = "rm -rf %s" % jamo_path
        std_out, std_err, exit_code = run_cmd(cmd, log)

    if not os.path.isdir(jamo_path):
        os.makedirs(jamo_path)

    # create project info.json

    portal_name_api = "ext-api/genome-admin/getPortalIdByParameter?parameterName=jgiProjectId\\&parameterValue=%s" % spid
    # doesn't work - returns "406" error?
    #response = get_web_page(PORTAL_URL, portal_name_api, log)
    portal_file = os.path.join(jamo_path, "portal_name.txt")
    cmd = "curl --silent %s/%s > %s" % (PORTAL_URL, portal_name_api, portal_file)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    portal_name = ""
    fh = open(portal_file, "r")
    for line in fh:
        portal_name = line.strip()
    fh.close()

    if not portal_name:
        log.error("- no portal name for spid: %s", spid)
        sys.exit(8)


    # create project_json file
    project_template = "%s/template/project.template" % itag_folder
    project_json = os.path.join(output_path, "project.json")
    cmd = "#jamo;jamo report select file_name, metadata.library_name, metadata.proposal.default_project_manager.first_name,"
    cmd += " metadata.proposal.default_project_manager.last_name, metadata.proposal.default_project_manager.email_address,"
    cmd += " metadata.proposal.pi.email_address, metadata.proposal.pi.first_name, metadata.proposal.pi.last_name, metadata.proposal_id, metadata.proposal.title,"
    cmd += " metadata.physical_run_unit.gls_physical_run_unit_id, metadata.parent_library_name, metadata.sow_segment.itag_primer_set,"
    cmd += " metadata.sow_segment.itag_primer_set_id, metadata.sequencing_project.sequencing_project_id, metadata.sequencing_project.sequencing_project_name"
    cmd += " where metadata.sow_segment.itag_primer_set = '%s' and metadata.parent_library_name = '%s'"
    cmd += " and metadata.physical_run_unit.gls_physical_run_unit_id = '%s' and metadata.sequencing_project_id = '%s'"
    cmd += " and metadata.sow_segment is not null and metadata.fastq_type = sdm_normal as json"
    cmd += " | jq --from-file %s --arg portal %s > %s"
    cmd = cmd % (primer_set, pool_lib, pru_id, spid, project_template, portal_name, project_json)
    std_out, std_err, exit_code = run_cmd(cmd, log)




    log.info("- copying over clustering files")

    # tar up core_diversity_analysis folder

    core_file = os.path.join(output_path, "clustering", "otu", "core_diversity_analyses.tar.gz")
    if not os.path.isfile(core_file):
        cmd = "cd %s;tar -czf core_diversity_analyses.tar.gz core_diversity_analyses/*" % (os.path.join(output_path, "clustering", "otu"))
        std_out, std_err, exit_code = run_cmd(cmd, log)

    # copy from clustering/otu, gzip *.fasta & align.txt
    file_list = glob.glob(os.path.join(output_path, "clustering", "otu/*"))

    for my_file in file_list:
        if os.path.isfile(my_file):

            gzip_flag = False
            if my_file.endswith(".fasta") or os.path.basename(my_file) == "align.txt":
                gzip_flag = True

            new_file = os.path.join(jamo_path, os.path.basename(my_file))

            cmd = "cp %s %s" % (my_file, new_file)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            if gzip_flag:

                cmd = "#pigz;pigz -f %s" % (new_file)
                std_out, std_err, exit_code = run_cmd(cmd, log)


    # copy files from clustering - README.txt, config.ini
    file_list = glob.glob(os.path.join(output_path, "clustering/*"))

    for my_file in file_list:
        if os.path.isfile(my_file):

            new_file = os.path.join(jamo_path, os.path.basename(my_file))
            # README.txt -> methods.txt
            if os.path.basename(my_file) == "README.txt":
                new_file = os.path.join(jamo_path, "methods.txt")
            cmd = "cp %s %s" % (my_file, new_file)
            std_out, std_err, exit_code = run_cmd(cmd, log)



    log.info("- creating data.json for each seq unit")
    qc_list = [] # for submission.json
    data_list = [] # for sample_data.json

    seq_unit_template = "%s/template/seq_unit_data.template" % itag_folder
    qc_template = "%s/template/sequence_qc.template" % itag_folder
    submission_template = "%s/template/submission.template" % itag_folder
    rqc_dump_api = "api/rqcws/dump"

    su_path = os.path.join(output_path, "sequnits")
    #if os.path.isdir(su_path):
    #    cmd = "rm -rf %s" % su_path
    #    std_out, std_err, exit_code = run_cmd(cmd, log)

    seq_obs_file = os.path.join(output_path, "seqobs.json")
    jfh = open(seq_obs_file)
    seq_obs_json = json.load(jfh)
    jfh.close()



    # paralleize this part?
    for seq_ob in seq_obs_json:
        seq_unit = seq_ob['metadata.seq_unit_name']
        log.info("--- seq_unit: %s", seq_unit)

        seq_path = os.path.join(output_path, SEQ_PATH, seq_unit)
        if not os.path.isdir(seq_path):
            os.makedirs(seq_path)

        # skip download if it exists
        raw_json = os.path.join(seq_path, "raw.json")
        if not os.path.isfile(raw_json):

            log.info("- curl: %s/%s/%s", RQC_URL, rqc_dump_api, seq_unit)
            response = get_web_page(RQC_URL, "%s/%s" % (rqc_dump_api, seq_unit), log)

            with open(raw_json, 'w') as outfile:
                json.dump(response, outfile)


        data_json = os.path.join(seq_path, "data.json")
        cmd = "jq --from-file %s %s > %s" % (seq_unit_template, raw_json, data_json)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        data_list.append(data_json)

        # qc.json
        qc_json = os.path.join(seq_path, "qc.json")
        cmd = "jq --from-file %s %s > %s" % (qc_template, raw_json, qc_json)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        qc_list.append(qc_json)






    log.info("- rework submission.json")
    submission_json = os.path.join(output_path, SEQ_PATH, "submission.json")

    cmd = "jq --from-file %s --slurp %s > %s" % (submission_template, " ".join(qc_list), submission_json)
    std_out, std_err, exit_code = run_cmd(cmd, log)


    log.info("- sample_data.json")
    sample_data_json = os.path.join(jamo_path, "sample_data.json") # combined  sequnit/*/data.json

    cmd = "jq . --slurp %s > %s" % (" ".join(data_list), sample_data_json)
    std_out, std_err, exit_code = run_cmd(cmd, log)


    log.info("- README.txt") #**
    # create README.txt - needs sample_data.json, project.json
    readme_txt = os.path.join(jamo_path, "README.txt")
    readme_template = "%s/template/letter.template" % itag_folder

    cmd = "jq --raw-output --slurp --exit-status --from-file %s %s %s > %s"  % (readme_template, project_json, sample_data_json, readme_txt)
    std_out, std_err, exit_code = run_cmd(cmd, log)


    log.info("- metadata.json")
    metadata_json = os.path.join(jamo_path, "metadata.json")
    metadata_template = "%s/template/metadata.template" % itag_folder
    # should be named with analysis project id prepended ...
    cmd = "jq --raw-output --exit-status --from-file %s %s > %s" % (metadata_template, project_json, metadata_json)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    log.info("- library.txt")
    library_txt = os.path.join(jamo_path, "library.txt")
    cmd = "jq -r 'map([.[\"metadata.sample_name\"], .[\"metadata.seq_unit_name\"]] | join(\"\t\")) | join(\"\n\")' %s > %s" % (seq_obs_file, library_txt)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    # sometimes contam.otu.fasta.* is missing, create empty files in jamo path
    contam_gz = os.path.join(jamo_path, "contam.otu.fasta.gz")
    if not os.path.isfile(contam_gz):
        contam_fasta = contam_gz.replace(".gz", "")
        cmd = "touch %s" % (contam_fasta)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        cmd = "gzip %s" % (contam_fasta)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        
    contam_obs = os.path.join(jamo_path, "contam.otu.fasta.obs")
    if not os.path.isfile(contam_obs):
        cmd = "touch %s" % contam_obs
        std_out, std_err, exit_code = run_cmd(cmd, log)
        
    tat_dict = {
        "16s-v4" : 37,
        "its2" : 58,
        "16s-v4-pna" : 59,
        "18s-v4" : 60,
        "16s-v4-v5" : 70,
        "16s-v4-v5-pna" : 71,
        "custom" : 68, # shouldn't be used
        "16s-v4-ver2" : 75,
        "16s-v4-ver2-pna" : 76,
    }
    tatid = tat_dict.get(primer_set.lower(), 0)

    # create a symlink to at_id.metadata.json   (RQCSUPPORT-1417)
    ap_id, at_id = get_analysis_project_id(spid, 30, tatid, output_path, log)

    log.info("- ap id: %s, at id: %s", ap_id, at_id)


    status = "%s complete" % step_name

    checkpoint_step(status_log, status)

    return status

'''
Do Itag jat submission and rework submission - only if qc_user

'''
def itag_submit_jamo_new(pool_lib, pru_id, spid, primer_set):
    log.info("itag_submit_jamo_new: pool = %s, pru_id = %s, spid = %s, primer_set = %s", pool_lib, pru_id, spid, primer_set)

    step_name = "itag_submit_jamo"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    jamo_path = os.path.join(output_path, JAMO_PATH)
    metadata_json = os.path.join(output_path, JAMO_PATH, "metadata.json")
    err_cnt = 0

    if os.path.isfile(metadata_json):
        jcmd = "module load jamo/dev"
        submitted_file = os.path.join(jamo_path, "submitted-dev.txt")
        
        if uname == "qc_user":
            jcmd = "#jamo"
            submitted_file = os.path.join(jamo_path, "submitted.txt")
            log.info("- submitting to jamo production  %s", msg_ok)

        if os.path.isfile(submitted_file):
            log.info("- jat submission completed previously, skipping  %s", msg_warn)
        else:

            # jamo/dev or jamo both send email
            cmd = "%s;cd %s;jat import itags metadata.json > %s" % (jcmd, jamo_path, submitted_file)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            if exit_code > 0:
                err_cnt += 1

    else:
        log.error("- jat metadata.json missing!  %s", msg_fail)
        err_cnt += 1



    submission_json = os.path.join(output_path, SEQ_PATH, "submission.json")
    submitted_file = os.path.join(output_path, SEQ_PATH, "submitted.txt")

    if os.path.isfile(submission_json):

        if os.path.isfile(submitted_file):
            log.info("- rework has been submitted previously, skipping  %s", msg_warn)
        else:

            cmd = "curl --header \"Content-Type: application/json\" --data-binary \"@%s\" %s/api/sow_qc/submit" % (submission_json, RQC_URL)
            if uname == "qc_user":
                std_out, std_err, exit_code = run_cmd(cmd, log)
                if exit_code > 0:
                    err_cnt += 1
            else:
                log.info("- rework cmd (not run): %s", cmd)


    else:
        log.error("- submission.json missing for rework!  %s", msg_fail)
        err_cnt += 1

    # close sow items

    if uname == "qc_user":
        if du_id:
            log.info("- close sow items for %s", du_id)
            cmd = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc/rqc_system/bin/rqc_monitor.py --cmd itag-%s" % (du_id)
            std_out, std_err, exit_code2 = run_cmd(cmd, log)

    status = "%s complete" % step_name
    if err_cnt > 0:
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)
    return status


'''
module load itagqc/3.x jamo itagger/2.0

# Runs the clustering step, likely needs an exclusive node for around 12-24 hours
clustering.mk PARENT_LIBRARY=... PRU_ID=... SPID=... PRIMER_SET=... clustering
 * deprecated 2018-01-18
'''
def itag_cluster(pool_lib, pru_id, spid, primer_set):
    log.info("itag_cluster: pool = %s, pru_id = %s, spid = %s, primer_set = %s", pool_lib, pru_id, spid, primer_set)

    step_name = "itag_cluster"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    project_json = os.path.join(output_path, "project.json") # this file is created at the end of the clustering step

    exit_code = 0

    if os.path.isfile(project_json):
        log.info("- skipping clustering, found project.json file")

    else:

        # make file for clustering
        cmd = "cd %s;clustering.mk PARENT_LIBRARY=%s PRU_ID=%s SPID=%s PRIMER_SET=%s clustering" % (output_path, pool_lib, pru_id, spid, primer_set)
        std_out, std_err, exit_code = run_cmd(cmd, log)



    # failure check?
    status = "%s complete" % step_name
    if exit_code > 0:
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)


    return status


'''
- do clean step first so we can re-run the prepare step and not need to re-do the itagger clustering

clustering.mk PARENT_LIBRARY=... PRU_ID=... SPID=... PRIMER_SET=... clean
clustering.mk PARENT_LIBRARY=... PRU_ID=... SPID=... PRIMER_SET=... prepare
 * deprecated 2018-01-18
'''
def itag_prepare_jamo(pool_lib, pru_id, spid, primer_set):
    log.info("itag_prepare_jamo: pool = %s, pru_id = %s, spid = %s, primer_set = %s", pool_lib, pru_id, spid, primer_set)

    step_name = "itag_prepare_jamo"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    # don't re-prepare if we found submitted - this is in JAMO already
    submitted_file = os.path.join(output_path, "jamo", "submitted.txt")
    exit_code = 0
    if os.path.isfile(submitted_file):
        log.info("- skipping jamo prepare, found jamo/submitted.txt file")
    else:


        # clean out the JAMO
        cmd = "cd %s;clustering.mk PARENT_LIBRARY=%s PRU_ID=%s SPID=%s PRIMER_SET=%s clean" % (output_path, pool_lib, pru_id, spid, primer_set)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        # make file which makes the jat metadata and the rqc qc data for submissions
        # --jobs 8 - automatically parallelize 8 threads, took out - might be messing up final steps "--jobs 8"
        cmd = "cd %s;clustering.mk PARENT_LIBRARY=%s PRU_ID=%s SPID=%s PRIMER_SET=%s prepare" % (output_path, pool_lib, pru_id, spid, primer_set)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # do the actual submission (can turn off for dev mode)

        # look up id for analysis task

        tat_dict = {
            "16s-v4" : 37,
            "its2" : 58,
            "16s-v4-pna" : 59,
            "18s-v4" : 60,
            "16s-v4-v5" : 70,
            "16s-v4-v5-pna" : 71,
            "custom" : 68, # shouldn't be used
            "16s-v4-ver2" : 75,
            "16s-v4-ver2-pna" : 76,
        }
        tatid = tat_dict.get(primer_set.lower(), 0)

        # create a symlink to at_id.metadata.json   (RQCSUPPORT-1417)
        ap_id, at_id = get_analysis_project_id(spid, 30, tatid, output_path, log)
        metadata_json = os.path.join(output_path, "jamo", "metadata.json")
        new_metadata_json = os.path.join(output_path, "jamo", "%s.metadata.json" % at_id)
        cmd = "ln -s %s %s" % (metadata_json, new_metadata_json)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    # failure check?
    status = "%s complete" % step_name
    if exit_code > 0:
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)
    return status



'''
# Compresses all the output files and loads into jamo. Can probably be run on a single node in the long queue
clustering.mk PARENT_LIBRARY=... PRU_ID=... SPID=... PRIMER_SET=... complete
 * deprecated 2018-01-18
'''
def itag_submit_jamo(pool_lib, pru_id, spid, primer_set, mode):
    log.info("itag_submit_jamo: pool = %s, pru_id = %s, spid = %s, primer_set = %s", pool_lib, pru_id, spid, primer_set)

    step_name = "itag_submit_jamo"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    submitted_file = os.path.join(output_path, "jamo", "submitted.txt")
    exit_code = 0
    if os.path.isfile(submitted_file):
        log.info("- skipping jamo submit, found jamo/submitted.txt file")
    else:

        if mode == "live":

            # make file which makes the jat metadata and the rqc qc data for submissions
            cmd = "cd %s;clustering.mk PARENT_LIBRARY=%s PRU_ID=%s SPID=%s PRIMER_SET=%s complete" % (output_path, pool_lib, pru_id, spid, primer_set)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            # do the actual submission (can turn off for dev mode)

            # close SOW status
            # rqc_system/bin/rqc_monitor.py --cmd itag-3658
            if du_id:
                cmd = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc/rqc_system/bin/rqc_monitor.py --cmd itag-%s" % (du_id)
                std_out, std_err, exit_code2 = run_cmd(cmd, log)


        else:
            log.info("- not running complete step, mode = %s  %s", mode, msg_warn)

    # failure check?
    status = "%s complete" % step_name
    if exit_code > 0:
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)
    return status


'''
Save data for reporting for RQC
- sequnit/(seq unit name)/data.json

'''
def save_itag_data():
    log.info("save_itag_data")

    step_name = "save_itag_data"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    seq_units_folder = os.path.join(output_path, "sequnit")

    if os.path.isdir(seq_units_folder):
        seq_unit_list = glob.glob(os.path.join(seq_units_folder, "*"))

        summary_file = os.path.join(output_path, "seq_unit_summary.txt")
        fh = open(summary_file, "w")
        a = ['seq_unit_name', 'sample_id', 'sample_name', 'raw', 'low_quality_removed', 'sequencing_adaptor_removed', 'artifacts_removed', 'initial', 'merged', 'primer_trimmed', 'final', 'unique']

        fh.write("#" + "|".join(a) + "\n")

        # each is a folder with data.json
        for seq_unit in seq_unit_list:
            #print seq_unit
            data_json = os.path.join(seq_unit, "data.json")
            if os.path.isfile(data_json):
                #print data_json
                jfh = open(data_json)
                # pass in a file handle
                itag_json = json.load(jfh)


                # flatten the json data
                itag_dict = {
                    "sample_id" : "",
                    "sample_name" : "",
                    "raw" : 0,
                    "low_quality_removed" : 0,
                    "sequencing_adaptor_removed" : 0,
                    "artifacts_removed" : 0,
                    "initial" : 0,
                    "merged" : 0,
                    "primer_trimmed" : 0,
                    "final" : 0,
                    "unique" : 0,
                }


                seq_unit_name = itag_json['metadata']['sequence_unit_name']

                for i in itag_json['metadata']:
                    if i in itag_dict:
                        itag_dict[i] = itag_json['metadata'][i]

                for i in itag_json['sequencing']['read_filtering']:
                    if i in itag_dict:
                        itag_dict[i] = itag_json['sequencing']['read_filtering'][i]

                for i in itag_json['sequencing']['itagger_read_processing']:
                    if i in itag_dict:
                        itag_dict[i] = itag_json['sequencing']['itagger_read_processing'][i]


                for k in itag_dict:
                    key = "%s_%s" % (k, seq_unit_name)
                    log.info("- %s: %s", key, itag_dict[k])

                    append_rqc_stats(rqc_stats_log, key, itag_dict[k])


                #print itag_dict
                listola = [seq_unit_name]
                for _ in a:
                    if _ == 'seq_unit_name':
                        continue

                    # join only works with str values
                    listola.append(str(itag_dict[_]))

                fh.write("|".join(listola) + "\n")


        fh.close()
        append_rqc_file(rqc_file_log, "seq_unit_summary", summary_file)

    status = "%s complete" % step_name

    checkpoint_step(status_log, status)

    return status



def do_purge():
    log.info("do_purge")

    # nothing to purge at the moment
    step_name = "purge"
    status = "%s complete" % step_name

    checkpoint_step(status_log, status)

    return status


## ----------------------
## control/helper functions

'''
Curl call to RQC website to get the itag metadata from the data_unit_id
https://rqc.jgi-psf.org/api/rqcws/itag/2408
'''
def get_itag_data(du_id):
    log.info("get_itag_data: %s", du_id)

    pool_lib_list = []
    spid_list = []
    pru_id_list = []
    primer_set_list = []

    pool_lib = ""
    spid = ""
    pru_id = ""
    primer_set = ""

    du_id_list = str(du_id).split(",")
    for my_du_id in du_id_list:

        rqc_itag_api = "%s/%s" % ("api/rqcws/itag", my_du_id)

        response = get_web_page(RQC_URL, rqc_itag_api, log)

        if response:
            if 'data_name' in response:
                log.info("- du_id: %s, spid = %s, pru_id = %s, pool_lib = %s, itag_primer_set = %s", my_du_id, response['spid'], response['pru_id'], response['pool_lib'], response['itag_primer_set'])
                pool_lib_list.append(response['pool_lib'])
                spid_list.append(str(response['spid']))
                pru_id_list.append(response['pru_id'])
                primer_set_list.append(response['itag_primer_set'])
            else:
                log.error("- cannot find itag data for du_id: %s  %s", du_id, msg_fail)



    primer_set_list = list(set(primer_set_list))

    # if len(primer_set_list) > 1:
    # die?

    pool_lib = ",".join(pool_lib_list)
    spid = ",".join(spid_list)
    pru_id = ",".join(pru_id_list)
    primer_set = ",".join(primer_set_list)

    log.info("- pool_lib: %s, spid: %s, pru_id: %s, primer_set: %s", pool_lib, spid, pru_id, primer_set)

    return pool_lib, spid, pru_id, primer_set

'''
Get the next thing to run
'''
def get_next_status(status):
    log.info("get_next_step: %s", status)

    next_status = None

    # this could be part of the config file
    status_dict = {
        # assembly
        "start" : "filter_pipeline_status_check",
        "filter_pipeline_status_check" : "seqobs_check",
        "seqobs_check" : "itag_cluster",
        "itag_cluster" : "itag_prepare_jamo",
        "itag_prepare_jamo" : "itag_submit_jamo",
        "itag_submit_jamo" : "save_itag_data",
        "save_itag_data" : "purge",
        "purge" : "complete",

        "failed" : "failed",
        "complete" : "complete",
    }


    if status:

        if status.lower() != "complete":
            status = status.lower().replace("skipped", "").replace("complete", "").strip()

        if status in status_dict:
            next_status = status_dict[status]

        else:
            if status.endswith("failed"):
                next_status = "failed"

            else:
                next_status = "failed: next step missing for '%s'" % (status)
                log.error("Cannot get the next step for '%s'  %s", status, msg_fail)
                sys.exit(12)



    log.info("- next step: %s%s%s -> %s%s%s", color['pink'], status, color[''], color['cyan'], next_status, color[''])

    #if stop_flag:
    #    next_status = "stop"

    # debugging
    if next_status == "stop":
        log.info("- stopping pipeline, stop flag is set")
        sys.exit(4)


    return next_status


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "iTagger-Wrapper"
    version = "1.2.1"

    #RQC_URL = "https://rqc-1.jgi-psf.org" # Bryce
    RQC_URL = "https://rqc.jgi-psf.org" # prod
    PORTAL_URL = "https://genome.jgi.doe.gov"

    SEQ_PATH = "sequnits"
    JAMO_PATH = "jamo"

    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)



    # Parse options
    usage = "itagger2.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
find the parent library name, PRU id, sequencing project id and itag primer set by looking up the RQC data_unit_id
$ itagger2.py -du (data_unit_id,data_unit_id)

    """

    parser = ArgumentParser(usage = usage)

    uname = getpass.getuser()


    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-du", "--data-unit-id", dest="du_id", help = "RQC data unit ids (looks up pru, spid, pooled lib, primer set)")
    parser.add_argument("-l", "--lib", dest="library_name", help = "Parent library name (required)")
    parser.add_argument("-pru", "--pru-id", dest="pru_id", help = "PRU Id (physical run unit id) (required)")
    parser.add_argument("-spid", "--spid", dest="spid", help = "Sequencing project id (required)")
    parser.add_argument("-p", "--primer-set", dest="primer_set", help = "iTag primer set (required)")
    parser.add_argument("-ns", "--no-submit", dest="no_submit", default=False, action="store_true", help = "Don't automatically do JAT submission (qc_user only)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)


    status = "start" # step + X = run step and exit?  (e.g. gc_covX)  -- to do
    output_path = None
    pool_lib = None
    pru_id = None
    spid = None
    primer_set = None
    submit_flag = False

    mode = "dev" # live = submit to jamo
    # 2018-01-18 - only do submissions if uname == qc_user - in *_new functions
    if uname == "qc_user":
        mode = "live"


    itag_folder = "/global/dna/projectdirs/PI/rqc/prod/itag-qc/"
    if uname == "brycef":
        itag_folder = "/global/homes/b/brycef/git/itag-qc/"

    du_id = "" # RQC.data_units.data_unit_id - can be a csv list as input like "2162,2166" for combo itag runs


    # parse args
    args = parser.parse_args()
    print_log = args.print_log
    if args.du_id:
        du_id = args.du_id

    if args.library_name:
        pool_lib = args.library_name

    if args.output_path:
        output_path = args.output_path

    if args.pru_id:
        pru_id = args.pru_id

    submit_flag = args.no_submit
    if submit_flag:
        mode = "no submit"


    if args.spid:
        spid = args.spid

    if args.primer_set:
        primer_set = args.primer_set



    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/itag2"

        if output_path == "t1":

            # 84 seq units
            #insert into data_units set data_name = 'iTag-10700-1', data_type1 = 'iTags', str_param1 = '2-263566', str_param2 = 'BBYBA', int_param1 = 1039665, int_param2 = 10700, parameter3 = 7, priority = 200, data_unit_status_id = 3, dt_created = now();
            #du_id = 2408

            #BAOZU = 2166
            du_id = 2166
            # du_id = 2133 = 10461 which is spid 1039665, 16-V4-V5 also??  AP says 16s-V4: ap_tool.py -spid 1039665
            #du_id = 2614
            #du_id = 2613
            #du_id = 2707
            du_id = 2912


            du_id = 2944 # 2995
            du_id = 2995

            du_id = 3186 # 10 minutes
            du_id = 3955

        if output_path == "t2":

            # run 10721, 2 spids
            pool_lib = "BBWPP"
            pru_id = "2-270091"
            spid = 1100005
            primer_set = "16S-V4"

            # BGPAS
            du_id = 2440
            # BCYPW
            du_id = 2438
            # 1095844
            du_id = 2499
            du_id = 2644
            du_id = 2645 # 2 seq units, failing - failed with --jobs 8?

            #du_id = 2667

            #1100005	16S-V4	BBWPP - 88 libs
            # BBSWH part of child lib
            #insert into data_units set data_name = 'iTag-10721-1', data_type1 = 'iTags', str_param1 = '2-270091', str_param2 = 'BBWPP', int_param1 = 1100005, int_param2 = 10721, parameter3 = 1, priority = 200, data_unit_status_id = 3, dt_created = now();

        if output_path == "t3":
            #1134744 = 1092708,1125756 = 16s-V4/1
            #du_id = [1991, 2600]
            du_id = "1991,2600"

            #spid = "1092708,1125756"
            #primer_set = "16S-V4"
            #pool_lib = "BNAXG,AYHYG"
            #pru_id = "2-1655283,2-1282868"

            """
select s.seq_unit_name, s.gls_physical_run_unit_id, l.itag_primer_set, l.itag_primer_set_id
from seq_units s
inner join library_info l on s.rqc_library_id = l.library_id
where
l.seq_proj_id in (1125756, 1092708)
and l.itag_primer_set_id = 5
            """

        output_path = os.path.join(test_path, output_path)


    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)



    # initialize my logger
    log_file = os.path.join(output_path, "itag2.log")

    # log = logging object
    log_level = "INFO"
    log = get_logger("itag2", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)


    # get du_id params
    if du_id > 0:
        pool_lib, spid, pru_id, primer_set = get_itag_data(du_id)


    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "pool_lib", pool_lib)
    log.info("%25s      %s", "spid", spid)
    log.info("%25s      %s", "pru_id", pru_id)
    log.info("%25s      %s", "primer_set", primer_set)
    log.info("%25s      %s", "mode", mode)
    log.info("")


    if pool_lib and spid and pru_id and primer_set:
        pass
    else:
        log.error("- missing parameters to run %s, exiting  %s", my_name, msg_fail)
        log.error("- pool_lib = %s, spid = %s, pru_id = %s, primer_set = %s", pool_lib, spid, pru_id, primer_set)
        status = "failed"
        checkpoint_step(status_log, status)
        sys.exit(2)


    status_log = os.path.join(output_path, "status.log")
    rqc_file_log = os.path.join(output_path, "rqc-files.tmp")
    rqc_stats_log = os.path.join(output_path, "rqc-stats.tmp")




    # test
    #filter_pipeline_status_check()
    #seqobs_check(spid, primer_set, 90)
    #ap_id, at_id = get_analysis_project_id(spid, 30,70, output_path, log)
    #itag_prepare_jamo_new(pool_lib, pru_id, spid, primer_set)
    #save_itag_data()
    #sys.exit(48)

    append_rqc_stats(rqc_stats_log, "pool_lib", pool_lib)
    append_rqc_stats(rqc_stats_log, "pru_id", pru_id)
    append_rqc_stats(rqc_stats_log, "spid", spid)
    append_rqc_stats(rqc_stats_log, "primer_set", primer_set)


    checkpoint_step(status_log, "## New Run")


    done = False
    cycle_cnt = 0
    filter_cnt = 0 # number of filtered files - expect this many seqobs
    while not done:

        cycle_cnt += 1

        # assembly -> contig trimming
        if status == "start":
            # check status of all pipelines
            status, filter_cnt = filter_pipeline_status_check()

        if status == "seqobs_check":
            status = seqobs_check(spid, primer_set, pru_id, filter_cnt)

        if status == "itag_cluster":
            #status = itag_cluster(pool_lib, pru_id, spid, primer_set)
            status = itag_cluster_new(pool_lib, pru_id, spid, primer_set)

        if status == "itag_prepare_jamo":
        #    status = itag_prepare_jamo(pool_lib, pru_id, spid, primer_set)
            status = itag_prepare_jamo_new(pool_lib, pru_id, spid, primer_set)


        if status == "itag_submit_jamo":
        #    status = itag_submit_jamo(pool_lib, pru_id, spid, primer_set, mode)
            if mode == "no submit":
                log.info("- skipping jamo/rework submission  %s", msg_warn)
                status = "itag_submit_jamo complete"
            else:
                status = itag_submit_jamo_new(pool_lib, pru_id, spid, primer_set)

        if status == "save_itag_data":
            status = save_itag_data()

        if status == "purge":
            status = do_purge()

        if status == "complete":
            done = True

        if status == "failed":
            done = True

        if cycle_cnt > 10:
            log.info("- max cycles reached.  %s", msg_warn)
            done = True

        status = get_next_status(status)


    checkpoint_step(status_log, status)
    if status == "complete":

        # move rqc-files.tmp to rqc-files.txt
        append_rqc_stats(rqc_stats_log, "itag_version", "%s %s" % (my_name, version))

        # no files to process
        #rqc_new_file_log = os.path.join(output_path, "rqc-files.txt")

        #cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        #std_out, std_err, exit_code = run_cmd(cmd, log)

        rqc_new_stats_log = os.path.join(output_path, "rqc-stats.txt")

        cmd = "mv %s %s" % (rqc_stats_log, rqc_new_stats_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    log.info("Completed %s", script_name)

    sys.exit(0)


"""

                      .-.
         .-._    _.../   `,    _.-.
         |   `'-'    \     \_'`   |        Ash nazg durbatuluk,          One Ring to rule them all,
         \            '.__,/ `\_.--,       Ash nazg gimbatul,            One Ring to find them,
          /                '._/     |      Ash nazg thrakatuluk,         One Ring to bring them all,
         /                    '.    /      Agh burzum-ishi krimpatul.    And in the darkness bind them.
        ;   _                  _'--;
     '--|- (_)       __       (_) -|--'
     .--|-          (__)          -|--.
      .-\-                        -/-.
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`

metadata/yaml:
utax_alignments = align.txt.gz
utax_classification = tax.tsv
otu_fasta = otu.fasta.gz
otu_observations = otu.fasta.obs
contaminating_otu_fasta = contam.otu.fasta.gz
contaminating_otu_observations= contam.otu.fasta.obs
unknown_otu_fasta = unk.otu.fasta.gz
unknown_otu_observations= unk.otu.fasta.obs
clustering_log = log.txt
qiime_mapping = mapping.tsv
species = otu.tsv
species_biom = otu.biom
otu_alignment = msa.fasta.gz
otu_tree = otu.tre
taxonomy_report = core_diversity_analysis.tar.gz
libraries = library.txt
itagger_config = config.ini
readme = README.txt
sample_summary = sample_data.json
methods = methods.txt

OTUs - operational taxonomic units - clusters of sequences that differ by less than a fixed dissimilarity threshold ~3%
DADA2: high resolution sample inference from Illumina amplicon data, R package on github
- Nature Methods Vol 13 N7 July 2016 - p581
DADA - 454 data



ROOT/otu/align.txt = UTAX classification alignments
ROOT/otu/tax.tsv = UTAX classification table
ROOT/otu/otu.fasta = final OTU centroid sequences
ROOT/otu/otu.fasta.obs = final OTU centroid observations per sample table
ROOT/otu/log.txt = summary report
ROOT/otu/mapping.txt = QIIME mapping file
ROOT/otu/otu.tsv = OTU table of abundances, including taxonomy
ROOT/otu/otu.biom = OTU abundance+tax table in BIOM format
ROOT/otu/msa.fasta = multiple sequence alignment of final centroids
ROOT/otu/otu.tre = phylogenetic tree of final centroids
ROOT/otu/core_diversity_analyses/ = folder containing QIIME core diversity analyses output

To resend the email:
$ cd jamo
$ cat README.txt | mail -s "Itags release: 16S-V4 - Metagenomic reconstruction of novel archaeal genomes from energy-limited hypersaline sediments." cppennacchio@lbl.gov
$ cat README.txt | mail -s "Itags release: 16S-V4 - Metagenomic reconstruction of novel archaeal genomes from energy-limited hypersaline sediments." valentine@geol.ucsb.edu
* use Itags proposal name & primer set


"""
