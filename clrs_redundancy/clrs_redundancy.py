#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
CLRS Redundancy Analysis
- try to measure coverage without a reference genome
- paired ended runs only
- takes one library or seq unit (filtered!)
* JGI does not do LMP libraries anymore (CLRS, Nextera, LMP, CLIP): 2017-06-02

1.0.5 - 2014-03-03
- added try/except block for counting kmers
~/code/my-qsub-himem.sh frag-TSAT '/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/clrs_redundancy.py --lib TSAT --mode inward --output-path /global/projectb/scratch/brycef/tmp2/clrs/TSAT > /global/projectb/scratch/brycef/tmp2/clrs/TSAT.log 2>&1'
~/code/my-qsub.sh frag-TSAT '/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/clrs_redundancy.py --lib TSAT --mode inward --output-path /global/projectb/scratch/brycef/tmp2/clrs/TSAT2 > /global/projectb/scratch/brycef/tmp2/clrs/TSAT2.log 2>&1'
- changed normalize function to report correctly

1.1 - 2014-03-14
- fixed the calculation, wasn't counting reads correctly
- added normalized redundancy to output

1.2 - 2015-08-06
- changed from gnuplot to matplotlib

1.2.1 - 2017-06-02
- updated to work on Denovo


2017-01-04 - CLRS libraries not made at JGI anymore, pipeline is deprecated


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

rm -rf /global/projectb/scratch/brycef/tmp2/NCWT /global/projectb/scratch/brycef/tmp2/NTAT

NTAT: circular = 42.57%, inward = 85.15% - 3.3m reads in filtered file, inward = 42.42% March 4
$ ~/code/my-qsub.sh frag-ntat "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/clrs_redundancy.py --lib NTAT -o /global/projectb/scratch/brycef/tmp2/NTAT"
$ ./clrs_redundancy.py --lib NTAT -o /global/projectb/scratch/brycef/tmp2/NTAT

NCWT: circular = 28.20%, inward = 61.40% - 1.5m reads ... inward = 30.71% March 4
$ ~/code/my-qsub.sh frag-ncwt "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/clrs_redundancy.py --lib NCWT -o /global/projectb/scratch/brycef/tmp2/NCWT"
- alternatively, with the path to the filtered fastq:
./clrs_redundancy.py -f /global/dna/dm_archive/rqc/pipelines/filter/archive/02/78/35/59/6626.5.48984.GTGAAA.acdnq.fastq.gz -o /global/projectb/scratch/brycef/tmp2/NCWT2


~/code/my-qsub.sh frag-ohgo "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/frag_align.py --lib OHGO > /global/projectb/scratch/brycef/tmp2/frag/OHGO2.log 2>&1"
~/code/my-qsub.sh frag-ohgc "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/frag_align.py --lib OHGC > /global/projectb/scratch/brycef/tmp2/frag/OHGC2.log 2>&1"


~/code/my-qsub.sh frag-ohgoc "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/clrs_redundancy.py --lib OHGO -m circular -o /global/projectb/scratch/brycef/tmp2/OHGO-c > /global/projectb/scratch/brycef/tmp2/OHGO-c.log 2>&1"
~/code/my-qsub.sh frag-ohgcc "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/clrs_redundancy.py --lib OHGC -m circular -o /global/projectb/scratch/brycef/tmp2/OHGC-c > /global/projectb/scratch/brycef/tmp2/OHGC-c.log 2>&1"


~/code/my-qsub.sh frag-nubu "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/clrs_redundancy.py --lib NUBU -o /global/projectb/scratch/brycef/tmp2/NUBU > /global/projectb/scratch/brycef/tmp2/NUBU.log 2>&1"
~/code/my-qsub.sh frag-nubc "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/clrs_redundancy.py --lib NUBC -o /global/projectb/scratch/brycef/tmp2/NUBC > /global/projectb/scratch/brycef/tmp2/NUBC.log 2>&1"

~/code/my-qsub.sh frag-oygo "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/clrs_redundancy.py --lib OYGO -o /global/projectb/scratch/brycef/tmp2/OYGO > /global/projectb/scratch/brycef/tmp2/OYGO.log 2>&1"

~/code/my-qsub.sh clrs-ACOUA "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/clrs_redundancy.py --lib ACOUA -o /global/projectb/scratch/brycef/clrs/ACOUA"

2.7x -> 2.2-2.4x reading 8 lines at a time
10-20% improvement in run time

long mate pair redundancy without a reference:
The method should do something like this:
1) take read 1. get last N bp.
2) take read 2. get last N bp. take complement (not reverse complement)
3) join result of #1 and #2.
4) take reverse complement of #3.
5) put result of #3 and #4 in dict. key = result from #3 or #4. value = true
6) repeat #1-#5 for all pairs
7) keep track of number of pairs (2 reads = 1 pair) counted.
8) unique pairs = count number of keys in dictionary / 2
9) percent unique pairs = #7 / #8
10) print #7, #8, #9

if > 120m reads then run on himem node - not always a good predictor
- long mem NUBB, ONTW, ONTX, ONTY, ONTZ, HHUU > 120mb filtered reads


Out of Memory: - hash size - not always a good predictor
44731077/60175000
44737174/83200000
44737045/65600000

libs: TSAT, TSAO, OGSH

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import gzip
import time
import numpy
import matplotlib
matplotlib.use("Agg") ## This needs to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import AutoMinorLocator, MultipleLocator, FormatStrFormatter
import mpld3

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, get_status, run_cmd, checkpoint_step, append_rqc_file, append_rqc_stats
from rqc_constants import RQCConstants

from clrs_redundancy_lib import get_fastq

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions




'''
CLRS Redundancy Calc
- default is inward which is what the normalization regression is based upon

inward:
beginning of read1
end of read2

outward:
end of read1
beginning of read2

circular:
end of read1
end of read2

dict[read1_kmer,read2_kmer] += 1
dict[(reverse compliment read1_kmer, read2_kmer)] += 1


count = count / 2 in end because we're counting the reverse compliment 2x
'''
def clrs_redundancy_calc(fastq, fastq_name, output_path, kmer_size, freq_cnt, mode, log):

    log.info("clrs_redundancy_calc: %s", fastq_name)

    status = "clrs redundancy calc in progress"
    checkpoint_step(status_log, status)

    start_time = time.time()

    # header line for reporting
    output_file = "%s.csv" % (fastq_name)
    data_file = os.path.join(output_path, output_file)

    fh_csv = open(data_file, "w")

    buffer = "Raw Pairs,Unique Pairs,Unique Ratio"
    if mode == "inward":
        buffer += ",Normalized Unique Pairs,Normalized Unique Ratio"

    fh_csv.write(buffer + "\n")

    fh = None

    mode = mode.lower()


    if fastq.endswith(".gz"):
        fh = gzip.open(fastq, "r")
    else:

        fh = open(fastq, "r")

    dict = {}

    min_read_length = 45 # minimum read length for counting only
    short_read_cnt = 0   # count reads shorter than min_read_length

    line_cnt = 0
    read1 = None
    read2 = None
    read_cnt = 0


    for line in fh:

        line_cnt += 1


        # read 8 lines at a time, then process...
        #line = fh.next()
        index1 = line

        line = fh.next()
        read1 = line
        read_cnt += 1

        line = fh.next()
        misc = line

        line = fh.next()
        qual1 = line

        line = fh.next()
        index2 = line

        line = fh.next()
        read2 = line
        #read_cnt += 1

        line = fh.next()
        misc = line

        line = fh.next()
        qual2 = line


        #print "R1: %s" % (read1)

        # convert read 2 to strand 1

        read2 = compliment(read2)
        #print "R2: %s" % (read2)
        #print "RC R2: %s" % (reverse_compliment(read2))

        if mode == "circular":
            key = "%s%s" % (read1[-kmer_size:], read2[-kmer_size:]) # ends of both reads - lfpe
        elif mode == "outward":
            key = "%s%s" % (read1[-kmer_size:], read2[:kmer_size])
        else:
            # inward - default
            key = "%s%s" % (read1[:kmer_size], read2[-kmer_size:])

        #print "  KK: %s" % key

        # convert key to binary
        #key = convert_bin(key)

        if len(read1) < min_read_length:
            short_read_cnt += 1
        if len(read2) < min_read_length:
            short_read_cnt += 1

        # set object
        if key in dict:
            dict[key] += 1
        else:
            try:
                dict[key] = 1
            except:
                # more memory please
                log.error("- out of memory at read %s!", "{:,}".format(read_cnt))

                status = "resubmit more memory"
                checkpoint_step(status_log, status)

                sys.exit(0)

        # reverse compliment of key
        key = compliment(key) # GTAC -> CATG
        key = key[::-1] # reverse it CATG -> GTAC

        #print "KK-R: %s" % key
        #exit(1)

        if key in dict:
            dict[key] += 1
        else:
            try:
                dict[key] = 1
            except:
                # more memory please
                log.error("- out of memory at read %s!", "{:,}".format(read_cnt))

                status = "resubmit more memory"
                checkpoint_step(status_log, status)

                sys.exit(0)

        # key-value store: tokyo cabinet, screed
        # brian, shoudan's kmer counter

        # counter to threshold is better than mod.
        if read_cnt % freq_cnt == 0:

            # memory check
            #82 371 078 / 23 917 794 = bad
            #52 700 000 / 22 364 120 = good
            kmer_cnt = len(dict) / 2.0 # number of unique kmers in the dictionary
            unique = kmer_cnt / float(read_cnt) # % of unqiue kmers / total read count thus far

            if mode == "inward":

                redundancy = 1 - unique
                normalized = 1 - normalize_function(redundancy)
                normalized_cnt = normalized * read_cnt

                fh_csv.write("%s,%s,%s,%s,%s\n" % (read_cnt, int(kmer_cnt), unique, normalized_cnt, normalized))
            else:
                fh_csv.write("%s,%s,%s\n" % (read_cnt, int(kmer_cnt), unique))

            fh_csv.flush()
            log.info("- read: %s, unique: %s", "{:,}".format(read_cnt), "{:.2%}".format(unique))


    fh.close()

    # Run last calculations
    kmer_cnt = len(dict) / 2.0

    unique = kmer_cnt / float(read_cnt)
    redundancy = 1 - unique
    normalized_redundant = normalize_function(redundancy) # 1 - normalized redundant = normalized unique
    normalized = 1 - normalized_redundant
    normalized_cnt = normalized * read_cnt

    if mode == "inward":
        fh_csv.write("%s,%s,%s,%s,%s\n" % (read_cnt, int(kmer_cnt), unique, normalized_cnt, normalized))
        log.info("- Final reads: %s, unique kmers: %s, unique pct: %s, normalized unique pct: %s", read_cnt, int(kmer_cnt), "{:.2%}".format(unique), "{:.2%}".format(normalized))

    else:
        fh_csv.write("%s,%s,%s\n" % (read_cnt, int(kmer_cnt), unique))
        log.info("- Final reads: %s, unique kmers: %s, unique pct: %s", read_cnt, int(kmer_cnt), "{:.2%}".format(unique))


    fh_csv.close()

    elapsed = (time.time() - start_time)

    log.info("- wrote output to: %s", output_file)
    log.info("- processing time: %s sec", elapsed)

    append_rqc_file(rqc_file_log, "clrs redundancy", os.path.join(output_path, output_file))


    redundant = 1 - unique
    #log.info("- unique: %s, redundant: %s", "{:.2%}".format(unique), "{:.2%}".format(redundant))

    # save stats
    append_rqc_stats(rqc_stats_log, "mode", mode)

    append_rqc_stats(rqc_stats_log, "raw pairs", read_cnt)
    append_rqc_stats(rqc_stats_log, "unique pairs", int(kmer_cnt))

    append_rqc_stats(rqc_stats_log, "unique ratio", unique)
    append_rqc_stats(rqc_stats_log, "short read count", short_read_cnt)
    append_rqc_stats(rqc_stats_log, "minimum read length", min_read_length)

    if mode == "inward":

        append_rqc_stats(rqc_stats_log, "normalized unique pairs", normalized_cnt)
        append_rqc_stats(rqc_stats_log, "normalized unique ratio", normalized)
        append_rqc_stats(rqc_stats_log, "normalized redundant ratio", normalized_redundant)


    status = "clrs redundancy calc complete"
    checkpoint_step(status_log, status)


    return status


'''
Feb 5, 2014 based on James' linear regression model using INWARD numbers
estimate_clrs_library_redundancy = 0.93* seq_based_redundancy + 13.25
- redundancy [0..1]
'''
def normalize_function(redundancy):

    norm = (0.93 * (100 * redundancy) + 13.25) / 100.0
    # keep between 0 and 1
    norm = max(0, norm)
    norm = min(1, norm)
    return norm

'''
GTACN -> CATGN
returns the compliment for each base in the string
'''

def compliment(read_str):

    new_read = ""

    if read_str:

        for base in list(read_str):
            if base in base_dict:
                new_read += base_dict[base]

    return new_read


'''
Creates chart from unique.csv
'''
def make_chart(fastq_name, output_path, mode, log):

    log.info("make_chart")

    status = "plotting in progress"
    checkpoint_step(status_log, status)

    csv_file = os.path.join(output_path, "%s.csv" % fastq_name)

    if not os.path.isfile(csv_file):
        log.error("- failed to find csv file: %s", csv_file)
        status = "plot failed"
        checkpoint_step(status_log, status)
        return status


    # skip first row (header)
    raw_data = numpy.loadtxt(csv_file, skiprows=1, comments='#', delimiter = ",") #, usecols=(0,12))

    fig, ax = plt.subplots()

    marker_size = 1.0
    line_width = 1.5

    p1 = ax.plot(raw_data[:,0], raw_data[:,2]*100, 'r', marker='o', markersize = marker_size, linewidth = line_width, alpha = 0.5, label = 'Unique Pct')

    p2 = ax.plot(raw_data[:,0], raw_data[:,4]*100, 'g', marker='x', markersize = marker_size, linewidth = line_width, alpha = 0.5, label = 'Normalized Unique Pct')


    plt.title("CLRS Redundancy (%s): %s" % (mode, fastq_name))

    font_prop = FontProperties()
    font_prop.set_size("small")
    font_prop.set_family("Bitstream Vera Sans")
    ax.legend(loc=1, prop=font_prop)

    ax.set_xlabel("Read Count/1000", fontsize=12, alpha=0.5)
    ax.set_ylabel("Percent Unique", fontsize=12, alpha=0.5)

    ax.grid(color="gray", linestyle=':')


    png_file = os.path.join(output_path, "%s.png" % (fastq_name))
    html_file = png_file.replace(".png", ".html")

    log.info("- created %s", os.path.basename(png_file))


    mpld3.save_html(fig, html_file)
    ## Save Matplotlib plot in png format
    plt.savefig(png_file, dpi=fig.dpi)

    # check that file exists
    if os.path.isfile(os.path.join(output_path, png_file)):
        append_rqc_file(rqc_file_log, "kmer count plot", os.path.join(output_path, png_file))
        append_rqc_file(rqc_file_log, "kmer count plot html", os.path.join(output_path, html_file))
        status = "plot complete"

    else:
        status = "plot failed"


    checkpoint_step(status_log, status)

    return status




'''
Just time how long it takes to read the fastq and count lines
'''
def benchmark_time(fastq, log):

    log.info("Running benchmark of simple line count of: %s", fastq)

    status = "benchmark in progress"
    checkpoint_step(status_log, status)

    start_time = time.time()

    fh = None


    if fastq.endswith(".gz"):
        fh = gzip.open(fastq, "r")
    else:

        fh = open(fastq, "r")

    line_cnt = 0
    for line in fh:
        #print line
        line_cnt += 1

    fh.close()

    elapsed = (time.time() - start_time)

    log.info("- benchmark: %s sec", elapsed)
    log.info("- line count: %s", line_cnt)

    status = "benchmark finish"
    checkpoint_step(status_log, status)




## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "CLRS Redundancy"
    version = "1.2.1"



    # Parse options
    usage = "prog [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-f", "--fastq", dest="fastq", type=str, help="fastq file (full path to fastq)")
    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-fq", "--fastq-name", dest="fastq_name", type=str, help="fastq name in RQC")
    parser.add_argument("-l", "--lib", dest="lib", type=str, help="library name in RQC")
    parser.add_argument("-m", "--mode", dest="mode", type=str, help="run mode to calculate kmer (inward [default], outward, circular(CLRS))")
    parser.add_argument("-b", "--benchmark", dest="benchmark", default = False, action="store_true", help="benchmark against counting lines in fastq")
    parser.add_argument("-k", "--kmer", dest="kmer_size", type=int, help="Kmer size, default = 20")
    parser.add_argument("-r", "--report-freq", dest="freq_cnt", type=int, help="Reporting frequency, default 25000 reads")
    parser.add_argument("-v", "--version", action="version", version=version)


    # Compliment of each base
    base_dict = { "A" : "T", "T" : "A", "G" : "C", "C": "G", "N" : "N" }

    # convert base to a binary value
    bin_dict = {
        "A" : 0b00,
        "T" : 0b11,
        "G" : 0b01,
        "C" : 0b10,
    }



    fs_fastq = None # file system fastq location

    mode = "inward" # outward, circular
    kmer_size = 20 # on each end
    freq_cnt = 25000 # report every 25000 reads

    fastq_name = None # name of fastq
    lib_name = None # name of library - look up filtered fastq

    benchmark_flag = False # compare reading lines

    output_path = None
    log_level = "INFO"




    # parse args
    args = parser.parse_args()

    if args.benchmark == True:
        benchmark_flag = True

    if args.fastq:
        fs_fastq = args.fastq


    if args.lib:
        lib_name = args.lib

    if args.kmer_size:
        kmer_size = args.kmer_size

    if args.freq_cnt:
        freq_cnt = args.freq_cnt

    if args.output_path:
        output_path = args.output_path


    if args.mode:
        mode = args.mode

        if mode.lower().startswith("c"): mode = "circular"
        if mode.lower().startswith("i"): mode = "inward"
        if mode.lower().startswith("o"): mode = "outward"

    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    # initialize my logger
    log_file = os.path.join(output_path, "clrs_redundancy.log")



    # log = logging object
    log = get_logger("clrs", log_file, log_level)

    log.info("%s", 80 * "~")
    log.info("Starting %s", script_name)

    log.info("")

    log.info("Run settings:")
    if lib_name:
        log.info("%25s      %s", "library_name", lib_name)
    elif fastq_name:
        log.info("%25s      %s", "fastq_name", fastq_name)
    else:
        log.info("%25s      %s", "fastq", fs_fastq)
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "mode", mode)
    log.info("%25s      %s", "kmer_size", kmer_size)
    log.info("%25s      %s", "freq_cnt", freq_cnt)
    log.info("%25s      %s", "benchmark", benchmark_flag)
    log.info("")



    if not fs_fastq:
        if lib_name or fastq_name:
            fs_fastq = get_fastq(fastq_name, lib_name, "filtered", log)

    if not fs_fastq:
        log.error("- no fastq, fastq_name or library_name passed!")
        sys.exit(2)


    if not os.path.isfile(fs_fastq):
        log.error("- fastq does not exist on file system!  %s", fs_fastq)
        sys.exit(2)




    fastq_name = os.path.basename(fs_fastq)
    fastq_name = fastq_name.replace(".fastq.gz", "")


    # create a name by stripping .fastq from the fastq name
    rqc_file_log = os.path.join(output_path, "clrs-files.tmp")
    rqc_stats_log = os.path.join(output_path, "clrs-stats.tmp")


    # RQC System log
    status_log = os.path.join(output_path, "clrs_status.log")

    # set umask
    umask = os.umask(RQCConstants.UMASK)
    status = get_status(status_log)

    # testing
    #make_chart(fastq_name, output_path, mode, log)
    #sys.exit(44)




    if status == "complete":
        log.info("Status is complete for %s, exiting.", fastq_name)
        sys.exit(0)
    elif status == "plotting in progress":
        status = "kmer unique calc complete"
    else:
        status = "initialize"
        checkpoint_step(status_log, status)



    if status == "initialize":
        # optional ... should skip this normally
        if benchmark_flag == True:
            benchmark_time(fs_fastq, log)


        status = clrs_redundancy_calc(fs_fastq, fastq_name, output_path, kmer_size, freq_cnt, mode, log)

    if status == "clrs redundancy calc complete":
        status = make_chart(fastq_name, output_path, mode, log)


    if status == "plot complete":
        status = "complete"

        # move rqc-files.tmp to rqc-files.txt
        rqc_new_file_log = os.path.join(output_path, "clrs-files.txt")

        cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        rqc_new_stats_log = os.path.join(output_path, "clrs-stats.txt")

        cmd = "mv %s %s" % (rqc_stats_log, rqc_new_stats_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        checkpoint_step(status_log, status)

    if status.find("failed") > -1:
        status = "failed"
        checkpoint_step(status_log, status)

    os.umask(umask)

    log.info("Completed %s: %s", script_name, fastq_name)

    sys.exit(0)


"""

        "AWWW HAMBURGERS!"
        ------------------
                     \

                    -.._       _-*"`
                      \ '-._.*' <"
                  _.--'          "*-._
               _-'             :''"*--^-
            -='.-*'.   _.-"-.  '.    ",
              /    ;.-'      '*-.'-.   \
             /    /'             '"*-   \
            ;                            :
           :        .*"*-.  .-*"*.        ;
           ;       /      ;:      \       :
           ;      ;    *  !!  *    :      :
           :      :     .'  '.     ;      ;
            ;      '-.-'      '-.-'      :
             \                          /
              \                        /
               '.                    .'
                /'.      _.._      .' \
               /   '-.          .-'    \
              /       "--.,,.--"        \
             :    :        |        ;    ;
             |.--.;        |        :.--.|
             (   ()        |        ()   )
              '--^_        |        _^--'
                 | "'*--.._I_..--*'" |
                 | __..._  | _..._   |
                .'"      `"'"     ''"'.
                '''''''''''''''''''''''



"""
