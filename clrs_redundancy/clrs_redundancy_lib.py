#!/usr/bin/env python
# -*- coding: utf-8 -*-


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import sys
import MySQLdb
import os
import re

# custom libs in "../lib/"
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir, '../lib'))
from db_access import jgi_connect_rqc
from common import human_size


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
A = 00
T = 11
G = 01
C = 10
** this function is not in use!

AAAA = 00000000 = 0
TTTT = 11111111 = 255
GGGG = 01010101 = 85
CCCC = 10101010 = 170
CATG = 10001101 = 142
AATT = 111100 = 60 00001111 =


    # testing ...
    # "0b1111" = "0b00001111"
    #convert_bin("AATT")
    # 0b10001101
    #convert_bin("CATG")
    #exit(1)

'''
def convert_bin(read_str):

    my_val = 0

    bin_dict = {} # should have values, but no values since this function is not uin use

    # ['a']
    for base in list(read_str):

        if base == "N":
            continue
            # if not tgc, then use A

        my_val = my_val << 2
        my_val = my_val | bin_dict[base]

        #print bin(my_val)

    #print "%s = %s" % (my_val, bin(my_val))
    #print

    return my_val



'''
Look up the filtered fastq from the database based on fastq name (seq_units.seq_unit_name) or library name (seq_units.library_name)


'''
def get_fastq(fastq_name, library_name, file_type, log = None):


    fs_fastq = None # full path to fastq

    if file_type == "filtered":
        file_type = "COMPRESSED FILTERED FASTQ"
    elif file_type == "raw compressed":
        file_type = "ILLUMINA COMPRESSED RAW FASTQ"
    else:
        file_type = "RAW FASTQ"

    # look up library or seq unit, read-only connection
    db = jgi_connect_rqc("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)

    sql = """
select
    s.seq_unit_name,
    s.library_name,
    s.raw_reads_count,
    s.filter_reads_count,
    concat(sf.fs_location, '/', sf.file_name) as fastq,
    sf.file_size
from seq_units s
inner join seq_unit_file sf on s.seq_unit_id = sf.seq_unit_id
    """

    # look up by library name
    if library_name:
        sql += """
where
    s.library_name = %s
    and sf.file_type = %s
    and s.seq_unit_status_id in (14,15,16)
        """

        sth.execute(sql, (library_name, file_type))

    # look up by raw fastq name
    elif fastq_name:

        sql += """
where
    s.seq_unit_name = %s
    and sf.file_type = %s
        """

        sth.execute(sql, (fastq_name, file_type))

    #print sql % lib_name

    rs = sth.fetchone()

    if rs:

        fs_fastq = rs['fastq']
        if file_type == "COMPRESSED FILTERED FASTQ":
            total_reads = rs['filter_reads_count']
        else:
            total_reads = rs['raw_reads_count']

        if log:
            log.info("%s (%s): %s reads, %s", rs['seq_unit_name'], rs['library_name'], "{:,}".format(total_reads), human_size(rs['file_size'] ))
            log.info("Filtered fastq: %s", fs_fastq)

        else:
            print
            print "%s (%s): %s reads, %s" % (rs['seq_unit_name'], rs['library_name'], "{:,}".format(total_reads), human_size(rs['file_size'] ))
            print "Filtered fastq: %s" % (fs_fastq)

    else:
        if log:
            log.error("Cannot find filtered fastq for %s (%s)", fastq_name, library_name)
        else:
            print "Cannot find filtered fastq for %s (%s)" % (fastq_name, library_name)

    db.close()

    return fs_fastq



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    # same file for both these
    seq_unit = "6626.5.48984.GTGAAA.srf"
    lib_name = "NCWT"

    fq = get_fastq(seq_unit, None, "filtered")
    print "Filtered Fastq: %s" % (fq)

    fq = get_fastq(None, lib_name, "filtered")
    print "Filtered Fastq: %s" % (fq)


    sys.exit(0)

