#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
CLRS Redundancy Analysis
- try to measure coverage without a reference genome
- paired ended runs only
- takes one library or seq unit

~/code/my-qsub.sh frag-tatt "/global/homes/b/brycef/git/jgi-rqc-pipeline/frag_align/frag_align.py --lib TATT > /global/projectb/scratch/brycef/tmp2/frag/TATT.log 2>&1"

~/code/my-qsub.sh frag-ohgo "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/frag_align.py --lib OHGO > /global/projectb/scratch/brycef/tmp2/frag/OHGO2.log 2>&1"
~/code/my-qsub.sh frag-ohgc "/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/frag_align.py --lib OHGC > /global/projectb/scratch/brycef/tmp2/frag/OHGC2.log 2>&1"

long mate pair redundancy without a reference:
The method should do something like this:
1) take read 1. get last N bp.
2) take read 2. get last N bp. take complement (not reverse complement)
3) join result of #1 and #2.
4) take reverse complement of #3.
5) put result of #3 and #4 in dict. key = result from #3 or #4. value = true
6) repeat #1-#5 for all pairs
7) keep track of number of pairs (2 reads = 1 pair) counted.
8) unique pairs = count number of keys in dictionary / 2
9) percent unique pairs = #7 / #8
10) print #7, #8, #9


Out of Memory:
44731077/60175000
44737174/83200000
44737045/65600000


"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import re
import sys
from argparse import ArgumentParser
import gzip
import MySQLdb
import time

# custom libs in "../lib/"
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir, '../lib'));

from common import get_logger, get_status, run_command, checkpoint_step, append_rqc_file, append_rqc_stats, post_mortem_cmd, get_read_count_fastq, human_size
from rqc_constants import RQCCommands, RQCReferenceDatabases
from db_access import jgi_connect_db


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Just time how long it takes to read the fastq
'''
def benchmark_time(fastq):
    
    print "Benchmarking simple read of file"
    
    start_time = time.time()
    
    fh = None
    
    
    if fastq.endswith(".gz"):
        fh = gzip.open(fastq, "r")
    else:
    
        fh = open(fastq, "r")

    line_cnt = 0       
    for line in fh:
        #print line
        line_cnt += 1
       
    fh.close()
    
    elapsed = (time.time() - start_time)
    
    print "# benchmark: %s sec" % (elapsed)


    
'''
For read1, read2
read kmer from beginning of read1 (R1-)
read kmer from end of read2 (R2+)
reverse compliment R2 so read is from same strand

dict[read1_kmer,read2_kmer] += 1
dict[(reverse compliment read1_kmer, read2_kmer)] += 1

count = count / 2 in end because we're counting the reverse compliment 2x
'''
def frag_kmer(fastq, kmer_size, rpt_freq, total_reads, mode = "inward"):
    

    start_time = time.time()
    
    # header line for reporting
    print "Unique Count;Read Count;Unique/Read;Pct Complete"
    
    fh = None
    
    mode = mode.lower()
    
    if fastq.endswith(".gz"):
        fh = gzip.open(fastq, "r")
    else:
    
        fh = open(fastq, "r")
    
    dict = {}
    
    line_cnt = 0
    read1 = None
    read2 = None
    read_cnt = 0
    
    for line in fh:
        #print line
        line_cnt += 1
        process_reads = False
    
        # read 8 lines at a time, then process...
        
        # header
        if line_cnt == 1 or line_cnt == 5:
            pass 
        elif line_cnt == 2:
            read1 = line.strip()
        elif line_cnt == 3 or line_cnt == 7:
            if not line.startswith("+"):
                print "fastq out of alignment! at read count %s" % (read_cnt+1)
                sys.exit(1)
        elif line_cnt == 4:
            pass
        elif line_cnt == 6:
            read2 = line.strip()
        elif line_cnt == 8:
            line_cnt = 0
            process_reads = True
            
            
            
        if process_reads == True:
            process_reads = False
            #print "R1: %s" % (read1)
            
            read_cnt += 1
            
            # convert read 2 to strand 1
            
            read2 = compliment(read2)
            #print "R2: %s" % (read2)
            #print "RC R2: %s" % (reverse_compliment(read2))
            
            if mode == "circular":
                key = "%s%s" % (read1[-kmer_size:], read2[-kmer_size:]) # ends of both reads - lfpe
            elif mode == "outward":
                # untested
                key = "%s%s" % (read1[-kmer_size:], read2[:kmer_size])
            else:
                # inward
                key = "%s%s" % (read1[:kmer_size], read2[-kmer_size:])
                
            #print "  KK: %s" % key
            
            # convert key to binary
            #key = convert_bin(key)
            
            # set object
            if key in dict:
                dict[key] += 1
            else:
                dict[key] = 1
                
            # reverse compliment of key
            key = compliment(key) # GTAC -> CATG
            key = key[::-1] # reverse it CATG -> GTAC
            
            #print "KK-R: %s" % key
            #exit(1)
            
            if key in dict:
                dict[key] += 1
            else:
                dict[key] = 1
                
            # key-value store: tokyo cabinet, screed
            # brian, shoudan's kmer counter
            
            
            # counter to threshold is better than mod.
            if read_cnt % freq_cnt == 0:
                freq = len(dict) / 2.0
                freq_ratio = freq / float(read_cnt)
                # take out total_read calculation
                pct_complete = 2*float(read_cnt) / float(total_reads)
                #print "%s = %s" % (read_cnt, total_reads)
                print "%s; %s; %s; %s" % ("{:,}".format(int(freq)),
                                          "{:,}".format(read_cnt),
                                          "{:.2%}".format(freq_ratio),
                                          "{:.2%}".format(pct_complete))
                # can move to "%s" % "{:,}{:,}".format(a,b) ... 
        
    
    fh.close()
    freq = len(dict) / 2.0
    
    freq_ratio = freq / float(read_cnt)
    pct_complete = 2*float(read_cnt) / float(total_reads)
    print "%s; %s; %s; %s" % ("{:,}".format(int(freq)), "{:,}".format(read_cnt), "{:.2%}".format(freq_ratio), "{:.2%}".format(pct_complete))

    elapsed = (time.time() - start_time)
    
    print "# frag_kmer: %s sec" % (elapsed)

    
    return freq_ratio

                              
        

    
'''
A = 00
T = 11
G = 01
C = 10

AAAA = 00000000 = 0
TTTT = 11111111 = 255
GGGG = 01010101 = 85
CCCC = 10101010 = 170
CATG = 10001101 = 142
AATT = 111100 = 60 00001111 = 

        
    # testing ... 
    # "0b1111" = "0b00001111"
    #convert_bin("AATT")
    # 0b10001101
    #convert_bin("CATG")
    #exit(1)

'''    
def convert_bin(read_str):
    
    my_val = 0
    
    dict = {
        "A" : 0b00,
        "T" : 0b11,
        "G" : 0b01,
        "C" : 0b10,
    }
    # ['a']
    for base in list(read_str):
        
        if base == "N":
            continue
            # if not tgc, then use A
            
        my_val = my_val << 2
        my_val = my_val | dict[base]
        
        #print bin(my_val)
        
    #print "%s = %s" % (my_val, bin(my_val))
    #print
    
    return my_val
        
        
    

'''
GTACN -> CATGN
returns the compliment for each base in the string
'''

def compliment(read_str):
    
    new_read = ""
    
    base_dict = { "A" : "T", "T" : "A", "G" : "C", "C": "G", "N" : "N" }
    
    if read_str:
        
        for base in list(read_str):
            if base in base_dict:
                new_read += base_dict[base]
            
    return new_read

'''
To do
- probably someone wrote this already ... 
'''
def is_paired(fastq):
    return True

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "RQC Frag Alignment"
    version = "0.55"
    
    print "deprecated, use clrs_redundancy"
    sys.exit(22)
    
    # Parse options
    usage = "prog [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    

    parser = ArgumentParser(usage = usage)

    parser.add_argument("-f", "--fastq", dest="fastq", type=str, help="raw fastq name in RQC (not path to fastq)")
    parser.add_argument("-file", "--file", dest="file", type=str, help="path to filtered fastq")
    parser.add_argument("-l", "--lib", dest="lib", type=str, help="library name in RQC")
    parser.add_argument("-m", "--mode", dest="mode", type=str, help="Mode (inward [default], outward, circular)")
    parser.add_argument("-k", "--kmer", dest="kmer_size", type=int, help="Kmer size, default = 20")
    parser.add_argument("-r", "--report-freq", dest="freq_cnt", type=int, help="Reporting frequency, default 25000 reads")
    parser.add_argument("-v", "--version", action="version", version=version) 
    
    
    fs_fastq = None
    fastq_file = None
    mode = "inward" # outward, circular
    kmer_size = 20 # on each end
    fastq = None # name of fastq
    freq_cnt = 25000 # report every 25000 reads
    lib_name = None # name of library - look up filtered fastq
    
    # parse args
    args = parser.parse_args()
    
    if args.fastq:
        fastq = args.fastq
    
    if args.file:
        fastq_file = args.file
        
    if args.lib:
        lib_name = args.lib

    if args.kmer_size:
        kmer_size = args.kmer_size

    if args.freq_cnt:
        freq_cnt = args.freq_cnt


    # look up library or seq unit, read-only connection
    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)
    
    sql = """
select
    s.seq_unit_name,
    s.library_name,
    s.filter_reads_count,
    concat(sf.fs_location, '/', sf.file_name) as fastq,
    sf.file_size
from seq_units s
inner join seq_unit_file sf on s.seq_unit_id = sf.seq_unit_id
    """
    
    # look up by library name
    if lib_name:
        sql += """
where
    s.library_name = %s
    and sf.file_type = 'COMPRESSED FILTERED FASTQ'
        """

        sth.execute(sql, (lib_name))
    
    # look up by raw fastq name
    elif fastq:

        sql += """
where
    s.seq_unit_name = %s
    and sf.file_type = 'COMPRESSED FILTERED FASTQ'
        """

        sth.execute(sql, (fastq))
    
    #print sql % lib_name
    
    rs = sth.fetchone()
    total_reads = 0
    if rs:
        print
        fs_fastq = rs['fastq']
        total_reads = rs['filter_reads_count']
        print "%s (%s): %s reads, %s" % (rs['seq_unit_name'], lib_name, "{:,}".format(total_reads), human_size(rs['file_size'] ))
        if not fs_fastq:
            fs_fastq = fastq_file
            
        print "Filtered: %s" % (fs_fastq)
        if fs_fastq:
            if os.path.isfile(fs_fastq):
                pass
            else:
                print "Error: filtered fastq not online!"
                sys.exit(1)
        else:
            print "Error: filtered fastq not found!"
            
    else:
        print "No such record??!"
        
    
    db.close()    
        
    if fs_fastq:
        
        print "-" * 80
        
        benchmark_time(fs_fastq)
        
        print "-" * 80
        
        freq_ratio = frag_kmer(fs_fastq, kmer_size, freq_cnt, total_reads, mode)
        
        if lib_name:
            print "# lib: %s, freq ratio: %s" % (lib_name.upper(), "{:.2%}".format(freq_ratio))
        
        if fastq:
            print "# fastq: %s, freq ration: %s"  % (fastq, "{:.2%}".format(freq_ratio))
    
        
    sys.exit(1)
