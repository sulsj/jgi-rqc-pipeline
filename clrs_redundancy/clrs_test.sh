#!/bin/bash -l

#set -e

# CLRS redundancy tests

folder="/global/projectb/sandbox/rqc/analysis/qc_user/clrs"
DATE=`date +"%Y%m%d"`


#dev for Bryce
cmd="/global/homes/b/brycef/git/jgi-rqc-pipeline/clrs_redundancy/clrs_redundancy.py"


# AGAUO (redundancy 17.47%)
qsub -b yes -j yes -m as -now no -w e -N clrs-AGAUO -l h_rt=43195,ram.c=30G -P gentech-rqc.p -o $folder/clrs-AGAUO.log "$cmd -o $folder/AGAUO-$DATE -f $folder/AGAUO.fastq"
 

# AGHCY (redundancy 86.34%)
qsub -b yes -j yes -m as -now no -w e -N clrs-AGHCY -l h_rt=43195,ram.c=30G -P gentech-rqc.p -o $folder/clrs-AGHCY.log "$cmd -o $folder/AGHCY-$DATE -f $folder/AGHCY.fastq"

#unpigz -c $SDM_FASTQ/00/89/63/8963.8.117093.CTTGTA.fastq.gz > AGHCY.fastq
#unpigz -c $SDM_FASTQ/00/89/65/8965.6.117383.ATGTCA.fastq.gz > AGAUO.fastq
# /global/projectb/sandbox/rqc/analysis/qc_user/clrs/AGAUO-???,/global/projectb/sandbox/rqc/analysis/qc_user/clrs/AGHCY-????

