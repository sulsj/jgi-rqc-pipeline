#!/bin/bash -l

# Bryce's tests for the SAG pipeline
# - 2017-01-24

SAG_CMD=/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py

# ASXWW
qsub -b yes -j yes -m as -now no -w e -N ASXWW -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-ASXWW.log -js 501 "$SAG_CMD -o t1"


# ATTTZ = 25 contigs
#qsub -b yes -j yes -m as -now no -w e -N ATTTZ -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-ATTTZ.log -js 502 "$SAG_CMD -o t3"

# ATNUW = 1 contig
#qsub -b yes -j yes -m as -now no -w e -N sag-t2 -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-t2.log -js 502 "$SAG_CMD -o t2"


# BAGOO - w/o chaff
qsub -b yes -j yes -m as -now no -w e -N BAGOO -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BAGOO.log -js 504 "$SAG_CMD -o t4"


# BAGOX - w/ chaff
qsub -b yes -j yes -m as -now no -w e -N BAGOX -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BAGOX.log -js 505 "$SAG_CMD -o t5"

# t6 = BCWAG
#qsub -b yes -j yes -m as -now no -w e -N BCWAG -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BCWAG.log -js 505 "$SAG_CMD -o t6"

# ATNUA = ncbi prescreen
#- does not run prodege
#qsub -b yes -j yes -m as -now no -w e -N ATNUA -l ram.c=120G,h_rt=43100 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-ATNUA.log -js 505 "$SAG_CMD -o t5"

#Sczerba.2 -
#BCWAY =
qsub -b yes -j yes -m as -now no -w e -N BCWAY -l ram.c=120G,h_rt=43100 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BCWAY.log -js 505 "$SAG_CMD -f /global/dna/dm_archive/rqc/filtered_seq_unit/00/01/08/46/10846.1.181577.TAGGCAT-CTAGTCG.anqdpht.fastq.gz -l BCWAY -o /global/projectb/scratch/brycef/sag/BCWAY"
#BCUZB =
qsub -b yes -j yes -m as -now no -w e -N BCUZB -l ram.c=120G,h_rt=43100 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BCUZB.log -js 505 "$SAG_CMD -f /global/dna/dm_archive/rqc/filtered_seq_unit/00/01/08/35/10835.1.181450.TAAGGCG-TCTTACG.anqdpht.fastq.gz -l BCUZB -o /global/projectb/scratch/brycef/sag/BCUZB"


#Stew -
#BHAHN =
qsub -b yes -j yes -m as -now no -w e -N BHAHN -l ram.c=120G,h_rt=86400 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BHAHN.log -js 505 "$SAG_CMD -f /global/dna/dm_archive/rqc/filtered_seq_unit/00/01/09/14/10914.1.183618.TAAGGCG-TATGCAG.filter-SAG.fastq.gz -l BHAHN -o /global/projectb/scratch/brycef/sag/BHAHN"
#BHAOP =
qsub -b yes -j yes -m as -now no -w e -N BHAOP -l ram.c=120G,h_rt=86400 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BHAOP.log -js 505 "$SAG_CMD -f /global/dna/dm_archive/rqc/filtered_seq_unit/00/01/09/14/10914.1.183618.GGACTCC-TACTCCT.filter-SAG.fastq.gz -l BHAOP -o /global/projectb/scratch/brycef/sag/BHAOP"
#BHAPH (6 contigs, small should fail)
qsub -b yes -j yes -m as -now no -w e -N BHAPH -l ram.c=120G,h_rt=86400 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BHAPH.log -js 505 "$SAG_CMD -f /global/dna/dm_archive/rqc/filtered_seq_unit/00/01/09/14/10914.1.183618.CTCTCTA-ATAGAGA.filter-SAG.fastq.gz -l BHAPH -o /global/projectb/scratch/brycef/sag/BHAPH"

#"BCWAG", "BCUYO", "BCUYP" - Scyrba.2
qsub -b yes -j yes -m as -now no -w e -N BCWAG -l ram.c=120G,h_rt=86400 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BCWAG.log -js 505 "$SAG_CMD -f /global/dna/dm_archive/rqc/filtered_seq_unit/00/01/08/35/10835.1.181450.CGTACTA-TAAGGCT.anqdpht.fastq.gz -l BCWAG -o /global/projectb/scratch/brycef/sag/BCWAG"
qsub -b yes -j yes -m as -now no -w e -N BCUYO -l ram.c=120G,h_rt=86400 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BCUYO.log -js 505 "$SAG_CMD -f /global/dna/dm_archive/rqc/filtered_seq_unit/00/01/08/35/10835.1.181450.TAAGGCG-AGCTAGA.anqdpht.fastq.gz -l BCUYO -o /global/projectb/scratch/brycef/sag/BCUYO"
qsub -b yes -j yes -m as -now no -w e -N BCUYP -l ram.c=120G,h_rt=86400 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BCUYP.log -js 505 "$SAG_CMD -f /global/dna/dm_archive/rqc/filtered_seq_unit/00/01/08/35/10835.1.181450.TCCTGAG-TCTTACG.anqdpht.fastq.gz -l BCUYP -o /global/projectb/scratch/brycef/sag/BCUYP"

# missing fastq files:
#BCUYO
#$SDM_FILTERED_FASTQ/00/01/08/35/10835.1.181450.TAAGGCG-AGCTAGA.anqdpht.fastq.gz
#BCUYP
#$SDM_FILTERED_FASTQ/00/01/08/35/10835.1.181450.TCCTGAG-TCTTACG.anqdpht.fastq.gz
#BCWAG
#$SDM_FILTERED_FASTQ/00/01/08/35/10835.1.181450.CGTACTA-TAAGGCT.anqdpht.fastq.gz

#ATXOC fix for contam_id #s
#- old atxoc didn't summarize contam_id, skip for now

#expected results:
#ASXWW - unscreen: 98 465280bp, screen: 8 113670
#ASXWW - unscreen 64 1.173mb, screen: 40, 1.062mb (Jigsaw 11-24-2015)
#ATTTZ - unscreen: 30 131494bp, 0
#ATTTZ - unscreen: 25 0.123mb, 0 (Jigsaw 11-19-2015)
#ATNUW - unscreen: 56 236671bp, screen: 1 10896bp
#ATNUW - unscreen: 57 0.241mb, 0 (Jigsaw 11-19-2015)
#ATXOC - unscreen: 86 1183630bp, screen: 44 623732bp
#ATNUA - unscreen: 39 166381bp,  0
#ATNUA - unscreen: 40 0.165mb, 0 (Jigsaw 11-19-2015)
#* Jigsaw normalization turned on 11-19-2015


