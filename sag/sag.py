#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

SAG Pipeline (SAG-omizer)

Dependencies
bbtools
spades
jigsaw - gc_cov, blast, megan, ncbi_prescreen, pct_reads_asm
blast+
texlive (pdf maker)
prodigal
checkm
bwa (pct_reads_asm)
java, R = tetramer

v 1.0: 2015-12-07

v 1.0.1: 2015-12-14
- green genes reporting
- pct of reads assembled - decontam and raw in screened report
- ncbi prescreeen in txt report
- gunzip files, rename files when re-running the pipeline
- set final status (complete/failed)
- if no ProDeGe output then no screened analysis or reports
- pooled decontam

v 1.0.2: 2016-01-05
- fixed errors if no assembly created (e.g. AUCAP)
- added gc avg, std dev for contig_gc

v 1.0.3: 2016-01-06
- fixed errors for pooled_decontam
- use pooled_decontam as cleaned file for reporting if prodege does not run

v 1.0.4: 2016-01-08
- change output path for rqc-files.txt
- look for rqc-files.txt* (and rqc-stats.txt*)
- save stats from stats.sh for .txt, .tsv with proper file names in rqc-files.txt
- change path to current path for files in rqc-files.txt when pulling from another run for pooled decomtam
- prevent *.txt, *.tsv from stats.sh from being marked as *.gz
- don't gzip or ungzip (for purge, setup) - causes too many problems

v 1.0.5: 2016-01-14
- SPAdes 3.6.2
- use scaffolds_trim if pooled_decontam is empty for prodege
- coverage histogram of subsampled fastq (input to SPAdes)

v 1.0.6: 2016-02-04
- organism name for Alex
- metadata.screen.json, metadata.unscreen.json added
- updated .tex reports to be more descriptive

v 1.0.7: 2016-04-25
- normalization removed all reads (10321.1.155714.GTAGAGG-TACTCCT.fastq.gz) - fail nicely
- coverage stats (from gc_cov and per fasta) added to rqc_stats.txt
- changed blast to use run_blastplus.py
- changed 16s blasting method - get_ssu

v 1.0.8: 2016-05-20
- added params to rqc_stats.txt for reporting
- added gc_cov/data/map_shreds_pileup.csv to rqc_files.txt
- t1, t2, t3 files in /global/projectb/sandbox/rqc/analysis/qc_user/sag/
- creates final directory and symlinks files for Kecia

v 1.0.9: 2016-06-02
- need to use fasta from SAG Decontam pipeline as the "unscreened" assembly, screened is after Prodege (per Kecia & confirmed with Stephan)
-- only applies to pool/plate SAGs

v 1.1: 2016-06-03
- fixed parsing & saving blast results
- greengenes bug fix

v 1.1.1: 2016-06-07
- pdf report, txt report - use basename of input fastq
- prodege 2.3
- fixed screen_screen_*, unscreen_unscreen_* for blast key names

v 1.1.2: 2016-06-08
- chaff file added for metadata.json for unscreened

v 1.1.3: 2016-06-24
- pdf updated (unscreened) with chaff description, Alex C's updates
- added "-s" option for blasting
- scg - (cnt > 1 copy) / (total number of copy) - for AutoQC

v 1.1.4: 2016-07-05
- paths for install different than local in install.sh

v 1.1.5: 2016-07-07
- metadata.json: include "input" field, ap id/at id to array

v 1.1.6: 2016-08-12
- subsampling calc fixed (not really used because normalization)
- final folder needs pool_decontam.fasta named lab_decontam.fasta

v 1.1.7: 2016-10-07
- switched to spades 3.9.0

v 1.1.8: 2016-10-28
- the ap id and at id in the sag.cfg for screened and unscreened were reversed (RQCSUPPORT-856)

v 1.1.9: 2016-12-12
- fix for do_decontam crashing if nothing to decontaminate

v 1.2: 2017-01-25
- changes per SEQQC-9399
  - tarball of prodege chaff & crossblock chaff (prodege_chaff.fasta, crossblock_chaff.fasta) - handle ASXWW vs pool_decontam mode
  - align each contig in the chaffs to all_contigs (input/dirty) using seal and create report (needed vsearch, mothur 16s results also)
  - updated to use libs from micro_lib.py
  - spades.trim.fasta = raw assembly - part of unscreened template (renamed unscreened_assembly to raw_assembly in metadata.json)
- SEQQC-10301 - added do_contig_report
- replace SCG (estimateGenomeCompleteness.pl) with checkm tool (RQC-865)
- updates to PDF report per Tanja W.
- exit/fail if pool_decontam has no contigs in the check - everything screened out after prodege
--- should still do contig report
- chaff MISSING in the unscreen.metadata.json! Not supposed to be in the unscreen.metadata.json, moved to screen.metadata.json for this version
- DW-2447 - create a RAW Analysis Project type (still called "unscreen" in this code for metrics)
- rename headers in scaffolds & contigs to prefix with library name (do_contig_trim)
- fixed mapped coverage from do_gc_cov (not reported)
- do_checkm: save gene_contigs.txt (new file)

v 1.2.1: 2017-02-16
- get_pooled_status: if sag_asm but no clean_fasta file then exit(2)
- if taxonomy_* == "-" then set to "" for do_decontam step
- use tax_id as a possible taxonomy input

v 1.2.2: 2017-02-27
- do_contig_report - changed "clean" to "retained" (Tanja)
- make_chaff_tarball - if 0 file size then make note that no contigs removed
- changed to jigsaw 2.6.5
- added isRIghtOrganism after do_blast in main code
- ignore .parsed files for gzip_folder

v 1.2.3: 2017-03-07
- check if checkm file exists (otherwise sag crashes - BPOWO)

v 1.2.4: 2017-03-09
- switched back to jigsaw 2.6.4

v 1.2.5: 2017-03-10
- if no checkm results create empty files & images (RQCSUPPORT-1112)

v 1.2.6: 2017-03-14
- exit if missing keys for pdf report (BNCUY - RQCSUPPORT-1134)
- sag.cfg use prodege settings for determining if fasta is too small
- exit correctly if failed (get_next_status) (BNCUY continued on even though gc_cov failed because it couldn't copy the blast to local disk)

v 1.2.7: 2017-03-24
- use new nr database and Jigsaw 2.6.5

v 1.2.8: 2017-03-29
- fix for determing if 16s hits the assembly (RQCSUPPORT-1095)

v 1.2.9: 2017-04-05
- gc cov contam check (RQC-949)

v 1.2.10: 2017-04-11
- if not pooled, do not exit (get_pooled_status)

v 1.2.11: 2017-04-20
- blast 2.2.31 -> blast 2.6.0
- changed tetramer code to use actual java and R script

v 1.2.12: 2017-05-01
- prodege: hard coded nt database locaton, header changed in 2017 which could break prodege

v 1.2.13: 2017-06-13
- switched to Spades 3.10.1
- symlink raw/screen.metadata.json to [at_id].raw/screen.metadata.json (RQCSUPPORT-1417)
- removed misc, split_input sub folders from asm folder (not needed: Alicia)

v 1.2.14: 2017-07-20
- updated make_asm_stats, get_bbtools_version not to use config variable

v 1.2.15: 2017-10-18
- added auspice statement to *.txt and *.pdf reports
- added antifam step
- replaced run_command wih run_cmd
- replaced EnvironmentModules("load", config['bbtools_module']]) with #bbtools, #pigz and #texlive

v 1.2.16: 2017-10-27
- added check for prodigal for antifam
- updated do_blast to skip if blast already run

v 1.2.17: 2017-11-01
- removed green genes blasting, fixed green genes table in pdf report to only use Silva SSU hits (RQC-1044)

v 1.3: 2018-03-21
- converted to work on Denovo
- updated jat templates to not include ap/at in the assembly stats, only in metadata section
- new tetramer kmer code from bbtools
- RQC-1092 - gc_cov, gc_hist, contig_gc_plot use fasta name and library name
- contab db changed to: microbial_watchlist.201803.fa

v 1.3.1: 2018-03-30
- added quality reporting (RQC-1090)

v 1.4: 2018-xx-xx
- add ACDC (RQC-1083) - waiting on Kecia
- copy in contig_hist.png, genome_vs_size.png from do_contig_gc_plot
- chaff_alignment code removed (deprecated function)
- presplit reads for spades
- use $SLURM_TMP for spades
- stats.sh extended=t


$ jat validate template microbial_screen_single_minimal.yaml
$ jat validate template microbial_raw_single_minimal.yaml

To do:
- PDF: The genome names in the contaminants table should be as specific as possible e.g. genus species strain  and if  easy should be in italics with Genus in leading caps ( Escherichia coli K12)
--- taxonomy web service?
- prodege took 10 hours for BCWAG - what part? BLAST - localize this ...


prodege - overscreens and aggressive, requires knowing the genome
acdc - alternative to prodege but not production ready - also no command line version - Bill looking into this



JAT Testing: (bryce-s branch)
$ module load jamo/dev
$ jat import bryce-s/microbial_unscreen_single_minimal unscreen.metadata.json (pre sag 1.2)
$ jat import bryce-s/microbial_screen_single_minimal screen.metadata.json
successfully imported as : DEV-5720 - 2017-01-25
$ jat import bryce-s/microbial_raw_single_minimal raw.metadata.json
successfully imported as : DEV-5721 - 2017-01-25

2018-03-29 - looks okay
$ jat import dev/microbial_raw_single_minimal raw.metadata.json
$ jat import dev/microbial_screen_single_minimal screen.metadata.json
12095.1.237334.CGAGGCT-TCTTACG.filter-SAG.fastq.gz not found in jamo?


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* needs library name to look up data for Prodege, ssu/16s and reporting

~~~~~~~~~~~~~~~~~~~[ Test Cases ]
t1, t2, t3 have the fastq on sandbox (no purge)


* can use sag-test.sh to run several tests, some might fail because fastq not online

# ASXWW
$ qsub -b yes -j yes -m as -now no -w e -N sag-t1 -l ram.c=120G,h_rt=86400 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-t1.log -js 501 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t1"
$ qsub -b yes -j yes -m as -now no -w e -N sag-t1 -l ram.c=3.5G,h_rt=86400 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-t1.log -js 501 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t1"
$ qsub -b yes -j yes -m as -now no -w e -N ASXWW -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-ASXWW.log -js 501 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t1"
or run on gpint209/210 ($ ./sag.py -o t1)
- from normalize to contig_trim = 7 minutes

# ATTTZ = 25 contigs
./sag.py -o t3
$ qsub -b yes -j yes -m as -now no -w e -N sag-t3 -l ram.c=3.5G,h_rt=86400 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-t3.log -js 502 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t3"
$ qsub -b yes -j yes -m as -now no -w e -N ATTTZ -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-ATTTZ.log -js 502 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t3"

# ATNUW = 1 contig
./sag.py -o t2
$ qsub -b yes -j yes -m as -now no -w e -N sag-t2 -l ram.c=3.5G,h_rt=86400 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-t2.log -js 502 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t2"
$ qsub -b yes -j yes -m as -now no -w e -N sag-t2 -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-t2.log -js 502 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t2"


# BAGOO - w/o chaff
./sag.py -o t4
x qsub -b yes -j yes -m as -now no -w e -N BAGOO -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BAGOO.log -js 504 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t4"
$ qsub -b yes -j yes -m as -now no -w e -N BAGOO -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BAGOO.log -js 504 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t4"


# BAGOX - w/ chaff
./sag.py -o t5
x qsub -b yes -j yes -m as -now no -w e -N BAGOX -l ram.c=3.5G,h_rt=86400 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BAGOX.log -js 505 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t5"
$ qsub -b yes -j yes -m as -now no -w e -N BAGOX -l ram.c=3.5G,h_rt=86400 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BAGOX.log -js 505 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t5"

# t6 = BCWAG
$ qsub -b yes -j yes -m as -now no -w e -N sag-t6 -l ram.c=3.5G,h_rt=86400 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-t6.log -js 505 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t6"

# ATNUA = ncbi prescreen
- does not run prodege
./sag.py -o t5
$ qsub -b yes -j yes -m as -now no -w e -N sag-t5 -l ram.c=120G,h_rt=86400 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-t5.log -js 505 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag.py -o t5"




ATXOC fix for contam_id #s
- old atxoc didn't summarize contam_id, skip for now



sag: fail if pool_asm has no reads when we get it (exit code = 8?), look at 16s reporting on chaff... use all 16s's?


What if sag_decontam removed some contigs but it didn't run through prodege.
- still considered screened? no, its the unscreened assembly



Future:
- more detection built in for problems (low assembly bases?)
- update analysis to use the same blast results for screened & unscreened, filter out results for each type so we only need to run once
- new tetramer analysis


Screened = sag_decontam_output_clean.fna
unscreened = spades/scaffolds.2k.fasta or pool_decontam.fasta
- no-decontam option = skip prodege step
raw = sapdes/scaffolds.2k.fasta (new 2017-01-24)

SEQQC-9399
Raw = assembly after trimming
Screened = raw assembly after crossblock & prodege

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~[ DENOVO ]
2017-12-07

t2: CANUT
$ sbatch /global/homes/b/brycef/git/jgi-rqc-pipeline/sag/sag-t2d.sh

$ qlogin (on denovo to get a node)
$ export PATH=/global/homes/b/brycef/miniconda2/bin:$PATH
$ source activate qaqc
$ ~/git/jgi-rqc-pipeline/sag/sag.py -o t2 -pl


"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import glob
import time
import collections
import getpass

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, run_cmd, checkpoint_step, append_rqc_file, append_rqc_stats, set_colors, get_analysis_project_id, get_msg_settings
#from rqc_constants import RQCReferenceDatabases # prodege-nt
from rqc_utility import get_blast_hit_count

from micro_lib import load_config_file, get_the_path, check_skip, make_asm_stats, save_asm_stats, get_library_info, save_file, get_file, \
get_bbtools_version, get_spades_version, get_ssu, green_genes_pdf, ncbi_prescreen_report, contam_summary_pdf, merge_template, get_taxonomy, \
get_taxonomy_by_id, format_val_pct, get_web_page, get_base_contig_cnt, create_empty_checkm_summary

# is_right_organism
import orgutils


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions


'''
Contamination Identification
- sagtaminants db - created by James and not updated (ribo removed)
- RQC-1095 - slightly modified, keep this one
- minkmerfraction=0.1 (mkf) - kmers must hit 10% of the read instead of 1 kmer hit in the read
'''
def do_contam_id():
    #'do_contam_id' - docstring + 0.01
    log.info("do_contam_id")


    fastq = get_file("input_fastq", file_dict, output_path, log)

    log.info("- input: %s", fastq)
    append_rqc_stats(rqc_stats_log, "input_fastq", fastq) # save for reporting ...

    step_name = "contam_id"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(CONTAM_ID_PATH, output_path)

    seal_stats = os.path.join(the_path, "seal.txt")


    if os.path.isfile(seal_stats):
        log.info("- skipping %s, found file: %s", step_name, seal_stats)

    elif check_skip(step_name, fastq, config, status_log, log):

        status = "%s skipped" % step_name
        return status


    else:


        seal_log = os.path.join(the_path, "seal.log")
        seal_fq = os.path.join(the_path, "seal.fastq") # is this a fastq?

        # using 50% mkf
        cmd = "#%s;seal.sh in=%s ref=%s stats=%s out=%s %s > %s 2>&1" % (config['bbtools_module'], fastq, config['contam_watchlist_db'], seal_stats, seal_fq, config['seal_params'], seal_log)
        # mkf = minkmerfraction = 50% of kmers must match read (instead of any kmer hit)

        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(seal_stats):

        append_rqc_file(rqc_file_log, "seal_contam_stats", seal_stats)

        contam_dict = {}
        fh = open(seal_stats, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            #Name   Reads   ReadsPct        Bases   BasesPct
            arr = line.split()
            #print arr
            if len(arr) > 1:
                contam = arr[1]
                read_cnt = int(arr[-4])
                base_cnt = int(arr[-2])

                if contam.startswith("gi"):
                    contam = arr[2]

                #print "%s: %s, %s" % (contam, read_cnt, base_cnt)
                if contam in contam_dict:
                    contam_dict[contam]['reads'] += read_cnt
                    contam_dict[contam]['bases'] += base_cnt
                else:
                    contam_dict[contam] = { "reads" : read_cnt, "bases" : base_cnt }

                #print "- %s: %s, %s" % (contam, contam_dict[contam]['reads'], contam_dict[contam]['bases'])

        fh.close()

        read_cnt = 0 # ttl contaminated reads
        base_cnt = 0 # ttl contaminated bases
        contam_limit = int(config['contam_limit_reads']) # must be > 100 reads
        contam_sum = os.path.join(the_path, "contam_summary.txt")
        fh = open(contam_sum, "w")
        fh.write("#contam name|read count|base count\n")
        for contam in contam_dict:
            #print "%s: %s, %s" % (contam, contam_dict[contam]['reads'], contam_dict[contam]['bases'])
            log.info("- %s: %s reads, %s bases", contam, contam_dict[contam]['reads'], contam_dict[contam]['bases'])

            if contam_dict[contam]['reads'] > contam_limit:
                fh.write("%s|%s|%s\n" % (contam, contam_dict[contam]['reads'], contam_dict[contam]['bases']))
                read_cnt += contam_dict[contam]['reads']
                base_cnt += contam_dict[contam]['bases']
        fh.close()

        append_rqc_file(rqc_file_log, os.path.basename(contam_sum), contam_sum)

        append_rqc_stats(rqc_stats_log, "contam_read_cnt", read_cnt)
        append_rqc_stats(rqc_stats_log, "contam_base_cnt", base_cnt)
        append_rqc_stats(rqc_stats_log, "seal_params", config['seal_params'])

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Normalize the fastq
- expect this to remove a LOT of reads
- normalizes depth between 2 and 100
'''
def do_normalization():
    log.info("do_normalization")


    fastq = get_file("input_fastq", file_dict, output_path, log)
    log.info("- input: %s", fastq)


    step_name = "normalize"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)



    the_path = get_the_path(NORM_PATH, output_path)


    norm_fastq = os.path.join(the_path, os.path.basename(fastq).replace(".fastq", ".norm.fastq"))

    # gzipped (or non-gzipped)
    norm_fastq2 = norm_fastq
    if norm_fastq.endswith(".gz"):
        norm_fastq2 = norm_fastq2.replace(".gz", "")
    else:
        norm_fastq2 += ".gz"




    if os.path.isfile(norm_fastq):
        log.info("- skipping %s, found file: %s", step_name, norm_fastq)
    elif os.path.isfile(norm_fastq2):
        norm_fastq = norm_fastq2
        log.info("- skipping %s, found file: %s", step_name, norm_fastq)

    else:

        norm_log = os.path.join(the_path, "norm.log")
        norm_params = config['normalize_params']

        cmd = "#%s;bbnorm.sh in=%s out=%s %s > %s 2>&1" % (config['bbtools_module'], fastq, norm_fastq, norm_params, norm_log)
        # cmd = "bbnorm.sh deterministic=f in=%s out=%s %s > %s 2>&1" % (fastq, norm_fastq, norm_params, norm_log) ## for clumpify test
        std_out, std_err, exit_code = run_cmd(cmd, log)



        tadpole_log = os.path.join(the_path, "tadpole.log")
        cmd="#%s;tadpole.sh in=%s %s > %s 2>&1" % (config['bbtools_module'], norm_fastq, config['tadpole_params'], tadpole_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(norm_fastq):

        norm_title = "normalized_fastq"
        save_file(norm_fastq, norm_title, file_dict, output_path)


        # stats on filtered fastq (just reads & bases)
        # - should skip if tsv exists
        fq_stats, fq_tsv = make_asm_stats(fastq, "input_fastq", the_path, rqc_file_log, log)
        save_asm_stats(fq_tsv, "input_fastq", rqc_stats_log, log)

        # stats on normalized fastq (just reads & bases)
        fq_stats, fq_tsv = make_asm_stats(norm_fastq, norm_title, the_path, rqc_file_log, log)
        save_asm_stats(fq_tsv, norm_title, rqc_stats_log, log)

        status = "%s complete" % step_name

        append_rqc_stats(rqc_stats_log, "normalize_params", config['normalize_params'])
        append_rqc_stats(rqc_stats_log, "tadpole_params", config['tadpole_params'])

        if os.path.getsize(norm_fastq) < 1000:
            log.error("- normalized fastq is too small!    %s", msg_fail)
            status = "%s failed" % step_name



    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Subsampling
- SAG = 20m
'''
def do_subsample():
    log.info("do_subsample")

    # find normal fastq
    fastq = get_file("normalized_fastq", file_dict, output_path, log)

    # read count before, after - bb?

    step_name = "subsample"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(SUBSAMPLE_PATH, output_path)

    subsample_fastq = os.path.join(the_path, os.path.basename(fastq).replace(".fastq", ".subsample.fastq"))
    cov_hist = os.path.join(the_path, "cov.txt") # coverage histogram of subsampled fastq

    if os.path.isfile(subsample_fastq):
        log.info("- skipping %s, found file: %s", step_name, subsample_fastq)


    elif check_skip(step_name, fastq, config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:

        subsample_log_file = os.path.join(the_path, "subsample.log")
        subsample_read_count = int(config['subsample']) # 20m for SAG
        subsample_read_count = int(subsample_read_count/2) # bbtools treats 20M as 20m PAIRED reads = 40m reads

        # reads= only process this many reads then exit
        #cmd = "reformat.sh reads=%s in=%s out=%s ow=t 2> %s" % (subsample_read_count, fastq, subsample_fastq, subsample_log_file)

        # samplereadstarget= exact number of output reads desired
        cmd = "#%s;reformat.sh samplereadstarget=%s in=%s out=%s ow=t 2> %s" % (config['bbtools_module'], subsample_read_count, fastq, subsample_fastq, subsample_log_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # coverage of subsampled fastq (runs quick)

        cov_log = os.path.join(the_path, "cov.log")
        cmd = "#%s;khist.sh in=%s khist=%s ow=t > %s 2>&1" % (config['bbtools_module'], subsample_fastq, cov_hist, cov_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(subsample_fastq):

        subsample_title = "subsampled_fastq"
        save_file(subsample_fastq, subsample_title, file_dict, output_path)

        # stats on subsampled fastq (just reads & bases)
        fq_stats, fq_tsv = make_asm_stats(subsample_fastq, subsample_title, the_path, rqc_file_log, log)
        save_asm_stats(fq_tsv, subsample_title, rqc_stats_log, log)

        append_rqc_file(rqc_file_log, "subsample_cov", cov_hist)
        append_rqc_stats(rqc_stats_log, "subsample_read_count", config['subsample'])
        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status



'''
Assemble using SPAdes
spades options: --phred-offset 33 -t 16 -m 120 --sc --careful -k 25,55,95 --12
-t = threads
-m = memory
--sc = MDA (single-cell) data
--careful = reduce indels and short mismatches
--12 = file has interleaved forward and reverse reads
-k = k-mer-sizes (odd, less than 128)
--phred-offset 33 - maybe not needed, auto function
* --tmp-dir (might speed up if on localdisk)
* --cov-cutoff - automatically determined
'''
def do_assembly():
    log.info("do_assembly")

    step_name = "assembly"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    do_split_reads = True # speeds up Spades

    # find normal fastq
    fastq = get_file("subsampled_fastq", file_dict, output_path, log)

    # read count before, after - bb?

    the_path = get_the_path(ASM_PATH, output_path)


    asm_fasta = os.path.join(the_path, "scaffolds.fasta")


    if os.path.isfile(asm_fasta):
        log.info("- skipping %s, found file: %s", step_name, asm_fasta)


    elif check_skip(step_name, fastq, config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:

        # split fastq into forward and reverse reads
        # spades does this but it is slower (Alicia/Alex)
        fastq_r1 = fastq.replace(".subsample.fastq", ".subsample.r1.fastq")
        fastq_r2 = fastq.replace(".subsample.fastq", ".subsample.r2.fastq")
        if do_split_reads:
            split_log = os.path.join(the_path, "reformat.log")
            #reformat.sh in=x.fq out1=r1.fq out2=r2.fq - use reformat rather than bbsplitpairs - Brian
            #cmd = "#%s;bbsplitpairs.sh in=%s out=%s out2=%s ow=t > %s 2>&1" % (config['bbtools_module'], fastq, fastq_r1, fastq_r2, split_log)
            cmd = "#%s;reformat.sh in=%s out1=%s out2=%s ow=t > %s 2>&1" % (config['bbtools_module'], fastq, fastq_r1, fastq_r2, split_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)


        asm_log = os.path.join(the_path, "assembly.log")


        # if burst buffer setup
        tmp_folder = os.environ.get('DW_JOB_STRIPED', '')
        # check for $SLURM_TMP, doesn't help much with runtime (39.68 min without --tmp-dir, 39.78 min with --tmp-dir)
        if not tmp_folder:
            tmp_folder = os.environ.get("SLURM_TMP", "")
            
        if tmp_folder != "":
            tmp_folder = "--tmp-dir %s" % tmp_folder
            log.info("- using tmp_folder: %s", tmp_folder)

        # 2017-05-15: spades only works on shifter in $CSCRATCH on Cori due to how spades handles an assertion (INC0101703)
        # not sure on quota ...
        the_new_path = the_path
        cluster = os.environ.get('NERSC_HOST', 'unknown')

        if cluster == "cori":
            cscratch = os.environ.get('CSCRATCH', 'unknown')
            if cscratch != "unknown":
                the_new_path = get_the_path(library_name, os.path.join(cscratch, "sag"))
                log.info("- using path: %s", the_new_path)

        # * create cscratch path on $CSCRATCH (/global/cscratch1/sd/brycef/11/)
        # * use the_path as cscratch_path
        # -m could use SHIFTER_MEM_* field
        if do_split_reads:
            cmd = "#%s;spades.py -o %s %s %s -1 %s -2 %s > %s 2>&1" % (config['spades_module'], the_new_path, tmp_folder, config['spades_params'], fastq_r1, fastq_r2, asm_log)
        else:
            cmd = "#%s;spades.py -o %s %s %s --12 %s > %s 2>&1" % (config['spades_module'], the_new_path, tmp_folder, config['spades_params'], fastq, asm_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)        


        if exit_code > 0:
            log.error("- fatal spades error: %s", exit_code)
            sys.exit(exit_code)



        # * rsync cscratch path to the_path
        if cluster == "cori":
            rsync_log = os.path.join(the_path, "cori-rsync.log")
            cmd = "rsync -a %s/* %s/. > %s 2>&1" % (the_new_path, the_path, rsync_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            # clean out cscratch - use a weekly job for this?
            if exit_code == 0:
                cmd = "rm -rf %s/*" % (the_new_path)
                std_out, std_err, exit_code = run_cmd(cmd, log)


        # remove split read files 
        if do_split_reads:
            cmd = "rm %s" % fastq_r1
            std_out, std_err, exit_code = run_cmd(cmd, log)
            cmd = "rm %s" % fastq_r2
            std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(asm_fasta):

        fasta_type = "scaffolds"
        save_file(asm_fasta, fasta_type, file_dict, output_path)

        # bbstats for scaffolds
        asm_stats, asm_tsv = make_asm_stats(asm_fasta, fasta_type, the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

        asm_contigs_fasta = os.path.join(the_path, "contigs.fasta")
        save_file(asm_contigs_fasta, "contigs", file_dict, output_path)

        get_spades_version(rqc_stats_log, config, output_path, log)
        append_rqc_stats(rqc_stats_log, "spades_params", config['spades_params'])

        # remove misc, split_input folders - not needed and saves 50% of disk space
        for f in ['misc', 'split_input']:
            cmd = "rm -rf %s" % (os.path.join(the_path, f))
            std_out, std_err, exit_code = run_cmd(cmd, log)


        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status



'''
could trim contigs & scaffolds in parallel but this is quick
trim contigs
trim scaffolds

'''
def do_contig_trim(library_name):
    log.info("do_contig_trim")


    the_path = get_the_path(CONTIG_TRIM_PATH, output_path)


    step_name = "contig_trim"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    contig_fasta = get_file("contigs", file_dict, output_path, log)
    scaffold_fasta = get_file("scaffolds", file_dict, output_path, log)
    fastq = get_file("subsampled_fastq", file_dict, output_path, log)


    contig_trim_fasta = ""
    if contig_fasta:
        contig_trim_fasta = os.path.join(the_path, os.path.basename(contig_fasta).replace(".fasta", ".trim.fasta"))

    scaffold_trim_fasta = ""
    if scaffold_fasta:
        scaffold_trim_fasta = os.path.join(the_path, os.path.basename(scaffold_fasta).replace(".fasta", ".trim.fasta"))


    if os.path.isfile(scaffold_trim_fasta) and os.path.isfile(contig_trim_fasta):
        log.info("- skipped %s, found file: %s", step_name, scaffold_trim_fasta)


    elif check_skip(step_name, scaffold_fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:

        # rename contigs to use library name

        # contigs mapping
        map_log = os.path.join(the_path, "map_contigs.log")
        cov_contig_stats = os.path.join(the_path, "contig_covstats.txt")

        # ambig=all maxindel=100 minhits=2
        if contig_fasta:
            cmd = "#%s;bbmap.sh in=%s ref=%s %s covstats=%s > %s 2>&1" % (config['bbtools_module'], fastq, contig_fasta, config['trim_map_params'], cov_contig_stats, map_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            log.error("- no contig file to trim!  %s", msg_fail)


        # scaffolds mapping
        map_log = os.path.join(the_path, "map_scaffolds.log")
        cov_scaffold_stats = os.path.join(the_path, "scaffold_covstats.txt")

        if scaffold_fasta:
            cmd = "#%s;bbmap.sh in=%s ref=%s %s covstats=%s > %s 2>&1" % (config['bbtools_module'], fastq, scaffold_fasta, config['trim_map_params'], cov_scaffold_stats, map_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            log.error("- no scaffold file to trim!  %s", msg_fail)



        # contigs trimming
        trim_log = os.path.join(the_path, "trim_contigs.log")
        contig_trim_fasta_tmp = contig_trim_fasta.replace(".trim.fasta", ".trim.unnamed.fasta") # before adding the lib name to the contig headers
        if contig_fasta:
            cmd = "#%s;filterbycoverage.sh in=%s out=%s cov=%s %s > %s 2>&1" % (config['bbtools_module'], contig_fasta, contig_trim_fasta_tmp, cov_contig_stats, config['trim_params'], trim_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            log.error("- no contig file to filter!  %s", msg_fail)

        # scaffolds trimming
        trim_log = os.path.join(the_path, "trim_scaffolds.log")
        scaffold_trim_fasta_tmp = scaffold_trim_fasta.replace(".trim.fasta", ".trim.unnamed.fasta") # before adding the lib name to the scaffold headers
        if scaffold_fasta:
            cmd = "#%s;filterbycoverage.sh in=%s out=%s cov=%s %s > %s 2>&1" % (config['bbtools_module'], scaffold_fasta, scaffold_trim_fasta_tmp, cov_scaffold_stats, config['trim_params'], trim_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            log.error("- no scaffold file to filter!  %s", msg_fail)


        # rename headers to >(lib_name)_(header)
        rename_log = os.path.join(the_path, "rename_contigs.log")
        cmd = "#%s;bbrename.sh in=%s out=%s prefix=%s addprefix ow=t > %s 2>&1" % (config['bbtools_module'], contig_trim_fasta_tmp, contig_trim_fasta, library_name, rename_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        rename_log = os.path.join(the_path, "rename_scaffolds.log")
        cmd = "#%s;bbrename.sh in=%s out=%s prefix=%s addprefix ow=t > %s 2>&1" % (config['bbtools_module'], scaffold_trim_fasta_tmp, scaffold_trim_fasta, library_name, rename_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)



    # check for trimmed files
    if os.path.isfile(contig_trim_fasta) and os.path.isfile(scaffold_trim_fasta):

        save_file(contig_trim_fasta, "contigs_trim", file_dict, output_path)

        fasta_type = "scaffolds_trim"
        save_file(scaffold_trim_fasta, fasta_type, file_dict, output_path)

        # bbstats on 2k files
        asm_stats, asm_tsv = make_asm_stats(scaffold_trim_fasta, fasta_type, the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

        append_rqc_stats(rqc_stats_log, "trim_map_params", config['trim_map_params'])
        append_rqc_stats(rqc_stats_log, "trim_params", config['trim_params'])


        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
ProDeGe (Kristin Tennessen's program)
- removes contam by taxonomy
- needs 3+ contigs, and asm bases > 200kb
~ 2 hrs
https://prodege.jgi.doe.gov/

- either pool_decontam or scaffolds_trim for fasta_type

- tax_name: look up taxonomy and use this instead of lib info (lib info uses gold's tax id ->

Non-pooled SAG:
trim -> Prodege
- not enough contigs/bps = no screened file, no screened analysis (creates empty *_output_clean.fna)
- prodege removes a few contigs = screened file, screened analysis (uses *_output_clean.fna)
- prodege removes all contigs = empty screened file, no screened analysis
- prodege removes no contigs = screened file, screened analysis (uses *_output_clean.fna)

Pooled SAG:
trim -> Pooled Decontam -> Prodege
- not enough contigs/bps = no screened file, no screened analysis (creates empty *_output_clean.fna)
- prodege removes a few contigs = screened file, screened analysis (uses *_output_clean.fna)
- prodege removes all contigs = empty screened file, no screened analysis
- prodege removes no contigs = use Pooled Decontam asm as screened file, screened analysis (uses *_output_clean.fna)
--- happens if prodege doesn't run
--- don't do this, if prodege doesn't run on it because its too small Kecia won't release it anyways and its consistant with previous behavior

- pooled decontam removes all contigs = no screened file, no screened analysis


** check with latest nt, IMG-db should be updated also?  how?
'''
def do_decontam(fasta_type, library_name, tax_id, org_name):
    log.info("do_decontam")


    step_name = "decontamination"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(DECONTAM_PATH, output_path)

    new_fasta_type = None # what to use for decontamination (pooled if prodege doesn't work)

    fasta = get_file(fasta_type, file_dict, output_path, log)
    # if pooled and empty file, use scaffolds_trim
    if not fasta and fasta_type == "pool_decontam":
        fasta_type = "scaffolds_trim"
        fasta = get_file(fasta_type, file_dict, output_path, log)



    # output in prodege/job_name/
    #job_name = "sd-%s" % library_name
    job_name = "sag_decontam" # to match Jigsaw-SAG

    clean_fasta = ""
    contam_fasta = ""
    if fasta:
        clean_fasta = os.path.join(the_path, job_name, "%s_output_clean.fna" % job_name)
        contam_fasta = os.path.join(the_path, job_name, "%s_output_contam.fna" % job_name)


    if os.path.isfile(clean_fasta):
        log.info("- skipped %s, found file: %s", step_name, clean_fasta)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status, fasta_type


    else:

        # don't really need this check, prodege will create empty dirty file & copy input file to clean file by itself
        # - if this is the case then don't run screened reporting


        # check size of input fasta (could be trimmed or from pooled decontam)
        asm_stats, asm_tsv = make_asm_stats(fasta, "tmp", the_path, rqc_file_log, log)

        # parse asm_tsv to get contigs and bases
        base_cnt, contig_cnt = get_base_contig_cnt(asm_tsv)


        # ProDeGe needs to be greater than these values to run
        base_limit = int(config['prodege_base_min']) # 200000
        base_status = msg_ok
        contig_limit = int(config['prodege_contig_min']) # 3
        contig_status = msg_ok

        if base_cnt < base_limit:
            base_status = msg_fail
        if contig_cnt < contig_limit:
            contig_status = msg_fail


        log.info("- ProDeGe check: bases = %s > %s %s, contigs = %s > %s  %s", base_cnt, base_limit, base_status, contig_cnt, contig_limit, contig_status)

        #if 1==1: # testing!!!!!
        if base_cnt < base_limit or contig_cnt < contig_limit:

            log.info("- stat check failed, skipping %s  %s", step_name, msg_warn)

            sag_path = os.path.join(the_path, job_name)
            if not os.path.isdir(sag_path):
                os.makedirs(sag_path)

            # 2017-12-07: force to use existing fasta
            #cmd = "cp %s %s" % (fasta, clean_fasta)
            #std_out, std_err, exit_code = run_cmd(cmd, log)

            # write to the file instead of touching it, in case it exists from a previous run - also creates an empty screened file
            # unscreened will continue through the process
            fh = open(clean_fasta, "w")
            fh.write("")
            fh.close()

            fh = open(contam_fasta, "w")
            fh.write("")
            fh.close()

        else:

            # read config file from config directory
            prodege_config_file = os.path.realpath(os.path.join(my_path, "../sag/config/prodege.config"))


            merge_dict = {}
            # use organism name provided on command line, otherwise use lib_info
            if org_name:
                merge_dict = get_taxonomy(org_name, log)
                merge_dict['display_name'] = org_name
                # use new web service!!!
                #sys.exit(31)
            elif tax_id:
                merge_dict = get_taxonomy_by_id(tax_id, log)
                merge_dict['display_name'] = merge_dict['tax_species']
            else:
                merge_dict = get_library_info(library_name, log)
                if 'library_name' in merge_dict:
                    merge_dict['display_name'] = merge_dict['ncbi_organism_name']


            merge_dict['output_path'] = the_path
            # check if in /scratch?  Using Dec 2016 because headers in nt changed in 2017
            #merge_dict['nt_db'] = "/global/dna/shared/rqc/ref_databases/ncbi/201612151040/nt/nt" # RQCReferenceDatabases.NT
            #merge_dict['nt_db'] = "/global/dna/shared/rqc/ref_databases/ncbi/20170413/nt/nt" # RQCReferenceDatabases.NT
            merge_dict['nt_db'] = "/global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nt/nt" # RQCReferenceDatabases.NT
            # check if localized: (nt/nt not there)
            #nt_file = "/scratch/blastdb/global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nt/nt.00.nhd" # misc nt index file

            merge_dict['fasta'] = fasta
            merge_dict['job_name'] = job_name

            if 'display_name' in merge_dict:

                merge_dict['display_name'] = str(merge_dict['display_name']).replace("unclassified ", "") # unclassified Archaea -> Archaea, ProDeGe works better this way


                # merge with config file
                my_prodege_cfg = os.path.join(the_path, "prodege.cfg")
                if os.path.isfile(my_prodege_cfg):
                    log.info("- using existing prodege config file: %s", my_prodege_cfg)
                else:
                    p_buffer = ""
                    fh = open(prodege_config_file, "r")
                    for line in fh:

                        line = line.strip()
                        if line.startswith("#"):
                            continue

                        p_buffer += line + "\n"


                    fh.close()



                    for the_key in merge_dict:
                        my_key = "{%s}" % the_key
                        p_buffer = p_buffer.replace(my_key, str(merge_dict[the_key]))

                    fhw = open(my_prodege_cfg, "w")
                    fhw.write(p_buffer)
                    fhw.close()

                    log.info("- created prodege config: %s", my_prodege_cfg)



                ### run ProDeGe
                ## Bill looking in ACDC as an alternative (2017-12-07)

                prodege_log = os.path.join(the_path, "scd.log")
                # module load R - in config['prodege']
                #cmd = "%s %s > %s 2>&1" % (config['prodege'], my_prodege_cfg, prodege_log)

                # prodege vol's saved on qc_user's hsi account
                # mount volumes - fails ...  INC0112407 - 2017-12-07
                # install_location important? yes - set to /prodege
                # on denovo, volume mounts must be world readable for the whole path ...
                cmd = """shifter --image=bryce911/prodege \
--volume %s:/prodege/IMG-db \
--volume %s:/prodege/NCBI-nt-euk \
/prodege/bin/prodege.sh %s > %s 2>&1""" % (config['prodege_img_vol'], config['prodege_nt_euk_vol'], my_prodege_cfg, prodege_log)

                std_out, std_err, exit_code = run_cmd(cmd, log)


            else:
                log.error("- skipping %s, no library_info/organism name available!  %s", step_name, msg_warn)



    if os.path.isfile(clean_fasta):


        # save decontam fasta (fna)
        # input = trim, output = clean_scaffolds - even if scaffold_cnt == 0
        # input = pool_decontam, output = clean_scaffolds if scaffold_cnt > 0
        #                        output = pool_decontam if scaffold_cnt == 0


        new_fasta_type = "clean_scaffolds"

        # bbstats on clean fasta
        asm_stats, asm_tsv = make_asm_stats(clean_fasta, new_fasta_type, the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, new_fasta_type, rqc_stats_log, log)
        base_cnt, scaffold_cnt = get_base_contig_cnt(asm_tsv)

        # this all looks the same but keep here for the logic documentation
        if fasta_type == "pool_decontam":

            if scaffold_cnt > 0:
                save_file(clean_fasta, new_fasta_type, file_dict, output_path)
            else:

                # don't do this:
                #log.warn("- prodege not run, using crossblock fasta  %s", msg_warn)
                #save_file(fasta, new_fasta_type, file_dict, output_path)

                # save empty fasta as cleaned, then rest of analysis should not run for cleaned
                save_file(clean_fasta, new_fasta_type, file_dict, output_path)
        else:

            # even if its empty, save it - the other functions will skip if the fasta is empty
            save_file(clean_fasta, new_fasta_type, file_dict, output_path)




        #log.info("- decontam_fasta_type: %s", my_decontam_fasta_type)

        # bbstats on dirty fasta (contamination) - more chaff for output
        asm_stats, asm_tsv = make_asm_stats(contam_fasta, "contam_scaffolds", the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, "contam_scaffolds", rqc_stats_log, log)
        save_file(contam_fasta, "prodege_chaff", file_dict, output_path)

        append_rqc_stats(rqc_stats_log, "prodege_version", config['prodege_version'])

        status = "%s complete" % step_name

    else:

        status = "%s failed" % step_name



    checkpoint_step(status_log, status)

    return status, new_fasta_type

'''
Run ACDC to do decontamination instead of prodege
- Automated Contamination Detection & Confidence Estimation for single-cell genome data
- Bill maintains docker container for ACDC, Kraken maintained by Bryce
~ 30 minutes to run
* png files generated are just icons for the web page
'''
def do_acdc(fasta_type, library_name):
    log.info("do_acdc: %s", library_name)

    step_name = "decontamination"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(ACDC_DECONTAM_PATH, output_path)

    new_fasta_type = None # what to use for decontamination (pooled if prodege doesn't work)

    fasta = get_file(fasta_type, file_dict, output_path, log)
    # if pooled and empty file, use scaffolds_trim
    if not fasta and fasta_type == "pool_decontam":
        fasta_type = "scaffolds_trim"
        fasta = get_file(fasta_type, file_dict, output_path, log)


    clean_fasta = ""
    contam_fasta = ""
    if fasta:
        clean_fasta = os.path.join(the_path, "clean.fasta")
        contam_fasta = os.path.join(the_path, "contamination.fasta")


    if os.path.isfile(clean_fasta):
        log.info("- skipped %s, found file: %s", step_name, clean_fasta)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status, fasta_type


    else:

        cmd = ""
        acdc_cmd = os.path.realpath(config['acdc_cmd'].replace("[my_path]", my_path))
        acdc_log = os.path.join(the_path, "acdc2.log")
                
        cmd = "%s -l %s -f %s -o %s > %s 2>&1" % (acdc_cmd, library_name, fasta, the_path, acdc_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(clean_fasta):
 
        # save decontam fasta (fna)
        # input = trim, output = clean_scaffolds - even if scaffold_cnt == 0
        # input = pool_decontam, output = clean_scaffolds if scaffold_cnt > 0
        #                        output = pool_decontam if scaffold_cnt == 0


        new_fasta_type = "clean_scaffolds"

        # bbstats on clean fasta
        asm_stats, asm_tsv = make_asm_stats(clean_fasta, new_fasta_type, the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, new_fasta_type, rqc_stats_log, log)
        base_cnt, scaffold_cnt = get_base_contig_cnt(asm_tsv)

        # this all looks the same but keep here for the logic documentation
        if fasta_type == "pool_decontam":

            if scaffold_cnt > 0:
                save_file(clean_fasta, new_fasta_type, file_dict, output_path)
            else:

                # don't do this:
                #log.warn("- acdc not run, using crossblock fasta  %s", msg_warn)
                #save_file(fasta, new_fasta_type, file_dict, output_path)

                # save empty fasta as cleaned, then rest of analysis should not run for cleaned
                save_file(clean_fasta, new_fasta_type, file_dict, output_path)
        else:

            # even if its empty, save it - the other functions will skip if the fasta is empty
            save_file(clean_fasta, new_fasta_type, file_dict, output_path)
        

        asm_stats, asm_tsv = make_asm_stats(contam_fasta, "contam_scaffolds", the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, "contam_scaffolds", rqc_stats_log, log)
        save_file(contam_fasta, "acdc_chaff", file_dict, output_path)

        acdc_version = "unknown"
        fh = open(os.path.join(the_path, "acdc_version.txt"), "r")
        for line in fh:
            if line:
                acdc_version = line.strip()
        fh.close()
        
        append_rqc_stats(rqc_stats_log, "acdc_version", acdc_version)

        status = "%s complete" % step_name

    else:

        status = "%s failed" % step_name



    checkpoint_step(status_log, status)

    
    return status, new_fasta_type

## ~~~~~~~~~~~~~~~~~~~
## QC Analysis things
## - these need output_path because output_path = output_path + screen or unscreen

'''
Contig gc stats
- stats & plot about contig gc levels
'''
def do_contig_gc_plot(my_output_path, fasta_type, mode):
    log.info("do_contig_gc_plot: %s", mode)

    step_name = "contig_gc_plot"

    if mode:
        step_name += ":%s" % mode

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(CONTIG_GC_PATH, my_output_path)

    fasta = get_file(fasta_type, file_dict, output_path, log)
    contig_gc_plot = os.path.join(the_path, "contig_gc.png")



    if os.path.isfile(contig_gc_plot):
        log.info("- skipping %s, found file: %s", step_name, contig_gc_plot)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        contig_gc_cmd = os.path.realpath(config['contig_gc_cmd'].replace("[my_path]", my_path))
        contig_gc_log = os.path.join(the_path, "contig_gc.log")
        #cmd = "%s -l %s -f %s -o %s > %s 2>&1" % (contig_gc_cmd, library_name, fasta, the_path, contig_gc_log)
        cmd = "%s -l %s -f %s -o %s > %s 2>&1" % (contig_gc_cmd, "%s:%s" % (library_name, os.path.basename(fasta)), fasta, the_path, contig_gc_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(contig_gc_plot):



        stats_file = os.path.join(the_path, "gc.stats.txt")
        gc_avg = 0.0
        gc_stdev = 0.0
        gc_median = 0.0
        fh = open(stats_file, "r")
        for line in fh:
            line = line.strip()
            arr = line.split("=")
            if line.startswith("gc_avg"):
                gc_avg = arr[-1]
            if line.startswith("gc_stdev"):
                gc_stdev = arr[-1]
            if line.startswith("gc_median"):
                gc_median = arr[-1]
        fh.close()

        log.info("- contig gc: %s +/- %s", "{:.2f}".format(float(gc_avg)), "{:.2f}".format(float(gc_stdev)))
        append_rqc_stats(rqc_stats_log, "%s_contig_gc_avg" % mode, gc_avg)
        append_rqc_stats(rqc_stats_log, "%s_contig_gc_stdev" % mode, gc_stdev)
        append_rqc_stats(rqc_stats_log, "%s_contig_gc_median" % mode, gc_median)

        f_key = "%s_%s" % (mode, os.path.basename(contig_gc_plot))
        log.info("- saving file: %s", f_key)
        append_rqc_file(rqc_file_log, f_key, contig_gc_plot)

        for png_file in ['contig_hist.png', 'genome_vs_size.png']:
            append_rqc_file(rqc_file_log, "%s_%s" % (mode, png_file), os.path.join(the_path, png_file))

        # weighted gc, gc histogram files
        append_rqc_file(rqc_file_log, "%s_contig_gc_hist" % mode, os.path.join(the_path, "contig.gc"))
        append_rqc_file(rqc_file_log, "%s_contig_gc_whist" % mode, os.path.join(the_path, "contig-weighted.gc"))


        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)

    return status



'''
GC Coverage of the contigs
- creates cloud images gc vs cov (gc_cov_Kingdom.png)
'''
def do_gc_cov(my_output_path, fasta_type, mode):
    log.info("do_gc_cov: %s", mode)

    step_name = "gc_cov"
    if mode:
        step_name += ":%s" % mode

    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(GC_COV_PATH, my_output_path)

    fasta = get_file(fasta_type, file_dict, output_path, log)
    fastq_type = "subsampled_fastq"
    fastq = get_file(fastq_type, file_dict, output_path, log)




    summary = os.path.join(the_path, "summary.txt")


    if os.path.isfile(summary):
        log.info("- skipping %s, found file: %s", step_name, summary)


    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        gc_cov_cmd = os.path.realpath(config['gc_cov_cmd'].replace("[my_path]", my_path))
        gc_cov_log = os.path.join(the_path, "gc_cov2.log")
        #cmd = "%s -l %s -f %s -fq %s -o %s > %s 2>&1" % (gc_cov_cmd, library_name, fasta, fastq, the_path, gc_cov_log)
        # RQC-1092 - Kecia wants to use the fasta name
        cmd = "%s -l %s -f %s -fq %s -o %s > %s 2>&1" % (gc_cov_cmd, "%s:%s" % (library_name, os.path.basename(fasta)), fasta, fastq, the_path, gc_cov_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(summary):

        # save *.png, *.txt for RQC
        file_list = glob.glob(os.path.join(the_path, "*"))
        for f in file_list:
            if f.endswith(".png") or f.endswith(".txt"):

                f_key = "%s_%s" % (mode, os.path.basename(f))

                # need to reaname files with multiple "." to work with pdf maker (texlive)
                # e.g. class.taxonomy.png is bad, needs to be converted to class_taxonomy.pdf
                base_name = os.path.basename(f)
                # gc_cov.py: scaffolds.trim.shreds_gc_cov_no_legend.png

                log.info("- saving file: %s", f_key)
                append_rqc_file(rqc_file_log, f_key, f)

        # save coverage stats for reporting
        cov_stats = os.path.join(the_path, "covstats.txt")
        if os.path.isfile(cov_stats):
            fh = open(cov_stats, "r")
            for line in fh:
                arr = line.strip().split("=")
                if len(arr) > 1:
                    f_key = "%s_cov_%s" % (mode, arr[0])

                    val = "{:.1f}".format(float(arr[1]))
                    append_rqc_stats(rqc_stats_log, f_key, val)

            fh.close()

        # look for anything with less than 20x coverage - Kecia's QC for possible contam: RQC-949
        gc_cov_contam_cnt = 0
        fh = open(summary, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            arr = line.strip().split()
            cov = int(float(arr[1])) # Avg_fold

            #print cov, arr[0]
            # Kecia's level:
            if cov < 20:
                gc_cov_contam_cnt += 1

        fh.close()

        append_rqc_stats(rqc_stats_log, "%s_gc_cov_contam_cnt" % mode, gc_cov_contam_cnt)
        log.info("- gc_cov contam cnt (cov < 20): %s", gc_cov_contam_cnt)


        map_shreds_txt = os.path.join(the_path, "shred", "frag_vs_gc_cov.txt")
        if os.path.isfile(map_shreds_txt):
            append_rqc_file(rqc_file_log, "%s_gc_cov_shreds_txt" % mode, map_shreds_txt)


        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Blast the contigs
- 5+ hours (to report "no hits")

2 minutes for this part if localized just for NT ...
$ module load blast+
$ blastn -query /global/projectb/scratch/brycef/sag/BAGHS/pool_asm/pool_decontam.fasta \
 -db /scratch/rqc/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted \
 -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true   \
 -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids'   \
 -num_threads 8 > nt.log

 $ ~/git/jgi-rqc-pipeline/tools/run_blastplus.py -o /global/projectb/scratch/brycef/sag/BAGHS/unscreen/megablast -q /global/projectb/scratch/brycef/sag/BAGHS/pool_asm/pool_decontam.fasta \
 -d /scratch/rqc/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted

'''
def do_blast(my_output_path, fasta_type, mode):
    log.info("do_blast: %s", mode)

    step_name = "blast"

    if mode:
        step_name += ":%s" % mode


    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(BLAST_PATH, my_output_path)


    fasta = get_file(fasta_type, file_dict, output_path, log)

    done_file = os.path.join(the_path, "blastx.done")


    if os.path.isfile(done_file):
        log.info("- skipping %s, found file: %s", step_name, done_file)


    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        # copy database list to the run folder
        db_fof = os.path.realpath(os.path.join(my_path, "../sag/config/sag_contig_megablast.fof"))
        my_db = os.path.join(the_path, os.path.basename(db_fof))

        if not os.path.isfile(my_db):
            cmd = "cp %s %s" % (db_fof, my_db)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        blast_cmd = os.path.realpath(config['run_blast_cmd'].replace("[my_path]", my_path))

        fh = open(my_db, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue
            if not line:
                continue

            # if find log file, can we skip this?
            blast_db = line
            blast_db_name = os.path.basename(blast_db)
            blast_log = os.path.join(the_path, "blastplus_%s.log" % blast_db_name)
            done_file = os.path.join(the_path, "blastplus_%s.done" % blast_db_name)
            if os.path.isfile(done_file):
                log.info("- skipping blast_db: %s, found file: %s", blast_db_name, done_file)

            else:
                log.info("- blasting: %s", os.path.basename(blast_db))

                cmd = "%s -s -o %s -q %s -d %s > %s" % (blast_cmd, the_path, fasta, blast_db, blast_log)
                # -s = show taxonomy in parsed results
                std_out, std_err, exit_code = run_cmd(cmd, log)


                fhd = open(done_file, "w")
                fhd.write("%s|%s\n" % (os.path.basename(blast_db), exit_code))
                fhd.close()

        fh.close()

    if os.path.isfile(done_file):

        # save blast results, count blast hits
        blast_total_hits = 0

        blast_list = glob.glob(os.path.join(the_path, "*.vs.*.parsed"))
        # look for parsed, then save the stats from the other files
        ext_list = ['.tophit', '.taxlist'] #, '.top100hit']
        for blast_file in blast_list:

            skip_file = False
            for e in ext_list:
                if blast_file.endswith(e):
                    skip_file = True


            if not skip_file:
                blast_name = os.path.basename(blast_file)
                arr = blast_name.split(".vs.")
                blast_name = "%s_%s" % (mode, arr[-1])

                hit_cnt = get_blast_hit_count(blast_file)
                log.info("- %s (%s hits)", blast_name, hit_cnt)


                if os.path.getsize(blast_file) > 0:

                    # save *.parsed and how many hits
                    append_rqc_file(rqc_file_log, blast_name, blast_file)

                    append_rqc_stats(rqc_stats_log, "%s_cnt" % blast_name, hit_cnt)

                    for e in ext_list:

                        blast_file_new = "%s%s" % (blast_file, e)
                        blast_file_new_key = "%s%s" % (blast_name, e)

                        if os.path.isfile(blast_file_new):

                            hit_cnt = get_blast_hit_count(blast_file_new)
                            # count total hits for stats
                            if e == ".tophit":
                                blast_total_hits += hit_cnt

                            append_rqc_stats(rqc_stats_log, "%s_cnt" % blast_file_new_key, hit_cnt)

                            append_rqc_file(rqc_file_log, blast_file_new_key, blast_file_new)
                            log.info("- %s (%s hits)", blast_file_new_key, hit_cnt)

                        else:
                            append_rqc_stats(rqc_stats_log, blast_file_new_key + "_cnt", 0)
                            log.info("- %s   MISSING", os.path.basename(blast_file_new))

        append_rqc_stats(rqc_stats_log, "%s_blast_cnt" % mode, blast_total_hits)

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status

'''
Stephan's code for scanning blast results looking if we sequenced what we expect
- uses library name to look up the tax_id and tax_name
- don't expect many SAGs to match the hits
- used for auto-qc
'''
def is_right_thing(my_output_path, tax_id, tax_name, library_name, mode):

    log.info("is_right_thing: %s", library_name)

    the_path = get_the_path(BLAST_PATH, my_output_path)

    if library_name and not tax_name and not tax_id:
        tax_dict = get_library_info(library_name, log)
        #tax_name = tax_dict['tax_species']
        # crawl up tree
        tax_name = tax_dict['gold_organism_name']
        if not tax_name:
            tax_name = tax_dict['ncbi_organism_name']
            
        #tax_list = ['tax_species', 'tax_genus', ,'tax_family', 'tax_order', 'tax_class']
        #for tax in tax_list:
        #    if tax_dict[tax] != "-":
        #        tax_name = tax_dict[tax]
        #        break
        
            
        tax_id = tax_dict['gold_tax_id']
        if tax_id == 0:
            tax_id = tax_dict['ncbi_tax_id']
            
    file_list = [] # list of blast .parsed files to use for checking

    parsed_list = glob.glob(os.path.join(the_path, "*.parsed"))
    for parsed_file in parsed_list:
        if parsed_file.endswith("vs.nt_bbdedupe_bbmasked_formatted.parsed"):
            file_list.append(parsed_file)
        elif parsed_file.endswith("vs.refseq.archaea.parsed"):
            file_list.append(parsed_file)
        elif parsed_file.endswith("vs.refseq.bacteria.parsed"):
            file_list.append(parsed_file)

    is_right_organism = 0

    if not tax_name:
        log.error("- no tax name to check!  %s", msg_warn)
    else:
        log.info("- tax_name: %s, tax_id: %s", tax_name, tax_id)

        is_right_organism = orgutils.Utils(debug=False).isRightOrganism(file_list, tax_name, taxId=tax_id, maxExpectScore=config['iro_max_expect_score'],
                                                                            minAlignLength=config['iro_min_align_length'], minPctId=config['iro_min_pct_id'])



    status = msg_fail
    if is_right_organism == 1:
        status = msg_ok
    log.info("- is_right_organism: %s  %s", is_right_organism, status)

    append_rqc_stats(rqc_stats_log, "%s_isRightOrganism" % mode, is_right_organism)





'''
Blast for the 16s (pretty fast)
- in a separate function so we can run it w/o re-running all of blast steps
- auto-qc criteria?  e = 0, pct_id = 98, min_len = 500
- does the assembly contain the 16s (SSU) from ITS?

'''
def do_blast_16s(my_output_path, fasta_type, library_name, mode):
    log.info("do_blast_16s: %s", mode)

    step_name = "blast_16s"

    if mode:
        step_name += ":%s" % mode


    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(BLAST_16S_PATH, my_output_path)


    fasta = get_file(fasta_type, file_dict, output_path, log)

    done_file = os.path.join(the_path, "blast16s.done")


    if os.path.isfile(done_file):
        log.info("- skipping %s, found file: %s", step_name, done_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:


        blast_cmd = "%s/run_blastplus.py" % (os.path.realpath(os.path.join(my_path, '../tools')))

        db_16s = os.path.join(the_path, "%s_16s.fasta" % library_name)

        if not os.path.isfile(db_16s):
            get_ssu(library_name, db_16s, log)
            save_file(db_16s, "16s", file_dict, output_path)

            # makeblastdb -dbtype nucl -in AUHTB_16s.fasta
            cmd = "#%s;makeblastdb -dbtype nucl -in %s" % (config['blast_module'], db_16s)
            std_out, std_err, exit_code = run_cmd(cmd, log)


        if os.path.getsize(db_16s) > 5:

            blast_log = os.path.join(the_path, "blastplus_16s_%s.log" % library_name)
            log.info("- blasting: 16s %s", library_name)
            # -l = don't localize this one
            cmd = "%s -l -o %s -q %s -d %s > %s" % (blast_cmd, the_path, fasta, db_16s, blast_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)


            if exit_code == 0:
                fh = open(done_file, "w")
                fh.write("%s_16s|%s\n" % (library_name, exit_code))
                fh.close()

        else:
            log.error("- 16s not found for %s", library_name)



    if os.path.isfile(done_file):

        # save blast results, count blast hits

        # criteria to match
        # num hits
        # expect < max_expect_score && align_len >= min_align_length && pct_id >= min_pct_id
        max_expect_score = 0.01
        min_align_length = 500
        min_pct_id = 98.0

        # only save the tophit file for 16s
        blast_list = glob.glob(os.path.join(the_path, "*.vs.*.parsed.tophit"))

        blast_file_key = "%s_blast_16s.parsed.tophit" % mode
        for blast_file in blast_list:

            hit_cnt = get_blast_hit_count(blast_file)
            append_rqc_stats(rqc_stats_log, blast_file_key + "_cnt", hit_cnt)

            append_rqc_file(rqc_file_log, blast_file_key, blast_file)
            log.info("- %s (%s hits)", blast_file_key, hit_cnt)

            # auto-qc to determine if we got a good 16s hit
            expect_score = 0.0
            align_length = 0
            pct_id = 0.0

            fh = open(blast_file, "r")
            for line in fh:
                if line.startswith("#"):
                    continue
                arr = line.split()
                #query subject expect length perc_id q_length s_length

                # tophit can be multi-line, use the first line
                if align_length == 0:
                    try:
                        expect_score = float(arr[2])
                        align_length = int(arr[3])
                        pct_id = float(arr[4])
                    except:
                        log.error("- could not get tophit data from %s", blast_file)

            fh.close()

            # auto-qc - doesn't need to be here
            blast_status = "16s not found"
            if expect_score < max_expect_score and align_length >= min_align_length and pct_id >= min_pct_id:
                blast_status = "16s found"
            log.info("- 16s: expect %s < %s, align_length %s >= %s, pct_id %s >= %s", expect_score, max_expect_score, align_length, min_align_length, pct_id, min_pct_id)
            append_rqc_stats(rqc_stats_log, "%s_16s_hit" % mode, blast_status)

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name
        if not os.path.getsize(db_16s) > 5:
            # skip 16s if no ssu found
            status = "%s skipped" % step_name

    checkpoint_step(status_log, status)

    return status



'''
reads_coverage - gc histograms
uses prodigal, lastal, bbtools

- reads_kingdom.png = histogram
'''
def do_gc_hist(my_output_path, fasta_type, library_name, mode):
    log.info("do_gc_hist: %s", mode)


    step_name = "gc_hist"
    if mode:
        step_name += ":%s" % mode

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(GC_HIST_PATH, my_output_path)


    contigs_class_png = os.path.join(the_path, "contigs_Class.png")
    fasta = get_file(fasta_type, file_dict, output_path, log)


    if os.path.isfile(contigs_class_png):
        log.info("- skipping %s, found file: %s", step_name, contigs_class_png)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        gc_hist_cmd = os.path.realpath(config['gc_hist_cmd'].replace("[my_path]", my_path))
        gc_hist_log = os.path.join(the_path, "gc_hist2.log")
        #cmd = "%s -l %s -f %s -o %s > %s 2>&1" % (gc_hist_cmd, library_name, fasta, the_path, gc_hist_log)
        cmd = "%s -l %s -f %s -o %s > %s 2>&1" % (gc_hist_cmd, "%s:%s" % (library_name, os.path.basename(fasta)), fasta, the_path, gc_hist_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(contigs_class_png):

        # save pngs
        png_list = glob.glob(os.path.join(the_path, "*.png"))
        for png in png_list:

            png_key = "%s_megan_%s" % (mode, os.path.basename(png))
            log.info("- saving file: %s", png_key)
            append_rqc_file(rqc_file_log, png_key, png)


        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Check for contigs that should not be submitted to NCBI
e.g. Cow, cat, dog, ...
- Brian Foster wrote the code to do this ...
The NCBI database is : $sandbox/rqc/prod/pipelines/external_tools/ncbiScreening/database/gcontam (last updated September 2013)

'''
def do_ncbi_prescreen(my_output_path, fasta_type, mode):
    log.info("do_ncbi_prescreen: %s", mode)

    step_name = "ncbi_prescreen"
    if mode:
        step_name += ":%s" % mode

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(NCBI_PRESCREEN_PATH, my_output_path)


    fasta = get_file(fasta_type, file_dict, output_path, log)
    out_file = os.path.join(the_path, "ncbi_screen.out")


    if os.path.isfile(out_file):
        log.info("- skipping %s, found file: %s", step_name, out_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:

        prescreen_log = os.path.join(the_path, "ncbi_prescreen.log")

        blast_cmd = os.path.realpath(config['run_blast_cmd'].replace("[my_path]", my_path))
        log.info("- blasting vs ncbi_prescreen: %s", library_name)

        # old version used megablast 2.2.21
        # /projectb/sandbox/rqc/prod/pipelines/external_tools/ncbiScreening/ncbiPreScreen.pl
        # megablast -p 90.0 -D3 -F \"m D\" -fT -UT -R -d (db) -i (query file)
        # -p 90.0 = identity percentage cut-off
        # -D3 = tab-delimited one line format
        # -F = filter query sequence
        # -fT = show full IDs in output
        # -UT = use lowercase filtering of fasta
        # -R = report log info
        # -d = database
        # -i = query file (in file)

        #fasta = os.path.join(the_path, "scaffolds.trim.fasta.split.fasta")

        cmd = "%s -l -o %s -q %s -d %s > %s" % (blast_cmd, the_path, fasta, config['ncbi_prescreen_db'], prescreen_log)
        # -l = don't localize this one
        # defaults to -perc_identity 90 using -task megablast
        std_out, std_err, exit_code = run_cmd(cmd, log)


        blast_file = glob.glob(os.path.join(the_path, "megablast.*.parsed"))[-1]
        hit_cnt = 0

        fhw = open(out_file, "w")

        if blast_file:

            fh = open(blast_file, "r")
            for line in fh:
                if line.startswith("#"):
                    continue

                line = line.strip()

                # is it a good hit?
                arr = line.split()
                query_len = 0
                if len(arr) > 7:
                    hit_bases = int(arr[4]) # = 4
                    query_len = int(arr[8]) # = 8

                if query_len > 0:

                    hit_pct = hit_bases / float(query_len)
                    if hit_pct > 0.75:
                        log.info("- hit to %s.  hit = %s, query_len = %s, pct = %s", arr[1], hit_bases, query_len, format_val_pct(hit_pct))
                        fhw.write(line + "\n")
                        hit_cnt += 1

            fh.close()

        hit_status = msg_fail
        if hit_cnt == 0:
            hit_status = msg_ok
            fhw.write("# No contaminants found\n")

        fhw.close()


        log.info("- hit_cnt: %s  %s", hit_cnt, hit_status)


    if os.path.isfile(out_file):

        file_key = "%s_ncbi_prescreen_%s" % (mode, os.path.basename(out_file))
        log.info("- saving file: %s", file_key)
        append_rqc_file(rqc_file_log, file_key, out_file)
        # should process and save results in rqc-stats.txt?  No.  We'll never find contaminates in our data.

        prescreen_cnt = 0
        fh = open(out_file, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            prescreen_cnt += 1


        fh.close()


        append_rqc_stats(rqc_stats_log, "%s_ncbi_prescreen_cnt" % mode, prescreen_cnt)

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status



'''
reporting % of reads assembled in assembly (quick)
- for coverage for GOLD
- needs unzipped fastq
- for screened, compare the summary contigs to the contigs in the decontam fasta
- if exists for unscreened then symlink for screened

'''
def do_pct_reads_asm(my_output_path, fasta_type, mode):
    log.info("do_pct_reads_asm: %s", mode)

    step_name = "pct_reads_asm"
    if mode:
        step_name += ":%s" % mode

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(PCT_READS_ASM_PATH, my_output_path)


    fasta = get_file(fasta_type, file_dict, output_path, log)
    ref_fasta = get_file("scaffolds_trim", file_dict, output_path, log) # use this for mapping, not the decontam

    fastq_type = "subsampled_fastq"
    fastq = get_file(fastq_type, file_dict, output_path, log)

    summary_file = os.path.join(the_path, "bbmap.stats.txt")


    if os.path.isfile(summary_file):
        log.info("- skipping %s, found file: %s", step_name, summary_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        bbmap_log = os.path.join(the_path, "bbmap.log")
        cmd = "#%s;bbmap.sh ref=%s in=%s nodisk=t ow=t machineout=t statsfile=%s > %s 2>&1" % (config['bbtools_module'], ref_fasta, fastq, summary_file, bbmap_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(summary_file):

        # save the info
        file_key = "%s_pct_reads_asm_%s" % (mode, os.path.basename(summary_file))
        log.info("- saving file: %s", file_key)
        append_rqc_file(rqc_file_log, file_key, summary_file)

        fh = open(summary_file, "r")

        r1_map = -1 # number of reads mapped to read 1
        r2_map = -1 # number of reads mapped to read 2
        r_ttl = 0 # total reads

        for line in fh:
            if line.startswith("R1_Mapped_Reads"):
                r1_map = int(line.split("=")[-1])

            if line.startswith("R2_Mapped_Reads"):
                r2_map = int(line.split("=")[-1])

            if line.startswith("Reads_Used"):
                r_ttl = int(line.split("=")[-1])

        fh.close()

        # pct = r1_map / total reads (if single stranded)
        pct = 0.0
        if r1_map > -1:
            log.info("- r1_map reads: %s", r1_map)
            pct = r1_map / float(r_ttl)

        # pct = (r1_map + r2_map)/ total reads (if paired, should always be paired)
        if r2_map > -1:
            log.info("- r2_map reads: %s", r2_map)
            pct = (r1_map + r2_map) / float(r_ttl)

        log.info("- total reads: %s", r_ttl)


        pct = pct * 100.0

        my_key = "%s_pct_aligned" % (mode)
        if mode == "screen":
            my_key = "%s_pct_aligned_decontam" % (mode)


        append_rqc_stats(rqc_stats_log, my_key, pct)
        log.info("- pct aligned: %s", "{:.2f}".format(float(pct)))



        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
checkM - replace older SCG script
 - uses a list of genes based on the taxonomoy
 - bacteria = 107, archaea = 116, both = 49
 - lwf = lineage workflow - tries to find best spot in a taxonomy tree to decide what genes to use (slower)

15 minutes
jgi-rqc-pipeline/tools/checkm.py -f (assembly) -o (outdir) -r d-bac -p -c
 -f = assembly to be evaluated
'''
def do_checkm(qc_path, fasta_type, mode):
    log.info("do_checkm")

    step_name = "checkm"
    if mode:
        step_name += ":%s" % mode

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(CHECKM_PATH, qc_path)
    fasta = get_file(fasta_type, file_dict, output_path, log)


    report_file = os.path.join(the_path, "bac", "report.txt") # always created
    missing_img = os.path.realpath(os.path.join(my_path, "../sag/templates/chart_not_avail.png"))


    if os.path.isfile(report_file):
        log.info("- skipping %s, found file: %s", step_name, report_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        checkm_log = os.path.join(the_path, "checkm.log")
        checkm_wrapper = config['checkm_wrapper']

        #jgi-rqc-pipeline repo: tools/checkm_helper.py -i FASTA -o JOBDIR
        checkm_wrapper = os.path.realpath(checkm_wrapper.replace("[my_path]", my_path))
        cmd = "%s -i %s -o %s > %s 2>&1" % (checkm_wrapper, fasta, the_path, checkm_log)
        # using qc_path, checkm_wrapper creates the checkm folder
        # checkm_helper runs all 3 models concurrently each with 8 threads

        std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(report_file):

        # need to save bac, arc, lwf
        # save summary.txt, contig_hits.txt, report.txt
        # need gene list for reporting
        for mtype in ['bac', 'arc', 'lwf']: # model types
            for f in ['report.txt', 'contig_hits.txt', 'gene_contigs.txt', 'summary.txt']:

                save_file = os.path.join(the_path, mtype, f)
                if os.path.isfile(save_file):
                    file_key = "%s_checkm_%s_%s" % (mode, mtype, f)

                    log.info("- saving file: %s", file_key)
                    append_rqc_file(rqc_file_log, file_key, save_file)

            # expect 3:
            # - checkm_chart_d-*.png = all counts for all genes
            # - checkm_chart_d-*_nsc.png = everything thats not single copy
            # - checkm_histogram_d-*.png = histogram count for each gene
            #png_list = glob.glob(os.path.join(the_path, mtype, "*.png"))
            png_list = ['checkm_chart_d-%s.png' % mtype, 'checkm_chart_d-%s_nsc.png' % mtype, 'checkm_histogram_d-%s.png' % mtype]
            for png in png_list:

                if mtype == "lwf":
                    png = png.replace("_d-", "_")

                png = os.path.join(the_path, mtype, png)
                file_key = os.path.basename(png).replace(".png", "_image")

                if not os.path.isfile(png):
                    log.info("- warning, no file created for %s, using missing image  %s", png, msg_warn)
                    cmd = "cp %s %s" % (missing_img, png)
                    std_out, std_err, exit_code = run_cmd(cmd, log)

                    #png = missing_img

                file_key = "%s_%s" % (mode, file_key)
                log.info("- saving file: %s", file_key)
                append_rqc_file(rqc_file_log, file_key, png)


            # summary from report for histogram - want to get number of 0 counts, 1 counts, 2+ counts
            gene_hist = collections.Counter()

            line_cnt = 0
            report_file = os.path.join(the_path, mtype, "report.txt")
            if os.path.isfile(report_file):
                fh = open(report_file, "r")
                for line in fh:
                    line_cnt += 1

                    arr = line.split()
                    for a in arr:
                        if a.startswith("/"):
                            continue
                        if a.startswith("Filename"):
                            continue

                        if line_cnt == 1:
                            pass
                        if line_cnt == 2:
                            gene_hist[str(a)] += 1

                fh.close()



            hit_0 = 0
            hit_1 = 0
            hit_2 = 0 # 2 or more hits

            for g in gene_hist:

                if g == "0":
                    hit_0 = gene_hist[g]
                elif g == "1":
                    hit_1 = gene_hist[g]
                else:
                    hit_2 += gene_hist[g]


            log.info("-- model: %s - 0 hits: %s, 1 hit: %s, 2+ hits: %s", mtype, hit_0, hit_1, hit_2)

            append_rqc_stats(rqc_stats_log, "%s_checkm_%s_hit_0" % (mode, mtype), hit_0)
            append_rqc_stats(rqc_stats_log, "%s_checkm_%s_hit_1" % (mode, mtype), hit_1)
            append_rqc_stats(rqc_stats_log, "%s_checkm_%s_hit_2" % (mode, mtype), hit_2)


            # save summary stats
            summary_file = os.path.join(the_path, mtype, "summary.txt")

            # RQCSUPPORT-1112: create dummy file full of 0's
            if not os.path.isfile(summary_file):
                create_empty_checkm_summary(summary_file)



            if os.path.isfile(summary_file):
                fh = open(summary_file, "r")
                line1 = ""
                line2 = ""
                for line in fh:

                    if line.startswith("#Filename"):
                        line1 = line.strip()
                    else:
                        line2 = line.strip()

                fh.close()

                arr1 = line1.split()
                arr2 = line2.split()

                cnt = 0
                for a in arr1:

                    if a != "#Filename":
                        my_key = "%s_checkm_%s_%s" % (mode, mtype, a)
                        log.info("-- %s:  %s = %s", mtype, my_key, arr2[cnt])
                        append_rqc_stats(rqc_stats_log, my_key, arr2[cnt])

                        # save as percents for pdf report, web report
                        if a in ("EstGenomeComSimple", "EstGenomeComNormalized"):
                            pct_key = "%s_checkm_%s_%s_pct" % (mode, mtype, a)

                            val = format_val_pct(arr2[cnt])
                            append_rqc_stats(rqc_stats_log, pct_key, val)


                    cnt += 1
            else:
                log.error("- warning, no summary file for %s  %s", mtype, msg_fail)
                sys.exit(8)




        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Tetramer calc
- create tetramer kmers: KmerFrequencies.jar
-- replaced with: BBTools.TetramerFreq.sh in=CONTIG_FASTA out=TETRAMER_FREQ window=2000 (1 sec)
- create tetramer plot: showKmerBin.R (6 sec)
Konstantinos Mavrommatis, Dec 2011

'''
def do_tetramer(my_output_path, fasta_type, mode):
    log.info("do_tetramer: %s", mode)

    step_name = "tetramer"
    if mode:
        step_name += ":%s" % mode

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)



    the_path = get_the_path(TETRAMER_PATH, my_output_path)


    tetramer_jpg = os.path.join(the_path, "tetramer.jpg")
    fasta = get_file(fasta_type, file_dict, output_path, log)



    if os.path.isfile(tetramer_jpg):
        log.info("- skipping %s, found file: %s", step_name, tetramer_jpg)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:


        # create kmer frequencies for each 2000bp window shifted by 500bp, kmer size = 4bp (256 total kmers)
        kmer_out = os.path.join(the_path, "sag.%s.tet.kmers" % mode)
        kmer_log = os.path.join(the_path, "kmer_freq.log")

        cmd = "#%s;tetramerfreq.sh in=%s out=%s %s > %s 2>&1" % (config['bbtools_module'], fasta, kmer_out, config['tetramer_config'], kmer_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # kmer_out needs at least 3 lines to run show_kmer_bin R script (3 + 1 header)
        line_cnt = 0
        fh = open(kmer_out, "r")
        for _ in fh:
            line_cnt += 1
        fh.close()

        # if less than 4 lines then repeat last line, this makes for a dull tetramer plot but it won't exit with an error
        if line_cnt < 4:
            fh = open(kmer_out, "a")
            for line in range(4 - line_cnt):
                fh.write(line)
            fh.close()


        tet_out = os.path.join(the_path, "tetramer") # automatically creates tetramer.jpg
        tet_log = os.path.join(the_path, "show_kmer_bin.log")
        show_kmer_bin = config['show_kmer_bin_r']
        show_kmer_bin = os.path.realpath(show_kmer_bin.replace("[my_path]", my_path))
        cmd = "#%s;Rscript --vanilla %s --input %s --output %s %s > %s 2>&1" % (config['r_module'], show_kmer_bin, kmer_out, tet_out, config['tetramer_r_config'], tet_log)
        # label was "iso - PC1 vs PC2
        # needs doturl, jgpurl - creates html version too but we ignore that
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(tetramer_jpg):

        f_key = "%s_tetramer_jpg" % (mode)
        append_rqc_file(rqc_file_log, f_key, tetramer_jpg)
        log.info("- saving file: %s", f_key)
        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status

'''
Antifam - run prodigal and hmmer to look for broken genes
1 minute
'''
def do_antifam(my_output_path, fasta_type, mode):
    log.info("do_antifam: %s", mode)

    step_name = "antifam"
    if mode:
        step_name += ":%s" % mode

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(ANTIFAM_PATH, my_output_path)

    antifam_report = os.path.join(the_path, "antifam.txt")

    fasta = get_file(fasta_type, file_dict, output_path, log)

    if os.path.isfile(antifam_report):
        log.info("- skipping %s, found file: %s", step_name, antifam_report)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        # min of 20000bp for prodigal
        asm_stats, asm_tsv = make_asm_stats(fasta, "tmp", the_path, rqc_file_log, log)

        # parse asm_tsv to get contigs and bases
        base_cnt, contig_cnt = get_base_contig_cnt(asm_tsv)
        min_base_cnt = int(config['prodigal_base_count'])
        if base_cnt < min_base_cnt:
            log.info("- base_cnt: %s < %s, cannot run antifam process  %s", base_cnt, min_base_cnt, msg_fail)
            fh = open(antifam_report, "w")
            fh.write("# cannot run prodigal\n")
            fh.write("# assembly base count: %s\n" % base_cnt)
            fh.write("# minimum prodigal base count: %s\n" % min_base_cnt)
            fh.close()

        else:

            # prodigal outputs
            protein_fa = os.path.join(the_path, "gene_p.fa")
            nuc_fa = os.path.join(the_path, "gene_n.fa")
            prodigal_log = os.path.join(the_path, "prodigal.log")

            cmd = "#%s;prodigal -i %s -a %s -d %s -o %s" % (config['prodigal_module_antifam'], fasta, protein_fa, nuc_fa, prodigal_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            # hmmsearch: Search a protein profile HMM against a protein sequence database
            hmmer_log = os.path.join(the_path, "hmmsearch.log")
            cmd = "#%s;hmmsearch --cpu 16 --tblout %s %s %s > %s" % (config['hmmer_module'], antifam_report, config['antifam_db'], protein_fa, hmmer_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)
            # --tblout = parsable table of per-sequence hits to file <f>
            # E-value - smaller = better hit
            # score = bitscore
            # Bias = only matters when its close to the score field

    if os.path.isfile(antifam_report):

        f_key = "antifam_report"
        append_rqc_file(rqc_file_log, f_key, antifam_report)
        log.info("- saving file: %s", f_key)

        #append_flow(flow_file, "polish", "", "antifam", "Antifam", "Antifam (prodigal 2.6.3, hmmer 3.1b2)")

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Use checkm, barrnap and tRNAscan-SE to determine the assembly quality
for papers (SIGS - standards in genomic sciences)
'''
def do_qual(qc_path, fasta_type, mode):
    log.info("do_qual: %s", mode)

    step_name = "qual"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(QUAL_PATH, qc_path)

    quality_report = os.path.join(the_path, "quality.txt")

    fasta = get_file(fasta_type, file_dict, output_path, log)

    if os.path.isfile(quality_report):
        log.info("- skipping %s, found file: %s", step_name, quality_report)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:
        
        qual_cmd = os.path.realpath(config['qual_cmd'].replace("[my_path]", my_path))

        qual2_log = os.path.join(the_path, "qual2.log")
        cmd = "%s -f %s -o %s > %s 2>&1" % (qual_cmd, fasta, the_path, qual2_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        
    if os.path.isfile(quality_report):

        fh = open(quality_report, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue
            arr = line.split("|")
            if len(arr) == 2:
                my_key = "%s_qual_%s" % (mode, arr[0])
                val = arr[1]
                append_rqc_stats(rqc_stats_log, my_key, val)
                if arr[0] == "quality":
                    log.info("-- assembly quality: %s", arr[1])
        fh.close()
        
        

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Create a report for each contig with (SEQQC-10301)
Contig| clean or ProDege or ACDC or CrossBlock| contaminating libraries
- get contigs-[lib_name].txt from sag_decontam
- include in JAT template
'''
def do_contig_report(my_output_path, library_name):
    log.info("do_contig_report")

    step_name = "contig_report"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(CONTIG_REPORT_PATH, my_output_path)

    contig_report = os.path.join(the_path, "contig_report.txt")

    if 1==2 and os.path.isfile(contig_report):
        log.info("- skipping contig report step")
    else:

        contig_dict = {} # key = contig name, val = Prodege or Crossblock

        # get crossblock chaff contigs
        crossblock_chaff_file = get_file("pool_decontam_chaff", file_dict, output_path, log)

        if crossblock_chaff_file and os.path.isfile(crossblock_chaff_file):
            fh = open(crossblock_chaff_file, "r")
            contig_cnt = 0
            for line in fh:
                line = line.strip()
                if line.startswith(">"):
                    header = line[1:]
                    contig_dict[header] = "CrossBlock"
                    contig_cnt += 1
            fh.close()

            log.info("- Crossblock chaff contig count: %s", contig_cnt)

        else:
            log.info("- no Crossblock chaff file  %s", msg_warn)

        # get prodege chaff contigs
        prodege_chaff_file = get_file("prodege_chaff", file_dict, output_path, log)

        if not prodege_chaff_file:
            prodege_chaff_file = os.path.join(output_path, DECONTAM_PATH, "sag_decontam/sag_decontam_output_contam.fna")


        if prodege_chaff_file and os.path.isfile(prodege_chaff_file):
            contig_cnt = 0
            fh = open(prodege_chaff_file, "r")
            for line in fh:
                line = line.strip()
                if line.startswith(">"):
                    header = line[1:]
                    contig_dict[header] = "ProDeGe"
                    contig_cnt += 1
            fh.close()

            log.info("- Prodege chaff contig count: %s", contig_cnt)

        else:
            log.info("- no Prodege chaff file  %s", msg_warn)

        # get acdc chaff contigs
        acdc_chaff_file = get_file("acdc_chaff", file_dict, output_path, log)
        if acdc_chaff_file and os.path.isfile(acdc_chaff_file):
            contig_cnt = 0
            fh = open(acdc_chaff_file, "r")
            for line in fh:
                line = line.strip()
                if line.startswith(">"):
                    header = line[1:]
                    contig_dict[header] = "ACDC"
                    contig_cnt += 1
            fh.close()

            log.info("- ACDC chaff contig count: %s", contig_cnt)

        else:
            log.info("- no ACDC chaff file  %s", msg_warn)            


        # contig report from SAG Decontam
        contig_file = get_file("contig_report", file_dict, output_path, log)
        if not contig_file:
            contig_cnt = 0
            # need to make our own, since we didn't run through SAG Decontam we will just read the headers from the trimmed assembly (raw)
            # - just report what Prodege removed
            # - ATTTZ - prodege did not run because not enough bp/contigs - report should show all contigs as clean since nothing removed them

            fasta = get_file("scaffolds_trim", file_dict, output_path, log)
            fh = open(fasta, "r")

            contig_file = os.path.join(the_path, "contigs-%s.txt" % (library_name))
            fhw = open(contig_file, "w")
            fhw.write("#Contig|Contaminating Libraries (none - not run through Crossblock)\n")
            for line in fh:
                if line.startswith(">"):
                    header = line.strip()[1:] # remove starting >
                    fhw.write("%s|\n" % header)
                    contig_cnt += 1
            fh.close()
            fhw.close()

            log.info("- created contig_report: %s contigs", contig_cnt)



        if os.path.isfile(contig_file):

            fh = open(contig_file, "r")
            fhw = open(contig_report, "w")
            fhw.write("#Contig Name,Reason for Removal,Contaminating Library/16s Similarity/Reads Mapping to Contig\n")
            contig_cnt = 0
            for line in fh:
                line = line.strip()
                if line.startswith("#"):
                    continue

                arr = line.split("|")
                chaff_status = "retained"
                if arr[0] in contig_dict:
                    chaff_status = contig_dict[arr[0]]

                lib_list = arr[1].split(",")
                contam_buffer = ""
                for contam_lib in lib_list:
                    arr2 = contam_lib.split("/") # lib, 16s vsearch, 16s mothur, reads mapping, reads pct, bases mapping, bases pct
                    if len(arr2) > 5:

                        read_cnt = int(arr2[3])
                        if read_cnt > 20: # 2017-01-04 - agreed at our SAG meeting by Tanja & Rex
                            contam_buffer += "%s/%s/%s " % (arr2[0], arr2[1], arr2[3]) # lib, 16s vsearch, reads mapping


                output_buffer = "%s,%s,%s\n" % (arr[0], chaff_status, contam_buffer)
                fhw.write(output_buffer)
                contig_cnt += 1
            fh.close()
            fhw.close()


            append_rqc_file(rqc_file_log, "contig_report", contig_report)

            log.info("- wrote %s contigs to %s", contig_cnt, contig_report)
            status = "%s complete" % step_name

        else:
            log.info("- Error - contig report does not exist: %s  %s", contig_file, msg_fail)
            status = "%s failed" % step_name

    checkpoint_step(status_log, status)

    return status


'''
create a tarball containing
- crossblock_chaff.fasta
- crossblock_chaff.txt
- prodege_chaff.fasta
- prodege_chaff.txt

'''
def make_chaff_tarball(my_output_path, library_name, output_path, mode):
    log.info("make_chaff_tarball")


    step_name = "chaff_tarball"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    CHAFF_PATH = "chaff"

    the_path = get_the_path(CHAFF_PATH, my_output_path)
    tarball = os.path.join(the_path, "%s.chaff.contigs.tar.gz" % library_name)

    # always re-create the tarball
    if os.path.isfile(tarball) and 1==2:
        log.info("- skipping tarball creation, found tarball already")
    else:

        # only include these if its pool-decontam
        if mode == "pool-decontam":
            # if file doesn't exist, create an empty one
            crossblock_chaff_org = get_file("pool_decontam_chaff", file_dict, output_path, log)
            crossblock_chaff = os.path.join(the_path, "crossblock_chaff.fasta")

            if crossblock_chaff_org and os.path.isfile(crossblock_chaff_org):
                cmd = "cp %s %s" % (crossblock_chaff_org, crossblock_chaff)
                std_out, std_err, exit_code = run_cmd(cmd, log)


            else:
                log.info("- warning: no chaff file for crossblock!  %s", msg_warn)
                cmd = "touch %s" % crossblock_chaff
                std_out, std_err, exit_code = run_cmd(cmd, log)

            asm_stats, asm_tsv = make_asm_stats(crossblock_chaff, "crossblock_chaff", the_path, rqc_file_log, log)

            if os.path.getsize(crossblock_chaff) == 0:
                fh = open(crossblock_chaff, "a")
                fh.write("\nNote: BBTools Crossblock did not remove any contigs.\n")
                fh.close()

        if acdc_flag:
            acdc_chaff_org = get_file("acdc_chaff", file_dict, output_path, log)
            acdc_chaff = os.path.join(the_path, "acdc_chaff.fasta")
    
            if acdc_chaff_org and os.path.isfile(acdc_chaff_org):
                cmd = "cp %s %s" % (acdc_chaff_org, acdc_chaff)
                std_out, std_err, exit_code = run_cmd(cmd, log)
    
            else:
                log.info("- warning: no chaff file for ACDC!  %s", msg_warn)
                cmd = "touch %s" % acdc_chaff
                std_out, std_err, exit_code = run_cmd(cmd, log)
    
    
            asm_stats, asm_tsv = make_asm_stats(acdc_chaff, "acdc_chaff", the_path, rqc_file_log, log)
    
            # remove tsv from tarballs
            cmd = "cd %s;rm *.tsv" % (the_path)
            std_out, std_err, exit_code = run_cmd(cmd, log)
    
            if os.path.getsize(acdc_chaff) == 0:
                fh = open(acdc_chaff, "a")
                fh.write("\nNote: ACDC did not remove any contigs.\n")
                fh.close()
    
        
        else:
            prodege_chaff_org = get_file("prodege_chaff", file_dict, output_path, log)
            prodege_chaff = os.path.join(the_path, "prodege_chaff.fasta")
    
            if prodege_chaff_org and os.path.isfile(prodege_chaff_org):
                cmd = "cp %s %s" % (prodege_chaff_org, prodege_chaff)
                std_out, std_err, exit_code = run_cmd(cmd, log)
    
            else:
                log.info("- warning: no chaff file for prodege!  %s", msg_warn)
                cmd = "touch %s" % prodege_chaff
                std_out, std_err, exit_code = run_cmd(cmd, log)
    
    
            asm_stats, asm_tsv = make_asm_stats(prodege_chaff, "prodege_chaff", the_path, rqc_file_log, log)
    
            # remove tsv from tarballs
            cmd = "cd %s;rm *.tsv" % (the_path)
            std_out, std_err, exit_code = run_cmd(cmd, log)
    
            if os.path.getsize(prodege_chaff) == 0:
                fh = open(prodege_chaff, "a")
                fh.write("\nNote: ProDeGe did not remove any contigs.\n")
                fh.close()
    




        cmd = "cd %s;tar -czaf %s *" % (the_path, tarball)
        # c = create, a = auto compress based on extension (none), f = file
        std_out, std_err, exit_code = run_cmd(cmd, log)
        # tar -tvf lists files in tarball


    if os.path.isfile(tarball):
        save_file(tarball, "chaff_tarball", file_dict, output_path)
        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


## ~~~~~~~~~~~~~~~~~~~
## PDF, TXT reporting

'''
Use sag-screen.tex or sag-unscreen.tex or sag-raw.tex
- images in the pdf report can only have one "." in the name, e.g. my_thing.fa.chart.png won't work.  Need to rename the file
- writes to (mode)/report
- also does the *metadata.json and *txt report
* if re-run change rqc-files.txt* & rqc-stats.txt* to *.tmp
'''
def do_pdf_report(library_name, fasta_type, org_name, tax_id, mode):
    log.info("do_pdf_report: %s", mode)

    step_name = "pdf_report"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)



    the_path = get_the_path(REPORT_PATH, output_path)

    # output file names
    report_type = mode
    if mode == "unscreen":
        report_type = "raw"
    pdf_report = os.path.join(the_path, "sag-%s-%s.pdf" % (library_name, report_type))
    latex_report = pdf_report.replace(".pdf", ".tex")
    txt_report = pdf_report.replace(".pdf", ".txt")
    # jat metadata.json in output_path because files are relative to output_path
    jat_metadata = os.path.join(output_path, "%s.metadata.json" % (report_type))

    if os.path.isfile(pdf_report) and 1==2:
        log.info("- skipping %s, found file: %s", step_name, pdf_report)

    elif check_skip(step_name, "run_me", config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:


        # setup merge dictionary to use for the merging
        merge_dict = get_library_info(library_name, log)

        if not 'seq_proj_id' in merge_dict:
            log.error("- no seq_proj_id for %s!  %s", library_name, msg_fail)
            status = "%s failed" % step_name
            return status



        # replace with passed in organism name
        if org_name:
            merge_dict['organism_name'] = org_name
        
        if tax_id == 0:
            tax_id = merge_dict['gold_tax_id']
            merge_dict['organism_name'] = merge_dict['gold_organism_name']
            
            # use gold by default, if not exists then use ncbi
            if not merge_dict['organism_name']:
                tax_id = merge_dict['ncbi_tax_id']
                merge_dict['organism_name'] = merge_dict['ncbi_organism_name']
                
        
        #if tax_id:
        #    tax_dict = get_taxonomy_by_id(tax_id, log)
        #    for k in tax_dict:
        #        merge_dict[k] = tax_dict[k]
        #    merge_dict['organism_name'] = tax_dict['tax_species']

        
        merge_dict['jgi_logo'] = os.path.realpath(os.path.join(my_path, "../sag/templates", "jgi_logo.jpg"))
        merge_dict['prodege_version'] = config['prodege_version']
        merge_dict['library_name'] = library_name

        merge_dict['assembler_name'] = config['spades_module']
        if mode == "screen":
            merge_dict['assembler_name'] += " with auto decontamination (ProDeGe %s)" % (merge_dict['prodege_version'])
        merge_dict['assembler_params'] = config['spades_params']

        merge_dict['assembler_version'] = config['spades_module'].replace("spades/","")
        # change to rqc-stats.txt - spades_version option because it might be different (later in this function)

        merge_dict['input_fastq_full_path'] = get_file("input_fastq", file_dict, output_path, log)
        merge_dict['input_fastq'] = os.path.basename(merge_dict['input_fastq_full_path'])



        merge_dict['report_date'] = time.strftime("%x") # %x = date in local format

        merge_dict['subsample_reads'] = config['subsample']
        merge_dict['normalize_params'] = config['normalize_params']
        merge_dict['trim_map_params'] = config['trim_map_params']
        merge_dict['trim_params'] = config['trim_params']




        tapid = config['screened_analysis_project_type_id']
        tatid = config['screened_analysis_task_type_id']
        if mode == "unscreen":
            #tapid = config['unscreened_analysis_project_type_id']
            #tatid = config['unscreened_analysis_task_type_id']
            tapid = config['raw_analysis_project_type_id']
            tatid = config['raw_analysis_task_type_id']

        merge_dict['analysis_project_id'], merge_dict['analysis_task_id'] = get_analysis_project_id(merge_dict['seq_proj_id'], tapid, tatid, the_path, log)



        # read in stats & files into merge dict
        for f in [rqc_stats_log, rqc_file_log]:

            #if not os.path.isfile(f):
            #    f = f.replace(".tmp", ".txt")

            log.info("- importing: %s", f)

            fh = open(f, "r")

            for line in fh:
                #print line
                if line.startswith("#"):
                    continue
                arr = line.split("=")

                if len(arr) > 1:
                    my_key = str(arr[0]).lower().strip()
                    my_val = str(arr[1]).strip()
                    merge_dict[my_key] = my_val
                    #print my_key, my_val, merge_dict[my_key]
            fh.close()


        # if no scaffolds then write a text file
        fail_flag = 0
        check_key = "%s_scaf_bp" % fasta_type
        check_val = 0
        if check_key in merge_dict:
            check_val = int(merge_dict[check_key])
        else:
            fail_flag = 1

        if check_val < 5:
            fail_flag = 1

        log.info("- check key %s = %s > 5", check_key, check_val)
        if fail_flag == 1:

            fh = open(txt_report, "w")
            fh.write("Report date: %s\n" % merge_dict['report_date'])
            fh.write("SAG decontamination did not produce any clean contigs/scaffolds for library %s.\n" % (merge_dict['library_name']))
            fh.close()

            append_rqc_file(rqc_file_log, os.path.basename(txt_report), txt_report)
            log.error("- %s = %s, not creating reports  %s", check_key, check_val, msg_fail)
            status = "%s skipped" % step_name
            checkpoint_step(status_log, status)

            return status

        # picks up the input fastq with full path sometimes
        if str(merge_dict['input_fastq']).startswith("/"):
            merge_dict['input_fastq'] = os.path.basename(merge_dict['input_fastq'])


        # text report ...
        #Library_Name	Number_of_Reads	Run_Type Read_Type	File_Name	Platform	Model
        merge_dict['raw_data'] = "%s\t%s\t%s\tIllumina Std PE (cassava 1.8)\t%s\tIllumina\t%s" % (merge_dict['library_name'], merge_dict['input_fastq_read_count'],
                                                                                                  merge_dict['run_configuration'], merge_dict['input_fastq'],
                                                                                                  merge_dict['instrument_type'])




        # pull in stats for the assembly based on the fasta_type
        asm_keys = ["n_scaffolds", "n_contigs", "scaf_bp", "contig_bp", "scaf_n50", "scaf_l50", "ctg_n50", "ctg_l50", "scaf_max", "ctg_max", "scaf_n_gt50k", "scaf_pct_gt50k", "gap_pct",
                    "gc_avg", "gc_std"]
        for asm_stat in asm_keys:
            the_key = "%s_%s" % (fasta_type, asm_stat)

            if the_key in merge_dict:
                #print the_key, merge_dict[the_key]
                merge_dict[asm_stat] = merge_dict[the_key]

        #print merge_dict
        # 3. Read QC results - how much was removed in normalization, subsampling
        normalized_reads_removed = 0
        subsampled_reads_removed = 0
        normalized_reads_pct = 0.0
        subsampled_reads_pct = 0.0
        if int(merge_dict['input_fastq_read_count']) > 0:
            if 'normalized_fastq_read_count' in merge_dict:
                normalized_reads_removed = int(merge_dict['input_fastq_read_count']) - int(merge_dict['normalized_fastq_read_count'])
                normalized_reads_pct = 1 - int(merge_dict['normalized_fastq_read_count']) / float(merge_dict['input_fastq_read_count'])

            if 'subsampled_fastq_read_count' in merge_dict:
                subsampled_reads_removed = int(merge_dict['normalized_fastq_read_count']) - int(merge_dict['subsampled_fastq_read_count'])
                subsampled_reads_pct = 1 - int(merge_dict['subsampled_fastq_read_count']) / float(merge_dict['normalized_fastq_read_count'])

            merge_dict['total_reads_removed'] = normalized_reads_removed + subsampled_reads_removed
            merge_dict['total_reads_removed_pct'] = int(merge_dict['total_reads_removed']) / float(merge_dict['input_fastq_read_count'])

            merge_dict['normalized_fastq_read_rmv'] = normalized_reads_removed
            merge_dict['normalized_fastq_read_pct'] = normalized_reads_pct


            merge_dict['subsampled_fastq_read_rmv'] = subsampled_reads_removed
            merge_dict['subsampled_fastq_read_pct'] = subsampled_reads_pct

            merge_dict['total_read_count'] = int(merge_dict['subsampled_fastq_read_count'])
            merge_dict['total_reads_pct'] = 1 - merge_dict['total_reads_removed_pct']



            # Contaminate summary
            merge_dict['contam_summary_table'] = "\\"
            merge_dict['contam_id_table'] = "\\"
            if 'contam_summary.txt' in merge_dict:
                merge_dict['contam_summary_table'], merge_dict['contam_id_table'], merge_dict['contam_txt'] = contam_summary_pdf(merge_dict['contam_summary.txt'],
                                                                                                                                 merge_dict['input_fastq_read_count'], log)


        # green_genes 16s RNA - use ssu tophits
        merge_dict['green_genes_table'] = ""

        if mode == "unscreen":
            merge_dict['green_genes_table'] += green_genes_pdf(merge_dict.get('unscreen_ssuref_tax_silva.parsed.tophit'), log)
        else:
            merge_dict['green_genes_table'] += green_genes_pdf(merge_dict.get('screen_ssuref_tax_silva.parsed.tophit'), log)

        if merge_dict['green_genes_table'] == "":
            merge_dict['green_genes_table'] = "\\\\No hits to the reference.\\\\"


        # stats for txt file
        asm_stats_file = os.path.join(output_path, DECONTAM_PATH, "sag_decontam_output_clean.txt")
        if acdc_flag:
            asm_stats_file = os.path.join(output_path, ACDC_DECONTAM_PATH, "clean.txt")
            
        if decontam_fasta_type == "pool_decontam":
            asm_stats_file = os.path.join(output_path, "pool_asm/pool_decontam.txt")

        if mode == "unscreen":
            asm_stats_file = os.path.join(output_path, CONTIG_TRIM_PATH, "scaffolds.trim.txt")

        asm_stats_buffer = ""
        fh = open(asm_stats_file, "r")
        for line in fh:
            asm_stats_buffer += line
        fh.close()
        merge_dict['asm_stats_file'] = asm_stats_buffer


        # ncbi prescreen - text file only
        #unscreen_ncbi_prescreen_ncbi_screen.out
        ncbi_key = "screen_ncbi_prescreen_cnt"
        ncbi_file_key = "screen_ncbi_prescreen_ncbi_screen.out"
        if mode == "unscreen":
            ncbi_key = "unscreen_ncbi_prescreen_cnt"
            ncbi_file_key = "unscreen_ncbi_prescreen_ncbi_screen.out"


        merge_dict['ncbi_contamination'] = ncbi_prescreen_report(merge_dict[ncbi_file_key], merge_dict[ncbi_key], log)


        # replace spades version with what is in the rqc-stats.txt
        if 'spades_version' in merge_dict:
            merge_dict['assembler_name'] = merge_dict['spades_version'] # v3.7.1
            if mode == "screen":
                if acdc_flag:
                    merge_dict['assembler_name'] += " with auto decontamination (ACDC %s)" % (merge_dict['acdc_version'])

                else:
                    merge_dict['assembler_name'] += " with auto decontamination (ProDeGe %s)" % (merge_dict['prodege_version'])


        # checkm sometimes doesn't do the lwf (BCWAG)

        #print merge_dict
        # checkm doesn't always produce output
        for checkm_type in ['arc', 'bac', 'lwf']:
            checkm_key = "%s_checkm_%s_foundscgenes" % (mode, checkm_type)
            if checkm_key not in merge_dict:
                log.info("- checkm %s data not found, setting to N/A  %s", checkm_type, msg_warn)
                for checkm_key in ['checkm_'+checkm_type+'_foundscgenes', 'checkm_'+checkm_type+'_totalscgenes', 'checkm_'+checkm_type+'_estgenomecomsimple_pct',
                                   'checkm_'+checkm_type+'_estgenomecomnormalized_pct']:
                    merge_dict[mode + "_" + checkm_key] = "N/A"


                # image for charts not created
                if checkm_type == "lwf":
                    for checkm_key in ['checkm_chart_'+checkm_type+'_image', 'checkm_histogram_'+checkm_type+'_image']:
                        merge_dict[mode + "_" + checkm_key] = os.path.realpath(os.path.join(my_path, "../sag/templates/chart_not_avail.png"))
                else:
                    for checkm_key in ['checkm_chart_d-'+checkm_type+'_image', 'checkm_histogram_d-'+checkm_type+'_image']:
                        merge_dict[mode + "_" + checkm_key] = os.path.realpath(os.path.join(my_path, "../sag/templates/chart_not_avail.png"))


        checkm_key = "%s_checkm_lwf_foundscgenes" % (mode)
        if checkm_key not in merge_dict:
            log.info("- checkm lwf not found, setting to N/A  %s", msg_warn)
            for checkm_key in ['checkm_lwf_foundscgenes', 'checkm_lwf_totalscgenes', 'checkm_lwf_estgenomecomsimple_pct', 'checkm_lwf_estgenomecomnormalized_pct']:
                merge_dict[mode + "_" + checkm_key] = "N/A"

            # images which shouldn't be there
            # - create image_not_available by going to 1001 Free Fonts and right clicking on a font image and saving it to my computer
            for checkm_key in ['checkm_chart_lwf_image', 'checkm_histogram_lwf_image']:
                merge_dict[mode + "_" + checkm_key] = os.path.realpath(os.path.join(my_path, "../sag/templates/image_not_available.png"))


        # debug2
        #for k in sorted(merge_dict.iterkeys()):
        #    print "* '%s' = '%s'" % (k, merge_dict[k])


        ## PDF & TXT report, metadata.json
        # original template files
        my_template = "sag-%s.tex" % report_type
        my_txt_template = "sag-%s.txt" % report_type
        my_json_template = "%s.metadata.json" % report_type

        latex_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_template))
        txt_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_txt_template))
        json_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_json_template))



        # metadata.json for JAT
        # written to output_path, files are relative to output_path
        merge_dict['pdf_report'] = "report/%s" % os.path.basename(pdf_report)
        merge_dict['txt_report'] = "report/%s" % os.path.basename(txt_report)

        x = "%s/%s" % (SUBSAMPLE_PATH, os.path.basename(get_file("subsampled_fastq", file_dict, output_path, log)))
        merge_dict['subsample_file'] = x
        #print fasta_type, decontam_fasta_type, get_file(fasta_type, file_dict, output_path, log)

        # get the relative path, not full path
        unscreened_fasta = get_file(fasta_type, file_dict, output_path, log)
        unscreened_fasta = "%s/%s" % (unscreened_fasta.split("/")[-2], os.path.basename(unscreened_fasta))


        screened_fasta = get_file(fasta_type, file_dict, output_path, log)
        screened_fasta = "%s/%s" % (screened_fasta.split("/")[-2], os.path.basename(screened_fasta))

        merge_dict['unscreened_assembly_fasta'] = unscreened_fasta
        merge_dict['raw_assembly_fasta'] = unscreened_fasta # v 1.2

        merge_dict['screened_assembly_fasta'] = screened_fasta


        # screened chaff: chaff tarball w/ prodege & crossblock chaff + stats files
        merge_dict['chaff_fasta_json'] = "" # unscreened - was part of 1.1, could remove this after 1.2 is released for a while, also update unscreened json template to remove it
        if mode == "screen":
            merge_dict['chaff_tarball'] = os.path.basename(get_file("chaff_tarball", file_dict, output_path, log))
            merge_dict['chaff_tarball'] = os.path.join("screen/chaff", merge_dict['chaff_tarball'])

        # get relative path of contig report
        merge_dict['contig_report'] = merge_dict['contig_report'].replace(output_path, "")
        if str(merge_dict['contig_report']).startswith("/"):
            merge_dict['contig_report'] = merge_dict['contig_report'][1:]



        # some fields in JAT template are named to be consistant with older templates per Andrew 2016-06-07

        #merge_template(json_template_file, jat_metadata, merge_dict, log)
        # create new [at_id].raw/screen.metadata.json: RQCSUPPORT-1417
        jat_metadata = os.path.join(output_path, "%s.%s.metadata.json" % (merge_dict['analysis_task_id'], report_type))
        merge_template(json_template_file, jat_metadata, merge_dict, log)


        # symlink to old file (jat_metadata) - remove these lines 2018?
        jat_metadata_new = "%s.metadata.json" % report_type
        cmd = "cd %s;ln -s %s %s" % (output_path, os.path.basename(jat_metadata), jat_metadata_new)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        # formatting pct, int after metadata.json written ...

        # formatting integers to have ,'s 1000000 -> 1,000,000
        format_list_int = ["contig_bp", "scaf_bp", "scaf_max", "ctg_max", "input_fastq_base_count", "input_fastq_read_count", "normalized_fastq_base_count", "normalized_fastq_read_count",
                            "subsampled_fastq_read_count", "subsampled_fastq_base_count", "normalized_fastq_read_rmv", "subsampled_fastq_read_rmv", "total_read_count",
                            "total_reads_removed", "raw_reads_count", "sdm_raw_base_count"]

        # seq length = 1.065 Mb (or kB), does this need to be formappted this way?
        for k in format_list_int:

            if k in merge_dict:
                try:
                    # convert to mb or kb or gb or ... ?
                    merge_dict[k] = "{:,}".format(int(merge_dict[k]))
                except:
                    pass



        # 0.2555 -> 25.55%
        format_list_pct = ["scaf_pct_gt50k", "unscreen_pct_aligned", "unscreen_pct_aligned", "screen_pct_aligned_decontam", "screen_pct_aligned",
                            "normalized_fastq_read_pct", "subsampled_fastq_read_pct", "total_reads_pct", "total_reads_removed_pct"]

        for k in format_list_pct:

            if k in merge_dict:
                merge_dict[k] = format_val_pct(merge_dict[k])


        # txt report
        merge_template(txt_template_file, txt_report, merge_dict, log)


        # PDF/latex
        err_cnt = merge_template(latex_template_file, latex_report, merge_dict, log)
        if err_cnt > 0:
            log.error("- fatal error: missing keys for pdf report!  %s", msg_fail)
            sys.exit(8)

        # convert .tex to .pdf
        # - okay if non-zero exit code

        pdflatex_log = os.path.join(the_path, "pdflatex.log")

        cmd = "#texlive;pdflatex -interaction=nonstopmode -output-directory=%s %s > %s 2>&1" % (the_path, os.path.basename(latex_report), pdflatex_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)



    if os.path.isfile(pdf_report) and os.path.isfile(txt_report):

        append_rqc_file(rqc_file_log, os.path.basename(pdf_report), pdf_report)
        append_rqc_file(rqc_file_log, os.path.basename(txt_report), txt_report)

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name
        log.error("- failed to create %s  %s", pdf_report, msg_fail)

    checkpoint_step(status_log, status)


    return status



## ~~~~~~~~~~~~~~~~~~~~~
## report/merge helper functions



'''
Symlink files to a "final" directory for Kecia to use
- need to do a "cd final;" before ln in order to prevent using full path in case the parent path changes
'''
def do_final():

    log.info("do_final")

    # current folder/filename -> alias in final
    ln_dict = {
        "subsample/*.norm.subsample.fastq.gz" : ".",
        "report/sag-*-screen.txt" : "QD.screen.finalReport.txt",
        #"report/sag-*-unscreen.txt" : "QD.unscreen.finalReport.txt",
        #"report/sag-*-unscreen.pdf" : "QC.unscreen.finalReport.pdf",
        "report/sag-*-raw.txt" : "QD.raw.finalReport.txt",
        "report/sag-*-raw.pdf" : "QC.raw.finalReport.pdf",
        "report/sag-*-screen.pdf" : "QC.screen.finalReport.pdf",
        "trim/scaffolds.trim.fasta" : "scaffolds.2k.fasta",
        "trim/contigs.trim.fasta" : "contigs.2k.fasta",
        "prodege/sag_decontam/sag_decontam_output_clean.fna" : ".",
    }

    the_path = get_the_path("final", output_path)


    # use pool_decontam for unscreened assembly if pool/plate
    # - SEQQC-9399, raw = trim/*.fasta
    #if run_mode == "pool-decontam":
    #    ln_dict["pool_asm/pool_decontam.fasta"] = "lab_decontam.fasta"
    #    ln_dict.pop("trim/scaffolds.trim.fasta")
    #    ln_dict.pop("trim/contigs.trim.fasta")

    for i in ln_dict:

        f = os.path.join(output_path, i)

        g_list = glob.glob(f)

        for g in g_list:

            g = g.replace(output_path, "../")
            dest_file = os.path.join("./final", ln_dict[i])

            # remove old file (symlink_file)
            s_file = os.path.join(output_path, dest_file)
            if s_file.endswith("."):
                s_file = os.path.join(output_path, "final", os.path.basename(g))

            if os.path.isfile(s_file):
                # don't use os.path.realpath because it removes the source file
                # using "unlink" instead of running the rm command so that we only remove the symlink
                log.info("- unlinking %s", s_file)
                os.unlink(s_file)
            else:
                log.warn("- cannot find file to unlink: %s", s_file)


            cmd = "cd %s;ln -s %s %s" % (output_path, g, dest_file)
            std_out, std_err, exit_code = run_cmd(cmd, log)



## ~~~~~~~~~~~~~~~~~~~~~
## setup, tear-down


'''
Rename rqc-files, rqc-stats to *.tmp (only if run previously)
- anything else?
'''
def setup_run():
    log.info("setup_run")

    # rename rqc-files.txt & rqc-stats.txt to *.tmp
    txt_list = glob.glob(os.path.join(output_path, "rqc-*s.txt*"))
    for txt_file in txt_list:

        #tmp_file = txt_file.replace(".txt", ".tmp")
        tmp_file = txt_file
        if "-files." in tmp_file:
            tmp_file = os.path.join(output_path, "rqc-files.tmp")
        elif "-stats." in tmp_file:
            tmp_file = os.path.join(output_path, "rqc-stats.tmp")
        cmd = "mv %s %s" % (txt_file, tmp_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)



'''
Clean up
- gzip files in some directories

'''
def do_purge():
    log.info("do_purge")

    step_name = "purge"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    # gzip files > 1m only in certain folders
    #gzip_folder(os.path.join(output_path, ASM_PATH, "split_input"))
    #gzip_folder(os.path.join(output_path, screened_path))
    #gzip_folder(os.path.join(output_path, unscreened_path))


    status = "%s complete" % step_name

    checkpoint_step(status_log, status)

    return status

'''
gzip files > 1M in some folders
'''
def gzip_folder(my_folder):
    log.info("gzip_folder: %s", my_folder)

    if os.path.isdir(my_folder):

        cmd = "#pigz;cd %s;find . -size +1M ! -iname '*.parsed' | xargs pigz" % (my_folder)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    else:
        log.warn("- no such folder: %s  %s", my_folder, msg_warn)


## ~~~~~~~~~~~~~~~~~~~
## helper functions


'''
Call RQC's web service to return:
- previous run folder for "pool" (if success)
- # of libraries in this pool that are Single Cells
- fs location of cleaned fasta

https://rqc.jgi-psf.org/api/rqcws/sag_pool/BAGOX
'''
def get_pooled_status(library_name):
    log.info("get_pooled_status: %s", library_name)

    clean_fasta = None
    chaff_fasta = None
    pool_cnt = 0
    sag_fs_location = None # sag asm run folder
    sd_fs_location = None # sag decontam run folder
    rqc_sag_pool_api = "%s/%s" % ("api/rqcws/sag_pool", library_name)

    response = get_web_page(RQC_URL, rqc_sag_pool_api, log)

    if response:
        if 'pool_name' in response:

            clean_fasta = response['clean_fasta']
            if 'chaff_fasta' in response:
                chaff_fasta = response['chaff_fasta']
            sag_fs_location = response['sag_run_folder'] # first run of sag (sag --mode pool)
            pool_cnt = response['pool_cnt']
            if 'sag_fs' in response:
                sd_fs_location = response['sag_fs']

            if uname == "brycef":

                # t3 = scyrba.2, t4 = stew, t5 = ridge
                if library_name in ("BAGOX", "BAGOO"): # x x
                    sd_fs_location = "/global/projectb/scratch/brycef/sag/t5"
                    clean_fasta = os.path.join(sd_fs_location, "clean", "%s_clean.fasta" % library_name)
                    chaff_fasta = os.path.join(sd_fs_location, "clean", "%s_chaff.fasta" % library_name)

                if library_name in ("BHAHN", "BHAPB", "BHAOP", "BHAPH"): # x o x x
                    sd_fs_location = "/global/projectb/scratch/brycef/sag/t4"
                    clean_fasta = os.path.join(sd_fs_location, "clean", "%s_clean.fasta" % library_name)
                    chaff_fasta = os.path.join(sd_fs_location, "clean", "%s_chaff.fasta" % library_name)

                if library_name in ("BCWAY", "BCUZB", "BCWAG", "BCUYO", "BCUYP"): # x x x x x
                    sd_fs_location = "/global/projectb/scratch/brycef/sag/t3"
                    clean_fasta = os.path.join(sd_fs_location, "clean", "%s_clean.fasta" % library_name)
                    chaff_fasta = os.path.join(sd_fs_location, "clean", "%s_chaff.fasta" % library_name)

            log.info("- pool: %s, libraries: %s", response['pool_name'], pool_cnt)
            log.info("- sag_asm run folder: %s", sag_fs_location)
            log.info("- clean fasta: %s", clean_fasta)
            log.info("- chaff fasta: %s", chaff_fasta)
            log.info("- sag_decontam run folder: %s", sd_fs_location)


    # if there is a sag_asm run folder but no clean_fasta then exit
    # - happens sometimes with pooled name
    if pool_cnt > 2:
        if sag_fs_location and not clean_fasta:
            log.error("- cannot find clean fasta!  %s", msg_fail)
            sys.exit(2)
    else:
        log.info("- less than 3 libraries, ignoring missing sag_decontam run folder null and clean_fasta null check")

    return clean_fasta, chaff_fasta, sag_fs_location, sd_fs_location, pool_cnt


'''
Can be run once we have the cleaned fasta
- logging the status but not returning it
- sag_run_folder = pipeline run for SAG-ASM (spades & trimming)
- sd_fs_location - path to sag decontam folder to grab other files
'''
def setup_pool_decontam(decontam_fasta, chaff_fasta, sag_run_folder, sd_fs_location, library_name):
    log.info("setup_pool_decontam: %s, %s", decontam_fasta, sag_run_folder)

    step_name = "setup_pool_decontam"
    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    fail_flag = 0


    if not os.path.isfile(decontam_fasta):
        log.error("- decontam fasta does not exist: %s  %s", decontam_fasta, msg_fail)
        fail_flag += 1

    if not os.path.isdir(sag_run_folder):
        log.error("- sag-asm run folder does not exist: %s  %s", sag_run_folder, msg_fail)
        fail_flag += 1


    # fail me
    if fail_flag > 0:
        status = "%s failed" % step_name
        checkpoint_step(status_log, status)

        return "", "failed"

    # rsync the old folders to this new run folder
    folder_list = ['asm', 'contam', 'normalize', 'subsample', 'trim']
    for f in folder_list:
        old_f = os.path.join(sag_run_folder, f)

        #rsync -a /global/dna/shared/rqc/pipelines/sag_asm/archive/02/88/66/18/contam /global/projectb/scratch/brycef/sag/ATXXO
        if os.path.isdir(old_f):

            cmd = "rsync -a %s %s" % (old_f, output_path)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            log.error("- missing folder: %s  %s", old_f, msg_warn)


    # save a couple of old files, skipping the sag.cfg file
    file_list = ['file_list.txt', 'spades.txt']
    for f in file_list:
        old_f = os.path.join(sag_run_folder, f)
        new_f = os.path.join(output_path, f)

        if os.path.isfile(old_f):

            cmd = "cp %s %s" % (old_f, new_f)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            log.error("- file missing: %s  %s", old_f, msg_warn)

    if os.path.isdir(os.path.join(output_path, DECONTAM_PATH)):
        job_name = "sag_decontam"
        clean_fasta = os.path.join(output_path, DECONTAM_PATH, job_name, "%s_output_clean.fna" % job_name)
        if os.path.isfile(clean_fasta):
            new_fasta_type = "clean_scaffolds"
            save_file(clean_fasta, new_fasta_type, file_dict, output_path)


    # copy contents of these files w/o overwriting existing *.tmp files
    file_list = ['rqc-files.txt', 'rqc-stats.txt']
    for f in file_list:
        f_old = glob.glob(os.path.join(sag_run_folder, f+"*"))[0]
        if not f_old:
            f_old = f.replace(".txt", ".tmp")

        if os.path.isfile(f_old):
            log.info("- copying info from: %s", f_old)
            f_new = os.path.join(output_path, f.replace(".txt", ".tmp"))
            fha = open(f_new, "a")

            fh = open(f_old, "r")
            for line in fh:
                if f == "rqc-files.txt":

                    # need to change path of files to this one
                    arr = line.split("=")
                    if len(arr) > 1:
                        if arr[1].strip().startswith("/"):
                            arr2 = arr[1].split("/")

                            b = "%s = %s" % (arr[0], os.path.join(output_path, arr2[-2], arr2[-1]))
                            fha.write(b)
                else:
                    fha.write(line)
            fh.close()

            fha.close()
        else:
            log.error("- missing file: %s  %s", f_old, msg_warn)


    # copy the sag pool level decontam fasta to POOL_ASM_PATH
    the_path = get_the_path(POOL_ASM_PATH, output_path)
    asm_fasta = os.path.join(the_path, "pool_decontam.fasta") # unscreened

    cmd = "cp %s %s" % (decontam_fasta, asm_fasta)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    fasta_type = "pool_decontam"
    save_file(asm_fasta, fasta_type, file_dict, output_path)

    # bbstats for scaffolds
    asm_stats, asm_tsv = make_asm_stats(asm_fasta, fasta_type, the_path, rqc_file_log, log)
    save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

    # check if pool_decontam.fasta has any contigs
    base_cnt, contig_cnt = get_base_contig_cnt(asm_tsv)
    contig_status = msg_fail
    if contig_cnt > 0:
        contig_status = msg_ok

    log.info("- %s.fasta contigs: %s, bases: %s  %s", fasta_type, contig_cnt, base_cnt, contig_status)

    if contig_cnt == 0:
        status = "%s failed" % step_name
        checkpoint_step(status_log, status)

        return "", "failed"


    if chaff_fasta and os.path.isfile(chaff_fasta):
        new_chaff_fasta = os.path.join(the_path, "pool_decontam_chaff.fasta")
        cmd = "cp %s %s" % (chaff_fasta, new_chaff_fasta)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        chaff_fasta_type = "pool_decontam_chaff"
        save_file(new_chaff_fasta, chaff_fasta_type, file_dict, output_path)

        asm_stats, asm_tsv = make_asm_stats(new_chaff_fasta, chaff_fasta_type, the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, chaff_fasta_type, rqc_stats_log, log)

        base_cnt, contig_cnt = get_base_contig_cnt(asm_tsv)
        log.info("- %s.fasta contigs: %s, bases: %s", chaff_fasta_type, contig_cnt, base_cnt)


    # get contig report from SAG Decontam
    contig_report = os.path.join(sd_fs_location, "read_ppm", "contigs-%s.txt" % library_name)
    if os.path.isfile(contig_report):
        new_contig_report = os.path.join(the_path, os.path.basename(contig_report))
        cmd = "cp %s %s" % (contig_report, new_contig_report)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        save_file(new_contig_report, "contig_report", file_dict, output_path)

    else:
        log.info("- %s missing!  %s", contig_report, msg_warn)

    status = "%s complete" % step_name
    checkpoint_step(status_log, status)

    new_status = "decontamination" # ready for ProDeGe step

    return fasta_type, new_status


'''
Get the next thing to run
'''
def get_next_status(status):
    log.info("get_next_step: %s", status)

    next_status = None

    # this could be part of the config file
    status_dict = {
        # assembly
        "start" : "contam_id",
        "contam_id" : "normalize",
        "normalize" : "subsample",
        "subsample" : "assembly",
        "assembly" : "contig_trim",
        "contig_trim" : "decontamination",
        "decontamination" : "contig_gc_plot",

        # reporting
        "contig_gc_plot" : "gc_cov",
        "gc_cov" : "blast",
        "blast" : "blast_16s",
        "blast_16s" : "gc_hist",
        "gc_hist" : "ncbi_prescreen",
        "ncbi_prescreen" : "pct_reads_asm",
        "pct_reads_asm" : "checkm",
        "checkm" : "tetramer",

        "tetramer" : "antifam",
        "antifam" : "qual",
        "qual" : "contig_report",
        "contig_report" : "chaff_tarball",
        "chaff_tarball" : "pdf_report",

        # pdf, txt, metadata.json
        "pdf_report" : "purge",

        "purge" : "complete",

        "failed" : "failed",
        "complete" : "complete",
    }


    if status:

        if status.lower() != "complete":
            status = status.lower().replace("skipped", "").replace("complete", "").strip()

        if status.endswith("failed"):
            next_status = "failed"

        # gc_cov:screened -> gc_cov
        if ":" in status:
            status = status.split(":")[0]

        if status in status_dict:
            next_status = status_dict[status]

        else:
            if status.endswith("failed"):
                next_status = "failed"

            else:
                next_status = "failed: next step missing for '%s'" % (status)
                log.error("Cannot get the next step for '%s'  %s", status, msg_fail)
                sys.exit(12)



    log.info("- next step: %s%s%s -> %s%s%s", color['pink'], status, color[''], color['cyan'], next_status, color[''])

    if stop_flag:
        next_status = "stop"

    # debugging
    if next_status == "stop":
        log.info("- stopping pipeline, stop flag is set")
        sys.exit(4)


    return next_status



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "SAG-omizer"
    version = "1.4"



    # path names
    CONTAM_ID_PATH = "contam"
    NORM_PATH = "normalize"
    SUBSAMPLE_PATH = "subsample"
    ASM_PATH = "asm"
    CONTIG_TRIM_PATH = "trim"
    POOL_ASM_PATH = "pool_asm"
    DECONTAM_PATH = "prodege"
    ACDC_DECONTAM_PATH = "acdc"
    TETRAMER_PATH = "tetramer"
    CHECKM_PATH = "checkm"
    ANTIFAM_PATH = "antifam"
    SINGLE_COPY_GENE_PATH = "single_copy_gene"
    PCT_READS_ASM_PATH = "pct_reads_assembled"
    NCBI_PRESCREEN_PATH = "ncbi_prescreen"
    GC_HIST_PATH = "gc_hist"
    BLAST_PATH = "megablast"
    BLAST_16S_PATH = "blast_16s"
    GC_COV_PATH = "gc_cov"
    CONTIG_GC_PATH = "contig_gc"
    CHAFF_ALIGN_PATH = "chaff_align"
    CONTIG_REPORT_PATH = "contig_report"
    QUAL_PATH = "qual"
    REPORT_PATH = "report"


    RQC_URL = "https://rqc.jgi-psf.org" # used in get_pooled_status

    DEFAULT_CONFIG_FILE = os.path.realpath(os.path.join(my_path, "../sag/", "config", "sag.cfg"))


    uname = getpass.getuser() # get the user: brycef for testing

    if uname == "brycef":
        RQC_URL = "https://rqc-test.jgi-psf.org" # used in get_pooled_status

    color = {}
    color = set_colors(color, True)
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)


    # Parse options
    usage = "sag.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
Steps: (for --step option)
  contam_id, normalize, subsample, assembly, contig_trim, decontamination,
  contig_gc_plot, gc_cov, blast, blast_16s, megan, ncbi_prescreen, pct_reads_asm, single_copy_gene_archaea, single_copy_gene_bacteria, tetramer
  pdf_report
add "_stop" to run that step and stop.  e.g. (./sag.py --step blast_stop)
    """

    parser = ArgumentParser(usage = usage)


    parser.add_argument("-f", "--fastq", dest="fastq", type=str, help="Input fastq (required)")
    parser.add_argument("-l", "--lib", dest="library_name", type=str, help="Library name (required)")
    parser.add_argument("-o", "--output-path", dest="output_path", type=str, help="Output path to write to, uses pwd if not set")

    parser.add_argument("-c", "--config", dest="config_file", type=str, help="Location of sag.cfg file to use")
    parser.add_argument("-s", "--step", dest="step", type=str, help="Step to start running on")
    parser.add_argument("-m", "--run-mode", dest="run_mode", type=str, help="Run mode (auto [default], pool-asm, pool-decontam, not-pool)")
    parser.add_argument("-org", "--org-name", dest="org_name", type=str, help="Organism name (optional), use this instead of whats in the library_info.tax* fields for ProDeGe")
    parser.add_argument("-t", "--tax-id", dest="tax_id", type=int, help="ncbi taxonomy id to use in place of library_info.tax* fields")
    parser.add_argument("--fasta", dest="input_fasta", type=str, help="Input fasta (optional)")
    parser.add_argument("-acdc", "--acdc", dest="acdc", default = False, action = "store_true", help = "use ACDC for decontamination (prodege is default)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)


    status = "start" # step + X = run step and exit?  (e.g. gc_covX)  -- to do
    output_path = None
    fastq = None
    input_fasta = None
    library_name = None
    org_name = None # organism name
    config_file = None
    stop_flag = False # stop after running a step, e.g. gc_cov_stop
    run_mode = "auto" # auto detect if pooled or not pooled, what type to run
    tax_id = 0 # ncbi tax id
    acdc_flag = False # run ACDC if true rather than prodege

    # parse args
    args = parser.parse_args()

    print_log = args.print_log
    acdc_flag = args.acdc
    
    if args.input_fasta:
        input_fasta = args.input_fasta

    if args.output_path:
        output_path = args.output_path

    if args.fastq:
        fastq = args.fastq

    if args.library_name:
        library_name = args.library_name

    if args.org_name:
        org_name = args.org_name

    if args.tax_id:
        tax_id = args.tax_id

    if args.run_mode:
        run_mode = str(args.run_mode).lower()



    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/sag"

        if output_path == "t1":
            # offline
            # ASXWW
            # /global/dna/shared/rqc/pipelines/jigsaw_single_cell/archive/02/88/53/03/
            # $RQC_SHARED/filtered_seq_unit/00/00/97/38/9738.1.141798.AGTTC.anqdpht.fastq.gz
            # jamo fetch all filename 9738.1.141798.AGTTC.anqdpht.fastq.gz
            #fastq = "/global/dna/dm_archive/rqc/filtered_seq_unit/00/00/97/38/9738.1.141798.AGTTC.anqdpht.fastq.gz"
            fastq = "/global/projectb/sandbox/rqc/analysis/qc_user/sag/9738.1.141798.AGTTC.anqdpht.fastq.gz"
            library_name = "ASXWW"
            tax_id = 9685 # felis catus, testing

            #$ qsub -b yes -j yes -m as -now no -w e -N BCWAG -l ram.c=120G,h_rt=86400 -P gentech-rqc.p -o /global/projectb/scratch/brycef/sag/sag-BCWAG.log -js 505 "/global/homes/b/brycef/git/seqqc-9399/sag/sag.py -f /global/dna/dm_archive/rqc/filtered_seq_unit/00/01/08/35/10835.1.181450.CGTACTA-TAAGGCT.anqdpht.fastq.gz -l BCWAG -o /global/projectb/scratch/brycef/sag/BCWAG"
            # Sczerba.2 pool
            #library_name = "BCWAG"
            #fastq = "/global/dna/dm_archive/rqc/filtered_seq_unit/00/01/08/35/10835.1.181450.CGTACTA-TAAGGCT.anqdpht.fastq.gz"

        if output_path == "t2":
            # available for different lib!!
            #fastq = "/global/dna/dm_archive/rqc/filtered_seq_unit/00/00/97/81/9781.1.142821.GTAGA.anqdpht.fastq.gz"
            #fastq = "/global/projectb/sandbox/rqc/analysis/qc_user/sag/9781.1.142821.GTAGA.anqdpht.fastq.gz"
            #library_name = "ATNUW"
            # ATNUW = 1 contig

            # 2017-12-07: denovo testing
            library_name = "CANUT"
            fastq = "/global/projectb/sandbox/rqc/analysis/qc_user/sag/11926.1.227557.TCCTGAG-ATTAGAC.filter-SAG.fastq.gz"


        elif output_path == "t3":
            #fastq = "/global/dna/dm_archive/rqc/filtered_seq_unit/00/00/97/82/9782.1.142826.TAGCT.anqdpht.fastq.gz"
            fastq = "/global/projectb/sandbox/rqc/analysis/qc_user/sag/9782.1.142826.TAGCT.anqdpht.fastq.gz"

            library_name = "ATTTZ"


        elif output_path == "t4":
            # for pooled test
            #fastq = "/global/dna/dm_archive/rqc/filtered_seq_unit/00/00/97/42/9742.1.141801.AGGCAGA-ATAGAGA.anqdpht.fastq.gz"
            #fastq = "/global/projectb/sandbox/rqc/analysis/qc_user/sag/9742.1.141801.AGGCAGA-ATAGAGA.anqdpht.fastq.gz"
            #library_name = "ATXOC"

            fastq = "/global/dna/dm_archive/rqc/filtered_seq_unit/00/01/05/47/10547.1.164947.CGGAGCC-ATTAGAC.anqdpht.fastq.gz"
            library_name = "BAGOO"
            # https://rqc.jgi-psf.org/api/rqcws/sag_pool/BAGOO
            # --run-mode pool-decontam
            # has 0 in chaff

        elif output_path == "t5":

            fastq = "/global/dna/dm_archive/rqc/filtered_seq_unit/00/01/05/47/10547.1.164947.TACGCTG-TACTCCT.anqdpht.fastq.gz"
            library_name = "BAGOX"

            # restoring it ...
            # Ridge.rhizoCC.SAGs.1, _id = 574eb1aa7ded5e3df1ee0b7d
            # jamo report select file_name, file_status, file_path, current_location where _id = '574eb1aa7ded5e3df1ee0b7d'
            # jamo fetch custom file_id=4981782
            # https://rqc.jgi-psf.org/api/rqcws/sag_pool/BAGOX
            # --run-mode pool-decontam
            # has chaff!
            # curl https://sdm2.jgi-psf.org//api/metadata/file/574eb1aa7ded5e3df1ee0b7d

        elif output_path == "t5x":
            library_name = "ATNUA"
            fastq = "/global/dna/dm_archive/rqc/filtered_seq_unit/00/00/97/53/9753.1.142243.GCCAA.anqdpht.fastq.gz"

        elif output_path == "t6":
            #library_name = "ATXXO"
            #fastq = "/global/dna/dm_archive/rqc/filtered_seq_unit/00/00/98/11/9811.1.143172.GGACTCC-TATGCAG.anqdpht.fastq.gz"
            library_name = "BCWAG"
            fastq = "/global/dna/dm_archive/rqc/filtered_seq_unit/00/01/08/35/10835.1.181450.CGTACTA-TAAGGCT.anqdpht.fastq.gz"

        output_path = os.path.join(test_path, library_name)


    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)




    config_file = os.path.join(output_path, "sag.cfg")

    if args.config_file:
        config_file = args.config_file



    # initialize my logger
    log_file = os.path.join(output_path, "sag.log")



    # log = logging object
    log_level = "INFO"
    log = get_logger("sag", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "fastq", fastq)
    if input_fasta:
        log.info("%25s      %s", "fasta", input_fasta)
    log.info("%25s      %s", "library_name", library_name)
    if org_name:
        log.info("%25s      %s", "org_name", org_name)
    if tax_id:
        log.info("%25s      %s", "tax_id", tax_id)
    log.info("%25s      %s", "run_mode", run_mode)
    log.info("%25s      %s", "step", status)
    log.info("%25s      %s", "config_file", config_file)
    if acdc_flag:
        log.info("%25s      %s", "acdc_flag", acdc_flag)
    log.info("")



    if not fastq:
        print "Error: no fastq specified!  %s" % msg_fail
        log.error("Error: no fastq specified!  %s", msg_fail)
        sys.exit(2)

    if not os.path.isfile(fastq):
        print "Error: cannot find fastq %s  %s" % (fastq, msg_fail)
        log.error("Error: cannot find fastq %s  %s", fastq, msg_fail)
        sys.exit(2)


    if input_fasta:
        if not os.path.isfile(input_fasta):
            print "Error: input fasta not on file system: %s  %s" % (input_fasta, msg_fail)
            log.error("Error: input fasta not on file system: %s  %s", input_fasta, msg_fail)
            sys.exit(2)

    if not os.path.isfile(config_file):
        log.info("- cannot find config file: %s  %s", config_file, msg_warn)
        log.info("- using default config file.")
        cmd = "cp %s %s" % (DEFAULT_CONFIG_FILE, config_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)



    status_log = os.path.join(output_path, "status.log")
    rqc_file_log = os.path.join(output_path, "rqc-files.tmp")
    rqc_stats_log = os.path.join(output_path, "rqc-stats.tmp")


    if org_name:
        append_rqc_stats(rqc_stats_log, "org_name", org_name)
    if tax_id:
        append_rqc_stats(rqc_stats_log, "tax_id", tax_id)

    # get info from RQC on the run info (/api/rqcws/sag_pool/AYZGN)
    decontam_fasta, chaff_fasta, sag_run_folder, sd_fs_location, pool_cnt = get_pooled_status(library_name)

    # testing
    #if uname == "brycef":
    #    if library_name == "CANUT": run_mode = "pool-asm"

    # detect the run mode
    if run_mode == "auto":
        if pool_cnt > 2:
            run_mode = "pool-asm"
            if decontam_fasta and sag_run_folder:
                run_mode = "pool-decontam"
                # its up to the pool-decontam setup function to determine if those are actually online!
                if not input_fasta:
                    input_fasta = decontam_fasta

        else:
            run_mode = "not-pool"

        log.info("- run_mode: %s, pool_cnt = %s, fasta = %s", run_mode, pool_cnt, decontam_fasta)



    # read in config file into config dictionary
    config = load_config_file(config_file, log)

    file_dict = {}



    checkpoint_step(status_log, "## New Run")

    fasta_type = "scaffolds_trim"  # Unscreened
    decontam_fasta_type = "clean_scaffolds" # Screened - switch to pooled if ProDeGe produces no output and its a pooled run
    fastq_type = "subsampled_fastq"

    # contants: changing these 2 will mess up the .tex and .txt templates
    screened_name = "screen"
    unscreened_name = "unscreen"

    screened_path = os.path.join(output_path, screened_name)
    unscreened_path = os.path.join(output_path, unscreened_name)



    # need pooled decontam fasta from sag_decontam.py
    if run_mode == "pool-decontam":
        fasta_type, status = setup_pool_decontam(input_fasta, chaff_fasta, sag_run_folder, sd_fs_location, library_name)

    else:
        if input_fasta:
            fasta_type = "input_fasta"
            save_file(input_fasta, fasta_type, file_dict, output_path)




    # test area

    #make_chaff_tarball(screened_path, output_path, run_mode)
    #run_mode = "pool-decontam"
    
    #print get_file("input_fastq")
    #print get_taxonomy("felis catus", log)
    # {'tax_order': 'Carnivora', 'tax_genus': 'Felis', 'tax_class': 'Mammalia', 'tax_family': 'Felidae', 'tax_phylum': 'Chordata', 'tax_species': 'Felis catus', 'tax_kingdom': 'Eukaryota'}
    #print get_taxonomy_by_id(9685, log)
    #print get_taxonomy_by_id(tax_id, log)
    #sys.exit(44)
    #get_ssu(library_name, "cat.txt")
    #do_blast_16s("/global/projectb/scratch/brycef/sag/ASXWW/unscreen/", "scaffolds_trim", library_name, unscreened_name)
    #ap_id, at_id = get_analysis_project_id(1085207, 37, 39, output_path, log)
    #print "ap: %s, at: %s" % (ap_id, at_id)
    #ap_id, at_id = get_analysis_project_id(1085207, 2, 47, output_path, log)
    #print "ap: %s, at: %s" % (ap_id, at_id)
    #do_final(run_mode)
    #print green_genes_pdf("/global/projectb/scratch/qc_user/rqc/prod/pipelines/sag_pool/failed/02/90/76/15/unscreen/megablast/megablast.pool_decontam.fasta.vs.green_genes16s.insa_gg16S.fasta.parsed.tophit")
    #    1093669
    #def get_ssu("AXWZW", "~/16s.fasta", log)
    #BAGAC
    # get_collab_16s
    # qaqc-microbial -get_collab_16s
    #get_ssu("ATXYG", "/global/projectb/scratch/brycef/ATXYG.fasta")

    #do_contig_report(screened_path, library_name)
    #sys.exit(48)



    # gunzip files so we don't reprocess, rename *.txt for stats & files to *.tmp
    setup_run()


    #if status == "start":
    save_file(fastq, "input_fastq", file_dict, output_path)
    get_bbtools_version(rqc_stats_log, output_path, log)



    # config options to skip screened or unscreened
    if 'skip_screened_report' in config:
        screened_path = None
    if 'skip_unscreened_report' in config:
        unscreened_path = None


    if args.step:
        status = args.step

        if status.endswith("_stop"):
            status = status.replace("_stop", "")
            stop_flag = True



    done = False
    cycle_cnt = 0
    while not done:

        cycle_cnt += 1

        # assembly -> contig trimming
        if status in ("start", "contam_id"):
            status = do_contam_id()

        if status == "normalize":
            status = do_normalization()
            fastq_type = "normalized_fastq"

        if status == "subsample":
            status = do_subsample()
            fastq_type = "subsampled_fastq"

        if status == "assembly":
            status = do_assembly()
            fasta = get_file("scaffolds", file_dict, output_path, log)

        if status == "contig_trim":
            status = do_contig_trim(library_name)
            fasta_type = "scaffolds_trim"


            # for pool-asm only run through contig_trim and quit
            if run_mode == "pool-asm":
                do_purge()
                status = "complete"
                # HCF? no, need complete for SAG Decontam to pull in assembly
                log.info("- this is a SAG from a pool, stopping to run pooled sag decontam.")



        # ProDeGe
        if status == "decontamination":
            # tax_id, org_name from args (optional)
            if acdc_flag:
                # ACDC uses rqc_api: "api/rqcws/lib2su/%s" % library_name to get taxonomy
                status, decontam_fasta_type = do_acdc(fasta_type, library_name)
            else:
                status, decontam_fasta_type = do_decontam(fasta_type, library_name, tax_id, org_name)
            # reset unscreened to trimmed scaffolds

            fasta_type = "scaffolds_trim"
            # the unscreened (fasta_type) = fasta after SAG Decontam if pool-decontam run (or plate level)

            # SEQQC-9399 - raw fasta = after trimming
            #if run_mode == "pool-decontam":
            #    fasta_type = "pool_decontam"


        # QC Analysis

        if status == "contig_gc_plot":
            if screened_path:
                status = do_contig_gc_plot(screened_path, decontam_fasta_type, screened_name)
            if unscreened_path:
                status = do_contig_gc_plot(unscreened_path, fasta_type, unscreened_name)


        if status == "gc_cov":
            if screened_path:
                status = do_gc_cov(screened_path, decontam_fasta_type, screened_name)
            if unscreened_path:
                status = do_gc_cov(unscreened_path, fasta_type, unscreened_name)


        if status == "blast":
            if screened_path:
                status = do_blast(screened_path, decontam_fasta_type, screened_name)
            if unscreened_path:
                status = do_blast(unscreened_path, fasta_type, unscreened_name)

        if status == "blast_16s":
            if screened_path:
                status = do_blast_16s(screened_path, decontam_fasta_type, library_name, screened_name)
                is_right_thing(screened_path, tax_id, org_name, library_name, screened_name)
            if unscreened_path:
                status = do_blast_16s(unscreened_path, fasta_type, library_name, unscreened_name)
                is_right_thing(unscreened_path, tax_id, org_name, library_name, unscreened_name)


        if status == "gc_hist":
            if screened_path:
                status = do_gc_hist(screened_path, decontam_fasta_type, library_name, screened_name)
            if unscreened_path:
                status = do_gc_hist(unscreened_path, fasta_type, library_name, unscreened_name)


        if status == "ncbi_prescreen":
            if screened_path:
                status = do_ncbi_prescreen(screened_path, decontam_fasta_type, screened_name)
            if unscreened_path:
                status = do_ncbi_prescreen(unscreened_path, fasta_type, unscreened_name)

        if status == "checkm":
            if screened_path:
                status = do_checkm(screened_path, decontam_fasta_type, screened_name)
            if unscreened_path:
                status = do_checkm(unscreened_path, fasta_type, unscreened_name)

        if status == "pct_reads_asm":
            if unscreened_path:
                status = do_pct_reads_asm(unscreened_path, fasta_type, unscreened_name)
            if screened_path:
                status = do_pct_reads_asm(screened_path, decontam_fasta_type, screened_name)

        if status == "tetramer":
            if screened_path:
                status = do_tetramer(screened_path, decontam_fasta_type, screened_name)
            if unscreened_path:
                status = do_tetramer(unscreened_path, fasta_type, unscreened_name)

        if status == "antifam":
            if screened_path:
                status = do_antifam(screened_path, decontam_fasta_type, screened_name)
            if unscreened_path:
                status = do_antifam(unscreened_path, fasta_type, unscreened_name)

        if status == "qual":
            if screened_path:
                status = do_qual(screened_path, decontam_fasta_type, screened_name)
            if unscreened_path:
                status = do_qual(unscreened_path, fasta_type, unscreened_name)

        if status == "contig_report":
            if screened_path:
                status = do_contig_report(screened_path, library_name)

        if status == "chaff_tarball":
            if screened_path:
                status = make_chaff_tarball(screened_path, library_name, output_path, run_mode)


        # Reports: pdf report does the *.txt, *.pdf, *.metadata.json
        if status in ("pdf_report", "pdf"):

            if screened_path:
                status = do_pdf_report(library_name, decontam_fasta_type, org_name, tax_id, screened_name)

            if unscreened_path:
                status = do_pdf_report(library_name, fasta_type, org_name, tax_id, unscreened_name)




        if status == "purge":
            do_final() # Kecia's final folder
            status = do_purge()

        if status == "complete":
            done = True

        if status == "failed":
            done = True

        if cycle_cnt > 85:
            log.info("- max cycles reached.  %s", msg_warn)
            done = True



        status = get_next_status(status)





    checkpoint_step(status_log, status)
    if status == "complete":

        # move rqc-files.tmp to rqc-files.txt
        append_rqc_stats(rqc_stats_log, "sag_version", "%s %s" % (my_name, version))
        rqc_new_file_log = os.path.join(output_path, "rqc-files.txt")

        cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        rqc_new_stats_log = os.path.join(output_path, "rqc-stats.txt")

        cmd = "mv %s %s" % (rqc_stats_log, rqc_new_stats_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    log.info("Completed %s", script_name)

    sys.exit(0)



"""
                      .-.
         .-._    _.../   `,    _.-.
         |   `'-'    \     \_'`   |
         \            '.__,/ `\_.--,      "Its not the size of the cat in the fight,
          /                '._/     |      Its the size of the fight in the cat!"
         /                    '.    /
        ;   _                  _'--;      For Whom The Bell Tools - Poe
     '--|- (_)       __       (_) -|--'
     .--|-          (__)          -|--.
      .-\-                        -/-.
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`




Other files for testing
  /global/dna/shared/data/functests/assembly

But for my recent BLAST testing I just pulled some random subsamples from APAHO and have copies here

/scratch/copeland/APAHO.1000.fa
/scratch/copeland/APAHO.1M.fa
/scratch/copeland/APAHO.4M.fa

"The whole point of a complete rewrite is to take a few steps backward in the short-run in order to be able to make some greater strides in the long-run"


auto-detect -- mode = "auto"
- call a rqc webservice with library name which returns...
-- pool count
-- fs_location for "pool"
-- cleaned fastq location
. if pool count > 2 then mode="pool"
. if cleaned fastq & fs_location then mode="pool-decontam"

mode = "pool-asm"
- run asm
- status = complete
- rqc-files & stats are renamed

mode="pool-decontam"
- rsync old "pool" folder (only certain folders?) - problem with log file?
- copy cleaned fastq & register as correct name
- start at ProDeGe and run screen, unscreen

mode="no-pool"
- run asm, screened, unscreened as normal

-------- RQC-747
To run the SAG-omizer pipeline:

$ module load jgi-rqc
$ sag.py -o (output path/run folder) -f (path-to-fastq/fastq.gz) -l (library name)

I've been using the following qsub parameters to launch the sag pipeline:
$ qsub -b yes -j yes -m as -now no -w e -N (name of the job)
-l ram.c=3.5G,h_rt=86400 -l exclusive.c=1 -pe pe_32 32
-P gentech-rqc.p -js 501
-o (cluster output log) "sag.py + options"

If you run the SAG-omizer on an existing run folder, it will skip each step if it finds that it ran that step.  To re-run a step you should rename/mv the folder for that step.  The steps are listed in the $ sag.py --help options

The normal run folder structure is:
- asm = spades assembly with full spades assembly
- contam = contam identification
- normalize = normalization with normalized fastq
- subsample = subsample step with subsampled fastq
- trim = trimmed fasta (scaffolds.2k.fasta) (raw assembly)
- prodege = prodege screening
--- sag_decontam = contains clean and dirty fasta (.fna)
- screen = analysis & reporting for screened assembly (prodege & crossblock cleaned fasta)
- unscreen = analysis & reporting for unscreened assembly (trimmed fasta/raw assembly)
- report = pdf, txt reports for screened and unscreened assembly

The main folder has:
 * sag.cfg = configuration file for SAG-omizer
 * sag.log = run log for SAG-omizer
 * status.log = step status (picked up by RQC for reporting, knowing when pipeline is failed or complete)
 * rqc-files.txt = files needed for reporting in RQC
 * rqc-stats.txt = stats needed for reporting in RQC
 * *.metadata.json = metadata for JAT submissions
 * file_list.txt, bbtools.txt, spades.txt are misc files for storing temporary data

Stuff to save:
- command line for seal + options
- Sag-omizer version
- assembly - spades version & options
- LAST version for gc histograms
- blast - version + cmdline
- tetramer version + cmdline
- scg - cmd + version
- bbtools version x

"""
