#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Contig_GC
- for ISO, CE, SPS, SAG pipelines
- weighted & normal gc histogram for contigs
- replaces Jigsaw: contigGCplot.pl

~/git/jgi-rqc-pipeline/sag/contig_gc.py -o t1


$RQC_SHARED/pipelines/microbe_iso/archive/02/94/56/39/

Dependencies
bbtools (stats.sh)
gnuplot (in bbtools container)

v 1.0: 2017-05-08
- initial version
- works with genepool & denovo

v 1.1: 2017-08-15
- RQC-1007: plot contig size (and add gc content per contig)

v 1.2: 2018-04-25
- added plot for genome size vs percent of genome
- added contig size histogram (RQC-1007)

v 1.3: 2018-07-12
- updated to work with megahit headers (headers have spaces in them)

To do:


gnupot alt:
wildish@mc1216: sles12:~> module purge
cmdModule.c(567):ERROR:50: Cannot open file '/usr/share/Modules/modulefiles/modules' for 'reading'
wildish@mc1216: sles12:~> module load gnuplot/4.6.2
wildish@mc1216: sles12:~> gnuplot


"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import getpass
import numpy
import pandas as pd
import re

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, run_cmd, get_colors, get_msg_settings


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Get the gc content and contig length per contig in the fasta
'''
def get_gc(fasta):
    log.info("get_gc: %s", fasta)

    gc_file = os.path.join(output_path, "contig_gc.txt")
    if os.path.isfile(gc_file):
        log.info("- found %s, skipping gc_histogram", gc_file)
    else:

        # gc per contig
        #$ stats.sh gc=stdout.txt in=contigs.gene.fasta gcformat=4 | head -20
        gc_log = os.path.join(output_path, "gc.log")
        cmd = "#bbtools;stats.sh gc=%s in=%s gcformat=4 ow=t > %s 2>&1" % (gc_file, fasta, gc_log)
        run_cmd(cmd, log)


    # update contig gc to add col 3 = cumulative bp and col 4 = percent of cumulative
    total_bp = 0
    fh = open(gc_file, "r")
    for line in fh:
        if line.startswith("#"):
            continue
        # megahit uses spaces in its header, so we need to use last 2 fields for base count and gc
        arr = line.split()
        bp = int(arr[-2])
        total_bp += bp
    fh.close()
    log.info("- assembly size: %s bp", total_bp)

    gc_sum_file = os.path.join(output_path, "contig_gc_sum.txt") # contig_gc with extra columns
    fhw = open(gc_sum_file, "w")
    fhw.write("#Name  Length  GC  CumLen  CumLenPct\n")
    bp_pct = 0.0
    bp_sum = 0

    fh = open(gc_file, "r")

    for line in fh:
        if line.startswith("#"):
            continue

        arr = line.split()
        bp_sum += int(arr[-2])
        bp_pct = bp_sum / float(total_bp)
        contig_name = "_".join(arr[0:-2]) # all but the last 2 fields
        contig_name = re.sub(r'[^0-9A-Za-z\=\-\.]+', '_', contig_name)
        fhw.write("%s  %s  %s  %s  %s\n" % (contig_name, arr[-2], arr[-1], bp_sum, bp_pct))

    fh.close()
    fhw.close()

    log.info("- created file: %s", gc_sum_file)


'''
using contig_gc, plot axis 1 as contig size, axis 2 as gc content
RQC-1007
'''
def get_contig_hist_plot():
    log.info("get_contig_hist_plot")

    contig_gc_file = os.path.join(output_path, "contig_gc_sum.txt")
    err_cnt = 0


    # set term dumb;
    gnu_template = """#!/usr/bin/gnuplot

set term png enhanced size [plot_size];
set output "[output_png]";

set grid;
set style fill transparent solid 0.75 border -1;

set xlabel "Contig Id";
set ylabel "Contig Size (BP)";
set y2label "Percent";
set y2tics border;

set xrange[0:];
set y2range[0:100];
set title "[plot_title]";
set key top box;
plot "[contig_gc_file]"  u 0:2 axes x1y1 with boxes lt rgb "#FF9999" title "Contig Size",\
"[contig_gc_file]" u 0:(100*$3) axis x1y2 with points lt rgb "#000099" title "GC Percent",\
"[contig_gc_file]" u 0:(100*$5) axis x1y2 with lines lt rgb "#009900" title "Percent Cumulative Assembly Size";
    """

    replace_keys = {
        "plot_title" : "Contig Size and GC Percent per Contig for %s" % library_name,
        "output_png" : os.path.join(output_path, "contig_hist.png"),
        "contig_gc_file" : contig_gc_file,
        "plot_size" : plot_size,
    }

    for my_key in replace_keys:
        my_key2 = "[%s]" % my_key
        gnu_template = gnu_template.replace(my_key2, replace_keys[my_key])

    gnu_file = os.path.join(output_path, "contig_hist.gnu.txt")
    fh = open(gnu_file, "w")
    fh.write(gnu_template)
    fh.close()
    log.info("- created: %s", gnu_file)


    gnu_log = os.path.join(output_path, "gnu_plot_contig_hist.log")
    cmd = "#gnuplot;gnuplot %s > %s 2>&1" % (gnu_file, gnu_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    if exit_code == 0:
        log.info("- created png: %s  %s", replace_keys['output_png'], msg_ok)
    else:
        log.error("- failed to create png: %s  %s", replace_keys['output_png'], msg_fail)
        err_cnt += 1

    return err_cnt


'''
Create GC Histograms
'''
def get_gc_histogram():
    log.info("get_gc_histogram")
    gc_file = os.path.join(output_path, "contig_gc.txt")

    weight_list = []
    gc_list = []

    fh = open(gc_file, "r")
    for line in fh:
        if line.startswith("#"):
            continue
        arr = line.split()
        if len(arr) > 2:

            gc = float(arr[-1]) * 100
            weight = int(arr[-2]) # length
            gc_list.append(gc)
            weight_list.append(weight)

    fh.close()

    # do gc avg, std dev
    gc_avg = numpy.average(gc_list)
    gc_stdev = numpy.std(gc_list)
    gc_median = numpy.median(gc_list)

    stats_file = os.path.join(output_path, "gc.stats.txt")
    fh = open(stats_file, "w")
    fh.write("gc_avg=%s\n" % gc_avg)
    fh.write("gc_stdev=%s\n" % gc_stdev)
    fh.write("gc_median=%s\n" % gc_median)
    fh.close()

    # write gc_histogram to a file, by percent

    gc_hist = numpy.histogram(gc_list, bins = range(0, 101))

    c = 0
    gc_sum = 0
    for i in gc_hist[0]:
        gc_sum += i

    gc_file = os.path.join(output_path, "contig.gc")
    fh = open(gc_file, "w")
    fh.write("#Pct\tGC_PCT\n")
    for i in gc_hist[0]:
        c += 1
        if i > 0:
            gc_pct = 100 * i / float(gc_sum)

            fh.write("%s\t%s\n" % (c, gc_pct))
    fh.close()

    log.info("- created: %s", gc_file)



    # write weighted gc_histogram to a file, by percent

    wgc_hist = numpy.histogram(gc_list, bins = range(0, 101), weights = weight_list)

    c = 0
    gc_sum = 0
    for i in wgc_hist[0]:
        gc_sum += i

    gc_file = os.path.join(output_path, "contig-weighted.gc")
    fh = open(gc_file, "w")
    fh.write("#Pct\tGC_PCT\n")
    for i in wgc_hist[0]:
        c += 1
        if i > 0:
            gc_pct = 100 * i / float(gc_sum)
            fh.write("%s\t%s\n" % (c, gc_pct))
    fh.close()

    log.info("- created: %s", gc_file)


'''
Do gnu plots
'''
def gnu_plot_gc(library_name):
    log.info("gnu_plot_gc: %s", library_name)
    err_cnt = 0

    gnu_template = """#!/usr/bin/gnuplot

set grid;
set term png enhanced size [plot_size];
set output "[output_png]";
set xlabel "Percent GC";
set ylabel "Percent";
set title "[plot_title]";
set xrange [0:100];
set style fill transparent solid 1.0 border;
plot "[gc_hist_file]" u 1:2 w lp t 'Non-weighted', \\
"[wgc_hist_file]" u 1:2 w lp t 'Weighted by contig length'
    """


    gc_file = os.path.join(output_path, "contig.gc")
    wgc_file = os.path.join(output_path, "contig-weighted.gc")
    replace_keys = {
        "plot_title" : "Contig GC Histogram for %s" % library_name,
        "output_png" : os.path.join(output_path, "contig_gc.png"),
        "gc_hist_file" : gc_file,
        "wgc_hist_file" : wgc_file,
        "plot_size" : plot_size,
    }

    for my_key in replace_keys:
        my_key2 = "[%s]" % my_key
        gnu_template = gnu_template.replace(my_key2, replace_keys[my_key])

    gnu_file = os.path.join(output_path, "contig_gc.gnu.txt")
    fh = open(gnu_file, "w")
    fh.write(gnu_template)
    fh.close()
    log.info("- created: %s", gnu_file)


    gnu_log = os.path.join(output_path, "gnu_plot_contig_gc.log")
    cmd = "#gnuplot;gnuplot %s > %s 2>&1" % (gnu_file, gnu_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    if exit_code == 0:
        log.info("- created png: %s  %s", replace_keys['output_png'], msg_ok)
    else:
        log.error("- failed to create png: %s  %s", replace_keys['output_png'], msg_fail)
        err_cnt += 1

    return err_cnt

'''
need to get size of each contig/scaffold in the assembly
create a file of
contig name|base count|percent of total cumulative
* file is ordered by smallest contig to largest
'''
def get_pct_of_genome(fasta):
    log.info("get_pct_of_genome: %s", fasta)

    contig_list = [] # [ { contig : x, bases : y }, ...]
    bcnt = 0 # base count per contig
    bcnt_ttl = 0 # total bases
    header = "" # contig header

    panda_flag = True # False = use panda 0.12.0 methods
    if pd.__version__ == "0.12.0":
        panda_flag = False

    fh = open(fasta, "r")
    for line in fh:
        if line.startswith(">"):

            if header:

                header = re.sub(r'[^0-9A-Za-z\=\-\.]+', '_', header)
                my_json = { "contig" : header, "bases" : bcnt, "pct" : 0.0 }
                contig_list.append(my_json) #[header] = bcnt
                bcnt_ttl += bcnt
                log.info("- %s: %s bases", header, bcnt)

            bcnt = 0
            header = line.strip().replace(">", "")

        else:
            bcnt += len(line.strip())

    if header:
        #contig_dict[header] = bcnt
        my_json = { "contig" : header, "bases" : bcnt, "pct" : 0.0 }
        contig_list.append(my_json) #[header] = bcnt
        bcnt_ttl += bcnt
        log.info("- %s: %s bases", header, bcnt)

    fh.close()

    log.info("- total bases: %s", bcnt_ttl) # matches scaffold count

    # create panda table from contig list
    tbl = pd.DataFrame(contig_list, columns = ['contig', 'bases', 'pct', 'cpct', 'n50', 'n90'])
    #print tbl

    # sort by bases - ascending
    if panda_flag:
        # denovo
        tbl = tbl.sort_values(by=['bases'], ascending=True)
    else:
        # genepool
        tbl = tbl.sort(['bases'], ascending=True)    
        

    # calc percent and cpct (cumulative percent)
    # get n50, n90

    cpct = 0
    n50 = 0
    n90 = 0

    for index, row in tbl.iterrows():

        bases = row['bases']
        pct = bases / float(bcnt_ttl)

        cpct = cpct + pct
        if panda_flag:
            tbl.at[index,'pct'] = pct
            tbl.at[index,'cpct'] = cpct
        else:
            tbl.set_value(index, 'pct', pct)
            tbl.set_value(index, 'cpct', cpct)


        if cpct > 0.1 and n90 == 0:
            n90 = cpct
        if cpct > 0.5 and n50 == 0:
            n50 = cpct
            #print "pct: %s, cpct: %s" % (pct, cpct)


    log.info("- n50: %s", n50)
    log.info("- n90: %s", n90)


    for index, row in tbl.iterrows():
        if panda_flag:
            tbl.at[index,'n50'] = n50
            tbl.at[index,'n90'] = n90
        else:
            tbl.set_value(index, 'n50', n50)
            tbl.set_value(index, 'n90', n90)

    #print tbl

    contig_plot_file = os.path.join(output_path, "contig_sum.txt")
    tbl.to_csv(contig_plot_file, sep='\t', index=False)
    log.info("- created: %s", contig_plot_file)


'''
Do the plot for the genome pct vs scaffold size
- wouuld like n50, n90 lines to go all the way across the plot but I don't know gnu plot enough
'''
def make_pct_genome_plot(library_name):
    log.info("make_pct_genome_plot: %s", library_name)


    err_cnt = 0
    gnu_template = """#!/usr/bin/gnuplot

set term png enhanced size [plot_size];
set output "[output_png]";
set grid;
set style fill transparent solid 0.75 border -1;
set xlabel "Scaffold Size (BP)";
set ylabel "Fraction of Assembly in Scaffolds < x";
set title "[plot_title]";

set logscale y;
set logscale x;

set key top box;

plot "[contig_plot_file]"  u 2:4 w lp notitle,\
"[contig_plot_file]"  u 2:5 w lp pt 0 title 'n90',\
"[contig_plot_file]"  u 2:6 w lp pt 0 title 'n50';
    """
    # plot title =  lib_name


    contig_sum = os.path.join(output_path, "contig_sum.txt")

    replace_keys = {
        "plot_title" : "Scaffold Size vs Percent of Genome for %s" % library_name,
        "output_png" : os.path.join(output_path, "genome_vs_size.png"),
        "contig_plot_file" : contig_sum,
        "plot_size" : plot_size,
    }

    for my_key in replace_keys:
        my_key2 = "[%s]" % my_key
        gnu_template = gnu_template.replace(my_key2, replace_keys[my_key])

    gnu_file = os.path.join(output_path, "contig_sum.gnu.txt")
    fh = open(gnu_file, "w")
    fh.write(gnu_template)
    fh.close()
    log.info("- created: %s", gnu_file)


    gnu_log = os.path.join(output_path, "gnu_plot_contig_sum.log")
    cmd = "#gnuplot;gnuplot %s > %s 2>&1" % (gnu_file, gnu_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    if exit_code == 0:
        log.info("- created png: %s  %s", replace_keys['output_png'], msg_ok)
    else:
        log.error("- failed to create png: %s  %s", replace_keys['output_png'], msg_fail)
        err_cnt += 1

    return err_cnt



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "Contig GC"
    version = "1.3"

    cmd = "#bbtools;bbversion.sh"
    bbtools_ver, _, _ = run_cmd(cmd)
    bbtools_ver = bbtools_ver.strip()
    version += " (bbtools %s)" % bbtools_ver

    uname = getpass.getuser() # get the user, brycef can't use scell.p, but qc_user can!


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)


    # Parse options
    usage = "contig_gc.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
Creates contig gc histogram (weighted too)
also genome size vs percent of genome and contig size histogram
    """

    parser = ArgumentParser(usage = usage)


    parser.add_argument("-f", "--fasta", dest="fasta", type = str, help = "Input fasta")
    parser.add_argument("-l", "--library", dest="library_name", type = str, help = "library name/chart title")
    parser.add_argument("-p", "--plot-size", dest="plot_size", type = str, help = "plot size e.g. '800,600', default 800,640")
    parser.add_argument("-o", "--output-path", dest="output_path", type = str, help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)

    status = "start" # step + X = run step and exit?  (e.g. gc_covX)  -- to do
    output_path = None
    fasta = None
    library_name = None
    plot_size = "800,640"
    print_log = False

    # parse args
    args = parser.parse_args()

    print_log = args.print_log

    if args.fasta:
        fasta = args.fasta

    if args.output_path:
        output_path = args.output_path

    if args.library_name:
        library_name = args.library_name

    if args.plot_size:
        plot_size = args.plot_size


    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/iso"

        if output_path == "t1":

            fasta = "/global/projectb/scratch/brycef/iso/BSHTN.fasta"
            library_name = "BSHTN"
            output_path = "gc1"
        output_path = os.path.join(test_path, output_path)


    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    if not library_name:
        library_name = os.path.basename(fasta)


    # initialize my logger
    log_file = os.path.join(output_path, "contig_gc.log")



    # log = logging object
    log_level = "INFO"
    log = None
    log = get_logger("contig_gc", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "fasta", fasta)
    log.info("%25s      %s", "library_name", library_name)
    log.info("%25s      %s", "plot_size", plot_size)
    log.info("")

    if not os.path.isfile(fasta):
        log.error("Error: cannot find fasta %s  %s", fasta, msg_fail)
        sys.exit(2)

    if not library_name:
        library_name = os.path.basename(fasta)

    # bbtools - calc gc for each contig
    get_gc(fasta)

    # plot gc histogram y1 = contig bp, y2 = gc content
    get_contig_hist_plot()


    # get this histogram of the gcs
    get_gc_histogram()

    # contig size vs percent of genome
    get_pct_of_genome(fasta)

    # make plot
    err_cnt = 0

    err_cnt = gnu_plot_gc(library_name)

    err_cnt += make_pct_genome_plot(library_name)

    if err_cnt > 0:
        log.error("errors encountered  %s", msg_fail)
        sys.exit(9)

    log.info("Completed %s", script_name)

    sys.exit(0)

'''

      _ _,---._
   ,-','       `-.___
  /-;'               `._           mmmm sacralicious waffles
 /\/          ._   _,'o \
( /\       _,--'\,','"`. )
 |\      ,'o     \'    //\
 |      \        /   ,--'""`-.
 :       \_    _/ ,-'         `-._
  \        `--'  /                )
   `.  \`._    ,'     ________,','
     .--`     ,'  ,--` __\___,;'
      \`.,-- ,' ,`_)--'  /`.,'
       \( ;  | | )      (`-/
         `--'| |)       |-/
           | | |        | |
           | | |,.,-.   | |_
           | `./ /   )---`  )
          _|  /    ,',   ,-'
         ,'|_(    /-<._,' |--,
         |    `--'---.     \/ \
         |          / \    /\  \
       ,-^---._     |  \  /  \  \
    ,-'        \----'   \/    \--`.
   /            \              \   \

'''
