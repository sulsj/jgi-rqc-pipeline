#!/bin/bash -l

# methylation release script
# updated 2018-05-30 (use "close" for ap_tool)

# usage ./meth.sh (output folder) (library name)
# ./meth.sh $RQC_SHARED/pipelines/heisenberg/archive/00/00/21/80/ BAGTA
#cd $1/final
echo "JAT release"
module load jamo
#module load jamo/dev

echo "jat import methylation $1/methylation.metadata.json"
jat import methylation $1/methylation.metadata.json
echo
module unload jamo

# include qaqc env
source /global/dna/projectdirs/PI/rqc/prod/jgi-rqc/sh/qaqc_env.sh


echo "AT Status update"
/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/ap_tool.py -l $2 -tatid 32 -c close -s 9
echo

