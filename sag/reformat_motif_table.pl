#!/usr/bin/env perl
use warnings;
use strict;
use Text::ParseWords;
use Data::Dumper;
use File::Basename;
use Cwd("abs_path");

my $motif_summary_table = $ARGV[0];

unless(@ARGV > 0){
	die "$0  <motif_summary.tab>\n";
}

my %grouped_motifs = motif_table_reader($motif_summary_table, ",");
#print Dumper(%grouped_motifs);

#output in the same directory as the motif_summary.tab file
my $results_dir  = dirname($motif_summary_table);
my $summary = "$results_dir/motif_summary_reformat.tab";
open(my $summary_fh, ">$summary") or die $!;

#print summary table headers
print $summary_fh(
	#first motif in pair
	"Motif\t",
	"Pos\t",
	"Type\t",
	"Count\t",
	"%Mod\t",
	"MeanScore\t",
	"MeanIpdR\t",
	"MeanCov\t",
	"ObjScore\n",
);

# Determine the maximum motif score associated with each groupTag
my %obj_score;
foreach my $groupTag(keys %grouped_motifs){
	foreach my $motif(keys %{$grouped_motifs{$groupTag}}){
		my $motif_score = $grouped_motifs{$groupTag}{$motif}{objective_score};
		if(exists($obj_score{$groupTag})){
			if($motif_score > $obj_score{$groupTag}){
				$obj_score{$groupTag} = $motif_score;
			}
		}else{
			$obj_score{$groupTag} = $motif_score;
		}
	}
}

# Sort the grouped motifs by descending objective score
foreach my $groupTag(sort {$obj_score{$b} <=> $obj_score{$a}} keys %obj_score) {
	print $summary_fh "$groupTag\t";
	# Sort the individual motifs in a group alphabetically (case insensitive)
	foreach my $motif(sort {lc($a) cmp lc($b)} keys %{$grouped_motifs{$groupTag}}){
		#print out the table data
		my @keys = qw(
			centerPos
			modificationType
			ngenome
			mod_fraction
			meanscore
			meanipdRatio
			meanCoverage
			objective_score
		);
		print $summary_fh join("\t", map {$grouped_motifs{$groupTag}{$motif}{$_}} @keys);
		last;
	}print $summary_fh "\n";
}

sub motif_table_reader {
	my ($motif_summary_table, $delimiter) = @_;
	open(my $motif_fh, "$motif_summary_table") or die $!;
	my %grouped_motifs;
	while(<$motif_fh>){
		$_ =~ s/\n//;
		if($_ =~ /motifString/i){next}
		my @split;
		if($delimiter eq "\t"){
			@split = split/$delimiter/, $_;
		}elsif($delimiter eq ","){
			@split = parse_line(q{,}, 0, $_);
		}else{
			die "delimiter \"$delimiter\" not recognized!\n";
		}
		my (
			$motifString,
			$centerPos,
			$modificationType,
			$mod_fraction,
			$modified,
			$ngenome,
			$groupTag,
			$partnerMotifString,
			$meanscore,
			$meanipdRatio,
			$meanCoverage,
			$objective_score,
			$new_objective_score
		) = @split;
		# replace modified_base with "?"
		$modificationType =~ s/modified_base/\?/;
		#reformat numbers
		$mod_fraction = sprintf("%.1f",($mod_fraction*100))."%";
		$meanscore = sprintf("%.1f",$meanscore);
		$meanipdRatio = sprintf("%.1f",$meanipdRatio);
		$meanCoverage = sprintf("%.1f",$meanCoverage);
		$objective_score = sprintf("%.0f",$objective_score);
		my $motif_name = lc($motifString);
		my @motif_name = split //, $motif_name;
		$motif_name[$centerPos] = uc($motif_name[$centerPos]);
		$motif_name = join("", @motif_name);	
		%{$grouped_motifs{$groupTag}{$motif_name}} = (
			motifString => $motifString,
			centerPos => $centerPos,
			modificationType => $modificationType,
			mod_fraction => $mod_fraction,
			modified => $modified,
			ngenome => $ngenome,
			groupTag => $groupTag,
			partnerMotifString => $partnerMotifString,
			meanscore => $meanscore,
			meanipdRatio => $meanipdRatio,
			meanCoverage => $meanCoverage,
			objective_score => $objective_score,
			new_objective_score => $new_objective_score,
		);		
	}
	# Fix motif group names (e.g. GATC -> gATc)
	foreach my $groupTag(keys %grouped_motifs){

		# For each motif group need to know 
		my $motif; # Base string to use for group name (e.g. GATC)
		my @motif_modification_positions; # Positions to capitalize (e.g. 1,-1 for gATc)

		my @motifs_in_group = keys(%{$grouped_motifs{$groupTag}});
		my $number_of_motifs_in_group = scalar(@motifs_in_group);
		
		# Single motif in group (Type II e.g. gATc, or Type III e.g. cAtag)
		if($number_of_motifs_in_group == 1){
			($motif) = @motifs_in_group;
			my $mod_pos = $grouped_motifs{$groupTag}{$motif}{centerPos};
			push(@motif_modification_positions, $mod_pos);

			# If palindromic (type II like), eg GATC, rename groupTag to gATc
			my $rc_motif = rc($motif);
			if(lc($motif) eq lc($rc_motif)){
				my $rc_mod_pos = -1*(1+$mod_pos); 
				push(@motif_modification_positions, $rc_mod_pos);
			}
		}

		# Pair of motifs in group (Type I. e.g. cAgnnnnTtc)
		elsif($number_of_motifs_in_group == 2){
			my @sorted_motifs = sort(@motifs_in_group);
			$motif = shift(@sorted_motifs);
			my $mod_pos = $grouped_motifs{$groupTag}{$motif}{centerPos};
			push(@motif_modification_positions, $mod_pos);	
			my $other_motif = shift(@sorted_motifs);
			my $other_mod_pos = $grouped_motifs{$groupTag}{$other_motif}{centerPos}; 
			$other_mod_pos = -1*(1+$other_mod_pos); 
			push(@motif_modification_positions, $other_mod_pos);
		}

		# Create the formatted grouptag
		my $new_groupTag = caps($motif, \@motif_modification_positions);
		
		# Edit hash keys
		$grouped_motifs{$new_groupTag} = delete $grouped_motifs{$groupTag};
	}
	return(%grouped_motifs); 
}

sub rc{
        my ($motif) = @_;
        my $reverse_motif = reverse($motif);
        my $rc_motif = $reverse_motif;
        $rc_motif =~ tr/acgtACGTKMRYSWBVHDN/tgcaTGCAMKYRSWVBDHN/;
        return($rc_motif);
}

sub caps{
	my($motifString, $mod_pos) = @_;
	my $motif_name = lc($motifString);
	my @motif_name = split //, $motif_name;
	foreach my $mod_pos(@{$mod_pos}){
		$motif_name[$mod_pos] = uc($motif_name[$mod_pos]);	
	}$motif_name = join("", @motif_name);	
	return($motif_name);
}

