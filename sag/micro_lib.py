#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Microbial functions library
- supports sag, iso, ce, sps, sag_decontam

2016-12-15:
- base count for save_asm_statswas using contig_bp, but changed to scaffold_bp.  Scaffold_bp counts N's in the reads, contig_bp does not.

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import re
import sys
import glob
import xml.etree.ElementTree as ET

# custom libs in "../lib/"
my_dir = os.path.dirname(__file__)

sys.path.append(os.path.join(my_dir, '../lib'))

from common import run_cmd, checkpoint_step, append_rqc_file, append_rqc_stats
from curl3 import Curl, CurlHttpException


RQC_URL = "https://rqc.jgi-psf.org"
#RQC_URL = "https://rqc-1.jgi-psf.org"
msg_ok = "[  \033[1;32mOK\033[m  ]"
msg_fail = "[ \033[1;31mFAIL\033[m ]"
msg_warn = "[ \033[1;33mWARN\033[m ]"

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions


'''
read in config file into config dictionary
'''
def load_config_file(config_file, log):

    config = {}
    if os.path.isfile(config_file):
        fh = open(config_file, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            if line:
                arr = line.split("=")
                config_key = str(arr[0]).strip().lower()
                config_val = "=".join(arr[1:]).strip()

                if str(config_val).isdigit():
                    config_val = int(config_val)
                elif str(config_val).replace(".","").isdigit():
                    config_val = float(config_val)

                if config_key:
                    config[config_key] = config_val

        fh.close()
    else:
        log.error("- no config file: %s  %s", config_file, msg_fail)


    return config


'''
Rename rqc-files, rqc-stats to *.tmp (only if run previously)
- anything else?
'''
def setup_run(output_path, log):
    log.info("setup_run")

    # rename rqc-files.txt & rqc-stats.txt to *.tmp
    txt_list = glob.glob(os.path.join(output_path, "rqc-*s.txt*"))
    for txt_file in txt_list:

        #tmp_file = txt_file.replace(".txt", ".tmp")
        tmp_file = txt_file
        if tmp_file.find("-files.") > -1:
            tmp_file = os.path.join(output_path, "rqc-files.tmp")
        elif tmp_file.find("-stats.") > -1:
            tmp_file = os.path.join(output_path, "rqc-stats.tmp")
        cmd = "mv %s %s" % (txt_file, tmp_file)

        std_out, std_err, exit_code = run_cmd(cmd, log)

    # maybe change output path in files.tmp?


'''
Combine the output_path and path_name into output_path/path_name
- create if it doesn't exist
'''
def get_the_path(path_name, output_path):

    the_path = os.path.join(output_path, path_name)
    if not os.path.isdir(the_path):
        os.makedirs(the_path)
    return the_path


'''
skip the step if config[step_name_skip] exists and there is an "input_file" variable
- input file needs to be not null, don't care if its on disk or not.  This is if there is no screened assembly it will skip the screened sections
'''
def check_skip(step_name, input_file, config, status_log, log):

    skip_flag = False
    if "%s_skip" % step_name in config:

        log.info("- skipping %s per config setting", step_name)

        status = "%s skipped" % step_name
        checkpoint_step(status_log, status)

        skip_flag = True

    if not input_file:
        log.info("- skipping %s because input file is empty  %s", step_name, msg_warn)
        status = "%s skipped" % step_name
        checkpoint_step(status_log, status)

        skip_flag = True

    return skip_flag

'''
Helper function to make 2 stats files for the input fasta
- works on fastqs also
RQC-1121: missing for format=3 (tab sep values) - fixed
$ shifter -i bryce911/bbtools stats.sh extended=t format=3 in=contigs.trim.fasta
logsum = sum(length * log(length)) = measure of completeness comparing metagenomes
powersum = sum(length * length ^ P) = another measure of completeness for metagenomes (P = 0.3333?)
assembly score = sum(scores) where score = 1 if length < 1kb, 2 if length between 1kb and 2kb ... for isolates

'''
def make_asm_stats(fasta, stats_key, my_output_path, rqc_file_log, log):
    log.info("make_asm_stats: %s", fasta)

    ext = ".fasta"
    if fasta.endswith(".fa"):
        ext = ".fa"
    elif fasta.endswith(".fna"):
        ext = ".fna"
    elif fasta.endswith(".fastq"):
        ext = ".fastq"
    elif fasta.endswith(".fastq.gz"):
        ext = ".fastq.gz"


    stats_txt = os.path.join(my_output_path, os.path.basename(fasta).replace(ext, ".txt"))
    stats_txt_err = os.path.join(my_output_path, os.path.basename(fasta).replace(ext, ".txt.err"))
    stats_tsv = os.path.join(my_output_path, os.path.basename(fasta).replace(ext, ".tsv"))
    stats_tsv_err = os.path.join(my_output_path, os.path.basename(fasta).replace(ext, ".tsv.err"))

    if os.path.isfile(fasta):

        if os.path.isfile(stats_txt):
            log.info("- skipping %s, file exists", stats_txt)
        else:

            if fasta.endswith(".fastq") or fasta.endswith(".fastq.gz"):
                log.info("- skipping %s, file is a fastq", stats_txt)
                # don't need this for fastqs
            else:
                cmd = "#bbtools;stats.sh in=%s format=2 > %s 2> %s"  % (fasta, stats_txt, stats_txt_err)
                std_out, std_err, exit_code = run_cmd(cmd, log)

                append_rqc_file(rqc_file_log, stats_key+"_txt", stats_txt)


        if os.path.isfile(stats_tsv):
            log.info("- skipping %s, file exists", stats_tsv)
        else:
            
            cmd = "#bbtools;stats.sh in=%s format=3 extended=t > %s 2> %s"  % (fasta, stats_tsv, stats_tsv_err)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            append_rqc_file(rqc_file_log, stats_key+"_tsv", stats_tsv)

    else:
        log.error("- cannot find %s!  %s", fasta, msg_fail)


    return stats_txt, stats_tsv


'''
Harvest the stats based on the fasta basename
associate fasta_type with the stats
'''
def save_asm_stats(tsv_file, fasta_type, rqc_stats_log, log):

    if os.path.isfile(tsv_file):

        fh = open(tsv_file, "r")
        line1 = None
        line2 = None

        for line in fh:

            if line.startswith("n_scaffolds"):
                line1 = line.strip()
            else:
                line2 = line.strip()
        fh.close()

        # split both lines of .tsv
        arr1 = []
        arr2 = []
        if line1:
            arr1 = line1.split("\t")
        if line2:
            arr2 = line2.split("\t")

        scaf_bp = 0 # needed for Coverage calc
        cnt = 0 # column count
        for a in arr1:

            my_key = "%s_%s" % (fasta_type, a)

            if fasta_type.endswith("fastq"):
                # fastq, only save read count and base count
                # using n_contigs [1], contig_bp [3], scaffold_bp [2] = count including N's, contig_bp does not include N's
                if cnt == 1 or cnt == 2:
                    my_key = "%s_read_count" % fasta_type
                    if cnt == 2:
                        my_key = "%s_base_count" % fasta_type

                    log.info("-- %s = %s", my_key, arr2[cnt])
                    append_rqc_stats(rqc_stats_log, my_key, arr2[cnt])

            else:
                log.info("-- %s = %s", my_key, arr2[cnt])

                append_rqc_stats(rqc_stats_log, my_key, arr2[cnt])
                if a == "scaf_bp":
                    scaf_bp = arr2[cnt]

            cnt += 1

    # only for fasta, not fastq
    if not fasta_type.endswith("fastq"):

        # coverage depth = scaf_bp / bases input to assembly

        input_bases = 0 # subsampled bases
        fh = open(rqc_stats_log, "r")
        for line in fh:
            if line.startswith("subsampled_fastq_base_count"):
                arr = line.split("=")
                input_bases = int(str(arr[1]).strip())
                break
        fh.close()

        cov = 0.0
        if input_bases > 0 and float(scaf_bp) > 0:

            cov = input_bases / float(scaf_bp)

        my_key = "%s_coverage_x" % (fasta_type)
        append_rqc_stats(rqc_stats_log, my_key, cov)



'''
Get library info for the seq unit
- use RQC api, if timeout exit
'''
def get_library_info(library_name, log):

    log.info("get_library_info: %s", library_name)

    # Look up spid and organism name
    lib_info = {}


    rqc_library_api = "%s/%s;" % ("api/library/info", library_name) # ';' is important in the api
    response = get_web_page(RQC_URL, rqc_library_api, log)

    if response:
        #print response
        if "library_info" in response:
            lib_info = response['library_info']

            #seq_proj_id = lib_info.get('seq_proj_id')

    return lib_info

'''
Get the info for a seq unit
- handles raw or filtered seq units
'''
def get_seq_unit_info(seq_unit_list, log):
    log.info("get_seq_unit_info: %s", seq_unit_list)

    seq_unit_info = {}
    rqc_seq_unit_api = "api/rqcws/seq_unit_data"  #%s/%s" % ("api/rqcws/seq_unit_info", seq_unit_name)
    #rqc_seq_unit_api += "/".join(seq_unit_list)

    for su in seq_unit_list:
        rqc_seq_unit_api += "/" + os.path.basename(su)

    response = get_web_page(RQC_URL, rqc_seq_unit_api, log)

    if response:
        seq_unit_info = response

    return seq_unit_info


'''
Put all curl get calls into one function
'''
def get_web_page(my_url, my_api, log = None):

    response = None

    if my_url and my_api:

        curl = Curl(my_url)

        try:
            response = curl.get(my_api)
        except CurlHttpException as e:
            if log:
                log.error("- Failed API Call: %s/%s, %s: %s  %s", my_url, my_api, e.code, e.response, msg_fail)
            else:
                print "- Failed API Call: %s/%s, %s: %s  %s" % (my_url, my_api, e.code, e.response, msg_fail)
            #code = e.code


            response = e.response
            sys.exit(1)

        except Exception as e:
            if log:
                log.error("- Failed API Call: %s/%s, %s  %s", my_url, my_api, e.args, msg_fail)
            else:
                print "- Failed API Call: %s/%s, %s  %s" % (my_url, my_api, e.args, msg_fail)

            sys.exit(1)

    return response


# use a txt file to define where files are, can overwrite ?
# save_file(fs, type)
# load_file(fs, type)

'''
Text file with list of key : value pairs
- appends to file, can add same file more than once
- used to store intermediate output files (input fastq -> normalized -> subsampled, etc)
'''
def save_file(my_file, file_type, file_dict, output_path):

    # set fs_location t relative to output_path
    if file_type != "input_fastq":
        my_file = my_file.replace(output_path, "")

        if my_file.startswith("/"):
            my_file = my_file[1:]

    file_list = os.path.join(output_path, "file_list.txt")
    fh = open(file_list, "a")
    fh.write("%s = %s\n" % (file_type, my_file))
    fh.close()

    file_dict[file_type] = my_file


'''
returns fs_location for the file_type
- not for output files, only for fastq (input, normalized, subsampled) and fasta (asm, trim, decontam)
'''
def get_file(file_type, file_dict, output_path, log):

    my_file = None

    if file_type in file_dict:
        my_file = file_dict[file_type]

    else:

        file_list = os.path.join(output_path, "file_list.txt")

        fh = open(file_list, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue

            arr = line.split("=")
            file_key = None
            the_file = None
            if len(arr) > 1:
                file_key = str(arr[0]).strip()
                the_file = str(arr[1]).strip()

            # yes, this might read the same file multiple times but it returns the last one in the file
            if file_key == file_type:
                my_file = the_file

        fh.close()

        file_dict[file_type] = my_file



    # add the current path unless its the input fastq
    if file_type != "input_fastq":
        if my_file:
            if my_file.startswith("/"):
                my_file = my_file[1:]

            my_file = os.path.join(output_path, my_file)


    if my_file:

        # if .gz, but not gzipped remove gzip
        # if not gzipped but .gz, add gz

        if not os.path.isfile(my_file):
            if my_file.endswith(".gz"):
                my_file = my_file.replace(".gz", "")
            else:
                my_file += ".gz"


        if os.path.isfile(my_file):
            # if empty file (< 50 bytes) then return None (or not on disk)
            if os.path.getsize(my_file) < 50:
                log.error("- file too small: %s  %s", my_file, msg_fail)
                my_file = None
        else:

            log.error("- file not on file system: %s  %s", my_file, msg_fail)
            my_file = None

    return my_file


'''
get 16s SSU (small sub unit)
https://rqc.jgi-psf.org/api/library/ssu/BSOSB
# BSOSB: >1119129_Streptomyces_sp__3211_6TGCAGT
Streptomyces sp. 3211.6
>(spid)_(seq_proj_name)
'''
def get_ssu(library_name, output_fasta, log = None):

    rqc_ssu_api = "%s/%s" % ("api/library/ssu", library_name)
    if log:
        log.info("- ssu web: %s/%s", RQC_URL, rqc_ssu_api)
        
    response = get_web_page(RQC_URL, rqc_ssu_api, log)

    # always write a ssu file, even if empty
    fh = open(output_fasta, "w")

    if response:
        if 'ssu' in response:

            # clean up SSU - not sure if this is needed
            if response['ssu']:
                ssu = re.sub(r'[^A-Za-z0-9\_\-\>\n]+', '_', response['ssu'])
                
                if log:
                    log.info("- ssu: %s chars", len(ssu))

                fh.write(ssu)
            else:
                if log:
                    log.info("- ssu is empty  %s", msg_warn)
        else:
            if log:
                log.info("- no ssu in web service  %s", msg_warn)


    fh.write("\n")
    fh.close()


## -----
## reporting helpers

'''
NCBI prescreen section in txt report
'''
def ncbi_prescreen_report(ncbi_file, ncbi_cnt, log):
    log.info("ncbi_prescreen_report: %s, hits = %s", ncbi_file, ncbi_cnt)

    prescreen_report = "** No potential contaminants found.\n"

    if int(ncbi_cnt) > 0:

        if os.path.isfile(ncbi_file):
            prescreen_report = "Query\tSubject\tAlign_Length\tPercent_id\n"

            fh = open(ncbi_file, "r")
            for line in fh:
                if line.startswith("#"):
                    pass
                    # header line
                else:
                    arr = line.split()
                    if len(arr) > 2:
                        prescreen_report += "%s\t%s\t%s\t%s\n" % (arr[0], arr[1], arr[3], arr[2])
            fh.close()
        else:
            log.error("- cannot find file: %s  %s", ncbi_file, msg_fail)

    return prescreen_report


'''
Contam summary information for pdf report & txt report
* fastq_read_count == 0 error checking to do

'''
def contam_summary_pdf(contam_summary_file, fastq_read_count, log):
    log.info("contam_summary_pdf: %s, read_count = %s", contam_summary_file, fastq_read_count)

    contam_sum = "" # contam summary table
    contam_id = "" # contam id table

    contam_txt = "" # contam summary for text report

    if os.path.isfile(contam_summary_file):

        #contam_id = "\\vspace{5mm}\\\\\n"
        #contam_id += "List of Contaminants Identified\n"
        #contam_id += "\\vspace{5mm}\\\\\n"

        contam_id += "\\begin{tabular}{|l|r|r|} \\hline\n"
        contam_id += "Description & Number of Reads & Percent of Reads \\tabularnewline \\hline\n"

        contam_id += "Input & %s & %s \\tabularnewline \\hline\n" % ("{:,}".format(int(fastq_read_count)), "100.00\\%")


        ttl_contam = 0 # number of reads identified as contam
        fh = open(contam_summary_file, "r")
        for line in fh:
            if line.startswith("#"):
                continue
            arr = line.strip().split("|")

            pct_of_reads =  format_val_pct(int(arr[1]) / float(fastq_read_count))
            contam_txt += "- %s: %s reads  (%s)\n" % (arr[0], "{:,}".format(int(arr[1])), pct_of_reads)

            pct_of_reads = str(pct_of_reads).replace("%", "\\%")
            contam_id += "%s & %s & %s \\tabularnewline \\hline\n" % (arr[0], "{:,}".format(int(arr[1])), pct_of_reads)
            ttl_contam += int(arr[1])


        fh.close()

        pct_of_reads =  format_val_pct(ttl_contam / float(fastq_read_count))
        contam_txt += "Total: %s  (%s)\n" % ("{:,}".format(ttl_contam), pct_of_reads)

        pct_of_reads = str(pct_of_reads).replace("%", "\\%")
        contam_id += "Total Contamination & %s & %s \\tabularnewline \\hline\n" % ("{:,}".format(ttl_contam), pct_of_reads)

        contam_id += "\\end{tabular}\\\\\n"



        # this is stupid because its in the previous table but ...
        contam_sum = "\\begin{tabular}{|l|r|r|} \\hline\n"
        contam_sum += "Description & Number of Reads & Percent of Reads \\tabularnewline \\hline\n"
        contam_sum += "Input & %s & %s \\tabularnewline \\hline\n" % ("{:,}".format(int(fastq_read_count)), "100.00\\%")
        contam_sum += "Contamination & %s & %s \\tabularnewline \\hline\n" % ("{:,}".format(ttl_contam), pct_of_reads)
        contam_sum += "\end{tabular}\\\\\n"

        if ttl_contam == 0:
            contam_id = "No contaminates found in the input data set.\\\n"
            #contam_id = "\n"
            contam_sum = "No contaminates found in the input data set.\\\n"

        # 2016-06-23: Alex C recommended to not include contam_sum in the report

    else:
        log.error("- cannot find file: %s  %s", contam_summary_file, msg_fail)
        contam_id = "No contaminates found in the input data set.\\\n"
        contam_sum = "No contaminates found in the input data set.\\\n"

    return contam_sum, contam_id, contam_txt


'''
Greengenes results
- was using green genes db, now only use Silva SSU

#query  subject expect  length  %id     q_length        s_length
NODE_45_length_3242_cov_60.8935_ID_34979        FM874383.1.1445 Bacteria;Proteobacteria;Alphaproteobacteria;Rhodospirillales;Acetobacteraceae;uncultured;uncultured bacterium  0.0     1445    99.86   2842    1445

'''
def green_genes_pdf(green_genes_file, log):
    log.info("green_genes_pdf: %s", green_genes_file)

    green_genes_table = ""

    if green_genes_file and os.path.isfile(green_genes_file):
        fh = open(green_genes_file, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                pass
            else:
                
                arr = line.split()
                #arr = line.split("\t")
                #if len(arr) == 1:
                #    arr = line.split() # new megablast format
                org_name = str(arr[1])
                if org_name.startswith("tid|"):
                    arr2 = org_name.split("|")
                    org_name = "N/A"
                    tid = str(arr2[1])
                    
                    if tid.isdigit():
                        t_dict = get_taxonomy_by_id(tid)
                        #print t_dict
                        # look up in tax server to be correctly formatted
                        for t in ['tax_kingdom', 'tax_phylum', 'tax_class', 'tax_order', 'tax_family', 'tax_genus', 'tax_species']:
                            if t in t_dict:
                                org_name += t_dict[t] + ";"
                    
                    
                green_genes_table += "Organism: %s\\\\\n" % (trans_latex(str(org_name).replace("_", " ")))  # replace _ with " " so it splits nicely in the report (sometimes too long)
                green_genes_table += "Contig Name: %s\\\\\n" % (trans_latex(arr[0]))
                green_genes_table += "Align Length: %s bp\\\\\n" % ("{:,}".format(int(arr[3])))
                green_genes_table += "Percent Id: %s\\\\\n" % (trans_latex(format_val_pct(arr[4])))
                green_genes_table += "\\vspace{3mm}\\\\\n"

        fh.close()

    return green_genes_table


'''
convert chars to latex chars
'''
def trans_latex(my_string):


    the_string = str(my_string)
    # add \ to beginning
    trans_dict = {
        #"\\" : "textbackslash ", # chr(92)
        "$" : "$",
        "%" : "%",
        "_" : "_",
        "{" : "{",
        "}" : "}",
        "&" : "&",
        "#" : "#",
        "^" : "textasciicircum",
        "~" : "textasciitilde",
        "*" : "textasteriskcentered ",
        "/" : "slash ",
        "|" : "textbar ",
        "-" : "textendash ",
        ">" : "textgreater ",
        "<" : "textless ",
    }
    
    if the_string.endswith(".jpg") or the_string.endswith(".png") or the_string.endswith(".pdf"):
        # or .txt or .tsv but we aren't imbedding these in the pdfs
        pass

    else:
        # first replace \, then replace everything else
        the_string = the_string.replace("\\", "%stextbackslash " % chr(92))

        for t in trans_dict:
            the_string = the_string.replace(t, "%s%s" % (chr(92), trans_dict[t]))

    return the_string

'''
Function to add values to the output file from merge_dict
returns number of missing fields or if template is missing (err_cnt)
'''
def merge_template(template_file, output_file, merge_dict, log):

    log.info("merge_template: %s, %s", os.path.basename(template_file), os.path.basename(output_file))
    err_cnt = 0

    sep1 = "[:"
    sep2 = ":]"
    search = r"\[:(.*?):\]" # regex to look for all occurences of [:key_name:] in the template

    merge_mode = "text"

    # treat pdf (latex) differently
    if output_file.endswith(".tex"):
        merge_mode = "latex"

    if os.path.isfile(template_file):

        fhw = open(output_file, "w")
        fh = open(template_file, "r")
        missing_list = [] # missing keys

        for line in fh:
            if line.strip().startswith("#"):
                continue

            arr = re.findall(search, line)
            if len(arr) > 0:
                for a in arr:

                    my_key = "%s%s%s" % (sep1, a, sep2)
                    my_val = "MISSING" # avoid these
                    if a in merge_dict:
                        my_val = str(merge_dict[a])

                        if merge_mode == "latex":
                            if a.endswith("_table"):
                                pass
                            else:
                                my_val = trans_latex(my_val)

                    else:
                        missing_list.append(a)

                    #print "my_key = %s, my_val = %s" % my_key, my_val

                    line = line.replace(my_key, my_val)

            fhw.write(line)
        fhw.close()
        fh.close()

        log.info("- wrote file: %s", output_file)

        # record keys that are missing
        missing_txt = "%s.missing.log" % (output_file)
        if len(missing_list) > 0:

            fh = open(missing_txt, "w")
            fh.write("Missing keys for %s:\n" % output_file)
            for i in missing_list:
                fh.write("- '%s'\n" % i)
                err_cnt += 1
            fh.close()
            log.warn("- missing %s keys in %s: %s  %s", len(missing_list), output_file, ", ".join(missing_list), msg_warn)

        else:
            # remove the file if last time we were missing something but now we are not
            if os.path.isfile(missing_txt):
                cmd = "rm %s" % missing_txt
                std_out, std_err, exit_code = run_cmd(cmd, log)


    else:
        log.error("- cannot find template: %s  %s", template_file, msg_fail)
        err_cnt = 1

    return err_cnt

## -----


'''
Extract version from the path
module load bbtools;bbversion.sh
37.90
bbversion.sh
'''
def get_bbtools_version(rqc_stats_log, output_path, log):
    log.info("get_bbtools_version")

    bbtools_log = os.path.join(output_path, "bbtools.txt")

    cmd = "#bbtools;bbversion.sh > %s 2>&1"  % (bbtools_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    
    bbtools_version = None
    fh = open(bbtools_log, "r")
    for line in fh:
        bbtools_version = line.strip()
        
    fh.close()

    log.info("- bbtools version: %s", bbtools_version)
    append_rqc_stats(rqc_stats_log, "bbtools_version", bbtools_version)

    return bbtools_version


'''
Don't trust module version listed
- called from do_assembly

This works too:
$ spades.py --version

'''
def get_spades_version(rqc_stats_log, config, output_path, log):
    log.info("get_spades_version")

    spades_log = os.path.join(output_path, "spades.txt")
    #  --version
    cmd = "#%s;spades.py > %s 2>&1"  % (config['spades_module'], spades_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    spades_version = "unknown"

    fh = open(spades_log, "r")
    for line in fh:
        if line.startswith("SPAdes genome ass"):
            spades_version = line.split()[-1]
    fh.close()


    log.info("- spades_version: %s", spades_version)
    append_rqc_stats(rqc_stats_log, "spades_version", spades_version)


"""
$ /projectb/sandbox/rqc/prod/pipelines/external_tools/taxMapper/DEFAULT/lookupTax.pl 'felis catus'
#- if nothing returned use
$ /projectb/sandbox/rqc/prod/pipelines/external_tools/taxMapper/DEFAULT/lookupTax.pl -taxid [ncbi_tax_id]

KINGDOM=Eukaryota
PHYLUM=Chordata
CLASS=Mammalia
ORDER=Carnivora
FAMILY=Felidae
GENUS=Felis
SPECIES=Felis catus
* use new taxonomy server instead - used in sag, try to depricate this

"""
def get_taxonomy(org_name, log):

    log.info("get_taxonomy: %s", org_name)

    tax_dict = { "tax_kingdom" : "", "tax_phylum" : "", "tax_class" : "", "tax_order" : "", "tax_family" : "", "tax_genus" : "", "tax_species" : "" }

    # should be config
    cmd = "/projectb/sandbox/rqc/prod/pipelines/external_tools/taxMapper/DEFAULT/lookupTax.pl"
    if org_name.isdigit():
        cmd += " -taxid %s" % org_name
    else:
        cmd += " '%s'" % (org_name)


    std_out, std_err, exit_code = run_cmd(cmd, log)


    for line in std_out.split("\n"):
        #print line
        arr = line.split("=")
        if arr[0]:
            k = "tax_%s" % (arr[0].lower().strip())
            v = arr[1].strip()
            if k in tax_dict:
                tax_dict[k] = v

    return tax_dict

'''
Use taxonomy web service to get taxonomy
'''
def get_taxonomy_by_id(my_tax_id, log = None):
    if log:
        log.info("get_taxonomy_by_id: %s", my_tax_id)

    tax_dict = {
        "tax_kingdom" : "", # aka kingdom/superkingdom
        "tax_phylum" : "",
        "tax_class" : "",
        "tax_order" : "",
        "tax_family" : "",
        "tax_genus" : "",
        "tax_species" : ""
    }

    if my_tax_id > 0:

        my_tax_id = str(my_tax_id)

        tax_url = "https://taxonomy.jgi-psf.org"
        tax_api = "tax/taxid/[tax_id]"
        tax_api = tax_api.replace("[tax_id]", my_tax_id)
        response = get_web_page(tax_url, tax_api, log)

        for tax_type in ['kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']: #tax_dict:
            if tax_type in response[my_tax_id]:
                tax_key = "tax_%s" % tax_type
                tax_dict[tax_key] = str(response[my_tax_id][tax_type]['name'])

        #if tax_dict['tax_kingdom'] == "":
        if 1==1:
            if "superkingdom" in response[my_tax_id]:
                tax_dict['tax_kingdom'] = str(response[my_tax_id]['superkingdom']['name'])
            # just superkingdom, kingdom is something else
            #if "kingdom" in response[my_tax_id]:
            #    tax_dict['domain'] = response[my_tax_id]['kingdom']['name']

    if log:
        log.info("- k: %s, p: %s, c: %s, o: %s, f: %s, g: %s, s: %s", tax_dict['tax_kingdom'], tax_dict['tax_phylum'], tax_dict['tax_class'],
                 tax_dict['tax_order'], tax_dict['tax_family'], tax_dict['tax_genus'], tax_dict['tax_species'])
    else:
        print "Taxonomy for: %s" % my_tax_id
        print "- k: %s, p: %s, c: %s, o: %s, f: %s, g: %s, s: %s" % (tax_dict['tax_kingdom'], tax_dict['tax_phylum'], tax_dict['tax_class'],
                 tax_dict['tax_order'], tax_dict['tax_family'], tax_dict['tax_genus'], tax_dict['tax_species'])
    return tax_dict


'''
convert a value to a percent
d = digits, default 2
- returns unformatted value if its not able to convert to a number
'''
def format_val_pct(my_val, d = 2):

    # 1.23363277713e-07
    my_new_val = re.sub(r'[^0-9e\.\-]', '', str(my_val))

    if not my_new_val:
        return my_val

    my_new_val = float(my_new_val)
    if my_new_val > 1.0:
        my_new_val = my_new_val / 100.0

    format_style = "{:.%s%%}" % d
    my_pct_val = format_style.format(float(my_new_val))

    return my_pct_val


'''
Read the .tsv file from stats.sh and return the contigs and base count
'''
def get_base_contig_cnt(asm_tsv):
    base_cnt = 0
    contig_cnt = 0

    if os.path.isfile(asm_tsv):
        cnt = 0
        fh = open(asm_tsv, "r")
        for line in fh:

            if cnt == 1:

                arr = line.split("\t")
                
                if len(arr) > 4:
                    contig_cnt = int(arr[1]) # 1 = contigs, 0 = scaffolds
                    base_cnt = int(arr[3]) # 3 = contig bp, 2 = scaffold bp
            cnt += 1
        fh.close()

    return base_cnt, contig_cnt

'''
If checkm failed to create a summary file, use this dummy one
'''
def create_empty_checkm_summary(summary_file):
    fh = open(summary_file, "w")
    fh.write("#Filename  GenomeSize  FoundSCGenes  TotalSCGenes  EstGenomeComSimple  EstGenomeSizeSimple  EstGenomeComNormalized  EstGenomeSizeNormalized\n")
    fh.write("no_summary_created  0  0  0  0.0  0  0.0  0\n")
    fh.close()

'''
Save versions to a standard file (version.txt)
'''
def save_version(output_path, program_name, program_version):

    version_file = os.path.join(output_path, "version.txt")

    fh = open(version_file, "a")
    fh.write("%s=%s\n" % (program_name, program_version))
    fh.close()


'''
Load the /global/projectb/shared/software/smrtanalysis/2.3.0/current/analysis/etc/algorithm_parameters/2014-09/mapping.xml
look at the metadata.xml file to determine the mapping: BindingKit.PartNumber and SequencingKit.PartNumber do it
- taken from /global/projectb/shared/software/smrtanalysis/2.2.0/current/analysis/bin/makeChemistryMapping.py

'''
def get_smrtcell_chemistry(metadata_xml, mapping_xml, log = None):

    if log:
        log.info("get_smrtcell_chemistry: %s", metadata_xml)

    smrtcell_chem = None

    # Namespace for parsing *.metadata.xml
    nsd = {None: 'http://pacificbiosciences.com/PAP/Metadata.xsd', "pb": 'http://pacificbiosciences.com/PAP/Metadata.xsd'}

    if log:
        log.info("- mapping_xml: %s", mapping_xml)

    if not os.path.isfile(mapping_xml):
        if log:
            log.error("- mapping_xml does not exist: %s", mapping_xml)
        return smrtcell_chem

    if not os.path.isfile(metadata_xml):
        if log:
            log.error("- metadata.xml does not exist: %s", metadata_xml)
        return smrtcell_chem

    my_tree = ET.parse(metadata_xml)
    my_root = my_tree.getroot()


    my_binding_kit = my_root.find("pb:BindingKit/pb:PartNumber", namespaces=nsd).text
    my_sequencing_kit = my_root.find("pb:SequencingKit/pb:PartNumber", namespaces=nsd).text

    #print "binding = %s, sequencing = %s" % (my_binding_kit, my_sequencing_kit)

    # parse mapping_xml to determine the chemistry
    chem = None
    binding_kit = None
    sequencing_kit = None
    chem_dict = {}

    fh = open(mapping_xml, "r")

    for line in fh:
        line = line.strip()
        if line == "<Mapping>":

            binding_kit = None
            sequencing_kit = None
            chem = None

        elif line == "</Mapping>":

            #print "- b=%s, s = %s --> %s" % (binding_kit, sequencing_kit, chem)
            key = "%s::%s" % (binding_kit, sequencing_kit)
            chem_dict[key] = chem

        elif line.startswith("<SequencingChemistry>"):
            chem = re.sub("<(.*?)>", "", line)
        elif line.startswith("<BindingKit>"):
            binding_kit = re.sub("<(.*?)>", "", line)
        elif line.startswith("<SequencingKit>"):
            sequencing_kit = re.sub("<(.*?)>", "", line)



    fh.close()


    my_key = "%s::%s" % (my_binding_kit, my_sequencing_kit)
    if my_key in chem_dict:
        smrtcell_chem = chem_dict[my_key]
        if log:
            log.info("- chemistry for binding = %s, sequencing = %s: %s", my_binding_kit, my_sequencing_kit, smrtcell_chem)
    else:
        if log:
            log.error("- cannot determine chemistry for binding = %s, sequencing = %s", my_binding_kit, my_sequencing_kit)
        smrtcell_chem = "P6-C4" # defaulting to P6-C4

    return smrtcell_chem

'''
Return number of processors available to us 
'''
def get_nproc():
    nproc = 16
    
    # slurm env vars
    #SLURM_CPUS_PER_TASK=16
    #SLURM_CPUS_ON_NODE=16
    #SLURM_JOB_CPUS_PER_NODE=16
    #Actually, it turns out nproc returns the number of processors you are allowed to use in SLURM, so you don't need to parse anything. 
    cmd = "nproc"
    std_out, std_err, exit_code = run_cmd(cmd)
    nproc = int(std_out.strip())

    return nproc

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":

    print "Micro-lib!"

    #get_ssu("BSOSB", "/global/projectb/scratch/brycef/BSOSB-16s.fa")
    #get_ssu("CNOAS", "/global/projectb/scratch/brycef/CNOAS-16s.fa")
    #sys.exit(44)
    
    # "Felus Catus" of course
    print get_taxonomy_by_id(9685, None)

    print get_nproc()

    sys.exit(0)

