#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
AC-DC
- wrapper for ACDC decontamination code, decontaminates single cell fasta files (acdc.clean.fasta, acdc.contamination.fasta)
- for SAG products
- RQC-1083


Dependencies
kraken
acdc (automated contamination detection and confidence estimation for single cell genome data) - Markus Lux

--- Citation:
Lux M, Kruger J, Rinke C, Maus I, Schluter A, Woyke T, Sczyrba A, Hammer B.
acdc - Automated Contamination Detection and Confidence estimation for single-cell genome data.
BMC Bioinformatics. 2016 Dec 20;17(1):543. doi: 10.1186/s12859-016-1397-7.

v 1.0: 2018-05-23
- initial version

To do:
- need -V (version) and -S (random seed) for ACDC
- throw -V output into a file so sag.py can read it

tests = CABXS- should be dirty, 30 minutes (or cached might be quicker)
~/git/jgi-rqc-pipeline/sag/ac-dc.py -o t1

CAYXB - clean, acdc fails
~/git/jgi-rqc-pipeline/sag/ac-dc.py -o t2

mlux86/acdc:stable -V

kraken: root;cellular organisms;bacteria;proteobacteria;betaproteobacteria;burkholderiales;comamonadaceae
- needs root;cellular organisms; appended
RQC: Bacteria;Proteobacteria;Betaproteobacteria;Burkholderiales;Comamonadaceae

failing: 2018-05-07, probably need newer acdc container
/global/u1/b/brycef/git/jgi-rqc-pipeline/sag/ac-dc.py -l CABXZ -f /global/projectb/scratch/brycef/sag/CABXZ/pool_asm/pool_decontam.fasta -o /global/projectb/scratch/brycef/sag/CABXZ/acdc

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import getpass
import glob

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, run_cmd, get_colors, get_msg_settings
from micro_lib import get_web_page


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions


'''
22 minutes
$ time shifter --image=bryce911/kraken kraken \
--db /global/projectb/sandbox/rqc/prod/kraken/minikraken_20171019_8GB/ \
--output CABXS.kraken \
/global/dna/shared/rqc/pipelines/sag_pool/archive/02/97/82/72/trim/scaffolds.trim.fasta


'''
def create_kraken_file(fasta):
    log.info("create_kraken_file: %s", fasta)

    if os.path.isfile(KRAKEN_FILE):
        log.info("- skipping create_kraken_file, found file: %s  %s", KRAKEN_FILE, msg_warn)
    else:

        kraken_log = os.path.join(output_path, "kraken.log")
        cmd = "#kraken;kraken --db %s --output %s %s > %s 2>&1" % (KRAKEN_DB, KRAKEN_FILE, fasta, kraken_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

'''
# 15 sec
$ time shifter --image=bryce911/kraken kraken-translate  \
--db /global/projectb/sandbox/rqc/prod/kraken/minikraken_20171019_8GB/  \
CABXS.kraken > CABXS.kraken.tax

'''
def translate_kraken():
    log.info("translate_kraken")

    if os.path.isfile(KRAKEN_TAX_FILE):
        log.info("- skipping translate_kraken, found file: %s  %s", KRAKEN_TAX_FILE, msg_warn)
    else:

        cmd = "#kraken;kraken-translate --db %s %s > %s" % (KRAKEN_DB, KRAKEN_FILE, KRAKEN_TAX_FILE)
        std_out, std_err, exit_code = run_cmd(cmd, log)

'''
$ time shifter --image=mlux86/acdc:stable acdc \
-K /global/projectb/scratch/andreopo/ACDC/KRAKEN/minikraken_20141208/ \
-i /global/dna/shared/rqc/pipelines/sag_pool/archive/02/97/82/72/trim/scaffolds.trim.fasta \
-x CABXS.kraken.tax  \
-X "Bacteria;Proteobacteria;Betaproteobacteria;Burkholderiales;Comamonadaceae" \
-b 10 -o /global/projectb/scratch/brycef/krak
7 minutes
-T = threads, normally auto-detects
'''
def run_acdc():
    log.info("run_acdc")

    out_file = os.path.join(output_path, "clusterinfo.js") # or the long_named_yaml_file ...
    if os.path.isfile(out_file):
        log.info("- skipping run_acdc, found file: %s  %s", out_file, msg_warn)

    else:
        #https://rqc-1.jgi-psf.org/api/rqcws/lib2su/cabxs

        target_tax = ""
        target_tax_id = 0

        rqc_api = "api/rqcws/lib2su/%s" % library_name

        response = get_web_page(RQC_URL, rqc_api, log)
        if 'tax' in response[0]:
            target_tax = response[0]['tax']
            target_tax_id = response[0]['tax_id']

        # RQC-1083 - taxonomy must match what is in kraken ... how am I supposed to know tax id -> kraken mapping??
        target_tax = "root;cellular organisms;%s" % target_tax.lower()

        log.info("- target taxonomy: %s", target_tax)
        log.info("- target tax id: %s", target_tax_id)

        if target_tax_id == 0:
            log.error("- RQC returned no taxonomy for library %s  %s", library_name, msg_fail)
            sys.exit(2)

        cmd = "#acdc;acdc -K %s -i %s -x %s -X \"%s\" -b 10 -R 42 -o %s" % (KRAKEN_DB, fasta, KRAKEN_TAX_FILE, target_tax, output_path)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        # -K = kraken db
        # -i = fasta
        # -x =file with taxonomy info
        # -X = target taxonomy
        # -b = bootsteps (10)
        # -R 42 = random seed to use (so results are always the same, otherwise different runs produce different results)

'''
quick
time shifter --image=mlux86/acdc:stable /usr/local/bin/acdc-make-fastas /global/projectb/scratch/brycef/krak/_global_dna_shared_rqc_pipelines_sag_pool_archive_02_97_82_72_trim_scaffolds.trim.fasta.yaml
returns what it considers contaminated fastas (contamination.names)
'''
def acdc_make_fasta():
    log.info("acdc_make_fasta")

    contam_state = ""

    yaml_list = glob.glob(os.path.join(output_path, "*.yaml"))
    if len(yaml_list) > 0:
        yaml_file = yaml_list[0]

        # look for "contamination_state: contaminated" in *.yaml file
        
        fh = open(yaml_file, "r")
        for line in fh:
            if line.strip().startswith("contamination_state:"):
                contam_state = line.split(":")[-1].strip()
        fh.close()

        log.info("- contamination state: %s", contam_state)

        # only run acdc-make-fastas if the contamination_state = "contaminated"        
        if contam_state.startswith("contam"):

            os.chdir(output_path) # need to put in this folder or it writes fasta to where the script ran from

            cmd = "#acdc;/usr/local/bin/acdc-make-fastas %s" % (yaml_file)
            std_out, std_err, exit_code = run_cmd(cmd, log)
    
            if exit_code > 0:
                # ACDC not fully stable ...
                sys.exit(exit_code)


    else:
        log.error("- no yaml output file!  %s", msg_fail)
        sys.exit(2)

    return contam_state

'''
Create a acdc.clean.fasta and a acdc.contam.fasta
based on acdc results

very quick, always recreate files - no
- just use the files created by ACDC
- the SAG pipeline does the contigs/bp reporting also
'''
def make_contam_fasta(fasta, contam_state):
    log.info("make_contam_fasta: %s", contam_state)


    # 2018-04-10 - instead of parsing the original fasta, use the clean.fasta and contamination.fasta as our output
    clean_fasta = os.path.join(output_path, "clean.fasta")
    contam_fasta = os.path.join(output_path, "contamination.fasta")

    if os.path.isfile(clean_fasta):
        log.info("- found clean fasta: %s  %s", clean_fasta, msg_ok)
    else:
        if contam_state.startswith("contam"):
            log.info("- clean fasta missing!  %s", msg_warn)
            sys.exit(2)
        else:
            # its clean, use input fasta as clean fasta
            cmd = "cp %s %s" % (fasta, clean_fasta)
            std_out, std_err, exit_code = run_cmd(cmd, log)
        

    if os.path.isfile(contam_fasta):
        log.info("- found contamination fasta: %s  %s", contam_fasta, msg_ok)

    else:
        # always create this file
        log.info("- contamination fasta missing!  %s", msg_warn)
        cmd = "touch %s" % (contam_fasta)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    return

# good code, but not used ... was using contamination.names to split the original fasta into a clean and contamination fasta
def make_contam_fasta_old():


    # get list of contaminated contigs to remove
    contam_list = []
    contam_names = os.path.join(output_path, "contamination.names") # list of contigs marked as contamination by acdc
    if os.path.isfile(contam_names):
        fh = open(contam_names, "r")
        for line in fh:
            line = line.strip()
            if line:
                contam_list.append(line)
        fh.close()


    # create a clean and contamination fasta
    clean_fasta = os.path.join(output_path, "acdc.clean.fasta")
    contam_fasta = os.path.join(output_path, "acdc.contamination.fasta")


    fh = open(fasta, "r")
    fhw = open(clean_fasta, "w")
    fhd = open(contam_fasta, "w")

    clean_flag = True
    bc = 0 # base count: debugging
    bc_clean = 0
    bc_contam = 0
    clean_cnt = 0
    contam_cnt = 0

    for line in fh:
        line = line.strip()
        if line.startswith(">"):
            if bc > 0:
                log.info("- base count: %s", bc)

            bc = 0

            if line[1:] in contam_list:
                clean_flag = False
                log.info("- contig %s: %s", line, msg_fail)
                contam_cnt += 1
            else:
                clean_flag = True
                log.info("- contig %s: %s", line, msg_ok)
                clean_cnt += 1

            if clean_flag:
                fhw.write(line + "\n") # write header
            else:
                fhd.write(line + "\n")

        else:
            bc += len(line)
            if clean_flag and line:
                fhw.write(line + "\n")
                bc_clean += len(line)
            else:
                fhd.write(line + "\n")
                bc_contam += len(line)

    if bc > 0:
        log.info("- base count: %s", bc)

    fh.close()
    fhw.close()
    fhd.close()

    log.info("- created clean fasta: %s", clean_fasta)
    log.info("- clean fasta: %s contigs, %s bp", clean_cnt, bc_clean)
    log.info("- created contaminaton fasta: %s", contam_fasta)
    log.info("- contamination fasta: %s contigs, %s bp", contam_cnt, bc_contam)




## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "AC-DC"

    RQC_URL = "https://rqc.jgi-psf.org"
    KRAKEN_DB = "/global/projectb/sandbox/rqc/prod/kraken/minikraken_20171019_8GB/"

    cmd = "shifter --image=bryce911/kraken kraken -v"
    std_out, std_err, exit_code = run_cmd(cmd)
    krak_ver = std_out.split("\n")[0].split()[-1]

    cmd = "shifter --image=mlux86/acdc:stable acdc -V" # 2018-05-07 - not in place yet
    acdc_ver = "1.0" # no version provided
    version = "1.0 (kraken: %s, acdc: %s)" % (krak_ver, acdc_ver)



    uname = getpass.getuser() # get the user, brycef can't use scell.p, but qc_user can!


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)

    # Parse options
    usage = "ac-dc.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
Identifies which contigs are contamination
    """

    parser = ArgumentParser(usage = usage)

    parser.add_argument("-f", "--fasta", dest="fasta", type = str, help = "Input fasta")
    parser.add_argument("-l", "--lib-name", dest="lib", type = str, help = "Library name")
    parser.add_argument("-o", "--output-path", dest="output_path", type = str, help = "Output path to write to, uses pwd if not set")

    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)

    output_path = None
    fasta = None
    library_name = None

    # parse args
    args = parser.parse_args()
    print_log = args.print_log

    if args.fasta:
        fasta = args.fasta
    if args.lib:
        library_name = args.lib

    if args.output_path:
        output_path = args.output_path


    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/acdc"

        if output_path == "t1":
            # 33% contam, now 0% contam
            fasta = "/global/projectb/scratch/brycef/acdc/CABXS.trim.fasta"
            library_name = "CABXS"
            #library_name = "BAGHS"
        if output_path == "t2":
            # no contam
            fasta = "/global/projectb/scratch/brycef/acdc/CAYXB.trim.fasta"
            library_name = "CAYXB"

        output_path = os.path.join(test_path, output_path)



    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    # initialize my logger
    log_file = os.path.join(output_path, "acdc.log")



    # log = logging object
    log_level = "INFO"
    log = None

    log = get_logger("acdc", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "fasta", fasta)
    log.info("%25s      %s", "library_name", library_name)

    log.info("")

    if not os.path.isfile(fasta):
        log.error("Error: cannot find fasta: %s  %s", fasta, msg_fail)
        sys.exit(2)


    KRAKEN_FILE = os.path.join(output_path, "%s.kraken" % library_name)
    KRAKEN_TAX_FILE = os.path.join(output_path, "%s.kraken.tax" % library_name)

    fh = open(os.path.join(output_path, "acdc_version.txt"), "w")
    fh.write("%s\n" % acdc_ver)
    fh.close()

    # steps to do decontam ...
    create_kraken_file(fasta)

    translate_kraken()

    run_acdc()

    contam_state = acdc_make_fasta()

    make_contam_fasta(fasta, contam_state)

    log.info("Completed %s", script_name)

    sys.exit(0)

'''
Wiki on AC/DC's Back in Black:
Worldwide, Back in Black is the second best-selling album of all time, behind only Michael Jackson's Thriller.
It is the best-selling hard rock album of all-time.

         ,.             .s.                                         .s.
      ,d$$$$b.        ,d$$$b.                 ,s$$$$$$$$s.        ,d$$$b.
    ,d$$$$$$$$b.   ,d$$$$$$$$b.      ssssss 4$$$$$$$$$$$$$b.   ,d$$$$$$$$b.
   $$$$$$`$$$$$$$ d$$$$$`$$$$$$b    d$$$$$'   $$$$$$`$$$$$$$b $$$$$$`$$$$$$b
   $$$$$$  $$$$$$ $$$$$$  "$$P"    $$$$$P     $$$$$$  "$$$$$$ $$$$$$  "$$P"
   $$$$$$  $$$$$$ $$$$$$          $$$$$P      $$$$$$   $$$$$$ $$$$$$
   $$$$$$ee$$$$$$ $$$$$$         $$$$$$$$$$"  $$$$$$   $$$$$$ $$$$$$
   $$$$$$$$$$$$$$ $$$$$$        d$$$$$$$$'    $$$$$$   $$$$$$ $$$$$$
   $$$$$$$$$$$$$$ $$$$$$  .s.      d$$$P      $$$$$$   $$$$$$ $$$$$$  .s.
   $$$$$$  $$$$$$ $$$$$$bd$$$$b.  d$$P'       $$$$$$.s$$$$$$$ $$$$$$bd$$$$b.
  ,d$$$$$bd$$$$$b."$$$$$$$$$$$P" d$$'       zd$$$$$$$$$$$$$"  "$$$$$$$$$$$P"
  "$$$$$$""$$$$$P"  "$$$$$$$$"  ;$'          "$$$$$$$$$$P"      "$$$$$$$$"
   "S$S"   "S$S"       "S$S"    '               """"""""           "S$S"


        I'm a rolling thunder, a pouring rain        I'll give you black sensations up and down your spine
        I'm coming on like a hurricane               If you're into evil you're a friend of mine
        My lightning's flashing across the sky       See my white light flashing as I split the night
        You're only young but you're gonna die       'Cause if God's on the left, then I'm stickin' to the right

                            I won't take no prisoners, won't spare no lives
                            Nobody's putting up a fight
                            I got my bell, I'm gonna take you to hell
                            I'm gonna get you, Satan get you

                            Hell's Bells
                            Yeah, Hell's Bells
                            You got me ringing Hell's Bells
                            My temperature's high, Hell's Bells


                            Hell's Bells, Satan's comin' to you
                            Hell's Bells, he's ringing them now
                            Hell's Bells, the temperature's high
                            Hell's Bells, across the sky
                            Hell's Bells, they're takin' you down
                            Hell's Bells, they're draggin' you around
                            Hell's Bells, gonna split the night
                            Hell's Bells, there's no way to fight, yeah


'''