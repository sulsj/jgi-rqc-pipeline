#!/bin/bash -l
#SBATCH -t 12:00:00
#SBATCH -c 32
#SBATCH --job-name=pbmid-t2
#SBATCH --mem=64G
#SBATCH -o /global/projectb/scratch/brycef/pbmid/t2.log
#SBATCH -e /global/projectb/scratch/brycef/pbmid/t2.err
#SBATCH --qos=jgi
#SBATCH -C haswell
#SBATCH -A fungalp

# no burst buffer


set -e

if [ $USER == "brycef" ]
then
    module load python
    export PATH=/global/homes/b/brycef/miniconda2/bin:$PATH
    source activate qaqc
fi

if [ $USER == "qc_user" ]
then
    export PATH=/global/homes/q/qc_user/miniconda/miniconda2/bin:$PATH
    source activate qaqc

fi

# user command to run
/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/pbmid.py -o t2
# sbatch -C haswell /global/homes/b/brycef/git/jgi-rqc-pipeline/sag/pbmid-t2.sh
# EOF