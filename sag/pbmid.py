#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
PBMID Pipeline
- PacBio Microbial Improved Draft

dependencies
bbtools (stats.sh, shred.sh, bbmap.sh, rename.sh)
pbsmrtpipe 5.0.1 - new
smrtlink (Pacbio - for arrow)
blast+
texlive (pdf maker)
prodigal (gc_histogram, antifam)
lastal (gc_histogram)
checkm
java, R (tetramer)
hmmer (antifam, duplication check)
mummer

RQC-1038 - revert to hgap 3?
* canu = celera assembler new version
* Alex prefers hgap3, hgap4 is more or diploid data


v 5.0: 2017-11-15
- initial version
- code reviewed
- added flow to create dot file
- added auspice statement to PDF & TXT report
- uses smrtlink 5.0.1 with HGAP4 (pbsmrtpipe.pipelines.polished_falcon_fat) (33% of run time for BWOAU 1.24 hrs for hgap4, 3.25 for hgap3)
http://pbsmrtpipe.readthedocs.io/en/master/getting_started.html
- added antifam

v 5.1: 2018-01-19
- converted to use smrtpipe 3.0 (HGAP3)
-- hgap4 is not optimized for microbial data, better for diploid data (... pacbio)
-- flow not updated
- moved circularize before polish because it removes repeats from the beginning of contigs
- added step to stop after hgap3/4 (Alex S request)
- added step to begin with new trimmed fasta (Alex) - set HCF status -> Ready

5.1.1: 2018-01-22
- os.chmod 775 for report, metadata files in do_pdf_report
- updates for metadata.json

5.1.2: 2018-01-30
- no trimming for the topology step
- use module load jamo instead of shifter

5.1.3: 2018-02-02
- use dedup_fasta for polishing
- new tetramer kmer code from bbtools

5.1.4: 2018-02-09
- RQC-1068 changes

5.1.5: 2018-02-12
- include file_path for input files

5.1.6: 2018-02-15
- fix bug with backup of hgap folder correctly
- load dedup from /global/projectb/scratch/qc_user/rqc/prod/pipelines/pbmid/dedup
- setup input.fofn correctly - was pullling in too many files

5.1.7: 2018-02-21
- move circularize after polish step (not trimming based on circularization steps: -t 0 option)
- added collab16s blast to blast_16s

5.1.8: 2018-02-27
- RQCSUPPORT-2410 - pdf gcontam fix
- RQCSUPPORT-2411 - pdf download fasta fix
- RQCSUPPORT-2408 - don't include chaff if empty in pbmid.metadata.json
- report hits to Silva SSU correctly

5.1.9: 2018-03-01
- use reformat.sh when importing dedup files (easier to read)
- arrow/reference - remove path correctly if exists, fails if blanks at the end of a sequence (SEQQC-12092, Alex will fix these)
- cleanup dedup files (remove nulls) tr command in dedup

5.1.10: 2018-03-02
- only count nulls in the dedup fasta, fail if > 1
- do not cleanup dedup files (tr or sed or anything else)

5.1.11: 2018-03-30
- collabs16s blast use different params in do_blast
- added quality reporting (RQC-1090)
- use gold_organism_name if available, otherwise use ncbi_organism_name

5.1.12: 2018-04-25
- copy in contig_hist.png, genome_vs_size.png from do_contig_gc_plot

5.1.13: 2018-05-07
- fail and exit if pipeline fails (CPSYC)

5.1.14: 2018-05-14
- skip topology failure (SEQQC-12183)

5.1.15: 2018-07-05
- add quality reporting to PDF & JAT template, do not include Quality assessment (RQCSUPPORT-2714)

To do:
- file from circularize ...
- SEQQC-12205 - run analysis on a fasta

note: read_count in RQC != read count from bax.h5 fasta files (or bam -> fasta files)

/global/dna/projectdirs/PI/rqc/prod/versions/jgi-rqc-pipeline-sag:
$ git checkout origin/sag
$ git pull
$ git checkout origin/sag

To do:
* hgap4 - not including all smrtcells in assemblies?? need to check
* needs dedup code from Alex S
* possible Sequel runs for PBMID once pooling is worked out - need pipeline changes


* gc_hist - lots of species in Jigsaw version vs sag version
--- sag version has best hits all to different taxonomies which all translate to "Clostridium saccharobutylicum" for the best hit
--- using newer nr, min id > 30% vs 0.05% in jigsaw


PDF changes:
 * tetramer, checkm not included in original PDF report but added to new report
 * annotation section not included
TXT changes:
 * removed Authors section
metadata.json changes:
 * need to update yaml file
 * use Kurt's production macro
 * took out ap/at for each file (metadata at top level is applied to each file)
 * inputs are only the h5 files that go into HGAP, not the *.fastq, xml, fasta in the same folder
 * added chaff_fasta, hgap_settings_xml
 * collect versions of all of the tools and all of the commands
 * add geneprimp: https://issues.jgi-psf.org/browse/SEQQC-11198



Changes from Hacksaw.py
- new pipeline called is "pbmid.py"
- re-run if contig count > 20 with new genome size estimate*
- polish step uses commands in pb.re-arrow.sh
- new circularization code
- new contig_gc plot code**
- new gc_cov code**
- new gc_histogram code**
- new ncbi_prescreen code**
- new single copy gene code (checkm)
- new tetramer code**


Alex's scripts: 2017-06-20
/global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv1000g/

pb.rlink.sh - link smrtportal jobs to local folder

pb.cp2data3.sh - rename files
/data/corrected.fasta|q = ./no_control_corrected.fasta|q
/data/filtered_subreads.fasta|q = ./
/data/nocontrol_filtered_subreads.fasta ./no_control_filtered_subreads_fasta
/data/polished_assembly_fasta.gz ./
* do something with the control.fa.head in the fasta?

pb.prelim_qc.sh - nucmer and spike-in detection
blasr to check that P4 spike-in cleared

pb.re-quiver.qsub.sh
- replaced with pb.arrow.sh

pb.circle.sh

pb.hacksaw_qc_only.sh

pb.hacklinks.sh (pack files)
* checks for files

pb.pre_report.sh
* removes old stuff, re-symlinks

pb.make_reports.sh
pb.final_check.sh
pb.summarize_portal_report.sh

~~~~~~~~~~~~~~~~~~~[ Test Cases ]

assembly is 3 - 12 hours (or more)

t1: BBBYN - 1 contig, 3 hrs asm
SEQQC-9480
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc1607/s9480/036767-cleanH5

#t2: BBTNX - 21 contigs, 3 hrs asm
t2: BWOAU - 4 contigs, 3 hrs asm -- 1 contig now

t2 = BWOAU = SEQQC-11057
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc1706/s11057/003319-cleanH5
~/pbmid-t2.sh
$ sbatch -C haswell /global/homes/b/brycef/git/jgi-rqc-pipeline/sag/pbmid-t2.sh

denovo:
$ sbatch /global/homes/b/brycef/git/jgi-rqc-pipeline/sag/pbmid-t2d.sh
$ sbatch /global/homes/b/brycef/git/jgi-rqc-pipeline/sag/pbmid-t3d.sh




t3 - BXSSA - 80 contigs in RQC, will reassembling at correct size help?

BXTAT - bad assembly
- asm 1 = 356 contigs, 1169647 bp
- asm 2 = 830 contigs, 2373836 bp
- asm 3 = 908 contigs, 2386519 bp

$ jat import microbial_isolate_improved_pacbio pbmid.metadata.json
* failed because its changed

2018-01-22
$ jat import pbmid/microbial_isolate_improved_pacbio pbmid.metadata.json

./pbmid.py -seqqc /global/projectb/scratch/brycef/pbmid/s11264/ -l BXHSS
~/git/jgi-rqc-pipeline/sag/pbmid.py -seqqc /global/projectb/scratch/brycef/pbmid/s11264/ -l BXHSS -pl

#1139662/BXHSS

run on this one:
rsync -a /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc1708/s11264/040485-cleanH5/* .
/global/projectb/scratch/brycef/pbmid/s11264/

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Denovo testing
sbatch /global/homes/b/brycef/git/jgi-rqc-pipeline/sag/pbmid-t2d.sh
sbatch /global/homes/b/brycef/git/jgi-rqc-pipeline/sag/pbmid-t3d.sh

CCZAH
./pbmid.py --hgap-only -t4

microbial_isolate_improved_pacbio

template changes:
metadata: use macro production_release (spid, library), analysis_info (ap, at)
+ filtered_subreads
- pacbio_h5
assembly: -analysis_info macro
+chaff_fasta
+assembler_settings
 + metadata.assember: HGAP, assembler_version: ...
* rename keys also per Alicia rules?

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import glob
import time
import json
import getpass
import collections
import numpy
import re


# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, checkpoint_step, append_rqc_file, append_rqc_stats, get_colors, get_analysis_project_id, run_cmd, get_msg_settings, append_flow, dot_flow

from rqc_utility import get_blast_hit_count

from micro_lib import load_config_file, get_the_path, check_skip, make_asm_stats, save_asm_stats, get_library_info, save_file, get_file, get_bbtools_version, \
get_ssu, green_genes_pdf, ncbi_prescreen_report, merge_template, get_base_contig_cnt, format_val_pct, create_empty_checkm_summary, get_smrtcell_chemistry, get_nproc


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
create input.fofn
- use bas.h5 as the smrt cell count and use to figure out chemistry used
- RS2 & Hgap3 
'''

def setup_pbsmrtpipe(library_name):
    log.info("setup_pbsmrtpipe: %s", library_name)

    step_name = "setup_pbsmrtpipe"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    input_fofn = os.path.join(output_path, "input.fofn")


    if os.path.isfile(input_fofn) and 1 == 2:
        log.info("- input file exists: %s, not generating new one  %s", input_fofn, msg_warn)
        status = "%s skipped" % step_name

    else:
        chem_list = []
        smrt_cell_cnt = 0
        missing_cnt = 0 # files missing on file system, probably need to be restored

        # for production_macro: seq_units list
        # RQC-1068: Alex want's em all
        #cmd = "#%s;jamo report select file_name, file_path where file_type = 'fastq' and metadata.library_name = '%s' as json" % (config['jamo_module'], library_name)
        cmd = "module load jamo;jamo report select file_name, file_path where file_type = 'fastq' and metadata.library_name = '%s' as json" % (library_name)

        std_out, std_err, exit_code = run_cmd(cmd, log)
        if exit_code == 0 and std_out:

            fq_list = []
            j = json.loads(std_out)
            for fs in j:
                fq_list.append(fs['file_name'])
            append_rqc_stats(rqc_stats_log, "fastq_list", ",".join(fq_list))
            log.info("- fastq list: %s", fq_list)


        #cmd = "#%s;jamo report select file_name, file_path where file_type = 'h5' and metadata.library_name = '%s' as json" % (config['jamo_module'], library_name)
        #cmd = "module load jamo;jamo report select file_name, file_path where file_type = 'h5' and metadata.library_name = '%s' as json" % (library_name)
        cmd = "module load jamo;jamo report select file_path, file_name, metadata.sdm_smrt_cell.fs_location, file_type where file_type = 'h5' and metadata.library_name = '%s' as json" % (library_name)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # returns
        # [{"file_name": "pbio-1220.11039.fastq", "metadata.sdm_smrt_cell.fs_location": "/global/seqfs/sdm/prod/pacbio/runs/pbr4197_668/A07_1"}, {"file_name": "pbio-1223.11060.fastq", "metadata.sdm_smrt_cell.fs_location": "/global/seqfs/sdm/prod/pacbio/runs/pbr4200_670/A04_1"}]

        if exit_code == 0 and std_out:

            j = json.loads(std_out)
            h5_cnt = 0

            fh = open(input_fofn, "w")

            for fs in j:

                # skip run folder archives
                if not fs['metadata.sdm_smrt_cell.fs_location']:
                    continue

                # h5 files only past this point
                if 'h5' not in fs['file_type']:
                    continue

                smrt_file = os.path.join(fs['file_path'], fs['file_name']) # bax.h5 or .bas.h5
                log.info(" - file: %s", smrt_file)
                if not os.path.isfile(smrt_file):
                    log.error("- file missing: %s  %s", smrt_file, msg_warn)
                    missing_cnt += 1

                if fs['file_name'].endswith(".bas.h5"):
                    smrt_cell_cnt += 1

                    xml_file = smrt_file.replace(".bas.h5", ".metadata.xml")

                    smrt_chemistry = get_smrtcell_chemistry(xml_file, config['chemistry_mapping_xml'], log)
                    log.info(" - smrt cell chemistry: %s", smrt_chemistry)
                    chem_list.append(smrt_chemistry)

                else:
                    fh.write(smrt_file+"\n")
                    h5_cnt += 1

            fh.close()
            log.info("- created file: %s", input_fofn)

            save_file(input_fofn, "input_fofn", file_dict, output_path)

            smrtcell_chem = ",".join(list(set(chem_list)))
            log.info("- chemistry list: %s", smrtcell_chem)
            log.info("- smrtcell cnt: %s, h5 cnt: %s", smrt_cell_cnt, h5_cnt)

            append_rqc_stats(rqc_stats_log, "chemistry", smrtcell_chem)
            append_rqc_stats(rqc_stats_log, "pacbio_smrtcell_cnt", smrt_cell_cnt)

            if h5_cnt > 0:
                status = "%s complete" % step_name
            else:
                status = "%s failed" % step_name

            if len(list(set(chem_list))) > 1:
                log.error("- more than one chemistry found!  %s", msg_fail)
                status = "%s failed" % step_name

            if missing_cnt > 0:
                log.error("- %s files not on the file system!  %s", missing_cnt, msg_fail)
                status = "%s failed" % step_name

            make_all_input_fofn(library_name)

            # initial input node
            append_flow(flow_file, "input_fofn", "%s H5<br><smf>SMRT Cells: %s<br>Chemistry: %s</f>" % (library_name, smrt_cell_cnt, smrtcell_chem), "", "", "")

        else:
            status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
http://pbsmrtpipe.readthedocs.io/en/master/getting_started.html
- 2017-10-12

 create the config files needed for hgap4
 $ dataset create --force --type HdfSubreadSet hdfsubreadset.xml /global/projectb/scratch/brycef/pbmid/BYGYG/input.fofn
 $ pbsmrtpipe show-workflow-options -j preset.json
 $ pbsmrtpipe show-template-details pbsmrtpipe.pipelines.polished_falcon_fat -j hgap_config.json
 # -j for json doesn't work as input

* don't need subread_xml, have *subread.bam already
shifter --image=registry.services.nersc.gov/jgi/smrtlink:5.0.1.9585 /smrtlink/smrtcmds/bin/pbsmrtpipe show-template-details pbsmrtpipe.pipelines.polished_falcon_fat
Description: 
 Same as polished_falcon_lean, but with reports.

**** Entry points (1) ****
$entry:eid_subread
* what is the latest smrtlink we got? correct hgap version? (0.2.0)
'''
def setup_hgap4_xml(genome_size):
    log.info("setup_hgap4_xml: %s bp", genome_size)

    step_name = "setup_hgap_xml"

    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(CONFIG_PATH, output_path)


    subread_xml = os.path.join(the_path, "hdfsubreadset.xml")
    #preset_file = os.path.join(the_path, "preset.json") # cluster & threads
    preset_file = os.path.join(the_path, "preset.xml")
    #hgap_file = os.path.join(the_path, "hgap_settings.json")
    hgap_file = os.path.join(the_path, "hgap_settings.xml")

    input_fofn = get_file("input_fofn", file_dict, output_path, log)

    if os.path.isfile(hgap_file) and 1 == 2:
        log.info("- hgap config file exists: %s, not generating new one  %s", hgap_file, msg_warn)
        status = "%s skipped" % step_name

    else:

        # create subreadset.xml file - used to convert bax.h5 to subreads.bam
        subread_log = os.path.join(the_path, "hdfsubreadset.log")
        cmd = "#%s;dataset create --force --type HdfSubreadSet %s %s > %s 2>&1" % (config['smrt_link_module'], subread_xml, input_fofn, subread_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        # create preset.xml
        workflow_log = os.path.join(the_path, "workflow.log")
        #preset_file_org = preset_file.replace(".json", ".pb.json")
        preset_file_org = preset_file.replace(".xml", ".pb.xml")
        #cmd = "#%s;pbsmrtpipe show-workflow-options -j %s > %s 2>&1" % (config['smrt_link_module'], preset_file_org, workflow_log)
        cmd = "#%s;pbsmrtpipe show-workflow-options -o %s > %s 2>&1" % (config['smrt_link_module'], preset_file_org, workflow_log)
        # -o = xml, -j = json
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # create hgap_settings.xml for falcon
        hgap_log = os.path.join(the_path, "hgap.log")
        #hgap_file_org = hgap_file.replace(".json", ".pb.json")
        hgap_file_org = hgap_file.replace(".xml", ".pb.xml")
        #cmd = "#%s;pbsmrtpipe show-template-details pbsmrtpipe.pipelines.polished_falcon_fat -j %s > %s 2>&1" % (config['smrt_link_module'], hgap_file_org, hgap_log)
        cmd = "#%s;pbsmrtpipe show-template-details pbsmrtpipe.pipelines.polished_falcon_fat -o %s > %s 2>&1" % (config['smrt_link_module'], hgap_file_org, hgap_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        # didn't see a way to set these settings from the command line

        # modify preset.json
        nproc = get_nproc()
        tmp_path = get_the_path("tmp", output_path)

        fh = open(preset_file_org, "r")
        fhw = open(preset_file, "w")
        val = ""
        for line in fh:

            if "distributed_mode" in line:
                #line = line.replace(": true,", ": false,")
                val = "False"
            if "max_nproc" in line:
                #line = line.replace(": 16,", ": %s," % nproc)
                val = nproc
            if "tmp_dir" in line:
                val = tmp_path
                #line = line.replace(": \"/tmp\",", ": \"%s\"," % tmp_path)

            if val and "value" in line:
                line = "            <value>%s</value>\n" % val
                val = ""

            fhw.write(line)

        fh.close()
        fhw.close()
        log.info("- created: %s", preset_file)


        # modify hgap.xml
        val = ""
        fh = open(hgap_file_org, "r")
        fhw = open(hgap_file, "w")

        for line in fh:
            if "HGAP_GenomeLength_str" in line:
                #line = line.replace(": \"5000000\",", ": \"%s\"," % genome_size)
                val = genome_size

            #if "_comment" in line:
            #    line = line.replace(" hgap.json'", " hgap.json', 'Modified by PBMID'")

            if val and "value" in line:
                line = "            <value>%s</value>\n" % val
                val = ""

            fhw.write(line)

        fh.close()
        fhw.close()
        log.info("- created: %s", hgap_file)

    save_file(subread_xml, "subread_file", file_dict, output_path)
    save_file(preset_file, "preset_file", file_dict, output_path)
    save_file(hgap_file, "hgap_file", file_dict, output_path)

    status = "%s complete" % step_name


    checkpoint_step(status_log, status)
    return status


'''
$ pbsmrtpipe pipeline-id pbsmrtpipe.pipelines.sa3_hdfsubread_to_subread \
  --preset-xml preset.xml -e eid_hdfsubread:3531.hdfsubreadset.xml \
  -o /global/projectb/scratch/brycef/pbmid/BYGYG/hdf_to_subread6

pbsmrtpipe pipeline-id pbsmrtpipe.pipelines.sa3_hdfsubread_to_subread \
  --preset-json preset.json -e eid_hdfsubread:/global/projectb/scratch/brycef/pbmid/CAHGT/config/hdfsubreadset.xml \
  -o /global/projectb/scratch/brycef/pbmid/CAHGT/subreads
* doesn't work with json (pb bug)


 UnicodeDecodeError: 'utf8' codec can't decode byte 0xff in position 9: invalid start byte
 QRSH_COMMAND is weird: filed nersc ticket ...
 $ export QRSH_COMMAND="/bin/bash"


'''
def do_hgap4_assembly(genome_size):
    log.info("do_hgap4_assembly: %s", genome_size)

    new_genome_size = genome_size

    step_name = "assembly"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(ASM_PATH, output_path)


    # tasks/pbcoretools.tasks.contigset2fasta-0/file.fasta is a symlink to consensus.fasta
    hgap_fasta = os.path.join(the_path, "tasks", "genomic_consensus.tasks.variantcaller-0", "consensus.fasta")


    if seqqc:

        log.info("- not running assembly, using SEQQC location: %s", seqqc)
        hgap_path = os.path.realpath(os.path.join(seqqc, "data"))
        hgap_path = os.path.realpath(os.path.join(os.path.realpath(hgap_path), ".."))

        cmd = "rsync -a %s/* %s/." % (hgap_path, the_path)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # got the job path from looking up folder in Alex's release folders and rsyncing his symlinked job

    if os.path.isfile(hgap_fasta):
        log.info("- skipping running assembly, found %s", hgap_fasta)
        #status = "%s skipped" % step_name

    else:

        preset_file = get_file("preset_file", file_dict, output_path, log)
        subread_xml = get_file("subread_file", file_dict, output_path, log)

        subread_path = get_the_path(SUBREAD_PATH, output_path)


        hdf_subread_log = os.path.join(the_path, "hdfsubread.log")
        subread_bam_list = glob.glob(os.path.join(subread_path, "tasks", "pbcoretools.tasks.h5_subreads_to_subread-0", "*.subreads.bam"))

        #if os.path.isfile(subread_bam):
        if len(subread_bam_list) > 0:
            log.info("- found *.subreads.bam, skipping step")

        else:
            cmd = "#%s;pbsmrtpipe pipeline-id pbsmrtpipe.pipelines.sa3_hdfsubread_to_subread"
            #cmd += "--preset-json %s -e eid_hdfsubread:%s "
            cmd += " --preset-xml %s -e eid_hdfsubread:%s"
            cmd += " -o %s > %s 2>&1"
            cmd = cmd % (config['smrt_link_module'], preset_file, subread_xml, subread_path, hdf_subread_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)


        if os.path.isfile(hgap_fasta):
            # create backup of old run folder

            timestamp = time.strftime("%Y%m%d%H%M%S")
            backup_path = "%s.%s" % (the_path, timestamp)
            cmd = "mv %s %s" % (the_path, backup_path)
            std_out, std_err, exit_code = run_cmd(cmd, log)
            # remake the path
            the_path = get_the_path(ASM_PATH, output_path)

        subread_xml = os.path.join(subread_path, "tasks", "pbcoretools.tasks.h5_subreads_to_subread-0", "subreads.subreadset.xml")
        save_file(subread_xml, "subread_bam_file", file_dict, output_path)

        hgap_file = get_file("hgap_file", file_dict, output_path, log)
        hgap_log = os.path.join(the_path, "hgap.log")
        cmd = "#%s;pbsmrtpipe pipeline-id pbsmrtpipe.pipelines.polished_falcon_fat"
        #cmd += "--preset-json %s --preset-json %s "
        cmd += " --preset-xml %s --preset-xml %s"
        cmd += " -e eid_subread:%s"
        cmd += " -o %s > %s 2>&1"
        cmd = cmd % (config['smrt_link_module'], preset_file, hgap_file, subread_xml, the_path, hgap_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)



    if os.path.isfile(hgap_fasta):

        # hgap fasta stats!
        fasta_type = "hgap_fasta"
        asm_stats, asm_tsv = make_asm_stats(hgap_fasta, fasta_type, the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

        # check for number of contigs, if > 10 need to rerun assembly with new genome size
        new_genome_size, contig_cnt = get_base_contig_cnt(asm_tsv)

        # flow_file, orig_node, orig_label, next_node, next_label, link label
        hgap_str = config['hgap_version_str'].replace(",", "<br>")
        append_flow(flow_file, "input_xml", "", "hgap_asm", "HGAP Assembly<br><smf>contigs: %s<br>assembly size: %s bp</f>" % (contig_cnt, new_genome_size), hgap_str)


        # 20 too many?
        # round to nearest 100,000
        new_genome_size = (int(new_genome_size / 100000.0 - 0.01)+1)* 100000
        log.info("- new_genome_size: %s", new_genome_size)

        # only re-run if not using an existing run
        if not seqqc:
            if int(contig_cnt) >= int(config['contig_max_limit']) and asm_cnt == 1:

                # round to nearest 100,000
                log.info("- contig_cnt = %s, should be < %s, new_genome_size = %s", contig_cnt, config['contig_max_limit'], new_genome_size)

                status = "rerun assembly: %s" % asm_cnt
                checkpoint_step(status_log, status)
                return status, new_genome_size

            else:
                # save the output files
                save_hgap4_files(the_path, hgap_fasta)


        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)

    return status, new_genome_size



'''
Save previous hgap runs
'''
def backup_hgap_folder(asm_cnt):
    log.info("backup_hgap_folder: %s", asm_cnt)

    the_path = os.path.join(output_path, ASM_PATH) # get_the_path(ASM_PATH, output_path)

    if os.path.isdir(the_path):

        new_path = "%s-%s" % (the_path, asm_cnt)
        if os.path.isdir(new_path):
            timestamp = time.strftime("%Y%m%d%H%M%S")
            new_path = "%s-%s-%s" % (the_path, asm_cnt, timestamp)

        cmd = "mv %s %s" % (the_path, new_path)
        std_out, std_err, exit_code = run_cmd(cmd, log)



'''
Files from HGAP

'''
def save_hgap4_files(hgap_path, hgap_fasta=None):
    log.info("save_hgap4_files: %s", hgap_path)

    if hgap_fasta:
        log.info("- hgap_fasta: %s", hgap_fasta)

    if os.path.isfile(hgap_fasta):
        task_path = os.path.join(hgap_path, "tasks")

        save_file(hgap_fasta, "hgap_fasta", file_dict, output_path)

        filtered_subreads_fasta = os.path.join(task_path, "pbcoretools.tasks.bam2fasta-0", "subreads.fasta")
        save_file(filtered_subreads_fasta, "filtered_subreads_fasta", file_dict, output_path)
        log.info("- saved: %s", filtered_subreads_fasta)


        error_corrected_fasta = os.path.join(task_path, "falcon_ns.tasks.task_falcon1_run_merge_consensus_jobs-0", "preads4falcon.fasta")
        save_file(error_corrected_fasta, "error_corrected_fasta", file_dict, output_path)
        log.info("- saved: %s", error_corrected_fasta)

        png_list = glob.glob(os.path.join(hgap_path, "tasks", "*", "*.png"))

        cov_plot_cnt = 0

        for my_png in png_list:

            if my_png.endswith("_thumb.png"):
                # skip it - we don't need the thumb files
                pass
            else:
                png_name = os.path.basename(my_png)
                plot_name = "%s_%s" % ("hgap", os.path.basename(my_png).replace("-", "").replace(".png", ""))

                # coverage plots named after contigs
                if png_name.startswith("coverage_plot_"):
                    cov_plot_cnt += 1
                    #my_png = os.path.join(hgap_path, "results", "coverage_plot-%s.png" % (cov_plot_cnt))
                    plot_name = "hgap_coverage_plot-%s" % (cov_plot_cnt)

                append_rqc_file(rqc_file_log, plot_name, my_png)
                log.info("- added plot: %s", my_png)

    else:
        log.error("- cannot find hgap_fasta: %s  %s", hgap_fasta)
        sys.exit(2)



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~[ HGAP3 ]




'''
Get all of the *.h5 and *.xml into a input.xml - needed for hgap
to do: check qc for each fastq found (RQCWS?)
* used for old hgap/smrtlink, deprecated

'''
def setup_hgap3_input_xml(library_name):
    log.info("setup_input_xml: %s", library_name)

    step_name = "setup_input_xml"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(CONFIG_PATH, output_path)

    input_xml = os.path.join(the_path, "input-%s.xml" % library_name)
    input_fofn = os.path.join(output_path, "input.fofn") # needed as input for other smrtportal tools


    if os.path.isfile(input_xml) and 1==2:
        log.info("- input file exists: %s, not generating new one  %s", input_xml, msg_warn)
        status = "%s skipped" % step_name

    else:

        chem_list = [] # list of different chemistries for each smrtcell, should be only 1 (P6-C4)
        smrt_cell_cnt = 0 # number of unique smrtcells

        # for production_macro: seq_units list
        #cmd = "#%s;jamo report select file_name, file_path where file_type = 'fastq' and metadata.library_name = '%s' as json" % (config['jamo_module'], library_name)
        cmd = "module load jamo;jamo report select file_path, file_name, file_path where file_type = 'fastq' and metadata.library_name = '%s' as json" % (library_name)

        std_out, std_err, exit_code = run_cmd(cmd, log)
        if exit_code == 0 and std_out:

            fq_list = []
            j = json.loads(std_out)
            for fs in j:
                fq_list.append(fs['file_name'])

            append_rqc_stats(rqc_stats_log, "fastq_list", ",".join(fq_list))
            log.info("- fastq list: %s", fq_list)

        # get input paths from JAMO
        # works with shifter container, possibly just a module for cori in the future
        # looking up the fastq and using the smrt_cell.fs_location to get the actual files
        # hgap does not work unless it points to the /global/seqfs location of the run (I tried 2017-09-13)
        # IOError Input file sanity check failed for xml:/global/projectb/scratch/brycef/pbmid/BWOAU/config/input-BWOAU.xml ... exiting
        #cmd = "#%s;jamo report select file_name, metadata.sdm_smrt_cell.fs_location where metadata.library_name = '%s' and file_type = 'fastq' as json" % (config['jamo_module'], library_name)
        cmd = "module load jamo;jamo report select file_path, file_name, metadata.sdm_smrt_cell.fs_location, file_type where metadata.library_name = '%s' and file_type = 'fastq' as json" % (library_name)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # returns
        # [{"file_name": "pbio-1220.11039.fastq", "metadata.sdm_smrt_cell.fs_location": "/global/seqfs/sdm/prod/pacbio/runs/pbr4197_668/A07_1"}, {"file_name": "pbio-1223.11060.fastq", "metadata.sdm_smrt_cell.fs_location": "/global/seqfs/sdm/prod/pacbio/runs/pbr4200_670/A04_1"}]

        if exit_code == 0 and std_out:

            j = json.loads(std_out)
            h5_cnt = 0

            fh = open(input_xml, "w")
            fh_fofn = open(input_fofn, "w") # input.fofn


            fh.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n")
            fh.write("<pacbioAnalysisInputs>\n")

            # skipping <job>..</job> section

            fh.write("  <dataReferences>\n")
            # for each one in the json
            for fs in j:

                h5_list = glob.glob(os.path.join(fs['metadata.sdm_smrt_cell.fs_location'], "Analysis_Results", "*.bax.h5"))
                fh.write("  <data ref=\"%s\">\n" % (fs['metadata.sdm_smrt_cell.fs_location']))
                for h5 in h5_list:
                    fh.write("      <location>%s</location>\n" % h5)
                    fh_fofn.write(h5 + "\n")
                    h5_cnt += 1
                fh.write("  </data>\n")


                xml_list = glob.glob(os.path.join(fs['metadata.sdm_smrt_cell.fs_location'], "*.metadata.xml"))
                for xml_file in xml_list:
                    smrt_chemistry = get_smrtcell_chemistry(xml_file, config['chemistry_mapping_xml'], log)
                    log.info("- chemistry: %s", smrt_chemistry)
                    chem_list.append(smrt_chemistry)
                    smrt_cell_cnt += 1

            fh.write("  </dataReferences>\n")
            fh.write("</pacbioAnalysisInputs>\n")
            fh.close()
            fh_fofn.close()

            log.info("- created %s", input_xml)
            log.info("- created %s", input_fofn)

            log.info("- h5 files: %s, smrt_cells: %s", h5_cnt, smrt_cell_cnt)


            save_file(input_xml, "input_xml", file_dict, output_path)
            save_file(input_fofn, "input_fofn", file_dict, output_path)

            smrtcell_chem = ",".join(list(set(chem_list)))
            log.info("- chemistry: %s", smrtcell_chem)

            append_rqc_stats(rqc_stats_log, "chemistry", smrtcell_chem)
            append_rqc_stats(rqc_stats_log, "pacbio_smrtcell_cnt", smrt_cell_cnt)

            if h5_cnt > 0:
                status = "%s complete" % step_name
            else:
                status = "%s failed" % step_name

            if len(list(set(chem_list))) > 1:
                log.info("- more than one chemistry found!  %s", msg_fail)
                status = "%s failed" % step_name

            make_all_input_fofn(library_name)

            # initial input node
            append_flow(flow_file, "input_xml", "%s H5<br><smf>SMRT Cells: %s<br>Chemistry: %s</f>" % (library_name, smrt_cell_cnt, smrtcell_chem), "", "", "")

        else:
            status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status



'''
Makes ./all_input.fofn
- exclude fasta except include nocontrol, exclude fastq also
- works for hgap3 or 4
'''
def make_all_input_fofn(library_name):
    log.info("make_all_input_fofon: %s", library_name)

    all_input_fofn = os.path.join(output_path, "all_input.fofn") # needed for metadata.json - do_pdf will filter out un-needed files
    # get all files for input_list for metadata.json
    cmd = "module load jamo;jamo report select file_path, file_name, file_type where metadata.library_name = '%s' as json" % (library_name)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    if exit_code == 0 and std_out:

        j = json.loads(std_out)

        fh_all = open(all_input_fofn, "w")

        for fs in j:
            save_flag = False

            if 'h5' in fs['file_type']:
                save_flag = True
            elif 'pacbio_metadata' in fs['file_type']:
                save_flag = True
            elif "control" in fs['file_name']:
                save_flag = True

            if save_flag:
                fh_all.write(os.path.join(fs['file_path'], fs['file_name']) + "\n")

        fh_all.close()
        log.info("- created %s", all_input_fofn)



'''
Set genome size in hgap_xml, update spike_in value too

'''
def setup_hgap3_xml(genome_size):
    log.info("setup_hgap3_xml: %s", genome_size)
    step_name = "setup_hgap_xml"

    spike_in_control = config['spike_in_control']

    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(CONFIG_PATH, output_path)

    hgap_xml = os.path.join(the_path, "hgap_settings.xml")

    # old one exists, create backup
    if os.path.isfile(hgap_xml):
        log.info("- hgap.xml exists, backing up")
        timestamp = time.strftime("%Y%m%d%H%M%S")
        hgap_xml_old = hgap_xml.replace(".xml", ".%s.xml" % timestamp)
        cmd = "cp %s %s" % (hgap_xml, hgap_xml_old)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    # doesn't exist, use default
    if not os.path.isfile(hgap_xml):
        log.info("- cannot find hgap_settings.xml, copying from source")
        # settings for which hgap file
        config_hgap_xml = os.path.realpath(os.path.join(my_path, "../sag/", "config", "hgap_settings.xml"))
        cmd = "cp %s %s" % (config_hgap_xml, hgap_xml)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    hgap_xml_new = os.path.join(the_path, hgap_xml).replace(".xml", ".xml-new")

    fh = open(hgap_xml)
    fhw = open(hgap_xml_new, "w")

    # things we are able change:
    # - spike-in type
    # - genome size

    fix_size_flag = False # genome size
    fix_control_flag = False # spike-in control
    nproc = get_nproc()

    for line in fh:
        # pbalign commands
        if "[nproc]" in line:
            line = line.replace("[nproc]", "--nproc "+str(nproc))

        if fix_size_flag == True:
            if "<value>" in line:
                line = "                <value>%s</value>\n"  % (genome_size)
                log.info("- changed genome size to: %s", genome_size)

            fix_size_flag = False

        if fix_control_flag == True:
            if "<value>" in line:
                if spike_in_control:
                    line = "            <value>%s</value>\n" % (spike_in_control)
                else:
                    line = "            <value></value>\n"

            fix_control_flag = False


        fhw.write(line)


        # if genome_size line
        if genome_size > 0:
            if "genome size (bp)" in line.lower() and "genomesize" in line.lower():
                fix_size_flag = True

        #<param name="control" hidden="true">
        #    <value>/global/projectb/shared/pacbio/references/4kb_Control</value>
        #</param>


        if '<param name="control" hidden="true">' in line.lower():
            # 2kb_control is the default
            if not spike_in_control.endswith("2kb_control"):
                fix_control_flag = True


    fh.close()
    fhw.close()


    # rename .xml to .xml-old
    # rename .xml-new to .xml
    # remove .xml-old
    hgap_xml_old = hgap_xml.replace(".xml", ".xml-old")
    cmd = "mv %s %s" % (hgap_xml, hgap_xml_old)
    std_out, std_err, exit_code = run_cmd(cmd, log)


    cmd = "mv %s %s" % (hgap_xml_new, hgap_xml)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    cmd = "rm %s" % (hgap_xml_old)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    save_file(hgap_xml, "hgap_xml", file_dict, output_path)

    status = "%s complete" % step_name


    checkpoint_step(status_log, status)
    return status


'''
Run Hgap assemler or rsync from an existing /data folder (seqqc path)

'''
def do_hgap3_assembly(genome_size):
    log.info("do_hgap3_assembly: %s", genome_size)

    new_genome_size = genome_size

    step_name = "assembly"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(ASM_PATH, output_path)

    hgap_fasta = os.path.join(the_path, "data", "polished_assembly.fasta.gz")

    hgap_done = os.path.join(output_path, "hgap_status.txt")

    if seqqc:

        log.info("- not running assembly, using SEQQC location: %s", seqqc)
        hgap_path = os.path.realpath(os.path.join(seqqc, "data"))
        hgap_path = os.path.realpath(os.path.join(os.path.realpath(hgap_path), ".."))

        cmd = "rsync -a %s/* %s/." % (hgap_path, the_path)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # got the job path from looking up folder in Alex's release folders and rsyncing his symlinked job


    if os.path.isfile(hgap_done):
        log.info("- skipping running assembly, found %s", hgap_done)

    #elif os.path.isfile(hgap_fasta):
    #    log.info("- skipping running assembly, found %s", hgap_fasta)
    #    #status = "%s skipped" % step_name

    else:

        # tmp = 3mb
        tmp_path = os.path.join(the_path, "tmp")
        hgap_log = os.path.join(the_path, "hgap.log")
        input_xml = get_file("input_xml", file_dict, output_path, log)
        hgap_xml = get_file("hgap_xml", file_dict, output_path, log)

        if os.path.isfile(hgap_fasta):
            # create backup of old run folder

            timestamp = time.strftime("%Y%m%d%H%M%S")
            backup_path = "%s.%s" % (the_path, timestamp)
            cmd = "mv %s %s" % (the_path, backup_path)
            std_out, std_err, exit_code = run_cmd(cmd, log)
            # remake the path
            the_path = get_the_path(ASM_PATH, output_path)


        # pbsmrtpipe in smrtlink?
        cmd = "#%s;smrtpipe.py --params=%s --output=%s -D TMP=%s xml:%s > %s 2>&1" % (config['smrt_analysis_module'], hgap_xml, the_path, tmp_path, input_xml, hgap_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        if exit_code > 0:
            log.error("- smrtpipe failed running assembly  %s", msg_fail)
            status = "%s failed" % step_name
            checkpoint_step(status_log, status)
            sys.exit(1)

    if os.path.isfile(hgap_fasta):

        # hgap fasta stats!
        fasta_type = "hgap_fasta"

        asm_stats, asm_tsv = make_asm_stats(hgap_fasta, fasta_type, the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

        # check for number of contigs, if > 10 need to rerun assembly with new genome size

        new_genome_size, contig_cnt = get_base_contig_cnt(asm_tsv)

        if new_genome_size == 0:
            log.error("- genome size is 0!  %s", msg_fail)
            # because failing to correctly parse *.tsv file ... bbtools adding stderr
            sys.exit(16)


        # flow_file, orig_node, orig_label, next_node, next_label, link label
        hgap_str = config['hgap_version_str'].replace(",", "<br>")
        append_flow(flow_file, "input_xml", "", "hgap_asm", "HGAP Assembly<br><smf>contigs: %s<br>assembly size: %s bp</f>" % (contig_cnt, new_genome_size), hgap_str)


        # 20 too many?
        # round to nearest 100,000
        new_genome_size = (int(new_genome_size / 100000.0 - 0.01)+1)* 100000
        log.info("- new_genome_size: %s", new_genome_size)


        # only re-run if not using an existing run
        if not seqqc:
            if int(contig_cnt) >= int(config['contig_max_limit']):

                # round to nearest 100,000
                log.info("- contig_cnt = %s, should be < %s, new_genome_size = %s", contig_cnt, config['contig_max_limit'], new_genome_size)

                status = "rerun assembly: %s" % asm_cnt
                checkpoint_step(status_log, status)
                return status, new_genome_size

            #else:
                # save the output files - done in __main__
                #save_hgap3_files(the_path)


        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)

    return status, new_genome_size

'''
Files from HGAP3

'''
def save_hgap3_files(hgap_path, hgap_fasta = None):
    log.info("save_hgap3_files: %s", hgap_path)
    if hgap_fasta:
        log.info("- hgap_fasta: %s", hgap_fasta)

    data_path = os.path.join(hgap_path, "data")


    final_path = get_the_path(FINAL_PATH, output_path)


    if hgap_fasta and os.path.isfile(hgap_fasta):
        task_path = os.path.join(hgap_path, "tasks")

        save_file(hgap_fasta, "hgap_fasta", file_dict, output_path)

        filtered_subreads_fasta = os.path.join(task_path, "pbcoretools.tasks.bam2fasta-0", "subreads.fasta")
        save_file(filtered_subreads_fasta, "filtered_subreads_fasta", file_dict, output_path)
        log.info("- saved: %s", filtered_subreads_fasta)

        # no error corrected fasta?
        png_list = glob.glob(os.path.join(hgap_path, "tasks", "*", "*.png"))

        cov_plot_cnt = 0

        for my_png in png_list:

            if my_png.endswith("_thumb.png"):
                # skip it - we don't need the thumb files
                pass
            else:
                png_name = os.path.basename(my_png)
                plot_name = "%s_%s" % ("hgap", os.path.basename(my_png).replace("-", "").replace(".png", ""))

                if png_name.startswith("coverage_plot_"):
                    cov_plot_cnt += 1
                    #my_png = os.path.join(hgap_path, "results", "coverage_plot-%s.png" % (cov_plot_cnt))
                    plot_name = "hgap_coverage_plot-%s" % (cov_plot_cnt)

                append_rqc_file(rqc_file_log, plot_name, my_png)
                log.info("- added plot: %s", my_png)

    else:

        # hgap v3 - deprecated code?
        # save polished to hgap.fasta (unzip also, samtools and mummer don't like gzipped files)
        hgap_fasta = os.path.join(data_path, "polished_assembly.fasta.gz")

        asm_fasta = "hgap.fasta"
        asm_fasta = os.path.join(final_path, asm_fasta)

        if hgap_fasta.endswith(".gz"):
            cmd = "#%s;unpigz -c %s > %s" % (config['pigz_module'], hgap_fasta, asm_fasta)
            std_out, std_err, exit_code = run_cmd(cmd, log)
        else:
            cmd = "cp %s %s" % (hgap_fasta, asm_fasta)
            std_out, std_err, exit_code = run_cmd(cmd, log)


        save_file(asm_fasta, "hgap_fasta", file_dict, output_path)



        hgap_error_corrected_fastq = os.path.join(data_path, "corrected.fastq")
        error_corrected_fastq = os.path.join(final_path, "error.corrected.reads.fastq.gz")

        if os.path.isfile(hgap_error_corrected_fastq):
            cmd = "#%s;pigz -c %s > %s" % (config['pigz_module'], hgap_error_corrected_fastq, error_corrected_fastq)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        save_file(error_corrected_fastq, "error_corrected_fastq", file_dict, output_path)

        hgap_error_corrected_fasta = os.path.join(data_path, "corrected.fasta")
        save_file(hgap_error_corrected_fasta, "error_corrected_fasta", file_dict, output_path)

        filtered_subreads_fasta = os.path.join(data_path, "filtered_subreads.fasta")
        save_file(filtered_subreads_fasta, "filtered_subreads_fasta", file_dict, output_path)

        new_filtered_subreads_fasta = os.path.join(final_path, os.path.basename(filtered_subreads_fasta))
        cmd = "cp %s %s" % (filtered_subreads_fasta, new_filtered_subreads_fasta)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # save PNGs also, do we need any other stats to replicate data from smrtportal?
        png_list = glob.glob(os.path.join(hgap_path, "results", "*.png"))

        cov_plot_cnt = 0
        var_plot_cnt = 0
        for my_png in png_list:

            if my_png.endswith("_thumb.png") or my_png.endswith("_thmb.png"):
                # skip it - we don't need the thumb files
                pass
            else:
                png_name = os.path.basename(my_png)
                plot_name = "%s_%s" % ("hgap", os.path.basename(my_png).replace("-", "").replace(".png", ""))

                # rename coverage plots and variants plot ...
                #coverage_plot_b9c24724c0f58d73b9ceadef52bdabd1.png
                if png_name.startswith("coverage_plot_"):
                    cov_plot_cnt += 1
                    #my_png = os.path.join(hgap_path, "results", "coverage_plot-%s.png" % (cov_plot_cnt))
                    plot_name = "hgap_coverage_plot-%s" % (cov_plot_cnt)

                elif png_name.startswith("variants_plot_"):
                    if png_name != "variants_plot_legend.png":
                        var_plot_cnt += 1
                        #my_png = os.path.join(hgap_path, "results", "variants_plot-%s.png" % (var_plot_cnt))
                        plot_name = "hgap_variants_plot-%s" % (var_plot_cnt)


                append_rqc_file(rqc_file_log, plot_name, my_png)
                log.info("- added plot: %s", my_png)

    # create a file indicating we are done with the hgap assembly
    hgap_done = os.path.join(output_path, "hgap_status.txt")
    fh = open(hgap_done, "w")
    fh.write("hgap is done as done can be.\n")
    fh.close()
    log.info("- created: %s", hgap_done)


'''
/global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv1000g/pb.prelim_qc.sh

pb.prelim_qc.sh:
- 4 functions
* Nucmer - check for duplications (run 2x)
x megablast - replaced with do_blast
* spike-in check with blasr (replace with daligner?)
x copy number - replaced with do_checkm

- conditions to fail pipeline here?
~ 6 minutes (mostly blasr alignment to corrected_subreads.fasta)

This step looks for repeats at the ends or small contigs (40kb-ish) that are repeats of larger contigs (65% similarity)

'''
def do_prelim_qc(library_name):
    log.info("do_prelim_qc: %s", library_name)

    step_name = "prelim_qc"
    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)


    the_path = get_the_path(PRELIM_QC_PATH, output_path)

    fasta = get_file("hgap_fasta", file_dict, output_path, log)

    output_file = os.path.join(the_path, "done")

    if os.path.isfile(output_file):
        log.info("- skipping prelim_qc, files exist already")

    else:

        fh = open(output_file, "w") # write to a "done" file what we've done

        os.chdir(the_path) # for mummerplot to work correctly

        log.info("- nucmer looking for UNIQUE matches")
        # normal stringency: nucmer looking for the unique matches only
        # check ends of each contigs for repeats
        nucmer_log = os.path.join(the_path, "nucmer.log")
        nucmer_prefix = "std.%s" % library_name
        nucmer_prefix = os.path.join(the_path, nucmer_prefix)
        # no config params for first run
        cmd = "#%s;nucmer --prefix %s %s %s > %s 2>&1" % (config['mummer_module'], nucmer_prefix, fasta, fasta, nucmer_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        mummerplot_log = os.path.join(the_path, "mummerplot.log")
        mummer_delta = "std.%s.delta" % library_name
        mummer_delta = os.path.join(the_path, mummer_delta)
        cmd = "#%s;mummerplot %s -title %s %s > %s 2>&1" % (config['mummer_module'], config['mummerplot_params'], nucmer_prefix, mummer_delta, mummerplot_log)
        # -l layout a .delta file in intelligible fashion (requires -R -Q)
        # where is my png? need to cd to the_path first
        std_out, std_err, exit_code = run_cmd(cmd, log)

        out_png = os.path.join(the_path, "out.png")
        mummer_png = os.path.join(the_path, "mummerplot.%s.png" % library_name)
        cmd = "mv %s %s" % (out_png, mummer_png)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        # produces list of coordinates, percent identity etc based on out.delta and append to nucmer_coord_csv
        tab_file = "%s.tab" % library_name
        tab_file = os.path.join(the_path, tab_file)
        # -I 90 = min pct identity 90%
        cmd = "#%s;show-coords %s -T %s | tail -n +4 > %s" % (config['mummer_module'], config['show_coords_params'], mummer_delta, tab_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        # change header from "[FRM]   [TAGS]" to "[FRM]   [TAG1]  [TAG2]"
        cmd = 'sed -i "s/\[TAGS\]/\[FRM2\]\t\[TAG1\]\t\[TAG2\]/" %s' % tab_file
        std_out, std_err, exit_code = run_cmd(cmd, log)


        cmd = "rm %s" % (os.path.join(the_path, "out*"))
        std_out, std_err, exit_code = run_cmd(cmd, log)

        fh.write("nucmer/mummer unique\n")

        # run again
        # repeat-targeted stringency: nucmer looking for all matches
        log.info("- nucmer looking for ALL matches")
        nucmer_log = os.path.join(the_path, "nucmer-all.log")
        nucmer_prefix = "std-all.%s" % library_name
        nucmer_prefix = os.path.join(the_path, nucmer_prefix)
        cmd = "#%s;nucmer %s --prefix %s %s %s > %s 2>&1" % (config['mummer_module'], config['nucmer2_params'], nucmer_prefix, fasta, fasta, nucmer_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        mummerplot_log = os.path.join(the_path, "mummerplot-all.log")
        mummer_delta = "std-all.%s.delta" % library_name
        mummer_delta = os.path.join(the_path, mummer_delta)
        cmd = "#%s;mummerplot %s -title %s %s > %s 2>&1" % (config['mummer_module'], config['mummerplot_params'], nucmer_prefix, mummer_delta, mummerplot_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        out_png = os.path.join(the_path, "out.png")
        mummer_png = os.path.join(the_path, "mummerplot-all.%s.png" % library_name)
        cmd = "mv %s %s" % (out_png, mummer_png)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        tab_file = "%s-all.tab" % library_name
        tab_file = os.path.join(the_path, tab_file)
        cmd = "#%s;show-coords %s -T %s | tail -n +4 > %s" % (config['mummer_module'], config['show_coords_params'], mummer_delta, tab_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        cmd = "rm %s" % (os.path.join(the_path, "out*"))
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # change header from "[FRM]   [TAGS]" to "[FRM]   [TAG1]  [TAG2]"
        cmd = 'sed -i "s/\[TAGS\]/\[FRM2\]\t\[TAG1\]\t\[TAG2\]/" %s' % tab_file
        std_out, std_err, exit_code = run_cmd(cmd, log)


        fh.write("nucmer/mummer all\n")



        # C2/C4 Spike-in with Blasr
        # flag if we find anything?
        fasta_list = ['corrected.fasta', 'nocontrol_filtered_subreads.fasta']
        for f in fasta_list:
            my_fasta = os.path.join(output_path, ASM_PATH, "data", f)
            log.info("- checking C2/C4 spike-in with Blasr")
            if os.path.isfile(my_fasta):
                align_summary = os.path.join(the_path, "spike.align2corr.txt")
                if f.endswith("subreads.fasta"):
                    align_summary = align_summary.replace("corr.", "filt.")

                # blasr also in smrtlink 5.0.1
                cmd = "%s %s %s -bestn 1 -nproc 7 > %s" % (config['blasr_cmd'], my_fasta, config['p4_spike_in_ref'], align_summary)
                if f.endswith("subreads.fasta"):
                    cmd = cmd.replace("-bestn 1", "-bestn 1 -header")
                std_out, std_err, exit_code = run_cmd(cmd, log)

                fh.write("blasr: %s\n" % f)

                append_rqc_file(rqc_file_log, os.path.basename(align_summary), align_summary)

            else:
                log.info("- warning: %s does not exist!  %s", my_fasta, msg_warn)




        fh.close() # done file


    if os.path.isfile(output_file):

        tab_file = "%s.tab" % library_name
        tab_file = os.path.join(the_path, tab_file)
        append_rqc_file(rqc_file_log, "nucmer_coord_csv", tab_file)

        mummer_png = os.path.join(the_path, "mummerplot.%s.png" % library_name)
        append_rqc_file(rqc_file_log, "mummer_plot", mummer_png)


        tab_file = "%s-all.tab" % library_name
        tab_file = os.path.join(the_path, tab_file)
        append_rqc_file(rqc_file_log, "nucmer_coord_csv_all", tab_file)

        mummer_png = os.path.join(the_path, "mummerplot-all.%s.png" % library_name)
        append_rqc_file(rqc_file_log, "mummer_plot_all", mummer_png)


        # get blasr version
        cmd = "%s -version" % (config['blasr_cmd'])
        blasr_ver, std_err, exit_code = run_cmd(cmd, log)

        append_flow(flow_file, "hgap_asm", "", "prelim_qc", "Prelim QC<br><smf>Duplicated strands<br>Spike-in Check</f>", "%s: nucmer, mummerplot<br>%s" % (config['mummer_module'], blasr_ver))

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)

    return status


'''
If files not here, then exit (complete) and wait for Alex to place them
if files here, process and continue

'''
def do_dedup(library_name):
    log.info("do_deduplication: %s", library_name)

    step_name = "deduplicate"
    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(DEDUP_PATH, output_path)

    fasta = get_file("hgap_fasta", file_dict, output_path, log)

    #P.fn.assembly_in        = 'polished_assembly.fasta';
    #P.fn.assembly_dedup     = 'polished_assembly.dedup.fasta';
    #P.fn.assembly_chaff     = 'polished_assembly.chaff.fasta';

    dedup_fasta = os.path.join(the_path, "polished_assembly.dedup.fasta")
    chaff_fasta = os.path.join(the_path, "polished_assembly.chaff.fasta")

    # location of Alex's deduped files - keep separated
    dedup_path = os.path.join("/global/projectb/scratch/qc_user/rqc/prod/pipelines/pbmid/dedup", library_name)
    c_tmp = os.path.join(dedup_path, "polished_assembly.chaff.fasta")
    d_tmp = os.path.join(dedup_path, "polished_assembly.dedup.fasta")


    if os.path.isfile(c_tmp):

        reformat_log = os.path.join(the_path, "chaff_reformat.log")

        # remove nulls from polish - added by windows
        # this doesn't fix it - it cuts the file at the first \00
        #c2_tmp = os.path.join(dedup_path, "p.chaff.fasta")
        #cmd = "tr < %s -d '\\000' > %s" % (c_tmp, c2_tmp)
        #std_out, std_err, exit_code = run_cmd(cmd, log)

        #null_cnt = get_null_cnt(c_tmp)
        #if int(null_cnt) > 1:
        #    log.error("- bad chaff file! %s null chars  %s", null_cnt, msg_fail)
        #    status = "hcf" # Halt and catch fire
        #    checkpoint_step(status_log, status)
        #    sys.exit(0)

        cmd = "#%s;reformat.sh in=%s out=%s ow=t > %s 2>&1" % (config['bbtools_module'], c_tmp, chaff_fasta, reformat_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        #cmd = "cp %s %s" % (c_tmp, chaff_fasta)
        #std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(d_tmp):
        reformat_log = os.path.join(the_path, "polish_dedup_reformat.log")
        #d2_tmp = os.path.join(dedup_path, "p.fasta")
        #cmd = "tr < %s -d '\\000' > %s" % (d_tmp, d2_tmp)
        #std_out, std_err, exit_code = run_cmd(cmd, log)

        null_cnt = get_null_cnt(d_tmp)
        if int(null_cnt) > 1:
            log.error("- bad polished_assembly.dedup.fasta file! %s null chars  %s", null_cnt, msg_fail)

            status = "failed" #
            checkpoint_step(status_log, status)
            sys.exit(2)

        cmd = "#%s;reformat.sh in=%s out=%s ow=t > %s 2>&1" % (config['bbtools_module'], d_tmp, dedup_fasta, reformat_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        #cmd = "cp %s %s" % (d_tmp, dedup_fasta)
        #std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(dedup_fasta):
        log.info("- skipping deduplication, file exist already")
    else:

        # Alex's R script, not ready yet

        # if no file, then stop here
        status = "%s failed" % step_name
        checkpoint_step(status_log, status)

        log.info("- stopping here, waiting for Alex's files")
        status = "hcf" # Halt and catch fire
        checkpoint_step(status_log, status)
        sys.exit(0)


    if os.path.isfile(dedup_fasta):

        fasta_type = "dedup_fasta"

        asm_stats, asm_tsv = make_asm_stats(dedup_fasta, fasta_type, the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

        save_file(dedup_fasta, fasta_type, file_dict, output_path)


        fasta_type = "chaff_fasta"
        if os.path.isfile(chaff_fasta):
            asm_stats, asm_tsv = make_asm_stats(chaff_fasta, fasta_type, the_path, rqc_file_log, log)
            save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

            save_file(chaff_fasta, fasta_type, file_dict, output_path)

            final_chaff = os.path.join(output_path, FINAL_PATH, "chaff.fasta")
            cmd = "cp %s %s" % (chaff_fasta, final_chaff)
            std_out, std_err, exit_code = run_cmd(cmd, log)


        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name



    checkpoint_step(status_log, status)
    return status

def get_null_cnt(my_file):
    null_cnt = 0
    cmd = "grep -cz '^' %s" % my_file
    std_out, std_err, exit_code = run_cmd(cmd, log)
    null_cnt = int(std_out.strip())

    return null_cnt

'''
maps inputs (corrected reads from fasta) to assembly
must have > 5 contigs mapped in the sam file over the junction with a specific length
uses ~/git/jgi-rqc-pipeline/sag/circular.py
* IMG is supposed to do something with this file but hasn't done anything yet
- trims repeats from fasta (not with -t 0 option), Alex doesn't want this trimmed
- do this after polish (use repolished_fasta instead of dedup_fasta)
'''
def do_circularize():
    log.info("do_circularize")

    step_name = "circularize"
    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)


    the_path = get_the_path(CIRCULARIZE_PATH, output_path)

    fasta = get_file("repolished_fasta", file_dict, output_path, log)
    #fasta = get_file("dedup_fasta", file_dict, output_path, log)

    topology = os.path.join(the_path, "topology.txt")
    circle_cmd = os.path.realpath(config['circularize_cmd'].replace("[my_path]", my_path))
    if os.path.isfile(topology):
        log.info("- skipping circularize, files exist already")

    else:

        circular_log = os.path.join(the_path, "circularize.log")
        corrected_fasta = get_file("error_corrected_fasta", file_dict, output_path, log)

        # -t 0: Alex asks that we don't do any trimming here
        cmd = "%s -t 0 -i %s -f %s -o %s > %s 2>&1" % (circle_cmd, corrected_fasta, fasta, the_path, circular_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(topology):

        save_topology(the_path)
        cmd = "%s -v" % circle_cmd
        std_out, circle_ver, exit_code = run_cmd(cmd, log)

        circle_cnt = 0
        fh = open(topology, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                pass
            if line.endswith("circular"):
                circle_cnt += 1
        fh.close()

        append_flow(flow_file, "polish", "", "circle", "Detect Circular Contigs<br><smf>Circular contigs: %s</f>" % circle_cnt, "RQC circular.py: %s" % circle_ver)


        status = "%s complete" % step_name
    else:
        #status = "%s failed" % step_name
        # 2018-05-14: SEQQC-12183 - pass this anyways for Alex
        status = "%s complete" % step_name



    checkpoint_step(status_log, status)
    return status



'''
Separate function so we can also call it from setup_qc_only()
'''
def save_topology(topology_path):

    circular_contig_cnt = 0
    topology_file = os.path.join(topology_path, "topology.txt")
    fh = open(topology_file, "r")
    for line in fh:
        if line.startswith("#"):
            continue
        if line.strip().endswith("circular"):
            circular_contig_cnt += 1
    fh.close()

    log.info("- circular contigs: %s", circular_contig_cnt)
    append_rqc_stats(rqc_stats_log, "circular_contig_cnt", circular_contig_cnt)

    final_topology = os.path.join(output_path, FINAL_PATH, "final.topology.txt")
    cmd = "cp %s %s" % (topology_file, final_topology)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    append_rqc_file(rqc_file_log, "topology", topology_file)

    #polished_assembly.dedup.trim.fasta
    trimmed_fasta = os.path.join(topology_path, "polished_assembly.dedup.trim.fasta")
    if os.path.isfile(trimmed_fasta):
        fasta_type = "trimmed_fasta"

        asm_stats, asm_tsv = make_asm_stats(trimmed_fasta, fasta_type, topology_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

        save_file(trimmed_fasta, fasta_type, file_dict, output_path)

        #chaff_fasta = os.path.join(topology_path, "hgap.chaff.topology.fasta")
        chaff_fasta = os.path.join(topology_path, "polished_assembly.dedup.chaff.fasta")
        fasta_type = "chaff_topology_fasta"

        asm_stats, asm_tsv = make_asm_stats(chaff_fasta, fasta_type, topology_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

        save_file(chaff_fasta, fasta_type, file_dict, output_path)

        # append topology trimmed onto final chaff.fasta
        final_chaff = os.path.join(output_path, FINAL_PATH, "chaff.fasta")
        if os.path.isfile(chaff_fasta):
            cmd = "cat %s >> %s" % (chaff_fasta, final_chaff)
            std_out, std_err, exit_code = run_cmd(cmd, log)



'''
pb.re-arrow.sh - 2017-07-24
/global/projectb/sandbox/rqc/ayspunde/pacbio/SMRTv1000g/pb.re-arrow.sh
~ 35 minutes
* if we polish here, do we need to polish in hgap?  Alex S - yes, hgap corrects 1000 -> 10000 errors
'''
def do_polish(library_name):
    log.info("do_polish")

    step_name = "polish"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(POLISH_PATH, output_path)

    #fasta = get_file("dedup_fasta", file_dict, output_path, log)

    # 2018-02-02: not creating trimmed_fasta from circularization
    fasta = get_file("trimmed_fasta", file_dict, output_path, log)
    if not fasta:
        log.info("- trimmed_fasta not available, using dedup_fasta  %s", msg_warn)
        fasta = get_file("dedup_fasta", file_dict, output_path, log)


    repolished_fasta = os.path.join(the_path, "repolished_assembly.dedup.fasta")

    if os.path.isfile(repolished_fasta):
        log.info("- skipping polishing, files exist already")

    else:

        # /global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc1707/s11253/003433-cleanH5/arrow
        ref_path_name = "reference"

        subread_path = get_the_path(SUBREAD_PATH, output_path)
        subreads_bam_path = os.path.join(subread_path, "tasks", "pbcoretools.tasks.h5_subreads_to_subread-0")

        # subreads.bam file created as process for assembly - saves 12 minutes
        if os.path.isdir(subreads_bam_path):

            # create bam.fofn
            subreads_bam_list = glob.glob(os.path.join(subreads_bam_path, "*.subreads.bam"))
            bam_fofn = os.path.join(the_path, "bam.fofn")
            fh = open(bam_fofn, "w")
            for subreads_bam in subreads_bam_list:
                fh.write(subreads_bam + "\n")
            fh.close()


        else:
            # need to create a bam file for each unique movie name
            input_fofn = os.path.join(output_path, "input.fofn")
            fh = open(input_fofn, "r")
            bam_cnt = 0
            fofn_list = []
            fhw = None

            lastf = "Rainbow" # Ritchie Blackmore's Rainbow - Live in Germany '76
            for line in fh:

                # unique file
                uf = line.strip().split(".")[-4]
                #print uf
                if uf != lastf:
                    bam_cnt += 1
                    if bam_cnt > 1:
                        fhw.close()
                    my_fofn = os.path.join(the_path, "i%s.fofn" % bam_cnt)
                    log.info("- creating %s", my_fofn)
                    fofn_list.append(my_fofn)
                    fhw = open(my_fofn, "w")

                fhw.write(line)

                lastf = uf

            # bax2 = RS2 format
            fhw.close()
            fh.close()
            bam_cnt = 0
            for fofn in fofn_list:
                bam_cnt += 1
                bax2bam_log = os.path.join(the_path, "bax2bam.%s.log" % bam_cnt)
                bax_file = os.path.join(the_path, "%s.%s.bax" % (library_name, bam_cnt))

                if os.path.isfile(bax_file + ".subreads.bam"):
                    log.info("- skipping bax2bam, found bam file")
                else:
                    # 9 minutes
                    cmd = "#%s;bax2bam -f %s -o %s > %s 2>&1" % (config['smrt_link_module'], fofn, bax_file, bax2bam_log)
                    std_out, std_err, exit_code = run_cmd(cmd, log)

            # create bam.fofn
            subreads_bam_list = glob.glob(os.path.join(the_path, "*.subreads.bam"))
            bam_fofn = os.path.join(the_path, "bam.fofn")
            fh = open(bam_fofn, "w")
            for subreads_bam in subreads_bam_list:
                fh.write(subreads_bam + "\n")
            fh.close()



        # make reference - requires samtools, sawriter - similar to heisenberg upload ref
        #Usage: fasta-to-reference [options] fasta-file output-dir name
        fasta2ref_log = os.path.join(the_path, "fasta2ref.log")
        # error if ref path exists
        if os.path.isdir(os.path.join(the_path, ref_path_name)):
            cmd = "rm -rf %s" % os.path.join(the_path, ref_path_name)
            std_out, std_err, exit_code = run_cmd(cmd, log)
        # fails if uneven line lengths, can use reformat.sh to fix
        cmd = "#%s;fasta-to-reference %s %s %s > %s 2>&1" % (config['smrt_link_module'], fasta, the_path, ref_path_name, fasta2ref_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # bam file
        nproc = get_nproc() # number of processors available to us


        # pbalign (options) inputFileName referencePath outputFileName
        # use bam.fofn from prev step
        # ~ 15 minutes
        pbalign_log = os.path.join(the_path, "pbalign.log")
        ref_file = os.path.join(the_path, ref_path_name, "sequence", "%s.fasta" % ref_path_name)
        tmp_fs = os.path.join(the_path, "tmp")
        align_file = os.path.join(the_path, "aligned_subreads.bam")
        if os.path.isfile(align_file):
            log.info("- found %s, skipping pbalign", align_file)
        else:
            cmd = "#%s;pbalign --nproc %s --tmpDir=%s %s %s %s  > %s 2>&1" % (config['smrt_link_module'], nproc, tmp_fs, bam_fofn, ref_file, align_file, pbalign_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)
        # pbalign uses blasr ...

        # 11 minutes
        arrow_log = os.path.join(the_path, "arrow.log")
        arrow_fastq = repolished_fasta.replace(".fasta", ".fastq.gz") # why do we need the fastq?
        arrow_gff = repolished_fasta.replace(".fasta", ".gff") # gff = gene finding file
        repolished_fasta_tmp = repolished_fasta.replace(".fasta", ".tmp.fasta") # before renaming contigs

        cmd = "#%s;arrow -j %s %s -r %s -o %s -o %s -o %s > %s 2>&1" % (config['smrt_link_module'], nproc, align_file, ref_file, repolished_fasta_tmp, arrow_fastq, arrow_gff, arrow_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # arrow finish


        # rename contigs with library name
        rename_log = os.path.join(the_path, "rename_scaffolds.log")

        cmd = "#%s;rename.sh in=%s out=%s prefix=%s addprefix ow=t > %s 2>&1" % (config['bbtools_module'], repolished_fasta_tmp, repolished_fasta, library_name, rename_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        cmd = "rm %s" % repolished_fasta_tmp
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(repolished_fasta):

        status = "%s complete" % step_name

        fasta_type = "repolished_fasta"
        asm_stats, asm_tsv = make_asm_stats(repolished_fasta, fasta_type, the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

        # check if genome size > 10000bp - polish might fail for random reasons
        base_cnt, contig_cnt = get_base_contig_cnt(asm_tsv)
        if base_cnt < 10000:
            log.info("- assembly is too small!  bases: %s bp  %s", base_cnt, msg_fail)
            status = "%s failed" % step_name

        save_file(repolished_fasta, fasta_type, file_dict, output_path)

        genome_size, contig_cnt = get_base_contig_cnt(asm_tsv)
        action = "%s: bax2bam, fasta-to-reference, pbalign, arrow<br>BBTools %s: rename.sh" % (config['smrt_link_module'], bbtools_ver)
        append_flow(flow_file, "dedup", "", "polish", "Polished Assembly<br><smf>contigs: %s<br>assembly size: %s bp</f>" % (contig_cnt, genome_size), action)


        final_fasta = os.path.join(output_path, FINAL_PATH, "final.assembly.fasta")
        cmd = "cp %s %s" % (repolished_fasta, final_fasta)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # chaff stats for final chaff
        fasta_type = "final_chaff_fasta"
        chaff_fasta = os.path.join(output_path, FINAL_PATH, "chaff.fasta")
        if os.path.isfile(chaff_fasta):
            asm_stats, asm_tsv = make_asm_stats(chaff_fasta, fasta_type, the_path, rqc_file_log, log)
            save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)



    else:
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)

    return status





'''
Contig gc stats
- plot histogram of gc per contig (weighted, not-weighted)
- 5 seconds
'''
def do_contig_gc_plot(qc_path, fasta_type, library_name):
    log.info("do_contig_gc_plot")

    step_name = "contig_gc_plot"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(CONTIG_GC_PATH, qc_path)

    fasta = get_file(fasta_type, file_dict, output_path, log)
    contig_gc_plot = os.path.join(the_path, "contig_gc.png")

    contig_gc_cmd = os.path.realpath(config['contig_gc_cmd'].replace("[my_path]", my_path))

    if os.path.isfile(contig_gc_plot):
        log.info("- skipping %s, found file: %s", step_name, contig_gc_plot)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:


        contig_gc_log = os.path.join(the_path, "contig_gc.log")
        cmd = "%s -l %s -f %s -o %s > %s 2>&1" % (contig_gc_cmd, library_name, fasta, the_path, contig_gc_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(contig_gc_plot):


        stats_file = os.path.join(the_path, "gc.stats.txt")
        gc_avg = 0.0
        gc_stdev = 0.0
        gc_median = 0.0
        fh = open(stats_file, "r")
        for line in fh:
            line = line.strip()
            arr = line.split("=")
            if line.startswith("gc_avg"):
                gc_avg = arr[-1]
            if line.startswith("gc_stdev"):
                gc_stdev = arr[-1]
            if line.startswith("gc_median"):
                gc_median = arr[-1]
        fh.close()

        log.info("- contig gc: %s +/- %s", gc_avg, gc_stdev)
        append_rqc_stats(rqc_stats_log, "contig_gc_avg", gc_avg)
        append_rqc_stats(rqc_stats_log, "contig_gc_stdev", gc_stdev)
        append_rqc_stats(rqc_stats_log, "contig_gc_median", gc_median)

        f_key = os.path.basename(contig_gc_plot)
        log.info("- saving file: %s", f_key)
        append_rqc_file(rqc_file_log, f_key, contig_gc_plot)

        for png_file in ['contig_hist.png', 'genome_vs_size.png']:
            append_rqc_file(rqc_file_log, png_file, os.path.join(the_path, png_file))

        # weighted gc, gc histogram files
        append_rqc_file(rqc_file_log, "contig_gc_hist", os.path.join(the_path, "contig.gc"))
        append_rqc_file(rqc_file_log, "contig_gc_whist", os.path.join(the_path, "contig-weighted.gc"))

        cmd = "%s -v" % contig_gc_cmd
        std_out, contig_gc_ver, exit_code = run_cmd(cmd)
        append_flow(flow_file, "polish", "", "contig_gc", "Contig GC Plot<br><smf>GC %%: %s +/- %s</f>" % (gc_avg, gc_stdev), "RQC contig_gc.py: %s" % contig_gc_ver)

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)


    return status



'''
GC Coverage of the contigs
- creates cloud images gc vs cov (gc_cov_Kingdom.png)
- 10 minutes
'''
def do_gc_cov(qc_path, fasta_type, library_name):
    log.info("do_gc_cov")

    step_name = "gc_cov"

    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)


    fasta = get_file(fasta_type, file_dict, output_path, log)
    filtered_subreads_fasta = get_file("filtered_subreads_fasta", file_dict, output_path, log)

    the_path = get_the_path(GC_COV_PATH, qc_path)

    summary = os.path.join(the_path, "shred", "coverage.txt")
    gc_cov_cmd = os.path.realpath(config['gc_cov_cmd'].replace("[my_path]", my_path))

    if os.path.isfile(summary):
        log.info("- skipping %s, found file: %s", step_name, summary)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:


        gc_cov_log = os.path.join(the_path, "gc_cov2.log")
        cmd = "%s -l %s -f %s -fq %s -o %s -m pacbio > %s 2>&1" % (gc_cov_cmd, library_name, fasta, filtered_subreads_fasta, the_path, gc_cov_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(summary):

        # save *.png, *.txt for RQC
        file_list = glob.glob(os.path.join(the_path, "*"))
        for f in file_list:
            if f.endswith(".png") or f.endswith(".txt"):

                f_key = os.path.basename(f)

                # need to reaname files with multiple "." to work with pdf maker (texlive)
                # e.g. class.taxonomy.png is bad, needs to be converted to class_taxonomy.pdf
                base_name = os.path.basename(f)
                # gc_cov.py: scaffolds.trim.shreds_gc_cov_no_legend.png
                if "shreds_gc_cov" in base_name and 1==2:

                    # change: trim.fasta.shreds_gc_vs_cov_multi_nolegend.png -> shreds_gc_vs_cov_multi_nolegend.png

                    arr = os.path.basename(f).split(".")
                    new_file = "%s.png" % arr[-2]
                    f_key = new_file
                    new_file = os.path.join(the_path, new_file)

                    cmd = "cp %s %s" % (f, new_file)
                    std_out, std_err, exit_code = run_cmd(cmd, log)


                    f = new_file

                log.info("- saving file: %s", f_key)
                append_rqc_file(rqc_file_log, f_key, f)

        # save coverage stats for reporting
        cov_stats = os.path.join(the_path, "covstats.txt")
        if os.path.isfile(cov_stats):
            fh = open(cov_stats, "r")
            for line in fh:
                arr = line.strip().split("=")
                if len(arr) > 1:
                    f_key = "cov_%s" % arr[0]

                    val = "{:.1f}".format(float(arr[1]))
                    append_rqc_stats(rqc_stats_log, f_key, val)

            fh.close()

        # look for anything with less than 20x coverage - Kecia's QC for possible contam: RQC-949
        gc_cov_contam_cnt = 0
        fh = open(summary, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            arr = line.strip().split()
            cov = int(float(arr[1])) # Avg_fold

            #print cov, arr[0]
            # Kecia's level:
            if cov < 20:
                gc_cov_contam_cnt += 1

        fh.close()

        append_rqc_stats(rqc_stats_log, "gc_cov_contam_cnt", gc_cov_contam_cnt)
        log.info("- gc_cov contam cnt (cov < 20): %s", gc_cov_contam_cnt)


        map_shreds_txt = os.path.join(the_path, "shred", "frag_vs_gc_cov.txt")
        if os.path.isfile(map_shreds_txt):
            append_rqc_file(rqc_file_log, "gc_cov_shreds_txt", map_shreds_txt)

        cmd = "%s -v" % gc_cov_cmd
        std_out, gc_cov_ver, exit_code = run_cmd(cmd, log)
        append_flow(flow_file, "polish", "", "gc_cov", "GC Coverage Plots", "RQC gc_cov.py: %s" % gc_cov_ver)

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)


    return status


'''
Blast the contigs
- 5+ hours (to report "no hits")

2 minutes for this part if localized just for NT ...
$ module load blast+
$ blastn -query /global/projectb/scratch/brycef/sag/BAGHS/pool_asm/pool_decontam.fasta \
 -db /scratch/rqc/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted \
 -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true   \
 -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids'   \
 -num_threads 8 > nt.log

 $ ~/git/jgi-rqc-pipeline/tools/run_blastplus.py -o /global/projectb/scratch/brycef/sag/BAGHS/megablast -q /global/projectb/scratch/brycef/sag/BAGHS/pool_asm/pool_decontam.fasta \
 -d /scratch/rqc/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted

* Alex are these blast dbs okay?:
nt
refseq.archaea
refseq.bacteria
refseq.plasmid
/global/dna/shared/rqc/ref_databases/silva/CURRENT/SSURef_tax_silva.fasta
/global/projectb/sandbox/rqc/qcdb/collab16s/collab16s.fa
- 32 threads?
'''
def do_blast(qc_path, fasta_type):
    log.info("do_blast")

    step_name = "blast"

    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(BLAST_PATH, qc_path)


    fasta = get_file(fasta_type, file_dict, output_path, log)

    done_file = os.path.join(the_path, "blastx.done")
    blast_cmd = os.path.realpath(config['run_blast_cmd'].replace("[my_path]", my_path))

    if os.path.isfile(done_file):
        log.info("- skipping %s, found file: %s", step_name, done_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        # copy database list to the run folder
        #['collab16s_ref', 'silva_16seq_ref']
        db_fof = os.path.realpath(os.path.join(my_path, "../sag/config/miid_contig_megablast.fof"))
        my_db = os.path.join(the_path, os.path.basename(db_fof))

        if not os.path.isfile(my_db):
            cmd = "cp %s %s" % (db_fof, my_db)
            std_out, std_err, exit_code = run_cmd(cmd, log)


        fh = open(my_db, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue
            if not line:
                continue

            # if find log file, can we skip this?
            blast_db = line
            blast_db_name = os.path.basename(blast_db)
            blast_log = os.path.join(the_path, "blastplus_%s.log" % blast_db_name)
            done_file = os.path.join(the_path, "blastplus_%s.done" % blast_db_name)
            if os.path.isfile(done_file):
                log.info("- skipping blast_db: %s, found file: %s", blast_db_name, done_file)

            else:
                log.info("- blasting: %s", os.path.basename(blast_db))

                # different params for collab16s  - RQCSUPPORT-2418
                #-p="-perc_identity 99 -num_threads 8 -evalue 1e-30 -task megablast -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slens'"
                blast_params = ""
                if blast_db == "collabs16s":
                    blast_params = '-p="-perc_identity 99 -num_threads 8 -evalue 1e-30 -task megablast -outfmt \'6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slens\'"'
                    
                cmd = "%s -s -o %s -q %s -d %s %s > %s" % (blast_cmd, the_path, fasta, blast_db, blast_params, blast_log)
                # -s = show taxonomy in parsed results
                std_out, std_err, exit_code = run_cmd(cmd, log)


                fhd = open(done_file, "w")
                fhd.write("%s|%s\n" % (os.path.basename(blast_db), exit_code))
                fhd.close()

        fh.close()


    if os.path.isfile(done_file):

        # save blast results, count blast hits
        blast_total_hits = 0

        blast_list = glob.glob(os.path.join(the_path, "*.vs.*.parsed"))
        # look for parsed, then save the stats from the other files
        ext_list = ['.tophit', '.taxlist'] #, '.top100hit']
        for blast_file in blast_list:

            skip_file = False
            for e in ext_list:
                if blast_file.endswith(e):
                    skip_file = True


            if not skip_file:
                blast_name = os.path.basename(blast_file)
                arr = blast_name.split(".vs.")
                blast_name = arr[-1]

                hit_cnt = get_blast_hit_count(blast_file)
                log.info("- %s (%s hits)", blast_name, hit_cnt)


                if os.path.getsize(blast_file) > 0:

                    # save *.parsed and how many hits
                    append_rqc_file(rqc_file_log, blast_name, blast_file)
                    append_rqc_stats(rqc_stats_log, "%s_cnt" % blast_name, hit_cnt)

                    for e in ext_list:

                        blast_file_new = "%s%s" % (blast_file, e)
                        blast_file_new_key = "%s%s" % (blast_name, e)

                        if os.path.isfile(blast_file_new):

                            hit_cnt = get_blast_hit_count(blast_file_new)
                            # count total hits for stats
                            if e == ".tophit":
                                blast_total_hits += hit_cnt

                            append_rqc_stats(rqc_stats_log, "%s_cnt" % blast_file_new_key, hit_cnt)

                            append_rqc_file(rqc_file_log, blast_file_new_key, blast_file_new)
                            log.info("- %s (%s hits)", blast_file_new_key, hit_cnt)

                        else:
                            append_rqc_stats(rqc_stats_log, blast_file_new_key + "_cnt", 0)
                            log.info("- %s   MISSING", os.path.basename(blast_file_new))

        append_rqc_stats(rqc_stats_log, "blast_cnt", blast_total_hits)

        cmd = "%s -v" % (blast_cmd)
        std_out, blastplus_ver, exit_code = run_cmd(cmd, log)

        append_flow(flow_file, "polish", "", "blast", "BLAST", "RQC run_blastplus.py: %s<br>Blast: %s" % (blastplus_ver, config['blast_module']))

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Blast for the 16s (pretty fast)
- in a separate function so we can run it w/o re-running all of blast steps
- auto-qc criteria?  e = 0, pct_id = 98, min_len = 500
- downloads 16s from ITS and does a blast

- also re-blasts against collabs16s - run_blastplus not picking up libs

'''
def do_blast_16s(qc_path, fasta_type, library_name):
    log.info("do_blast_16s")

    step_name = "blast_16s"

    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(BLAST_16S_PATH, qc_path)


    fasta = get_file(fasta_type, file_dict, output_path, log)

    done_file = os.path.join(the_path, "blast16s.done")
    blast_cmd = os.path.realpath(config['run_blast_cmd'].replace("[my_path]", my_path))

    collab_log = os.path.join(the_path, "collab16s.txt")

    if os.path.isfile(done_file):
        log.info("- skipping %s, found file: %s", step_name, done_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        db_16s = os.path.join(the_path, "%s_16s.fasta" % library_name)

        if not os.path.isfile(db_16s):
            get_ssu(library_name, db_16s, log)

            # makeblastdb -dbtype nucl -in AUHTB_16s.fasta
            cmd = "#%s;makeblastdb -dbtype nucl -in %s" % (config['blast_module'], db_16s)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        # N's in 16s ref?

        if os.path.getsize(db_16s) > 5:

            blast_log = os.path.join(the_path, "blastplus_16s_%s.log" % library_name)
            log.info("- blasting: 16s %s", library_name)
            # -l = don't localize this one
            cmd = "%s -l -o %s -q %s -d %s > %s" % (blast_cmd, the_path, fasta, db_16s, blast_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            if exit_code == 0:
                fh = open(done_file, "w")
                fh.write("%s_16s|%s\n" % (library_name, exit_code))
                fh.close()

        else:
            log.error("- 16s not found for %s", library_name)


        # collab16s - do_blast might not have hits, grep for "LIBS_(lib_name)"
        # run_blastplus.py doesn't get hits in collab16s
        cmd = "#%s;blastn -query %s -db %s %s > %s 2>&1" % (config['blast_module'], fasta, config['collab16s_db'], config['collab16s_blast_config'], collab_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        cmd = "sed -i '1s/^/#qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slens\\n/' %s" % collab_log
        std_out, std_err, exit_code = run_cmd(cmd, log)



    if os.path.isfile(done_file):

        # save blast results, count blast hits

        # criteria to match
        # num hits
        # expect < max_expect_score && align_len >= min_align_length && pct_id >= min_pct_id
        max_expect_score = 0.01
        min_align_length = 500
        min_pct_id = 98.0

        # only save the tophit file for 16s
        blast_list = glob.glob(os.path.join(the_path, "*.vs.*.parsed.tophit"))

        blast_file_key = "blast_16s.parsed.tophit"
        for blast_file in blast_list:

            hit_cnt = get_blast_hit_count(blast_file)
            append_rqc_stats(rqc_stats_log, blast_file_key + "_cnt", hit_cnt)

            append_rqc_file(rqc_file_log, blast_file_key, blast_file)
            log.info("- %s (%s hits)", blast_file_key, hit_cnt)

            # auto-qc to determine if we got a good 16s hit
            expect_score = 0.0
            align_length = 0
            pct_id = 0.0


            fh = open(blast_file, "r")
            for line in fh:
                if line.startswith("#"):
                    continue
                arr = line.split()
                #query subject expect length perc_id q_length s_length

                # tophit can be multi-line, use the first line - BBOSU
                if align_length == 0:
                    try:
                        expect_score = float(arr[2])
                        align_length = int(arr[3])
                        pct_id = float(arr[4])
                    except:
                        log.error("- could not get tophit data from %s", blast_file)

            fh.close()

            # auto-qc - doesn't need to be here
            blast_status = "16s not found"
            if expect_score < max_expect_score and align_length >= min_align_length and pct_id >= min_pct_id:
                blast_status = "16s found"
            log.info("- 16s: expect %s < %s, align_length %s >= %s, pct_id %s >= %s", expect_score, max_expect_score, align_length, min_align_length, pct_id, min_pct_id)


            append_rqc_stats(rqc_stats_log, "16s_hit", blast_status)


        hit_collab16s = 0 # number of good hits to 16s in collab16s
        fh = open(collab_log, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            arr = line.split()
            expect_score = float(arr[3])
            align_length = int(arr[4])
            pct_id = float(arr[5])

            if library_name in arr[1]:
                if align_length >=  min_align_length and pct_id >= min_pct_id and expect_score <= max_expect_score:
                    hit_collab16s += 1
        fh.close()
        log.info("- collab16s good hits: %s", hit_collab16s)
        append_rqc_stats(rqc_stats_log, "collab_16s_hits", hit_collab16s)


        cmd = "%s -v" % (blast_cmd)
        std_out, blastplus_ver, exit_code = run_cmd(cmd, log)
        append_flow(flow_file, "polish", "", "blast16s", "BLAST 16s", "RQC run_blastplus.py: %s<br>Blast: %s" % (blastplus_ver, config['blast_module']))


        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name
        if not os.path.getsize(db_16s) > 5:
            # skip 16s if no ssu found
            status = "%s skipped" % step_name

    checkpoint_step(status_log, status)

    return status



'''
reads_coverage - gc histograms
uses prodigal, lastal, bbtools

- reads_kingdom.png = histogram
'''
def do_gc_hist(qc_path, fasta_type, library_name):
    log.info("do_gc_hist")

    step_name = "gc_hist"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(GC_HIST_PATH, qc_path)


    contigs_class_png = os.path.join(the_path, "contigs_Class.png")
    fasta = get_file(fasta_type, file_dict, output_path, log)

    gc_hist_cmd = os.path.realpath(config['gc_hist_cmd'].replace("[my_path]", my_path))

    if os.path.isfile(contigs_class_png):
        log.info("- skipping %s, found file: %s", step_name, contigs_class_png)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        gc_hist_log = os.path.join(the_path, "gc_hist2.log")
        cmd = "%s -l %s -f %s -o %s > %s 2>&1" % (gc_hist_cmd, library_name, fasta, the_path, gc_hist_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(contigs_class_png):

        # save pngs
        png_list = glob.glob(os.path.join(the_path, "*.png"))
        for png in png_list:

            png_key = "megan_%s" % (os.path.basename(png))
            log.info("- saving file: %s", png_key)
            append_rqc_file(rqc_file_log, png_key, png)

        cmd = "%s -v" % (gc_hist_cmd)
        std_out, gc_hist_ver, exit_code = run_cmd(cmd, log)
        append_flow(flow_file, "polish", "", "gc_hist", "GC Histogram Plots", "RQC gc_hist.py: %s" % (gc_hist_ver))

        status = "%s complete" % step_name



    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Check for contigs that should not be submitted to NCBI
e.g. Cow, cat, dog, human, e.coli ...
Prescreen db from 2013: /global/dna/shared/rqc/ref_databases/qaqc/databases/ncbi_prescreen_gcontam.fa
- 2017-06-06: no new gcontam db
ftp://ftp.ncbi.nih.gov/pub/kitts/ - gcontam.gz = 2012-03-02
'''
def do_ncbi_prescreen(qc_path, fasta_type):
    log.info("do_ncbi_prescreen")

    step_name = "ncbi_prescreen"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(NCBI_PRESCREEN_PATH, qc_path)


    fasta = get_file(fasta_type, file_dict, output_path, log)
    out_file = os.path.join(the_path, "ncbi_screen.out")

    blast_cmd = os.path.realpath(config['run_blast_cmd'].replace("[my_path]", my_path))

    if os.path.isfile(out_file):
        log.info("- skipping %s, found file: %s", step_name, out_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:


        prescreen_log = os.path.join(the_path, "ncbi_prescreen.log")


        log.info("- blasting vs ncbi_prescreen: %s", library_name)

        # old version used megablast 2.2.21
        # /projectb/sandbox/rqc/prod/pipelines/external_tools/ncbiScreening/ncbiPreScreen.pl
        # megablast -p 90.0 -D3 -F \"m D\" -fT -UT -R -d (db) -i (query file)
        # -p 90.0 = identity percentage cut-off
        # -D3 = tab-delimited one line format
        # -F = filter query sequence
        # -fT = show full IDs in output
        # -UT = use lowercase filtering of fasta
        # -R = report log info
        # -d = database
        # -i = query file (in file)

        #fasta = os.path.join(the_path, "scaffolds.trim.fasta.split.fasta")

        cmd = "%s -l -o %s -q %s -d %s > %s" % (blast_cmd, the_path, fasta, config['ncbi_prescreen_db'], prescreen_log)
        # -l = don't localize this one
        # defaults to -perc_identity 90 using -task megablast
        std_out, std_err, exit_code = run_cmd(cmd, log)


        blast_file = glob.glob(os.path.join(the_path, "megablast.*.parsed"))[-1]
        hit_cnt = 0

        fhw = open(out_file, "w")

        if blast_file:

            fh = open(blast_file, "r")
            for line in fh:
                if line.startswith("#"):
                    continue

                line = line.strip()

                # is it a good hit?
                arr = line.split()
                query_len = 0
                if len(arr) > 7:
                    hit_bases = int(arr[4]) # = 4
                    query_len = int(arr[8]) # = 8

                if query_len > 0:

                    hit_pct = hit_bases / float(query_len)
                    if hit_pct > 0.75:
                        log.info("- hit to %s.  hit = %s, query_len = %s, pct = %s", arr[1], hit_bases, query_len, format_val_pct(hit_pct))
                        fhw.write(line + "\n")
                        hit_cnt += 1

            fh.close()

        hit_status = msg_fail
        if hit_cnt == 0:
            hit_status = msg_ok
            fhw.write("# No contaminants found\n")

        fhw.close()


        log.info("- hit_cnt: %s  %s", hit_cnt, hit_status)


    if os.path.isfile(out_file):

        file_key = "ncbi_prescreen_%s" % (os.path.basename(out_file))
        log.info("- saving file: %s", file_key)
        append_rqc_file(rqc_file_log, file_key, out_file)
        # should process and save results in rqc-stats.txt?  No.  We'll never find contaminates in our data.

        prescreen_cnt = 0
        fh = open(out_file, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            prescreen_cnt += 1

        fh.close()

        append_rqc_stats(rqc_stats_log, "ncbi_prescreen_cnt", prescreen_cnt)

        cmd = "%s -v" % (blast_cmd)
        std_out, blastplus_ver, exit_code = run_cmd(cmd, log)
        append_flow(flow_file, "polish", "", "ncbi_prescreen", "NCBI Screening<br><smf>hits: %s</f>" % prescreen_cnt, "RQC run_blastplus.py: %s<br>Blast: %s" % (blastplus_ver, config['blast_module']))


        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status



'''
reporting % of reads assembled in assembly (quick)
- map the subsampled fastq against the raw/screened file
- for coverage for GOLD

'''
def do_pct_reads_asm(qc_path, fasta_type):
    log.info("do_pct_reads_asm")


    step_name = "pct_reads_asm"


    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(PCT_READS_ASM_PATH, qc_path)

    ref_fasta = get_file(fasta_type, file_dict, output_path, log)
    filtered_subreads_fasta = get_file("filtered_subreads_fasta", file_dict, output_path, log)

    summary_file = os.path.join(the_path, "bbmap.stats.txt")


    if os.path.isfile(summary_file):
        log.info("- skipping %s, found file: %s", step_name, summary_file)


    elif check_skip(step_name, ref_fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        bbmap_log = os.path.join(the_path, "mapPacBio.log")
        cmd = "#%s;mapPacBio.sh ref=%s in=%s ambig=random maxindel=100 fastareadlen=500 nodisk=t ow=t machineout=t statsfile=%s > %s 2>&1" % (config['bbtools_module'], ref_fasta, filtered_subreads_fasta, summary_file, bbmap_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(summary_file):

        # save the info
        file_key = "pct_reads_asm_%s" % (os.path.basename(summary_file))
        log.info("- saving file: %s", file_key)
        append_rqc_file(rqc_file_log, file_key, summary_file)

        fh = open(summary_file, "r")

        # PB has only 1 read, Illumina = 2
        r1_map = -1 # number of reads mapped to read 1
        r_ttl = 0 # total reads

        for line in fh:
            if line.startswith("R1_Mapped_Reads"):
                r1_map = int(line.split("=")[-1])

            if line.startswith("Reads_Used"):
                r_ttl = int(line.split("=")[-1])

        fh.close()

        # pct = r1_map / total reads (if single stranded)
        pct = 0.0
        if r1_map > -1:
            log.info("- r1_map reads: %s", r1_map)
            pct = r1_map / float(r_ttl)

        log.info("- total reads: %s", r_ttl)


        pct = pct * 100

        my_key = "pct_aligned"
        append_rqc_stats(rqc_stats_log, my_key, pct)
        log.info("- pct aligned: %s", pct)


        append_flow(flow_file, "polish", "", "pct_aligned", "Alignment<br><smf>reads aligned: %s%%</f>" % "{:.2f}".format(pct), "BBTools %s: bbmap" % bbtools_ver)


        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)


    return status


'''
checkM - replaced older SCG script
 - uses a list of genes based on the taxonomoy
 - bacteria = 107, archaea = 116, both = 49
 - lwf = lineage workflow - tries to find best spot in a taxonomy tree to decide what genes to use (slower)

15 minutes
jgi-rqc-pipeline/tools/checkm.py -f (assembly) -o (outdir) -r d-bac -p -c
 -f = assembly to be evaluated
'''
def do_checkm(qc_path, fasta_type):
    log.info("do_checkm")

    step_name = "checkm"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(CHECKM_PATH, qc_path)
    fasta = get_file(fasta_type, file_dict, output_path, log)

    report_file = os.path.join(the_path, "bac", "report.txt") # just check bacteria to start with
    missing_img = os.path.realpath(os.path.join(my_path, "../sag/templates/chart_not_avail.png"))

    if os.path.isfile(report_file):
        log.info("- skipping %s, found file: %s", step_name, report_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        checkm_log = os.path.join(the_path, "checkm.log")
        checkm_wrapper = config['checkm_wrapper']


        #module load jgi-rqc; checkm_helper.py -i FASTA -o JOBDIR
        checkm_wrapper = os.path.realpath(checkm_wrapper.replace("[my_path]", my_path))
        cmd = "%s -i %s -o %s --verbose > %s 2>&1" % (checkm_wrapper, fasta, the_path, checkm_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        # using qc_path, checkm_wrapper creates the checkm subdir
        # checkm_helper runs all 3 models concurrently each with 8 threads


    if os.path.isfile(report_file):

        # need to save bac, arc, lwf
        # save summary.txt, contig_hits.txt, report.txt
        # need gene list for reporting
        for mtype in ['bac', 'arc', 'lwf']: # model types
            for f in ['report.txt', 'contig_hits.txt', 'gene_contigs.txt', 'summary.txt']:
                save_file = os.path.join(the_path, mtype, f)
                if os.path.isfile(save_file):
                    file_key = "checkm_%s_%s" % (mtype, f)

                    log.info("- saving file: %s", file_key)
                    append_rqc_file(rqc_file_log, file_key, save_file)



            # expect 3:
            # - checkm_chart_d-*.png = all counts for all genes
            # - checkm_chart_d-*_nsc.png = everything thats not single copy
            # - checkm_histogram_d-*.png = histogram count for each gene
            #png_list = glob.glob(os.path.join(the_path, mtype, "*.png"))
            png_list = ['checkm_chart_d-%s.png' % mtype, 'checkm_chart_d-%s_nsc.png' % mtype, 'checkm_histogram_d-%s.png' % mtype]
            for png in png_list:

                # not sure why arc, bac have -d but lwf does not
                if mtype == "lwf":
                    png = png.replace("_d-", "_")

                png = os.path.join(the_path, mtype, png)
                file_key = os.path.basename(png).replace(".png", "_image")

                if not os.path.isfile(png):
                    log.info("- warning, no file created for %s, using missing image  %s", png, msg_warn)
                    cmd = "cp %s %s" % (missing_img, png)
                    std_out, std_err, exit_code = run_cmd(cmd, log)

                log.info("- saving file: %s", file_key)
                append_rqc_file(rqc_file_log, file_key, png)


            # summary from report for histogram - want to get number of 0 counts, 1 counts, 2+ counts
            gene_hist = collections.Counter()

            line_cnt = 0
            report_file = os.path.join(the_path, mtype, "report.txt")
            fh = open(report_file, "r")
            for line in fh:
                line_cnt += 1

                arr = line.split()
                for a in arr:
                    if a.startswith("/"):
                        continue
                    if a.startswith("Filename"):
                        continue

                    if line_cnt == 1:
                        pass
                    if line_cnt == 2:
                        gene_hist[str(a)] += 1

            fh.close()



            hit_0 = 0
            hit_1 = 0
            hit_2 = 0 # 2 or more hits

            for g in gene_hist:

                if g == "0":
                    hit_0 = gene_hist[g]
                elif g == "1":
                    hit_1 = gene_hist[g]
                else:
                    hit_2 += gene_hist[g]


            log.info("-- model: %s - 0 hits: %s, 1 hit: %s, 2+ hits: %s", mtype, hit_0, hit_1, hit_2)

            append_rqc_stats(rqc_stats_log, "checkm_%s_hit_0" % mtype, hit_0)
            append_rqc_stats(rqc_stats_log, "checkm_%s_hit_1" % mtype, hit_1)
            append_rqc_stats(rqc_stats_log, "checkm_%s_hit_2" % mtype, hit_2)


            # save summary stats
            summary_file = os.path.join(the_path, mtype, "summary.txt")

            if not os.path.isfile(summary_file):
                create_empty_checkm_summary(summary_file)

            if os.path.isfile(summary_file):
                fh = open(summary_file, "r")
                line1 = ""
                line2 = ""
                for line in fh:

                    if line.startswith("#Filename"):
                        line1 = line.strip()
                    else:
                        line2 = line.strip()

                fh.close()

                arr1 = line1.split()
                arr2 = line2.split()

                cnt = 0
                for a in arr1:

                    if a != "#Filename":
                        my_key = "checkm_%s_%s" % (mtype, a)
                        log.info("-- %s:  %s = %s", mtype, my_key, arr2[cnt])
                        append_rqc_stats(rqc_stats_log, my_key, arr2[cnt])

                        # save as percents for pdf report, web report
                        if a in ("EstGenomeComSimple", "EstGenomeComNormalized"):
                            pct_key = "checkm_%s_%s_pct" % (mtype, a)

                            val = format_val_pct(arr2[cnt])
                            append_rqc_stats(rqc_stats_log, pct_key, val)

                    cnt += 1

            else:
                log.info("- warning, no summary file for %s  %s", mtype, msg_warn)

        append_flow(flow_file, "polish", "", "checkm", "CheckM", "Checkm")

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status



'''
Tetramer plots
- create tetramer kmers: KmerFrequencies.jar
-- replaced with: BBTools.TetramerFreq.sh in=CONTIG_FASTA out=TETRAMER_FREQ window=2000 (1 sec)
- create tetramer plot: showKmerBin.R (6 sec)
Konstantinos Mavrommatis, Dec 2011

* need at least 2 data lines in the *.kmer file to make the tetramer.jpg

'''
def do_tetramer(qc_path, fasta_type):
    log.info("do_tetramer")

    step_name = "tetramer"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(TETRAMER_PATH, qc_path)

    tetramer_jpg = os.path.join(the_path, "tetramer.jpg")
    fasta = get_file(fasta_type, file_dict, output_path, log)

    if os.path.isfile(tetramer_jpg):
        log.info("- skipping %s, found file: %s", step_name, tetramer_jpg)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        # check for 10+ contigs in fasta?

        # create kmer frequencies for each 2000bp window shifted by 500bp, kmer size = 4bp (256 total kmers)
        kmer_out = os.path.join(the_path, "pbmid.tet.kmers")
        kmer_log = os.path.join(the_path, "kmer_freq.log")

        cmd = "#%s;tetramerfreq.sh in=%s out=%s %s > %s 2>&1" % (config['bbtools_module'], fasta, kmer_out, config['tetramer_config'], kmer_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # kmer_out needs at least 3 lines to run show_kmer_bin R script (3 + 1 header)
        line_cnt = 0
        line = "" # last line read
        fh = open(kmer_out, "r")
        for _ in fh:
            line_cnt += 1
            if _:
                line = _
        fh.close()

        # if less than 4 lines then repeat last line, this makes for a dull tetramer plot but it won't exit with an error
        if line_cnt < 5:
            fh = open(kmer_out, "a")
            for _ in range(5 - line_cnt):
                fh.write(line)
            fh.close()


        tet_out = os.path.join(the_path, "tetramer") # automatically creates tetramer.jpg
        tet_log = os.path.join(the_path, "show_kmer_bin.log")
        show_kmer_bin = config['show_kmer_bin_r']
        show_kmer_bin = os.path.realpath(show_kmer_bin.replace("[my_path]", my_path))
        cmd = "#%s;Rscript --vanilla %s --input %s --output %s %s > %s 2>&1" % (config['r_module'], show_kmer_bin, kmer_out, tet_out, config['tetramer_r_config'], tet_log)
        # label was "iso - PC1 vs PC2
        # needs doturl, jgpurl - creates html version too but we ignore that
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(tetramer_jpg):

        f_key = "tetramer_jpg"
        append_rqc_file(rqc_file_log, f_key, tetramer_jpg)
        log.info("- saving file: %s", f_key)

        append_flow(flow_file, "polish", "", "tetramer", "Tetramer Plot", "RQC Tetramer")

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Antifam - run prodigal and hmmer to look for broken genes
1 minute
'''
def do_antifam(qc_path, fasta_type):
    log.info("do_antifam")

    step_name = "antifam"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(ANTIFAM_PATH, qc_path)

    antifam_report = os.path.join(the_path, "antifam.txt")

    fasta = get_file(fasta_type, file_dict, output_path, log)

    if os.path.isfile(antifam_report):
        log.info("- skipping %s, found file: %s", step_name, antifam_report)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        # min of 20000bp for prodigal
        asm_tsv = os.path.join(output_path, POLISH_PATH, "repolished_assembly.dedup.tsv")
        base_cnt, contig_cnt = get_base_contig_cnt(asm_tsv)

        min_base_cnt = int(config['prodigal_base_count'])
        if base_cnt < min_base_cnt:
            log.info("- prodigal check - base_cnt: %s >= $s  %s", base_cnt, min_base_cnt, msg_fail)
            fh = open(antifam_report, "w")
            fh.write("# cannot run prodigal\n")
            fh.write("# assembly base count: %s\n" % base_cnt)
            fh.write("# minimum prodigal base count: %s\n" % min_base_cnt)
            fh.close()

        else:

            log.info("- prodigal check - base_cnt: %s >= %s  %s", base_cnt, min_base_cnt, msg_ok)

            # prodigal outputs
            protein_fa = os.path.join(the_path, "gene_p.fa")
            nuc_fa = os.path.join(the_path, "gene_n.fa")
            prodigal_log = os.path.join(the_path, "prodigal.log")

            cmd = "#%s;prodigal -i %s -a %s -d %s -o %s" % (config['prodigal_module'], fasta, protein_fa, nuc_fa, prodigal_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            # hmmsearch: Search a protein profile HMM against a protein sequence database
            hmmer_log = os.path.join(the_path, "hmmsearch.log")
            cmd = "#%s;hmmsearch --cpu 16 --tblout %s %s %s > %s" % (config['hmmer_module'], antifam_report, config['antifam_db'], protein_fa, hmmer_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)
            # --tblout = parsable table of per-sequence hits to file <f>
            # E-value - smaller = better hit
            # score = bitscore
            # Bias = only matters when its close to the score field

    if os.path.isfile(antifam_report):

        f_key = "antifam_report"
        append_rqc_file(rqc_file_log, f_key, antifam_report)
        log.info("- saving file: %s", f_key)

        append_flow(flow_file, "polish", "", "antifam", "Antifam", "Antifam (prodigal 2.6.3, hmmer 3.1b2)")

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Use checkm, barrnap and tRNAscan-SE to determine the assembly quality
for papers (SIGS - standards in genomic sciences)
'''
def do_qual(qc_path, fasta_type):
    log.info("do_qual")

    step_name = "qual"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(QUAL_PATH, qc_path)

    quality_report = os.path.join(the_path, "quality.txt")

    fasta = get_file(fasta_type, file_dict, output_path, log)

    if os.path.isfile(quality_report):
        log.info("- skipping %s, found file: %s", step_name, quality_report)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:
        
        qual_cmd = os.path.realpath(config['qual_cmd'].replace("[my_path]", my_path))

        qual2_log = os.path.join(the_path, "qual2.log")
        cmd = "%s -f %s -o %s > %s 2>&1" % (qual_cmd, fasta, the_path, qual2_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        
    if os.path.isfile(quality_report):

        fh = open(quality_report, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue
            arr = line.split("|")
            if len(arr) == 2:
                my_key = "qual_%s" % arr[0]
                val = arr[1]
                append_rqc_stats(rqc_stats_log, my_key, val)
                if arr[0] == "quality":
                    log.info("-- assembly quality: %s", arr[1])
        fh.close()
        
        

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
All shorter reads are aligned to the seed reads, in order to generate consensus sequences with high accuracy.
We refer to these as pre-assembled reads but they can also be thought of as “error corrected” reads.

Get read stats for:
- raw: convert *.bam to fasta files
- filtered subreads: pbcoretools.tasks.bam2fasta-0/subreads.fasta
- error corrected: falcon_ns.tasks.task_falcon1_run_merge_consensus_jobs-0/preads4falcon.fasta

# converts the bam files into the raw reads - same results as using bam2fasta in smrtlink
$ shifter --image=registry.services.nersc.gov/jgi/smrtlink:5.0.1.9585 \
/smrtlink/smrtcmds/bin/pbsmrtpipe pipeline-id pbsmrtpipe.pipelines.sa3_ds_subreads_to_fastx \
--preset-xml /global/projectb/scratch/brycef/pbmid/CAOTA/config/preset.xml \
-e eid_subread:/global/projectb/scratch/brycef/pbmid/CAOTA/subread/tasks/pbcoretools.tasks.h5_subreads_to_subread-0/subreads.subreadset.xml \
-o /global/projectb/scratch/brycef/pbmid/CAOTA/fasta
9.04minutes
215359 contigs, 940,763,724 bp
RQC reports from the same library's fasta files in jamo: 187683 contigs, 871,401,075 bp

TO DO: convert to work with hgap3
'''
def do_fasta_stats(qc_path):
    log.info("get_fasta_stats")


    step_name = "fasta_stats"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(FASTA_PATH, qc_path)
    subread_path = get_the_path(SUBREAD_PATH, output_path)

    fasta = get_file(fasta_type, file_dict, output_path, log)

    done_file = os.path.join(the_path, "done.txt") # never created

    fasta_list = []

    if os.path.isfile(done_file):
        log.info("- skipping %s, found file: %s", step_name, done_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        subread_path = get_the_path(SUBREAD_PATH, output_path)
        # create subreads + scraps together to get ALL raw reads

        # http://pacbiofileformats.readthedocs.io/en/5.0/BAM.html
        # scraps.bam = Excised adapters, barcodes, and rejected subreads
        # subreads.bam = Analysis-ready subreads from movie
        bam_file_list = glob.glob(os.path.join(subread_path, "tasks", "pbcoretools.tasks.h5_subreads_to_subread-0", "*.bam"))


        log.info("- found %s bam files to convert", len(bam_file_list))
        for bam_file in bam_file_list:
            bam_log = os.path.join(the_path, os.path.basename(bam_file).replace(".bam", ".log"))

            fasta_file = os.path.join(the_path, os.path.basename(bam_file).replace(".bam", "")) # don't add .fasta, done automatically

            if os.path.isfile(fasta_file + ".fasta"):
                log.info("- found fasta: %s.fasta", fasta_file)

            else:
                cmd = "#%s;bam2fasta -u -o %s %s > %s 2>&1" % (config['smrt_link_module'], fasta_file, bam_file, bam_log)
                # -u = don't gzip it
                std_out, std_err, exit_code = run_cmd(cmd, log)

            fasta_list.append(fasta_file + ".fasta")



    if len(fasta_list) > 0:

        # raw reads
        log.info("- raw reads: %s fasta files", len(fasta_list))
        get_contig_length(fasta_list, "raw_reads")

        # filtered subreads - part of hgap4, also the *subreads.bam from subreads
        fasta = get_file("filtered_subreads_fasta", file_dict, output_path, log)
        fasta_list = [fasta]

        log.info("- filtered subreads: %s fasta files", len(fasta_list))
        get_contig_length(fasta_list, "filtered_subreads")

        # error corrected reads
        fasta = get_file("error_corrected_fasta", file_dict, output_path, log)
        fasta_list = [fasta]

        log.info("- error corrected reads: %s fasta files", len(fasta_list))
        get_contig_length(fasta_list, "error_corrected_reads")

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Get read stats for:
- raw input: convert h5 to fastas, concat together to get stats
- error corrected: corrected.fastq
- input reads: filtered_subreads.fastq

Need:
Read Count
Base Count
Average Read Length
- also stats for reads > 5000bp

'''
def do_fasta_stats_hgap3(qc_path):
    log.info("get_fasta_stats_hgap3")


    step_name = "fasta_stats"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(FASTA_PATH, qc_path)


    fasta = get_file(fasta_type, file_dict, output_path, log)

    done_file = os.path.join(the_path, "done.txt")
    fasta_list = []
    h5_list = []
    # convert the .bax.h5's into .fasta files
    input_fofn = os.path.join(output_path, "input.fofn")
    fh = open(input_fofn, "r")
    for line in fh:
        line = line.strip()
        if line.startswith("#"):
            continue

        if line:
            fasta = os.path.join(the_path, os.path.basename(line).replace(".bax.h5", ".fasta"))
            fasta_list.append(fasta)
            h5_list.append(line)
    fh.close()


    if os.path.isfile(done_file):
        log.info("- skipping %s, found file: %s", step_name, done_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        #for fasta in fasta_list:
        for h5 in h5_list:
            fasta = os.path.join(the_path, os.path.basename(h5).replace(".bax.h5", ".fasta"))
            if os.path.isfile(fasta):
                log.info("- skipping %s, already exists  %s", fasta, msg_warn)
            else:
                pls2fasta_log = os.path.join(the_path, fasta + ".log")
                cmd = "#%s;pls2fasta %s %s > %s 2>&1" % (config['smrt_analysis_module'], h5, fasta, pls2fasta_log)
                std_out, std_err, exit_code = run_cmd(cmd, log)



        # if done converting
        fh = open(done_file, "w")
        fh.write("all done\n")
        fh.close()


    if os.path.isfile(done_file):

        # get stats for fasta list
        get_contig_length(fasta_list, "raw_reads")


        # filtered subreads
        fasta = get_file("filtered_subreads_fasta", file_dict, output_path, log)
        fasta_list = [fasta]


        get_contig_length(fasta_list, "filtered_subreads")

        # error corrected reads
        fasta = get_file("error_corrected_fasta", file_dict, output_path, log)
        fasta_list = [fasta]

        get_contig_length(fasta_list, "error_corrected_reads")


        # save stats
        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status

'''
return the length of all contigs from the fasta
checked against BWOAU and it was correct
'''
def get_contig_length(fasta_list, stats_key):
    log.info("get_contig_length: %s", fasta_list)

    contig_length_list = []
    contig_big_list = [] # all contigs > big_size are in this list

    # config = 5000bp
    big_size = int(config['contig_length_big']) # contigs > 5000 bases are likely to have the 16s sequence coverage high enough - indicates sequencing quality
    for fasta in fasta_list:
        if os.path.isfile(fasta):

            contig_length = 0
            fh = open(fasta, "r")

            for line in fh:
                line = line.strip()
                if line.startswith(">"):
                    if contig_length > 0:
                        contig_length_list.append(contig_length)
                        if contig_length > big_size:
                            contig_big_list.append(contig_length)
                        contig_length = 0
                else:
                    contig_length += len(line)

            if contig_length > 0:
                contig_length_list.append(contig_length)
                if contig_length > big_size:
                    contig_big_list.append(contig_length)

            fh.close()
        else:
            log.error("- missing file: %s", fasta)

    read_cnt = len(contig_length_list)
    read_length_avg = numpy.average(contig_length_list)
    read_length_stdev = numpy.std(contig_length_list)
    base_cnt = numpy.sum(contig_length_list)
    log.info("- read count: %s", "{:,}".format(read_cnt))
    log.info("- base count: %s", "{:,}".format(base_cnt))
    log.info("- avg read length: %s +/- %s", "{:.1f}".format(read_length_avg), "{:.1f}".format(read_length_stdev))


    # save stats

    append_rqc_stats(rqc_stats_log, stats_key + "_read_cnt", read_cnt)
    append_rqc_stats(rqc_stats_log, stats_key + "_base_cnt", base_cnt)
    append_rqc_stats(rqc_stats_log, stats_key + "_avg_read_length", read_length_avg)
    append_rqc_stats(rqc_stats_log, stats_key + "_stdev_read_length", read_length_stdev)

    read_cnt = len(contig_big_list)
    read_length_avg = numpy.average(contig_big_list)
    read_length_stdev = numpy.std(contig_big_list)
    base_cnt = numpy.sum(contig_big_list)


    log.info("- read count > %sbp: %s", big_size, "{:,}".format(read_cnt))
    log.info("- base count > %sbp: %s", big_size, "{:,}".format(base_cnt))
    log.info("- avg read length > %sbp: %s +/- %s", big_size, "{:.1f}".format(read_length_avg), "{:.1f}".format(read_length_stdev))

    # save stats
    append_rqc_stats(rqc_stats_log, stats_key + "_big_read_cnt", read_cnt)
    append_rqc_stats(rqc_stats_log, stats_key + "_big_base_cnt", base_cnt)
    append_rqc_stats(rqc_stats_log, stats_key + "_big_avg_read_length", read_length_avg)
    append_rqc_stats(rqc_stats_log, stats_key + "_big_stdev_read_length", read_length_stdev)



## ~~~~~~~~~~~~~~~~~~~
## PDF, TXT reporting

'''
Use pb-mid.tex, pbmid.metadata.json, pbmid.txt
- images in the pdf report can only have one "." in the name, e.g. my_thing.fa.chart.png won't work.  Need to rename the file
- writes to report
* if re-run change rqc-files.txt* & rqc-stats.txt* to *.tmp
'''
def do_pdf_report(library_name, fasta_type):
    log.info("do_pdf_report: %s", library_name)

    step_name = "pdf_report"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)



    the_path = get_the_path(REPORT_PATH, output_path)

    # output file names
    #pdf_report = os.path.join(the_path, "pbmid-%s.pdf" % (library_name))
    pdf_report = os.path.join(the_path, "QC.report.%s.pdf" % (library_name))
    latex_report = pdf_report.replace(".pdf", ".tex")
    #txt_report = pdf_report.replace(".pdf", ".txt")
    txt_report = os.path.join(the_path, "QD.report.%s.txt" % (library_name))
    # jat metadata.json in output_path because files are relative to output_path
    jat_metadata = os.path.join(output_path, "pbmid.metadata.json")

    if os.path.isfile(pdf_report) and 1 == 2:
        log.info("- skipping %s, found file: %s", step_name, pdf_report)

    elif check_skip(step_name, "run_me", config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:
        # setup merge dictionary to use for the merging
        merge_dict = get_library_info(library_name, log)

        merge_dict['organism_name'] = merge_dict['ncbi_organism_name']
        if 'gold_organism_name' in merge_dict and merge_dict['gold_organism_name']:
            merge_dict['organism_name'] = merge_dict['gold_organism_name']

        merge_dict['jgi_logo'] = os.path.realpath(os.path.join(my_path, "../sag/templates", "jgi_logo.jpg"))
        merge_dict['library_name'] = library_name

        # using HGAP, not spades
        merge_dict['assembler_name'] = "HGAP"
        #merge_dict['assembler_params'] = "" # - its the whole hgap.xml
        merge_dict['assembler_version'] = config['hgap_version_str']

        timestamp = time.strftime("%Y%m%d")
        merge_dict['assembly_v'] = "v.%s" % timestamp

        # input files for metadata.json
        input_h5_list = []
        input_fofn = os.path.join(output_path, "all_input.fofn") # RQC-1068 - Alex wants all the files - except fastq, fasta

        fh = open(input_fofn)
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue
            # skip fastq files
            if line.endswith(".fastq"):
                continue
            # skip regular fasta, include *-nocontrol.fasta
            if line.endswith(".fasta") and "control" not in line:
                continue
            if line:
                input_h5_list.append(line.strip())
        fh.close()
        merge_dict['h5_list'] = json.dumps(input_h5_list) # json.dumps to convert ['A', 'B'] to ["A", "B"]

        merge_dict['report_date'] = time.strftime("%x") # %x = date in local format

        tapid = config['analysis_project_type_id']
        tatid = config['analysis_task_type_id']

        merge_dict['analysis_project_id'], merge_dict['analysis_task_id'] = get_analysis_project_id(merge_dict['seq_proj_id'], tapid, tatid, the_path, log)


        # read in stats & files into merge dict
        for f in [rqc_stats_log, rqc_file_log]:

            log.info("- importing: %s", f)

            fh = open(f, "r")

            for line in fh:
                #print line
                if line.startswith("#"):
                    continue
                arr = line.split("=")

                if len(arr) > 1:
                    my_key = str(arr[0]).lower().strip()
                    my_val = str(arr[1]).strip()
                    merge_dict[my_key] = my_val
                    #print my_key, my_val, merge_dict[my_key]
            fh.close()


        # if no scaffolds then write a text file
        fail_flag = False
        check_key = "%s_scaf_bp" % fasta_type
        check_val = 0
        if check_key in merge_dict:
            check_val = int(merge_dict[check_key])
        else:
            fail_flag = True

        if check_val < 5:
            fail_flag = True

        log.info("- check key %s = %s > 5", check_key, check_val)




        if fail_flag:

            fh = open(txt_report, "w")
            fh.write("Report date: %s\n" % merge_dict['report_date'])
            fh.write("PBMID did not produce any clean contigs/scaffolds for library %s.\n" % (merge_dict['library_name']))
            fh.close()

            append_rqc_file(rqc_file_log, os.path.basename(txt_report), txt_report)
            log.error("- %s = %s, not creating reports  %s", check_key, check_val, msg_fail)
            status = "%s skipped" % step_name
            checkpoint_step(status_log, status)

            return status






        # pull in stats for the assembly based on the fasta_type
        asm_keys = ["n_scaffolds", "n_contigs", "scaf_bp", "contig_bp", "scaf_n50", "scaf_l50", "ctg_n50", "ctg_l50", "scaf_max", "ctg_max", "scaf_n_gt50k", "scaf_pct_gt50k", "gap_pct",
                    "gc_avg", "gc_std"]
        for asm_stat in asm_keys:
            the_key = "%s_%s" % (fasta_type, asm_stat)

            if the_key in merge_dict:
                #print the_key, merge_dict[the_key]
                merge_dict[asm_stat] = merge_dict[the_key]

        # green_genes 16s RNA - use ssu tophits
        merge_dict['green_genes_table'] = ""
        merge_dict['green_genes_table'] += green_genes_pdf(merge_dict.get('ssuref_tax_silva.parsed.tophit'), log)
        #print merge_dict['green_genes_table']
        #sys.exit(44)

        if merge_dict['green_genes_table'] == "":
            merge_dict['green_genes_table'] = "\\\\No hits to the reference.\\\\"


        # stats for txt file
        asm_stats_file = os.path.join(output_path, POLISH_PATH, "repolished_assembly.dedup.txt")

        asm_stats_buffer = ""
        fh = open(asm_stats_file, "r")
        for line in fh:
            asm_stats_buffer += line
        fh.close()
        merge_dict['asm_stats_file'] = asm_stats_buffer


        # ncbi prescreen - text file only
        ncbi_key = "ncbi_prescreen_cnt"
        ncbi_file_key = "ncbi_prescreen_ncbi_screen.out"

        merge_dict['ncbi_contamination'] = ncbi_prescreen_report(merge_dict[ncbi_file_key], merge_dict[ncbi_key], log)


        # checkm doesn't always produce output
        for checkm_type in ['arc', 'bac', 'lwf']:
            checkm_key = "checkm_%s_foundscgenes" % (checkm_type)
            if checkm_key not in merge_dict:
                log.info("- checkm %s data not found, setting to N/A  %s", checkm_type, msg_warn)
                for checkm_key in ['checkm_'+checkm_type+'_foundscgenes', 'checkm_'+checkm_type+'_totalscgenes', 'checkm_'+checkm_type+'_estgenomecomsimple_pct',
                                   'checkm_'+checkm_type+'_estgenomecomnormalized_pct']:
                    merge_dict[checkm_key] = "N/A"

                # image for charts not created
                if checkm_type == "lwf":
                    for checkm_key in ['checkm_chart_'+checkm_type+'_image', 'checkm_histogram_'+checkm_type+'_image']:
                        merge_dict[checkm_key] = os.path.realpath(os.path.join(my_path, "../sag/templates/chart_not_avail.png"))
                else:
                    for checkm_key in ['checkm_chart_d-'+checkm_type+'_image', 'checkm_histogram_d-'+checkm_type+'_image']:
                        merge_dict[checkm_key] = os.path.realpath(os.path.join(my_path, "../sag/templates/chart_not_avail.png"))


        # debug2
        #for k in sorted(merge_dict.iterkeys()):
        #    print "* '%s' = '%s'" % (k, merge_dict[k])


        ## PDF & TXT report
        # read template
        my_template = "pbmid.tex"
        my_txt_template = "pbmid.txt"
        my_json_template = "pbmid.metadata.json"


        latex_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_template))
        txt_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_txt_template))
        json_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_json_template))



        # metadata.json for JAT
        # written to output_path, files are relative to output_path
        merge_dict['pdf_report'] = "report/%s" % os.path.basename(pdf_report)
        merge_dict['txt_report'] = "report/%s" % os.path.basename(txt_report)
        merge_dict['topology_file'] = "%s/final.topology.txt" % FINAL_PATH
        merge_dict['error_corrected_fastq'] = "%s/error.corrected.reads.fastq.gz" % FINAL_PATH
        merge_dict['chaff_fasta'] = "%s/chaff.fasta" % FINAL_PATH
        merge_dict['hgap_settings_xml'] = "config/hgap_settings.xml"
        merge_dict['assembly_fasta'] = "%s/final.assembly.fasta" % FINAL_PATH
        merge_dict['filtered_subreads_fasta'] = "%s/final.fasta" % FINAL_PATH
        fq_list = merge_dict['fastq_list'].split(",")
        merge_dict['seq_unit_list'] = json.dumps(fq_list)

        # use chaff file from circular.py (hgap.chaff.fasta) or create empty one if its not there
        chaff_file = os.path.join(output_path, FINAL_PATH, "chaff.fasta")

        # if chaff is empty then don't include it (RQCSUPPORT-2408)
        merge_dict['chaff_json'] = ""
        #merge_dict['final_chaff_fasta_contig_bp'] = 0 #test
        if 'final_chaff_fasta_contig_bp' in merge_dict:
            if merge_dict['final_chaff_fasta_contig_bp'] > 0:
                if os.path.isfile(chaff_file):
                    chaff_file = "%s/chaff.fasta" % FINAL_PATH
                    merge_dict['chaff_json'] = """
        {
            "label":"chaff_fasta",
            "file":"%s",
            "metadata" : { "n_contigs" : %s, "contig_bp" : %s }
        },""" % (chaff_file, merge_dict['final_chaff_fasta_n_contigs'], merge_dict['final_chaff_fasta_contig_bp'])

                    #if not os.path.isfile(chaff_file):
                        #cmd = "touch %s" % chaff_file
                        #std_out, std_err, exit_code = run_cmd(cmd, log)


        # create new [at_id].pbmid.metadata.json: RQCSUPPORT-1426
        jat_metadata = os.path.join(output_path, "%s.pbmid.metadata.json" % merge_dict['analysis_task_id'])
        merge_template(json_template_file, jat_metadata, merge_dict, log)


        # symlink to old file (jat_metadata)
        jat_metadata_new = "pbmid.metadata.json"
        if os.path.isfile(os.path.join(output_path, jat_metadata_new)):
            cmd = "rm %s" % os.path.join(output_path, jat_metadata_new)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        cmd = "cd %s;ln -s %s %s" % (output_path, os.path.basename(jat_metadata), jat_metadata_new)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # formatting pct, int after metadata.json written ...

        # formatting integers to have ,'s 1000000 -> 1,000,000
        format_list_int = ["contig_bp", "scaf_bp", "scaf_max", "ctg_max",
                        "raw_reads_read_cnt", "filtered_subreads_read_cnt", "error_corrected_reads_read_cnt",
                        "raw_reads_base_cnt", "filtered_subreads_base_cnt", "error_corrected_reads_base_cnt",
                        "raw_reads_big_read_cnt", "filtered_subreads_big_read_cnt", "error_corrected_reads_big_read_cnt",
                        "raw_reads_big_base_cnt", "filtered_subreads_big_base_cnt", "error_corrected_reads_big_base_cnt"]

        for k in format_list_int:

            if k in merge_dict:
                try:
                    # convert to mb or kb or gb or ... ?
                    merge_dict[k] = "{:,}".format(int(merge_dict[k]))
                except:
                    merge_dict[k] = "{:,}".format(int(0))

        # plural for scaffold, contig count
        merge_dict['pl'] = ""
        if int(merge_dict['n_scaffolds']) > 1:
            merge_dict['pl'] = "s"

        # floats to .1
        format_float_list = ['raw_reads_avg_read_length', 'raw_reads_stdev_read_length', 'raw_reads_big_avg_read_length',
                             'raw_reads_big_stdev_read_length', 'filtered_subreads_avg_read_length', 'filtered_subreads_stdev_read_length',
                             'filtered_subreads_big_avg_read_length', 'filtered_subreads_big_stdev_read_length', 'error_corrected_reads_avg_read_length',
                             'error_corrected_reads_stdev_read_length', 'error_corrected_reads_big_avg_read_length', 'error_corrected_reads_big_stdev_read_length']


        for my_key in format_float_list:
            merge_dict[my_key] = "{:,.1f}".format(float(merge_dict[my_key]))


        # 0.2555 -> 25.55%
        format_list_pct = ["scaf_pct_gt50k", "pct_aligned"]


        # convert from 0.xxyy to xx.yy%
        for k in format_list_pct:

            if k in merge_dict:
                merge_dict[k] = format_val_pct(merge_dict[k])



        # txt report
        merge_template(txt_template_file, txt_report, merge_dict, log)


        # PDF/latex
        err_cnt = merge_template(latex_template_file, latex_report, merge_dict, log)
        if err_cnt > 0:
            log.error("- fatal error: missing keys for pdf report!  %s", msg_fail)
            sys.exit(8)

        # convert .tex to .pdf
        # - okay if non-zero exit code
        pdflatex_log = os.path.join(the_path, "pdflatex.log")

        cmd = "#texlive;pdflatex -interaction=nonstopmode -output-directory=%s %s > %s 2>&1" % (the_path, os.path.basename(latex_report), pdflatex_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(pdf_report) and os.path.isfile(txt_report):
        append_rqc_file(rqc_file_log, "pbmid_report_pdf", pdf_report)
        append_rqc_file(rqc_file_log, "pbmid_report_txt", txt_report)

        status = "%s complete" % step_name

        # link inputs to reports from other functions
        append_flow(flow_file, "circle", "", "pdf_report", "Project Reports<br><smf>JAT metadata.json<br>PDF Report<br>Text Report</f>", "")
        append_flow(flow_file, "contig_gc", "", "pdf_report", "", "")
        append_flow(flow_file, "gc_hist", "", "pdf_report", "", "")
        append_flow(flow_file, "gc_cov", "", "pdf_report", "", "")
        append_flow(flow_file, "ncbi_prescreen", "", "pdf_report", "", "")
        append_flow(flow_file, "checkm", "", "pdf_report", "", "")
        append_flow(flow_file, "tetramer", "", "pdf_report", "", "")
        append_flow(flow_file, "pct_aligned", "", "pdf_report", "", "")
        append_flow(flow_file, "antifam", "", "pdf_report", "", "")


        # rwxrwxr-x for certain files, need leading 0 - Alex S needs to be able to edit them
        os.chmod(txt_report, 0775)
        os.chmod(latex_report, 0775)
        os.chmod(jat_metadata, 0775)

        # python 3 = os.chmod('somefile', 0o755)

    else:
        status = "%s failed" % step_name
        log.error("- failed to create %s  %s", pdf_report, msg_fail)

    checkpoint_step(status_log, status)


    return status



'''
Clean up
?
'''
def do_purge():
    log.info("do_purge")

    step_name = "purge"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    status = "%s complete" % step_name

    checkpoint_step(status_log, status)

    return status



## ----------------------
## control/helper functions



'''
Get the next step to run
'''
def get_next_status(status):
    log.info("get_next_step: %s", status)

    next_status = None

    # this could be part of the config file
    status_dict = {
        # assembly
        "start" : "setup_input_xml",
        "setup_input_xml" : "setup_hgap_xml",
        "setup_hgap_xml" : "assembly",
        "setup_pbsmrtpipe" : "setup_hgap_xml",
        #"setup_hgap_xml" : "assembly",

        "assembly" : "prelim_qc",
        "prelim_qc" : "deduplicate",
        "deduplicate" : "polish",
        "polish" : "circularize",
        "circularize" : "contig_gc_plot",

        # v 5.1: dedup doesn't do anything, moved circularize before polishing
        #"prelim_qc" : "deduplicate",
        #"deduplicate" : "polish",
        #"polish" : "circularize",
        #"circularize" : "contig_gc_plot",

        # reporting
        "contig_gc_plot" : "gc_cov",
        "gc_cov" : "blast",
        "blast" : "blast_16s",
        "blast_16s" : "gc_hist",
        "gc_hist" : "ncbi_prescreen",
        "ncbi_prescreen" : "pct_reads_asm",
        "pct_reads_asm" : "checkm",
        "checkm" : "tetramer",
        "tetramer" : "antifam",
        "antifam" : "fasta_stats",
        "fasta_stats" : "qual",
        "qual" : "pdf_report",


        # pdf, txt, metadata.json
        "pdf_report" : "purge",

        "purge" : "complete",

        "failed" : "failed",
        "complete" : "complete",
    }



    if status:

        if status.lower() != "complete":
            status = status.lower().replace("skipped", "").replace("complete", "").strip()

        # gc_cov:screened -> gc_cov
        if ":" in status:
            status = status.split(":")[0]

        if status in status_dict:
            next_status = status_dict[status]

        else:
            if status.endswith("failed"):
                next_status = "failed"

            else:
                next_status = "failed: next step missing for '%s'" % (status)
                log.error("Cannot get the next step for '%s'  %s", status, msg_fail)
                sys.exit(12)



    log.info("- next step: %s%s%s -> %s%s%s", color['pink'], status, color[''], color['cyan'], next_status, color[''])

    if stop_flag:
        next_status = "stop"

    # debugging
    if next_status == "stop":
        log.info("- stopping pipeline, stop flag is set")
        sys.exit(13)


    return next_status


'''
Alex already created the assembly & manually curated it
- need settings.xml, files from hgap run (data), settings.xml
- copy ./settings.xml to config/hgap_settings.xml
- ./input.xml to config/input-(lib name).xml
x ./quiver/repolished_assembly.dedup.fasta to final, also do stats in quiver
- ./arrow/repolished_assembly.dedup.fasta to final, also do stats in arrow


'''
def setup_qc_only(seqqc_path, library_name):
    log.info("setup_qc_only: %s", seqqc_path)


    if os.path.isdir(seqqc_path):

        save_hgap3_files(seqqc_path) # assumes hgap3

        polish_path = os.path.join(output_path, "arrow") # vs quiver
        qc_fasta = os.path.join(polish_path, "repolished_assembly.dedup.fasta")

        # rename contigs with library name and copy to final path
        if os.path.isfile(qc_fasta):
            final_path = get_the_path(output_path, FINAL_PATH)

            fasta_type = "repolished_fasta"
            asm_stats, asm_tsv = make_asm_stats(qc_fasta, fasta_type, polish_path, rqc_file_log, log)
            save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

            final_path = get_the_path(output_path, FINAL_PATH)
            repolished_fasta = os.path.join(final_path, "final.fasta")
            rename_log = os.path.join(final_path, "rename_scaffolds.log")

            cmd = "#%s;rename.sh in=%s out=%s prefix=%s addprefix ow=t > %s 2>&1" % (config['bbtools_module'], qc_fasta, repolished_fasta, library_name, rename_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            asm_stats, asm_tsv = make_asm_stats(repolished_fasta, fasta_type, final_path, rqc_file_log, log)
            save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

            save_file(repolished_fasta, fasta_type, file_dict, output_path)
        else:
            log.error("- cannot find file: %s  %s", qc_fasta, msg_fail)
            sys.exit(2)


        # these files are needed for do_pdf (metadata.json)
        settings_xml = os.path.join(seqqc_path, "settings.xml")
        if os.path.isfile(settings_xml):
            new_settings_xml = os.path.join(output_path, CONFIG_PATH, "hgap_settings.xml")
            cmd = "cp %s %s" % (settings_xml, new_settings_xml)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            log.error("- cannot find file: %s  %s", settings_xml, msg_fail)
            sys.exit(2)

        input_xml = os.path.join(seqqc_path, "input.xml")

        if os.path.isfile(input_xml):
            new_input_xml = os.path.join(output_path, CONFIG_PATH, "input-%s.xml" % library_name)
            cmd = "cp %s %s" % (input_xml, new_input_xml)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            log.error("- cannot find file: %s  %s", input_xml, msg_fail)
            sys.exit(2)

        # if doesn't exist, then create it
        input_fofn = os.path.join(seqqc_path, "input.fofn")
        if os.path.isfile(input_fofn):
            new_input_fofn = os.path.join(output_path, "input.fofn")
            # okay if its the same folder and it cannot copy the file onto itself
            cmd = "cp %s %s" % (input_fofn, new_input_fofn)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            h5_cnt = 0
            log.info("- creating: %s", input_fofn)
            fhw = open(input_fofn, "w")
            fh = open(input_xml, "r")
            pattern_h5 = re.compile('<location>(.*)</location>', re.IGNORECASE)
            for line in fh:
                match = pattern_h5.match(line.strip())
                if match:
                    h5 = match.group(1)
                    log.info("--- h5: %s", h5)
                    fhw.write("%s\n" % h5)
                    h5_cnt += 1

            fhw.close()
            fh.close()

            if h5_cnt == 0:
                log.error("- cannot find h5 files in: %s  %s", input_xml, msg_fail)
                sys.exit(2)

        # grab topology.txt too
        topology = os.path.join(seqqc, "circle", "topology.txt")
        if os.path.isfile(topology):
            save_topology(topology)
        else:
            log.error("- topology missing: %s  %s", topology, msg_fail)
            sys.exit(2)

    else:
        log.error("- cannot find qc path: %s  %s", seqqc, msg_fail)
        sys.exit(2)


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "PBMID"
    version = "5.1.15"



    # path names
    CONFIG_PATH = "config"
    ASM_PATH = "hgap"
    SUBREAD_PATH = "subread"
    PRELIM_QC_PATH = "prelim_qc"
    DEDUP_PATH = "dedup"
    POLISH_PATH = "arrow"
    CIRCULARIZE_PATH = "circle"

    TETRAMER_PATH = "tetramer"
    CHECKM_PATH = "checkm"
    PCT_READS_ASM_PATH = "pct_reads_assembled"
    NCBI_PRESCREEN_PATH = "ncbi_prescreen"
    GC_HIST_PATH = "gc_hist"
    BLAST_PATH = "megablast"
    BLAST_16S_PATH = "blast_16s"
    GC_COV_PATH = "gc_cov"
    CONTIG_GC_PATH = "contig_gc"
    ANTIFAM_PATH = "antifam"
    QUAL_PATH = "qual"
    FASTA_PATH = "fasta"
    REPORT_PATH = "report"
    FINAL_PATH = "final"



    DEFAULT_CONFIG_FILE = os.path.realpath(os.path.join(my_path, "../sag/", "config", "pb-mid.cfg"))


    uname = getpass.getuser()


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)

    # Parse options
    usage = "pbmid.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)


    parser = ArgumentParser(usage=usage)

    parser.add_argument("-l", "--lib", dest="library_name", help="Library name (required)")
    parser.add_argument("-o", "--output-path", dest="output_path", help="Output path to write to, uses pwd if not set")
    parser.add_argument("-g", "--genome-size", dest="genome_size", help="Estimated genome size (default 5m bp)")
    parser.add_argument("-c", "--config", dest="config_file", help="Location of pb-mid.cfg file to use")
    parser.add_argument("-seqqc", "--seq-qc", dest="seqqc", help="Location of previous run")
    parser.add_argument("--qc-only", dest="qc_only", default=False, action="store_true", help="Run the QC steps only using --output-path as the input")
    parser.add_argument("--qc-fasta", dest="qc_fasta", help="Use this fasta for the QC reporting (final.fasta)")

    parser.add_argument("-4", "--hgap4", dest="hgap4", default=False, action="store_true", help="Use HGAP4 (HGAP3 = default)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default=False, action="store_true", help="print log to screen")
    #parser.add_argument("-s", "--step", dest="step", help="Step to start running on")
    parser.add_argument("-v", "--version", action="version", version=version)


    status = "start" # step + X = run step and exit?  (e.g. gc_covX)  -- to do
    output_path = None
    library_name = None
    config_file = None
    stop_flag = False # stop after running a step, e.g. gc_cov_stop
    genome_size = 5000000 # 5m bp

    
    
    hgap4 = False # run hgap4, hgap3 default

    seqqc = None # path to Alex's seqqc run to skip running the assembly
    qc_only = False
    qc_fasta = None



    # parse args
    args = parser.parse_args()

    print_log = args.print_log
    hgap4 = args.hgap4 # not tested
    qc_only = args.qc_only

    if args.output_path:
        output_path = args.output_path

    if args.library_name:
        library_name = args.library_name.upper()

    if args.seqqc:
        seqqc = args.seqqc

    if args.qc_fasta:
        qc_fasta = args.qc_fasta



    if args.genome_size:
        genome_size = int(args.genome_size)
        # max = 150,000,000, per hgap_settings.xml

    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t") and uname == "brycef":
        test_path = "/global/projectb/scratch/brycef/pbmid"

        if output_path == "t1":
            # 1 contig
            library_name = "BBBYN"
            genome_size = 5600000 # 5519813
            seqqc = "seqqc1607/s9480/036767-cleanH5"
            #/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc1607/s9480/036767-cleanH5

        if output_path == "t2":
            library_name = "BWOAU"
            # 1 smrtcell

        if output_path == "t3":
            #library_name = "BXSSA"
            #library_name = "CAHGT" # 1 smrt cell
            library_name = "CAOTA" # 2 smrt cells

        if output_path == "t4":
            #library_name = "CAOTW" # 46 contigs w/ 1st assembly
            library_name = "CCZAH"

        if output_path == "t5":
            library_name = "CAOTT"


        output_path = os.path.join(test_path, library_name)


    # use current directory if no output path
    if not output_path:
        if seqqc:
            # use seqqc path
            output_path = seqqc
        else:
            output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    if seqqc:
        if not seqqc.startswith("/global"):
            seqqc = os.path.join("/global/projectb/scratch/ayspunde/qc/impQD/pacbio", seqqc)
        qc_only = True




    config_file = os.path.join(output_path, "pb-mid.cfg")

    if args.config_file:
        config_file = args.config_file

    # SEQQC-12092
    #if library_name in ["BSSWS","BSSWA"]:
    #    genome_size = 8000000


    # initialize my logger
    log_file = os.path.join(output_path, "pbmid.log")



    # log = logging object
    log_level = "INFO"
    log = get_logger("pbmid", log_file, log_level, print_log)


    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "library_name", library_name)
    log.info("%25s      %s", "genome_size est (bp)", genome_size)

    if qc_only:
        log.info("%25s      %s", "qc_only", qc_only)
    if seqqc:
        log.info("%25s      %s", "seqqc_path", seqqc)
    if qc_fasta:
        log.info("%25s      %s", "qc_file", qc_fasta)




    #log.info("%25s      %s", "step", status)
    log.info("%25s      %s", "config_file", config_file)

    log.info("")
    if hgap4:
        log.info("* USING HGAP4")
    else:
        log.info("* USING HGAP3")

    #if not os.path.isfile(config_file):
    if 1==1:
        #log.info("- cannot find config file: %s  %s", config_file, msg_warn)
        #log.info("- using default config file.")
        cmd = "cp %s %s" % (DEFAULT_CONFIG_FILE, config_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)




    status_log = os.path.join(output_path, "status.log")
    rqc_file_log = os.path.join(output_path, "rqc-files.tmp")
    rqc_stats_log = os.path.join(output_path, "rqc-stats.tmp")
    flow_file = os.path.join(output_path, "pbmid.flow")

    # overwrite old flow file
    fh = open(flow_file, "w")
    timestamp = time.strftime("%Y-%m-%d %H:%M:%S")
    fh.write("# %s %s flow file: %s\n" % (my_name, version, timestamp))
    fh.close()



    # set config dict with values from iso.cfg
    config = load_config_file(config_file, log)

    file_dict = {} # dictionary of file type and locations

    # setup Alex's run to finish the run
    if qc_only:
        setup_qc_only(seqqc, library_name)
        status = "contig_gc_plot"
        fasta_type = "repolished_fasta"


    checkpoint_step(status_log, "## New Run")



    bbtools_ver = get_bbtools_version(rqc_stats_log, output_path, log)

    miid_path = os.path.join(output_path, "miid") # Microbial Isolate Improved Draft, folder for qc analysis


    # test
    #do_polish(library_name)
    #do_prelim_qc(library_name)
    #fasta_type = "repolished_fasta"
    #do_fasta_stats(miid_path)
    #do_blast_16s(miid_path, fasta_type, library_name)
    #do_pdf_report(library_name, fasta_type)
    #sys.exit(48)


    done = False
    cycle_cnt = 0
    while not done:

        cycle_cnt += 1

        # assembly -> contig trimming
        if status in ("start", "setup_input_xml"):
            if hgap4:
                status = setup_pbsmrtpipe(library_name)
            else:
                status = setup_hgap3_input_xml(library_name)


        if status == "setup_hgap_xml":
            if hgap4:
                status = setup_hgap4_xml(genome_size)
            else:
                status = setup_hgap3_xml(genome_size)

        if status == "assembly":

            done_asm = False
            asm_cnt = 0

            asm_path = get_the_path(ASM_PATH, output_path)

            while not done_asm:
                asm_cnt += 1

                if hgap4:
                    status, new_genome_size = do_hgap4_assembly(genome_size)
                else:
                    status, new_genome_size = do_hgap3_assembly(genome_size)


                if status.startswith("rerun assembly"):
                    if hgap4:
                        status = setup_hgap4_xml(new_genome_size)
                    else:
                        status = setup_hgap3_xml(new_genome_size)

                    genome_size = new_genome_size

                if status == "assembly complete":
                    done_asm = True

                # don't get stuck in a loop! (2 attempts)
                if asm_cnt >= 2 and not done_asm:
                    log.info("- assembly was not good, using last assembly  %s", msg_warn)
                    status = "assembly complete"
                    # CAOTA - the first was better (64 contigs vs 94 contigs)
                    done_asm = True

                if done_asm:
                    if hgap4:
                        save_hgap4_files(asm_path)
                    else:
                        save_hgap3_files(asm_path)





        if status == "prelim_qc":
            status = do_prelim_qc(library_name)

        if status == "deduplicate":
            status = do_dedup(library_name)
            fasta_type = "dedup_fasta"



        if status == "polish":
            status = do_polish(library_name)
            fasta_type = "repolished_fasta" # final.fasta

        if status == "circularize":
            status = do_circularize()
            #fasta_type = "trimmed_fasta"


        # QC Analysis

        if status == "contig_gc_plot":
            status = do_contig_gc_plot(miid_path, fasta_type, library_name)


        if status == "gc_cov":
            status = do_gc_cov(miid_path, fasta_type, library_name)


        if status == "blast":
            status = do_blast(miid_path, fasta_type)

        if status == "blast_16s":
            status = do_blast_16s(miid_path, fasta_type, library_name)


        if status == "gc_hist":
            status = do_gc_hist(miid_path, fasta_type, library_name)

        if status == "ncbi_prescreen":
            status = do_ncbi_prescreen(miid_path, fasta_type)

        if status == "checkm":
            status = do_checkm(miid_path, fasta_type)

        if status == "pct_reads_asm":
            status = do_pct_reads_asm(miid_path, fasta_type)

        if status == "tetramer":
            status = do_tetramer(miid_path, fasta_type)

        if status == "antifam":
            status = do_antifam(miid_path, fasta_type)

        if status == "fasta_stats":
            if hgap4:
                status = do_fasta_stats(miid_path)
            else:
                status = do_fasta_stats_hgap3(miid_path)

        if status == "qual":
            status = do_qual(miid_path, fasta_type)

        # Reports: pdf report does the *.txt, *.pdf, *.metadata.json
        if status in ("pdf_report", "pdf"):
            status = do_pdf_report(library_name, fasta_type)

        if status == "purge":
            status = do_purge()

        if status == "complete":
            done = True

        if status == "failed":
            done = True

        if cycle_cnt > 88:
            log.info("- max cycles reached.  %s", msg_warn)
            done = True

        status = get_next_status(status)


    # finish flow
    fh = open(flow_file, "a")
    timestamp = time.strftime("%Y-%m-%d %H:%M:%S")
    fh.write("*label|Pacbio Microbial Improved Draft Pipeline (PBMID.py v%s)<br><smf>Run Date:%s</f>\n" % (version, timestamp))
    fh.close()

    dot_file = os.path.join(output_path, "%s.dot" % library_name)
    dot_flow(flow_file, dot_file)

    flow_png = os.path.join(output_path, "%s-flow.png" % library_name)
    cmd = "#graphviz;dot -T png %s > %s" % (dot_file, flow_png)
    std_out, std_err, exit_code = run_cmd(cmd, log)
    append_rqc_file(rqc_file_log, "pipeline_flow", flow_png)


    checkpoint_step(status_log, status)
    if status == "complete":



        # move rqc-files.tmp to rqc-files.txt
        append_rqc_stats(rqc_stats_log, "pbmid_version", "%s %s" % (my_name, version))
        rqc_new_file_log = os.path.join(output_path, "rqc-files.txt")

        cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        rqc_new_stats_log = os.path.join(output_path, "rqc-stats.txt")

        cmd = "mv %s %s" % (rqc_stats_log, rqc_new_stats_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    log.info("Completed %s", script_name)

    sys.exit(0)



"""
   _ __  __ _  ___
  | '__)/ _' |/ __\
  | |  | (_| | (__
  |_|   \__. |\___/
           | |
           | |
           |/

                      .-.
         .-._    _.../   `,    _.-.       Darth Vader: He is here.
         |   `'-'    \     \_'`   |
         \            '.__,/ `\_.--,      Governor Tarkin: Obi-Wan Kenobi? What makes you think so?
          /                '._/     |
         /                    '.    /     Darth Vader: A tremor in the Force. The last time I felt it was in the presence of my old master.
        ;   _                  _'--;
     '--|- (_)       __       (_) -|--'   Governor Tarkin: Surely he must be dead by now.
     .--|-          (__)          -|--.
      .-\-          /||\          -/-.    Darth Vader: Don't underestimate the Force.
     '   '.        /||||\         .'  `
           '-._   /||||||\   _.-'         Governor Tarkin: The Jedi are extinct, their fire has gone out of the universe.
               `""--....--""`                              You, my friend, are all that's left of their religion.


 o-----------------------------------------------------------------------------------------------o

 Alex wants to run through smrtportal - how do we track when this dude is done?
 qc_user login, password - use Bryce's for now in config

https://github.com/PacificBiosciences/SMRT-Analysis/wiki/Secondary-Analysis-Web-Services-API-v2.2.0#JOB_CR
curl -u administrator:administrator#1 -d 'data={"name":"DemoJobName", "createdBy":"testuser", "description":"demo job", "protocolName":"RS_Resequencing.1", "groupNames":["all"], "inputIds":["78807"]}' http://pssc1:8080/smrtportal/api/jobs/create
{
"idValue" : 16478,
"idProperty" : "jobId"
}

questions:
- how to get inputIds for xml
- how to pass in our special xml, set with new protocol name?

module load lapinpy

from lapinpy.curl import Curl

Created like this:
smrtPortalCurl = Curl('smrt_portal_address'+"/smrtportal",'smrt_portal_user','smrt_portal_password')

{ "name" : "hgap-libname",
"description" : "pbmid",
"protocolName" : "?", name of el protocol ...
"groupNames" : ["all"],

"inputIds" : [inputId]
}

and for an example calling this for job creation:
result = smrtPortalCurl.post('api/jobs/create',
data = 'data={"name":"%s", "createdBy":"%s", "description":"Auto kicked off", "protocolName":"%s", "groupNames":["all"], "inputIds":["%d"]}'  %(jobName,settings['smrt_portal_user'],
smrtCell['secondary_analysis_prot'], inputId))





-------

http://pacbio.jgi-psf.org/smrtportal/#/View-Data/Details-of-Job/40432
/global/seqfs/sdm/prod/pacbio/jobs/040/040432

"""
