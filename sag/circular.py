#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Circularize
- replace existing topology script with a Cori/Genepool compatible version
/global/projectb/sandbox/rqc/ayspunde/pacbio/pbpython/detectCircle/topologyPbAsm.py
/global/projectb/sandbox/rqc/ayspunde/pacbio/pbpython/detectCircle/trimDuplicateContigEnds.py

- originally written by James Han and Alex Spunde

Checks if each contig is circular


For each contig:
- combine the beginning 5000 bp(1) and ending 5000 bp(2) into one "circularized" contig (end joined to beginning)
- map the circularized contig against the input (corrected) reads
- if alignment length > 4000bp and left position < middle position, right position > middle position and overlap on each size > 500bp then count as a hit
- if 5+ hits, label the contig as circular

Circularized contig [------------------------------M----------------------------------] (M = joined middle position)
read 1                                 [------------------------]  = good
read 2                   [-------------------] = bad, doesn't overlap the middle
read 3                                            [---------------] = bad, not enough left side overlap


topology logic:
  left_pos = pos
  right_pos = pos + align_length (TLEN)

  if left < middle - overlap and right_pos > middle + overlap then match++


Overhangs on both ends

[-------c1-------------repeat][repeat-------c2---------][----------c3---------]
[repeat--c1--b]

--------------------.....AAAAAAAAAAAAAAAAAAA    AAAAAAAAAAAAAAAAA...----------
becomes
AAAAAAAAAAAAAAAAAA|--------------------
... only those reads with AAAAAAAAAAAAAAAAAA|-------- in them will map, that is okay


c1 --------------........AAAAAAAAAAAA   c2  AAAAAAAAAAAA....--------------...AAAAAAAAAAA  c3 AAAAAAAAAAA....----------------
Assembler split C1, C2, C3 at the repeats because not enough reads span the repeats





Trimming logic:
* check if the beginning of the contig is the same as the end.  If so, trim the beginning

Dependencies:
blasr (Alex Spunde's folder - v211?) Pacbio long read aligner- from Jan 28, 2014
bbtools (trim left) only if trim_begin option > 0
blast+

v 1.0: 2017-06-28
- initial version
- works with genepool & cori

v 1.0.1: 2017-07-24
- updated based on code review
- added sam alignment quality check (> 10)

v 1.0.2: 2017-11-20
- bug with contigs < 5000bp (GAA-3421)

v 1.1: 2017-11-30
- allow param to manually trim beginning of contigs (GAA-3421)

v 1.2: 2017-12-06
- auto-trimming using blast results, on by default

v 1.2.1: 2017-12-12
- added chaff file when trimming bases
- reformats trim.fasta to have even line lengths (PacBio software complains)
- fixes for Kurt (GAA-3421)

v 1.2.2: 2018-02-08
- max trim length option (Kurt request), 0 = no maximum trim length (default), max_trim_length was 20000bp (James)

To do
- try out bbtools for alignment (no)
- validate this logic: if read maps to only one circularized contig then it can count, otherwise do not count as a read spanning the overlap


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~[ tests ]

~/git/jgi-rqc-pipeline/sag/circular.py -o t1
-t1 = BWOAU
* 1 contig, circular
#1=contigID 2=contigLength 3=bridgingReads 4=topology
BWOAU_unitig_0|quiver|quiver 5125463 87 circular


~/git/jgi-rqc-pipeline/sag/circular.py -o t2
-t2 = BTYXB: 26 seconds
* 3 contigs, linear
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc1706/s11035/003300-cleanH5/circle/topology.txt
3 contigs: 4706642 in length

topologyPbAsm.py:
#1=contigID 2=contigLength 3=bridgingReads 4=topology
unitig_4|quiver 774302 0 linear
unitig_7|quiver 802880 0 linear
unitig_6|quiver 3129460 0 linear



This script t2 results:
#1=contigID 2=contigLength 3=bridgingReads 4=topology
unitig_4|quiver 774302 0 linear
unitig_6|quiver 3129460 0 linear
unitig_7|quiver 802880 46 circular

* same ref can map to multiple contigs but topologyPBasm.py only uses the last mapping which might not be the best one (bug in topologyPBasm.py)


* test case: did not map anything for COGZY

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import getpass

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, run_cmd, get_colors, get_msg_settings


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
blast assembly against itself to look for overlapping regions


makeblastdb -dbtype nucl -in asm.scf.fasta -out circle.db

blastn -query asm.scf.fasta \
-db asm.scf.fasta \
-perc_identity 98 \
-num_threads 16 \
-evalue 1e30 \
-task megablast \
-outfmt '6 qseqid sseqid qlen qstart qend slen sstart send length' \
-num_alignments 10

qlen = length of query db
slen = lengh of db sequence (qlen == slen)

makeblastdb -dbtype nucl -in asm.scf.fasta -out BWOAN/circle.x

very quick, 10 seconds
'''

def do_self_blast(fasta):
    log.info("do_self_blast: %s", fasta)

    min_blast_pct_id = 98
    blast_log = os.path.join(output_path, "blast.log")

    if os.path.isfile(blast_log):
        log.info("- blast already run, skipping")
    else:


        makedb_log = os.path.join(output_path, "makedb.log")
        blast_db = os.path.join(output_path, os.path.basename(fasta).replace(".fasta", ".blast"))
        cmd = "#blast;makeblastdb -dbtype nucl -in %s -out %s > %s 2>&1" % (fasta, blast_db, makedb_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        blast_params = "-perc_identity %s -num_threads 16 -evalue 1e-30 -task megablast -outfmt '6 qseqid sseqid qstart qend qlen sstart send slen length' -num_alignments 10" % min_blast_pct_id
        cmd = "#blast;blastn -query %s -db %s %s > %s 2>&1" % (fasta, blast_db, blast_params, blast_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # sed -i -- add header line to the blast file inline
        cmd = "sed -i '1s/^/#qseqid sseqid qstart qend qlen sstart send slen length\\n/' %s" % blast_log
        std_out, std_err, exit_code = run_cmd(cmd, log)

'''
Convert fasta filename to *.trim.fasta

'''
def get_trim_fasta_name(fasta):
    trim_fasta = fasta

    trim_ext = ".trim.fasta"

    # handle .fasta, .fna, .fa -> .trim.fasta

    trim_fasta = os.path.basename(fasta)
    if trim_fasta.endswith(".fasta"):
        trim_fasta = trim_fasta.replace(".fasta", trim_ext)
    elif trim_fasta.endswith(".fna"):
        trim_fasta = trim_fasta.replace(".fna", trim_ext)
    elif trim_fasta.endswith(".fa"):
        trim_fasta = trim_fasta.replace(".fa", trim_ext)
    else:
        trim_fasta = trim_fasta + trim_ext

    trim_fasta = os.path.join(output_path, trim_fasta)

    return trim_fasta


'''
If there are regions that overlap the beginning and the end, trmi from beginning

Rules: (from original trim & topology script)
* starting alignment < max_start (don't count if not in the first 25 bases)
* min length of alignment > min_length (500 bases)
* max length of alignment < max_length (2500 bases)
* alignment length * 3 < length of entire contig

Creates files:
*.trim.tmp.fasta: each contig trimmed at the beginning based on alignments, uneven line lengths
*.chaff.fasta: the contigs with the bases that were removed
*.trim.fasta: reformatted fasta with even line lengths (problem for pacbio code)

'''
def make_trim_fasta(fasta):
    log.info("make_trim_fasta: %s", fasta)

    trim_fasta = fasta


    # need to get length of each contig
    contig_len_dict = get_contig_lengths(fasta)


    trim_fasta = get_trim_fasta_name(fasta)
    trim_tmp_fasta = trim_fasta.replace(".trim.fasta", ".trim.tmp.fasta") # possibly uneven line lengths
    chaff_fasta = trim_fasta.replace(".trim.fasta", ".chaff.fasta") # trimmed reads go here
    trim_dict = {} # contig = trim length

    blast_log = os.path.join(output_path, "blast.log")
    if os.path.isfile(blast_log):
        fh = open(blast_log, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            arr = line.strip().split("\t")
            my_contig = arr[0]
            #print arr
            #align_len = int(arr[8]) # length of repeat, could include indels (GAA-3421)
            align_len = int(arr[3]) # use query end pos instead
            start_pos = int(arr[2])
            end_pos = int(arr[3])
            qlen = int(arr[4])

            # must be the same contig
            if my_contig != arr[1]:
                if debug_flag:
                    log.info("- contig: %s not mapped to same contig (%s)", my_contig, arr[1])
                continue

            # starting alignment must be in the first 25 bp
            if start_pos > max_trim_start:
                if debug_flag:
                    log.info("- contig: %s - start_pos (%s) > max_start (%s)", my_contig, start_pos, max_trim_start)
                continue


            # if repeat length * 3 > contig length then don't trim it

            contig_len = contig_len_dict.get(my_contig, 0)
            #if contig_len == -1:
            #    log.error("- no such contig: %s  %s", my_contig, msg_fail)
            #    sys.exit(2)

            #log.info("contig length: %s", contig_len)
            if align_len * 3 > contig_len:
                if debug_flag:
                    log.info("- contig: %s - align_len * 3 (%s) > contig_len (%s)", my_contig, align_len * 3, contig_len)
                continue



            # alignment length between min & max length?
            #if align_len >= min_trim_length and align_len <= max_trim_length:
            
            do_trim = False
            if align_len >= min_trim_length:
                if max_trim_length > 0:
                    if align_len <= max_trim_length:
                        do_trim = True
                else:
                    # trim regardless of length
                    do_trim = True 
                    
            if do_trim:
                #print "contig: %s, repeat len: %s" % (contig, align_len)
                if my_contig in trim_dict:
                    if trim_dict[my_contig] < align_len:
                        trim_dict[my_contig] = align_len
                else:
                    trim_dict[my_contig] = align_len
                log.info("- contig: %s, bases to trim: %s  %s", my_contig, align_len, msg_ok)

                #print "YES"
                # is align_len > contig_dict[arr[0]]? then keep it - trim this many bases from the left
            else:
                if debug_flag:
                    log.info("- contig: %s, %s > aligment length (%s) > %s", my_contig, min_trim_length, align_len, max_trim_length)



        # create new fasta with trimmed contigs
        #print trim_dict
        if len(trim_dict) > 0:

            fh = open(fasta, "r")
            fhw = open(trim_tmp_fasta, "w")
            fhc = open(chaff_fasta, "w")

            for line in fh:
                line = line.strip()
                if not line:
                    continue
                if line.startswith(">"):
                    my_contig = line[1:]
                    trim_bases = 0
                    fhc.write(line + "\n")
                    if my_contig in trim_dict:
                        trim_bases = trim_dict[my_contig]

                else:
                    if trim_bases > 0:
                        line_len = len(line)

                        if trim_bases >= line_len:
                            trim_bases = trim_bases - line_len
                            #line = "-" * line_len
                            fhc.write(line + "\n")
                            line = ""

                        else:
                            #line = "-" * trim_bases + line[trim_bases:]
                            fhc.write(line[0:trim_bases] + "\n")

                            line = line[trim_bases:]
                            trim_bases = 0
                if line:
                    fhw.write(line + "\n")

            fh.close()
            fhw.close()
            fhc.close()

            # make line lengths even
            reformat_log = os.path.join(output_path, "reformat.log")
            cmd = "#bbtools;reformat.sh ow=t in=%s out=%s > %s 2>&1" % (trim_tmp_fasta, trim_fasta, reformat_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            log.info("- created trimmed fasta: %s", trim_fasta)
            log.info("- trimmed bases put in chaff file: %s", chaff_fasta)

            cmd = "rm %s" % (trim_tmp_fasta)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            log.info("- nothing to trim, copying file  %s", msg_warn)
            cmd = "cp %s %s" % (fasta, trim_fasta)
            std_out, std_err, exit_code = run_cmd(cmd, log)

    else:
        log.error("- cannot find blast log: %s  %s", blast_log, msg_fail)
        sys.exit(2)




    return trim_fasta


'''
Get the length of each contig
- removes starting ">" in the dict
'''
def get_contig_lengths(fasta):
    log.info("get_contig_lengths: %s", fasta)
    contig_dict = {}

    if os.path.isfile(fasta):

        my_contig = ""
        base_cnt = 0

        fh = open(fasta, "r")
        for line in fh:

            if line.startswith(">"):
                if my_contig:
                    contig_dict[my_contig] = base_cnt
                base_cnt = 0
                my_contig = line.strip()
                if my_contig.startswith(">"):
                    my_contig = my_contig[1:]

            else:
                base_cnt += len(line.strip())

        fh.close()

        contig_dict[my_contig] = base_cnt
    else:
        log.error("- no such file: %s  %s", fasta, msg_fail)
        sys.exit(2)


    total_bases = 0 # check if we counted corrected, matches bbtools!
    contig_cnt = 0
    for contig in contig_dict:
        #print "%s: %s" % (contig, contig_dict[contig])
        total_bases += contig_dict[contig]

    log.info("- total bases: %s in %s contigs", total_bases, len(contig_dict))


    return contig_dict


'''
Get the first 5000bp from the contig and the last 5000bp from the contig and merge into one for each contig
/global/projectb/scratch/ayspunde/qc/impQD/pacbio/seqqc1706/s11038/040012-cleanH5/circle
store contig + length of circularized reads in *.circle.stats
'''
def make_circular_reference(fasta):
    log.info("make_circular_reference: %s", fasta)
    log.info("- use %s bases from beginning and end of each contig", base_length)

    circle_fasta = os.path.join(output_path, os.path.basename(fasta).replace(".fasta", ".circle.fasta"))
    circle_tmp_fasta = os.path.join(output_path, os.path.basename(fasta).replace(".fasta", ".circle.tmp.fasta")) # intermediate file

    # length of each circularized contig, normally read_length * 2 but contig might be shorter than read_length * 2
    circle_stats = circle_fasta.replace(".fasta", ".stats")

    if os.path.isfile(circle_fasta):
        log.info("- skipping circularization, found file: %s", circle_fasta)
        return circle_fasta, circle_stats

    # force trimming of each contig
    if trim_bp > 0:
        log.info("- trimming %s bases from the beginning of each contig", trim_bp)
        trim_log = os.path.join(output_path, "trim_bp.log")
        trim_fasta = get_trim_fasta_name(fasta)
        cmd = "#bbtools;reformat.sh ftl=%s ow=t in=%s out=%s > %s 2>&1" % (trim_bp, fasta, trim_fasta, trim_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        fasta = trim_fasta

    fh = open(fasta, "r")
    fhw = open(circle_tmp_fasta, "w") # writes with uneven contigs
    fhw2 = open(circle_stats, "w") # contig_name==circularized length==total length

    bases = ""
    base_len_tmp = base_length # length to save off each end

    log.info("- processing: %s", fasta)

    contig_name = None
    for line in fh:
        line = line.strip()
        if line.startswith(">"):

            if len(bases) > 0:
                base_len_tmp = base_length
                if len(bases) < base_length * 2:
                    base_len_tmp = len(bases) // 2 # enough bases for left and right

                b1 = bases[0:base_len_tmp]
                b2 = bases[-base_len_tmp:]
                #fhw.write(b1 + b2 + "\n") # join beginning and end (no, middle then is the gap between beginning and end)
                fhw.write(b2 + b1 + "\n") # join end to beginning
                log.info("-- trimmed base count: %s, total bases: %s", len(b2) + len(b1), len(bases))
                fhw2.write("%s==%s==%s\n" % (contig_name, len(b2) + len(b1), len(bases)))
                bases = ""

            # write header
            fhw.write(line + "\n")
            contig_name = line
            log.info("-- contig: %s", contig_name)


        else:
            bases += line # could use a list and join together

    fh.close()

    if len(bases) > 0:
        base_len_tmp = base_length
        if len(bases) < base_length * 2:
            base_len_tmp = len(bases) // 2 # enough bases for left and right

        b1 = bases[0:base_len_tmp]
        b2 = bases[-base_len_tmp:]
        fhw.write(b2 + b1 + "\n") # join end to beginning
        log.info("-- trimmed base count: %s, total bases: %s", len(b1 + b2), len(bases))
        fhw2.write("%s==%s==%s\n" % (contig_name, len(b1+b2), len(bases)))
        bases = ""
    # 6033 per bbtools
    fhw.close()
    fhw2.close()

    reformat_log = os.path.join(output_path, "reformat_circle.log")
    cmd = "#bbtools;reformat.sh in=%s out=%s ow=t > %s 2>&1" % (circle_tmp_fasta, circle_fasta, reformat_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    cmd = "rm %s" % (circle_tmp_fasta)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    log.info("- created: %s", circle_fasta)



    return circle_fasta, circle_stats


'''
aligner: blazr or bbmap
align input reads to the circularized fasta
- create a sam file for alignment
~ 15 seconds
'''
def do_mapping(circle_fasta, input_reads):
    log.info("do_mapping")

    sam_file = os.path.join(output_path, "circular.sam")

    map_log = os.path.join(output_path, "mapping.log")

    if os.path.isfile(sam_file):
        log.info("- skipping sam step, found sam file: %s", sam_file)
        return sam_file

    # nproc could be higher ... could check for number of cpus
    cmd = "%s %s %s -sam -clipping hard -out %s -nproc 16 > %s 2>&1" % (BLASR_CMD, input_reads, circle_fasta, sam_file, map_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    # bbmap: local=True ambiguous=toss outputunmapped=f cigar=f bs=sortBam.sh fastareadlen=6000 strictmaxindel=t maxindel=200
    # - need to check with BB

    if not os.path.isfile(sam_file):
        log.error("- no sam file created!  %s", msg_fail)
        sys.exit(2)

    return sam_file

'''
https://samtools.github.io/hts-specs/SAMv1.pdf
0 QNAME
1 FLAG
2 RNAME
3 POS
4 MAPQ
5 CIGAR
6 RNEXT
7 PNEXT
8 TLEN
9 SEQ
10 QUAL


parse the sam file
count as a hit if:
    * flag indicate reads aligned
    * hardclipping in CIGAR string condition?
    * align_length >= min_length
    * right pos > middle_pos + overlap
    * left pos < middle - overlap
'''
def parse_alignment(sam_file, circle_stats, min_length, overlap_length, min_reads, min_quality):

    log.info("parse_alignment: %s", sam_file)



    contig_dict = {}
    fh = open(circle_stats, "r")
    for line in fh:
        arr = line.split("==")
        # middle = length evenly divided by 2
        contig_dict[arr[0]] = {"middle" : int(arr[1]) // 2, "base_cnt" : int(arr[2]), "cnt" : 0}

    fh.close()



    log.info("- processing: %s", sam_file)

    # read sam file 2x
    read_dict = {} # for each read, how many contigs does it hit where length > min_length?
    count_read_flag = False # temp off
    if count_read_flag:
        fh = open(sam_file, "r")
        line_cnt = 0
        for line in fh:
            if line.startswith("@"):
                continue

            arr = line.split('\t')
            if len(arr) > 9:

                # not checking 0x4 flag here, check next time around
                qual = int(arr[4])
                contig_name = ">%s" % arr[2]
                align_len = len(arr[9])
                read_name = arr[0]
                if align_len >= min_length and qual >= min_quality:
                    if read_name in read_dict:
                        read_dict[read_name]['cnt'] += 1
                        read_dict[read_name]['contigs'].append(contig_name)
                    else:
                        read_dict[read_name] = {"cnt" : 1, "contigs" : [contig_name]}
        fh.close()


    fh = open(sam_file, "r")
    line_cnt = 0
    for line in fh:
        if line.startswith("@"):
            continue

        arr = line.split('\t')
        if len(arr) > 9:
            line_cnt += 1
            check_flag = True

            f = int(arr[1]) # flag
            if f > 0:

                # 0x4 = unmapped
                if f & 0x4 > 0:
                    check_flag = False


                # hardclipping test - CIGAR check - e.g. 15000H, look at ends and shorter than desired length

                # is hardclipped read aligned to one contig?
                # hardclipping length > length of half of contigs

            qual = int(arr[4])
            if qual < min_quality: # 10 = pr(mapping is wrong) = 10%, 5 = pr(mapping is wrong) = 30%
                check_flag = False

            if check_flag:
                cigar = arr[5]
                left_pos = int(arr[3])
                contig_name = ">%s" % arr[2]
                align_len = len(arr[9]) # int(arr[8]) # or len(arr[9]) not sure about hard clipping
                right_pos = left_pos + align_len
                overlap_check = False # whether contig overlaps the middle of the circular contig

                status = msg_fail
                middle_pos = 0
                if contig_name in contig_dict:
                    middle_pos = contig_dict[contig_name]['middle']

                    if left_pos < middle_pos - overlap_length and right_pos > middle_pos + overlap_length and align_len >= min_length:
                        if arr[0] in read_dict:
                            if len(list(set(read_dict[arr[0]]['contigs']))) > 1:
                                overlap_check = False
                            else:
                                overlap_check = True
                                status = msg_ok

                        else:
                            overlap_check = True
                            status = msg_ok

                if debug_flag:
                    log.info("-- contig: %s, left pos: %s, right pos: %s, length: %s, middle: %s, overlap_check: %s  %s", contig_name, left_pos, right_pos, align_len, middle_pos, overlap_check, status)
                    log.info("-- ref: %s, cigar: %s, qual: %s", arr[0], cigar, arr[4])

                # read crosses intersection
                if status == msg_ok:
                    contig_dict[contig_name]['cnt'] += 1

    fh.close()


    if line_cnt == 0:
        log.error("- no alignments in sam file!  %s", msg_fail)
        sys.exit(2)

    log.info("- processed %s alignments", line_cnt)



    # create topology file
    topology_file = os.path.join(output_path, "topology.txt")
    fhw = open(topology_file, "w")
    fhw.write("#1=contigID 2=contigLength 3=bridgingReads 4=topology\n")


    for my_key in sorted(contig_dict.iterkeys()):
        topology = "linear"
        if contig_dict[my_key]['cnt'] > min_reads:
            topology = "circular"

        contig_name = my_key
        if contig_name.startswith(">"):
            contig_name = contig_name[1:] # trim leading ">"
        fhw.write("%s %s %s %s\n" % (contig_name, contig_dict[my_key]['base_cnt'], contig_dict[my_key]['cnt'], topology))
        log.info("- %s: %s = %s", contig_name, contig_dict[my_key]['cnt'], topology)

    fhw.close()



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "Circular"
    version = "1.2.2"
    version += " (Blasr 211"
    BLASR_CMD = "/global/projectb/sandbox/rqc/ayspunde/pacbio/BLASR/blasr_211/blasr"

    cmd = "#bbtools;bbversion.sh"
    bbtools_ver, _, _ = run_cmd(cmd)
    bbtools_ver = bbtools_ver.split("\n")[0].replace("BBMap version ", "")
    version += ", BTools %s" % bbtools_ver

    cmd = "#blast;blastn -version"
    blast_ver, _, _ = run_cmd(cmd)
    blast_ver = blast_ver.split("\n")[0].replace("blastn: ", "")
    version += ", Blast %s" % blast_ver

    version += ")"

    uname = getpass.getuser() # get the user, brycef can't use scell.p, but qc_user can!


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)

    # Parse options
    usage = "circular.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
Trims contigs if the beginning of the contig is repeated at the end of the contig.
Then determines if each contig is circular by aligning the reads to the end-beginning junctions to see if there is overlap.

    """

    parser = ArgumentParser(usage = usage)


    parser.add_argument("-i", "--input-reads", dest="input", type = str, help = "Input reads (corrected.fasta)")
    parser.add_argument("-f", "--fasta", dest="fasta", type = str, help = "Assembly/reference fasta to test for circularization")
    parser.add_argument("-o", "--output-path", dest="output_path", type = str, help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-b", "--base-length", dest="base_length", type = int, help = "Left and right number of bases to combine from each contig (default 5000)")
    parser.add_argument("-mr", "--min-reads", dest="min_reads", type = int, help = "Minimum number of reads to align and pass overlap tests to declare contig is circular (default 5)")
    parser.add_argument("-ml", "--min-length", dest="min_length", type = int, help = "Minimum alignment length of input reads to circular contig (default 4000)")
    parser.add_argument("-mq", "--min-quality", dest="min_quality", type = int, help = "Minimum alignment quality 0..256 (default 10)")
    parser.add_argument("-mt", "--max-trim-length", dest="max_trim", type = int, help = "Maximum trim length (default 0 bp - no limit)")
    parser.add_argument("-ol", "--overlap-length", dest="overlap_length", type = int, help = "Minimum overlap on left and right side of circular contig from middle (default 500)")
    parser.add_argument("-t", "--trim", dest="trim_begin", type = str, help = "Trim this many bases from the beginning of the contig. 'auto' blasts the contigs against themselves and calculates the amount to trim automatically (default setting)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-d", "--debug", dest="debug_flag", default = False, action = "store_true", help = "additional debugging info")
    parser.add_argument("-v", "--version", action="version", version=version)

    output_path = None
    fasta = None
    input_reads = None
    circle_fasta = None # fasta with the ends joined together (made in make_circular_reference)


    base_length = 5000 # length at beginning and end of contig to examine
    min_reads = 5 # min number of reads mapping
    min_length = 4000 # min length for query read
    overlap_length = 500 # min overlap
    min_quality = 10 # 0..256 - min mapping quality, depends on aligner

    # trimming params
    min_blast_pct_id = 98
    trim_bp = -1 # trim this many bp from the beginning of the contig, -1 = automatic detection
    min_trim_length = 500 # aligned sequence minimum length for trimming
    max_trim_length = 0 # aligned sequence maximum length for trimming, 0 = no limit
    max_trim_start = 25 # start pos of aligned sequence needs to be in the first 25 bp of the contig/query

    # parse args
    args = parser.parse_args()

    print_log = args.print_log
    debug_flag = args.debug_flag # extra verbosity

    if args.fasta:
        fasta = args.fasta

    if args.input:
        input_reads = args.input

    if args.output_path:
        output_path = args.output_path

    if args.trim_begin:
        trim_bp = args.trim_begin
        if trim_bp.isdigit():
            trim_bp = int(trim_bp)
        elif trim_bp.startswith("auto"):
            trim_bp = -1

    if args.max_trim:
        max_trim_length = args.max_trim
        
    if args.base_length:
        base_length = args.base_length
    if args.min_reads:
        min_reads = args.min_reads
    if args.min_length:
        min_length = args.min_length
    if args.overlap_length:
        overlap_length = args.overlap_length
    if args.min_quality:
        min_quality = args.min_quality

    # testing (-o t1, -o t2)
    # Kurt's paths begin with t also (GAA-3421)
    if output_path and output_path.startswith("t") and uname == "brycef":
        test_path = "/global/projectb/scratch/brycef/cir"

        if output_path == "t1":
            fasta = "/global/projectb/scratch/brycef/pbmid/BWOAU.fasta"
            input_reads = "/global/projectb/scratch/brycef/pbmid/BWOAU.input_reads.fasta"
            output_path = "BWOAU"
            debug_flag = True

        if output_path == "t2":
            #fasta = "/global/projectb/scratch/brycef/pbmid/BTYXB.fasta"
            #input_reads = "/global/projectb/scratch/brycef/pbmid/BTYXB.input_reads.fasta"
            #output_path = "BTYXB"
            input_reads = "/global/dna/shared/rqc/pipelines/pbmid/archive/00/00/39/37/hgap/tasks/falcon_ns.tasks.task_falcon1_run_merge_consensus_jobs-0/preads4falcon.fasta"
            fasta = "/global/dna/shared/rqc/pipelines/pbmid/archive/00/00/39/37/arrow/repolished_assembly.dedup.fasta"
            output_path = "CCASG"
            # CCASG_38 = circular

        if output_path == "t3":
            # GAA-3421
            fasta = "/global/projectb/scratch/brycef/cir/asm.scf.fasta"
            input_reads = "/global/projectb/scratch/brycef/cir/reads.fa"
            output_path = "BWOAN"
            debug_flag = True

        if output_path == "t4":
            # GAA-3421, Kurt test: deg = linear, scf = circular
            fasta = "/global/projectb/scratch/brycef/cir/BSSZT.mito.fa"
            input_reads = "/global/projectb/scratch/brycef/cir/BSSZT.reads.fa"
            output_path = "BSSZT"
            debug_flag = False

        output_path = os.path.join(test_path, output_path)


    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # if not starting with /  use relative path (GAA-3421)
    if output_path.startswith("/"):
        pass
    else:
        output_path = os.path.join(os.getcwd(), output_path)


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)




    # initialize my logger
    log_file = os.path.join(output_path, "circular.log")



    # log = logging object
    log_level = "INFO"
    log = None
    log = get_logger("circular", log_file, log_level, print_log)


    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "fasta", fasta)
    log.info("%25s      %s", "input_reads", input_reads)
    log.info("%25s      %s", "base_length", base_length)
    log.info("%25s      %s", "min_reads", min_reads)
    log.info("%25s      %s", "min_length", min_length)
    if max_trim_length > 0:
        log.info("%25s      %s", "max_trim_length", max_trim_length)
    else:
        log.info("%25s      %s", "max_trim_length", "no max length")
    log.info("%25s      %s", "overlap_length", overlap_length)
    if trim_bp > 0:
        log.info("%25s      %s", "trim", trim_bp)
    else:
        log.info("%25s      %s", "trim", "automatic")
    if debug_flag:
        log.info("%25s      %s", "debug", "on")
    log.info("")

    if not os.path.isfile(fasta):
        log.error("Error: cannot find fasta: %s  %s", fasta, msg_fail)
        sys.exit(2)

    if not os.path.isfile(input_reads):
        log.error("Error: cannot find input reads: %s  %s", input_reads, msg_fail)
        sys.exit(2)



    #get_contig_lengths("/global/dna/shared/rqc/pipelines/pbmid/archive/00/00/39/37/arrow/repolished_assembly.dedup.fasta")
    # 5544322 = 5544322
    #sys.exit(44)


    # Steps to run

    trim_fasta = fasta
    if trim_bp == -1:
        do_self_blast(fasta)
        trim_fasta = make_trim_fasta(fasta)

    circle_fasta, circle_stats = make_circular_reference(trim_fasta)

    sam_file = do_mapping(circle_fasta, input_reads)

    parse_alignment(sam_file, circle_stats, min_length, overlap_length, min_reads, min_quality)

    log.info("Completed %s", script_name)

    sys.exit(0)

'''
Perl code that prints digits of pi!

              $|=3,141592;sub _
          {print@_}sub o{_++$O[0
        ];_ 0for 1..$#O}sub O{$;=int
      $=/10,'0/^           ^';if($;<9)
     {_$_ for                 @O;;@O=()
    ;0}push                     @O,$;;0
   ,;push@                       O,'.'if
   $^==1;                         0;if($;
   ==10){          print          ,o,@O=(
   )}}$~=         1000000         ;$-=10*
   (q/@O=       digits of pi      =10/,1)
   *int($~                       /3)+1;$
    _=2for@                     ,[0..$-]
     ,;for$^                   (1..$~){
     $:=$-;$O                 =0;until
       ($:<0){$/=          2*$:+1;$/=
        10if$/==1;$==10*$,[$:]+$O;$,
          [$:]=$=%$/;$O=int($=/$/
               )*$:--,10}O}o

'''