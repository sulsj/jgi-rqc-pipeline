#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
GC_Histogram
- for ISO, CE, SPS, SAG pipelines
- creates barcharts of percent gene hits vs gc for each organism identified
- replaces "megan" code even though it doesn't use megan at all (jigsaw/taxGcPlot.pl)


Dependencies
prodigal/2.6.3
lastal, using nr in CURRENT
gnuplot (in bbtools container)
diamond (alternate, faster aligner)

To do:
- pacbio special params?
- localize nr?
- better way to visualize this plot (R&D)


v 1.0: 2017-05-08
- initial version
- works with genepool & denovo

v 1.1: 2018-07-09
- replace last with diamond because last doesn't work on Cori cluster
--- LAST does work on Cori now
- changed common dbs to constants


~/git/jgi-rqc-pipeline/sag/gc_histogram.py -o t1

t1 = BSHTN: 8.68 min, jigsaw 49.9 minutes (w lastal 712, not multhreaded)
t2 = BTHZO: 5.91 min/104mb, jigsaw: 7.5 min/111mb (* all ecoli?  yes)




* moving nr to /scratch would save us time
module load last/828;lastal -F15 -P0 -f BlastTab /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nr/LAST/nr /global/projectb/scratch/brycef/iso/gch1/prodigal/contigs.gene.fasta > /global/projectb/scratch/brycef/iso/gch1/lastal/last.txt
time module load last/828;
time lastal -F15 -P0 -f BlastTab /scratch/brycef/nr/LAST/nr /global/projectb/scratch/brycef/iso/gch1/prodigal/contigs.gene.fasta > /scratch/brycef/last.txt
- on projectb took 7 minutes
* only need to rsync the nr/LAST/nr part... 162g
time lastal -F15 -P0 -f BlastTab /scratch/brycef/nr/nr /global/projectb/scratch/brycef/iso/gch1/prodigal/contigs.gene.fasta > /scratch/brycef/last.txt
2m45.943s - much quicker



GC Histogram Process:
* run prodigal to create contigs.gene.fasta & contig.gene.faa (protiens)
- shreds contigs into pieces (3712 contigs)
* get the gc per each contig in contigs.gene.fasta with bbtools:stats.sh

* run lastal vs contigs.gene.fasta against nr
* get tophits from lastal (alignment length * percent id)

* for each tophit, look up taxonomy in tax server (gi #)
- lastal/best.txt summarizes this


for each taxonimic level:
  for each unique taxonomic thing:
     get gc histogram (numpy)
       overlay gc histograms on top of each other

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import getpass
import numpy
import cPickle as pickle

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, run_cmd, set_colors, get_msg_settings
from micro_lib import get_the_path, get_nproc
from taxonomy import get_taxonomy

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Run prodigal on fasta to generate
- fast, about 1 minute
'''
def do_prodigal(fasta):
    log.info("do_prodigal: %s", fasta)

    the_path = the_path = get_the_path("prodigal", output_path)

    contig_gene_faa = os.path.join(the_path, "contigs.gene.faa")
    contig_gene_fasta = os.path.join(the_path, "contigs.gene.fasta")
    prodigal_log = os.path.join(the_path, "prodigal.out")
    prodigal_log2 = os.path.join(the_path, "prodigal.log")

    if os.path.isfile(contig_gene_fasta):
        log.info("- skipping prodigal, found: %s", contig_gene_fasta)

    else:

        cmd = "#%s;prodigal -a %s -d %s -i %s -o %s -p meta > %s 2>&1" % (prodigal_module, contig_gene_faa, contig_gene_fasta, fasta, prodigal_log, prodigal_log2)
        run_cmd(cmd, log)


    return contig_gene_fasta

'''
Run lastal on the prodigal created contigs.gene.fasta
- took 7.16 minutes

'''
def run_lastal(fasta):
    log.info("do_last: %s", fasta)

    the_path = get_the_path("lastal", output_path)


    # jigsaw was using lastal: April 2014 - 712
    #/global/common/genepool/jgi/qaqc/jigsaw/2.6.4-prod_2.6.4_1-1-g356a568/bin/../external_tools/Last/DEFAULT/bin/lastal

    # default format if .maf
    last_output = os.path.join(the_path, "last.txt")
    last_log = os.path.join(the_path, "last.log")



    if os.path.isfile(last_log):
        log.info("- skipping last, found: %s", last_log)
    else:

        # genepool: 843, shifter = 869

        cmd = "#%s;lastal -F15 -P0 -f BlastTab %s %s > %s 2> %s" % (lastal_module, LAST_NR, fasta, last_output, last_log)
        # -F15 = framshift of 15, -P0 = use all threads
        # blasttab = easier format to parse

        # before lastal, JIGSAW used blastx but blastx is very slow - testing 2017-08-31
        if uname == "brycef1":
            blastx_output = os.path.join(the_path, "blastx.txt")
            blastx_log = os.path.join(the_path, "blastx.log")
            blastx_params = "-evalue 1e-30 -word_size 7 -task blastx -show_gis -soft_masking true -num_alignments 5 "
            blastx_params += "-outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles' -num_threads 8"
            cmd = "#blast+/2.6.0;blastx -db %s -query %s %s > %s 2> %s" % (BLASTX_NR, fasta, blastx_params, blastx_output, blastx_log)


        run_cmd(cmd, log)
        # -task blastx-fast?

        # gc per contig
        #$ stats.sh gc=stdout.txt in=contigs.gene.fasta gcformat=4 | head -20
        gc_file = os.path.join(the_path, "contig_gc.txt")
        gc_log = os.path.join(the_path, "gc.log")
        cmd = "#bbtools;stats.sh gc=%s in=%s gcformat=4 ow=t > %s 2>&1" % (gc_file, fasta, gc_log)
        run_cmd(cmd, log)

'''

v0.9.19.120
/global/projectb/sandbox/rqc/qc_user/miniconda/miniconda2/envs/qaqc2/bin/diamond blastp \
-d /global/projectb/scratch/syao/data2/nr/diamond/nr \
-q /global/projectb/scratch/brycef/gc_hist/prodigal/contigs.gene.faa
-p 16 -o /global/projectb/scratch/brycef/gc_hist/diamond.txt > /global/projectb/scratch/brycef/gc_hist/diamond.log 2>&1
~ 9 minutes for BSHTN
'''
def run_diamond(fasta):
    log.info("run_diamond: %s", fasta)

    the_path = get_the_path("diamond", output_path)

    faa = fasta.replace(".fasta", ".faa")
    if not os.path.isfile(faa):
        log.error("- cannot find prodigal faa file: %s  %s", faa, msg_fail)
        sys.exit(2)

    # default format if .maf
    diamond_output = os.path.join(the_path, "diamond.txt")

    nproc = get_nproc()

    if os.path.isfile(diamond_output):
        log.info("- skipping last, found: %s", diamond_output)
    else:

        # Default columns: qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore
        diamond_log = os.path.join(the_path, "diamond.log")
        cmd = "#diamond;diamond blastp -d %s -q %s -p %s -o %s > %s 2>&1" % (DIAMOND_NR, faa, nproc, diamond_output, diamond_log)

        run_cmd(cmd, log)

        # add fields line to output
        # Fields: query id, subject id, % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score
        cmd = "sed -i '1s/^/#query id, subject id, pct identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score\\n/' %s" % diamond_output
        run_cmd(cmd, log)

        # gc per contig
        #$ stats.sh gc=stdout.txt in=contigs.gene.fasta gcformat=4 | head -20
        gc_file = os.path.join(the_path, "contig_gc.txt")
        gc_log = os.path.join(the_path, "gc.log")
        cmd = "#bbtools;stats.sh gc=%s in=%s gcformat=4 ow=t > %s 2>&1" % (gc_file, fasta, gc_log)
        run_cmd(cmd, log)


'''
Get one best hit per contig as long as the pct_id > min_pct_id
* creates best.txt: summary of each contig
* creates gc_hist.txt: gc histograms for each level and "thing"

# Fields: query id, subject id, % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score
# batch 0
BSHTN_NODE_1_length_1275468_cov_53.5401_1_1     gi|763360523|ref|WP_044218059.1|        44.40   259     129     5       37      795     1       250     9.1e-79 300
BSHTN_NODE_1_length_1275468_cov_53.5401_1_1     gi|1000257734|gb|KXK20542.1|    36.64   262     136     9       16      795     19      252     3.8e-37 165
BSHTN_NODE_1_length_1275468_cov_53.5401_1_2     gi|502446257|ref|WP_012789096.1|        76.70   339     79      0       1       1017    1       339     2.7e-235        772


EG2 is "expected alignments per square giga". This is the expected number of alignments with greater or equal score, between two randomly-shuffled sequences of length 1 billion each.
E is the expected number of alignments with greater or equal score, between: a random sequence with the same length as the query sequence, and a random sequence with the same length as the database.

'''
def get_best_hits(min_pct_id):
    log.info("get_best_hits: min_pct_id = %s", min_pct_id)


    # top hit score = align length * pct  id

    the_path = get_the_path("lastal", output_path)
    align_file = "last.txt"
    if diamond_flag:
        the_path = get_the_path("diamond", output_path)
        align_file = "diamond.txt"

    best_hits = os.path.join(the_path, "best.txt")
    if os.path.isfile(best_hits) and 1==2:
        log.info("- skipping best hits, found: %s", best_hits)

    else:

        contig_hits = {} # [contig] = { score: highest score, gi: gi for taxonomy, gc: gc for contig, len: contig length }
        gc_list = []
        gc_file = os.path.join(the_path, "contig_gc.txt")
        log.info("- processing: %s", gc_file)
        fh = open(gc_file, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            arr = line.split()
            if len(arr) > 6:
                contig = arr[0]
                #gc = float(float(arr[-1]) * 10000.0) / 100.0
                gc = float("{:.2f}".format(float(arr[-1])*100))
                clen = int(arr[-2])
                contig_hits[contig] = { "score" : 0, "gi" : 0, "gc" : gc, "len" : clen }
                gc_list.append(gc)

        fh.close()




        align_output = os.path.join(the_path, align_file)
        log.info("- processing: %s", align_output)

        # dict of contigs
        fh = open(align_output, "r")

        contig = ""
        gi_id = 0
        taxon = ""

        for line in fh:
            if line.startswith("#"):
                continue
            arr = line.split()
            if len(arr) > 8:
                contig = arr[0]
                taxon = arr[1]
                gi_id = 0
                if taxon.startswith("gi|"):
                    tax_list = taxon.split("|")
                    gi_id = int(tax_list[1])

                pct_id = float(arr[2])
                align_len = int(arr[3])

                # min pct id
                if pct_id > min_pct_id:
                    score = pct_id * align_len

                    if contig in contig_hits:
                        if score > contig_hits[contig]['score']:
                            contig_hits[contig]['score'] = score # = { "score" : score, "gi" : gi_id }
                            contig_hits[contig]['gi'] = gi_id
                    else:
                        #print "* missing contig: %s" % contig
                        # shouldn't happen
                        contig_hits[contig] = { "score" : score, "gi" : gi_id, "gc" : 0, "len" : 0 }

        fh.close()


        # get overall GC histogram
        gc_hist_file = os.path.join(the_path, "gc_hist.txt")

        gc_hist = numpy.histogram(gc_list, bins=range(0, 101))

        fhgc = open(gc_hist_file, "w")
        fhgc.write("#Domain|Name|GC Count|GC Hist\n")
        my_list = ",".join(str(x) for x in gc_hist[0])
        fhgc.write("Total|-|%s|%s\n" % (len(gc_list), my_list))

        # create best hits file - probably don't need this but good for history
        fh = open(best_hits, "w")
        fh.write("#contig|score|gi|gc|length|Taxonomy\n")
        best_cnt = 0

        gi_dict = {} # gi -> dict
        pickle_file = os.path.join(the_path, "gi.p")
        if os.path.isfile(pickle_file):
            gi_dict = pickle.load(open(pickle_file, "rb"))


        tax_list = ['kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
        t_gc = {}
        for i in tax_list:
            t_gc[i] = {}

        for contig in contig_hits:
            best_cnt += 1

            my_buffer = "%s|%s|%s|%s|%s|" % (contig, contig_hits[contig]['score'], contig_hits[contig]['gi'], contig_hits[contig]['gc'], contig_hits[contig]['len'])

            my_list = []
            tax_d = {}
            if contig_hits[contig]['gi'] in gi_dict:
                tax_d = gi_dict[contig_hits[contig]['gi']]
            else:
                tax_d = get_taxonomy(contig_hits[contig]['gi'], "gi")
                tax_d['kingdom'] = tax_d['superkingdom']
                # sometimes domain also?

                gi_dict[contig_hits[contig]['gi']] = tax_d

            for i in tax_list:
                my_list.append(tax_d[i])
                if tax_d[i] in t_gc[i]:
                    t_gc[i][tax_d[i]].append(contig_hits[contig]['gc'])
                else:
                    t_gc[i][tax_d[i]] = [contig_hits[contig]['gc']]


            my_buffer += "|".join(my_list)

            my_buffer = my_buffer + "\n"

            fh.write(my_buffer)




        fh.close()

        pickle.dump(gi_dict, open(pickle_file, "wb"))

        log.info("- wrote %s contigs to: %s", best_cnt, best_hits)

        # difference between No Hit and Unassigned?
        #print t_gc
        for i in tax_list:
            log.info("- %s", i)

            for tt in t_gc[i]:
                l = len(t_gc[i][tt])
                log.info("--- %s: %s hits", tt, l)

                gc_hist = numpy.histogram(t_gc[i][tt], bins=range(0, 101))

                my_list = ",".join(str(x) for x in gc_hist[0])
                fhgc.write("%s|%s|%s|%s\n" % (i, tt, l, my_list))

        fhgc.close()
        log.info("- created: %s", gc_hist_file)




'''
For each taxonomic level:
* create file for gnuplot for each taxonomic level (*.gc)
* use top 99% of organisms and convert histogram hit from gc_hist to percent of Total GC

'''
def do_gnu_plot_file():
    log.info("gnu_plot_file")

    gnu_path = get_the_path("gnu", output_path)
    the_path = get_the_path("lastal", output_path)
    if diamond_flag:
        the_path = get_the_path("diamond", output_path)
    tax_list = ['kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    #tax_list = ['genus'] # tmp
    gc_hist_file = os.path.join(the_path, "gc_hist.txt")

    for t in tax_list:
        tax_ttl = 0.0 # float for division
        gc_hist = []

        tax_hist = {} # thing = csv of taxonomy
        tax_cnt = {} # count of each thing for sorting
        fh = open(gc_hist_file, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            line = line.strip()

            arr = line.split("|")
            if line.startswith("Total|-|"):
                tax_ttl = float(arr[2])
                gc_hist = str(arr[3]).split(",")

            if line.startswith(t + "|"):
                tax_cnt[arr[1]] = int(arr[2])
                gc_list = str(arr[3]).split(",")
                tax_hist[arr[1]] = { "ttl" : int(arr[2]), "gc" : gc_list }


        fh.close()

        # get top 99% of hits
        tax_list = []  # list of 99% of the things
        max_val = 0.99
        tax_sum = 0
        list_cnt = 0
        max_list_size = 20 # could be a param ...
        for i in sorted([(v,k) for (k,v) in tax_cnt.items()], reverse = True):
            #print i[0], i[1]

            if tax_sum / float(tax_ttl) < max_val and list_cnt < max_list_size:
                taxa = str(i[1]).replace("_", " ")
                tax_list.append(taxa)
                list_cnt += 1

            tax_sum += i[0]

        #print tax_list
        tax_list.append("Unassigned") # other = Total - sum of each one

        # create gc data
        gc_data = os.path.join(gnu_path, "%s.gc" % t)
        fh = open(gc_data, "w")

        # GC pct, Total, org1, org2 ... Others
        my_buffer = "#GC_Pct\tTotal\t"
        my_buffer += "\t".join(tax_list)
        fh.write(my_buffer + "\n")


        for x in range(1,101):
            my_list = []

            # total pct, only add if > 0
            # list index out of range?
            vttl = int(gc_hist[x-1])

            if vttl > 0:
                vttl = 100.0 * vttl / tax_ttl
                #print vttl, gc_hist[x-1], tax_ttl


                my_list.append(str(x))
                my_list.append(str(vttl)) # Total

                vs = 0.0 # sum of all in the tax list
                for t in tax_list:
                    v = 0.0
                    if t in tax_hist:
                        v = int(tax_hist[t]['gc'][x-1])

                    v = 100.0 * v / tax_ttl
                    vs += v
                    my_list.append(str(v))


                # add "Unassigned" to the list
                if vttl - vs < 0.0:
                    my_list.append("0.00")
                else:
                    my_list.append(str(vttl - vs))

                my_buffer = "\t".join(my_list)

                fh.write(my_buffer + "\n")

        fh.close()
        log.info("- created file: %s", gc_data)


'''
Create gnu plot files and create the png files
returns number that failed to plot
- runs twice, once with total and once without

'''
def do_gnu_plot(library_name, show_total_flag, file_type = "contigs"):
    log.info("do_gnu_plot: %s, show total: %s", library_name, show_total_flag)

    err_cnt = 0

    gnu_template = """#!/usr/bin/gnuplot

set grid;
set term png enhanced size [plot_size];
set output "[gc_hist_png]";
set grid;
set style fill transparent solid 0.75 border -1;
set boxwidth 1;
set xlabel "Percent GC";
set xtics 10;
set xrange [0:100];
set ylabel "Percent Gene Hits";
set title "[plot_title]";
set palette defined(1 "#FFFF00",2 "#008000",3 "#FFA500",4 "#A52A2A",5 "#1E90FF",6 "#DAA520",7 "#9FAFDF",8 "#FF6347",9 "#8FBC8F",10 "#DDA0DD",11 "#FFD700",12 "#9932CC",13 "#BC8F8F",14 "#483D8B",15 "#CD5C5C",16 "#00FFFF",17 "#FFFF00",18 "#48D1CC",19 "#778899");
set key right top box;
plot [plot_data];
    """

    gnu_path = get_the_path("gnu", output_path)

    tax_list = ['kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    for t in tax_list:
        gc_data = os.path.join(gnu_path, "%s.gc" % t)

        field_list = []
        fh = open(gc_data, "r")
        for line in fh:
            if line.startswith("#"):
                field_list = line.strip().split('\t')
                break
        fh.close()

        plot_list = []

        # taken from jigsaw.taxGcPlot.pl - I assume we need these for some reason? Currently not in use
        color_list = [
        "#FF0000", # red
        "#FFFF00", # yellow
        "#008000", # green
        "#FFA500", # orange,
        "#A52A2A", # brown
        "#1E90FF", # dodgerblue
        "#DAA520", # goldenrod
        "#9FAFDF", # navyblue
        "#FF6347", # tomato
        "#8FBC8F", # darkseagreen
        "#DDA0DD", # plum
        "#FFD700", # gold
        "#9932CC", # darkorchid
        "#BC8F8F", # rosybrown
        "#483D8B", # darkslateblue
        "#CD5C5C", # indianred
        "#00FFFF", # cyan
        ]

        #color_len = len(color_list)

        # for each field, create the plot line for gnuplot
        fcnt = 0
        for f in field_list:
            fcnt += 1
            if f.startswith("#"):
                continue

            if f == "Total":
                if not show_total_flag:
                    continue

            if f == "-":
                f = "No Hit"

            plot_buff = "'%s' u 1:%s with boxes" % (gc_data, fcnt)
            plot_buff += " title \"%s\"" % (f)

            #plot_buff += " notitle"

            if f == "Total":
                plot_buff += " lt rgb 'gray'"
            elif f == "Unassigned":
                plot_buff += " lt rgb 'blue'"
            else:

                pass
                # 3 = first element item
                #if fcnt < color_len + 3:
                #    plot_buff += " lt rgb '%s'" % (color_list[fcnt - 3])


            plot_list.append(plot_buff)


        gc_hist_png = os.path.join(output_path, "%s_%s.png" % (file_type, t.title()))
        plot_title = "%s level GC for %s" % (t.title(), library_name)
        if not show_total_flag:
            gc_hist_png = os.path.join(output_path, "%s_nototal_%s.png" % (file_type, t.title()))
            plot_title += " (No Total)"


        replace_keys = {
            "plot_size" : plot_size,
            "gc_hist_png" : gc_hist_png,
            "plot_title" : plot_title,
            "plot_data" : ",\\\n".join(plot_list)
        }


        my_gnu_template = gnu_template
        for my_key in replace_keys:
            my_key2 = "[%s]" % my_key
            my_gnu_template = my_gnu_template.replace(my_key2, replace_keys[my_key])

        gnu_file = os.path.join(gnu_path, "%s.gnu.txt" % t)
        fh = open(gnu_file, "w")
        fh.write(my_gnu_template)
        fh.close()
        log.info("- created: %s", gnu_file)

        # do the plot
        gnu_log = os.path.join(gnu_path, "gnu_plot_%s.log" % t)
        cmd = "#gnuplot;gnuplot %s > %s 2>&1" % (gnu_file, gnu_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        if exit_code == 0:
            log.info("- created png: %s  %s", replace_keys['gc_hist_png'], msg_ok)
        else:
            log.error("- failed to create png: %s  %s", replace_keys['gc_hist_png'], msg_fail)
            err_cnt += 1

    return err_cnt


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "GC_Histogram"
    version = "1.1"

    prodigal_module = "prodigal/2.6.3"

    DIAMOND_NR = "/global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nr/diamond/nr" 
    #DIAMOND_CMD = "/global/projectb/sandbox/rqc/qc_user/miniconda/miniconda2/envs/qaqc2/bin/diamond"

    # burst buffer location?
    LAST_NR = "/global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nr/LAST/nr"
    BLASTX_NR = "/global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nr/nr"


    cluster = os.environ.get('NERSC_HOST', 'unknown')
    lastal_module = "lastal/869" # Cori/Denovo
    diamond_version = "diamond v0.9.19.120"

    version += " (%s, %s)" % (prodigal_module, lastal_module)


    uname = getpass.getuser() # get the user, brycef can't use scell.p, but qc_user can!

    color = {}
    color = set_colors(color, True)
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)


    # Parse options
    usage = "gc_histogram.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
Runs Prodigal to create protein based contigs (shreds the assembly)
Runs lastal against the Prodigal assembly (or diamond aligner)
Runs bbtools.stats.sh to get gc content for each contig in the Prodigal assembly
Looks up best hit for each contig in taxonomy server
Plots the gc histograms at each taxonomic level for each organism found (* almost everything)
    """

    parser = ArgumentParser(usage = usage)


    parser.add_argument("-f", "--fasta", dest="fasta", type = str, help = "Input fasta")
    parser.add_argument("-l", "--library", dest="library_name", type = str, help = "library name/chart title")
    parser.add_argument("-p", "--plot-size", dest="plot_size", type = str, help = "plot size in pixels e.g. '1800,1600px' (default 800,640px)")
    parser.add_argument("-t", "--file-type", dest="file_type", type = str, help = "png file prefix, (default = contigs)")
    parser.add_argument("-id", "--min-pct-id", dest="pct_id", type = float, help = "min percent identity (default = 30.0 = 30.0%)")
    parser.add_argument("-d", "--diamond", dest="diamond", default = False, action = "store_true", help = "use diamond aligner (default is last)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-o", "--output-path", dest="output_path", type = str, help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-v", "--version", action="version", version=version)

    status = "start" # step + X = run step and exit?  (e.g. gc_covX)  -- to do
    output_path = None
    fasta = None
    library_name = None
    plot_size = "800,640"
    file_type = "contigs" # or reads... prefix for png images
    min_pct_id = 30.0 # Alex approved it
    print_log = False
    diamond_flag = False

    # parse args
    args = parser.parse_args()
    print_log = args.print_log
    diamond_flag = args.diamond

    if args.pct_id:
        min_pct_id = args.pct_id

    if args.fasta:
        fasta = args.fasta

    if args.output_path:
        output_path = args.output_path

    if args.library_name:
        library_name = args.library_name

    if args.file_type:
        file_type = args.file_type

    if args.plot_size:
        plot_size = args.plot_size


    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/iso"


        if output_path == "t1":

            fasta = "/global/projectb/scratch/brycef/iso/BSHTN.fasta"
            library_name = "BSHTN"
            output_path = "gch1"

        if output_path == "t2":

            fasta = "/global/projectb/scratch/brycef/iso/CTOCX.fasta"
            library_name = "CTOCX"
            output_path = "gch2"

        if output_path == "t3":

            fasta = "/global/projectb/scratch/brycef/iso/CTOGZ.fasta"
            library_name = "CTOGZ"
            output_path = "gch3"

        if diamond_flag:
            output_path += ".diamond"

        output_path = os.path.join(test_path, output_path)

    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    if not library_name:
        library_name = os.path.basename(fasta)


    # initialize my logger
    log_file = os.path.join(output_path, "gc_hist.log")



    # log = logging object
    log_level = "INFO"
    log = None
    log = get_logger("gc_hist", log_file, log_level, print_log)


    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "fasta", fasta)
    log.info("%25s      %s", "library_name", library_name)
    log.info("%25s      %s", "min_pct_id", min_pct_id)
    log.info("%25s      %spx", "plot_size", plot_size)
    log.info("%25s      %s", "diamond_flag", diamond_flag)
    log.info("")

    if not os.path.isfile(fasta):
        log.error("Error: cannot find fasta %s  %s", fasta, msg_fail)
        sys.exit(2)

    if not library_name:
        library_name = os.path.basename(fasta)

    # gene contigs with genes(?)
    gene_fasta = do_prodigal(fasta)

    # use last aligner
    if diamond_flag:
        run_diamond(gene_fasta)
        if uname == "brycef":
            library_name += "-diamond"
    else:
        run_lastal(gene_fasta)
        if uname == "brycef":
            library_name += "-last"

    # summarize last log into best hits ...
    get_best_hits(min_pct_id)

    # make data for plotting
    do_gnu_plot_file()

    # plot with gnu
    err_cnt = do_gnu_plot(library_name, True, file_type)
    err_cnt += do_gnu_plot(library_name, False, file_type)

    if err_cnt > 0:
        log.error("errors encountered  %s", msg_fail)
        sys.exit(9)


    log.info("Completed %s", script_name)

    sys.exit(0)

