# ISO
# 2018-03-30
QD/JGI MICROBIAL ISOLATE ASSEMBLY QC REPORT: [:report_date:]
--------------------------------------------------------------------------------
Sequencing Project Id: [:seq_proj_id:]
Sequencing Project Name: [:seq_proj_name:]
Organism: [:organism_name:]


1) RAW DATA

Library_Name	Num_Reads	Run_Type	Read_Type	File_Name	Platform	Model
[:raw_data:]


2) ILLUMINA STD PE READ FILTERING STATS

Pairs of matching reads were removed from the dataset.

Total input reads: [:input_fastq_read_count:]  100.00%
Subsampled reads removed: [:subsampled_fastq_read_rmv:]  [:subsampled_fastq_read_pct:] 
Total reads removed: [:total_reads_removed:]  [:total_reads_removed_pct:] 
Total reads remaining: [:total_read_count:]  [:total_reads_pct:]


3) ILLUMINA STD PE READ IDENTIFICATION STATS

This step identifies contaminants but does not remove them from the dataset.

Total input reads:  [:input_fastq_read_count:]  (100.00)%

[:contam_txt:]


4) ASSEMBLY STATS

Assembly stats of the SPAdes assembly.

Estimated Percent Genome Recovery using checkM:
archaea: [:checkm_arc_estgenomecomsimple_pct:] 
bacteria: [:checkm_bac_estgenomecomsimple_pct:] 
lineage workflow: [:checkm_lwf_estgenomecomsimple_pct:]

[:asm_stats_file:]

Mapped Coverage: [:cov_avg_cov:]X


5) NCBI SCREENING STATS

This step identifies potential contaminants screened by NCBI for submission.

[:ncbi_contamination:]


6) KEY PIPELINE COMMANDS

A) Subsampling

Use bbtools to subsample the input fastq (a) down to 10m reads if more than 10m reads
reformat.sh: samplereadstarget=[:subsample_reads:]

B) Assembly

Assembler: [:assembler_name:]
Assembler params: [:assembler_params:]

C) Trimming

Use bbtools to exclude any contigs with:
- less than 1000 bases for each contig/scaffold

reformat.sh:  [:trim_params:]


7) WORKFLOW STEPS

1. Created SPAdes assembly of the normalized filtered data.
2. Trimmed SPAdes assembly.



8) ASSESSED GENOME PROJECT STANDARD:
Standard Draft


9) DOE AUSPICE STATEMENT FOR PUBLICATION

The work conducted by the U.S. Department of Energy Joint Genome Institute, a DOE Office of Science User Facility, is supported
under Contract No. DE-AC02-05CH11231.  The data was generated for JGI Proposal #[:proposal_id:].


10) Notes

If you are parsing this file to get metrics, please contact your JGI Project Manager to let them know what you need.
This report format is subject to change without notice.

