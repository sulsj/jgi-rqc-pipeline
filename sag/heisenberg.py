#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Heisenberg: runs methylation analysis using PacBio's smrtpipe
* yes, Breaking Bad was a good show
- replaces jgi-qaqc-hacksaw/bin/heisenberg.py

Dependencies
smrt_link_module 5.0.1 (Pacbio)
texlive (pdf maker)
bbtools (stats.sh)
pigz

To do:
- try TMP=different location for methylation
- constants for EXIT CODES (look at err.h)
- update methods with SOPs
- update motifMaker version (motifMaker --help doesn't show version, removed v1 from pdf, txt file)

Heisenberg 2.3  - 2015-01-07
- works with taxonomy id & library id
- fixed bug with motif list
- added inputs + ref to inputs
-- as outputs
- added chemistry to methylation template
- image name changes in motif/results folder
- jat update for analysis project id - if no ap id then leave at 0

2.3.1 - 2015-10-02
- chemistry fix

2.3.2 - 2016-03-28
- ap/at lookup & update

2.3.3 - 2016-08-08
- resubmit with more memory if motif fails

2.3.4 - 2016-09-22
- motifMaker.sh (part of smrtpipe) uses "java -Xmx8000m ", setting env var ''export _JAVA_OPTIONS="-Xmx32G"'' should fix it
- this export did not fix it, nor did submitting qsub with same var

2.3.5 - 2016-09-27
- create copies of motifMaker.sh with 32G instead of 8G and re-run last 2 steps (if find out of memory error only)
- fix for getting the status for initial runs

2.3.6 - 2016-11-11
- removed pacbio_library_type from reports (was set to MDA) - SEQQC-7471

2.4 - 2017-07-31
- updated to work on Cori
- major refactoring to simplify code (50% smaller)
- moved to jgi-rqc-pipeline repo
- new tests

2.4.1 - 2017-08-10
- fix for missing ap/at values in metadata.json (RQCSUPPORT-1625)

2.4.2 - 2017-08-16
- new motif_settings.xml (2.3.0), added --nproc to pbalign in motif_settings.xml
Note:
--- RS_Modification_Detection.1 - does NOT produce motifs found (motif_summary.csv)
--- RS_Modification_and_Motif_Analysis.1 does
- added "Motifs with ObjScores less than 5000 should be considered suspect" to pdf and txt report
- added [nproc] to pbalign params in motif_settings.xml
- fail if more than one chemistry found in smrtcells
- use img direct file location for reference
- use xmx memory env var (BPPBO)

2.4.3 - 2017-10-19
- added auspice statement

5.0 - 2017-11-09
- use pbsmrtlink 5.0.1
- refactored code to handle different output from smrtlink
- updated pdf, txt report to handle new changes (reviewed with Matt Blow)

5.0.1 - 2017-11-21
- can't use a list of files as outputs for jamo (filtered_subreads)

5.0.2 - 2017-11-28
- create a tarball for filtered_subreads if more than one *subreads.bam file

5.0.3 - 2018-01-30
- use module load jamo (shifter image broken), run on denovo

5.0.4 - 2018-07-16
- ignore missing files and ignore "None" for chemistry (SDM-2096)

* BZGTW, BZGTC, BZGTA, BZGSZ, BZGSX all have multiple files for filtered_subreads - how to handle it?

methylation
- release to IMG (optional, for now only if it has IMG ref id)


module load smrtlink/5.0.1.9585

dataset create --force --type HdfSubreadSet /global/projectb/scratch/brycef/pbmid/BWOAU/config/hdfsubreadset.xml /global/projectb/scratch/brycef/pbmid/BWOAU/input.fofn
pbsmrtpipe show-workflow-options -o /global/projectb/scratch/brycef/pbmid/BWOAU/config/preset.pb.xml
pbsmrtpipe show-template-details pbsmrtpipe.pipelines.ds_modification_motif_analysis -o motif.pb.xml
 pbsmrtpipe show-templates
eid_ref_dataset?


 17. Base Modification Detection                            0.1.0 pbsmrtpipe.pipelines.ds_modification_detection
Base Modification Analysis Pipeline - performs resequencing workflow and detects methylated bases from kinetic data
 18. Base Modification and Motif Analysis                   0.1.0 pbsmrtpipe.pipelines.ds_modification_motif_analysis *
$entry:eid_subread
$entry:eid_ref_dataset
Modification and Motif Analysis Pipeline - performs resequencing workflow, detects methylated bases from kinetic data, and identifies consensus nucleotide motifs


Methylation notes:
- Needs a reference to run methylation, normally from the assembled fasta which comes from IMG after they edit it
- 4mC, 6mA detected by Pacbio, the other 6 need 200x coverage
- Don't need a "good" assembly for methylation
- Pacbio library: need 50x 3kb library for methylation, 100x 10kb (high molecular weight DNA) for assembly

PB 2.3:
 Base Modification The Motif Finder software does not work well with high-GC genome base modifications. (24315)


$ module load jamo/dev
$ jat import methylation .

testing ...
$ jat import bryce-nov/methylation methylation.metadata.json
* worked!
* good if reference_genome_size int vs string resolved (set to number in template)

jat import methylation methylation.metadata.json

$ jat import
- lists the templates available

~~~~~~~~~~~~~~~~~~~[ Test Cases ]
./heisenberg.py -o t1
7.24 hours (mostly meth step)
original run: $RQC_SHARED/pipelines/heisenberg/archive/00/00/30/21/
$ qsub -b yes -j yes -m n -w e -l ram.c=3.5G,h_vmem=3.5G,h_rt=108000,s_rt=107995 -pe pe_16 16 -N meth-t1 -P gentech-rqc.p \
-o /global/projectb/scratch/brycef/meth/t1.qsub.log \
-js 504 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/heisenberg.py -o t1"

cori
$ sbatch -C haswell /global/homes/b/brycef/git/jgi-rqc-pipeline/sag/meth-t1.sh
Submitted batch job 5968853

t2: 9hrs 37min (old code) - 10.98 hours new code (mostly meth step)
$ qsub -b yes -j yes -m n -w e -l ram.c=3.5G,h_vmem=3.5G,h_rt=108000,s_rt=107995 -pe pe_16 16 -N meth-t2 -P gentech-rqc.p \
-o /global/projectb/scratch/brycef/meth/t2.qsub.log \
-js 504 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/heisenberg.py -o t2"


t3 - for new pb runs
$ qsub -b yes -j yes -m n -w e -l ram.c=3.5G,h_vmem=3.5G,h_rt=108000,s_rt=107995 -pe pe_16 16 -N meth-BZGSH -P gentech-rqc.p \
-o /global/projectb/scratch/brycef/meth/BZGSH.qsub.log \
-js 508 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/heisenberg.py -o t3"




"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import re
import sys
from argparse import ArgumentParser
import time
import json
import glob
import getpass
import numpy

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, checkpoint_step, append_rqc_file, append_rqc_stats, get_colors, get_analysis_project_id, run_cmd, get_msg_settings
from micro_lib import load_config_file, get_the_path, get_library_info, save_file, get_file, merge_template, get_smrtcell_chemistry, make_asm_stats, save_asm_stats, get_nproc, format_val_pct


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
create input.fofn
- use bas.h5 as the smrt cell count and use to figure out chemistry used

'''

def setup_pbsmrtpipe(library_name):
    log.info("setup_pbsmrtpipe: %s", library_name)

    step_name = "setup_pbsmrtpipe"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    input_fofn = os.path.join(output_path, "input.fofn")

    if os.path.isfile(input_fofn) and 1 == 2:
        log.info("- input file exists: %s, not generating new one  %s", input_fofn, msg_warn)
        status = "%s skipped" % step_name

    else:
        chem_list = []
        smrt_cell_cnt = 0
        missing_cnt = 0 # files missing on file system, probably need to be restored

        # for production_macro: seq_units list
        #cmd = "#%s;jamo report select file_name, file_path where file_type = 'fastq' and metadata.library_name = '%s' as json" % (config['jamo_module'], library_name)
        cmd = "module load jamo;jamo report select file_name, file_path where file_type = 'fastq' and metadata.library_name = '%s' as json" % (library_name)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        if exit_code == 0 and std_out:

            fq_list = []
            j = json.loads(std_out)
            for fs in j:
                fq_list.append(fs['file_name'])
            append_rqc_stats(rqc_stats_log, "fastq_list", ",".join(fq_list))
            log.info("- fastq list: %s", fq_list)

        #cmd = "#%s;jamo report select file_name, file_path where file_type = 'h5' and metadata.library_name = '%s' as json" % (config['jamo_module'], library_name)
        cmd = "module load jamo;jamo report select file_name, file_path where file_type = 'h5' and metadata.library_name = '%s' as json" % (library_name)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # returns
        # [{"file_name": "pbio-1220.11039.fastq", "metadata.sdm_smrt_cell.fs_location": "/global/seqfs/sdm/prod/pacbio/runs/pbr4197_668/A07_1"}, {"file_name": "pbio-1223.11060.fastq", "metadata.sdm_smrt_cell.fs_location": "/global/seqfs/sdm/prod/pacbio/runs/pbr4200_670/A04_1"}]

        if exit_code == 0 and std_out:

            j = json.loads(std_out)
            h5_cnt = 0

            fh = open(input_fofn, "w")
            for fs in j:

                smrt_file = os.path.join(fs['file_path'], fs['file_name']) # bax.h5 or .bas.h5
                log.info(" - file: %s", smrt_file)
                if not os.path.isfile(smrt_file):
                    log.error("- file missing: %s  %s", smrt_file, msg_warn)
                    missing_cnt += 1

                if fs['file_name'].endswith(".bas.h5"):
                    smrt_cell_cnt += 1

                    xml_file = smrt_file.replace(".bas.h5", ".metadata.xml")
                    smrt_chemistry = get_smrtcell_chemistry(xml_file, config['chemistry_mapping_xml'], log)
                    log.info(" - smrt cell chemistry: %s", smrt_chemistry)
                    if smrt_chemistry:
                        chem_list.append(smrt_chemistry)

                else:
                    fh.write(smrt_file+"\n")
                    h5_cnt += 1

            fh.close()
            log.info("- created file: %s", input_fofn)

            save_file(input_fofn, "input_fofn", file_dict, output_path)
            
            smrtcell_chem = ",".join(list(set(chem_list)))
            log.info("- chemistry list: %s", smrtcell_chem)
            log.info("- smrtcell cnt: %s, h5 cnt: %s", smrt_cell_cnt, h5_cnt)

            append_rqc_stats(rqc_stats_log, "chemistry", smrtcell_chem)
            append_rqc_stats(rqc_stats_log, "pacbio_smrtcell_cnt", smrt_cell_cnt)

            if h5_cnt > 0:
                status = "%s complete" % step_name
            else:
                status = "%s failed" % step_name

            
            if len(list(set(chem_list))) > 1:
                log.error("- more than one chemistry found!  %s", msg_fail)
                status = "%s failed" % step_name

            if missing_cnt > 0:
                log.error("- %s files not on the file system!  %s", missing_cnt, msg_fail)
                # 2018-07-16: turned off because some weird files still in JAMO (SDM-2096)
                # - temp, turn back on in August
                #status = "%s failed" % step_name

            # initial input node
            #append_flow(flow_file, "input_fofn", "%s H5<br><smf>SMRT Cells: %s<br>Chemistry: %s</f>" % (library_name, smrt_cell_cnt, smrtcell_chem), "", "", "")

        else:
            status = "%s failed" % step_name


    checkpoint_step(status_log, status)
    
    
    return status


'''
http://pbsmrtpipe.readthedocs.io/en/master/getting_started.html

 create the config files needed for hgap4
$ dataset create --force --type HdfSubreadSet /global/projectb/scratch/brycef/pbmid/BWOAU/config/hdfsubreadset.xml /global/projectb/scratch/brycef/pbmid/BWOAU/input.fofn
$ pbsmrtpipe show-workflow-options -o /global/projectb/scratch/brycef/pbmid/BWOAU/config/preset.pb.xml
$ pbsmrtpipe show-template-details pbsmrtpipe.pipelines.ds_modification_motif_analysis -o motif.pb.xml

'''
def setup_motif_xml():
    log.info("setup_motif_xml")

    step_name = "setup_motif_xml"

    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(CONFIG_PATH, output_path)


    subread_xml = os.path.join(the_path, "hdfsubreadset.xml")
    preset_file = os.path.join(the_path, "preset.xml")
    motif_file = os.path.join(the_path, "motif_settings.xml")

    input_fofn = get_file("input_fofn", file_dict, output_path, log)

    if os.path.isfile(motif_file):
        log.info("- motif config file exists: %s, not generating new one  %s", motif_file, msg_warn)
        status = "%s skipped" % step_name

    else:

        # create subreadset.xml file - used to convert bax.h5 to subreads.bam
        subread_log = os.path.join(the_path, "hdfsubreadset.log")
        cmd = "#%s;dataset create --force --type HdfSubreadSet %s %s > %s 2>&1" % (config['smrt_link_module'], subread_xml, input_fofn, subread_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        # create preset.xml
        workflow_log = os.path.join(the_path, "workflow.log")
        preset_file_org = preset_file.replace(".xml", ".pb.xml")

        cmd = "#%s;pbsmrtpipe show-workflow-options -o %s > %s 2>&1" % (config['smrt_link_module'], preset_file_org, workflow_log)
        # -o = xml, -j = json
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # create motif_settings.xml
        motif_log = os.path.join(the_path, "motif.log")
        #motif_file_org = motif_file.replace(".xml", ".pb.xml")

        cmd = "#%s;pbsmrtpipe show-template-details pbsmrtpipe.pipelines.ds_modification_motif_analysis -o %s > %s 2>&1" % (config['smrt_link_module'], motif_file, motif_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # didn't see a way to set these settings from the command line

        # modify preset.json
        nproc = get_nproc()
        tmp_path = get_the_path("tmp", output_path)

        fh = open(preset_file_org, "r")
        fhw = open(preset_file, "w")
        val = ""
        for line in fh:

            if "distributed_mode" in line:
                val = "False"

            if "max_nproc" in line:
                val = nproc

            if "tmp_dir" in line:
                val = tmp_path

            if val and "value" in line:
                line = "            <value>%s</value>\n" % val
                val = ""

            fhw.write(line)

        fh.close()
        fhw.close()
        log.info("- created: %s", preset_file)


        # modify motif.xml - nothing to change


    save_file(subread_xml, "subread_file", file_dict, output_path)
    save_file(preset_file, "preset_file", file_dict, output_path)
    save_file(motif_file, "motif_file", file_dict, output_path)

    status = "%s complete" % step_name


    checkpoint_step(status_log, status)
    return status


'''
Copy the reference file from the original location to output_path/REFERENCE_PATH
- remove bad chars from header

'''
def get_reference(taxon_id, reference_fasta):
    log.info("get_reference: taxon_id = %s, ref file = %s", taxon_id, reference_fasta)

    step_name = "get_reference"
    status = "%s in progress" % step_name


    checkpoint_step(status_log, status)

    the_path = get_the_path(REFERENCE_PATH, output_path)

    ref_file = "no_ref"
    append_rqc_stats(rqc_stats_log, 'reference_id', taxon_id)

    if taxon_id:
        ref_file = os.path.join(the_path, "%s.clean.fasta" % taxon_id)
    else:
        ref_file = os.path.join(the_path, os.path.basename(reference_fasta).replace(".fasta", ".clean.fasta"))

    if os.path.isfile(ref_file):
        log.info("- ref file exists: %s, not generating new one  %s", ref_file, msg_warn)
        status = "%s skipped" % step_name

    else:


        if taxon_id  > 0:

            # location of references for img are:
            #/global/dna/projectdirs/microbial/img_web_data/taxon.fna/(taxon_id).fna
            img_fasta = os.path.join(config['img_ref_path'], "%s.fna" % taxon_id)
            if os.path.isfile(img_fasta):
                img_ref_file = os.path.join(the_path, os.path.basename(img_fasta))
                cmd = "cp %s %s" % (img_fasta, img_ref_file)
                std_out, std_err, exit_code = run_cmd(cmd, log)
                ref_file = img_ref_file
                log.info("- found IMG file: %s", img_fasta)

            else:

                img_file = os.path.join(config['img_path'], "%s.tar.gz" % taxon_id)

                if os.path.isfile(img_file):
                    tar_log = os.path.join(the_path, "tar.log")
                    cmd = "cd %s;tar -xzf %s --strip-components 1 > %s 2>&1" % (the_path, img_file, tar_log)
                    std_out, std_err, exit_code = run_cmd(cmd, log)


                    ref_file = os.path.join(the_path, "%s.fna" % taxon_id)
                    log.info("- found IMG file: %s", ref_file)

                else:
                    log.error("- no IMG reference file for taxon_id %s  %s", taxon_id, msg_fail)


        else:
            if os.path.isfile(reference_fasta):
                ref_file = os.path.join(the_path, os.path.basename(reference_fasta))
                cmd = "cp %s %s" % (reference_fasta, ref_file)
                std_out, std_err, exit_code = run_cmd(cmd, log)

            else:
                log.error("- no reference file %s!  %s", reference_fasta, msg_fail)


        # create a "clean" reference by removing illegal chars from the headers
        if os.path.isfile(ref_file):
            clean_ref_file = ref_file.replace(".fasta", ".clean.fasta")
            if ref_file.endswith(".fa"):
                clean_ref_file = ref_file.replace(".fa", ".clean.fasta")

            if taxon_id > 0:
                clean_ref_file = ref_file.replace(".fna", ".clean.fasta")

            # 2017-10-25 - Pacbio cannot handle a simple blank line at the end of the reference ...

            fh = open(ref_file, "r")
            fhw = open(clean_ref_file, "w")
            for line in fh:
                if len(line.strip()) > 0:
                    line = re.sub(r'[^A-Za-z0-9\>\n]+', "_", line)
                    fhw.write(line)
            fh.close()
            fhw.close()


            # replace ":" with "_" because pacbio motif detection crashes otherwise
            #cmd = "sed -e s/:/_/g %s > %s" % (ref_file, clean_ref_file)
            #std_out, std_err, exit_code = run_cmd(cmd, log)

            log.info("- created clean reference: %s", clean_ref_file)
            save_file(clean_ref_file, "ref_fasta", file_dict, output_path)

            status = "%s complete" % step_name

        else:
            log.error("- Reference file does not exists!  %s", msg_fail)
            status = "%s failed" % step_name


    checkpoint_step(status_log, status)
    return status


'''
could use smrtlink to format the reference file
$ pbsmrtpipe show-template-details pbsmrtpipe.pipelines.sa3_ds_fasta_to_reference -o ref.xml
$ pbsmrtpipe pipeline-id pbsmrtpipe.pipelines.sa3_ds_fasta_to_reference \
  --preset-xml /global/projectb/scratch/brycef/meth/BZGSH/config/preset.xml \
  --preset-xml /global/projectb/scratch/brycef/meth/BZGSH/ref.xml \
  -e eid_ref_fasta:/global/projectb/scratch/brycef/meth/BZGSH/r.fasta \
  -o /global/projectb/scratch/brycef/meth/BPPBO/ref4
* assertion error if blank lines in fasta (including the LAST line)

perl -pe 's/[^A-Za-z0-9\>\n]+/_/g' ref/2754412715.clean.fasta > r.fasta


# fasta-to-reference fasta_file output_path name
--ploidy haploid
$ fasta-to-reference /global/projectb/scratch/brycef/meth/BPPBO/ref/2740891844.clean.fasta \
/global/projectb/scratch/brycef/meth/BPPBO/ref5 \
reference

2 minutes
'''
def upload_pbreference(library_name):
    log.info("upload_reference: %s", library_name)

    step_name = "upload_reference"
    status = "%s in progress" % step_name


    checkpoint_step(status_log, status)

    config_path = get_the_path(CONFIG_PATH, output_path)
    the_path = get_the_path(REF_UPLOAD_PATH, output_path)

    ref_pb_config = os.path.join(config_path, "reference.pb.xml")
    ref_config = os.path.join(config_path, "reference.xml")

    ref_fasta = get_file("ref_fasta", file_dict, output_path, log)

    ref_path_name = "reference"
    ref_xml = os.path.join(the_path, ref_path_name, "referenceset.xml")

    if os.path.isfile(ref_xml):
        log.info("- skipping upload reference")
        status = "%s skipped" % step_name

    else:

        """
        ref_log = os.path.join(config_path, "ref.log")
        cmd = "#%s;pbsmrtpipe show-template-details pbsmrtpipe.pipelines.sa3_ds_fasta_to_reference -o %s" % (config['smrt_link_module'], ref_pb_config, ref_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        fh = open(ref_config, "r")
        fhw = open(ref_pb_config, "w")
        val = ""
        for line in fh:

            if "reference_name" in line:
                val = library_name
            # organism -> don't have this info
            # ploidy: haploid = 1 = microbial data, diploid = 2 (no)

            if "value" in line and val:
                line = "            <value>%s</value>\n" % val
                val = ""

            fhw.write(line)
        fh.close()
        fhw.close()

        log.info("- created reference config: %s", ref_config)
        """

        #Usage: fasta-to-reference [options] fasta-file output-dir name
        fasta2ref_log = os.path.join(the_path, "fasta2ref.log")

        # error if ref path exists
        if os.path.isdir(ref_path_name):
            cmd = "rm -rf %s" % ref_path_name
            std_out, std_err, exit_code = run_cmd(cmd, log)

        cmd = "#%s;fasta-to-reference %s %s %s > %s 2>&1" % (config['smrt_link_module'], ref_fasta, the_path, ref_path_name, fasta2ref_log)
        # --ploidy haploid
        std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(ref_xml):
        save_file(ref_xml, "ref_xml", file_dict, output_path)
        status = "%s complete" % step_name

    else:
        log.error("- Reference xml not created: %s  %s", ref_xml, msg_fail)
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)
    return status



'''
Do Modification and Motif analysis to find methylated bases

$ pbsmrtpipe show-template-details pbsmrtpipe.pipelines.ds_modification_motif_analysis
$entry:eid_subread
$entry:eid_ref_dataset


pbsmrtpipe pipeline-id pbsmrtpipe.pipelines.sa3_ds_resequencing \
  -e eid_subread:/data/smrt/2372215/0007/Analysis_Results/m150404_101626_42267_c100807920800000001823174110291514_s1_p0.all.subreadset.xml \
  -e eid_ref_dataset:/data/references/lambdaNEB/lambdaNEB.referenceset.xml


pbsmrtpipe pipeline-id pbsmrtpipe.pipelines.ds_modification_motif_analysis \
--preset-xml /global/projectb/scratch/brycef/meth/BZGSH/config/preset.xml \
--preset-xml /global/projectb/scratch/brycef/meth/BZGSH/config/motif_settings.xml \
-e eid_subread:/global/projectb/scratch/brycef/meth/BZGSH/subreads/tasks/pbcoretools.tasks.h5_subreads_to_subread-0/subreads.subreadset.xml \
-e eid_ref_dataset:/global/projectb/scratch/brycef/meth/BZGSH/ref_upload/reference/sequence/reference.fasta \
 -o /global/projectb/scratch/brycef/meth/BZGSH/motif4
* cannot have blank lines in reference.fasta or we get:
    assert (header_[0] == ">" and header_[-1] == "\n")
AssertionError

BZGSH - 3.0 took 7 hours, 5.0 took 2 hours

'''
def do_pb_modification_n_motif():
    log.info("do_methylation")

    step_name = "methylation"
    status = "%s in progress" % step_name


    checkpoint_step(status_log, status)

    the_path = get_the_path(METHYLATION_PATH, output_path)

    motif_summary = os.path.join(the_path, "tasks", "motif_maker.tasks.find_motifs-0", "motifs.csv")

    if os.path.isfile(motif_summary):

        log.info("- motif_summary file exists: %s, not generating new one  %s", motif_summary, msg_warn)
        status = "%s skipped" % step_name

    else:

        preset_file = get_file("preset_file", file_dict, output_path, log)
        subread_xml = get_file("subread_file", file_dict, output_path, log)

        subread_path = get_the_path(SUBREAD_PATH, output_path)


        hdf_subread_log = os.path.join(the_path, "hdfsubread.log")
        subread_bam_list = glob.glob(os.path.join(subread_path, "tasks", "pbcoretools.tasks.h5_subreads_to_subread-0", "*.subreads.bam"))

        #if os.path.isfile(subread_bam):
        if len(subread_bam_list) > 0:
            log.info("- found *.subreads.bam, skipping step")

        else:
            # 10 minutes to convert 3 h5s
            cmd = "#%s;pbsmrtpipe pipeline-id pbsmrtpipe.pipelines.sa3_hdfsubread_to_subread"
            #cmd += "--preset-json %s -e eid_hdfsubread:%s "
            cmd += " --preset-xml %s -e eid_hdfsubread:%s"
            cmd += " -o %s > %s 2>&1"
            cmd = cmd % (config['smrt_link_module'], preset_file, subread_xml, subread_path, hdf_subread_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        subread_xml = os.path.join(subread_path, "tasks", "pbcoretools.tasks.h5_subreads_to_subread-0", "subreads.subreadset.xml")
        save_file(subread_xml, "subread_bam_file", file_dict, output_path)

        # modification & motif analysis
        motif_file = get_file("motif_file", file_dict, output_path, log)
        ref_fasta = os.path.join(output_path, "ref_upload/reference/sequence", "reference.fasta")

        motif_log = os.path.join(the_path, "motif.log")
        cmd = "#%s;pbsmrtpipe pipeline-id pbsmrtpipe.pipelines.ds_modification_motif_analysis"
        cmd += " --preset-xml %s --preset-xml %s "
        cmd += " -e eid_subread:%s"
        cmd += " -e eid_ref_dataset:%s"
        cmd += " -o %s > %s 2>&1"
        cmd = cmd % (config['smrt_link_module'], preset_file, motif_file, subread_xml, ref_fasta, the_path, motif_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(motif_summary):

        # files needed for metadata:
        # - added in do_pdf_report, just gzip *'d ones
        # motif_summary.csv: ./motif/tasks/motif_maker.tasks.find_motifs-0/motifs.csv
        # motifs.gff.gz: motif/tasks/motif_maker.tasks.reprocess-0/motifs.gff    *
        # modifications.gff.gz: ./motif/tasks/kinetics_tools.tasks.ipd_summary-0/basemods.gff *
        # filtered_subreads.bam: subreads/tasks/pbcoretools.tasks.h5_subreads_to_subread-0/*.subreads.bam = filtered subreads - multiple files?
        # aligned_reads.bam: tasks/pbalign.tasks.pbalign-0/mapped.alignmentset.bam

        pigz_list = ['tasks/motif_maker.tasks.reprocess-0/motifs.gff', 'tasks/kinetics_tools.tasks.ipd_summary-0/basemods.gff']
        for pfile in pigz_list:

            pig_file = os.path.join(the_path, pfile)
            if os.path.isfile(pig_file):
                cmd = "#pigz;pigz %s" % pig_file
                std_out, std_err, exit_code = run_cmd(cmd, log)


        status = "%s complete" % step_name

    else:
        log.error("- Methylation failed!  %s", msg_fail)
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)
    return status


'''
All shorter reads are aligned to the seed reads, in order to generate consensus sequences with high accuracy.
We refer to these as pre-assembled reads but they can also be thought of as “error corrected” reads.

Get read stats for:
- raw: convert *.bam to fasta files
- filtered subreads: hgap/tasks/pbcoretools.tasks.bam2fasta-0/subreads.fasta
- error corrected: falcon_ns.tasks.task_falcon1_run_merge_consensus_jobs-0/preads4falcon.fasta

# converts the bam files into the raw reads - same results as using bam2fasta in smrtlink
$ shifter --image=registry.services.nersc.gov/jgi/smrtlink:5.0.1.9585 \
/smrtlink/smrtcmds/bin/pbsmrtpipe pipeline-id pbsmrtpipe.pipelines.sa3_ds_subreads_to_fastx \
--preset-xml /global/projectb/scratch/brycef/pbmid/CAOTA/config/preset.xml \
-e eid_subread:/global/projectb/scratch/brycef/pbmid/CAOTA/subread/tasks/pbcoretools.tasks.h5_subreads_to_subread-0/subreads.subreadset.xml \
-o /global/projectb/scratch/brycef/pbmid/CAOTA/fasta
9.04minutes
215359 contigs, 940,763,724 bp
RQC reports from the same library's fasta files in jamo: 187683 contigs, 871,401,075 bp

'''
def do_fasta_stats():
    log.info("get_fasta_stats")


    step_name = "fasta_stats"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(FASTA_PATH, output_path)
    subread_path = get_the_path(SUBREAD_PATH, output_path)

    done_file = os.path.join(the_path, "done.txt") # never created

    raw_fasta_list = []
    filtered_list = [] # subreads/*subreads.bam

    if os.path.isfile(done_file):
        log.info("- skipping %s, found file: %s", step_name, done_file)

    else:

        subread_path = get_the_path(SUBREAD_PATH, output_path)
        # create subreads + scraps together to get ALL raw reads

        # http://pacbiofileformats.readthedocs.io/en/5.0/BAM.html
        # scraps.bam = Excised adapters, barcodes, and rejected subreads
        # subreads.bam = Analysis-ready subreads from movie
        bam_file_list = glob.glob(os.path.join(subread_path, "tasks", "pbcoretools.tasks.h5_subreads_to_subread-0", "*.bam"))


        log.info("- found %s bam files to convert", len(bam_file_list))
        for bam_file in bam_file_list:
            bam_log = os.path.join(the_path, os.path.basename(bam_file).replace(".bam", ".log"))

            fasta_file = os.path.join(the_path, os.path.basename(bam_file).replace(".bam", "")) # don't add .fasta, done automatically

            if os.path.isfile(fasta_file + ".fasta"):
                log.info("- found fasta: %s.fasta", fasta_file)

            else:
                cmd = "#%s;bam2fasta -u -o %s %s > %s 2>&1" % (config['smrt_link_module'], fasta_file, bam_file, bam_log)
                # -u = don't gzip it
                std_out, std_err, exit_code = run_cmd(cmd, log)

            raw_fasta_list.append(fasta_file + ".fasta")
            if "scraps" not in fasta_file:
                filtered_list.append(fasta_file + ".fasta")


    if len(raw_fasta_list) > 0:

        # raw reads
        log.info("- raw reads: %s fasta files", len(raw_fasta_list))
        get_contig_length(raw_fasta_list, "raw_reads")

        # filtered subreads - part of hgap4, also the *subreads.bam from subreads
        fasta = get_file("filtered_subreads_fasta", file_dict, output_path, log)
        fasta_list = [fasta]

        log.info("- filtered subreads: %s fasta files", len(fasta_list))
        get_contig_length(filtered_list, "filtered_subreads")

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
return the length of all contigs from the fasta
checked against BWOAU and it was correct
'''
def get_contig_length(fasta_list, stats_key):
    log.info("get_contig_length: %s", fasta_list)

    contig_length_list = []
    contig_big_list = [] # all contigs > big_size are in this list

    # config = 5000bp
    big_size = int(config['contig_length_big']) # contigs > 5000 bases are likely to have the 16s sequence coverage high enough - indicates sequencing quality
    for fasta in fasta_list:
        if os.path.isfile(fasta):

            contig_length = 0
            fh = open(fasta, "r")

            for line in fh:
                line = line.strip()
                if line.startswith(">"):
                    if contig_length > 0:
                        contig_length_list.append(contig_length)
                        if contig_length > big_size:
                            contig_big_list.append(contig_length)
                        contig_length = 0
                else:
                    contig_length += len(line)

            if contig_length > 0:
                contig_length_list.append(contig_length)
                if contig_length > big_size:
                    contig_big_list.append(contig_length)

            fh.close()
        else:
            log.error("- missing file: %s", fasta)

    read_cnt = len(contig_length_list)
    read_length_avg = numpy.average(contig_length_list)
    read_length_stdev = numpy.std(contig_length_list)
    base_cnt = numpy.sum(contig_length_list)
    log.info("- read count: %s", "{:,}".format(read_cnt))
    log.info("- base count: %s", "{:,}".format(base_cnt))
    log.info("- avg read length: %s +/- %s", "{:.1f}".format(read_length_avg), "{:.1f}".format(read_length_stdev))


    # save stats

    append_rqc_stats(rqc_stats_log, stats_key + "_read_cnt", read_cnt)
    append_rqc_stats(rqc_stats_log, stats_key + "_base_cnt", base_cnt)
    append_rqc_stats(rqc_stats_log, stats_key + "_avg_read_length", read_length_avg)
    append_rqc_stats(rqc_stats_log, stats_key + "_stdev_read_length", read_length_stdev)

    read_cnt = len(contig_big_list)
    read_length_avg = numpy.average(contig_big_list)
    read_length_stdev = numpy.std(contig_big_list)
    base_cnt = numpy.sum(contig_big_list)


    log.info("- read count > %sbp: %s", big_size, "{:,}".format(read_cnt))
    log.info("- base count > %sbp: %s", big_size, "{:,}".format(base_cnt))
    log.info("- avg read length > %sbp: %s +/- %s", big_size, "{:.1f}".format(read_length_avg), "{:.1f}".format(read_length_stdev))

    # save stats
    append_rqc_stats(rqc_stats_log, stats_key + "_big_read_cnt", read_cnt)
    append_rqc_stats(rqc_stats_log, stats_key + "_big_base_cnt", base_cnt)
    append_rqc_stats(rqc_stats_log, stats_key + "_big_avg_read_length", read_length_avg)
    append_rqc_stats(rqc_stats_log, stats_key + "_big_stdev_read_length", read_length_stdev)



'''
Create formatted motif table for pdf & txt report
Grab the png files
Calculate mapping stats

'''
def calc_pb_report():
    log.info("calc_pb_report")

    motif_path = os.path.join(output_path, METHYLATION_PATH)
    task_path = os.path.join(motif_path, "tasks")

    step_name = "calc_report"
    status = "%s in progress" % step_name

    the_path = get_the_path(REPORT_PATH, output_path)

    checkpoint_step(status_log, status)

    err_cnt = 0

    if 1==1:

        # motif summary table
        motif_csv = os.path.join(task_path, "motif_maker.tasks.find_motifs-0", "motifs.csv")

        append_rqc_file(rqc_file_log, "motif_csv", motif_csv)


        # use Matt Blow's reformatter to change motif_csv to the tab version
        motif_log = os.path.join(the_path, "motif_format.log")
        reformat_cmd = config['reformat_motif_cmd'].replace("[my_path]", my_path)
        cmd = "%s %s > %s 2>&1" % (reformat_cmd, motif_csv, motif_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        motif_tab = os.path.join(task_path,  "motif_maker.tasks.find_motifs-0", "motif_summary_reformat.tab")

        if os.path.isfile(motif_tab):

            # better column alignment in txt report
            motif_txt = os.path.join(the_path, "motif_summary.txt")
            cmd = "column -t %s > %s" % (motif_tab, motif_txt)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            append_rqc_file(rqc_file_log, "motif_tab", motif_txt)

            # for pdf report
            motif_tex = "\\begin{tabular}{|l|r|c|r|r|r|r|r|r|}\n"
            motif_tex += "\\hline\n"
            motif_tex += "Motif & Pos & Type & Count & Pct Mod & MeanScore & MeanIpdR & MeanCov & ObjScore\\tabularnewline\n"
            motif_tex += "\\hline\n"


            motif_list = []
            fh = open(motif_tab, "r")
            for line in fh:
                if line.startswith("Motif"):
                    continue
                arr = line.split()
                if len(arr) > 2:
                    motif_list.append(arr[0])

                    # highlight rows with ObjScore < 5000 as "dodgy" - that is the scientific term according to Matt
                    if float(arr[8]) < 5000:

                        for i in range(len(arr)):
                            arr[i] = "\\textcolor{dodgy}{"+arr[i]+"}"

                    motif_tex += " & ".join(arr) + "\\tabularnewline\n"
                    motif_tex += "\\hline\n"


            fh.close()

            # write motif_tex
            motif_tex = motif_tex.replace("%", "\\%").replace("_", "\_")
            motif_tex += "\\end{tabular}\\\\\n"
            fhw = open(os.path.join(the_path, "motif_table.tex"), "w")
            fhw.write(motif_tex)
            fhw.close()

            motif_list_csv = ",".join(motif_list)
            append_rqc_stats(rqc_stats_log, "motif_list", motif_list_csv)

        else:
            log.error("- motif tab file missing!  %s", msg_fail)
            err_cnt += 1



        # images to save from motif/results
        img_dict = {
            "kinetic_detections_img" : "pbreports.tasks.modifications_report-0/kinetic_detections.png",
            "kinetic_histogram_img" : "pbreports.tasks.modifications_report-0/kinetic_histogram.png",
            "motif_histogram_img" : "pbreports.tasks.motifs_report-0/motif_histogram.png",
            "mapped_readlength_histogram" : "pbreports.tasks.mapping_stats-0/mapped_readlength_histogram.png",
            "mapped_subreadlength_histogram" : "pbreports.tasks.mapping_stats-0/mapped_subreadlength_histogram.png",
            # concordance: Map subreads of a ZMW to the same genomic location
            "mapped_concordance_vs_read_length_img" : "pbreports.tasks.mapping_stats-0/mapped_concordance_vs_read_length.png",
            "mapped_subread_concordance_histogram_img" : "pbreports.tasks.mapping_stats-0/mapped_subread_concordance_histogram.png",
        }

        for img in img_dict:
            my_img = os.path.join(task_path, img_dict[img])
            if os.path.isfile(my_img):

                append_rqc_file(rqc_file_log, img, my_img)
                log.info("- saved %s", my_img)

            else:
                log.warn("- missing file: %s  %s", my_img, msg_warn)
                err_cnt += 1

        # no coverage histograms

        # mostly useless info
        mapping_json = os.path.join(task_path, "pbreports.tasks.mapping_stats-0", "mapping_stats.json")
        j = open(mapping_json, "r")

        report_dict = {}
        my_json = json.load(j)
        if "attributes" in my_json:
            for a in my_json['attributes']:
                report_dict[a['id']] = a['value']


        # get reference genome size
        ref_fasta = get_file("ref_fasta", file_dict, output_path, log)
        asm_stats, asm_tsv = make_asm_stats(ref_fasta, "ref_fasta", the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, "ref_fasta", rqc_stats_log, log)



        my_dict = {} # save these to rqc_stats_log

        mapped_reads = int(report_dict['mapping_stats.mapped_subreads_n'])
        mapped_bases = int(report_dict['mapping_stats.mapped_subread_bases_n'])

        # load in fasta read stats from stats.tmp
        fh = open(rqc_stats_log, "r")
        for line in fh:
            #print line
            if line.startswith("#"):
                continue
            arr = line.split("=")

            if len(arr) > 1:
                my_key = str(arr[0]).lower().strip()
                my_val = str(arr[1]).strip()

                report_dict[my_key] = my_val

        fh.close()

        # *_pct are formatted in do_pdf_report

        my_dict['filtered_subreads_mapped'] = mapped_reads
        my_dict['filtered_subreads_mapped_pct'] = mapped_reads / float(report_dict['filtered_subreads_read_cnt'])

        my_dict['filtered_subreads_bases_mapped'] = mapped_bases
        my_dict['filtered_subreads_bases_mapped_pct'] = mapped_bases / float(report_dict['filtered_subreads_base_cnt'])

        my_dict['raw_reads_mapped'] = mapped_reads
        my_dict['raw_reads_mapped_pct'] = mapped_reads / float(report_dict['raw_reads_read_cnt'])

        my_dict['raw_bases_mapped'] = mapped_bases
        my_dict['raw_bases_mapped_pct'] = mapped_bases / float(report_dict['raw_reads_base_cnt'])

        my_dict['filtered_subreads_read_pct'] = float(report_dict['filtered_subreads_read_cnt']) / float(report_dict['raw_reads_read_cnt'])
        my_dict['filtered_subreads_base_pct'] = float(report_dict['filtered_subreads_base_cnt']) / float(report_dict['raw_reads_base_cnt'])


        # BBMAP/mapPacBio can do coverage also but it takes 30 minutes
        my_dict['coverage'] = float(report_dict['filtered_subreads_base_cnt']) / float(report_dict['ref_fasta_scaf_bp'])

        #mapPacBio.sh in=fasta/subreads.subreads.fasta  ref=ref/2754412715.clean.fasta \
        #path=/global/projectb/scratch/brycef/meth/BZGSH/fasta \
        #covstats=/global/projectb/scratch/brycef/meth/BZGSH/fasta/full_coverage_stats.txt \
        #ow=t nodisk ambig=random maxindel=100 fastareadlen=500 build=1

        # write everything from my_dict to stats file
        for my_key in my_dict:
            append_rqc_stats(rqc_stats_log, my_key, my_dict[my_key])

    if err_cnt > 0:
        status = "%s failed" % step_name
    else:
        status = "%s complete" % step_name

    checkpoint_step(status_log, status)
    return status


'''
Create pdf, txt and metadata.json reports

'''
def do_pdf_report(library_name):
    log.info("do_pdf_report: %s", library_name)

    step_name = "pdf_report"
    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(REPORT_PATH, output_path)
    motif_path = os.path.join(output_path, METHYLATION_PATH)
    task_path = os.path.join(METHYLATION_PATH, "tasks") # relative path for metadata.json file
    subread_path = os.path.join(SUBREAD_PATH, "tasks")

    # names of the reports
    pdf_report = os.path.join(the_path, "methylation-%s.pdf" % (library_name))
    latex_report = pdf_report.replace(".pdf", ".tex")
    txt_report = pdf_report.replace(".pdf", ".txt")
    # jat metadata.json in output_path because files are relative to output_path
    jat_metadata = os.path.join(output_path, "metadata.json")


    if 1==1:


        merge_dict = get_library_info(library_name, log)

        merge_dict['jgi_logo'] = os.path.realpath(os.path.join(my_path, "../sag/templates", "jgi_logo.jpg"))
        merge_dict['library_name'] = library_name
        merge_dict['report_date'] = time.strftime("%x") # %x = date in local format

        tapid = config['analysis_project_type_id']
        tatid = config['analysis_task_type_id']

        #merge_dict['analysis_project_id'], merge_dict['analysis_task_id'] = get_analysis_project_id(merge_dict['seq_proj_id'], tapid, tatid, the_path, log)
        merge_dict['analysis_project_id'], merge_dict['analysis_task_id'] = get_analysis_project_id(merge_dict['seq_proj_id'], 0, tatid, the_path, log, "-sa")

        # copy config vals into merge_dict
        for x in ['smrtpipe_version_str', 'motif_version_str', 'aligner', 'aligner_version', 'aligner_options']:
            merge_dict[x] = config[x]



        # read in stats & files into merge dict
        for f in [rqc_stats_log, rqc_file_log]:

            #if not os.path.isfile(f):
            #    f = f.replace(".tmp", ".txt")

            log.info("- importing: %s", f)

            fh = open(f, "r")

            for line in fh:
                #print line
                if line.startswith("#"):
                    continue
                arr = line.split("=")

                if len(arr) > 1:
                    my_key = str(arr[0]).lower().strip()
                    my_val = str(arr[1]).strip()
                    if my_val.endswith(".png"):
                        my_key += ".png"
                        #my_val = my_val.replace(".png", "")
                    merge_dict[my_key] = my_val
                    #print my_key, my_val, merge_dict[my_key]
            fh.close()




        # input files for metadata.json
        input_h5_list = []
        input_fofn = os.path.join(output_path, "input.fofn")

        fh = open(input_fofn)
        for line in fh:
            if line.startswith("#"):
                continue
            if line:
                input_h5_list.append(line.strip())
        fh.close()
        merge_dict['h5_list'] = json.dumps(input_h5_list) # json.dumps to convert ['A', 'B'] to ["A", "B"]


        # motif table
        merge_dict['motif_table'] = ""
        fh = open(os.path.join(the_path, "motif_table.tex"), "r")
        for line in fh:
            merge_dict['motif_table'] += line
        fh.close()

        merge_dict['motif_summary'] = ""
        fh = open(os.path.join(the_path, "motif_summary.txt"), "r")
        for line in fh:
            merge_dict['motif_summary'] += line
        fh.close()

        # for jat template convert "55.2%" -> 55.2
        for k in ['filtered_subreads_read_pct', 'raw_reads_mapped_pct', 'filtered_subreads_mapped_pct', 'filtered_subreads_bases_mapped_pct']:
            if k in merge_dict:
                merge_dict[k] = format_val_pct(merge_dict[k])
                my_new_key = k + "_json"
                merge_dict[my_new_key] = float(str(merge_dict[k]).replace("%",""))

        # read in templates
        my_template = "methylation.tex"
        my_txt_template = "methylation.txt"
        my_json_template = "methylation.json"


        latex_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_template))
        txt_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_txt_template))
        json_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_json_template))



        # metadata.json for JAT
        # written to output_path, files are relative to output_path
        merge_dict['pdf_report'] = "report/%s" % os.path.basename(pdf_report)
        merge_dict['txt_report'] = "report/%s" % os.path.basename(txt_report)

        # JAT files
        # using relative paths (task_path) vs full path


        merge_dict['motif_summary.csv'] = os.path.join(task_path, "motif_maker.tasks.find_motifs-0", "motifs.csv") # motif_maker.tasks.find_motifs-0/motifs.csv
        merge_dict['motifs.gff.gz'] = os.path.join(task_path, "motif_maker.tasks.reprocess-0", "motifs.gff.gz") # motif_maker.tasks.reprocess-0/motifs.gff
        merge_dict['modifications.gff.gz'] = os.path.join(task_path, "kinetics_tools.tasks.ipd_summary-0", "basemods.gff.gz") # tasks/kinetics_tools.tasks.ipd_summary-0/basemods.gff
        merge_dict['aligned_reads.bam'] = os.path.join(task_path, "pbalign.tasks.pbalign-0", "mapped.alignmentset.bam") # pbalign.tasks.pbalign-0/mapped.alignmentset.bam



        merge_dict['filtered_subreads.bam'] = os.path.join(subread_path, "pbcoretools.tasks.h5_subreads_to_subread-0", "subreads.subreads.bam")
        if not os.path.isfile(merge_dict['filtered_subreads.bam']):
            # create a tarball
            cmd = "cd %s;tar -cvf filtered_subreads.tar *.subreads.bam" % (os.path.join(output_path, subread_path, "pbcoretools.tasks.h5_subreads_to_subread-0"))
            std_out, std_err, exit_code = run_cmd(cmd, log)
            merge_dict['filtered_subreads.bam'] = os.path.join(subread_path, "pbcoretools.tasks.h5_subreads_to_subread-0", "filtered_subreads.tar")

        fq_list = merge_dict['fastq_list'].split(",")
        merge_dict['seq_unit_list'] = json.dumps(fq_list)

        merge_dict['motif_settings'] = os.path.join(CONFIG_PATH, "motif_settings.xml")

        ref_file = get_file("ref_fasta", file_dict, output_path, log)
        merge_dict['reference_fasta'] = os.path.join(REFERENCE_PATH, os.path.basename(ref_file))
        #merge_dict['input_reads_H5'] = "raw.h5.reads.tar.gz" # nope

        jat_metadata = os.path.join(output_path, "%s.methylation.metadata.json" % merge_dict['analysis_task_id'])
        merge_template(json_template_file, jat_metadata, merge_dict, log)


        # symlink to old file (jat_metadata) - remove these lines 2018?
        jat_metadata_new = "methylation.metadata.json"
        if os.path.isfile(os.path.join(output_path, jat_metadata_new)):
            cmd = "rm %s" % os.path.join(output_path, jat_metadata_new)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        cmd = "cd %s;ln -s %s %s" % (output_path, os.path.basename(jat_metadata), jat_metadata_new)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        # format numbers
        # formatting integers to have ,'s 1000000 -> 1,000,000
        format_list_int = ["ref_fasta_scaf_bp", "raw_reads_read_cnt", "filtered_subreads_read_cnt", "filtered_subreads_base_cnt", "raw_reads_mapped", "raw_bases_mapped",
                           "filtered_subreads_mapped", "filtered_subreads_bases_mapped"]

        for k in format_list_int:
            if k in merge_dict:
                merge_dict[k] = "{:,}".format(int(merge_dict[k]))


        # float values
        for k in ['coverage']:
            if k in merge_dict:
                merge_dict['coverage'] = "{:,.2f}".format(float(merge_dict[k]))

        # txt report
        merge_template(txt_template_file, txt_report, merge_dict, log)


        # PDF/latex
        err_cnt = merge_template(latex_template_file, latex_report, merge_dict, log)
        if err_cnt > 0:
            log.error("- fatal error: missing keys for pdf report!  %s", msg_fail)
            sys.exit(8)

        # convert .tex to .pdf
        # - okay if non-zero exit code
        pdflatex_log = os.path.join(the_path, "pdflatex.log")

        cmd = "#texlive;pdflatex -interaction=nonstopmode -output-directory=%s %s > %s 2>&1" % (the_path, os.path.basename(latex_report), pdflatex_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(pdf_report) and os.path.isfile(txt_report):
        append_rqc_file(rqc_file_log, "methylation_report_pdf", pdf_report)
        append_rqc_file(rqc_file_log, "methylation_report_txt", txt_report)

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name
        log.error("- failed to create %s  %s", pdf_report, msg_fail)

    checkpoint_step(status_log, status)

    return status


'''
Get the next step to run
'''
def get_next_status(status):
    log.info("get_next_step: %s", status)

    next_status = None

    status_dict = {
        "start" : "setup_pbsmrtpipe",
        "setup_pbsmrtpipe" : "setup_motif_xml",
        "setup_motif_xml" : "get_reference",
        "get_reference" : "upload_reference",
        "upload_reference" : "methylation",
        "methylation" : "fasta_stats",
        "fasta_stats" : "calc_report",
        "calc_report" : "pdf_report",
        "pdf_report" : "complete",

        "failed" : "failed",
        "complete" : "complete",
    }

    if status:

        if status.lower() != "complete":
            status = status.lower().replace("skipped", "").replace("complete", "").strip()

        # gc_cov:screened -> gc_cov
        if ":" in status:
            status = status.split(":")[0]

        if status in status_dict:
            next_status = status_dict[status]

        else:
            if status.endswith("failed"):
                next_status = "failed"

            else:
                next_status = "failed: next step missing for '%s'" % (status)
                log.error("Cannot get the next step for '%s'  %s", status, msg_fail)
                sys.exit(12)



    log.info("- next step: %s%s%s -> %s%s%s", color['pink'], status, color[''], color['cyan'], next_status, color[''])

    if stop_flag:
        next_status = "stop"

    # debugging
    if next_status == "stop":
        log.info("- stopping pipeline, stop flag is set")
        sys.exit(13)


    return next_status


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "Heisenberg (Methylation)"
    version = "5.0.4"

    CONFIG_PATH = "config"
    REFERENCE_PATH = "ref"
    REF_UPLOAD_PATH = "ref_upload"
    SUBREAD_PATH = "subreads"
    FASTA_PATH = "fasta"
    METHYLATION_PATH = "motif"
    TMP_PATH = "tmp"
    REPORT_PATH = "report"


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)
    uname = getpass.getuser()


    DEFAULT_CONFIG_FILE = os.path.realpath(os.path.join(my_path, "../sag/", "config", "methylation.cfg"))
    MOTIF_SETTINGS_FILE = os.path.realpath(os.path.join(my_path, "../sag/", "config", "motif_settings.xml"))

    # Parse options
    usage = "prog [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)



    parser = ArgumentParser(usage = usage)

    parser.add_argument("-o", "-od", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-r", "--ref", dest="ref_file", help = "Reference fasta")
    parser.add_argument("-c", "--config", dest="config_file", help = "Location of methylation.cfg file to use")
    parser.add_argument("-l", "--library", dest="library_name", help = "Library Name")
    parser.add_argument("-t", "--taxon-id", dest="taxon_id", type=int, help = "IMG Taxonomy Id to automatically get reference fasta")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)

    status = "start" # step + X = run step and exit?  (e.g. gc_covX)  -- to do
    reference_fasta = None
    library_name = "unknown"
    taxon_id = None
    output_path = None
    stop_flag = False # stop after running a step, e.g. gc_cov_stop
    print_log = False


    # parse args
    args = parser.parse_args()
    print_log = args.print_log

    if args.library_name:
        library_name = args.library_name

    if args.taxon_id:
        taxon_id = args.taxon_id

    if args.ref_file:
        reference_fasta = args.ref_file


    if args.output_path:

        output_path = args.output_path

        # test case params: t1, ...
        if output_path.startswith("t"):
            test_path = "/global/projectb/scratch/brycef/meth"

            if output_path in ("t1", "t11"):
                # t11 = rsync from BSOTB existing run folder
                #BSOTB; run time 5:53:00
                library_name = "BSOTB"
                taxon_id = 2734482165

            elif output_path == "t2":
                library_name = "BTOOX"
                taxon_id = 2738541338

            elif output_path == "t3":
                #library_name = "BZGSH" # 7:40
                #taxon_id = 2754412715

                library_name = "BZGSG" # old = 8:11:25
                taxon_id = 2754412639


            output_path = os.path.join(test_path, library_name)


        output_path = os.path.realpath(output_path)


    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    config_file = os.path.join(output_path, "methylation.cfg")

    if args.config_file:
        config_file = args.config_file



    # initialize my logger
    log_file = os.path.join(output_path, "heisenberg.log")

    log_level = "INFO"
    log = get_logger("meth", log_file, log_level, print_log)


    log.info("%s", 80 * "~")
    log.info("Starting %s v%s", script_name, version)


    if not reference_fasta and not taxon_id:
        log.error("MISSING REFERENCE FASTA!  %s", msg_fail)
        sys.exit(2)

    if reference_fasta and not os.path.isfile(reference_fasta):
        log.error("Reference file %s is missing!  %s", reference_fasta, msg_fail)
        sys.exit(2)

    log.info("")

    log.info("Run settings:")

    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "library_name", library_name)
    if taxon_id > 0:
        log.info("%25s      %s", "taxon_id", taxon_id)
    if reference_fasta:
        log.info("%25s      %s", "reference_fasta", reference_fasta)



    log.info("")

    if not os.path.isfile(config_file):
        log.info("- cannot find config file: %s  %s", config_file, msg_warn)
        log.info("- using default config file.")
        cmd = "cp %s %s" % (DEFAULT_CONFIG_FILE, config_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    # Hacksaw Status log
    status_log = os.path.join(output_path, "heisenberg_status.log")
    rqc_file_log = os.path.join(output_path, "heisenberg-files.tmp")
    rqc_stats_log = os.path.join(output_path, "heisenberg-stats.tmp")



    # set config dict with values from iso.cfg
    config = load_config_file(config_file, log)

    file_dict = {} # dictionary of file type and locations


    # test  area

    if uname == "brycef":
        pass
        #status = calc_pb_report()
        #do_fasta_stats()
    #print get_analysis_project_id(1134414, 0, 32, output_path, log, "-sa") # (1134373, 170054)
    #sys.exit(48)

    checkpoint_step(status_log, "## New Run")


    done = False
    cycle_cnt = 0
    while not done:

        cycle_cnt += 1

        if status in ("start", "setup_input_xml"):
            status = setup_pbsmrtpipe(library_name)

        if status == "setup_motif_xml":
            status = setup_motif_xml()


        if status == "get_reference":
            status = get_reference(taxon_id, reference_fasta)

        if status == "upload_reference":
            status = upload_pbreference(library_name)


        if status in ("motif_detection", "methylation"): # methylation detection
            status = do_pb_modification_n_motif()


        if status == "calc_report":
            status = calc_pb_report()

        if status == "fasta_stats":
            status = do_fasta_stats()

        if status == "pdf_report":
            status = do_pdf_report(library_name)

        if cycle_cnt > 15:
            log.info("- max cycles reached.  %s", msg_warn)
            done = True

        if status in ("complete", "failed", ""):
            done = True

        status = get_next_status(status)


    checkpoint_step(status_log, status)
    if status == "complete":

        # move rqc-files.tmp to rqc-files.txt
        append_rqc_stats(rqc_stats_log, "heisenberg_version", "%s %s" % (my_name, version))
        rqc_new_file_log = os.path.join(output_path, "heisenberg-files.txt")

        cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        rqc_new_stats_log = os.path.join(output_path, "heisenberg-stats.txt")

        cmd = "mv %s %s" % (rqc_stats_log, rqc_new_stats_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    log.info("Completed %s", script_name)

    sys.exit(0)



'''


   .-------.
   |       |     If that's true --
 -=_________=-   if you don't know who I am
    __   __
   |__)=(__|     -- then maybe your best course is to tread lightly.

      ###
     # = #
     #####
      ###

-----
http://pacbio.jgi-psf.org/smrtportal/#/View-Data/Details-of-Job/40405
40362
/global/seqfs/sdm/prod/pacbio/jobs/040/040362
'''
