#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Qual
- for ISO, SAG, PBMID pipelines
- Quality reporting to indicate quality of assembly
- RQC-1090
- uses Kingdom from checkm results to run tRNAscan and barrnap models, could use rqc.library_info.tax_kingdom




Dependencies
checkm
barnap
tRNASCAN-se

v 1.0: 2018-03-27
- initial version (code reviewed)

v 1.1: 2018-04-05
- switched to docker for barrnap, conda perl messes up barrnap
Error: ListUtil.c: loadable library and perl binaries are mismatched (got handshake key 0xdb80080, needed 0xdb00080)
- if no kingdom, default to bacteria (CNGST)

tests = high quality
~/git/jgi-rqc-pipeline/sag/qual.py -o t1

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import getpass

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, run_cmd, get_colors, get_msg_settings
from micro_lib import get_the_path, get_nproc


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
barrnap:
Bacterial/Archaea ribosomal RNA predictor
https://github.com/tseemann/barrnap

very quick
'''
def do_barrnap(kingdom):
    log.info("do_barrnap: %s", kingdom)

    the_path = get_the_path(BARRNAP_PATH, output_path)

    gff_output = os.path.join(the_path, BARRNAP_FILE)

    barrnap_kingdom = "bac"
    if kingdom.startswith("arch"):
        barrnap_kingdom = "arc"

    if os.path.isfile(gff_output):
        log.info("- skipping barrnap, found output file  %s", msg_warn)

    else:

        cmd = "#trnascan-se;barrnap --threads %s --kingdom %s %s > %s 2>&1" % (nproc_cnt, barrnap_kingdom, fasta, gff_output)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        # --kingdom bac,arc,euk,mito (default bac)
        # --lencutoff 0.8 = default = 80% of gene mapped
        # --reject 0.5 = default = reject if 50% or less of gene mapped
        if exit_code > 0:
            log.error("- barrnap failed!  %s", msg_fail)
            sys.exit(2)
            

'''
tRNAscan-SE (2.0)
http://trna.ucsc.edu/software/trnascan-se-2.0.0.tar.gz
- scan for transfer RNAs
- tRNA count = number of lines in the table after -----, not looking at COVE score
~ 30 seconds to 3 minutes
'''
def do_trnascan(kingdom):
    log.info("do_trnascan: %s", kingdom)

    the_path = get_the_path(TRNA_PATH, output_path)

    trna_kingdom = "-B" # Bacteria
    if kingdom.startswith("arch"):
        trna_kingdom = "-A" # archaea

    trna_file = os.path.join(the_path, TRNA_FILE)

    if os.path.isfile(trna_file):
        log.info("- skipping tRNAscan, found output file  %s", msg_warn)
    else:
        # run both B, A options and pull best result (closest to 20)
        trnascan_log = os.path.join(the_path, "trnascan-se.log")


        cmd = "#trnascan-se;tRNAscan-SE %s -o %s %s > %s 2>&1" % (trna_kingdom, trna_file, fasta, trnascan_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        # -B = bacteria
        # -A = archea
        if exit_code > 0:
            log.error("- tRNAscan-SE failed!  %s", msg_fail)
            sys.exit(2)


'''
Get quality (completeness, contam, lineage) from checkm
https://github.com/Ecogenomics/CheckM/wiki/Workflows#taxonomic-specific-workflow).
~ 10 minutes
- return kingdom for barrnap and trnascan to use (might not be 100% correct ...)
'''
def do_checkm_completeness():
    log.info("checkm_completeness")

    kingdom = "-"

    the_path = get_the_path(QUAL_PATH, output_path)
    checkm_output_path = get_the_path(CHECKM_OUTPUT, the_path)


    tree_qa_file = os.path.join(the_path, TREE_QA_FILE)
    qa_file = os.path.join(the_path, QA_FILE)

    if os.path.isfile(tree_qa_file) and os.path.isfile(qa_file):
        log.info("- skipping checkm_completeness, found output files  %s", msg_warn)

    else:

        # copy/symlink fasta here
        new_fasta = os.path.join(the_path, "assembly.fa")
        # -x fa = assembly fasta file is extention .fa
        cmd = "ln -s %s %s" % (fasta, new_fasta)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # I. Place bins in the genome tree.
        # 5 minutes
        tree_log = os.path.join(the_path, "checkm_tree.log")
        cmd = "#checkm;checkm tree %s %s -x fa -t %s > %s 2>&1" % (the_path, checkm_output_path, nproc_cnt, tree_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # II. Assess phylogenetic markers found in each bin.
        # create tree_qa.txt, shows taxonomy (sister lineage)
        # quick
        tree_qa_log = os.path.join(the_path, "checkm_tree_qa.log")
        cmd = "#checkm;checkm tree_qa %s -o 2 --tab_table -f %s > %s 2>&1" % (checkm_output_path, tree_qa_file, tree_qa_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        # -o 2 : 2. detailed summary of genome tree placement including lineage-specific statistics
        # --tab_table: print tab-separated values table
        # -f = print results to a file

        # III. Infer lineage-specific marker sets for each bin.
        # quick
        lineage_log = os.path.join(the_path, "checkm_lineage_set.log")
        marker_file = os.path.join(the_path, "markers.lmf")
        cmd = "#checkm;checkm lineage_set %s %s > %s 2>&1" % (checkm_output_path, marker_file, lineage_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # IIII. Identify marker genes in bins and calculate genome statistics.
        # 2 minutes
        analyze_log = os.path.join(the_path, "checkm_analyze.log")
        cmd = "#checkm;checkm analyze %s %s %s -x fa -t %s > %s 2>&1" % (marker_file, the_path, checkm_output_path, nproc_cnt, analyze_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # IIIII. Assess bins for contamination and completeness.
        # quick, has completeness and contamination
        qa_log = os.path.join(the_path, "checkm_qa.log")
        cmd = "#checkm;checkm qa %s %s -o 2 --tab_table -f %s -t %s > %s 2>&1" % (marker_file, checkm_output_path, qa_file, nproc_cnt, qa_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        # -o 2 = 2. extended summary of bin statistics (includes GC, genome size, ...)


    if not os.path.isfile(tree_qa_file) and os.path.isfile(qa_file):
        log.info("- failed to create checkm output files!  %s", msg_fail)
        sys.exit(2)
    else:

        # lineage
        taxonomy = ""
        fh = open(tree_qa_file, "r")
        for line in fh:
            if line.startswith("Bin"):
                continue
            arr = line.split("\t")
            taxonomy = str(arr[4]).lower()

        fh.close()
        if taxonomy.startswith("k__ba"):
            kingdom = "bacteria"
        elif taxonomy.startswith("k__arc"):
            kingdom = "archaea"
        elif taxonomy.startswith("k__euk"):
            kingdom = "eukaryote"

        log.info("- checkm kingdom: %s", kingdom)
        if kingdom == "-":
            kingdom = "bacteria" # bad assumption?
            
    return kingdom


'''
Calculate quality from results and put into a quality.txt file

'''
def calc_quality():
    log.info("calc_quality")


    qual_file = os.path.join(output_path, "quality.txt")

    qual_path = get_the_path(QUAL_PATH, output_path)

    tree_qa_file = os.path.join(qual_path, TREE_QA_FILE)
    qa_file = os.path.join(qual_path, QA_FILE)

    barrnap_path = get_the_path(BARRNAP_PATH, output_path)
    gff_file = os.path.join(barrnap_path, BARRNAP_FILE)

    trna_path = get_the_path(TRNA_PATH, output_path)
    trna_file = os.path.join(trna_path, TRNA_FILE)

    qual_data = { "5S" : "No", "16S" : "No", "23S" : "No",
                 "completeness" : 0.0, "contamination" : 0.0, "trna_cnt" : 0 }

    # lineage
    fh = open(tree_qa_file, "r")
    for line in fh:
        if line.startswith("Bin"):
            continue

        arr = line.split("\t")

        qual_data['taxonomy'] = "%s;%s" % (arr[4], arr[5]) # taxonomy + sister lineage
    fh.close()

    # trna count
    # count unique only in "tRNA Type" field
    cnt_flag = False # start counting lines after finding "-----"
    trna_cnt = 0
    trna_list = [] # list of "tRNA Type" field
    fh = open(trna_file, "r")
    for line in fh:
        if cnt_flag:
            trna_cnt += 1
            arr = line.split()
            trna_list.append(arr[4])

        if line.startswith("----"):
            cnt_flag = True

    fh.close()
    trna_cnt = len(set(trna_list)) # creates unique list of tRNAs
    #log.info("- unique tRNA count: %s", trna_cnt)

    qual_data['trna_cnt'] = trna_cnt


    # completeness, contamination
    fh = open(qa_file, "r")
    for line in fh:
        if line.startswith("Bin"):
            continue
        arr = line.split("\t")
        #print arr
        qual_data['completeness'] = float(arr[5])
        qual_data['contamination'] = float(arr[6])
    fh.close()


    fh = open(gff_file, "r")
    
    # high: 80% length and above (check options), exclude partial (80% length is default for barrnap)
    # 5S = 120 bases
    # 16S = ~1500 bases, binds with 23S
    # 23S = 2904 bases
    gff_flag = False
    for line in fh:
        if line.startswith("##gff-v"):
            gff_flag = True

        # don't count partial hits
        if "partial" in line:
            continue

        if gff_flag:
            if "5S ribo" in line:
                qual_data['5S'] = "Yes"
            elif "16S ribo" in line:
                qual_data['16S'] = "Yes"
            elif "23S ribo" in line:
                qual_data['23S'] = "Yes"

    fh.close()

    # format taxonomy
    if 'taxonomy' in qual_data:
        tax_dict = { "kingdom" : "-", "phylum" : "-", "class" : "-", "order" : "-", "family" : "-", "genus" : "-", "species" : "-" }

        #k__Bacteria;p__Bacteroidetes;c__Sphingobacteriia;o__Sphingobacteriales;f__Chitinophaga;g__Chitinophaga;s__Chitinophaga_pinensis
        for t in qual_data['taxonomy'].split(";"):
            if t.startswith("k__"):
                tax_dict['kingdom'] = t.replace("k__", "")
            if t.startswith("p__"):
                tax_dict['phylum'] = t.replace("p__", "")
            if t.startswith("c__"):
                tax_dict['class'] = t.replace("c__", "")
            if t.startswith("o__"):
                tax_dict['order'] = t.replace("o__", "")
            if t.startswith("f__"):
                tax_dict['family'] = t.replace("f__", "")
            if t.startswith("g__"):
                tax_dict['genus'] = t.replace("g__", "")
            if t.startswith("s__"):
                tax_dict['species'] = t.replace("s__", "")

        for t in tax_dict:
            qual_data[t] = tax_dict[t]


    log.info("- completeness: %s%%", qual_data['completeness'])
    log.info("- contamination: %s%%", qual_data['contamination'])
    log.info("- 5S: %s, 16S: %s, 23S: %s", qual_data['5S'], qual_data['16S'], qual_data['23S'])
    log.info("- tRNA Count: %s", qual_data['trna_cnt'])

    # detemine quality
    # change trna cnt to unqiue_trna_cnt
    qual_data['quality'] = "-"
    if qual_data['completeness'] > 90.0 and qual_data['contamination'] < 5.0 \
    and qual_data['5S'] == "Yes" and qual_data['16S'] == "Yes" and qual_data['23S'] == "Yes" \
    and qual_data['trna_cnt'] >= 18:
        qual_data['quality'] = "High Quality"

    elif qual_data['completeness'] > 50.0 and qual_data['contamination'] < 10.0:
        qual_data['quality'] = "Medium Quality"
    else:
        qual_data['quality'] = "Low Quality"


    if qual_data['quality'] == "-":
        qual_data['quality'] = "No Quality"

    log.info("- quality: %s", qual_data['quality'])

    # write report
    qual_report = os.path.join(output_path, "quality.txt")
    fh = open(qual_report, "w")
    fh.write("#Key|Value\n")
    for i in qual_data:
        fh.write("%s|%s\n" % (i, qual_data[i]))
    fh.close()

    log.info("- wrote quality report: %s", qual_report)


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "Microbe Qual"

    # added to tRNASCAN-se docker container but get error "Could not find 'nhmmer' executable in PATH" even though its totally in the path
    BARRNAP_CMD = "/global/dna/projectdirs/PI/rqc/prod/tools/barrnap/bin/barrnap"

    BARRNAP_PATH = "barrnap"
    BARRNAP_FILE = "barrnap.gff"

    TRNA_PATH = "trna"
    TRNA_FILE = "trna.txt"

    QUAL_PATH = "checkm"
    CHECKM_OUTPUT = "checkm_out"
    TREE_QA_FILE = "tree_qa.txt"
    QA_FILE = "qa.txt"



    # checkm v, barnap v - to do
    checkm_v = "1.0.7" # ...::: CheckM v1.0.7 :::... - checkm, at top
    barrnap_v = "0.9-dev" # ./barrnap --version
    trna_scan_v = "2.0"
    version = "1.1 (checkm: %s, barrnap: %s, tRNAscan-SE: %s)" % (checkm_v, barrnap_v, trna_scan_v)



    uname = getpass.getuser() # get the user, brycef can't use scell.p, but qc_user can!


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)

    # Parse options
    usage = "qual.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
Generates quality info for microbial assemblies using barrnap, tRNAscan-SE and checkm
    """

    parser = ArgumentParser(usage = usage)

    parser.add_argument("-f", "--fasta", dest="fasta", type = str, help = "Input fasta")
    parser.add_argument("-o", "--output-path", dest="output_path", type = str, help = "Output path to write to, uses pwd if not set")

    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)

    output_path = None
    fasta = None

    # parse args
    args = parser.parse_args()
    print_log = args.print_log

    if args.fasta:
        fasta = args.fasta

    if args.output_path:
        output_path = args.output_path


    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/qual"

        if output_path == "t1":

            fasta = "/global/projectb/scratch/brycef/iso/BSHTN.fasta"


        if output_path == "t2":
            fasta = "/global/projectb/scratch/brycef/iso/BWHNN.fasta"

        if output_path == "t3":
            fasta = ""

        if output_path == "t4":
            fasta = "/global/dna/shared/rqc/pipelines/pbmid/archive/00/00/42/18/arrow/repolished_assembly.dedup.fasta"

        output_path = os.path.join(test_path, output_path)



    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    # initialize my logger
    log_file = os.path.join(output_path, "qual.log")



    # log = logging object
    log_level = "INFO"
    log = None

    log = get_logger("qual", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "fasta", fasta)

    log.info("")

    if not os.path.isfile(fasta):
        log.error("Error: cannot find fasta: %s  %s", fasta, msg_fail)
        sys.exit(2)


    nproc_cnt = get_nproc()

    # run checkm for completeness, get kingdom from checkm's model
    kingdom = do_checkm_completeness()

    if kingdom == "-":
        log.error("Error: no kingdom available!  %s", msg_fail)
        sys.exit(19)

    # run barrnap
    do_barrnap(kingdom)

    # run tRNAScan-SE
    do_trnascan(kingdom)

    # summarize results into a report
    calc_quality()


    log.info("Completed %s", script_name)

    sys.exit(0)

