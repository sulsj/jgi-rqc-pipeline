#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
ISO Pipeline
- very similar to CE, SPS pipeline and SAG pipeline

Dependencies
bbtools
spades
blast+
texlive (pdf maker)
prodigal, lastal (gc_histogram)
checkm
java, R = tetramer

v 1.0: 2016-07-13
- initial version

v 1.0.1: 2016-08-25
- fix for blast_16s issue
- run megan on subsampled reads

v 1.0.2: 2016-09-06
- fix for megan_reads function

v 1.0.3: 2016-10-07
- switched to spades 3.9.0

v 1.0.4: 2016-10-18
- combine multiple fastqs into 1 (RQC-873) - but use same library
- updated .tex report with SOP 1061 info

v 1.1: 2016-11-14
- replace SCG (estimateGenomeCompleteness.pl) with checkm tool (RQC-865), code for SCG still here but not called
- updates to PDF report per Tanja W. and checkM

v 1.1.1: 2016-12-14
- checkM LWF doesn't always work, updated text reports to use simple % instead of normalized % for checkm

v 1.1.2: 2017-01-12
- coverage calculation fix from do_gc_cov, include in PDF report
- renamed contigs based on library name
- do_checkm: save gene_contigs.txt (new file)

v 1.1.3: 2017-02-27
- jigsaw 2.6.4 -> 2.6.5
- added is_right_thing for auto-qc
- ignore .parsed files for gzip_folder

v 1.1.4: 2017-03-09
- jigsaw 2.6.5 -> 2.6.4 (no runs affected)

v 1.1.5: 2017-03-23
- run_last use -P0 (all processors), correct nr database and last/828 (do_megan)
--- No: configured to use old Last w/ params and old nr database, next version will update (JIGSAW-65)
- exit if missing keys for pdf report
- exit correctly if failed (get_next_status)
- if no checkm results create empty files & images (RQCSUPPORT-1112)

v 1.1.6: 2017-03-24
- use new nr database and Jigsaw 2.6.5

v 1.1.7: 2017-03-29
- fix for determing if 16s hits the assembly (RQCSUPPORT-1095)

v 1.1.8: 2017-04-05
- gc cov contam check (RQC-949)

v 1.1.9: 2017-04-20
- blast 2.2.31 -> blast 2.6.0
- changed tetramer code to use actual java and R script

v 1.2: 2017-06-06
- modified to work on denovo/cori systems
- spades 3.9 with --tmpdir pointing to burst buffer option, 3.10.1 available
- changed gc_cov, contig_gc_plot, megan (gc_hist), tetramer, pct_reads_asm, ncbi_prescreen to use code in jgi-rqc-pipeline/sag repo or inline
- changed megan step name to gc_hist
- do_contig_trim: only keep contigs > 1000bp, do not use mapping coverage for keeping contigs
- tetramer fix for short *.kmer files
- changed sagtaminants db for contam id to: /global/projectb/sandbox/gaag/bbtools/commonMicrobes/fusedERPBBmasked.fa.gz

v 1.2.1: 2017-06-19
- switched to Spades 3.10.1
- symlink iso.metadata.json to [at_id].iso.metadata.json (RQCSUPPORT-1418)
- removed misc, split_input sub folders from asm folder (not needed: Alicia)

v 1.2.2: 2017-07-20
- save contig.gc and contig-weighted.gc to file log (RQCSUPPORT-1564)
- updated make_asm_stats, get_bbtools_version not to use config variable

v 1.2.3: 2017-07-26
- added option to print log to screen
- changed msg_ok, msg_warn, msg_fail to function in common.py
- changed to use run_blastplus_taxserver.py

v 1.2.4: 2017-10-18
- checkm update
- added auspice statement to .txt and .pdf report
- added antifam step

v 1.2.5: 2017-10-27
- added check for prodigal for antifam
- updated do_blast to skip if blast already run

v 1.2.6: 2017-11-01
- removed green genes blasting, fixed green genes table in pdf report to only use Silva SSU hits (RQC-1044)

v 1.2.7: 2017-11-28
- code cleanup in gc_cov (unused code)

v 1.2.8: 2018-02-08
- new tetramer kmer code from bbtools
- use spades 3.11.0 (GAA-3383)

v 1.2.9: 2018-03-22
- RQC-1092 - gc_cov, gc_hist, contig_gc_plot use fasta name and library name

v 1.3: 2018-03-30
- RQC-1090 - quality assessment added to pdf report
- contam db changed to: microbial_watchlist.201803.fa
- use gold_organism_name if available, otherwise use ncbi_organism_name

v 1.3.1: 2018-04-25
- copy in contig_hist.png, genome_vs_size.png from do_contig_gc_plot
- disabled do_gc_hist_reads step

v 1.3.2: 2018-05-17
- use gold_tax_id and gold_organism_name in is_right_thing, if it doesn't exist then use ncbi_tax_id and ncbi_organism_name

v 1.3.3: 2018-07-05
- add quality reporting to PDF & JAT template, do not include Quality assessment (RQCSUPPORT-2714)

v 1.3.4: 2018-07-17
- presplit reads for spades
- use $SLURM_TMP for spades
- stats.sh extended=t

To do:
- tests to check the output (Behave/girkin?)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~[ Test Cases ]
t1, t2 have the fastq on sandbox (no purge)

T1 - AZNWT
$ qsub -b yes -j yes -m as -now no -w e -N iso-AZNWT -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/iso/iso-AZNWT.log -js 501 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/iso.py -o t1"

T2 - AZNWU
$ qsub -b yes -j yes -m as -now no -w e -N iso-AZNWU -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/iso/iso-AZNWU.log -js 502 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/iso.py -o t2"

T3 - 	CBWBP - 5 hrs - 2017-11-01
$ qsub -b yes -j yes -m as -now no -w e -N iso-CBWBP -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/iso/iso-CBWBP.log -js 503 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/iso.py -o t3"

T4 - AZNGH - 2 hrs, 1.5 hrs 2017-05-10
$ qsub -b yes -j yes -m as -now no -w e -N iso-AZNGH -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/iso/iso-AZNGH.log -js 504 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/iso.py -o t4"

T5 - BBOSU - 5 hrs (old)
T5 - 11456.8.207797.CCGTCC.fastq.gz - BTHZO, has ncbi contam: 1.51 hrs (old with Jigsaw),
$ qsub -b yes -j yes -m as -now no -w e -N iso-BTHZO -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/iso/iso-BTHZO.log -js 504 "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/iso.py -o t5"
$ sbatch -t 180 --mincpus=16 --job-name=iso-BTHZO --mem=110G -o /global/projectb/scratch/brycef/iso/iso-BTHZO.log "/global/homes/b/brycef/git/jgi-rqc-pipeline/sag/iso-t5cori.sh"
# denovo
$ sbatch /global/homes/b/brycef/git/jgi-rqc-pipeline/sag/iso-t5.sh


T3 AXNHZ - 2 hour run time in Jigsaw
T4 AXNGH - 2 hour run time also

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import glob
import time
import re
import getpass
import collections


# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, checkpoint_step, append_rqc_file, append_rqc_stats, get_colors, get_analysis_project_id, run_cmd, get_msg_settings

from rqc_utility import get_blast_hit_count

from micro_lib import load_config_file, setup_run, get_the_path, check_skip, make_asm_stats, save_asm_stats, get_library_info, save_file, get_file, get_bbtools_version, get_spades_version, get_ssu, green_genes_pdf, ncbi_prescreen_report, contam_summary_pdf, merge_template, get_seq_unit_info, format_val_pct, create_empty_checkm_summary, get_base_contig_cnt

# is_right_organism
import orgutils

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Combine 2 or more fastqs into 1 file for processing
- assumes inputs are raw fastq or gzipped fastqs
'''
def combine_fastq(my_fastq_list):
    log.info("combine_fastq: %s", my_fastq_list)
    fastq = None


    step_name = "combine_fastq"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    if len(my_fastq_list) > 1:

        concat_cmd = "cat "

        for f in my_fastq_list:
            new_f = f

            # if not gzipped, create a gzip file
            if not f.endswith(".gz"):

                new_f = os.path.join(output_path, os.path.basename(f) + ".gz")

                cmd = "#pigz;pigz -c %s > %s" % (f, new_f)
                std_out, std_err, exit_code = run_cmd(cmd, log)

            concat_cmd += " %s" % new_f


        fastq = os.path.join(output_path, "iso-combo.fastq.gz")
        if os.path.isfile(fastq):
            log.info("- skipping creating %s, file exists", fastq)

        else:
            concat_cmd += " > %s" % (fastq)
            std_out, std_err, exit_code = run_cmd(concat_cmd, log)


    else:
        log.info("- 1 fastq, nothing to combine")
        fastq = my_fastq_list[0]

    save_file(fastq, "input_fastq", file_dict, output_path)

    status = "%s complete" % step_name
    checkpoint_step(status_log, status)

    return status


'''
Contamination Identification
- sagtaminants db - created by James and not updated (ribo removed)
- minkmerfraction=0.5 (mkf) - kmers must hit 50% of the read instead of 1 kmer hit in the read
'''
def do_contam_id():
    #'do_contam_id' - docstring + 0.01
    log.info("do_contam_id")


    fastq = get_file("input_fastq", file_dict, output_path, log)
    log.info("- input: %s", fastq)
    append_rqc_stats(rqc_stats_log, "input_fastq", fastq) # save for reporting ...

    step_name = "contam_id"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(CONTAM_ID_PATH, output_path)

    seal_stats = os.path.join(the_path, "seal.txt")


    if os.path.isfile(seal_stats):
        log.info("- skipping %s, found file: %s", step_name, seal_stats)

    elif check_skip(step_name, fastq, config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:

        seal_log = os.path.join(the_path, "seal.log")
        seal_fq = os.path.join(the_path, "seal.fastq") # is this a fastq?


        cmd = "#%s;seal.sh in=%s ref=%s stats=%s out=%s %s > %s 2>&1" % (config['bbtools_module'], fastq, config['contam_watchlist_db'], seal_stats, seal_fq, config['seal_params'], seal_log)
        # mkf = minkmerfraction = 50% of kmers must match read (instead of any kmer hit)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(seal_stats):

        append_rqc_file(rqc_file_log, "seal_contam_stats", seal_stats)

        contam_dict = {}
        fh = open(seal_stats, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            #Name   Reads   ReadsPct        Bases   BasesPct
            arr = line.split()
            #print arr
            if len(arr) > 1:
                contam = arr[1]
                read_cnt = int(arr[-4])
                base_cnt = int(arr[-2])

                if contam.startswith("gi"):
                    contam = arr[2]

                #print "%s: %s, %s" % (contam, read_cnt, base_cnt)
                if contam in contam_dict:
                    contam_dict[contam]['reads'] += read_cnt
                    contam_dict[contam]['bases'] += base_cnt
                else:
                    contam_dict[contam] = { "reads" : read_cnt, "bases" : base_cnt }

                #print "- %s: %s, %s" % (contam, contam_dict[contam]['reads'], contam_dict[contam]['bases'])

        fh.close()

        read_cnt = 0 # ttl contaminated reads
        base_cnt = 0 # ttl contaminated bases
        contam_limit = int(config['contam_limit_reads']) # must be > 100 reads
        contam_sum = os.path.join(the_path, "contam_summary.txt")
        fh = open(contam_sum, "w")
        fh.write("#contam name|read count|base count\n")
        for contam in contam_dict:
            #print "%s: %s, %s" % (contam, contam_dict[contam]['reads'], contam_dict[contam]['bases'])
            log.info("- %s: %s reads, %s bases", contam, contam_dict[contam]['reads'], contam_dict[contam]['bases'])

            if contam_dict[contam]['reads'] > contam_limit:
                fh.write("%s|%s|%s\n" % (contam, contam_dict[contam]['reads'], contam_dict[contam]['bases']))
                read_cnt += contam_dict[contam]['reads']
                base_cnt += contam_dict[contam]['bases']
        fh.close()

        append_rqc_file(rqc_file_log, os.path.basename(contam_sum), contam_sum)

        append_rqc_stats(rqc_stats_log, "contam_read_cnt", read_cnt)
        append_rqc_stats(rqc_stats_log, "contam_base_cnt", base_cnt)
        append_rqc_stats(rqc_stats_log, "seal_params", config['seal_params'])

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status




'''
Subsampling
- ISO = 10m

'''
def do_subsample():
    log.info("do_subsample")

    # find input fastq
    fastq = get_file("input_fastq", file_dict, output_path, log)

    # read count before, after - bb?

    step_name = "subsample"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(SUBSAMPLE_PATH, output_path)

    subsample_fastq = os.path.join(the_path, os.path.basename(fastq).replace(".fastq", ".subsample.fastq"))
    cov_hist = os.path.join(the_path, "cov.txt") # coverage histogram of subsampled fastq

    if os.path.isfile(subsample_fastq):
        log.info("- skipping %s, found file: %s", step_name, subsample_fastq)

    elif check_skip(step_name, fastq, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        subsample_log_file = os.path.join(the_path, "subsample.log")
        subsample_read_count = int(config['subsample']) # 10m for ISO
        subsample_read_count = int(subsample_read_count/2) # bbtools treats 10M as 10m PAIRED reads = 20m reads

        # reads= only process this many reads then exit
        #cmd = "reformat.sh reads=%s in=%s out=%s ow=t 2> %s" % (subsample_read_count, fastq, subsample_fastq, subsample_log_file)

        # samplereadstarget = exact number of output reads desired
        cmd = "#%s;reformat.sh samplereadstarget=%s in=%s out=%s ow=t 2> %s" % (config['bbtools_module'], subsample_read_count, fastq, subsample_fastq, subsample_log_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # coverage of subsampled fastq (runs quick)
        cov_log = os.path.join(the_path, "cov.log")
        cmd = "#%s;khist.sh in=%s khist=%s ow=t > %s 2>&1" % (config['bbtools_module'], subsample_fastq, cov_hist, cov_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(subsample_fastq):

        # save input fastq read stats
        fq_stats, fq_tsv = make_asm_stats(fastq, "input_fastq", the_path, rqc_file_log, log)
        save_asm_stats(fq_tsv, "input_fastq", rqc_stats_log, log)

        subsample_title = "subsampled_fastq"
        save_file(subsample_fastq, subsample_title, file_dict, output_path)

        # stats on subsampled fastq (just reads & bases)
        fq_stats, fq_tsv = make_asm_stats(subsample_fastq, subsample_title, the_path, rqc_file_log, log)
        save_asm_stats(fq_tsv, subsample_title, rqc_stats_log, log)

        append_rqc_file(rqc_file_log, "subsample_cov", cov_hist)
        append_rqc_stats(rqc_stats_log, "subsample_read_count", config['subsample'])
        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status



'''
Assemble using SPAdes
spades options: --phred-offset 33 -t 16 -m 120 --sc --careful -k 25,55,95 --12
-t = threads
-m = memory
--sc = MDA (single-cell) data
--careful = reduce indels and short mismatches
--12 = file has interleaved forward and reverse reads
-k = k-mer-sizes (odd, less than 128)
--phred-offset 33 - maybe not needed, auto function
* --tmp-dir (might speed up if on localdisk)
* --cov-cutoff - automatically determined

using library name to write to cscratch on Cori
'''
def do_assembly(library_name):
    log.info("do_assembly: %s", library_name)
    
    step_name = "assembly"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    do_split_reads = True # speeds up Spades

    # find normal fastq
    fastq = get_file("subsampled_fastq", file_dict, output_path, log)

    # read count before, after - bb?

    the_path = get_the_path(ASM_PATH, output_path)


    asm_fasta = os.path.join(the_path, "scaffolds.fasta")


    if os.path.isfile(asm_fasta):
        log.info("- skipping %s, found file: %s", step_name, asm_fasta)

    elif check_skip(step_name, fastq, config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:


        # split fastq into forward and reverse reads
        # spades does this but it is slower (Alicia/Alex)
        fastq_r1 = fastq.replace(".subsample.fastq", ".subsample.r1.fastq")
        fastq_r2 = fastq.replace(".subsample.fastq", ".subsample.r2.fastq")
        if do_split_reads:
            split_log = os.path.join(the_path, "reformat.log")
            #reformat.sh in=x.fq out1=r1.fq out2=r2.fq - use reformat rather than bbsplitpairs - Brian
            #cmd = "#%s;bbsplitpairs.sh in=%s out=%s out2=%s ow=t > %s 2>&1" % (config['bbtools_module'], fastq, fastq_r1, fastq_r2, split_log)
            cmd = "#%s;reformat.sh in=%s out1=%s out2=%s ow=t > %s 2>&1" % (config['bbtools_module'], fastq, fastq_r1, fastq_r2, split_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)
            # doesn't help much with runtime (28.31 min with reformat.sh, 29.71 min without reformat (--12 option for spades))
            
            
        # log for spades
        asm_log = os.path.join(the_path, "assembly.log")


        # if burst buffer setup
        tmp_folder = os.environ.get("DW_JOB_STRIPED", "")
        # check for $SLURM_TMP, doesn't help much with runtime (39.68 min without --tmp-dir, 39.78 min with --tmp-dir)
        if not tmp_folder:
            tmp_folder = os.environ.get("SLURM_TMP", "")
            
        if tmp_folder != "":
            tmp_folder = "--tmp-dir %s" % tmp_folder
            log.info("- using tmp_folder: %s", tmp_folder)

        # 2017-05-15: spades only works on shifter in $CSCRATCH on Cori due to how spades handles an assertion (INC0101703)
        # not sure on quota ...
        the_new_path = the_path
        cluster = os.environ.get('NERSC_HOST', 'unknown')
        if cluster == "cori":
            cscratch = os.environ.get('CSCRATCH', 'unknown')
            if cscratch != "unknown":
                the_new_path = get_the_path(library_name, os.path.join(cscratch, "iso"))
                log.info("- using path: %s", the_new_path)


        if do_split_reads:
            cmd = "#%s;spades.py -o %s %s %s -1 %s -2 %s > %s 2>&1" % (config['spades_module'], the_new_path, tmp_folder, config['spades_params'], fastq_r1, fastq_r2, asm_log)
        else:
            cmd = "#%s;spades.py -o %s %s %s --12 %s > %s 2>&1" % (config['spades_module'], the_new_path, tmp_folder, config['spades_params'], fastq, asm_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        if exit_code > 0:
            log.error("- fatal spades error: %s", exit_code)
            sys.exit(exit_code)

        # * rsync cscratch path to the_path
        if cluster == "cori":
            rsync_log = os.path.join(the_path, "cori-rsync.log")
            cmd = "rsync -a %s/* %s/. > %s 2>&1" % (the_new_path, the_path, rsync_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            # clean out cscratch - use a weekly job for this?
            if exit_code == 0:
                cmd = "rm -rf %s/*" % (the_new_path)
                std_out, std_err, exit_code = run_cmd(cmd, log)

        # remove split read files 
        if do_split_reads:
            cmd = "rm %s" % fastq_r1
            std_out, std_err, exit_code = run_cmd(cmd, log)
            cmd = "rm %s" % fastq_r2
            std_out, std_err, exit_code = run_cmd(cmd, log)
            

    if os.path.isfile(asm_fasta):

        fasta_type = "scaffolds"
        save_file(asm_fasta, fasta_type, file_dict, output_path)

        # bbstats for scaffolds
        asm_stats, asm_tsv = make_asm_stats(asm_fasta, fasta_type, the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

        asm_contigs_fasta = os.path.join(the_path, "contigs.fasta")
        save_file(asm_contigs_fasta, "contigs", file_dict, output_path)


        get_spades_version(rqc_stats_log, config, output_path, log)
        append_rqc_stats(rqc_stats_log, "spades_params", config['spades_params'])

        # remove misc, split_input folders - not needed and saves 50% of disk space
        for f in ['misc', 'split_input']:
            cmd = "rm -rf %s" % (os.path.join(the_path, f))
            std_out, std_err, exit_code = run_cmd(cmd, log)


        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status



'''
ISOLATE
- only drop contigs less than 1000bp
- does not need coverage trimming (RQCSUPPORT-1328)
'''
def do_contig_trim(library_name):
    log.info("do_contig_trim")


    the_path = get_the_path(CONTIG_TRIM_PATH, output_path)


    step_name = "contig_trim"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    contig_fasta = get_file("contigs", file_dict, output_path, log)
    scaffold_fasta = get_file("scaffolds", file_dict, output_path, log)
    fastq = get_file("subsampled_fastq", file_dict, output_path, log)


    contig_trim_fasta = ""
    if contig_fasta:
        contig_trim_fasta = os.path.join(the_path, os.path.basename(contig_fasta).replace(".fasta", ".trim.fasta"))

    scaffold_trim_fasta = ""
    if scaffold_fasta:
        scaffold_trim_fasta = os.path.join(the_path, os.path.basename(scaffold_fasta).replace(".fasta", ".trim.fasta"))


    if os.path.isfile(scaffold_trim_fasta) and os.path.isfile(contig_trim_fasta):
        log.info("- skipped %s, found file: %s", step_name, scaffold_trim_fasta)

    elif check_skip(step_name, scaffold_fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:

        # contig trimming
        trim_log = os.path.join(the_path, "trim_contigs.log")
        contig_trim_fasta_tmp = contig_trim_fasta.replace(".trim.fasta", ".trim.unnamed.fasta") # before adding the lib name to the contig headers
        cmd = "#%s;reformat.sh in=%s out=%s %s > %s 2>&1" % (config['bbtools_module'], contig_fasta, contig_trim_fasta_tmp, config['trim_params'], trim_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)



        # scaffolds trimming
        trim_log = os.path.join(the_path, "trim_scaffolds.log")
        scaffold_trim_fasta_tmp = scaffold_trim_fasta.replace(".trim.fasta", ".trim.unnamed.fasta") # before adding the lib name to the scaffold headers
        cmd = "#%s;reformat.sh in=%s out=%s %s > %s 2>&1" % (config['bbtools_module'], scaffold_fasta, scaffold_trim_fasta_tmp, config['trim_params'], trim_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)



        # rename headers to >(lib_name)_(header)
        rename_log = os.path.join(the_path, "rename_contigs.log")
        cmd = "#%s;rename.sh in=%s out=%s prefix=%s addprefix ow=t > %s 2>&1" % (config['bbtools_module'], contig_trim_fasta_tmp, contig_trim_fasta, library_name, rename_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        rename_log = os.path.join(the_path, "rename_scaffolds.log")
        cmd = "#%s;rename.sh in=%s out=%s prefix=%s addprefix ow=t > %s 2>&1" % (config['bbtools_module'], scaffold_trim_fasta_tmp, scaffold_trim_fasta, library_name, rename_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    # check for trimmed files
    if os.path.isfile(contig_trim_fasta) and os.path.isfile(scaffold_trim_fasta):


        save_file(contig_trim_fasta, "contigs_trim", file_dict, output_path)

        fasta_type = "scaffolds_trim"
        save_file(scaffold_trim_fasta, fasta_type, file_dict, output_path)

        # bbstats on 1k files
        asm_stats, asm_tsv = make_asm_stats(scaffold_trim_fasta, fasta_type, the_path, rqc_file_log, log)
        save_asm_stats(asm_tsv, fasta_type, rqc_stats_log, log)

        append_rqc_stats(rqc_stats_log, "trim_params", config['trim_params'])


        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status

# need to continue from here!

## ~~~~~~~~~~~~~~~~~~~
## QC Analysis things


'''
Contig gc stats
- plot histogram of gc per contig (weighted, not-weighted)
- 5 seconds
'''
def do_contig_gc_plot(qc_path, fasta_type, library_name):
    log.info("do_contig_gc_plot")

    step_name = "contig_gc_plot"



    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(CONTIG_GC_PATH, qc_path)

    fasta = get_file(fasta_type, file_dict, output_path, log)
    contig_gc_plot = os.path.join(the_path, "contig_gc.png")


    if os.path.isfile(contig_gc_plot):
        log.info("- skipping %s, found file: %s", step_name, contig_gc_plot)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        contig_gc_cmd = os.path.realpath(config['contig_gc_cmd'].replace("[my_path]", my_path))
        contig_gc_log = os.path.join(the_path, "contig_gc.log")
        #cmd = "%s -l %s -f %s -o %s > %s 2>&1" % (contig_gc_cmd, library_name, fasta, the_path, contig_gc_log)
        cmd = "%s -l %s -f %s -o %s > %s 2>&1" % (contig_gc_cmd, "%s:%s" % (library_name, os.path.basename(fasta)), fasta, the_path, contig_gc_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(contig_gc_plot):


        stats_file = os.path.join(the_path, "gc.stats.txt")
        gc_avg = 0.0
        gc_stdev = 0.0
        gc_median = 0.0
        fh = open(stats_file, "r")
        for line in fh:
            line = line.strip()
            arr = line.split("=")
            if line.startswith("gc_avg"):
                gc_avg = arr[-1]
            if line.startswith("gc_stdev"):
                gc_stdev = arr[-1]
            if line.startswith("gc_median"):
                gc_median = arr[-1]
        fh.close()

        log.info("- contig gc: %s +/- %s", gc_avg, gc_stdev)
        append_rqc_stats(rqc_stats_log, "contig_gc_avg", gc_avg)
        append_rqc_stats(rqc_stats_log, "contig_gc_stdev", gc_stdev)
        append_rqc_stats(rqc_stats_log, "contig_gc_median", gc_median)

        f_key = os.path.basename(contig_gc_plot)
        log.info("- saving file: %s", f_key)
        append_rqc_file(rqc_file_log, f_key, contig_gc_plot)

        
        for png_file in ['contig_hist.png', 'genome_vs_size.png']:
            append_rqc_file(rqc_file_log, png_file, os.path.join(the_path, png_file))

        # weighted gc, gc histogram files
        append_rqc_file(rqc_file_log, "contig_gc_hist", os.path.join(the_path, "contig.gc"))
        append_rqc_file(rqc_file_log, "contig_gc_whist", os.path.join(the_path, "contig-weighted.gc"))



        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)

    return status



'''
GC Coverage of the contigs
- creates cloud images gc vs cov (gc_cov_Kingdom.png)
- 10 minutes
'''
def do_gc_cov(qc_path, fasta_type, library_name):
    log.info("do_gc_cov")

    step_name = "gc_cov"

    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)


    fasta = get_file(fasta_type, file_dict, output_path, log)
    fastq_type = "subsampled_fastq"
    fastq = get_file(fastq_type, file_dict, output_path, log)


    the_path = get_the_path(GC_COV_PATH, qc_path)

    summary = os.path.join(the_path, "shred", "coverage.txt")


    if os.path.isfile(summary):
        log.info("- skipping %s, found file: %s", step_name, summary)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        gc_cov_cmd = os.path.realpath(config['gc_cov_cmd'].replace("[my_path]", my_path))
        gc_cov_log = os.path.join(the_path, "gc_cov2.log")
        #cmd = "%s -l %s -f %s -fq %s -o %s > %s 2>&1" % (gc_cov_cmd, library_name, fasta, fastq, the_path, gc_cov_log)
        # RQC-1092 - Kecia wants to use the fasta name
        cmd = "%s -l %s -f %s -fq %s -o %s > %s 2>&1" % (gc_cov_cmd, "%s:%s" % (library_name, os.path.basename(fasta)), fasta, fastq, the_path, gc_cov_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(summary):

        # save *.png, *.txt for RQC
        file_list = glob.glob(os.path.join(the_path, "*"))
        for f in file_list:
            if f.endswith(".png") or f.endswith(".txt"):

                f_key = os.path.basename(f)

                # need to reaname files with multiple "." to work with pdf maker (texlive)
                # e.g. class.taxonomy.png is bad, needs to be converted to class_taxonomy.pdf
                base_name = os.path.basename(f)

                log.info("- saving file: %s", f_key)
                append_rqc_file(rqc_file_log, f_key, f)

        # save coverage stats for reporting
        cov_stats = os.path.join(the_path, "covstats.txt")
        if os.path.isfile(cov_stats):
            fh = open(cov_stats, "r")
            for line in fh:
                arr = line.strip().split("=")
                if len(arr) > 1:
                    f_key = "cov_%s" % arr[0]

                    val = "{:.1f}".format(float(arr[1]))
                    append_rqc_stats(rqc_stats_log, f_key, val)

            fh.close()

        # look for anything with less than 20x coverage - Kecia's QC for possible contam: RQC-949
        gc_cov_contam_cnt = 0
        fh = open(summary, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            arr = line.strip().split()
            cov = int(float(arr[1])) # Avg_fold

            #print cov, arr[0]
            # Kecia's level:
            if cov < 20:
                gc_cov_contam_cnt += 1

        fh.close()

        append_rqc_stats(rqc_stats_log, "gc_cov_contam_cnt", gc_cov_contam_cnt)
        log.info("- gc_cov contam cnt (cov < 20): %s", gc_cov_contam_cnt)


        map_shreds_txt = os.path.join(the_path, "shred", "frag_vs_gc_cov.txt")
        if os.path.isfile(map_shreds_txt):
            append_rqc_file(rqc_file_log, "gc_cov_shreds_txt", map_shreds_txt)

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Blast the contigs
- 5+ hours (to report "no hits")

2 minutes for this part if localized just for NT ...
$ module load blast+
$ blastn -query /global/projectb/scratch/brycef/sag/BAGHS/pool_asm/pool_decontam.fasta \
 -db /scratch/rqc/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted \
 -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true   \
 -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids'   \
 -num_threads 8 > nt.log

 $ ~/git/jgi-rqc-pipeline/tools/run_blastplus.py -o /global/projectb/scratch/brycef/sag/BAGHS/megablast -q /global/projectb/scratch/brycef/sag/BAGHS/pool_asm/pool_decontam.fasta \
 -d /scratch/rqc/bbtools_dedupe_mask/nt_bbdedupe_bbmasked_formatted

'''
def do_blast(qc_path, fasta_type):
    log.info("do_blast")

    step_name = "blast"

    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(BLAST_PATH, qc_path)


    fasta = get_file(fasta_type, file_dict, output_path, log)

    done_file = os.path.join(the_path, "blastx.done")


    if os.path.isfile(done_file):
        log.info("- skipping %s, found file: %s", step_name, done_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        # copy database list to the run folder
        db_fof = os.path.realpath(os.path.join(my_path, "../sag/config/sag_contig_megablast.fof"))
        my_db = os.path.join(the_path, os.path.basename(db_fof))

        if not os.path.isfile(my_db):
            cmd = "cp %s %s" % (db_fof, my_db)
            std_out, std_err, exit_code = run_cmd(cmd, log)


        blast_cmd = os.path.realpath(config['run_blast_cmd'].replace("[my_path]", my_path))

        fh = open(my_db, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue
            if not line:
                continue

            # if find log file, can we skip this?
            blast_db = line
            blast_db_name = os.path.basename(blast_db)
            blast_log = os.path.join(the_path, "blastplus_%s.log" % blast_db_name)
            done_file = os.path.join(the_path, "blastplus_%s.done" % blast_db_name)
            if os.path.isfile(done_file):
                log.info("- skipping blast_db: %s, found file: %s", blast_db_name, done_file)

            else:
                log.info("- blasting: %s", os.path.basename(blast_db))

                cmd = "%s -s -o %s -q %s -d %s > %s" % (blast_cmd, the_path, fasta, blast_db, blast_log)
                # -s = show taxonomy in parsed results
                std_out, std_err, exit_code = run_cmd(cmd, log)


                fhd = open(done_file, "w")
                fhd.write("%s|%s\n" % (os.path.basename(blast_db), exit_code))
                fhd.close()

        fh.close()


    if os.path.isfile(done_file):

        # save blast results, count blast hits
        blast_total_hits = 0

        blast_list = glob.glob(os.path.join(the_path, "*.vs.*.parsed"))
        # look for parsed, then save the stats from the other files
        ext_list = ['.tophit', '.taxlist'] #, '.top100hit']
        for blast_file in blast_list:

            skip_file = False
            for e in ext_list:
                if blast_file.endswith(e):
                    skip_file = True


            if not skip_file:
                blast_name = os.path.basename(blast_file)
                arr = blast_name.split(".vs.")
                blast_name = arr[-1]

                hit_cnt = get_blast_hit_count(blast_file)
                log.info("- %s (%s hits)", blast_name, hit_cnt)


                if os.path.getsize(blast_file) > 0:

                    # save *.parsed and how many hits
                    append_rqc_file(rqc_file_log, blast_name, blast_file)

                    append_rqc_stats(rqc_stats_log, "%s_cnt" % blast_name, hit_cnt)

                    for e in ext_list:

                        blast_file_new = "%s%s" % (blast_file, e)
                        blast_file_new_key = "%s%s" % (blast_name, e)

                        if os.path.isfile(blast_file_new):

                            hit_cnt = get_blast_hit_count(blast_file_new)
                            # count total hits for stats
                            if e == ".tophit":
                                blast_total_hits += hit_cnt

                            append_rqc_stats(rqc_stats_log, "%s_cnt" % blast_file_new_key, hit_cnt)

                            append_rqc_file(rqc_file_log, blast_file_new_key, blast_file_new)
                            log.info("- %s (%s hits)", blast_file_new_key, hit_cnt)

                        else:
                            append_rqc_stats(rqc_stats_log, blast_file_new_key + "_cnt", 0)
                            log.info("- %s   MISSING", os.path.basename(blast_file_new))

        append_rqc_stats(rqc_stats_log, "blast_cnt", blast_total_hits)



        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Stephan's code for scanning blast results looking if we sequenced what we expect
- uses library name to look up the tax_id and tax_name
- used for auto-qc
- t3, t4 = 0 but others work okay (BPGHW)
'''
def is_right_thing(my_output_path, library_name):

    log.info("is_right_thing: %s", library_name)

    the_path = get_the_path(BLAST_PATH, my_output_path)

    tax_name = None
    tax_id = 0

    if library_name and not tax_name and not tax_id:
        tax_dict = get_library_info(library_name, log)
        
        tax_name = tax_dict['gold_organism_name']
        if not tax_name:
            tax_name = tax_dict['ncbi_organism_name']

    file_list = [] # list of blast .parsed files to use for checking

    parsed_list = glob.glob(os.path.join(the_path, "*.parsed"))
    for parsed_file in parsed_list:
        if parsed_file.endswith("vs.nt_bbdedupe_bbmasked_formatted.parsed"):
            file_list.append(parsed_file)
        elif parsed_file.endswith("vs.refseq.archaea.parsed"):
            file_list.append(parsed_file)
        elif parsed_file.endswith("vs.refseq.bacteria.parsed"):
            file_list.append(parsed_file)


    is_right_organism = 0

    if not tax_name:
        log.error("- no tax name to check!  %s", msg_warn)
    else:
        log.info("- tax_name: %s, tax_id: %s", tax_name, tax_id)

        is_right_organism = orgutils.Utils(debug=False).isRightOrganism(file_list, tax_name, taxId=tax_id, maxExpectScore=config['iro_max_expect_score'],
                                                                        minAlignLength=config['iro_min_align_length'], minPctId=config['iro_min_pct_id'])

    status = msg_fail
    if is_right_organism == 1:
        status = msg_ok
    log.info("- is_right_organism: %s  %s", is_right_organism, status)

    append_rqc_stats(rqc_stats_log, "isRightOrganism", is_right_organism)



'''
Blast for the 16s (pretty fast)
- in a separate function so we can run it w/o re-running all of blast steps
- auto-qc criteria?  e = 0, pct_id = 98, min_len = 500
- does the assembly contain the 16s (SSU) from ITS?

'''
def do_blast_16s(qc_path, fasta_type, library_name):
    log.info("do_blast_16s")

    step_name = "blast_16s"



    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    the_path = get_the_path(BLAST_16S_PATH, qc_path)


    fasta = get_file(fasta_type, file_dict, output_path, log)

    done_file = os.path.join(the_path, "blast16s.done")


    if os.path.isfile(done_file):
        log.info("- skipping %s, found file: %s", step_name, done_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        blast_cmd = os.path.realpath(config['run_blast_cmd'].replace("[my_path]", my_path))

        db_16s = os.path.join(the_path, "%s_16s.fasta" % library_name)

        if not os.path.isfile(db_16s):
            get_ssu(library_name, db_16s, log)
            # TODO - what if no 16s returned?

            # makeblastdb -dbtype nucl -in AUHTB_16s.fasta
            cmd = "#%s;makeblastdb -dbtype nucl -in %s" % (config['blast_module'], db_16s)
            std_out, std_err, exit_code = run_cmd(cmd, log)



        if os.path.getsize(db_16s) > 5:

            blast_log = os.path.join(the_path, "blastplus_16s_%s.log" % library_name)
            log.info("- blasting: 16s %s", library_name)
            # -l = don't localize this one
            cmd = "%s -l -o %s -q %s -d %s > %s" % (blast_cmd, the_path, fasta, db_16s, blast_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            if exit_code == 0:
                fh = open(done_file, "w")
                fh.write("%s_16s|%s\n" % (library_name, exit_code))
                fh.close()

        else:
            log.error("- 16s not found for %s", library_name)





    if os.path.isfile(done_file):

        # save blast results, count blast hits

        # criteria to match
        # num hits
        # expect < max_expect_score && align_len >= min_align_length && pct_id >= min_pct_id
        max_expect_score = 0.01
        min_align_length = 500
        min_pct_id = 98.0

        # only save the tophit file for 16s
        blast_list = glob.glob(os.path.join(the_path, "*.vs.*.parsed.tophit"))

        blast_file_key = "blast_16s.parsed.tophit"
        for blast_file in blast_list:

            hit_cnt = get_blast_hit_count(blast_file)
            append_rqc_stats(rqc_stats_log, blast_file_key + "_cnt", hit_cnt)

            append_rqc_file(rqc_file_log, blast_file_key, blast_file)
            log.info("- %s (%s hits)", blast_file_key, hit_cnt)

            # auto-qc to determine if we got a good 16s hit
            expect_score = 0.0
            align_length = 0
            pct_id = 0.0


            fh = open(blast_file, "r")
            for line in fh:
                if line.startswith("#"):
                    continue
                arr = line.split()
                #query subject expect length perc_id q_length s_length

                # tophit can be multi-line, use the first line - BBOSU
                if align_length == 0:
                    try:
                        expect_score = float(arr[2])
                        align_length = int(arr[3])
                        pct_id = float(arr[4])
                    except:
                        log.error("- could not get tophit data from %s", blast_file)

            fh.close()

            # auto-qc - doesn't need to be here
            blast_status = "16s not found"
            if expect_score < max_expect_score and align_length >= min_align_length and pct_id >= min_pct_id:
                blast_status = "16s found"
            log.info("- 16s: expect %s < %s, align_length %s >= %s, pct_id %s >= %s", expect_score, max_expect_score, align_length, min_align_length, pct_id, min_pct_id)


            append_rqc_stats(rqc_stats_log, "16s_hit", blast_status)

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name
        if not os.path.getsize(db_16s) > 5:
            # skip 16s if no ssu found
            status = "%s skipped" % step_name

    checkpoint_step(status_log, status)

    return status




'''
Megan plots: reads_coverage - gc histograms
similar to do_megan but we subsample the input fastq and converts to a fasta before running
* why do this on the input reads for isolates?
2018-04-25 - Kecia doesn't use this so we are skipping it

- reads_kingdom.png = histogram

'''
def do_gc_hist_reads(qc_path, library_name):
    log.info("do_gc_hist_reads")

    step_name = "gc_hist_reads"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(GC_HIST_READ_PATH, qc_path)


    contigs_class_png = os.path.join(the_path, "reads_Class.png")

    fastq = get_file("input_fastq", file_dict, output_path, log)

    if os.path.isfile(contigs_class_png):
        log.info("- skipping %s, found file: %s", step_name, contigs_class_png)

    elif check_skip(step_name, fastq, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        # subsample to 10000 (5000 paired reads)

        subsample_reads_target = 5000 # 5000 paired reads = 10000 reads
        fasta = os.path.join(the_path, os.path.basename(fastq).replace(".fastq.gz", "subsampled-5000.fasta"))
        reformat_log = os.path.join(the_path, "reformat.log")
        cmd = "#%s;reformat.sh in=%s out=%s samplereadstarget=%s ow=t > %s 2>&1" % (config['bbtools_module'], fastq, fasta, subsample_reads_target, reformat_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        gc_hist_cmd = os.path.realpath(config['gc_hist_cmd'].replace("[my_path]", my_path))
        gc_hist_log = os.path.join(the_path, "gc_hist.log")
        cmd = "%s -l %s -f %s -o %s -t reads > %s 2>&1" % (gc_hist_cmd, library_name, fasta, the_path, gc_hist_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(contigs_class_png):

        # save pngs
        png_list = glob.glob(os.path.join(the_path, "*.png"))
        for png in png_list:

            png_key = "read_%s" % (os.path.basename(png))
            log.info("- saving file: %s", png_key)
            append_rqc_file(rqc_file_log, png_key, png)


        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
reads_coverage - gc histograms
uses prodigal, lastal, bbtools

- reads_kingdom.png = histogram
'''
def do_gc_hist(qc_path, fasta_type, library_name):
    log.info("do_gc_hist")

    step_name = "gc_hist"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(GC_HIST_PATH, qc_path)


    contigs_class_png = os.path.join(the_path, "contigs_Class.png")
    fasta = get_file(fasta_type, file_dict, output_path, log)


    if os.path.isfile(contigs_class_png):
        log.info("- skipping %s, found file: %s", step_name, contigs_class_png)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        gc_hist_cmd = os.path.realpath(config['gc_hist_cmd'].replace("[my_path]", my_path))
        gc_hist_log = os.path.join(the_path, "gc_hist2.log")
        #cmd = "%s -l %s -f %s -o %s > %s 2>&1" % (gc_hist_cmd, library_name, fasta, the_path, gc_hist_log)
        cmd = "%s -l %s -f %s -o %s > %s 2>&1" % (gc_hist_cmd, "%s:%s" % (library_name, os.path.basename(fasta)), fasta, the_path, gc_hist_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(contigs_class_png):

        # save pngs
        png_list = glob.glob(os.path.join(the_path, "*.png"))
        for png in png_list:

            png_key = "megan_%s" % (os.path.basename(png))
            log.info("- saving file: %s", png_key)
            append_rqc_file(rqc_file_log, png_key, png)

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Check for contigs that should not be submitted to NCBI
e.g. Cow, cat, dog, human, e.coli ...
Prescreen db from 2013: /global/dna/shared/rqc/ref_databases/qaqc/databases/ncbi_prescreen_gcontam.fa
- 2017-06-06: no new gcontam db
ftp://ftp.ncbi.nih.gov/pub/kitts/ - gcontam.gz = 2012-03-02

BTHYZ (iso) has lots of hits
find tests: select rqc_pipeline_queue_id from rqc_pipeline_stats where stats_name = 'ncbi_prescreen_cnt' and dt_created > '2017-01-01';
'''
def do_ncbi_prescreen(qc_path, fasta_type):
    log.info("do_ncbi_prescreen")

    step_name = "ncbi_prescreen"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)


    the_path = get_the_path(NCBI_PRESCREEN_PATH, qc_path)


    fasta = get_file(fasta_type, file_dict, output_path, log)
    out_file = os.path.join(the_path, "ncbi_screen.out")


    if os.path.isfile(out_file):
        log.info("- skipping %s, found file: %s", step_name, out_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:


        prescreen_log = os.path.join(the_path, "ncbi_prescreen.log")

        blast_cmd = os.path.realpath(config['run_blast_cmd'].replace("[my_path]", my_path))
        log.info("- blasting vs ncbi_prescreen: %s", library_name)

        # old version used megablast 2.2.21
        # /projectb/sandbox/rqc/prod/pipelines/external_tools/ncbiScreening/ncbiPreScreen.pl
        # megablast -p 90.0 -D3 -F \"m D\" -fT -UT -R -d (db) -i (query file)
        # -p 90.0 = identity percentage cut-off
        # -D3 = tab-delimited one line format
        # -F = filter query sequence
        # -fT = show full IDs in output
        # -UT = use lowercase filtering of fasta
        # -R = report log info
        # -d = database
        # -i = query file (in file)

        #fasta = os.path.join(the_path, "scaffolds.trim.fasta.split.fasta")

        cmd = "%s -l -o %s -q %s -d %s > %s" % (blast_cmd, the_path, fasta, config['ncbi_prescreen_db'], prescreen_log)
        # -l = don't localize this one
        # defaults to -perc_identity 90 using -task megablast
        std_out, std_err, exit_code = run_cmd(cmd, log)


        blast_file = glob.glob(os.path.join(the_path, "megablast.scaffolds.trim.*.parsed"))[-1]
        hit_cnt = 0

        fhw = open(out_file, "w")

        if blast_file:

            fh = open(blast_file, "r")
            for line in fh:
                if line.startswith("#"):
                    continue

                line = line.strip()

                # is it a good hit?
                arr = line.split()
                query_len = 0
                if len(arr) > 7:
                    hit_bases = int(arr[4]) # = 4
                    query_len = int(arr[8]) # = 8

                if query_len > 0:

                    hit_pct = hit_bases / float(query_len)
                    if hit_pct > 0.75:
                        log.info("- hit to %s.  hit = %s, query_len = %s, pct = %s", arr[1], hit_bases, query_len, format_val_pct(hit_pct))
                        fhw.write(line + "\n")
                        hit_cnt += 1

            fh.close()

        hit_status = msg_fail
        if hit_cnt == 0:
            hit_status = msg_ok
            fhw.write("# No contaminants found\n")

        fhw.close()


        log.info("- hit_cnt: %s  %s", hit_cnt, hit_status)


    if os.path.isfile(out_file):

        file_key = "ncbi_prescreen_%s" % (os.path.basename(out_file))
        log.info("- saving file: %s", file_key)
        append_rqc_file(rqc_file_log, file_key, out_file)
        # should process and save results in rqc-stats.txt?  No.  We'll never find contaminates in our data.

        prescreen_cnt = 0
        fh = open(out_file, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            prescreen_cnt += 1

        fh.close()


        append_rqc_stats(rqc_stats_log, "ncbi_prescreen_cnt", prescreen_cnt)

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status



'''
reporting % of reads assembled in assembly (quick)
- map the subsampled fastq against the raw/screened file
- for coverage for GOLD
'''
def do_pct_reads_asm(qc_path, fasta_type):
    log.info("do_pct_reads_asm")

    step_name = "pct_reads_asm"


    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(PCT_READS_ASM_PATH, qc_path)

    fasta = get_file(fasta_type, file_dict, output_path, log)
    ref_fasta = get_file("scaffolds_trim", file_dict, output_path, log)

    fastq_type = "subsampled_fastq"
    fastq = get_file(fastq_type, file_dict, output_path, log)

    summary_file = os.path.join(the_path, "bbmap.stats.txt")


    if os.path.isfile(summary_file):
        log.info("- skipping %s, found file: %s", step_name, summary_file)


    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        bbmap_log = os.path.join(the_path, "bbmap.log")
        cmd = "#%s;bbmap.sh ref=%s in=%s nodisk=t ow=t machineout=t statsfile=%s > %s 2>&1" % (config['bbtools_module'], ref_fasta, fastq, summary_file, bbmap_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(summary_file):

        # save the info
        file_key = "pct_reads_asm_%s" % (os.path.basename(summary_file))
        log.info("- saving file: %s", file_key)
        append_rqc_file(rqc_file_log, file_key, summary_file)

        fh = open(summary_file, "r")

        r1_map = -1 # number of reads mapped to read 1
        r2_map = -1 # number of reads mapped to read 2
        r_ttl = 0 # total reads

        for line in fh:
            if line.startswith("R1_Mapped_Reads"):
                r1_map = int(line.split("=")[-1])

            if line.startswith("R2_Mapped_Reads"):
                r2_map = int(line.split("=")[-1])

            if line.startswith("Reads_Used"):
                r_ttl = int(line.split("=")[-1])

        fh.close()

        # pct = r1_map / total reads (if single stranded)
        pct = 0.0
        if r1_map > -1:
            log.info("- r1_map reads: %s", r1_map)
            pct = r1_map / float(r_ttl)

        # pct = (r1_map + r2_map)/ total reads (if paired, should always be paired)
        if r2_map > -1:
            log.info("- r2_map reads: %s", r2_map)
            pct = (r1_map + r2_map) / float(r_ttl)

        log.info("- total reads: %s", r_ttl)


        pct = pct * 100

        my_key = "pct_aligned"
        append_rqc_stats(rqc_stats_log, my_key, pct)
        log.info("- pct aligned: %s", pct)

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
checkM - replace older SCG script
 - uses a list of genes based on the taxonomoy
 - bacteria = 107, archaea = 116, both = 49
 - lwf = lineage workflow - tries to find best spot in a taxonomy tree to decide what genes to use (slower)

15 minutes
jgi-rqc-pipeline/tools/checkm.py -f (assembly) -o (outdir) -r d-bac -p -c
 -f = assembly to be evaluated
'''
def do_checkm(qc_path, fasta_type):
    log.info("do_checkm")

    step_name = "checkm"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(CHECKM_PATH, qc_path)
    fasta = get_file(fasta_type, file_dict, output_path, log)

    report_file = os.path.join(the_path, "bac", "report.txt") # just check bacteria to start with
    missing_img = os.path.realpath(os.path.join(my_path, "../sag/templates/chart_not_avail.png"))

    if os.path.isfile(report_file):
        log.info("- skipping %s, found file: %s", step_name, report_file)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        checkm_log = os.path.join(the_path, "checkm.log")
        checkm_wrapper = config['checkm_wrapper']


        #module load jgi-rqc; checkm_helper.py -i FASTA -o JOBDIR
        checkm_wrapper = os.path.realpath(checkm_wrapper.replace("[my_path]", my_path))
        cmd = "%s -i %s -o %s --verbose > %s 2>&1" % (checkm_wrapper, fasta, the_path, checkm_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        # using qc_path, checkm_wrapper creates the checkm subdir
        # checkm_helper runs all 3 models concurrently each with 8 threads


    if os.path.isfile(report_file):

        # need to save bac, arc, lwf
        # save summary.txt, contig_hits.txt, report.txt
        # need gene list for reporting
        for mtype in ['bac','arc','lwf']: # model types
            for f in ['report.txt', 'contig_hits.txt', 'gene_contigs.txt', 'summary.txt']:
                save_file = os.path.join(the_path, mtype, f)
                if os.path.isfile(save_file):
                    file_key = "checkm_%s_%s" % (mtype, f)

                    log.info("- saving file: %s", file_key)
                    append_rqc_file(rqc_file_log, file_key, save_file)



            # expect 3:
            # - checkm_chart_d-*.png = all counts for all genes
            # - checkm_chart_d-*_nsc.png = everything thats not single copy
            # - checkm_histogram_d-*.png = histogram count for each gene
            #png_list = glob.glob(os.path.join(the_path, mtype, "*.png"))
            png_list = ['checkm_chart_d-%s.png' % mtype, 'checkm_chart_d-%s_nsc.png' % mtype, 'checkm_histogram_d-%s.png' % mtype]
            for png in png_list:

                if mtype == "lwf":
                    png = png.replace("_d-", "_")

                png = os.path.join(the_path, mtype, png)
                file_key = os.path.basename(png).replace(".png", "_image")

                if not os.path.isfile(png):
                    log.info("- warning, no file created for %s, using missing image  %s", png, msg_warn)
                    cmd = "cp %s %s" % (missing_img, png)
                    std_out, std_err, exit_code = run_cmd(cmd, log)

                log.info("- saving file: %s", file_key)
                append_rqc_file(rqc_file_log, file_key, png)


            # summary from report for histogram - want to get number of 0 counts, 1 counts, 2+ counts
            gene_hist = collections.Counter()

            line_cnt = 0
            report_file = os.path.join(the_path, mtype, "report.txt")
            fh = open(report_file, "r")
            for line in fh:
                line_cnt += 1
                #if line.startswith("Filename"):
                #    continue


                #if line.startswith("/"):
                #    continue

                arr = line.split()
                for a in arr:
                    if a.startswith("/"):
                        continue
                    if a.startswith("Filename"):
                        continue

                    if line_cnt == 1:
                        pass
                    if line_cnt == 2:
                        gene_hist[str(a)] += 1

            fh.close()



            hit_0 = 0
            hit_1 = 0
            hit_2 = 0 # 2 or more hits

            for g in gene_hist:

                if g == "0":
                    hit_0 = gene_hist[g]
                elif g == "1":
                    hit_1 = gene_hist[g]
                else:
                    hit_2 += gene_hist[g]


            log.info("-- model: %s - 0 hits: %s, 1 hit: %s, 2+ hits: %s", mtype, hit_0, hit_1, hit_2)

            append_rqc_stats(rqc_stats_log, "checkm_%s_hit_0" % mtype, hit_0)
            append_rqc_stats(rqc_stats_log, "checkm_%s_hit_1" % mtype, hit_1)
            append_rqc_stats(rqc_stats_log, "checkm_%s_hit_2" % mtype, hit_2)


            # save summary stats
            summary_file = os.path.join(the_path, mtype, "summary.txt")

            if not os.path.isfile(summary_file):
                create_empty_checkm_summary(summary_file)

            if os.path.isfile(summary_file):
                fh = open(summary_file, "r")
                line1 = ""
                line2 = ""
                for line in fh:

                    if line.startswith("#Filename"):
                        line1 = line.strip()
                    else:
                        line2 = line.strip()

                fh.close()

                arr1 = line1.split()
                arr2 = line2.split()

                cnt = 0
                for a in arr1:

                    if a != "#Filename":
                        my_key = "checkm_%s_%s" % (mtype, a)
                        log.info("-- %s:  %s = %s", mtype, my_key, arr2[cnt])
                        append_rqc_stats(rqc_stats_log, my_key, arr2[cnt])

                        # save as percents for pdf report, web report
                        if a in ("EstGenomeComSimple", "EstGenomeComNormalized"):
                            pct_key = "checkm_%s_%s_pct" % (mtype, a)

                            val = format_val_pct(arr2[cnt])
                            append_rqc_stats(rqc_stats_log, pct_key, val)

                    cnt += 1

            else:
                log.info("- warning, no summary file for %s  %s", mtype, msg_warn)


        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Tetramer calc
- create tetramer kmers: KmerFrequencies.jar
-- replaced with: BBTools.TetramerFreq.sh in=CONTIG_FASTA out=TETRAMER_FREQ window=2000 (1 sec)
- create tetramer plot: showKmerBin.R (6 sec)
Konstantinos Mavrommatis, Dec 2011

* need at least 2 data lines in the *.kmer file to make the tetramer.jpg
Kurt: tetramer should be run on contigs, scaffolds messes it up

'''
def do_tetramer(qc_path, fasta_type):
    log.info("do_tetramer")

    step_name = "tetramer"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(TETRAMER_PATH, qc_path)

    tetramer_jpg = os.path.join(the_path, "tetramer.jpg")
    fasta = get_file(fasta_type, file_dict, output_path, log)

    if os.path.isfile(tetramer_jpg):
        log.info("- skipping %s, found file: %s", step_name, tetramer_jpg)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        # check for 10+ contigs in fasta?

        # create kmer frequencies for each 2000bp window shifted by 500bp, kmer size = 4bp (256 total kmers)
        kmer_out = os.path.join(the_path, "iso.tet.kmers")
        kmer_log = os.path.join(the_path, "kmer_freq.log")

        cmd = "#%s;tetramerfreq.sh in=%s out=%s %s > %s 2>&1" % (config['bbtools_module'], fasta, kmer_out, config['tetramer_config'], kmer_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # kmer_out needs at least 3 lines to run show_kmer_bin R script (3 + 1 header)
        line_cnt = 0
        line = "" # last line read
        fh = open(kmer_out, "r")
        for _ in fh:
            line_cnt += 1
            if _:
                line = _
        fh.close()

        # if less than 4 lines then repeat last line, this makes for a dull tetramer plot but it won't exit with an error
        if line_cnt < 5:
            fh = open(kmer_out, "a")
            for _ in range(5 - line_cnt):
                fh.write(line)
            fh.close()


        tet_out = os.path.join(the_path, "tetramer") # automatically creates tetramer.jpg
        tet_log = os.path.join(the_path, "show_kmer_bin.log")
        show_kmer_bin = config['show_kmer_bin_r']
        show_kmer_bin = os.path.realpath(show_kmer_bin.replace("[my_path]", my_path))

        cmd = "#%s;Rscript --vanilla %s --input %s --output %s %s > %s 2>&1" % (config['r_module'], show_kmer_bin, kmer_out, tet_out, config['tetramer_r_config'], tet_log)
        # label was "iso - PC1 vs PC2
        # needs doturl, jgpurl - creates html version too but we ignore that
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(tetramer_jpg):

        f_key = "tetramer_jpg"
        append_rqc_file(rqc_file_log, f_key, tetramer_jpg)
        log.info("- saving file: %s", f_key)
        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Antifam - run prodigal and hmmer to look for broken genes
1 minute
'''
def do_antifam(qc_path, fasta_type):
    log.info("do_antifam")

    step_name = "antifam"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(ANTIFAM_PATH, qc_path)

    antifam_report = os.path.join(the_path, "antifam.txt")

    fasta = get_file(fasta_type, file_dict, output_path, log)

    if os.path.isfile(antifam_report):
        log.info("- skipping %s, found file: %s", step_name, antifam_report)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:

        # min of 20000bp for prodigal
        asm_stats, asm_tsv = make_asm_stats(fasta, "tmp", the_path, rqc_file_log, log)

        # parse asm_tsv to get contigs and bases
        base_cnt, contig_cnt = get_base_contig_cnt(asm_tsv)
        min_base_cnt = int(config['prodigal_base_count'])
        if base_cnt < min_base_cnt:
            log.info("- base_cnt: %s < %s, cannot run antifam process  %s", base_cnt, min_base_cnt, msg_fail)
            fh = open(antifam_report, "w")
            fh.write("# cannot run prodigal\n")
            fh.write("# assembly base count: %s\n" % base_cnt)
            fh.write("# minimum prodigal base count: %s\n" % min_base_cnt)
            fh.close()

        else:

            # prodigal outputs
            protein_fa = os.path.join(the_path, "gene_p.fa")
            nuc_fa = os.path.join(the_path, "gene_n.fa")
            prodigal_log = os.path.join(the_path, "prodigal.log")

            cmd = "#%s;prodigal -i %s -a %s -d %s -o %s" % (config['prodigal_module'], fasta, protein_fa, nuc_fa, prodigal_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            # hmmsearch: Search a protein profile HMM against a protein sequence database
            hmmer_log = os.path.join(the_path, "hmmsearch.log")
            cmd = "#%s;hmmsearch --cpu 16 --tblout %s %s %s > %s" % (config['hmmer_module'], antifam_report, config['antifam_db'], protein_fa, hmmer_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)
            # --tblout = parsable table of per-sequence hits to file <f>
            # E-value - smaller = better hit
            # score = bitscore
            # Bias = only matters when its close to the score field

    if os.path.isfile(antifam_report):

        f_key = "antifam_report"
        append_rqc_file(rqc_file_log, f_key, antifam_report)
        log.info("- saving file: %s", f_key)

        #append_flow(flow_file, "polish", "", "antifam", "Antifam", "Antifam (prodigal 2.6.3, hmmer 3.1b2)")

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


'''
Use checkm, barrnap and tRNAscan-SE to determine the assembly quality
for papers (SIGS - standards in genomic sciences)
'''
def do_qual(qc_path, fasta_type):
    log.info("do_qual")

    step_name = "qual"

    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    the_path = get_the_path(QUAL_PATH, qc_path)

    quality_report = os.path.join(the_path, "quality.txt")

    fasta = get_file(fasta_type, file_dict, output_path, log)

    if os.path.isfile(quality_report):
        log.info("- skipping %s, found file: %s", step_name, quality_report)

    elif check_skip(step_name, fasta, config, status_log, log):
        status = "%s skipped" % step_name
        return status

    else:
        
        qual_cmd = os.path.realpath(config['qual_cmd'].replace("[my_path]", my_path))

        qual2_log = os.path.join(the_path, "qual2.log")
        cmd = "%s -f %s -o %s > %s 2>&1" % (qual_cmd, fasta, the_path, qual2_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        
    if os.path.isfile(quality_report):

        fh = open(quality_report, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue
            arr = line.split("|")
            if len(arr) == 2:
                my_key = "qual_%s" % arr[0]
                val = arr[1]
                append_rqc_stats(rqc_stats_log, my_key, val)
                if arr[0] == "quality":
                    log.info("-- assembly quality: %s", arr[1])
        fh.close()
        
        

        status = "%s complete" % step_name

    else:
        status = "%s failed" % step_name


    checkpoint_step(status_log, status)

    return status


## ~~~~~~~~~~~~~~~~~~~
## PDF, TXT reporting

'''
Use iso.tex, iso.metadata.json, iso.txt
- images in the pdf report can only have one "." in the name, e.g. my_thing.fa.chart.png won't work.  Need to rename the file
- writes to report
* if re-run change rqc-files.txt* & rqc-stats.txt* to *.tmp
'''
def do_pdf_report(library_name, fasta_type, org_name, fastq_list):
    log.info("do_pdf_report: %s", library_name)

    step_name = "pdf_report"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)



    the_path = get_the_path(REPORT_PATH, output_path)

    # output file names
    pdf_report = os.path.join(the_path, "iso-%s.pdf" % (library_name))
    latex_report = pdf_report.replace(".pdf", ".tex")
    txt_report = pdf_report.replace(".pdf", ".txt")
    # jat metadata.json in output_path because files are relative to output_path
    jat_metadata = os.path.join(output_path, "iso.metadata.json")

    if os.path.isfile(pdf_report) and 1==2:
        log.info("- skipping %s, found file: %s", step_name, pdf_report)

    elif check_skip(step_name, "run_me", config, status_log, log):
        status = "%s skipped" % step_name
        return status


    else:


        # setup merge dictionary to use for the merging
        merge_dict = get_library_info(library_name, log)

        if not 'seq_proj_id' in merge_dict:
            log.error("- no seq_proj_id for %s!  %s", library_name, msg_fail)
            status = "%s failed" % step_name
            return status

        merge_dict['organism_name'] = merge_dict['ncbi_organism_name']
        if 'gold_organism_name' in merge_dict and merge_dict['gold_organism_name']:
            merge_dict['organism_name'] = merge_dict['gold_organism_name']
        
            
        # replace with passed in organism name
        if org_name:
            merge_dict['organism_name'] = org_name

        merge_dict['jgi_logo'] = os.path.realpath(os.path.join(my_path, "../sag/templates", "jgi_logo.jpg"))
        merge_dict['library_name'] = library_name

        merge_dict['assembler_name'] = config['spades_module'] #"SPAdes" # spades/3.7.1
        merge_dict['assembler_params'] = config['spades_params']
        merge_dict['assembler_version'] = config['spades_module'].replace("spades/","")

        merge_dict['input_fastq_full_path'] = get_file("input_fastq", file_dict, output_path, log)
        merge_dict['input_fastq'] = os.path.basename(merge_dict['input_fastq_full_path'])

        merge_dict['report_date'] = time.strftime("%x") # %x = date in local format

        merge_dict['subsample_reads'] = config['subsample']
        #merge_dict['trim_map_params'] = config['trim_map_params']
        merge_dict['trim_params'] = config['trim_params']


        tapid = config['analysis_project_type_id']
        tatid = config['analysis_task_type_id']

        merge_dict['analysis_project_id'], merge_dict['analysis_task_id'] = get_analysis_project_id(merge_dict['seq_proj_id'], tapid, tatid, the_path, log)



        # read in stats & files into merge dict
        for f in [rqc_stats_log, rqc_file_log]:

            #if not os.path.isfile(f):
            #    f = f.replace(".tmp", ".txt")

            log.info("- importing: %s", f)

            fh = open(f, "r")

            for line in fh:
                #print line
                if line.startswith("#"):
                    continue
                arr = line.split("=")

                if len(arr) > 1:
                    my_key = str(arr[0]).lower().strip()
                    my_val = str(arr[1]).strip()
                    merge_dict[my_key] = my_val
                    #print my_key, my_val, merge_dict[my_key]
            fh.close()


        # if no scaffolds then write a text file
        fail_flag = 0
        check_key = "%s_scaf_bp" % fasta_type
        check_val = 0
        if check_key in merge_dict:
            check_val = int(merge_dict[check_key])
        else:
            fail_flag = 1

        if check_val < 5:
            fail_flag = 1

        log.info("- check key %s = %s > 5", check_key, check_val)
        if fail_flag == 1:

            fh = open(txt_report, "w")
            fh.write("Report date: %s\n" % merge_dict['report_date'])
            fh.write("ISO did not produce any clean contigs/scaffolds for library %s.\n" % (merge_dict['library_name']))
            fh.close()

            append_rqc_file(rqc_file_log, os.path.basename(txt_report), txt_report)
            log.error("- %s = %s, not creating reports  %s", check_key, check_val, msg_fail)
            status = "%s skipped" % step_name
            checkpoint_step(status_log, status)

            return status

        # picks up the input fastq with full path sometimes
        if str(merge_dict['input_fastq']).startswith("/"):
            merge_dict['input_fastq'] = os.path.basename(merge_dict['input_fastq'])


        # if 2+ seq units used then show in a nice table
        instrument_type_list = []
        merge_dict['input_fastq_tex'] = "\\"
        if len(fastq_list) >= 1:
            # get seq_unit_info per seq_unit
            raw_data = ""
            input_fastq_tex = """
\\begin{tabular}{|l|l|r|l|} \\hline
File name & Library & Number of reads & Read type \\tabularnewline \\hline
"""

            seq_unit_info_list = get_seq_unit_info(fastq_list, log)
            for su in seq_unit_info_list:
                raw_data += "%s\t%s\t%s\tIllumina Std PE (cassava 1.8)\t%s\tIllumina\t%s\n" % (su['library_name'], su['filter_reads_count'], su['run_configuration'],
                                                                                             su['input_name'], su['instrument_type'])

                # assuming we are using filtered fastqs
                read_count = "{:,}".format(su['filter_reads_count'])
                input_fastq_tex += "%s & %s & %s & %s bp \\tabularnewline \\hline\n" % (su['input_name'], su['library_name'], read_count, su['run_configuration'])

                instrument_type_list.append(su['instrument_type'])

            input_fastq_tex += "\\end{tabular}\n"

            if len(fastq_list) == 1:
                for su in seq_unit_info_list:
                    read_count = "{:,}".format(su['filter_reads_count'])
                    input_fastq_tex = """
\\begin{tabular}{|l|l|} \\hline
File name & %s \\tabularnewline \\hline
Library & %s \\tabularnewline \\hline
Number of reads & %s \\tabularnewline \\hline
Read type & %s bp \\tabularnewline \\hline
\\end{tabular}
                    """ % (su['input_name'], su['library_name'], read_count, su['run_configuration'])

            merge_dict['instrument_type'] = ", ".join(list(set(instrument_type_list))) # merges duplicates into the same element
            merge_dict['raw_data'] = raw_data
            merge_dict['input_fastq_table'] = input_fastq_tex




        # pull in stats for the assembly based on the fasta_type
        asm_keys = ["n_scaffolds", "n_contigs", "scaf_bp", "contig_bp", "scaf_n50", "scaf_l50", "ctg_n50", "ctg_l50", "scaf_max", "ctg_max", "scaf_n_gt50k", "scaf_pct_gt50k", "gap_pct",
                    "gc_avg", "gc_std"]
        for asm_stat in asm_keys:
            the_key = "%s_%s" % (fasta_type, asm_stat)

            if the_key in merge_dict:
                #print the_key, merge_dict[the_key]
                merge_dict[asm_stat] = merge_dict[the_key]

        #print merge_dict
        # 3. Read QC results - how much was removed in normalization, subsampling

        subsampled_reads_removed = 0
        subsampled_reads_pct = 0.0
        if int(merge_dict['input_fastq_read_count']) > 0:

            if 'subsampled_fastq_read_count' in merge_dict:
                subsampled_reads_removed = int(merge_dict['input_fastq_read_count']) - int(merge_dict['subsampled_fastq_read_count'])
                subsampled_reads_pct = 1 - int(merge_dict['subsampled_fastq_read_count']) / float(merge_dict['input_fastq_read_count'])

            merge_dict['total_reads_removed'] = subsampled_reads_removed
            merge_dict['total_reads_removed_pct'] = int(merge_dict['total_reads_removed']) / float(merge_dict['input_fastq_read_count'])

            merge_dict['subsampled_fastq_read_rmv'] = subsampled_reads_removed
            merge_dict['subsampled_fastq_read_pct'] = subsampled_reads_pct

            merge_dict['total_read_count'] = int(merge_dict['subsampled_fastq_read_count'])
            merge_dict['total_reads_pct'] = 1 - merge_dict['total_reads_removed_pct']



            # Contaminate summary
            merge_dict['contam_summary_table'] = "\\"
            merge_dict['contam_id_table'] = "\\"
            if 'contam_summary.txt' in merge_dict:
                merge_dict['contam_summary_table'], merge_dict['contam_id_table'], merge_dict['contam_txt'] = contam_summary_pdf(merge_dict['contam_summary.txt'],
                                                                                                                                 merge_dict['input_fastq_read_count'], log)


        # green_genes 16s RNA - use ssu tophits
        merge_dict['green_genes_table'] = ""

        merge_dict['green_genes_table'] += green_genes_pdf(merge_dict.get('ssuref_tax_silva.parsed.tophit'), log)

        if merge_dict['green_genes_table'] == "":
            merge_dict['green_genes_table'] = "\\\\No hits to the reference.\\\\"


        # stats for txt file
        asm_stats_file = os.path.join(output_path, CONTIG_TRIM_PATH, "scaffolds.trim.txt")

        asm_stats_buffer = ""
        fh = open(asm_stats_file, "r")
        for line in fh:
            asm_stats_buffer += line
        fh.close()
        merge_dict['asm_stats_file'] = asm_stats_buffer


        # ncbi prescreen - text file only
        ncbi_key = "ncbi_prescreen_cnt"
        ncbi_file_key = "ncbi_prescreen_ncbi_screen.out"

        merge_dict['ncbi_contamination'] = ncbi_prescreen_report(merge_dict[ncbi_file_key], merge_dict[ncbi_key], log)


        # checkm doesn't always produce output
        for checkm_type in ['arc', 'bac', 'lwf']:
            checkm_key = "checkm_%s_foundscgenes" % (checkm_type)
            if checkm_key not in merge_dict:
                log.info("- checkm %s data not found, setting to N/A  %s", checkm_type, msg_warn)
                for checkm_key in ['checkm_'+checkm_type+'_foundscgenes', 'checkm_'+checkm_type+'_totalscgenes', 'checkm_'+checkm_type+'_estgenomecomsimple_pct',
                                   'checkm_'+checkm_type+'_estgenomecomnormalized_pct']:
                    merge_dict[checkm_key] = "N/A"

                # image for charts not created
                if checkm_type == "lwf":
                    for checkm_key in ['checkm_chart_'+checkm_type+'_image', 'checkm_histogram_'+checkm_type+'_image']:
                        merge_dict[checkm_key] = os.path.realpath(os.path.join(my_path, "../sag/templates/chart_not_avail.png"))
                else:
                    for checkm_key in ['checkm_chart_d-'+checkm_type+'_image', 'checkm_histogram_d-'+checkm_type+'_image']:
                        merge_dict[checkm_key] = os.path.realpath(os.path.join(my_path, "../sag/templates/chart_not_avail.png"))


        # debug2
        #for k in sorted(merge_dict.iterkeys()):
        #    print "* '%s' = '%s'" % (k, merge_dict[k])


        ## PDF & TXT report
        # read template
        my_template = "iso.tex"
        my_txt_template = "iso.txt"
        my_json_template = "iso.metadata.json"


        latex_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_template))
        txt_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_txt_template))
        json_template_file = os.path.realpath(os.path.join(my_path, "../sag/templates", my_json_template))



        # metadata.json for JAT
        # written to output_path, files are relative to output_path
        merge_dict['pdf_report'] = "report/%s" % os.path.basename(pdf_report)
        merge_dict['txt_report'] = "report/%s" % os.path.basename(txt_report)

        x = "%s/%s" % (SUBSAMPLE_PATH, os.path.basename(get_file("subsampled_fastq", file_dict, output_path, log)))
        merge_dict['subsample_file'] = x
        #print fasta_type, decontam_fasta_type, get_file(fasta_type)

        # get the relative path, not full path
        asm_fasta = get_file(fasta_type, file_dict, output_path, log)
        asm_fasta = "%s/%s" % (asm_fasta.split("/")[-2], os.path.basename(asm_fasta))

        merge_dict['assembly_fasta'] = asm_fasta


        # some fields in JAT template are named to be consistant with older templates per Andrew 2016-06-07


        # create new [at_id].iso.metadata.json: RQCSUPPORT-1418
        jat_metadata = os.path.join(output_path, "%s.iso.metadata.json" % merge_dict['analysis_task_id'])
        merge_template(json_template_file, jat_metadata, merge_dict, log)


        # symlink to old file (jat_metadata) - remove these lines 2018?
        jat_metadata_new = "iso.metadata.json"
        cmd = "cd %s;ln -s %s %s" % (output_path, os.path.basename(jat_metadata), jat_metadata_new)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # formatting pct, int after metadata.json written ...

        # formatting integers to have ,'s 1000000 -> 1,000,000
        format_list_int = ["contig_bp", "scaf_bp", "scaf_max", "ctg_max", "input_fastq_base_count", "input_fastq_read_count",
                            "subsampled_fastq_read_count", "subsampled_fastq_base_count", "subsampled_fastq_read_rmv", "total_read_count",
                            "total_reads_removed", "raw_reads_count", "sdm_raw_base_count"]


        for k in format_list_int:

            if k in merge_dict:
                try:
                    # convert to mb or kb or gb or ... ?
                    merge_dict[k] = "{:,}".format(int(merge_dict[k]))
                except:
                    pass



        # 0.2555 -> 25.55%
        format_list_pct = ["scaf_pct_gt50k", "pct_aligned", "subsampled_fastq_read_pct", "total_reads_pct", "total_reads_removed_pct"]

        # convert from 0.xxyy to xx.yy%
        for k in format_list_pct:

            if k in merge_dict:
                merge_dict[k] = format_val_pct(merge_dict[k])



        # txt report
        merge_template(txt_template_file, txt_report, merge_dict, log)


        # PDF/latex
        err_cnt = merge_template(latex_template_file, latex_report, merge_dict, log)
        if err_cnt > 0:
            log.error("- fatal error: missing keys for pdf report!  %s", msg_fail)
            sys.exit(8)

        # convert .tex to .pdf
        # - okay if non-zero exit code
        pdflatex_log = os.path.join(the_path, "pdflatex.log")

        cmd = "#texlive;pdflatex -interaction=nonstopmode -output-directory=%s %s > %s 2>&1" % (the_path, os.path.basename(latex_report), pdflatex_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(pdf_report) and os.path.isfile(txt_report):

        append_rqc_file(rqc_file_log, os.path.basename(pdf_report), pdf_report)
        append_rqc_file(rqc_file_log, os.path.basename(txt_report), txt_report)

        status = "%s complete" % step_name
    else:
        status = "%s failed" % step_name
        log.error("- failed to create %s  %s", pdf_report, msg_fail)

    checkpoint_step(status_log, status)


    return status



'''
Clean up
- gzip files in some directories

'''
def do_purge():
    log.info("do_purge")

    step_name = "purge"
    status = "%s in progress" % step_name

    checkpoint_step(status_log, status)

    # gzip files > 1m only in certain folders
    #gzip_folder(os.path.join(output_path, ASM_PATH, "split_input"))
    gzip_folder(os.path.join(output_path, "iso"))

    status = "%s complete" % step_name

    checkpoint_step(status_log, status)

    return status

'''
gzip files > 1M in some folders
'''
def gzip_folder(my_folder):
    log.info("gzip_folder: %s", my_folder)

    if os.path.isdir(my_folder):

        cmd = "#pigz;cd %s;find . -size +1M ! -iname '*.parsed' | xargs pigz" % (my_folder)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    else:
        log.warn("- no such folder: %s  %s", my_folder, msg_warn)



## ----------------------
## control/helper functions



'''
Get the next thing to run
'''
def get_next_status(status):
    log.info("get_next_step: %s", status)

    next_status = None

    # this could be part of the config file
    status_dict = {
        # assembly
        "start" : "combine_fastq",
        "combine_fastq" : "contam_id",
        "contam_id" : "subsample",
        "subsample" : "assembly",
        "assembly" : "contig_trim",
        "contig_trim" : "contig_gc_plot",

        # reporting
        "contig_gc_plot" : "gc_cov",
        "gc_cov" : "blast",
        "blast" : "blast_16s",
        "blast_16s" : "gc_hist",
        #"gc_hist" : "gc_hist_reads",
        #"gc_hist_reads" : "ncbi_prescreen",
        "gc_hist" : "ncbi_prescreen",
        "ncbi_prescreen" : "pct_reads_asm",
        "pct_reads_asm" : "checkm",
        "checkm" : "tetramer",
        "tetramer" : "antifam",
        "antifam" : "qual",
        "qual" : "pdf_report",

        # pdf, txt, metadata.json
        "pdf_report" : "purge",

        "purge" : "complete",

        "failed" : "failed",
        "complete" : "complete",
    }


    if status:

        if status.lower() != "complete":
            status = status.lower().replace("skipped", "").replace("complete", "").strip()

        if status.endswith("failed"):
            next_status = "failed"

        if status in status_dict:
            next_status = status_dict[status]

        else:
            if status.endswith("failed"):
                next_status = "failed"

            else:
                next_status = "failed: next step missing for '%s'" % (status)
                log.error("Cannot get the next step for '%s'  %s", status, msg_fail)
                sys.exit(12)



    log.info("- next step: %s%s%s -> %s%s%s", color['pink'], status, color[''], color['cyan'], next_status, color[''])

    if stop_flag:
        next_status = "stop"

    # debugging
    if next_status == "stop":
        log.info("- stopping pipeline, stop flag is set")
        sys.exit(4)


    return next_status


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "ISO"
    version = "1.3.4"



    # path names
    CONTAM_ID_PATH = "contam"
    SUBSAMPLE_PATH = "subsample"
    ASM_PATH = "asm"
    CONTIG_TRIM_PATH = "trim"
    TETRAMER_PATH = "tetramer"
    SINGLE_COPY_GENE_PATH = "single_copy_gene"
    CHECKM_PATH = "checkm"
    PCT_READS_ASM_PATH = "pct_reads_assembled"
    NCBI_PRESCREEN_PATH = "ncbi_prescreen"
    GC_HIST_PATH = "gc_hist"
    GC_HIST_READ_PATH = "gc_hist_reads"
    BLAST_PATH = "megablast"
    BLAST_16S_PATH = "blast_16s"
    GC_COV_PATH = "gc_cov"
    CONTIG_GC_PATH = "contig_gc"
    ANTIFAM_PATH = "antifam"
    QUAL_PATH = "qual"
    REPORT_PATH = "report"


    DEFAULT_CONFIG_FILE = os.path.realpath(os.path.join(my_path, "../sag/", "config", "iso.cfg"))


    uname = getpass.getuser() # get the user, brycef can't use scell.p, but qc_user can!


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)



    # Parse options
    usage = "iso.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
Steps: (for --step option)
  contam_id, subsample, assembly, contig_trim,
  contig_gc_plot, gc_cov, blast, blast_16s, gc_hist, gc_hist_reads, ncbi_prescreen, pct_reads_asm, checkm, tetramer
  pdf_report
add "_stop" to run that step and stop.  e.g. (./iso.py --step blast_stop)
    """

    parser = ArgumentParser(usage = usage)


    parser.add_argument("-f", "--fastq", dest="fastq", help = "Input fastqs (required) - comma separated values")
    parser.add_argument("-l", "--lib", dest="library_name", help = "Library name (required)")
    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-c", "--config", dest="config_file", help = "Location of sag.cfg file to use")
    parser.add_argument("-s", "--step", dest="step", help = "Step to start running on")
    # remove or comment out ... 
    parser.add_argument("-x", "--opt", dest="opt", help = "Bryce testing options")
    parser.add_argument("-org", "--org-name", dest="org_name", help = "Organism name (optional), use this instead of whats in the library_info.tax* fields for ProDeGe")
    parser.add_argument("--fasta", dest="input_fasta", help = "Input fasta (optional)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)


    status = "start" # step + X = run step and exit?  (e.g. gc_covX)  -- to do
    output_path = None
    fastq = None
    input_fasta = None
    library_name = None
    org_name = None # organism name
    config_file = None
    stop_flag = False # stop after running a step, e.g. gc_cov_stop
    fastq_list = []
    my_opt = "" # testing for Bryce, not in use

    # parse args
    args = parser.parse_args()

    print_log = args.print_log

    if args.input_fasta:
        input_fasta = args.input_fasta

    if args.output_path:
        output_path = args.output_path

    if args.fastq:
        fastq_list = re.split('[;,]', str(args.fastq).replace(" ",""))

    if args.opt:
        my_opt = args.opt
        
    if args.library_name:
        library_name = args.library_name

    if args.org_name:
        org_name = args.org_name



    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/iso"

        if output_path == "t1":

            fastq_list = ["/global/projectb/sandbox/rqc/analysis/qc_user/iso/10459.1.161930.GGTAG.anqdpht.fastq.gz"]
            library_name = "AZNWT"

        if output_path == "t2":

            fastq_list = ["/global/projectb/sandbox/rqc/analysis/qc_user/iso/10430.1.160161.ATCAC.anqdpht.fastq.gz"]
            library_name = "AZNWU"

        if output_path == "t3":

            fastq_list = ["/global/dna/shared/rqc/filtered_seq_unit/00/01/19/84/11984.1.231679.TTAGG.filter-ISO.fastq.gz"]
            library_name = "CBWBP"

        if output_path == "t4":
            fastq_list = ["/global/projectb/sandbox/rqc/analysis/qc_user/iso/10255.6.154484.ATCTAT.anqdpht.fastq.gz"]
            library_name = "AXNGH"

        if output_path == "t5":
            #/global/dna/dm_archive/rqc/filtered_seq_unit/00/01/07/11/

            #fastq_list = ['/global/dna/dm_archive/rqc/filtered_seq_unit/00/01/07/11/10711.6.175781.GTAACGA-GTCGTTA.anqdpht.fastq.gz',
            #              '/global/dna/dm_archive/rqc/filtered_seq_unit/00/01/07/89/10789.2.179402.GTAACGA-GTCGTTA.anqdpht.fastq.gz']
            #library_name = "BBOSU"

            # not filtered correctly but it was an R&D library ...
            fastq_list = ['/global/dna/dm_archive/rqc/analyses/AUTO-63379/11456.8.207797.CCGTCC.filter-DNA.fastq.gz']
            library_name = "BTHZO"


        output_path = os.path.join(test_path, library_name)


    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)




    config_file = os.path.join(output_path, "iso.cfg")

    if args.config_file:
        config_file = args.config_file



    # initialize my logger
    log_file = os.path.join(output_path, "iso.log")



    # log = logging object
    log_level = "INFO"
    log = get_logger("iso", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "fastq", fastq_list)
    if input_fasta:
        log.info("%25s      %s", "fasta", input_fasta)
    log.info("%25s      %s", "library_name", library_name)
    if org_name:
        log.info("%25s      %s", "org_name", org_name)

    log.info("%25s      %s", "step", status)
    log.info("%25s      %s", "config_file", config_file)
    if my_opt:
        log.info("%25s      %s", "my_opt", my_opt)
    log.info("")


    if not fastq_list:
        print "Error: no fastq specified!  %s" % msg_fail
        log.error("Error: no fastq specified!  %s", msg_fail)
        sys.exit(2)

    for f in fastq_list:
        if not os.path.isfile(f):
            print "Error: cannot find fastq %s  %s" % (f, msg_fail)
            log.error("Error: cannot find fastq %s  %s", f, msg_fail)
            sys.exit(2)


    if input_fasta:
        if not os.path.isfile(input_fasta):
            print "Error: input fasta not on file system: %s  %s" % (input_fasta, msg_fail)
            log.error("Error: input fasta not on file system: %s  %s", input_fasta, msg_fail)
            sys.exit(2)

    if not os.path.isfile(config_file):
        log.info("- cannot find config file: %s  %s", config_file, msg_warn)
        log.info("- using default config file.")
        cmd = "cp %s %s" % (DEFAULT_CONFIG_FILE, config_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)




    status_log = os.path.join(output_path, "status.log")
    rqc_file_log = os.path.join(output_path, "rqc-files.tmp")
    rqc_stats_log = os.path.join(output_path, "rqc-stats.tmp")

    if org_name:
        append_rqc_stats(rqc_stats_log, "org_name", org_name)


    file_dict = {} # dictionary of file type and locations

    # test
    #get_ssu("AUHTB", "AUHTB_16s.txt")
    #get_ssu(library_name, "%s_16s.txt" % library_name)
    #fastq = combine_fastq(fastq_list)
    #x = get_seq_unit_info(fastq_list, log)
    #for su in x:
    #    print su
    #sys.exit(18)

    # set config dict with values from iso.cfg
    config = load_config_file(config_file, log)





    checkpoint_step(status_log, "## New Run")




    # gunzip files so we don't reprocess, rename *.txt for stats & files to *.tmp
    setup_run(output_path, log)

    fasta_type = ""
    if status == "start":
        #save_file(fastq, "input_fastq", file_dict, output_path)
        get_bbtools_version(rqc_stats_log, output_path, log)



    if args.step:
        status = args.step
        fasta_type = "scaffolds_trim"
        if status.endswith("_stop"):
            status = status.replace("_stop", "")
            stop_flag = True

    iso_path = os.path.join(output_path, "iso")

    done = False
    cycle_cnt = 0
    while not done:

        cycle_cnt += 1
        if status in ("start", "combine_fastq"):
            status = combine_fastq(fastq_list)

        # assembly -> contig trimming
        if status == "contam_id":
            status = do_contam_id()

        if status == "subsample":
            status = do_subsample()
            fastq_type = "subsampled_fastq"

        if status == "assembly":
            status = do_assembly(library_name)
            fasta = get_file("scaffolds", file_dict, output_path, log)

        if status == "contig_trim":
            status = do_contig_trim(library_name)
            fasta_type = "scaffolds_trim"

        # QC Analysis

        if status == "contig_gc_plot":
            status = do_contig_gc_plot(iso_path, fasta_type, library_name)


        if status == "gc_cov":
            status = do_gc_cov(iso_path, fasta_type, library_name)


        if status == "blast":
            status = do_blast(iso_path, fasta_type)
            is_right_thing(iso_path, library_name)

        if status == "blast_16s":
            status = do_blast_16s(iso_path, fasta_type, library_name)

        if status == "gc_hist":
            status = do_gc_hist(iso_path, fasta_type, library_name)

        if status == "gc_hist_reads":
            status = do_gc_hist_reads(iso_path, library_name) # uses fastq

        if status == "ncbi_prescreen":
            status = do_ncbi_prescreen(iso_path, fasta_type)

        if status == "checkm":
            status = do_checkm(iso_path, fasta_type)

        if status == "pct_reads_asm":
            status = do_pct_reads_asm(iso_path, fasta_type)

        if status == "tetramer":
            status = do_tetramer(iso_path, fasta_type)

        if status == "antifam":
            status = do_antifam(iso_path, fasta_type)

        if status == "qual":
            status = do_qual(iso_path, fasta_type)

        # Reports: pdf report does the *.txt, *.pdf, *.metadata.json
        if status in ("pdf_report", "pdf"):
            status = do_pdf_report(library_name, fasta_type, org_name, fastq_list)



        if status == "purge":
            #do_final() # Kecia's final folder
            status = do_purge()

        if status == "complete":
            done = True

        if status == "failed":
            done = True

        if cycle_cnt > 88:
            log.info("- max cycles reached.  %s", msg_warn)
            done = True



        status = get_next_status(status)





    checkpoint_step(status_log, status)
    if status == "complete":

        # move rqc-files.tmp to rqc-files.txt
        append_rqc_stats(rqc_stats_log, "iso_version", "%s %s" % (my_name, version))
        rqc_new_file_log = os.path.join(output_path, "rqc-files.txt")

        cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        rqc_new_stats_log = os.path.join(output_path, "rqc-stats.txt")

        cmd = "mv %s %s" % (rqc_stats_log, rqc_new_stats_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    log.info("Completed %s", script_name)

    sys.exit(0)



"""
                      .-.
         .-._    _.../   `,    _.-.
         |   `'-'    \     \_'`   |
         \            '.__,/ `\_.--,
          /                '._/     |
         /                    '.    /
        ;   _                  _'--;
     '--|- (_)       __       (_) -|--'
     .--|-          (__)          -|--.
      .-\-                        -/-.
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`

"Eat a live frog every morning, and nothing worse will happen to you the rest of the day." - Markus Twain
-----


If you run the Iso-? on an existing run folder, it will skip each step if it finds that it ran that step.  To re-run a step you should rename/mv the folder for that step.  The steps are listed in the $ sag.py --help options

The normal run folder structure is:
- asm = spades assembly with full spades assembly
- contam = contam identification
- normalize = normalization with normalized fastq
- subsample = subsample step with subsampled fastq
- trim = trimmed fasta (scaffolds.2k.fasta)
- unscreen = analysis & reporting for unscreened assembly (trimmed fasta)
- report = pdf, txt reports for screened and unscreened assembly

The main folder has:
 * iso.cfg = configuration file for SAG-omizer
 * iso.log = run log for SAG-omizer
 * status.log = step status (picked up by RQC for reporting, knowing when pipeline is failed or complete)
 * rqc-files.txt = files needed for reporting in RQC
 * rqc-stats.txt = stats needed for reporting in RQC
 * unscreen.metadata.json = metadata for JAT submissions
 * file_list.txt, bbtools.txt, spades.txt are misc files for storing temporary data

Stuff to save:
- command line for seal + options
- Iso-omizer version
- assembly - spades version & options
- LAST version for gc histograms
- blast - version + cmdline
- tetramer version + cmdline
- bbtools version

"""
