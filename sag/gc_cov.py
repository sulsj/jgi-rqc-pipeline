#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
GC_Cov
- for ISO, CE, SPS, SAG, PBMID, Fungal Min and Fungal Improved Draft pipelines
- does the cloud plots (gc vs coverage and shows hits of shreds to nt, also creates frag vs cov & gc plot)

~/git/jgi-rqc-pipeline/sag/gc_cov.py -o t1
t1 = BSHTN = 12.38 minutes (2x speed up!)
gcCovTaxPacbio.py = 25.36 minutes
t2 = BWHNN - pacbio - 21.42 minutes
gcCovTaxPacbio.py = 37.68 minutes

Dependencies
bbtools (shred.sh, bbmap.sh) + tax server
blast+
gnuplot (in bbtools container)

v 1.0: 2017-05-03
- initial version
- works with genepool & denovo

v 1.1: 2017-05-22
- fix if no hits (BUCSN - SPS)
- order the plots by the taxonomies with the most contig hits
- include summary.txt (RQCSUPPORT-1318)

v 1.2: 2017-06-05
- updated bbmap parameters to work for either Illumina or Pacbio data

v 1.2.1: 2017-06-08
- handle bug in bbtools not including coverage for all contigs

v 1.2.2: 2017-06-12
- changed run_blast.py to run_blastplus_taxserver.py

v 1.2.3: 2017-06-14
- bug fix if nothing found for green genes

v 1.2.4: 2017-06-26
- bug fix if gg has kingdom and nt has kingdom then gg overwrites nt plotting

v 1.2.5: 2017-07-25
- bug fix for green genes looking up wrong taxonomy (RQCSUPPORT-1565)

v 1.2.6: 2017-08-31
- bug fix for green genes looking up taxonomy (RQCSUPPORT-1689)

v 2.0: 2017-11-15
- updated to use pandas
- updated to include fungal requirements (gccoverage.txt, new blast databases)
- changed how parse blast tophit files with pandas
- updated to use mapTaxPacbio.sh for fungal mode
* 22 hours for fungalicious
- look up SSU, green genes hits with get_taxonomy_custom
- minimap test: https://github.com/lh3/minimap2
- changed green-genes to SSU for normal operations

v 2.1: 2017-11-20
- use minimap2 option, remove intermediate sam file

v 2.2: 2018-01-26
- fail if blast fails
- fix for int lookup in dictionary (PBMID failures)

v 2.2.1: 2018-01-29
- added -gccov flag to generate gccovstats.txt (map_coverage) - takes longer to do another mapping

v 2.2.2: 2018-02-05
- ssu taxonomy lookup bug fix (RQCSUPPORT-2334)

v 2.2.3: 2018-02-08
- ssu - don't count anything less than 600 bp (RQC-1068)

v 2.2.4 - 2018-02-09
- fix for looking up refseq.* hits for fungal

v 2.2.5 - 2018-02-20
- fix for coverage stats (RQCSUPPORT-2381)

v 2.3 - 2018-04-13
- use sketch (refseq) rather than blast, new do_sketch and summarize_sketch functions (shred size = 10000bp)
- works with refseq,silva,nt - but only refseq is on - nt or refseq needs different marker in gnuplot if we turn it on
- if mode = fungal then minimap_flag = True

v 2.3.1 - 2018-07-23
- changed path to test files to sandbox
- added plot of contig length vs gc vs coverage (Kurt request)


To do:
- Kurt thinks there is a bug with the contig vs gc % and coverage because its not ordered from largest contig to smallest
--- this comes from the original order of the contigs from the assembly (2018-02-28) - arrow had it this way
- 0's in cov_stats causes mode = 0.0 for fungal projects (2018-07-25)

- use Brian's smaller refseq.bacteria for fungal
/global/projectb/sandbox/gaag/bbtools/refseqMicrobial/nov16_2017/bacteria_98.fa.gz
The new file is in the same folder (/global/projectb/sandbox/gaag/bbtools/refseqMicrobial/nov16_2017) and is called bacteria_98_clumped.fa.gz
$ cd /global/dna/shared/rqc/ref_databases/ncbi/test
$ unpigz -c /global/projectb/sandbox/gaag/bbtools/refseqMicrobial/nov16_2017/bacteria_98.fa.gz > bacteria_98.fasta
shifter --image=sulsj/ncbi-blastplus:2.6.0 makeblastdb -dbtype nucl -in bacteria_98.fasta




./minimap -t 16 /global/projectb/scratch/brycef/gc_cov/BSSZZ.ref.fasta /global/projectb/scratch/brycef/gc_cov/BSSZZ/shred/BSSZZ.shred.fasta > out.mini
* 124 sec
query name, length, 0-based start, end, strand, target name, length of target, start, end, the number of matching bases, the number of co-linear minimizers in the match
and the fraction of matching bases.
Segkk0|arrow_0-4999     5000    51      4938    -       m54014_170606_195703/55771296/16704_25408       8704    2155    7177    1600    5022    255     cm:i:174
Segkk0|arrow_0-4999     5000    1885    4972    +       m54014_170609_224739/7733571/14397_17926        3529    66      3227    1367    3161    255     cm:i:171
Segkk0|arrow_0-4999     5000    8       4961    +       m54014_170609_224739/37683690/4968_16298        11330   709     5685    1349    4976    255     cm:i:151
Segkk0|arrow_0-4999     5000    490     4893    +       m54014_170609_224739/43450942/10231_17340       7109    145     4532    1239    4403    255     cm:i:140
Segkk0|arrow_0-4999     5000    8       4984    -       m54014_170606_195703/55771296/0_7889    7889    1350    6418    1385    5068    255     cm:i:138


# T1 - isolate test
qsub -b yes -j yes -m as -now no -w e -N GC_COV1 -l ram.c=120G,h_rt=86400 -P gentech-rqc.p -o /global/projectb/scratch/brycef/gc_cov/t1.log -js 515 /global/homes/b/brycef/git/jgi-rqc-pipeline/sag/gc_cov2.py -o t1



# Fungalicious test: 24 hours to run
qsub -b yes -j yes -m as -now no -w e -N GC_COV3 -l ram.c=120G,h_rt=43100 -P gentech-rqc.p -o /global/projectb/scratch/brycef/gc_cov/t3.log -js 535 /global/homes/b/brycef/git/jgi-rqc-pipeline/sag/gc_cov2.py -o t3.3

# T4 with sketch:
~/git/jgi-rqc-pipeline/sag/gc_cov2.py -o t4 -pl
11 minutes

# T1 with sketch = 1 minute
- results have more hits to more things, did not test vs silva


"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import glob
import getpass
import re
#import numpy
from scipy import stats
import pandas as pd

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, run_cmd, get_colors, get_msg_settings
from micro_lib import get_the_path
from taxonomy import get_taxonomy

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Shred assembly into 5000bp chunks

'''
def shred_fasta(fasta, shred_length):

    log.info("shred_fasta: %s, %s bp", fasta, shred_length)
    the_path = get_the_path("shred", output_path)

    shred_fasta = os.path.join(the_path, os.path.basename(fasta).replace(".fasta", ".shred.fasta"))
    exit_code = 0

    if os.path.isfile(shred_fasta):
        log.info("- skipping shred, found file: %s", shred_fasta)
    else:

        shred_log = os.path.join(the_path, "shred.log")
        cmd = "#bbtools;shred.sh in=%s out=%s length=%s ow=t > %s 2>&1" % (fasta, shred_fasta, shred_length, shred_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    return shred_fasta


'''
Get coverage for each shred from the fastq file
- also includes gc%
~ 3 hours fungal
'''
def map_shred_coverage(shred_fasta, fastq, mode):
    log.info("map_shred_coverage: %s vs %s (%s)", shred_fasta, fastq, mode)

    the_path = get_the_path("shred", output_path)
    cov_file = os.path.join(the_path, "coverage.txt")

    if os.path.isfile(cov_file):
        log.info("- skipping mapping, found file: %s", cov_file)
    else:

        cov_log = os.path.join(the_path, "cov.log")

        if mode in ["pacbio", "fungal"]:

            if minimap_flag:

                # 3 minutes
                sam_file = os.path.join(the_path, "shred.sam")

                cmd = "%s -t 16 -a -x map-pb %s %s > %s" % (minimap2_cmd, shred_fasta, fastq, sam_file)
                std_out, std_err, exit_code = run_cmd(cmd, log)

                pileup_log = os.path.join(the_path, "pileup.log")
                # need shred_fasta to get ref_gc
                cmd = "#bbtools;pileup.sh ow=t in=%s out=%s ref=%s > %s 2>&1" % (sam_file, cov_file, shred_fasta, pileup_log)
                std_out, std_err, exit_code = run_cmd(cmd, log)

                # sam file is big and we don't need it
                cmd = "rm %s" % sam_file
                std_out, std_err, exit_code = run_cmd(cmd, log)

            else:
                # 3 hours
                bbmap_param = "ow=t nodisk ambig=random maxindel=100 fastareadlen=500 build=1 maxlength=1000"
                cmd = "#bbtools;mapPacBio.sh in=%s ref=%s path=%s covstats=%s %s > %s 2>&1"
                cmd = cmd % (fastq, shred_fasta, output_path, cov_file, bbmap_param, cov_log)
                std_out, std_err, exit_code = run_cmd(cmd, log)


        else:
            bbmap_param = "ow=t nodisk ambig=random maxindel=100 minscaf=200 build=1"

            cmd = "#bbtools;bbmap.sh in=%s ref=%s path=%s covstats=%s %s > %s 2>&1"
            cmd = cmd % (fastq, shred_fasta, the_path, cov_file, bbmap_param, cov_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)


'''
want to map input reads to the final assembly
- fungal option, creates gccovstats.txt
3 hours?
fungal CNCAB (RQC-1103)
- minimap2 takes 3 minutes, bbtools.pileup takes 22 seconds
- using arrow/aligned.bam takes 0 minutes, bbtools.pileup takes 3 minutes
-- results different too because aligned.bam using pre-polished assembly

'''
def map_coverage(fasta, fastq, mode):
    log.info("map_coverage: %s vs %s (%s)", fasta, fastq, mode)

    cov_file = os.path.join(output_path, "full_coverage_stats.txt")

    if os.path.isfile(cov_file):
        log.info("- skipping mapping, found file: %s", cov_file)
    else:

        cov_log = os.path.join(output_path, "full_cov.log")


        if mode in ["pacbio", "fungal"]:

            if minimap_flag:

                # 3 minutes
                sam_file = os.path.join(output_path, "full.sam")

                # use input bam file?

                cmd = "%s -t 16 -a -x map-pb %s %s > %s" % (minimap2_cmd, fasta, fastq, sam_file)
                std_out, std_err, exit_code = run_cmd(cmd, log)

                pileup_log = os.path.join(output_path, "pileup-full.log")
                cmd = "#bbtools;pileup.sh ow=t in=%s out=%s ref=%s > %s 2>&1" % (sam_file, cov_file, fasta, pileup_log)
                std_out, std_err, exit_code = run_cmd(cmd, log)

                # sam file is big and we don't need it
                cmd = "rm %s" % sam_file
                std_out, std_err, exit_code = run_cmd(cmd, log)

            else:

                bbmap_param = "ow=t nodisk ambig=random maxindel=100 fastareadlen=500 build=1 maxlength=1000"
                # maxlength reduces the all-to-all comparison from 5000^2 to 1000^2 - should speed up the mapping
                cmd = "#bbtools;mapPacBio.sh in=%s ref=%s path=%s covstats=%s %s > %s 2>&1"
                cmd = cmd % (fastq, fasta, output_path, cov_file, bbmap_param, cov_log)
                std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            bbmap_param = "ow=t nodisk ambig=random maxindel=100 minscaf=200 build=1"

            cmd = "#bbtools;bbmap.sh in=%s ref=%s path=%s covstats=%s %s > %s 2>&1"
            cmd = cmd % (fastq, fasta, output_path, cov_file, bbmap_param, cov_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)


    tbl = pd.read_table(cov_file, sep = "\t")
    t2 = tbl[['#ID', 'Avg_fold', 'Length', 'Ref_GC']].copy() # just get these 4 columns
    t2.rename(columns={'Avg_fold' : 'Coverage'}, inplace=True) # rename columns

    gc_cov_file = os.path.join(output_path, "gccovstats.txt")
    t2.to_csv(gc_cov_file, sep='\t', index=False)



'''
Create a file called covstats.txt:
avg_cov=147.135285914
median_cov=146.0
std_dev=35.8329433458
mode=147.0

'''
def cov_stats():
    log.info("cov_stats")
    the_path = get_the_path("shred", output_path)
    cov_file = os.path.join(the_path, "coverage.txt")

    cov_stats = os.path.join(output_path, "covstats.txt")
    if os.path.isfile(cov_stats) and 1==2:
        log.info("- skipping cov_stats, found file: %s", cov_stats)
    else:

        tbl = pd.read_table(cov_file, sep="\t")

        # COHYT - filter out 0's?

        #my_col = "ChunkCov"
        my_col = "Avg_fold"
        avg_cov = tbl[my_col].mean()
        median_cov = tbl[my_col].median()
        std_dev_cov = tbl[my_col].std()
        cov_min = tbl[my_col].min()
        cov_max = tbl[my_col].max()

        cov_list = [] # list to calc stats

        fh = open(cov_file, "r")
        for line in fh:
            if line.startswith("#"):
                continue
            arr = line.split()
            cov_list.append(float(arr[1]))
        fh.close()

        
        #avg_cov = numpy.average(cov_list)
        #median_cov = numpy.median(cov_list)
        #std_dev_cov  = numpy.std(cov_list)
        mode_cov = stats.mode(cov_list)[0][0]
        #cov_min = numpy.min(cov_list)
        #cov_max = numpy.max(cov_list)

        
        log.info("- avg: %s", avg_cov)
        log.info("- median: %s", median_cov)
        log.info("- std_dev: %s", std_dev_cov)
        log.info("- mode: %s", mode_cov)
        log.info("- min: %s, max: %s", cov_min, cov_max)

        # needed by pipelines
        fh = open(cov_stats, "w")
        fh.write("avg_cov=%s\n" % avg_cov)
        fh.write("median_cov=%s\n" % median_cov)
        fh.write("std_dev=%s\n" % std_dev_cov)
        fh.write("mode=%s\n" % mode_cov)
        fh.write("min=%s\n" % cov_min)
        fh.write("max=%s\n" % cov_max)
        fh.close()
        log.info("- wrote: %s", cov_stats)


'''
Use nt, green genes to blast contigs
~ 10 minutes for nt

fungal blast hit:
curl taxonomy.jgi-psf.org/tax/accession/NT_165936.1 -> Aspergillus terreus

fungi funky format, some spaces some tabs
perl -pe 's/\ /\t/g' megablast.BSSZZ.shred.fasta.vs.refseq.fungi.parsed.tophit > th2.txt

fungi: (normally don't use nt)
nt = 50 minutes
refseq.archaea = 1 minute
refseq.bacteria = 16 hours
fungi = 2 minutes


'''
def blast_shreds(shred_fasta):
    log.info("blast_shreds")


    the_path = get_the_path("shred", output_path)

    for blast_db in blast_list:
        log.info("- blasting against %s", blast_db)

        blast_log = os.path.join(the_path, "blast-%s.log" % blast_db)
        blast_done = os.path.join(the_path, "blast-%s.done" % blast_db)

        if os.path.isfile(blast_done):
            log.info("- skipping blast %s, found file: %s", blast_db, blast_done)
        else:

            #old: '-task megablast -perc_identity 90 -evalue 1e-30 -dust yes -num_threads 16'
            #run_blastplus: evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis
            #-dust yes -soft_masking true -num_alignments 5  -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles'   -num_threads 8
            # defaults to 8 threads

            # might need different params for ssu - RQC-1068, using min length for now

            cmd = "%s -o %s -q %s -d %s > %s" % (blast_cmd, the_path, shred_fasta, blast_db, blast_log)
            # removed -s to show lineage, we will do this ourselves later
            std_out, std_err, exit_code = run_cmd(cmd, log)

            if exit_code == 0:
                fh = open(blast_done, "w")
                fh.write("Blast for %s is done\n" % blast_db)
                fh.close()
            else:
                log.error("- blast failed!  %s", msg_fail)
                sys.exit(exit_code)


'''
Create one file for kingdom, phylum... for each blast type
format:
#GC  [org name] tab [org name]
- cloud plots
- order [org name] by number of hits desc

returns number of failures to create gnuplot


'''
def summarize_hits():
    log.info("summarize_hits")


    min_pct_id = 1.0 # min percent identity from blast hit (1.0 = 1%)
    the_path = get_the_path("shred", output_path)
    gnu_path = get_the_path("gnu", output_path)
    #sketch_path = get_the_path("sketch", output_path)

    contig_dict = {}
    hit_dict = {} # hits[blast_type] = [list of taxonomies to look up]



    gg = re.compile(r'\d+:\d+_(.*)_1\.\.\d+') # special green genes matching

    # for each *.parsed.tophit, store the contig with the best hit
    tophit_list = glob.glob(os.path.join(the_path, "*.parsed.tophit"))

    # error: tophit missing ...
    for tophit_file in tophit_list:


        blast_type = "nt"
        #for bt in ['green_genes16s', 'archaea', 'bacteria', 'fungi', 'SSURef_tax_silva']:
        for bt in ['green_genes16s', 'refseq.archaea', 'refseq.bacteria', 'refseq.fungi', 'SSURef_tax_silva']:
            if bt in tophit_file:
                blast_type = bt


        # force tophit file to be tab separated
        panda_tophit_file = os.path.join(the_path, "%s.tophit" % (blast_type))
        cmd = "perl -pe 's/\ /\t/g' %s > %s" % (tophit_file, panda_tophit_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        tbl = pd.read_table(panda_tophit_file, sep="\t")
        log.info("- processing %s", blast_type)

        if blast_type == "SSURef_tax_silva":
            blast_type = "ssu"
        if blast_type == "green_genes16s":
            blast_type = "green-genes"



        hit_dict[blast_type] = []

        # list of unique tax ids once we know the "best" hits
        for index, row in tbl.iterrows():
            #print row
            pct_id = 0.0
            try:
                pct_id = float(row['perc_id'])
            except:
                pct_id = 0.0

            good_hit = False
            hit_length = int(row['length'])



            if pct_id > min_pct_id:
                good_hit = True

            # RQC-1068 - ignore hits to ssu under 600bp
            if blast_type == 'ssu':
                good_hit = False
                if hit_length > 600:
                    good_hit = True


            if good_hit:

                taxon = str(row['subject'])
                contig = row['#query']

                tax_id = "" # 0 or other value like "NW_001884682.1"
                #curl taxonomy.jgi-psf.org/tax/accession/NW_001884682.1

                if blast_type == "green-genes":
                    m = gg.match(taxon)

                    if m:
                        tax_id = m.group(1)
                    else:
                        tax_id = taxon.split(":")[0]
                    if tax_id:
                        tax_id = "gg%s" % tax_id

                else:
                    if taxon.startswith("gi|"):
                        tax_id = taxon.split("|")[1]

                    # ref|NZ_MUAR01000203.1| = refseq.bacteria, refseq.fungi
                    elif taxon.startswith("ref|"):
                        tax_id = taxon.split("|")[1]
                    elif taxon.startswith("tid|"):
                        # ssu = tid:%s
                        tax_id = "tid:%s" % taxon.split("|")[1]
                    else:
                        tax_id = taxon

                if tax_id:
                    #print tax_id, contig, blast_type
                    if contig in contig_dict:
                        contig_dict[contig][blast_type] = tax_id
                    else:
                        contig_dict[contig] = { blast_type : tax_id }
                    #if tax_id not in hits[blast_type]['tax_id']:
                    hit_dict[blast_type].append(tax_id)




    # look up taxonomy for each unique hit in the hit_dict for the blast_type

    tax_lookup = {}
    for blast_type in hit_dict:
        tax_file = os.path.join(the_path, "%s-tax.txt" % blast_type)

        fh = open(tax_file, "w")
        fh.write("#Tax ID|Taxonomy (%s)\n" % (";".join(tax_list)))

        tax_ids = list(set(hit_dict[blast_type]))
        tax_lookup[blast_type] = {}
        # { tax_id: { "superkingdom" : "Eukaryota", "phylum" : "Ascomycota" ... }
        for tax_id in tax_ids:
            taxonomy = "" # look up taxonomy using taxonomy lib

            td = {}
            #print tax_id
            if tax_id.isdigit():
                td = get_taxonomy(tax_id, "gi")
            elif tax_id.startswith("N"):
                td = get_taxonomy(tax_id, "acc")
            elif tax_id.startswith("SSU"):
                td = get_taxonomy_custom(tax_id, "ssu") # custom function in gc_cov
            elif tax_id.startswith("tid:"):
                #tid:1488
                my_tax_id = tax_id.split(":")[1]
                td = get_taxonomy(my_tax_id, "tax_id")

            elif tax_id.startswith("gg"):
                td = get_taxonomy_custom(tax_id, "green-genes") # custom function in gc_cov
            elif tax_id.startswith("tax"):
                #tax_id = tax_id.replace("tax", "")
                td = get_taxonomy(tax_id.replace("tax", ""), "")
            else:
                td = get_taxonomy(tax_id, "name")

            for t in tax_list:
                if t in td:
                    taxonomy += td[t] + ";"
                else:
                    taxonomy += "-;"

            #taxonomy = td
            tax_lookup[blast_type][tax_id] = td

            fh.write("%s|%s\n" % (tax_id, taxonomy))
        fh.close()
        log.info("- created: %s", tax_file)

    #for c in contig_dict:
    #    if 'ssu' in contig_dict[c]:
    #        print contig_dict[c]['ssu']
    #print contig_dict #[contig][blast_type]

    #sys.exit(44)

    # create new panda table with subset of fields from coverage + field for each
    cov = os.path.join(the_path, "coverage.txt")
    tbl = pd.read_table(cov, sep="\t")
    tbl['Ref_GC'] = tbl['Ref_GC'] * 100

    t2 = tbl[['#ID', 'Length', 'Ref_GC', 'Avg_fold']].copy() # just get these 4 columns, would like contig length too
    # rename columns
    t2.rename(columns={'Avg_fold' : 'Chunk_Coverage', '#ID' : "# Chunk_Name", 'Length' : 'Chunk_Length', 'Ref_GC' : "Chunk_GC"}, inplace=True)


    # add new columns for each blast_type containing the tax_id for each blast_type
    for blast_type in sorted(hit_dict.iterkeys()):

        t2[blast_type] = "-"

        for index, row in tbl.iterrows():

            tax_id = "-"

            if row['#ID'] in contig_dict:
                #print "** %s" % contig_dict[row['#ID']]

                if blast_type in contig_dict[row['#ID']]:
                    tax_id = contig_dict[row['#ID']][blast_type]

            t2.set_value(index, blast_type, tax_id)

    summary_file = os.path.join(output_path, "summary.txt")
    # Chunk_Name  Chunk_Length  Contig_Length  Chunk_GC Chunk_Cov tax-($key type)
    t2.to_csv(summary_file, sep='\t', index=False)



    # create one file of no hits used for each plot - grey cloud
    no_hit_file = os.path.join(the_path, "no_hit.txt")
    fh = open(no_hit_file, "w")
    fh.write("#GC\tNo Hit\n")
    for index, row in tbl.iterrows():
        gc = row['Ref_GC']
        cov = row['Avg_fold']
        my_buffer = "%s\t%s\n" % (gc, cov)
        fh.write(my_buffer)

    fh.close()
    log.info("- created: %s", no_hit_file)


    summary = os.path.join(output_path, "summary.txt")
    tbl = pd.read_table(summary, sep="\t")


    # reset contents of superkingdom-fields.txt ...
    for t in tax_list:
        field_file = os.path.join(gnu_path, "%s-fields.txt" % t)
        fh2 = open(field_file, "w")
        fh2.write("# file_name|blast_db|tax name|plot order|hits\n")
        fh2.close()

    #print tax_lookup['nt']
    #print tax_lookup['nt']['336293787']

    #sys.exit(44)


    for blast_type in blast_list:

        log.info("- summarizing: %s", blast_type)

        # get unique hits for each db at each level

        # unique taxonomy at each level
        tax_sum = {} # tax_sum['superkingdom'] = ["Eukaryota","Archaea"]

        for index, row in tbl.iterrows():
            #print row
            # # Chunk_Name       CCUZU_unitig_0|arrow_15000-19999
            tax_id = row[blast_type]

            if tax_id != "-":
                #tax_id = str(tax_id)
                for t in tax_list:

                    tax_info = "-"
                    tax_id_str = str(tax_id)
                    if tax_id_str in tax_lookup[blast_type]:
                        tax_info = tax_lookup[blast_type][tax_id_str].get(t)


                    if t in tax_sum:
                        tax_sum[t].append(tax_info)
                    else:
                        tax_sum[t] = [tax_info]
                    #print tax_id, tax_lookup[blast_type][tax_id]


        # write to file
        # gc  [org 1]  [org 2] ... [org n]
        # 32.22  cov - - ... -
        # 33.33  - cov - ... -
        #print tax_sum

        for t in tax_list:
            gc_file = os.path.join(the_path, "%s-%s.txt" % (blast_type, t))

            fh = open(gc_file, "w")


            my_header = "#GC\t"
            #GC Dothideomycetes Xylonomycetes Leotiomycetes Eurotiomycete
            org_tax = []

            if t in tax_sum:

                if len(tax_sum[t]) > 0:
                    org_tax = list(set(tax_sum[t]))

                    # should sort by number of hits
                    org_tax.sort()
                    my_header += "\t".join(org_tax)

                    fh.write(my_header + "\n")


            # for each row, look up the taxonomy and write the level
            for index, row in tbl.iterrows():

                tax_id = str(row[blast_type])

                if tax_id != "-":

                    cov = row['Chunk_Coverage']
                    tax_info = "-"
                    if tax_id in tax_lookup[blast_type]:
                        tax_info = tax_lookup[blast_type][tax_id].get(t)

                    #print row['Chunk_GC'], row['Chunk_Coverage'], tax_info, tax_id

                    this_row = ['-'.format(i) for i in range(len(org_tax))]
                    this_row[org_tax.index(tax_info)] = str(cov)

                    my_row = "\t".join(this_row)
                    fh.write("%s\t%s\n" % (row['Chunk_GC'], my_row))

            fh.close()
            log.info("-- created: %s", gc_file)
            #sys.exit(44)

            # create a file that has the data source, field name, column id sorted by number of hits
            # used to create gnu_plots

            col_dict = {} # count of hits for each column number
            col_list = [] # taxonomy list
            fh = open(gc_file, "r")
            for line in fh:

                if line.startswith("#GC"):
                    # get a list of the taxonomies
                    arr = line.strip().split("\t")
                    col_list = arr
                    continue

                arr = line.strip().split("\t")

                cnt = 0
                for a in arr:
                    cnt += 1
                    # skip #GC column
                    if cnt == 1:
                        continue

                    # if there is a hit to the organism in this column then increment the count
                    if a != "-":
                        if cnt in col_dict:
                            col_dict[cnt] += 1
                        else:
                            col_dict[cnt] = 1
            fh.close()


            field_file = os.path.join(gnu_path, "%s-fields.txt" % t)
            fh = open(field_file, "a")

            cnt = 0
            min_cnt = 5 # only include this many blast organisms per db, more makes the legend messed up
            if sketch_flag:
                min_cnt = 15 # should use top 80%, was 8
            for c in sorted(col_dict.items(), key=lambda x:x[1], reverse=True):
                cnt += 1
                #fh2.write("# file_name|database|tax name|plot order|hits\n")
                if cnt <= min_cnt:
                    fh.write("%s|%s|%s|%s|%s\n" % (os.path.basename(gc_file), blast_type, col_list[c[0]-1], c[0], c[1]))

            fh.close()
            log.info("- created: %s", field_file)



"""
Create cloud plots with hits to each taxon
- gc vs coverage

Can add this to format the key text (font is "<font>,<size>")
set key samplen 2 spacing .5 font ",8";
"""
def do_plots(library_name):
    log.info("do_plots: %s", library_name)

    err_cnt = 0
    min_hits = 0 # to plot this tax it must have > min_hits hits

    tax_list = ['kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']

    # plot point type
    pt_dict = {
        "nt" : { "pt" : 6, "name" : "NCBI nt" }, # open circle
        "refseq.fungi" : { "pt" : 4, "name" : "refseq.fungi" }, # open square
        "green-genes" : { "pt" : 9, "name" : "Greengenes 16S" }, # filled triangle up
        "ssu" : { "pt" : 9, "name" : "Silva SSU" }, # filled triangle up
        "refseq.bacteria" : { "pt" : 9, "name" : "refseq.bacteria" }, # filled triangle up
        "refseq.archaea" : { "pt" : 2, "name" : "refseq.archaea" }, # x
        "refseq" : { "pt" : 6, "name" : "sketch refseq" }, # open circle
        "silva" : { "pt" : 9, "name" : "sketch silva" }, # filled triangle up
    }


    shred_path = get_the_path("shred", output_path)
    gnu_path = get_the_path("gnu", output_path)

    no_hit_file = os.path.join(shred_path, "no_hit.txt")

    for t in tax_list:

        plot_list = []

        t_alt = t
        if t_alt == "kingdom":
            t_alt = "superkingdom"

        field_file = os.path.join(gnu_path, "%s-fields.txt" % t_alt)

        db_dict = {} # what db corresponds to each point shape, unique dict - if there isn't a hit to a db (e.g. archaea then it does not show in the plot)

        fh = open(field_file, "r")
        for line in fh:
            if line.startswith("#"):
                continue

            arr = line.strip().split("|")
            #'/global/projectb/scratch/brycef/iso/AXNHZ/iso/gc_cov/shred/NCBI_nt_order.txt' u 1:2 with points pointsize 1 pt 6 title "Bacillales"
            plot_file = os.path.join(shred_path, arr[0])
            db_name = arr[1]
            field_name = arr[2]
            col_id = int(arr[3])
            hits = int(arr[4])


            if db_name in pt_dict:
                pt_type = pt_dict[db_name]['pt']
                if db_name not in db_dict:
                    #NaN title        "NCBI nt" w points pt 6 lt rgb "black",\\
                    #NaN title "Greengenes 16S" w points pt 9 lt rgb "black";
                    db_dict[db_name] = "NaN title \"%s\" w points pt %s lt rgb \"black\"" % (pt_dict[db_name]['name'], pt_type)
                    # NaN is correct ... no data to plot

                if hits > min_hits:
                    plot_str = "'%s' u 1:%s with points pointsize 1 pt %s title \"%s\"" % (plot_file, col_id, pt_type, field_name)

                if field_name != "-":
                    plot_list.append(plot_str)

        fh.close()

        replace_keys = {
            "output_png" : os.path.join(output_path, "gc_cov_%s.png" % t.title()),
            "output_no_legend_png" : os.path.join(output_path, "gc_cov_%s_no_legend.png" % t.title()),
            "tax_level" : t.title(),
            "library_name" : library_name,
            "no_hit.txt" : no_hit_file,
            "plot_size" : plot_size
        }

        replace_keys['plot_file_list'] = ",\\\n".join(plot_list)

        db_list = [] #replace_keys['db_list'] = ""
        for db in db_dict:
            db_list.append(db_dict[db])
        replace_keys['db_list'] = ",\\\n".join(db_list)

        gnu_template = """#!/usr/bin/gnuplot

set term png enhanced size [plot_size];
set output "[output_png]";
set grid;
set logscale y;
set xlabel "Percent GC";
set ylabel "Coverage";
set xrange[0:100];
set title "[tax_level] Level GC vs Coverage For [library_name]";
set key top box;
plot '[no_hit.txt]' u 1:2 with points pt 1 lt rgb "grey" title "No Hit",\\
[plot_file_list],\\
[db_list];
        """



        for my_key in replace_keys:
            my_key2 = "[%s]" % my_key
            gnu_template = gnu_template.replace(my_key2, replace_keys[my_key])


        gnu_file = os.path.join(gnu_path, "%s.gnu.txt" % t)
        # need line per line, breaks gnuplot if a lone line with "" or "\," ....
        fh = open(gnu_file, "w")
        for line in gnu_template.split("\n"):
            if line == ",\\":
                continue
            fh.write(line + "\n")
        #fh.write(gnu_template)
        fh.close()
        log.info("- created: %s", gnu_file)


        gnu_log = os.path.join(gnu_path, "gnu_plot_%s.log" % t)
        cmd = "#gnuplot;gnuplot %s > %s 2>&1" % (gnu_file, gnu_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        if exit_code == 0:
            log.info("- created png: %s  %s", replace_keys['output_png'], msg_ok)
        else:
            log.error("- failed to create png: %s  %s", replace_keys['output_png'], msg_fail)
            err_cnt += 1


        # no_legend ...
        # remove titles ...
        replace_keys['plot_file_list'] = re.sub(r'title "(.*)"', "notitle", replace_keys['plot_file_list'])
        gnu_template = """#!/usr/bin/gnuplot

set term png enhanced size [plot_size];
set output "[output_no_legend_png]";
set grid;
set logscale y;
set xlabel "Percent GC";
set ylabel "Coverage";
set xrange[0:100];
set title "[tax_level] Level GC vs Coverage For [library_name]";
plot '[no_hit.txt]' u 1:2 with points pt 1 lt rgb "grey" notitle,\
[plot_file_list];
        """

        for my_key in replace_keys:
            my_key2 = "[%s]" % my_key
            gnu_template = gnu_template.replace(my_key2, replace_keys[my_key])

        gnu_file = os.path.join(gnu_path, "%s_no_legend.gnu.txt" % t)
        fh = open(gnu_file, "w")
        fh.write(gnu_template)
        fh.close()
        log.info("- created: %s", gnu_file)

        gnu_log = os.path.join(gnu_path, "gnu_plot_%s_no_legend.log" % t)
        cmd = "#gnuplot;gnuplot %s > %s 2>&1" % (gnu_file, gnu_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        if exit_code == 0:
            log.info("- created png: %s  %s", replace_keys['output_no_legend_png'], msg_ok)
        else:
            log.error("- failed to create png: %s  %s", replace_keys['output_no_legend_png'], msg_fail)
            err_cnt += 1

    # works only if --fungal (runs map_coverage function)
    gc_cov_file = os.path.join(output_path, "gccovstats.txt")
    if os.path.isfile(gc_cov_file):

        gnu_template = """#!/usr/bin/gnuplot

set term png enhanced size [plot_size];
set output "[output_contig_len_gc_cov]";
set xlabel "Contig Length (bp)";
set ylabel "GC Ratio";
set y2label "Coverage";
set cbrange[0:200];
set xtics font ", 8";
set grid;
set title "Contig Length vs GC vs Aligned Reads Coverage For [library_name]";
plot '[gc_cov_file]' u 3:4:2 with points lt 1 pt 20 lc palette notitle;
        """
        # plot '[gc_cov_file]' u 3:4:2 with points lt 1 pt 20 lc palette t "contig";
        
        length_gc_cov_png = os.path.join(output_path, "contig_len_gc_cov.png")
        replace_keys = {
            "gc_cov_file" : gc_cov_file,
            "library_name" : library_name,
            "output_contig_len_gc_cov" : length_gc_cov_png,
            "plot_size" : plot_size
        }

        for my_key in replace_keys:
            my_key2 = "[%s]" % my_key
            gnu_template = gnu_template.replace(my_key2, replace_keys[my_key])

        gnu_file = os.path.join(gnu_path, "contig_len_gc_cov.txt")
        fh = open(gnu_file, "w")
        fh.write(gnu_template)
        fh.close()
        log.info("- created: %s", gnu_file)

        gnu_log = os.path.join(gnu_path, "gnu_plot_contig_len_gc_cov.log")
        cmd = "#gnuplot;gnuplot %s > %s 2>&1" % (gnu_file, gnu_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        if exit_code == 0:
            log.info("- created png: %s  %s", replace_keys['output_contig_len_gc_cov'], msg_ok)
        else:
            log.error("- failed to create png: %s  %s", replace_keys['output_contig_len_gc_cov'], msg_fail)
            err_cnt += 1

    return err_cnt



'''
Plots shreded fasta fragment vs gc & coverage
'''
def make_frag_vs_gc_cov_gnuplots(library_name):
    log.info("make_frag_vs_gc_cov_gnuplots: %s", library_name)
    err_cnt = 0

    the_path = get_the_path("shred", output_path)
    gnu_path = get_the_path("gnu", output_path)

    cov = os.path.join(the_path, "coverage.txt")

    frag_file = os.path.join(the_path, "frag_vs_gc_cov.txt")


    fh = open(cov, "r")
    fhw = open(frag_file, "w")
    fhw.write("#Contig Name\tGC\tCov\tColor Id\n")

    last_shred = "Motorhead: Iron Fist"

    color_cnt = 0
    cnt = 0
    for line in fh:

        if line.startswith("#"):
            continue
        arr = line.split()
        shred_name = arr[0] # contig name

        # make shred name the same
        # BSHTN_NODE_1_length_1275468_cov_53.5401_250000-254999
        shred = re.sub(r'_\d+-\d+$', "", shred_name)

        if shred != last_shred:
            color_cnt += 1

        last_shred = shred

        cov_x = arr[1] # avg fold
        gc = "{:.2f}".format(float(arr[3])*100) # ref gc converted to percent
        cnt += 1
        my_buffer = "%s\t%s\t%s\t%s\n" % (shred_name, gc, cov_x, color_cnt)
        fhw.write(my_buffer)
    fh.close()
    fhw.close()

    log.info("- created: %s", frag_file)
    log.info("- color count: %s", color_cnt)

    # gnuplot file

    # colorbox shows the gradient for the palette
    # the palette should be one color for each contig in col 3
    # cbrange is not needed
    # http://htmlcolorcodes.com/
    color_list = ['#FF0000', '#00FF00', '#0000FF', '#FF00FF', '#8e44ad', '#AAAA00', '#D10D10', '#00FFFF', '#000000', '#AC00DC',
                  '#e67e22', '#990000', '#009900', '#3498db', '#000099', '#990099', '#999900', '#009999', '#808080']


    # palette needs to define a color for each contig

    l = len(color_list)

    palette_list = []
    for i in range(color_cnt):
        my_color = "%s '%s'" % (i+1, color_list[i % l])
        palette_list.append(my_color)

    palette = ", ".join(palette_list)

    # Kurt request: is there a way to avoid the coverage being directly over the GC?   That happens on some infrequent occasions, and logscale doesn't allow you to see subtle trend shifts.
    # also less white space on the rhs

    gnu_template = """#!/usr/bin/gnuplot

set grid;
set term png enhanced size [plot_size];
set output "[shreds_gc_cov_no_legend_png]";
set multiplot;
set y2tics border;
set logscale y2;
set xlabel "DNA Fragment ID";
set ylabel "Percent GC";
set y2label "Coverage";
set yrange[0:100];
set title "[library_name] DNA Fragment Id vs GC and Coverage";

unset colorbox;
set key spacing 1;
set palette defined([palette]);
plot "[frag_file]" u 0:2:4 axis x1y1 with points pt 1 lc palette z notitle,\\
"[frag_file]" u 0:3:4 axis x1y2 with lines lt rgb "#006090" notitle;
    """
    #NaN title "[library_name]" with points pt 1 lt rgb "#FF0000";
    replace_keys = {
        "library_name" : library_name,
        "shreds_gc_cov_png" : os.path.join(output_path, "shreds_gc_cov.png"),
        "shreds_gc_cov_no_legend_png" : os.path.join(output_path, "shreds_gc_cov_no_legend.png"),
        "frag_file" : frag_file,
        "plot_size" : plot_size,
        "palette" : palette,
    }

    for my_key in replace_keys:
        my_key2 = "[%s]" % my_key
        gnu_template = gnu_template.replace(my_key2, replace_keys[my_key])

    gnu_file = os.path.join(gnu_path, "shreds_gc_cov.gnu.txt")
    fh = open(gnu_file, "w")
    fh.write(gnu_template)
    fh.close()
    log.info("- created: %s", gnu_file)


    gnu_log = os.path.join(gnu_path, "gnu_plot_shreds_gc_cov.log")
    cmd = "#gnuplot;gnuplot %s > %s 2>&1" % (gnu_file, gnu_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    if exit_code == 0:
        log.info("- created png: %s  %s", replace_keys['shreds_gc_cov_png'], msg_ok)
    else:
        log.error("- failed to create png: %s  %s", replace_keys['shreds_gc_cov_png'], msg_fail)
        err_cnt += 1



    # non-log scale on y axis - red plots of gc coverage
    gnu_template = """#!/usr/bin/gnuplot

set grid;
set term png enhanced size [plot_size];
set output "[shreds_gc_vs_cov_png]";
set xlabel "Percent GC";
set ylabel "Coverage";
set xrange[0:100];
set title "[library_name] GC vs. Coverage";
set key spacing 1;
plot "[no_hits_file]" u 1:2 with points pt 1 lt rgb "red" notitle;
    """

    replace_keys = {
        "library_name" : library_name,
        "shreds_gc_vs_cov_png" : os.path.join(output_path, "shreds_gc_vs_cov_single.png"),
        "no_hits_file" : os.path.join(the_path, "no_hit.txt"),
        "plot_size" : plot_size,
    }

    for my_key in replace_keys:
        my_key2 = "[%s]" % my_key
        gnu_template = gnu_template.replace(my_key2, replace_keys[my_key])

    gnu_file = os.path.join(gnu_path, "shreds_gc_vs_cov.gnu.txt")
    fh = open(gnu_file, "w")
    fh.write(gnu_template)
    fh.close()
    log.info("- created: %s", gnu_file)


    gnu_log = os.path.join(gnu_path, "gnu_plot_shreds_vs_gc_cov.log")
    cmd = "#gnuplot;gnuplot %s > %s 2>&1" % (gnu_file, gnu_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    if exit_code == 0:
        log.info("- created png: %s  %s", replace_keys['shreds_gc_vs_cov_png'], msg_ok)
    else:
        log.error("- failed to create png: %s  %s", replace_keys['shreds_gc_vs_cov_png'], msg_fail)
        err_cnt += 1



    # multi colored GC vs Cov with log scale y
    # plot gc vs cov coloring each contig
    gnu_template = """#!/usr/bin/gnuplot

set grid;
set term png enhanced size [plot_size];
set output "[shreds_gc_vs_cov_pileup_png]";
set xlabel "Percent GC";
set ylabel "Coverage";
set xrange[0:100];
set logscale y;
set title "[library_name] GC vs. Coverage per Contig";
set key spacing 1;

unset colorbox;
set palette defined([palette]);
plot "[frag_file]" u 2:3:4 with points pt 1 lc palette z notitle;
    """

    # pileup file:
    # shreded contig  gc  cov  contig id

    replace_keys = {
        "library_name" : library_name,
        "shreds_gc_vs_cov_pileup_png" : os.path.join(output_path, "shreds_gc_vs_cov_multi_nolegend.png"),
        "frag_file" : frag_file,
        "plot_size" : plot_size,
        "palette" : palette,
    }


    for my_key in replace_keys:
        my_key2 = "[%s]" % my_key
        gnu_template = gnu_template.replace(my_key2, replace_keys[my_key])

    gnu_file = os.path.join(gnu_path, "shreds_gc_vs_cov_multi.gnu.txt")
    fh = open(gnu_file, "w")
    fh.write(gnu_template)
    fh.close()
    log.info("- created: %s", gnu_file)


    gnu_log = os.path.join(gnu_path, "gnu_plot_shreds_gc_vs_cov_pileup.log")
    cmd = "#gnuplot;gnuplot %s > %s 2>&1" % (gnu_file, gnu_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    if exit_code == 0:
        log.info("- created png: %s  %s", replace_keys['shreds_gc_vs_cov_pileup_png'], msg_ok)
    else:
        log.error("- failed to create png: %s  %s", replace_keys['shreds_gc_vs_cov_pileup_png'], msg_fail)
        err_cnt += 1


    return err_cnt


'''
Handle SSU, Green Genes
$ cd /global/dna/shared/rqc/ref_databases
$ grep -i ">"  /global/dna/shared/rqc/ref_databases/silva/CURRENT/SILVA_128_SSURef_tax_silva_trunc_utot.fasta | sed 's/>//' > Silva_SSURef_128.list

$ grep -i ">" /global/projectb/sandbox/rqc/blastdb/green_genes/2011.04.green_genes16s/green_genes16s.insa_gg16S.fasta > green_genes.list
* not removing leading ">" from ids because "10 " could match "101010 "

get_taxonomy_custom("100688", "green-genes") # lactobacillus
'''
def get_taxonomy_custom(tax_id, tax_type):
    log.info("get_taxonomy_custom: %s, %s", tax_id, tax_type)

    tax_dict = {
        "superkingdom" : "-",
        "phylum" : "-",
        "class" : "-",
        "order" : "-",
        "family" : "-",
        "genus" : "-",
        "species" : "-"
    }

    if tax_type.lower() == "ssu":
        ssu_list = "/global/dna/shared/rqc/ref_databases/Silva_SSURef_128.list"

        if not os.path.isfile(ssu_list):
            log.error("- cannot find ssu_list: %s  %s", ssu_list, msg_fail)
            sys.exit(2)


        cmd = "grep -i '%s' %s" % (tax_id, ssu_list)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # assume first row is it
        if std_out.startswith(tax_id):
            arr = std_out.replace(tax_id, "").strip().split(";")

            if len(arr) == 7:
                tax_dict['superkingdom'] = arr[0]
                tax_dict['phylum'] = arr[1]
                tax_dict['class'] = arr[2]
                tax_dict['order'] = arr[3]
                tax_dict['family'] = arr[4]
                tax_dict['genus'] = arr[5]
                tax_dict['species'] = arr[6]
            else:
                log.error("- bad format for ssu taxonomy: %s", std_out.strip())

    # green genes - 2011 db created by JGI, not sure whom
    if tax_type.lower().startswith("green"):
        gg_list = "/global/dna/shared/rqc/ref_databases/green_genes.list"

        tax_id = tax_id.replace("gg", "")

        if not os.path.isfile(gg_list):
            log.error("- cannot find gg_list: %s  %s", gg_list, msg_fail)
            sys.exit(2)

        cmd = "grep '>%s ' %s" % (tax_id, gg_list)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        tax_name = " ".join(std_out.split()[1:])

        # look up tax_name with taxonomy service
        tax_dict = get_taxonomy(tax_name, "name")



    log.info("- taxonomy: %s", tax_dict)

    return tax_dict

'''
Run sketch and create file with:
contig|tax_id|tax_name

Sketch by default uses all of refseq
sendsketch.sh mode=sequence in=polished_assembly.shred.fasta minhits=1 > $S/sk.txt
'''
def do_sketch(fasta, db_name):
    log.info("do_sketch: %s", db_name)
    the_path = get_the_path("sketch", output_path)
    shred_path = get_the_path("shred", output_path)



    sketch_file = os.path.join(the_path, "sketch-%s.txt" % db_name)
    sketch_sum = os.path.join(the_path, "sketch-%s.sum.txt" % db_name)



    if os.path.isfile(sketch_sum):
        log.info("- skipping do_sketch, found file: %s", sketch_sum)
    else:

        sketch_log = os.path.join(the_path, "sketch-%s.log" % db_name)
        #k=31,24 records=5
        sketch_params = "printdepth=t mode=sequence minhits=1 color=f"
        # minhits=1 = only needs 1 hit to count, better results

        # CAOZA - doesn't work with "local" - dies
        if db_name == "silva":
            sketch_params += " local"

        cmd = "#bbtools;sendsketch.sh in=%s %s %s > %s 2> %s" % (fasta, sketch_params, db_name, sketch_file, sketch_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        # process sketch file
        log.info("- processing: %s", sketch_file)
        fh = open(sketch_file, "r")

        sketch_dict = {}
        contig_info = {}
        wkid_cnt = 0
        for line in fh:
            line = line.strip()
            
            if line.startswith("Query:"):
                contig = line.split()[1]
            elif line.startswith("<html"):
                log.error("sketch failed for %s  %s", db_name, msg_fail)
                break
            elif line.startswith("WKID"):
                wkid_cnt += 1
                #if wkid_cnt == 1:
                #    log.info("CONTIG\t%s", line)

                continue
            elif line.startswith("No hits."):
                continue
            elif not line:
                continue

            else:

                arr = line.split()
                if len(arr) < 10:
                    break
                
                #print arr
                tax_id = arr[9]
                tax_hit = " ".join(arr[12:])
                m_cnt = arr[6]
                u_cnt = arr[7]
                #log.info("- %s: %s", contig, line)

                # condition to accept as a hit
                if u_cnt > 0:
                    # grab top hit only
                    if contig not in contig_info:
                        contig_info[contig] = { "tax_id" : tax_id, "tax_hit" : tax_hit }


                    # what was hit
                    if tax_hit in sketch_dict:
                        sketch_dict[tax_hit] += 1 #int(m_cnt)
                    else:
                        sketch_dict[tax_hit] = 1 #int(m_cnt)

        fh.close()



        # sort by hits
        log.info("- tax hit summary for %s:", db_name)
        for k in sorted(sketch_dict.items(), key=lambda x:x[1], reverse=True):
            log.info("-- %s: %s contigs", k[0], k[1])

        fh = open(sketch_sum, "w")
        fh.write("#contig/tax_id/tax_hit\n") # using / instead of | because contig can be named "Segkk3|arrow_1450000-1454999"
        for contig in contig_info:
            fh.write("%s/%s/%s\n" % (contig, contig_info[contig]['tax_id'], contig_info[contig]['tax_hit']))
        fh.close()
        log.info("- wrote file: %s", sketch_sum)

'''
Summarize sketch results for plotting
- similar to summarize_hits but I don't want to mess up that code

refseq-class.txt
# GC   A \t B \t C ...
47 - 1929 - -

#Tax ID|Taxonomy (superkingdom;phylum;class;order;family;genus;species)
NZ_MAQX01000032.1|Bacteria;Firmicutes;Bacilli;Bacillales;Bacillaceae;Bacillus;Bacillus sp. FJAT-26390;
'''
def summarize_sketch():
    log.info("summarize_sketch")

    the_path = get_the_path("shred", output_path)
    gnu_path = get_the_path("gnu", output_path)
    sketch_path = get_the_path("sketch", output_path)

    contig_dict = {} # contigs in sketch_sum
    hit_dict = {} # tax_ids to look up



    for db_name in sketch_list:

        sketch_sum = os.path.join(sketch_path, "sketch-%s.sum.txt" % db_name)

        hit_dict[db_name] = []

        fh = open(sketch_sum, "r")
        for line in fh:
            line = line.strip()
            if line.startswith("#"):
                continue


            arr = line.split("/")
            contig = arr[0]
            tax_id = arr[1]
            if contig in contig_dict:
                contig_dict[contig][db_name] = tax_id
            else:
                contig_dict[contig] = { db_name : tax_id }
            hit_dict[db_name].append(tax_id)

        fh.close()


    # look up taxonomy for each unique hit in the hit_dict for the blast_type
    tax_lookup = {}
    for db_name in hit_dict:
        tax_file = os.path.join(the_path, "%s-tax.txt" % db_name)

        fh = open(tax_file, "w")
        fh.write("#Tax ID|Taxonomy (%s)\n" % (";".join(tax_list)))

        tax_ids = list(set(hit_dict[db_name]))
        tax_lookup[db_name] = {}
        # { tax_id: { "superkingdom" : "Eukaryota", "phylum" : "Ascomycota" ... }
        for tax_id in tax_ids:
            taxonomy = "" # look up taxonomy using taxonomy lib

            td = {}

            if tax_id.isdigit():
                td = get_taxonomy(tax_id, "tax_id")

            tax_lookup[db_name][tax_id] = td

            for t in tax_list:
                if t in td:
                    taxonomy += td[t] + ";"
                else:
                    taxonomy += "-;"


            fh.write("%s|%s\n" % (tax_id, taxonomy))
        fh.close()
        log.info("- created: %s", tax_file)


    cov = os.path.join(the_path, "coverage.txt")
    tbl = pd.read_table(cov, sep="\t")
    tbl['Ref_GC'] = tbl['Ref_GC'] * 100

    t2 = tbl[['#ID', 'Length', 'Ref_GC', 'Avg_fold']].copy() # just get these 4 columns, would like contig length too
    # rename columns
    t2.rename(columns={'Avg_fold' : 'Chunk_Coverage', '#ID' : "# Chunk_Name", 'Length' : 'Chunk_Length', 'Ref_GC' : "Chunk_GC"}, inplace=True)


    # add new columns for each blast_type containing the tax_id for each blast_type
    for db_name in sorted(hit_dict.iterkeys()):

        t2[db_name] = "-"

        for index, row in tbl.iterrows():

            tax_id = "-"

            if row['#ID'] in contig_dict:
                #print "** %s" % contig_dict[row['#ID']]

                if db_name in contig_dict[row['#ID']]:
                    tax_id = contig_dict[row['#ID']][db_name]

            t2.set_value(index, db_name, tax_id)

    summary_file = os.path.join(output_path, "summary.txt")
    # Chunk_Name  Chunk_Length  Contig_Length  Chunk_GC Chunk_Cov tax-($key type)
    t2.to_csv(summary_file, sep='\t', index=False)



    # create one file of no hits used for each plot - grey cloud
    no_hit_file = os.path.join(the_path, "no_hit.txt")
    fh = open(no_hit_file, "w")
    fh.write("#GC\tNo Hit\n")
    for index, row in tbl.iterrows():
        gc = row['Ref_GC']
        cov = row['Avg_fold']
        my_buffer = "%s\t%s\n" % (gc, cov)
        fh.write(my_buffer)

    fh.close()
    log.info("- created: %s", no_hit_file)



    summary = os.path.join(output_path, "summary.txt")
    tbl = pd.read_table(summary, sep="\t")


    # reset contents of superkingdom-fields.txt ...
    for t in tax_list:
        field_file = os.path.join(gnu_path, "%s-fields.txt" % t)
        fh2 = open(field_file, "w")
        fh2.write("# file_name|blast_db|tax name|plot order|hits\n")
        fh2.close()



    for db_name in sketch_list:

        log.info("- summarizing: %s", db_name)

        # get unique hits for each db at each level

        # unique taxonomy at each level
        tax_sum = {} # tax_sum['superkingdom'] = ["Eukaryota","Archaea"]

        for index, row in tbl.iterrows():
            #print row
            # # Chunk_Name       CCUZU_unitig_0|arrow_15000-19999
            tax_id = row[db_name]

            if tax_id != "-":
                #tax_id = str(tax_id)
                for t in tax_list:

                    tax_info = "-"
                    tax_id_str = str(tax_id)
                    if tax_id_str in tax_lookup[db_name]:
                        tax_info = tax_lookup[db_name][tax_id_str].get(t)


                    if t in tax_sum:
                        tax_sum[t].append(tax_info)
                    else:
                        tax_sum[t] = [tax_info]
                    #print tax_id, tax_lookup[blast_type][tax_id]


        # write to file
        # gc  [org 1]  [org 2] ... [org n]
        # 32.22  cov - - ... -
        # 33.33  - cov - ... -
        #print tax_sum

        for t in tax_list:
            gc_file = os.path.join(the_path, "%s-%s.txt" % (db_name, t))

            fh = open(gc_file, "w")

            my_header = "#GC\t"
            #GC Dothideomycetes Xylonomycetes Leotiomycetes Eurotiomycete
            org_tax = []

            if t in tax_sum:

                if len(tax_sum[t]) > 0:
                    org_tax = list(set(tax_sum[t]))

                    # should sort by number of hits
                    org_tax.sort()
                    my_header += "\t".join(org_tax)

                    fh.write(my_header + "\n")


            # for each row, look up the taxonomy and write the level
            for index, row in tbl.iterrows():

                tax_id = str(row[db_name])

                if tax_id != "-":

                    cov = row['Chunk_Coverage']
                    tax_info = "-"
                    if tax_id in tax_lookup[db_name]:
                        tax_info = tax_lookup[db_name][tax_id].get(t)

                    #print row['Chunk_GC'], row['Chunk_Coverage'], tax_info, tax_id

                    this_row = ['-'.format(i) for i in range(len(org_tax))]
                    this_row[org_tax.index(tax_info)] = str(cov)

                    my_row = "\t".join(this_row)
                    fh.write("%s\t%s\n" % (row['Chunk_GC'], my_row))

            fh.close()
            log.info("-- created: %s", gc_file)
            #sys.exit(44)

            # create a file that has the data source, field name, column id sorted by number of hits
            # used to create gnu_plots

            col_dict = {} # count of hits for each column number
            col_list = [] # taxonomy list
            fh = open(gc_file, "r")
            for line in fh:

                if line.startswith("#GC"):
                    # get a list of the taxonomies
                    arr = line.strip().split("\t")
                    col_list = arr
                    continue

                arr = line.strip().split("\t")

                cnt = 0
                for a in arr:
                    cnt += 1
                    # skip #GC column
                    if cnt == 1:
                        continue

                    # if there is a hit to the organism in this column then increment the count
                    if a != "-":
                        if cnt in col_dict:
                            col_dict[cnt] += 1
                        else:
                            col_dict[cnt] = 1
            fh.close()


            field_file = os.path.join(gnu_path, "%s-fields.txt" % t)
            fh = open(field_file, "a")

            cnt = 0
            min_cnt = 5 # only include this many blast organisms per db, more makes the legend messed up
            if sketch_flag:
                min_cnt = 8 # should use top 80%
            for c in sorted(col_dict.items(), key=lambda x:x[1], reverse=True):
                cnt += 1
                #fh2.write("# file_name|database|tax name|plot order|hits\n")
                if cnt <= min_cnt:
                    fh.write("%s|%s|%s|%s|%s\n" % (os.path.basename(gc_file), db_name, col_list[c[0]-1], c[0], c[1]))

            fh.close()
            log.info("- created: %s", field_file)





'''
    # need hit_dict for "blast type" which is the db type
    # contig_dict[contig] to add also
    # files named sketch/sketch-(db).sum.txt
    if sketch_flag:
        # reset dicts
        contig_dict = {}
        hit_dict = {} # hits[blast_type] = [list of taxonomies to look up]
        sketch_list = glob.glob(os.path.join(sketch_path, "*.sum.txt"))

        for sketch_file in sketch_list:
            log.info("- process sketch file: %s", sketch_file)

            blast_type = "nt"
            if "silva" in sketch_file:
                blast_type = "ssu"
            if "refseq" in sketch_file:
                blast_type = "bacteria"

            hit_dict[blast_type] = []

            fh = open(sketch_file, "r")
            for line in fh:
                line = line.strip()
                if line.startswith("#"):
                    continue

                arr = line.split("/")
                tax_id = "tax%s" % arr[1]
                contig = arr[0]
                hit_dict[blast_type].append(tax_id)
                if contig in contig_dict:
                    contig_dict[contig][blast_type] = tax_id
                else:
                    contig_dict[contig] = { blast_type : tax_id }

            fh.close()

'''

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "GC_COV"

    cmd = "#bbtools;bbversion.sh"
    bbtools_ver, _, _ = run_cmd(cmd)
    bbtools_ver = bbtools_ver.strip()

    blast_cmd = "%s/run_blastplus_taxserver.py" % (os.path.realpath(os.path.join(my_path, '../tools')))
    cmd = "%s -v" % (blast_cmd)
    _, blastplus_ver, _ = run_cmd(cmd)


    minimap2_cmd = "%s/minimap2" % (os.path.realpath(os.path.join(my_path, '../tools')))
    cmd = "%s --version" % (minimap2_cmd)
    mm_ver, _, _ = run_cmd(cmd)


    version = "2.3.1 (bbtools %s, run_blastplus: %s, minimap2: %s)" % (bbtools_ver, blastplus_ver.strip(), mm_ver.strip())



    uname = getpass.getuser() # get the user, brycef can't use scell.p, but qc_user can!

    tax_list = ['superkingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    #tax_list = ['phylum']


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)

    # Parse options
    usage = "gc_cov.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
creates gc vs coverage cloud plots and the frag vs gc & coverage plots
    """

    parser = ArgumentParser(usage = usage)


    parser.add_argument("-f", "--fasta", dest="fasta", type = str, help = "Input fasta")
    parser.add_argument("-fq", "--fastq", dest="fastq", type = str, help = "Input fastq or input reference (e.g. subreads.fasta for pacbio)")
    parser.add_argument("-l", "--library", dest="library_name", type = str, help = "library name/chart title")
    parser.add_argument("-p", "--plot-size", dest="plot_size", type = str, help = "plot size e.g. '800,600', default 800,640")
    parser.add_argument("-m", "--mode", dest="mode", type = str, help = "mode [illumina, pacbio, fungal], default illumina (different bbmap params)")
    # mode = fungal, microbial-ill, microbial-pb
    parser.add_argument("-s", "--shred-len", dest="shred_len", type = int, help = "shred fasta into this many bases, default 5000bp ")
    parser.add_argument("-o", "--output-path", dest="output_path", type = str, help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-mm", "--minimap", dest="minimap", default = False, action = "store_true", help = "Use minimap2 instead of bbtools/mappacbio.sh")
    parser.add_argument("-gccov", "--gccov", dest="gccov_flag", default = False, action = "store_true", help = "Do additional mapping to generate gccovstats.txt (--mode fungal turns this on too)")
    parser.add_argument("-sketch", "--sketch", dest="sketch_flag", default = False, action = "store_true", help = "Use BBTools sketch rather than blast to identify hits (refseq only)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)

    output_path = None
    fasta = None
    fastq = None # used to get input coverage (aka reference)
    library_name = None
    plot_size = "800,640"
    shred_length = 5000 # number picked by james to have len of 1 or 2 rRNA (1500bp) in it
    mode = "illumina"
    minimap_flag = False
    sketch_flag = False # use sketch


    # parse args
    args = parser.parse_args()
    print_log = args.print_log
    minimap_flag = args.minimap
    gccov_flag = args.gccov_flag
    sketch_flag = args.sketch_flag

    if args.fasta:
        fasta = args.fasta

    if args.fastq:
        fastq = args.fastq

    if args.output_path:
        output_path = args.output_path

    if args.library_name:
        library_name = args.library_name

    if args.mode:
        mode = args.mode


    if args.plot_size:
        plot_size = args.plot_size

    if args.shred_len:
        shred_length = args.shred_len

    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/gc_cov"

        if output_path == "t1":
            # Isolate
            fasta = "/global/projectb/scratch/brycef/iso/BSHTN.fasta"
            fastq = "/global/projectb/scratch/brycef/iso/BSHTN.fastq.gz" # subsampled, filtered
            library_name = "BSHTN"


        if output_path == "t2":
            # Pacbio MIID
            fasta = "/global/projectb/sandbox/rqc/analysis/qc_user/gc_cov/CTCGB.fasta"
            fastq = "/global/projectb/sandbox/rqc/analysis/qc_user/gc_cov/CTCGB.ref.fasta"
            mode = "PaCBiO"
            library_name = "CTCGB"

        if output_path in ("t3", "t3.2", "t3.3"):
            # fungal
            fasta = "/global/projectb/sandbox/rqc/analysis/qc_user/gc_cov/BSSZZ.fasta"
            fastq = "/global/projectb/sandbox/rqc/analysis/qc_user/gc_cov/BSSZZ.ref.fasta"
            mode = "fungalicious"
            library_name = "BSSZZ"
            if output_path == "t3.2":
                library_name = "BSSZZ.2"
            if output_path == "t3.3": # want this one
                library_name = "BSSZZ-mm"
                sketch_flag = True
                minimap_flag = True
                shred_length = 10000
                gccov_flag = True
                
        if output_path == "t4":
            #library_name = "CNCAB" # fungal with contamination
            #fasta = "/global/projectb/scratch/brycef/gc_cov/CNCAB.fasta"
            #fastq = "/global/projectb/scratch/brycef/gc_cov/CNCAB.ref.fasta"
            mode = "fungalrama"
            library_name = "COHYT"
            fasta = "/global/dna/shared/rqc/pipelines/falcon/archive/00/00/43/67/arrow/polished_assembly.fasta"
            fastq = "/global/dna/shared/rqc/pipelines/falcon/archive/00/00/43/67/gc_cov/ref.fasta"
            

            sketch_flag = True
            shred_length = 10000
            minimap_flag = True
            gccov_flag = True


        output_path = os.path.join(test_path, library_name)



    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    if not library_name:
        library_name = os.path.basename(fasta)


    if mode.lower().startswith("p"):
        mode = "pacbio"
    elif mode.lower().startswith("f"):
        mode = "fungal"
        gccov_flag = True
        minimap_flag = True
    else:
        mode = "illumina"


    # need to use 10kbp for sketch to work nicely
    if sketch_flag:
        shred_length = 10000

    # initialize my logger
    log_file = os.path.join(output_path, "gc_cov.log")



    # log = logging object
    log_level = "INFO"
    log = None

    log = get_logger("gc_cov", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "fasta", fasta)
    log.info("%25s      %s", "fastq/ref", fastq)
    log.info("%25s      %s", "library_name", library_name)
    log.info("%25s      %s", "shred_length", shred_length)
    log.info("%25s      %s", "minimap", minimap_flag)
    if gccov_flag:
        log.info("%25s      %s", "gccovstats", gccov_flag)
    log.info("%25s      %s", "plot_size", plot_size)
    log.info("%25s      %s", "sketch_flag", sketch_flag)
    log.info("%25s      %s", "mode", mode)
    log.info("")

    if not os.path.isfile(fasta):
        log.error("Error: cannot find fasta: %s  %s", fasta, msg_fail)
        sys.exit(2)

    if not os.path.isfile(fastq):
        log.error("Error: cannot find fastq: %s  %s", fastq, msg_fail)
        sys.exit(2)



    # shred fasta into 5000bp chunks
    shred_fasta = shred_fasta(fasta, shred_length)

    # bbmap shreded fasta for coverage
    map_shred_coverage(shred_fasta, fastq, mode)

    # map fungal contigs - Kurt request for pacbio and illumina data
    if gccov_flag:
        map_coverage(fasta, fastq, mode)

    # write covstats.txt
    cov_stats()


    blast_list = ['nt', 'ssu'] # microbial list: green_genes - removed, Kecia okay with this
    if mode == "fungal":
        blast_list = ['refseq.fungi', 'refseq.bacteria', 'refseq.archaea'] #, 'nt']


    if sketch_flag:

        #sketch_list = ['refseq', 'silva', 'nt'] # this does work
        sketch_list = ['refseq', 'silva'] # Silva on top of refseq 
        for sketch_db in sketch_list:
            do_sketch(shred_fasta, sketch_db)

        # create correct files for do_plots
        summarize_sketch()

    else:
        # blast shreds vs nt, ssu (blast_list, order is important)
        blast_shreds(shred_fasta)
        # gc_vs_coverage clouds
        summarize_hits()


    err_cnt = do_plots(library_name)

    # fragment vs gc and coverage, gc_vs_cov for fungal
    err_cnt += make_frag_vs_gc_cov_gnuplots(library_name)


    if err_cnt > 0:
        log.error("errors encountered  %s", msg_fail)
        sys.exit(9)





    log.info("Completed %s", script_name)

    sys.exit(0)

