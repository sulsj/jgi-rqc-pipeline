#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Constants specific for RQC PacbioQc pipeline (Illumina).
Global constants are defined in a file under the directory lib/rqc_constants.py .

Created: Oct 15 2014

sulsj (ssul@lbl.gov)

"""

from rqc_constants import *
#from common import get_run_path
#from os_utility import get_tool_path


# global configuration variables
class RQCPacbioQcConfig:
    CFG = {}
    CFG["output_path"] = ""
    CFG["status_file"] = ""
    CFG["files_file"] = ""
    CFG["stats_file"] = ""
    CFG["log_file"] = ""
    CFG["no_cleanup"] = ""
    CFG["run_path"] = ""


# PacbioQc constants
class RQCPacbioQc:
    PACBIO_SAMPLE_PERCENTAGE = 0.25
    PACBIO_SAMPLE_COUNT = 50000
    PACBIO_SMRTCELL_RUN_FOLDER = "/global/seqfs/sdm/prod/pacbio/jobs"
    PACBIO_SMRTCELL_RUN_FOLDER_SEQUEL = "/global/seqfs/sdm/prod/pacbio/jobs_smrtlink"


## Constants for reporting
class PacbioQcStats:
    ## STEP1

    OVERVIEW_JSON = "overview.json"
    FILTER_REPORTS_ADAPTERS_JSON = "filter_reports_adapters.json"
    FILTER_REPORTS_LOADING_JSON = "filter_reports_loading.json"
    FILTER_REPORTS_FILTER_STATS_JSON = "filter_reports_filter_stats.json"
    FILTER_REPORTS_FILTER_SUBREAD_STATS_JSON = "filter_reports_filter_subread_stats.json"
    CONTROL_REPORT_JSON = "control_report.json"

    ADAPTER_OBSERVED_INSERT_LENGTH_DISTRIBUTION_PNG = "adapter_observed_insert_length_distribution.png"
    POST_FILTER_READLENGTH_HISTOGRAM_PNG = "post_filter_readlength_histogram.png"
    PRE_FILTERREAD_SCORE_HISTOGRAM_PNG = "pre_filterread_score_histogram.png"
    POST_FILTERREAD_SCORE_HISTOGRAM_PNG = "post_filterread_score_histogram.png"
    PRE_FILTER_READLENGTH_HISTOGRAM_PNG = "pre_filter_readlength_histogram.png"
    FILTERED_SUBREAD_REPORT_PNG = "filtered_subread_report.png"
    CONTROL_NON_CONTROL_READQUALITY_PNG = "control_non-control_readquality.png"
    CONTROL_NON_CONTROL_READLENGTH_PNG = "control_non-control_readlength.png"

    ## Sequel

    ## PNG
    # ./dataset-reports/control/readlength_plot.png
    # ./dataset-reports/control/concordance_plot.png
    # ./dataset-reports/loading_xml/raw_read_length_plot.png
    # ./dataset-reports/adapter_xml/interAdapterDist0.png
    # ./dataset-reports/filter_stats_xml/insertLenDist0.png
    # ./dataset-reports/filter_stats_xml/readLenDist0.png
    ## JSON
    # ./workflow/datastore.json
    # ./dataset-reports/datastore.json
    # ./datastore.json

    ## STEP2
    PACBIO_TOTAL_BASE_COUNT = "pacbio_total_base_count"
    PACBIO_TOTAL_READ_COUNT = "pacbio_total_read_count"

    PACBIO_READ_GC_MEAN = "pacbio_read_gc_mean"
    PACBIO_READ_GC_MEDIAN = "pacbio_read_gc_median"
    PACBIO_READ_GC_MODE = "pacbio_read_gc_mode"
    PACBIO_READ_GC_STD = "pacbio_read_gc_std"
    PACBIO_READ_GC_TEXT = "pacbio_read_gc_text"
    PACBIO_READ_GC_PLOT = "pacbio_read_gc_plot"
    PACBIO_READ_GC_D3_HTML_PLOT = "pacbio_read_gc_d3_html_plot"

    ## STEP3
    PACBIO_SKIP_BLAST = "pacbio_skip_blast"
    PACBIO_READ_TOPHIT_FILE = "read tophit file of"
    PACBIO_READ_TOP100HIT_FILE = "read top100hit file of"
    PACBIO_READ_TAXLIST_FILE = "read taxlist file of"
    PACBIO_READ_TAX_SPECIES = "read tax species of"
    PACBIO_READ_TOP_HITS = "read top hits of"
    PACBIO_READ_TOP_100_HITS = "read top 100 hits of"
    PACBIO_READ_PARSED_FILE = "read parsed file of"
    PACBIO_READ_MATCHING_HITS = "read matching hits of"
    PACBIO_SAMPLED_READS_NUMBER = "sampled reads number"

    ## STEP4
    PACBIO_READ_LEVEL_MEGAN_TAXA_DIST = 'read level MEGAN: Taxa Distribution of contigs vs NT'
    PACBIO_READ_LEVEL_MEGAN_NT = 'read level MEGAN: Phylogeny of contigs vs NT'
    PACBIO_READ_LEVEL_MEGAN_FILTERED = 'read level MEGAN: Phylogeny of contigs vs NT filtered by class'
    PACBIO_READ_LEVEL_GC_HISTO = 'read level GC Histogram of contigs'

## EOF