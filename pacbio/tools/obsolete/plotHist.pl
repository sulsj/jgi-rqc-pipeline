#!/usr/bin/env perl

=head1 NAME

plotHist.pl

=head1 SYNOPSIS

plotHist.pl [options] <input_file>

  Options:
  -o                    Output image file (required).
  -x <column>           Column containing x-vals (optional; default=1st column).
  -y <column>           Column containing y-vals (optional; use multiple flags to
                        specify more than one y-plot).
  -min <number>         Minimum X-coord (optional).
  -max <number>         Maximum X-coord (optional).
  -title <string>       Title of plot (optional).
  -xlabel <string>      X-axis label (optional).
  -ylabel <string>      Y-axis label (optional).
  -y2                   Show Percent in y2-axis (optional).
  -y2label <string>     Y2-axis label (optional; must be used with -y2pct).
  -ytitle <string>      Title for the y-plot (optional; use multiple flags to 
                        specify more than one y-plot).
  -lpos <left|right>    Legend position (optional; default=right);
  -ig                   Ignore headers in file that start with '#' (optional).
  -nox                  Specify this if you don't want to use the first column as
                        the x-values. X-vals start with 1 and are automatically
		        incremented by 1. The default y-value is the first column
    		        of the dataset if this option is used (optional).
  -xstep                Used with -nox, specify the x-step size.
  -xstart               Used with -nox, specify the x-starting value.
  -xrange <string>      X-range in format "<start> <end>" (optional).
  -yrange <string>      Y-range in format "<start> <end>" (optional).
  -xlog                 Set x-scale to be log scale (optional).
  -ylog                 Set y-scale to be log scale (optional).
  -keep                 Keep intermediate files (optional).
  -h                    Detailed help message (optional).

  This script takes a stream containing x and y values for plotting using
  gnuplot. The output is a png file with filled box style. The format of default
  the input stream is as follows (tab delimited):

  # <x-label> <y1-label> [<y2-label> ...] (optional)
  <x-val> <y1-value> [<y2-value> ...]
  
=head1 HISTORY

=over

=item *

Stephan Trong - 05/09/2012 Creation

=back

=cut

use strict;
use Graphics::GnuplotIF qw(GnuplotIF);
use Pod::Usage;
use Getopt::Long;
use FileHandle;
use File::Temp qw(tempfile tempdir);
use Cwd;
use Data::Dumper;
use vars qw( $optHelp $optMin $optMax $optTitle $optXlabel $optYlabel
    $xAxisColumn @yAxisColumn @optLegend $optIgnoreHeader $optOutputFile
    $optNoXval $optXstep $optXstart $optXrange $optYrange $optKeepTmpFiles
    $optDelimiter $optXlog $optYlog $optPrintGnuCmd $optAddY2PctTics
    $optY2label $optLegendPos);

if( !GetOptions(
        "o=s"=>\$optOutputFile,
        "min=n"=>\$optMin,
        "max=n"=>\$optMax,
	"title=s"=>\$optTitle,
	"xlabel=s"=>\$optXlabel,
	"ylabel=s"=>\$optYlabel,
	"y2"=>\$optAddY2PctTics,
	"y2label=s"=>\$optY2label,
	"x=n"=>\$xAxisColumn,
	"y=n@"=>\@yAxisColumn,
        "ytitle=s@"=>\@optLegend,
        "lpos=s"=>\$optLegendPos,
        "ig"=>\$optIgnoreHeader,
	"nox"=>\$optNoXval,
	"xstep=n"=>\$optXstep,
	"xstart=n"=>\$optXstart,
        "xrange=s"=>\$optXrange,
        "yrange=s"=>\$optYrange,
        "xlog"=>\$optXlog,
        "ylog"=>\$optYlog,
	"d=s"=>\$optDelimiter,
	"pcmd"=>\$optPrintGnuCmd,
	"keep"=>\$optKeepTmpFiles,
        "h"=>\$optHelp,
    )
) {
    pod2usage();
    exit 1;
}

pod2usage(-verbose=>1) and exit 1 if defined $optHelp;

if ( !$optOutputFile ) {
    print STDERR "Must specify -o <outputFile> option.\n";
    exit 1;
}

$optXstep = 1 if !$optXstep;
$optXstart = 1 if !$optXstart;
$optDelimiter = "\\t" if !$optDelimiter;
$optLegendPos = "right" if (!defined $optLegendPos || $optLegendPos
    !~ /^(left|right)$/);
$optXrange =~ s/\s+/:/ if defined $optXrange;
$optYrange =~ s/\s+/:/ if defined $optYrange;

# my $GNUPLOT_EXE = defined $ENV{GNUPLOT_EXE} ? $ENV{GNUPLOT_EXE} : "/usr/common/jgi/math/gnuplot/4.6.2/bin/gnuplot";
my $GNUPLOT_EXE = defined $ENV{GNUPLOT_EXE} ? $ENV{GNUPLOT_EXE} : "/usr/bin/gnuplot";

my @x;
my @y;
my $firstLine = 1;
my $containsHeader = 0;
my $xval = $optXstart;

my $line = <>;
if ( $optIgnoreHeader && $line =~ /^#/ ) {
    $line = <>;
    while ( defined $line && $line =~ /^#/ ) {
        $line = <>;
    }
}
    
while (defined $line) {
    chomp $line;
    if ( $firstLine && $line =~ /^#/ ) {
	$line =~ s/^#\s*//;
        $containsHeader = 1;
    }
    my @datapoints = split /$optDelimiter/, $line;
    my $xcol = $xAxisColumn ? $xAxisColumn - 1: 0;
    
    $line = <>; # get next entry
    
    unless( !$optNoXval && $firstLine && $containsHeader ) {
	next if $optMin && $datapoints[$xcol] < $optMin;
        next if $optMax && $datapoints[$xcol] > $optMax;
    }
    
    $firstLine = 0;
    
    if ( !$optNoXval && scalar(@datapoints) < $xcol ) {
	print "x-column $xcol cannot be found in file.\n";
	exit 1;
    }
    
    if ( $xAxisColumn ) {
	push @x, $datapoints[$xAxisColumn-1] if defined $datapoints[$xAxisColumn-1];
    } elsif ( $optNoXval ) {
	push @x, $xval;
	$xval += $optXstep;
    } else {
        push @x, $datapoints[0] if length $datapoints[0];
    }
    
    my $idx = 0;
    if ( @yAxisColumn ) {
	foreach my $ycol (@yAxisColumn) {
	    if ( scalar(@datapoints) >= $ycol ) {
	        push @{$y[$idx]}, $datapoints[$ycol-1];
    	        $idx++;
            }
	}
    } else {
	shift @datapoints if !$optNoXval;
	foreach my $data (@datapoints) {
	    push @{$y[$idx]}, $data;
	    $idx++;
	}
    }
}

my @labels = ();
if ( @optLegend ) {
    @labels = @optLegend;
} elsif ( $containsHeader ) {
    shift @x if !$optNoXval;
    my $idx = 0;
    foreach my $ref (@y) {
	my $label = shift @{$y[$idx]};
        push @labels, $label;
	$idx++;
    }
}

if ( !@x ) {
    print "Cannot find any x-values.\n";
    exit 1;
}
if ( !@y ) {
    print "Cannot find any y-values.\n";
    exit 1;
}
    
chartMe($optOutputFile, $optXrange, $optYrange, \@x, \@y, \@labels);

exit 0;

#============================================================================#
sub chartMe {
    my ($outputName, $optXrange, $optYrange, $arefXvals, $arefYvals,
        $arefPlotTitles) = @_;

    # Create tmp data file.
    #
    my $tmpFile = "$outputName.csv";
    my $tmpFH = FileHandle->new(">$tmpFile");
    die "Failed to create tmp data file $tmpFile: $!\n" if !defined $tmpFH;
    
    # Get datapoints to create new working csv file.
    #
    my $maxYval = 0;
    my $totalVals = 0;
    my $maxYCol = 0;
    my $numCols = scalar(@$arefYvals);
    my $yTotals = 0;
    for (my $xidx = 0; $xidx <= $#$arefXvals; $xidx++) {
	my $line = $$arefXvals[$xidx];
	for (my $yidx = 0; $yidx <= $#$arefYvals; $yidx++) {
	    if ( defined $$arefYvals[$yidx][$xidx] &&
		$$arefYvals[$yidx][$xidx] =~ /^[-+]?([0-9]*\.[0-9]+|[0-9]+)$/ ) {
		if ( $$arefYvals[$yidx][$xidx] > $maxYval ) {
                    $maxYval = $$arefYvals[$yidx][$xidx];
		    $maxYCol = $yidx;
		}
                $yTotals += $$arefYvals[$yidx][$xidx];
	        $line .= "\t$$arefYvals[$yidx][$xidx]";
	    } else {
		$line .= "\t";
	    }
	}
	$line =~ s/\t$//;
	print $tmpFH "$line\n";
    }
    close $tmpFH;
    
    # Get y2-axis percent label.
    #
    my $y2TicLabel = getPctYtickLabel($maxYval, $yTotals);
    
    # Set gnuplot commands.
    #
    my $gnuCmds = <<EOM;
set terminal png truecolor size 640,480;
set output "$outputName";
set grid;
set style fill transparent solid 0.3 border;
set key $optLegendPos top box;
EOM
    $gnuCmds .= "set title \"".reformatTitle($optTitle)."\"\n" if $optTitle;
    $gnuCmds .= "set xlabel \"$optXlabel\"\n" if $optXlabel;
    $gnuCmds .= "set ylabel \"$optYlabel\"\n" if $optYlabel;
    $gnuCmds .= "set xrange [$optXrange]\n" if $optXrange;
    
    # If -y2 option is set to display percent in y2-axis, set yranges
    # accordingly.
    #
    if ( $optAddY2PctTics ) {
        $gnuCmds .= "set yrange [*:$maxYval*1.1]\n";
        $gnuCmds .= "set y2range [*:$maxYval*1.1]\n";
        $gnuCmds .= "set y2tics $y2TicLabel\n";
        $gnuCmds .= "set y2label \"$optY2label\"\n" if $optY2label;
    } else {
        $gnuCmds .= "set logscale x\n" if $optXlog;
        $gnuCmds .= "set logscale y\n" if $optYlog;
        $gnuCmds .= "set yrange [$optYrange]\n" if $optYrange;
    }
    
    # Create plot.
    #
    $gnuCmds .= " plot";
    for ( my $idx = 0; $idx <= $#$arefYvals; $idx++ ) {
	$gnuCmds .= " \"$tmpFile\" u 1:".($idx+2)." axis x1y1";
	if ( defined $$arefPlotTitles[$idx] ) {
	    $gnuCmds .= " title \"".$$arefPlotTitles[$idx]."\"";
	}
	$gnuCmds .= " with boxes,\\\n";
    }
    $gnuCmds =~ s/\,\\\n$//;
    
    # Create file containing gnu cmds if -keep flag is set.
    #
    if ( $optKeepTmpFiles ) {
        my $tmpFile = "$outputName.gnu";
        my $tmpFH = FileHandle->new(">$tmpFile");
	print $tmpFH "$gnuCmds;\n";
    }
    
    # Run gnuplot to create image file.
    #
    gnuplot($gnuCmds, "$outputName.gnu.err.log");
    
    unlink $tmpFile if (!$optKeepTmpFiles && -e $tmpFile);
}

#============================================================================#
sub getPctYtickLabel {
    my $maxVal = shift;
    my $total = shift;
    
    # Create y-tick label for plotting.
    #
    my $ticstep = $maxVal/10;
    my $y2ticsLabel = '("0" 0';
    for (my $i=1; $i <= 10; $i++) {
        my $y2qpos = sprintf("%.2f", $i*$ticstep);
        my $y2lab = sprintf("%.1f", $i/10*$maxVal/$total*100);
        $y2ticsLabel .= ',"' . $y2lab . '"  ' . $y2qpos ;
    }
    $y2ticsLabel.= ')';
    return $y2ticsLabel;
}

#============================================================================#
sub gnuplot {
    my $gnuCmds = shift;
    my $errLog = shift || '';
    
    my $cmd = "| " . $GNUPLOT_EXE ;
       $cmd .= " 2> $errLog" if length $errLog;
    open CMD, $cmd;
    print CMD $gnuCmds;
    close CMD;
    
    unlink $errLog if !-s $errLog;
}

#============================================================================#
sub reformatTitle {
    my $title = shift;
    my $length = shift || 75;
    
    # Add newlines to title if length is > $length.
    #
    my $delimiter = "\\n";
    $title =~ s/(.{$length})/$1$delimiter/g;
    
    return $title;
}
    
#============================================================================#

