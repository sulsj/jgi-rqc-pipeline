package PGF::Utilities::FastqUtils;

=head1 NAME

=head1 VERSION

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 AUTHOR(S)

Stephan Trong

=head1 HISTORY

=over

=item *

S.Trong - 11/30/2011 - initial version

=back

=cut

use strict;
use warnings;
use FileHandle;
use File::Basename;
use File::Copy;
use File::Path;
use File::Path qw(rmtree);
use Cwd;
use Cwd qw (realpath getcwd abs_path);
require Exporter;

our @ISA = qw(Exporter);
our @EXPORT_OK = qw(
getNumberOfReads
concatFastqFiles
getFileHandle
isCasava18Format
getNextRead
isPaired
);
our %EXPORT_TAGS = (
    all=>[ qw(
        getNumberOfReads
        concatFastqFiles
        getFileHandle
        isCasava18Format
        getNextRead
        isPaired
    ) ]
);

use constant SUCCESS => 0;
use constant FAILURE => 1;

#============================================================================#
sub getNumberOfReads {
    my @files = @_;
    
    my $count = 0;
    my $readName = '';
    
    foreach my $file (@files) {
        my $fh = getFileHandle($file);
        my @readDatas = ();
        while ( getNextRead(\$readName, \@readDatas, $fh) ) {
            $count++;
        }
        close $fh;
    }
    
    return $count;
}

#============================================================================#
sub getFileHandle {
    my $file = shift;
    
    my $fh;
    if ($file =~ /\.bz2$/) {
        $fh = FileHandle->new("bzcat $file |");
    } elsif ($file =~ /\.gz$/) {
        $fh = FileHandle->new("zcat $file |");
    } else {
        $fh = FileHandle->new("< $file");
    }
    print STDERR "Can't open file $file: $!\n" if !defined $fh;
    
    return $fh;
}

#============================================================================#
sub concatFastqFiles {
    my $outputDir = shift;
    my $srefConcatFastqFile = shift;
    my $arefFastqFiles = shift;
    
    # If only one fastq file is specified and is not compressed, do nothing,
    # setting concatFastqFile = the fastq file name.
    #
    if ( @$arefFastqFiles == 1 && $$arefFastqFiles[0] !~ /\.(bz|gz)$/ ) {
        my $inputFastq = abs_path($$arefFastqFiles[0]);
        if ( !defined $inputFastq ) {
            print STDERR "Cannot find input fastq file $$arefFastqFiles[0].\n";
            return FAILURE;
        } elsif ( !-s $inputFastq ) {
            print STDERR "Cannot find input fastq file $inputFastq.\n";
            return FAILURE;
        }
        if ( !length $$srefConcatFastqFile ) {
            $$srefConcatFastqFile = abs_path($inputFastq);
        }
        my $concatFile = "$outputDir/".basename($$srefConcatFastqFile);
        if ( !-e $concatFile ) {
            symlink( $inputFastq, $concatFile );
            print "symlink $inputFastq, $concatFile\n";
        }
        return SUCCESS;
    }
    
    print "Concatenating/uncompressing fastq file(s) ...\n";
    
    my $concatFastqFile = '';
    if ( length $$srefConcatFastqFile ) {
        $concatFastqFile = $$srefConcatFastqFile;
    } else {
        $concatFastqFile = $outputDir . "/";
        foreach my $fastqFile (@$arefFastqFiles) {
            if ( $fastqFile =~ /^(.+)\.fastq*/ ) {
                $concatFastqFile .= basename($1).".";
            } else {
                $concatFastqFile .= basename($fastqFile).".";
            }
        }
        $concatFastqFile =~ s/\.$//;
        $concatFastqFile .= ".fastq" if $concatFastqFile !~ /\.fastq/;
        $$srefConcatFastqFile = $concatFastqFile;
    }
    
    open OUT, ">$concatFastqFile" or die "Can't open file $concatFastqFile: $!\n";
    foreach my $fastqFile (@$arefFastqFiles) {
        print "reading $fastqFile ...\n";
        $fastqFile = abs_path($fastqFile);
        if ($fastqFile =~ /bz2$/) {
            open (IN, "bzcat $fastqFile |") or die "Can't open file $fastqFile: $!\n";
        } elsif ($fastqFile =~ /fastq.gz$/) {
            open (IN, "zcat $fastqFile |") or die "Can't open file $fastqFile: $!\n";
        } else {
            open (IN, "$fastqFile") or die "Can't open file $fastqFile: $!\n";
        }
        while (<IN>) {
            print OUT $_;
        }
        close IN;
    }
    close OUT;
    
    if ( _checkFileExistence($concatFastqFile) != SUCCESS ) {
        return FAILURE;
    } else {
        print "Created $concatFastqFile.\n";
    }
    
    return SUCCESS;
}

#============================================================================#
sub isCasava18Format {
    my $fastqFile = shift;
    
    # Get fastq format (casava version)
    #
    open IN, $fastqFile or die "ERROR: failed to open file $fastqFile: $!\n";
    my $readName = <IN>;
    close IN;
    
    $readName = $1 if $readName =~ /^(\S+)/;
    
    my @parts = split /:/, $readName;
    my $isCasava_1_8 = @parts == 5 ? 0:1;
    
    return $isCasava_1_8;
}

#============================================================================#
sub getNextRead {
    my $srefReadName = shift;  # scalar reference to store name of read
    my $arefReadData = shift;  # array reference to store read entries
    my $fh = shift || 0;       # opened file handle (optional). if not specified,
                               # will use <>.
    
    @$arefReadData = ();
    
    foreach my $lineNum (1..4) {
        my $line = $fh ? <$fh> : <>;
        return 0 if !defined $line;
        $$srefReadName = $1 if ($lineNum == 1 && $line =~ /^@(.+)/);
        push @{$arefReadData}, $line;
    }
    
    return 1;
}

#============================================================================#
sub isPaired {
    my $readName1 = shift;
    my $readName2 = shift;
    
    my $isPair = 0;
    
    # casava 1.3-1.5 format
    if ( $readName1 =~ /^(?:[^\/])+\/[12]/ &&
         $readName2 =~ /^(?:[^\/])+\/[12]/ ) {
        my $read1 = $1 if $readName1 =~ /^([^\/]+)/;
        my $read2 = $1 if $readName2 =~ /^([^\/]+)/;
        $isPair = 1 if $read1 eq $read2;
    # casava 1.8+ format
    } elsif ( $readName1 =~ /^\S+\s+([12]):/ &&
         $readName2 =~ /^\S+\s+([12]):/ ) {
        my $read1 = (split(/\s/, $readName1))[0];
        my $read2 = (split(/\s/, $readName2))[0];
        $isPair = 1 if $read1 eq $read2;
    }
    
    return $isPair;
}

#============================================================================#
sub _checkFileExistence {
    my @files = shift;
    
    my $msg = "";
    foreach my $file (@files) {
        if ( !-s $file ) {
            print STDERR "Failed to create file $file.\n";
            return FAILURE;
        } else {
            $msg .= "Created $file.\n";
        }
    }
    $msg =~ s/\n$//;
    
    print "$msg\n";
    
    return SUCCESS;
}

#============================================================================#
1;
