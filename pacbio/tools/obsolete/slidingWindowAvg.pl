#!/usr/bin/env perl

=head1 NAME

slidingWindowAvg.pl

=head1 SYNOPSIS

slidingWindowAvg.pl [options] <file>

  Options:
  -w <number>     Window size (optional; default=1).
  -stdev          Print Stdev in a column next to value (optional).
  -log            Make first column in log scale (optional).
  -bz             Print blank if value is zero.
  -bzc <string>   When used with -bz, replace zero values with this string.
  -h              Detailed help message (optional).
  
=head1 DESCRIPTION

This script takes a stream of data containing two or more columns and
prints to STDOUT the average values based on a sliding window of the
first column.

=head1 HISTORY

=over

=item *

Stephan Trong - 05/04/2012 Creation

=back

=cut

use strict;
use warnings;
use File::Basename;
use Statistics::Descriptive;
use Pod::Usage;
use Getopt::Long;
use FileHandle;
use FindBin qw($RealBin);
use vars qw($help $optWindowSize $optLogScale $optBlankZeroVals $optShowStdev $optBlankZeroValsWithThisString);

#============================================================================#
# INPUT VALIDATION
#============================================================================#
if( !GetOptions(
        "w=f"=>\$optWindowSize,
        "stdev"=>\$optShowStdev,
        "bz"=>\$optBlankZeroVals,
        "bzc=s"=>\$optBlankZeroValsWithThisString,
        "log"=>\$optLogScale,
        "h"=>\$help,
    )
) {
    pod2usage();
    exit 1;
}

pod2usage(-verbose => 2) and exit 1 if defined $help;

$optBlankZeroValsWithThisString = '' if !defined $optBlankZeroValsWithThisString;

#============================================================================#
# MAIN
#============================================================================

my $windowSize = $optWindowSize ? $optWindowSize : 1;
my $nextWindowPos = $windowSize;
my @entries = ();
my $windowPos = 0;
#my $rounds = 0;
my @vals = ();

my $logWindowSize = $windowSize == 1 ? $windowSize + 1 : $windowSize;
my $nextLogWindowPos = nextLogWindowPos($logWindowSize);
$logWindowSize += $windowSize;
   
while( my $line = <> ) {
    chomp $line;
    next if !length $line;
    if ( $line =~ /^#/ ) {
        printHeader($line);
        next;
    }
    @entries = split /\t/, $line;
    $windowPos = $entries[0];
    addValues(\@entries, \@vals);

    if ( ($optLogScale && $windowPos >= $nextLogWindowPos) || (!$optLogScale && $windowPos >= $nextWindowPos) ) {
        if ( $optLogScale ) {
            $nextLogWindowPos = nextLogWindowPos($logWindowSize);
            $logWindowSize += $windowSize;
        } else {
            $nextWindowPos += $windowSize;
        }

        printResults($windowPos, \@vals);
        @vals = ();
    }
}

# Print reamining window values.
#
printResults($windowPos, \@vals) if @vals;
    
#============================================================================#
# SUBROUTINES
#============================================================================#
sub printHeader {
    my $line = shift;
    
    my $out = '';
    if ( $optShowStdev ) {
        my @lines = split /\t+/, $line;
        $out = shift @lines;
        $out .= "\t";
        foreach (@lines) {
            my $std = "${_}Stdev";
               $std =~ s/^#//;
            $out .= "$_\t$std\t";
        }
        $out =~ s/\t$//;
    } else {
        $out = $line;
    }
    print "$out\n";
}

#============================================================================#
sub addValues {
    my $arefEntries = shift;
    my $arefVals = shift;

    foreach my $idx (1..$#$arefEntries) {
        my $val = $$arefEntries[$idx] =~ /\d+/ ? $$arefEntries[$idx] : 0;
        push @{$$arefVals[$idx - 1]}, $val;
    }
}

#============================================================================#
sub nextLogWindowPos {
    my $nextWindowPos = shift;

    my $log = log($nextWindowPos) / log(10);
    $nextWindowPos = $nextWindowPos + $nextWindowPos*$log;

    return $nextWindowPos;
}

#============================================================================#
sub printResults {
    my $curWindowSize = shift;
    my $arefVals = shift;
    
    my $out = "$curWindowSize\t";
    for (my $idx = 0; $idx <= $#$arefVals; $idx++) {
        my $avg = sprintf("%.2f", average($$arefVals[$idx]));
        if ( $optBlankZeroVals && $avg == 0 ) {
            $out .= "$optBlankZeroValsWithThisString\t";
            $out .= "$optBlankZeroValsWithThisString\t" if $optShowStdev;
        } else {
            $out .= "$avg\t";
            if ( $optShowStdev ) {
                my $std = sprintf("%.2f", stdev($$arefVals[$idx]));
                $out .= "$std\t";
            }
        }
    }
    $out =~ s/\t$//;
    print "$out\n";
}
    
#============================================================================#
sub average {
    my $arefData = shift;
    
    if (!@$arefData) {
        die("Empty array\n");
    }
    my $total = 0;
    foreach (@$arefData) {
        $total += $_;
    }
    my $average = $total / @$arefData;
    return $average;
}

#============================================================================#
sub stdev {
    my $arefData = shift;
    
    if (@$arefData == 1) {
        return 0;
    }
    
    my $average = &average($arefData);
    my $sqtotal = 0;
    
    foreach(@$arefData) {
        $sqtotal += ($average - $_) ** 2;
    }
    my $std = ($sqtotal / (@$arefData - 1)) ** 0.5;
    return $std;
}
    
#============================================================================#
