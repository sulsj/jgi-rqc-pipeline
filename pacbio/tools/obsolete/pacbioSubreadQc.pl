#!/usr/bin/env perl

=head1 NAME

pacbioSubreadQc.pl

=head1 SYNOPSIS

  pacbioSubreadQc.pl [options] <fastq_file>

  Options:
  -od <outputDir>  Output dir (optional; default is current dir).
  -pre <string>    Prefix output file name (optional; default=stats).
  -min <number>    Minimum read length (optional).
  -w <number>      Sliding window size for computing average quality
                   for qual by base plot (optional; default=50)
  -h               Detailed message (optional).
  
  Takes a pacbio CCR fastq file and create qc plots.

=head1 VERSION

$Revision$

$Date$

=head1 AUTHOR(S)

Stephan Trong

=head1 HISTORY

=over

=item *

S.Trong 2011/04/21 creation

=back

=cut

use strict;
use warnings;
use FileHandle;
use Cwd;
use Cwd qw (realpath getcwd abs_path);
use File::Basename;
use File::Path;
use Pod::Usage;
use Getopt::Long;
use Tie::IxHash;
use FindBin qw($RealBin);
# use lib "$RealBin/../lib";
use lib "$RealBin";
# use PGF::Utilities::FastqUtils qw(getNextRead);
# use FastqUtils qw(getNextRead);
use vars qw($optHelp $optOutputDir $optOutputFilePrefix $optMinReadLength $optWindowSize);

#============================================================================#
# CHECKS
#============================================================================#
if( !GetOptions(
        "od=s"=>\$optOutputDir,
        "pre=s"=>\$optOutputFilePrefix,
        "min=n"=>\$optMinReadLength,
        "w=n"=>\$optWindowSize,
        "h" =>\$optHelp,
        )
    ) {
    printhelp(1);
}

printhelp(2) if $optHelp;
printhelp(1) if @ARGV != 1;

my ($fastqFile) = @ARGV;

if ( !-s $fastqFile ) {
    print STDERR "The fastq file $fastqFile does not exist or is zero size.\n";
    exit 1;
}

#============================================================================#
# INITIALIZE VARIABLES
#============================================================================#
use constant SUCCESS => 0;
use constant FAILURE => 1;

# my $GNUPLOT_EXE = defined $ENV{GNUPLOT_EXE} ? $ENV{GNUPLOT_EXE} : "/usr/common/jgi/math/gnuplot/4.6.2/bin/gnuplot";
my $GNUPLOT_EXE = defined $ENV{GNUPLOT_EXE} ? $ENV{GNUPLOT_EXE} : "/usr/bin/gnuplot";
# if [ "$NERSC_HOST" == "denovo" ] || [ "$NERSC_HOST" == "cori" ]
# then
#     # GNUPLOT=`which gnuplot`
#     GNUPLOT="shifter --image=bryce911/bbtools gnuplot"
# else
#     GNUPLOT=`module load gnuplot; which gnuplot`
# fi
my $PLOTHIST_EXE = "$RealBin/plotHist.pl";
my $DRAWHISTOGRAM_EXE = "$RealBin/drawHistogram.pl";
my $HISTBINNING_EXE = "$RealBin/histBinning.pl";
my $SLIDNINGWINDOW_EXE = "$RealBin/slidingWindowAvg.pl";
my $outputDir = defined $optOutputDir ? $optOutputDir : getcwd;
   
$optOutputFilePrefix = "stats" if !defined $optOutputFilePrefix;

#============================================================================#
# MAIN
#============================================================================#

# Create output dir if not exist.
#
mkpath($outputDir) if !-e $outputDir;

# Get file handle to open file either uncompressed or compressed.
#
if ($fastqFile =~ /\.bz2$/) {
    open INFILE, "bzcat $fastqFile |" or die "Can't open file $fastqFile: $!\n";
} elsif ($fastqFile =~ /\.gz$/) {
    open INFILE, "zcat $fastqFile |" or die "Can't open file $fastqFile: $!\n";
} else {
    open INFILE, $fastqFile or die "Can't open file $fastqFile: $!\n";
}

my $subreadName = '';
my @readBlock = ();
my $maxLength = 0;
my @longestReadBlock = ();
my $rawSeq = '';
my $rawQual = '';
my $longestReadLength = 0;
my %qcDatas = ();
my $windowSize = $optWindowSize ? $optWindowSize : 50; # average qual using this sliding window size.
my $rawReadName = '';
my $oldRawReadName = '';

tie my %TYPES, "Tie::IxHash";
$TYPES{'raw'} = 'Full_Read';
$TYPES{'subread'} = 'Subread';
$TYPES{'lrgSubread'} = 'Longest_Subread';

while ( getNextRead(\$subreadName, \@readBlock, *INFILE) ) {
    chomp @readBlock;
    
    my ($seq, $qual) = getReadSeqAndQual(\@readBlock);
    
    ## $optMinReadLength is not used now
    next if ($optMinReadLength && length($rawSeq) < $optMinReadLength);
    
    # pacbio read name like:
    # m120303_072354_00123_c100302052550000001523010507231234_s1_p0/170/0_377
    #
    # where,
    # m120303_072354_00123_c100302052550000001523010507231234_s1_p0/170 is
    # the full (raw) read.
    #
    # m120303_072354_00123_c100302052550000001523010507231234_s1_p0/170/0_377
    # is the subread.
    
    # Add qc stats for this read.
    addQcData(\%qcDatas, $seq, $qual, $TYPES{subread});
    
    # Get raw read name.
    #
    if ( $subreadName =~ /^(.+)\/\d+_\d+$/ ) {
        $rawReadName = $1;
    } else {
        $rawReadName = $subreadName;
    }
    
    # If new raw read is found, store qc stats for the raw and longest subread
    # from the previous raw read set.
    #
    if ( length $oldRawReadName && $rawReadName ne $oldRawReadName ) {
        my ($lrgSeq, $lrgQual) = getReadSeqAndQual(\@longestReadBlock);
        
        # Add qc stats for longest read.
        addQcData(\%qcDatas, $lrgSeq, $lrgQual, $TYPES{lrgSubread});
        
        # Add qc stats for raw read.
        addQcData(\%qcDatas, $rawSeq, $rawQual, $TYPES{raw});
        
        $maxLength = 0;
        $rawSeq = '';
        $rawQual = '';
        @longestReadBlock = @readBlock;
        
    # If same subread, check if longest subread and store read block if longest.
    #
    } elsif ( $rawReadName eq $oldRawReadName ) {
        my $len = length($readBlock[1]);
        if ( $len >= $maxLength ) {
            @longestReadBlock = @readBlock;
            $maxLength = $len;
        }
    } else {
        @longestReadBlock = @readBlock;
    }
    $rawSeq .= $seq;
    $rawQual.= $qual;
    $oldRawReadName = $rawReadName;
}

# Add remaining read entry.
#
my ($lrgSeq, $lrgQual) = getReadSeqAndQual(\@longestReadBlock);

# Add qc stats for longest read.
#
addQcData(\%qcDatas, $lrgSeq, $lrgQual, $TYPES{lrgSubread});

# Add qc stats for raw read.
#
addQcData(\%qcDatas, $rawSeq, $rawQual, $TYPES{raw});

# Create plots.
#
createReadLengthPlot(\%qcDatas, $outputDir, $optOutputFilePrefix, $fastqFile);
createReadQualHistPlot(\%qcDatas, $outputDir, $optOutputFilePrefix, $fastqFile);
createGcPlot(\%qcDatas, $outputDir, $optOutputFilePrefix, $fastqFile);
createQualByPosPlot(\%qcDatas, $outputDir, $optOutputFilePrefix, $windowSize);

# sulsj debug
#use Data::Dumper;
#print Dumper(\%qcDatas);

#use JSON qw( );
#my $json = JSON->new;
#my $dataFile = "$outputDir/qcData.json";
#open my $fh, ">", $dataFile;
##print $fh encode_json($qcDatas);
#print $fh $json->encode(\%qcDatas);
#close $fh;
#
#if ( checkFileExistence($dataFile) != SUCCESS ) {
#    exit -1;
#}
    
exit;

#============================================================================#
# SUBROUTINES
#============================================================================#

sub getNextRead {
    my $srefReadName = shift;  # scalar reference to store name of read
    my $arefReadData = shift;  # array reference to store read entries
    my $fh = shift || 0;       # opened file handle (optional). if not specified,
                               # will use <>.
    
    @$arefReadData = ();
    
    foreach my $lineNum (1..4) {
        my $line = $fh ? <$fh> : <>;
        return 0 if !defined $line;
        $$srefReadName = $1 if ($lineNum == 1 && $line =~ /^@(.+)/);
        push @{$arefReadData}, $line;
    }
    
    return 1;
}


#
# addQcData(\%qcDatas, $rawSeq, $rawQual, $TYPES{raw});
# addQcData(\%qcDatas, $seq,    $qual,    $TYPES{subread});
# addQcData(\%qcDatas, $lrgSeq, $lrgQual, $TYPES{lrgSubread});
# 
sub addQcData {
    my $hrefQcDatas = shift;
    my $seq = shift;
    my $qual = shift;
    my $type = shift;
    
    my $readLength = length $seq;
    
    # GC content
    #
    my $readGC = ($seq =~ tr/GCgc/GCgc/);
    my $gcBin = sprintf("%.1f", $readGC/$readLength * 100);
    $$hrefQcDatas{$type}{gc}{$gcBin}++;
    
    # Read length distribution.
    #
    $$hrefQcDatas{$type}{readLength}{$readLength}++;
    
    # Read quality distribution
    #
    my @tempqual = split( //, $qual);
    my $sumQual = 0;
    for (my $i=0; $i<= $#tempqual; $i++) {
       my $baseQual = ord($tempqual[$i]) - 33; # sanger offset
       $sumQual += $baseQual;
       
       # qual by base distribution
       $$hrefQcDatas{$type}{posQual}[$i] += $baseQual;
       $$hrefQcDatas{$type}{posQualCnt}[$i]++;
    }
    my $avgQual = sprintf("%.1f", $sumQual/scalar(@tempqual));
    $$hrefQcDatas{$type}{readQual}{$avgQual}++;
}

#============================================================================#
sub getReadSeqAndQual {
    my $arefReadBlock = shift;
    
    my ($seq, $qual) = ($$arefReadBlock[1], $$arefReadBlock[3]);
    chomp $seq;
    chomp $qual;
    
    return $seq, $qual;
}

#============================================================================#
sub normalizeBasePosQual {
    my $hrefQcDatas = shift;
    
    # normalize the position qual
    #
    my $maxQual = -1;
    foreach my $type (keys %$hrefQcDatas ) {
        for (my $i = 0; $i <= @{$$hrefQcDatas{$type}{posQual}}; $i++) {
            next if !defined $$hrefQcDatas{$type}{posQualCnt}[$i];
            $$hrefQcDatas{$type}{posQual}[$i] /= $$hrefQcDatas{$type}{posQualCnt}[$i];
            $maxQual = $$hrefQcDatas{$type}{posQual}[$i] if ($maxQual < $$hrefQcDatas{$type}{posQual}[$i]);
        }
    }
    
    return $maxQual;
}

#============================================================================#
sub makePlot {
    my $csvFile = shift;
    my $imgFile = shift;
    my $hrefParams = shift;
    
    # Valid $hrefParams:
    # bin=binsize
    # title=title of plot
    # xlabel=x-axis label
    # ylabel=y-axis label
    # xrange=x-axis range in the format "start end"
    # ylog=0|1; display y-axis in log format?
    
    my $bin = defined $$hrefParams{bin} ? $$hrefParams{bin} : 1;
    my $title = defined $$hrefParams{title} ? $$hrefParams{title} : "";
    my $xlabel = defined $$hrefParams{xlabel} ? $$hrefParams{xlabel} : "";
    my $ylabel = defined $$hrefParams{ylabel} ? $$hrefParams{ylabel} : "";
    my $xrange = defined $$hrefParams{xrange} ? $$hrefParams{xrange} : "";
    my $ylog = defined $$hrefParams{ylog} ? $$hrefParams{ylog} : 0;
    my $showEmptyBins = defined $$hrefParams{showEmptyBins} ? 1:0;
    
    my $plotCmd = "$HISTBINNING_EXE -b $bin";
       $plotCmd .= " -se" if $showEmptyBins;
       $plotCmd .= " $csvFile |";
       $plotCmd .= $PLOTHIST_EXE;
       $plotCmd .= " -title '$title'";
       $plotCmd .= " -xlabel '$xlabel'";
       $plotCmd .= " -ylabel 'Number of Reads'";
       $plotCmd .= " -xrange '$xrange'";
       $plotCmd .= " -ylog" if $ylog;
       $plotCmd .= " -o $imgFile";
    if ( runCommand($plotCmd, 1) != SUCCESS ) {
        return FAILURE;
    }
    
    if ( checkFileExistence($imgFile) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS;
}

#============================================================================#
sub createReadLengthPlot {
    my $hrefQcDatas = shift;
    my $outputDir = shift;
    my $outputFilePrefix = shift;
    my $fastqFile = shift;
    
    my $csvFile = "$outputDir/$outputFilePrefix.readLength.csv" ;
    
    my $bin = 100;
    
    # Create csv file.
    #
    if ( createReadLengthDataFile(\%qcDatas, $csvFile) != SUCCESS ) {
        return FAILURE;
    }
    
    # Create histogram txt files.
    #
    my $colNum = 2;
    my %histCols = ();
    foreach my $type (keys %$hrefQcDatas) {
        my $histFile = "$outputDir/$outputFilePrefix.readLength.$type.hist";
        if ( createHistogramFile($csvFile, $histFile, $colNum, $bin) !=
            SUCCESS ) {
            return FAILURE;
        }
        $histCols{$type} = $colNum;
        $colNum++;
    }
    
    # Create read length plot.
    #
    my $imgFile = "$outputDir/$outputFilePrefix.readLength.png";
    my $title = "Read Length Histogram for ".basename($fastqFile);
    my $xlabel = "Read Length (bp)";
    #my $xrange = "0 *";
    my $xrange = "0:*";
    my %plotParams = (
        bin=>$bin,
        title=>$title,
        xlabel=>$xlabel,
        ylabel=>"Number of Reads",
        xrange=>$xrange,
        ylog=>0,
        showEmptyBins=>1,
    );
    if ( makePlot($csvFile, $imgFile, \%plotParams) != SUCCESS ) {
        return FAILURE;
    }
    
    # Create read length plot with y-log scale.
    #
    $plotParams{ylog} = 1;
    $imgFile = "$outputDir/$outputFilePrefix.readLength.log.png";
    if ( makePlot($csvFile, $imgFile, \%plotParams) != SUCCESS ) {
        return FAILURE;
    }
    
    # Create read length plot based on percentages.
    #
    $imgFile = "$outputDir/$outputFilePrefix.readLengthByPct.png";
    my $ylabel = "Percent Reads";
    if ( createHistPlotByPct($imgFile, $csvFile, \%histCols, $title, $xlabel,
        $ylabel, $xrange, $bin) != SUCCESS ) {
        return FAILURE;
    }
    
    # Create plot truncate at a given max percentile of reads.
    #
    my $maxPctReads = 95;
    my $maxNumReads = getReadLengthDistAtThisPercentile($maxPctReads,
        $csvFile);
    $title = "Read Length Histogram for ".basename($fastqFile).
        " (${maxPctReads}th Percentile)";
    $bin = 50;
    $imgFile = "$outputDir/$outputFilePrefix.readLength.p$maxPctReads.png";
    my $plotCmd = "$HISTBINNING_EXE -b $bin -se -max $maxNumReads $csvFile |";
    $plotCmd .= $PLOTHIST_EXE;
    $plotCmd .= " -title '$title'";
    $plotCmd .= " -xlabel '$xlabel'";
    $plotCmd .= " -ylabel 'Number of Reads'";
    $plotCmd .= " -xrange '$xrange'";
    $plotCmd .= " -ylog";
    $plotCmd .= " -o $imgFile";
    if ( runCommand($plotCmd, 1) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS;
}

#============================================================================#
sub createHistPlotByPct_orig {
    my $imgFile = shift;
    my $hrefHistFile = shift;
    my $title = shift;
    my $xlabel = shift;
    my $ylabel = shift;
    my $xrange = shift || '';
    
    $title = reformatTitle($title);
    $xrange =~ s/^(\d+)\s+(\d+)/$1:$2/; # if xrange passed as "start end",
                                        # change to "start:end"
    
    my $gnuCmds = <<EOM;
set terminal png truecolor size 640,480;
set output "$imgFile";
set grid;
set xlabel "$xlabel";
set ylabel "$ylabel";
set title "$title";
set logscale y;
set style fill transparent solid 0.3 border;
EOM
    $gnuCmds .= "set xrange [$xrange]\n" if length $xrange;

    $gnuCmds .= "plot ";
    foreach my $type (keys %$hrefHistFile) {
        $gnuCmds .= "\"$$hrefHistFile{$type}\" u 2:8 title \"$TYPES{$type}\" ".
            "with boxes,\\\n";
    }
    $gnuCmds =~ s/\,\\\n$/;\n/;
    
    # Create image file using gnuplot.
    #
    my $gnuplotLogFile = $imgFile.".gnuplot.err.log";
    gnuplot($gnuCmds, $gnuplotLogFile);
    
    if ( checkFileExistence($imgFile) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS;
}
    
#============================================================================#
sub createHistPlotByPct {
    my $imgFile = shift;
    my $csvFile = shift;
    my $hrefHistCols = shift;
    my $title = shift;
    my $xlabel = shift;
    my $ylabel = shift;
    my $xrange = shift || '';
    my $binSize = shift || 1;
    
    $title = reformatTitle($title);
    $xrange =~ s/^(\d+)\s+(\d+)/$1:$2/; # if xrange passed as "start end",
                                        # change to "start:end"
    
    my $gnuCmds = <<EOM;
set terminal png truecolor size 640,480;
set output "$imgFile";
set grid;
set xlabel "$xlabel";
set ylabel "$ylabel";
set title "$title";
set logscale y;
set style fill transparent solid 0.3 border;
EOM
    $gnuCmds .= "set xrange [$xrange]\n" if length $xrange;

    my $dataVals = '';
    my $plotCmds = '';
    foreach my $type (sort {$a cmp $b} keys %$hrefHistCols) {
        my $col = $$hrefHistCols{$type};
        my @datas = `$DRAWHISTOGRAM_EXE -c 1 -cn $col -csv -se -nh -b $binSize $csvFile | awk '{print \$1\"\t\"\$3}'`;
        $plotCmds .= "     \'-\' using 1:2 title \"$type\" with boxes,\\\n";
        $dataVals .= join "", map {" $_"} @datas;
        $dataVals .= "EOF\n";
    }
    $plotCmds =~ s/\,\\\n$/\n/;
    $gnuCmds .= "plot $plotCmds$dataVals";
    
    # Create image file using gnuplot.
    #
    my $gnuplotLogFile = $imgFile.".gnuplot.err.log";
    gnuplot($gnuCmds, $gnuplotLogFile);
    
    if ( checkFileExistence($imgFile) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS;
}
    
#============================================================================#
sub getReadLengthDistAtThisPercentile {
    my $pct = shift;
    my $csvFile = shift;
    
    my $cmd = "$DRAWHISTOGRAM_EXE -cn 3 -nh -csv $csvFile | ".
              " awk '{ if (\$4<=$pct){max=\$1} } END {print max}'";
    print "$cmd\n\n";
    chomp(my $maxVal = `$cmd`);
    
    return $maxVal;
}

#============================================================================#
sub createReadQualHistPlot {
    my $hrefQcDatas = shift;
    my $outputDir = shift;
    my $outputFilePrefix = shift;
    my $fastqFile = shift;
    
    my $csvFile = "$outputDir/$outputFilePrefix.readQual.csv";
    my $bin = 0.5;
    
    # Create csv file.
    #
    if ( createReadQualDataFile(\%qcDatas, $csvFile) != SUCCESS ) {
        return FAILURE;
    }
    
    # Create histogram txt files.
    #
    my $colNum = 2;
    my %histCols = ();
    foreach my $type (keys %$hrefQcDatas) {
        my $histFile = "$outputDir/$outputFilePrefix.readQual.$type.hist";
        if ( createHistogramFile($csvFile, $histFile, $colNum, $bin) != SUCCESS ) {
            return FAILURE;
        }
        $histCols{$type} = $colNum;
        $colNum++;
    }

    # Create plot.
    #
    my $imgFile = "$outputDir/$outputFilePrefix.readQual.png";
    my $title = "Read Quality Histogram for ".basename($fastqFile);
    my $xlabel = "Read Quality";
    #my $xrange = "0 *";
    my $xrange = "0:*";
    my %plotParams = (
        bin=>$bin,
        title=>$title,
        xlabel=>$xlabel,
        ylabel=>"Number of Reads",
        xrange=>$xrange,
        ylog=>0,
        showEmptyBins=>1,
    );
    if ( makePlot($csvFile, $imgFile, \%plotParams) != SUCCESS ) {
        return FAILURE;
    }
    
    # Create plot with y-log scale.
    #
    $plotParams{ylog} = 1;
    $imgFile = "$outputDir/$outputFilePrefix.readQual.log.png";
    if ( makePlot($csvFile, $imgFile, \%plotParams) != SUCCESS ) {
        return FAILURE;
    }
    
    # Create read qual plot based on percentages.
    #
    $imgFile = "$outputDir/$outputFilePrefix.readQualByPct.png";
    my $ylabel = "Percent Reads";
    if ( createHistPlotByPct($imgFile, $csvFile, \%histCols, $title, $xlabel,
        $ylabel, $xrange) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS;
}

#============================================================================#
sub createGcPlot {
    my $hrefQcDatas = shift;
    my $outputDir = shift;
    my $outputFilePrefix = shift;
    my $fastqFile = shift;
    
    my $csvFile = "$outputDir/$outputFilePrefix.gc.csv";
    my $bin = 1;
    
    # Create csv file.
    #
    if ( createGcDataFile(\%qcDatas, $csvFile) != SUCCESS ) {
        return FAILURE;
    }
    
    # Create histogram txt files.
    #
    my $colNum = 2;
    my %histCols = ();
    foreach my $type (keys %$hrefQcDatas) {
        my $histFile = "$outputDir/$outputFilePrefix.gc.$type.hist";
        if ( createHistogramFile($csvFile, $histFile, $colNum, $bin) != SUCCESS ) {
            return FAILURE;
        }
        $histCols{$type} = $colNum;
        $colNum++;
    }

    # Create plot.
    #
    my $imgFile = "$outputDir/$outputFilePrefix.gc.png";
    my $title = "GC Histogram for ".basename($fastqFile);
    my $xlabel = "GC Percent";
    my $xrange = "0 100";
    my %plotParams = (
        bin=>$bin,
        title=>$title,
        xlabel=>$xlabel,
        ylabel=>"Number of Reads",
        xrange=>$xrange,
        ylog=>0,
        showEmptyBins=>1,
    );
    if ( makePlot($csvFile, $imgFile, \%plotParams) != SUCCESS ) {
        return FAILURE;
    }
    
    # Create plot in y-log scale.
    #
    $plotParams{ylog} = 1;
    $imgFile = "$outputDir/$outputFilePrefix.gc.log.png";
    if ( makePlot($csvFile, $imgFile, \%plotParams) != SUCCESS ) {
        return FAILURE;
    }
    
    # Create gc plot based on percentages.
    #
    $imgFile = "$outputDir/$outputFilePrefix.gcByPct.png";
    my $ylabel = "Percent Reads";
    if ( createHistPlotByPct($imgFile, $csvFile, \%histCols, $title, $xlabel,
        $ylabel, $xrange, $bin) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS;
}

#============================================================================#
sub createQualByPosPlot {
    my $hrefQcDatas = shift;
    my $outputDir = shift;
    my $outputFilePrefix = shift;
    my $fastqFile = shift;
    my $windoeSize = shift;
    
    # Normalize base position quality scores.
    #
    my $maxQual = normalizeBasePosQual(\%qcDatas);

    my $csvFile = "$outputDir/$outputFilePrefix.qualByPos.csv";
    #my $bin = 1;
    
    # Create csv file.
    #
    if ( createQualByPosDataFile(\%qcDatas, $csvFile) != SUCCESS ) {
        return FAILURE;
    }
    
    # Create csv file containing sliding window average of quals by base pos.
    #
    my $csvAvgFile = "$outputDir/$outputFilePrefix.qualByPos.w$windowSize.csv";
    my $slwCmd = "$SLIDNINGWINDOW_EXE -w $windowSize -log -bz -bzc '-' -stdev $csvFile";
    $slwCmd .= " > $csvAvgFile";
    if ( runCommand($slwCmd, 1) != SUCCESS ) {
        return FAILURE;
    }
    
    my $imgFile = "$outputDir/$outputFilePrefix.qualByPos.png";
    my $title = "Average Quality Scores By Base Position";
    my $y2ticsLabel = getQualByPosYtickLabel($maxQual);
    my $gnuCmds = <<EOM;
set terminal png truecolor size 640,480;
set output "$imgFile";
set grid;
set key left;
set title '$title';
set xlabel 'Base Position (bp)';
set ylabel 'Average Quality Score';
set y2label 'Average Accuracy';
set yrange [0:15];
set y2range [0:15];
set y2tics $y2ticsLabel;
set style line 1 lc rgb "red";
set style line 2 lc rgb "#008000";
set style line 3 lc rgb "blue";
EOM

    $gnuCmds .= "plot ";
    my $col = 2;
    my $lineStyle = 1;
    foreach my $type (keys %TYPES) {
        my $label = $TYPES{$type};
        $gnuCmds .= "\"$csvAvgFile\" using 1:$col:".($col+1)." ls $lineStyle title \"$label\" with yerrorbars,\\\n";
        $col += 2;
        $lineStyle++;
    }
    
    $col = 2;
    $lineStyle = 1;
    foreach my $type (keys %TYPES) {
        my $label = $TYPES{$type};
        $gnuCmds .= "\"$csvAvgFile\" using 1:$col:".($col+1)." ls $lineStyle notitle with lines,\\\n";
        $col += 2;
        $lineStyle++;
    }
    $gnuCmds =~ s/,\\$/;/;

    # Create gnuplot input file (for reference only).
    #
    my $gnuplotInFile = "$outputDir/$outputFilePrefix.qualByPos.gnuplot.log";
    unless (open OUT, ">$gnuplotInFile") {
        print STDERR "Failed to create file $gnuplotInFile: $!\n";
        return FAILURE;
    }
    print OUT $gnuCmds;
    close OUT;

    # Create image file using gnuplot.
    #
    my $gnuplotLogFile = "$outputDir/$outputFilePrefix.qualByPos.gnuplot.err.log";
    gnuplot($gnuCmds, $gnuplotLogFile);
    
    if ( checkFileExistence($imgFile) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS
}

#============================================================================#
sub reformatTitle {
    my $title = shift;
    my $length = shift || 75;
    
    # Add newlines to title if length is > $length.
    #
    my $delimiter = "\\n";
    $title =~ s/(.{$length})/$1$delimiter/g;
    
    return $title;
}
    
#============================================================================#
sub gnuplot {
    my $gnuCmds = shift;
    my $errLog = shift || '';
    
    my $cmd = "| " . $GNUPLOT_EXE ;
       $cmd .= " 2> $errLog" if length $errLog;
    open CMD, $cmd;
    print CMD $gnuCmds;
    close CMD;
    
    unlink $errLog if !-s $errLog;
}

#============================================================================#
sub getQualByPosYtickLabel {
    my $maxQual = shift;
    
    # Create y-tick label for plotting.
    #
    my $ticstep = $maxQual/7;
    my $y2ticsLabel = '("0" 0';
    for (my $i=1; $i <= 7; $i++) {
        my $y2qpos = sprintf("%.2f", $i*$ticstep);
        my $y2lab = sprintf("%.2f", 1-10**(-$y2qpos/10));
        $y2ticsLabel .= ',"' . $y2lab . '"  ' . $y2qpos ;
    }
    $y2ticsLabel .= ')';
    
    return $y2ticsLabel;
}

#============================================================================#
sub createHistogramFile {
    my $csvFile = shift;
    my $outputFile = shift;
    my $col = shift;
    my $bin = shift;
    
    my $cmd = "$DRAWHISTOGRAM_EXE -c 1 -cn $col -b $bin $csvFile > $outputFile";
    if ( runCommand($cmd, 1) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS;
}
    
#============================================================================#
sub createReadLengthDataFile {
    my $hrefQcDatas = shift;
    my $file = shift;
    
    my @types = ();
    my %readLengths = ();
    my $shortestReadLen = 0;
    my $longestReadLen = 0;
    
    foreach my $type (sort {$a cmp $b} keys %$hrefQcDatas) {
        push @types, $type;
        foreach my $readLength( keys %{$$hrefQcDatas{$type}{readLength}} ) {
            $shortestReadLen = $readLength if !$shortestReadLen;
            $shortestReadLen = $readLength if $readLength < $shortestReadLen;
            $longestReadLen = $readLength if $readLength > $longestReadLen;
            $readLengths{$type}{$readLength} = $$hrefQcDatas{$type}{readLength}{$readLength};
        }
    }
    
    open OUT, ">$file" or die "Failed to create file $file: $!\n";
    my $header = join "\t", @types;
       $header = "#NumReads\t$header";
       $header =~ s/\s+$//;
    print OUT "$header\n";
    
    foreach my $readLength ($shortestReadLen..$longestReadLen) {
        my $entry = "$readLength\t";
        foreach my $type (@types) {
            if ( defined $readLengths{$type}{$readLength} ) {
                $entry .= "$readLengths{$type}{$readLength}\t";
            } else {
                $entry .= "\t";
            }
        }
        $entry =~ s/\t$//;
        print OUT "$entry\n";
    }
    close OUT;
    
    if ( checkFileExistence($file) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS
}

#============================================================================#
sub createReadQualDataFile {
    my $hrefQcDatas = shift;
    my $file = shift;
    
    my @types = ();
    my %readQuals = ();
    foreach my $type (sort {$a cmp $b} keys %$hrefQcDatas) {
        push @types, $type;
        foreach my $avgQual ( keys %{$$hrefQcDatas{$type}{readQual}} ) {
            $readQuals{$avgQual}{$type} = $$hrefQcDatas{$type}{readQual}{$avgQual};
        }
    }
    
    open OUT, ">$file" or die "Failed to create file $file: $!\n";
    my $header = join "\t", @types;
       $header = "#Qual\t$header";
       $header =~ s/\s+$//;
    print OUT "$header\n";
    
    foreach my $avgQual (sort {$a <=> $b} keys %readQuals) {
        my $entry = "$avgQual\t";
        foreach my $type (@types) {
            if ( defined $readQuals{$avgQual}{$type} ) {
                $entry .= "$readQuals{$avgQual}{$type}\t";
            } else {
                $entry .= "\t";
            }
        }
        $entry =~ s/\t$//;
        print OUT "$entry\n";
    }
    close OUT;
    
    if ( checkFileExistence($file) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS
}
    
#============================================================================#
sub createGcDataFile {
    my $hrefQcDatas = shift;
    my $file = shift;
    
    my @types = ();
    my %gcBins = ();
    foreach my $type (sort {$a cmp $b} keys %$hrefQcDatas) {
        push @types, $type;
        foreach my $gcBin ( sort {$a<=>$b} keys %{$$hrefQcDatas{$type}{gc}} ) {
            $gcBins{$gcBin}{$type} = $$hrefQcDatas{$type}{gc}{$gcBin};
        }
    }
    
    open OUT, ">$file" or die "Failed to create file $file: $!\n";
    my $header = join "\t", @types;
       $header = "#GC\t$header";
       $header =~ s/\s+$//;
    print OUT "$header\n";
    
    foreach my $gcBin (sort {$a <=> $b} keys %gcBins ) {
        my $entry = "$gcBin\t";
        foreach my $type (@types) {
            if ( defined $gcBins{$gcBin}{$type} ) {
                $entry .= "$gcBins{$gcBin}{$type}\t";
            } else {
                $entry .= "\t";
            }
        }
        $entry =~ s/\t$//;
        print OUT "$entry\n";
    }
    close OUT;
    
    if ( checkFileExistence($file) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS
}

#============================================================================#
sub createQualByPosDataFile {
    my $hrefQcDatas = shift;
    my $file = shift;
    
    my @types = ();
    my %qualByPos = ();
    foreach my $type (keys %TYPES) {
        my $label = $TYPES{$type};
        push @types, $label;
        for (my $pos = 0; $pos <= $#{$$hrefQcDatas{$label}{posQual}}; $pos++ ) {
            $qualByPos{$pos}{$label} = $$hrefQcDatas{$label}{posQual}[$pos];
        }
    }
    
    open OUT, ">$file" or die "Failed to create file $file: $!\n";
    my $header = join "\t", @types;
       $header = "#Pos\t$header";
       $header =~ s/\s+$//;
    print OUT "$header\n";
    
    foreach my $pos (sort {$a <=> $b} keys %qualByPos ) {
        my $entry = ($pos+1)."\t";
        foreach my $type (@types) {
            if ( defined $qualByPos{$pos}{$type} ) {
                $entry .= sprintf("%.2f\t",$qualByPos{$pos}{$type});
            } else {
                $entry .= "\t";
            }
        }
        $entry =~ s/\t$//;
        print OUT "$entry\n";
    }
    close OUT;
    
    if ( checkFileExistence($file) != SUCCESS ) {
        return FAILURE;
    }
    
    return SUCCESS
}

#============================================================================#
sub runCommand {
    my $cmd = shift;
    my $printCmd = shift || 0;
    
    print "$cmd\n\n" if $printCmd;
    
    my $exitStatus = system($cmd);
    $exitStatus = $exitStatus >> 8;
    
    if ( $exitStatus != 0 ) {
        print STDERR "$cmd failed with exit status=$exitStatus\n";
        return FAILURE;
    }
    
    return SUCCESS;
}

#============================================================================#
sub checkFileExistence {
    my @files = @_;
    
    my $msg = "";
    foreach my $file (@files) {
        if ( -d $file || !-s $file ) {
            print STDERR "Failed to create file $file.\n";
            return FAILURE;
        } else {
            $msg .= "Created $file.\n\n";
        }
    }
    $msg =~ s/\n$//;
    
    print "$msg\n";
    
    return SUCCESS;
}

#============================================================================#
sub printhelp {
    my $verbose = shift || 1;
    pod2usage(-verbose=>$verbose);
    exit 1;
}

#============================================================================#
