#!/usr/bin/env perl

=head1 NAME

getLongestSubreads.pl

=head1 SYNOPSIS

  getLongestSubreads.pl [options] <fastq_file>

  Options:
  -of          Output in fasta file (optional; default=fastq).
  -min         Specify mininum read length (optional).
  -h           Detailed message (optional).
  
  Takes a stream of pacbio CCR data from a fastq file prints th longest subread
  to STDOUT.

=head1 VERSION

$Revision$

$Date$

=head1 AUTHOR(S)

Stephan Trong

=head1 HISTORY

=over

=item *

S.Trong 2011/04/21 creation

=back

=cut

use strict;
use warnings;
use FileHandle;
use Pod::Usage;
use Getopt::Long;
use FindBin qw($RealBin);
# use lib "$RealBin/../lib";
# use PGF::Utilities::FastqUtils qw(getNextRead);
use vars qw( $optHelp $optOutputInFasta $optMinLength);

#============================================================================#
# CHECKS
#============================================================================#
if( !GetOptions(
        "of"=>\$optOutputInFasta,
        "min=n"=>\$optMinLength,
        "h" =>\$optHelp,
        )
    ) {
    printhelp(1);
}

printhelp(1) if $optHelp;

#============================================================================#
# MAIN
#============================================================================#

my $readName = '';
my $oldReadName = '';
my @readDatas = ();
my $maxLength = 0;
my @longestReadDatas = ();

while ( getNextRead(\$readName, \@readDatas) ) {
    chomp @readDatas;
    $readName =~ s/\/([^\/]+)$//;
    # print "read name = $readName";
    
    # Print longest subread.
    #
    if ( length $oldReadName && $readName ne $oldReadName ) {
        my $readlen = length($longestReadDatas[1]);
        my $readEntry = getReadEntry(\@longestReadDatas, $optOutputInFasta);
        
        # If -min option not specified or if specified and read length >= value
        # specified, print read entry.
        #
        if ( !defined $optMinLength ||
            (defined $optMinLength && $readlen >= $optMinLength) ) {
            print "$readEntry";
        }
        
        $maxLength = 0;
        @longestReadDatas = @readDatas;
        
    } elsif ( $readName eq $oldReadName ) {
        my $len = length($readDatas[1]);
        if ( $len >= $maxLength ) {
            @longestReadDatas = @readDatas;
            $maxLength = $len;
        }
    } else {
        @longestReadDatas = @readDatas;
    }
    $oldReadName = $readName;
}

my $readlen = length($longestReadDatas[1]);
if ( !defined $optMinLength ||
    (defined $optMinLength && $readlen >= $optMinLength ) ) {
    print getReadEntry(\@longestReadDatas, $optOutputInFasta);
}

exit;


## sulsj 07-19-2017
## From FastqUtils.pm
#============================================================================#
sub getNextRead {
    my $srefReadName = shift;  # scalar reference to store name of read
    my $arefReadData = shift;  # array reference to store read entries
    my $fh = shift || 0;       # opened file handle (optional). if not specified,
                               # will use <>.
    
    @$arefReadData = ();
    
    foreach my $lineNum (1..4) {
        my $line = $fh ? <$fh> : <>;
        return 0 if !defined $line;
        $$srefReadName = $1 if ($lineNum == 1 && $line =~ /^@(.+)/);
        push @{$arefReadData}, $line;
    }
    
    return 1;
}


#============================================================================#
# SUBROUTINES
#============================================================================#
sub getReadEntry {
    my $hrefReadDatas = shift;
    my $outputInFasta = shift || 0;
    
    my $readEntry = '';
    
    # If $outputInFasta = 1, then define entry in fasta format.
    #
    if ( $outputInFasta ) {
        my $defline = $$hrefReadDatas[0];
           $defline =~ s/^\@/\>/;
        my $seq = $$hrefReadDatas[1];
           $seq =~s /(.{50})/$1\n/g; # chop string into 50 bp chunks
        $readEntry = "$defline\n$seq\n";
    
    # Otherwise, define entry in fastq format.
    #
    } else {
        $readEntry = join "\n", @$hrefReadDatas;
        $readEntry .= "\n";
    }
    
    return $readEntry;
}

#============================================================================#
sub printhelp {
    my $verbose = shift || 1;
    pod2usage(-verbose=>$verbose);
    exit 1;
}

#============================================================================#
