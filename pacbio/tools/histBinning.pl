#!/usr/bin/env perl

=head1 NAME

histBinning.pl

=head1 SYNOPSIS

histBinning.pl [options] <file>

  Options:
  -c <number>        Column containing observations (optional; default=1)
  -cn <number>       Column containing observation counts (optional; this option
                     may be specified multiple times for different columns;
                     default=all columns except first)
  -b <number>        Bin size (optional; default=1)
  -d <character>     Column delimiter (optional; default=tab)
  -s <number>        Starting bin (optional; default=0)
  -min <number>      Minimum bin size cutoff (optional)
  -max <number>      Maximum bin size cutoff (optional)
  -se                Show empty bins (optional)
  -h                 Detailed help message (optional)
 
=head1 DESCRIPTION

Given a file containing at least two columns, one with the observation value
and another with the number of observations for that value, this script will
bin the values into the specified binning size. Multiple columns containing
the number of observation for the value in -c <column_number> can be specified
by using multiple -cn <column_number> options. The output is printed to STDOUT.

=head1 VERSION

$Revision$

$Date$

=head1 HISTORY

=over

=item *

Stephan Trong 02/11/2011 Creation

=back

=cut

use strict;
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use FindBin;
# use lib "$FindBin::RealBin/../lib";
# use PGF::QDExtension::Histogram;
use lib "$FindBin::RealBin";
use Histogram;
use vars qw( $optHelp $optDelimiter $optInitialBinSize $optColumnOfObservations
$optColumnOfObservationCounts $optBinSize $optMinBinSizeForDisplay
$optMaxBinSizeForDisplay @optColumnOfObservationCounts $optShowEmptyBins);

#============================================================================#
# INPUT VALIDATION
#============================================================================#
if( !GetOptions(
        "d=n"=>\$optDelimiter,
        "c=n"=>\$optColumnOfObservations,
        "cn=n@"=>\@optColumnOfObservationCounts,
        "i=n"=>\$optInitialBinSize,
        "b=f"=>\$optBinSize,
        "min=n"=>\$optMinBinSizeForDisplay,
        "max=n"=>\$optMaxBinSizeForDisplay,
        "se"=>\$optShowEmptyBins,
        "h"=>\$optHelp,
    )
) {
    pod2usage();
    exit 1;
}

pod2usage(-verbose=>2) and exit 1 if defined $optHelp;
pod2usage and exit 1 if @ARGV != 1;

my $inputFile = $ARGV[0];
my $binSize = defined $optBinSize ? $optBinSize:1;

#============================================================================#
# INITIALIZE VARIABLES
#============================================================================#
my $delimiter = defined $optDelimiter ? $optDelimiter : '\t';
my $initialMinBin = defined $optInitialBinSize ? $optInitialBinSize : 0;

$optColumnOfObservations = 1 if !defined $optColumnOfObservations;

#============================================================================#
# MAIN
#============================================================================#

# Get observations from file.
#

my ($hrefDataSetsByColumn, $header) = getObservationsByColumnAndTotal($inputFile, $optColumnOfObservations, \@optColumnOfObservationCounts, $delimiter);

if ( !%$hrefDataSetsByColumn) {
    print STDERR "Cannot find any dataSetsByColumn.\n";
    exit 1;
}

# Bin dataset.
#
my %binDataset = ();
my %uniqueBins = ();
foreach my $col (keys %$hrefDataSetsByColumn) {
    my %columnObs = %{$$hrefDataSetsByColumn{$col}};
    my %binning = binDataset($binSize, $initialMinBin, \%columnObs, \%uniqueBins);
    $binDataset{$col} = \%binning;
}

print "$header\n" if $header;

my @sortedBins = sort {$a<=>$b} keys %uniqueBins;
if ( $optShowEmptyBins ) {
    my $min = $sortedBins[0];
    my $max = $sortedBins[$#sortedBins];
    # Expand bins starting with min bin, stepping $optBinSize to $max bin.
    #
    @sortedBins = map { $optBinSize * $_ } $min..sprintf("%.0f", ($max-$min)/$optBinSize);
}

foreach my $bin (@sortedBins) {
    print $bin;
    foreach my $col (@optColumnOfObservationCounts) {
        if ( defined $binDataset{$col} && $binDataset{$col}{$bin} ) {
            print "\t$binDataset{$col}{$bin}";
        } else {
            print "\t0";
        }
    }
    print "\n";
}

exit 0;

#============================================================================#
# SUBROUTINES
#============================================================================#
sub getObservationsByColumnAndTotal {
    
    my $file = shift;
    my $columnOfObservations = shift;
    my $refArrayColumnOfObservationTotals = shift;
    my $delimiter = shift;
    
    my %observations = ();
    my @headers = ();
        
    open IFILE, $file or die "ERROR: cannot open file $file: $!\n";
    
    while (<IFILE>) {
        next if !length $_;
        if ( /^#/ ) {
            @headers = split /\s+/, $_;
            next;
        }
        my @cols = split(/$delimiter/);
        if ( @$refArrayColumnOfObservationTotals ) {
            foreach my $column (@$refArrayColumnOfObservationTotals) {
                if ( defined $cols[$column-1] &&
                    $cols[$column-1] =~ /^[-+]?([0-9]*\.[0-9]+|[0-9]+)$/
                ) {
                    $observations{$column}{$cols[$columnOfObservations-1]} +=
                        $cols[$column-1];
                } else {
                    $observations{$column}{$cols[$columnOfObservations-1]} += 0;
                }
            }
        } else {
            my $numCols = scalar @cols;
            foreach my $column (2..$numCols) {
                if ( defined $cols[$column-1] &&
                    $cols[$column-1] =~ /^[-+]?([0-9]*\.[0-9]+|[0-9]+)$/
                ) {
                    $observations{$column}{$cols[$columnOfObservations-1]} += 
                        $cols[$column-1];
                } else {
                    $observations{$column}{$cols[$columnOfObservations-1]} += 0;
                }
            }
        }
    }
    
    close IFILE;
    
    if ( !@$refArrayColumnOfObservationTotals ) {
        foreach my $col (sort {$a <=> $b} keys %observations ) {
            push @$refArrayColumnOfObservationTotals, $col;
        }
    }
    
    my $header = '';
    if ( @$refArrayColumnOfObservationTotals ) {
        my @tmpHeaders = ();
        foreach my $column ($columnOfObservations, @$refArrayColumnOfObservationTotals) {
            $header .= "$headers[$column-1]\t";
        }
        $header =~ s/\t$//;
    } else {
        $header = join "\t", @headers;
    }

    return \%observations, $header;
    
}

#============================================================================#
sub binDataset {
    
    my $binSize = shift;
    my $initialMinBin = shift;
    my $refHashOrArrayOfObservations = shift;
    my $refHashOfUniqueBins = shift;
    
    # Create object to create histogram.
    #
    my $histObject = PGF::QDExtension::Histogram->new();
    
    $histObject->setMinObservation($optMinBinSizeForDisplay) if $optMinBinSizeForDisplay;
    $histObject->setMaxObservation($optMaxBinSizeForDisplay) if $optMaxBinSizeForDisplay;

    # Create histogramof depth of coverage.
    #
    $histObject->computeHistogram(
        $refHashOrArrayOfObservations,
        $binSize,
        $initialMinBin
    );
    
    my $pictureFormat = 'csv';
    
    my @histDatas = $histObject->getBinAttribs();
    my %results = ();
    
    foreach my $data (@histDatas) {
        my $bin = $$data{bin};
        $results{$bin} = $$data{quantity};
        $$refHashOfUniqueBins{$bin} = 1;
    }
        
    return %results;

}

#============================================================================#
