#!/bin/bash -l

#set -e

# Pacbio qc pipeline tests

# How to compare the results: jgi-rqc/rqc_system/test/compare.py -rf [list of output folders] -p [pacbio|raed|filter|assemblyqc]

OUTDIR="/global/projectb/sandbox/rqc/analysis/qc_user/pacbioqc"
DATE=`date +"%Y%m%d"`

# dev for sulsj
cmd="/global/homes/s/sulsj/work/bitbucket-repo/jgi-rqc-pipeline/pacbio/pacbioqc2.py"

# just need the names of pacbio sequnits
SEQUNIT="pbio-387.4210.fastq"
qsub -cwd -b yes -j yes -m n -w e -terse -q normal.c -l ram.c=5.25g,h_rt=11:00:00 -pe pe_slots 8 -N pacbioqc2-test-$SEQUNIT -P gentech-rqc.p -o $OUTDIR/pacbioqc2-$SEQUNIT-$DATE.log "$cmd -f $SEQUNIT -o $OUTDIR/$SEQUNIT-$DATE"

SEQUNIT="pbio-702.6741.fastq"
qsub -cwd -b yes -j yes -m n -w e -terse -q normal.c -l ram.c=5.25g,h_rt=11:00:00 -pe pe_slots 8 -N pacbioqc2-test-$SEQUNIT -P gentech-rqc.p -o $OUTDIR/pacbioqc2-$SEQUNIT-$DATE.log "$cmd -f $SEQUNIT -o $OUTDIR/$SEQUNIT-$DATE"

SEQUNIT="pbio-712.6814.fastq"
qsub -cwd -b yes -j yes -m n -w e -terse -q normal.c -l ram.c=5.25g,h_rt=11:00:00 -pe pe_slots 8 -N pacbioqc2-test-$SEQUNIT -P gentech-rqc.p -o $OUTDIR/pacbioqc2-$SEQUNIT-$DATE.log "$cmd -f $SEQUNIT -o $OUTDIR/$SEQUNIT-$DATE"

SEQUNIT="pbio-720.6861.fastq"
qsub -cwd -b yes -j yes -m n -w e -terse -q normal.c -l ram.c=5.25g,h_rt=11:00:00 -pe pe_slots 8 -N pacbioqc2-test-$SEQUNIT -P gentech-rqc.p -o $OUTDIR/pacbioqc2-$SEQUNIT-$DATE.log "$cmd -f $SEQUNIT -o $OUTDIR/$SEQUNIT-$DATE"

SEQUNIT="pbio-726.6907.fastq"
qsub -cwd -b yes -j yes -m n -w e -terse -q normal.c -l ram.c=5.25g,h_rt=11:00:00 -pe pe_slots 8 -N pacbioqc2-test-$SEQUNIT -P gentech-rqc.p -o $OUTDIR/pacbioqc2-$SEQUNIT-$DATE.log "$cmd -f $SEQUNIT -o $OUTDIR/$SEQUNIT-$DATE"

SEQUNIT="pbio-726.6908.fastq"
qsub -cwd -b yes -j yes -m n -w e -terse -q normal.c -l ram.c=5.25g,h_rt=11:00:00 -pe pe_slots 8 -N pacbioqc2-test-$SEQUNIT -P gentech-rqc.p -o $OUTDIR/pacbioqc2-$SEQUNIT-$DATE.log "$cmd -f $SEQUNIT -o $OUTDIR/$SEQUNIT-$DATE"
