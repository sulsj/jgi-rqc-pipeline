#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Pacbio qc II utilities

Created: Oct 15 2014

sulsj (ssul@lbl.gov)

"""

import os

from rqc_utility import *
from pacbioqc2_constants import *
from db_access import *
from os_utility import make_dir, change_mod, run_sh_command, get_tool_path
from rqc_constants import *


'''===========================================================================
    file_name_trim

'''
def file_name_trim(fname):
    return fname.replace(".gz", "").replace(".fastq", "").replace(".fasta", "")


'''===========================================================================
    checkpoint_step_wrapper

'''
def checkpoint_step_wrapper(status):
    assert RQCPacbioQcConfig.CFG["status_file"]
    checkpoint_step(RQCPacbioQcConfig.CFG["status_file"], status)


'''===========================================================================
    get_pacbio_qid

'''
def get_pacbio_qid(sequnitName, log):
    ## read only connection

    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)

    sql = """select su.pb_job_id
             from seq_units su
             inner join rqc_pipeline_queue rpq on su.seq_unit_id = rpq.seq_unit_id
             where su.seq_unit_name = '%s';"""

    qid = None

    ## TODO: don't need to stage the input pacbio sequnit
    ## if the sequnit name is full path,
    if sequnitName.find("/") != -1:
        ## /global/projectb/scratch/qc_user/rqc/prod/staging/00/00/00/00/rqc-pbio-733.6968.fastq ==> rqc-pbio-733.6968.fastq
        sequnitName, exitCode = safe_basename(sequnitName, log)
        assert exitCode == 0, "Calling safe_basename() failed."
        ## rqc-pbio-733.6968.fastq ==> pbio-733.6968.fastq
        sequnitName = sequnitName.replace("rqc-", "")
        assert (sequnitName.startswith("pbio-") or sequnitName.startswith("nanopore")) and (sequnitName.endswith(".fastq") or sequnitName.endswith(".fastq.gz")), "Not valid pacbio sequnit name " + sequnitName

    log.debug("sql= %s, seq_unit_name = %s", sql, sequnitName)
    sth.execute(sql % (sequnitName))
    rs = sth.fetchone()

    if rs:
        qid = int(rs['pb_job_id']) if rs['pb_job_id'] else 0

    else:
        log.error("cannot find information from the seq_units table for %s", sequnitName)
        qid = -1

    db.close()


    return qid


##'''===========================================================================
##    pacbio_read_blastn_refseq_microbial
##
##'''
##def pacbio_read_blastn_refseq_microbial(subFastaFile, log, blastDbPath=None):
## 12212015 sulsj REMOVED!


'''===========================================================================
    pacbio_read_blastn_refseq_archaea

'''
def pacbio_read_blastn_refseq_archaea(subFastaFile, log, blastDbPath=None):
    ## verify the fastq file
    if not os.path.isfile(subFastaFile):
        log.error("Failed to find fastq file for blastn: %s", subFastaFile)
        return RQCExitCodes.JGI_FAILURE

    log.info("Read level contamination analysis using blastn")

    PB_OUTPUT_PATH = RQCPacbioQcConfig.CFG["output_path"]

    ## output dir
    megablastDir = "blast"
    megablastPath = os.path.join(PB_OUTPUT_PATH, megablastDir)
    make_dir(megablastPath)
    change_mod(megablastPath, "0755")

    ##
    ## Run blastn
    ##
    ## 11032015 RQC-741: Removed refseq.microbial and added archaea and bacteria
    ##
    if blastDbPath:
        db = os.path.join(blastDbPath, "refseq.archaea/refseq.archaea")
    else:
        db = RQCReferenceDatabases.REFSEQ_ARCHAEA

    log.info("Blast database used: %s", db)


    log.info("----------------------------------")
    log.info("Start blastn search against %s", db)
    log.info("----------------------------------")

    ## final output ==> PB_OUTPUT_PATH/megablast
    ## ex) /usr/common/jgi/aligners/blast+/2.2.28/bin/blastn -query .../megablast/reads.fa -db /scratch/rqc/refseq.archaea/refseq.archaea -num_threads 8 -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -outfmt 0 -show_gis -dust yes -soft_masking true > .../megablast/megablast.reads.fa.v.refseq.archaea.FmLD2a8p90E30JFfTITW45 2> .../megablast/megablast.reads.fa.v.refseq.archaea.FmLD2a8p90E30JFfTITW45.log
    retCode, megablastHitFile = run_blastplus_py(subFastaFile, megablastPath, "refseq.archaea", log)

    retCode = None

    if retCode != RQCExitCodes.JGI_SUCCESS:
        if megablastHitFile is None:
            log.error("Failed to run blastn against %s. Ret = %s", db, retCode)
            retCode = RQCExitCodes.JGI_FAILURE
        elif megablastHitFile == -143:
            log.error("Blast overtime. Skip the search against %s.", db)
            retCode = -143 ## blast overtime
    else:
        log.info("Successfully ran blastn of reads against %s", db)
        retCode = RQCExitCodes.JGI_SUCCESS


    return retCode



'''===========================================================================
    pacbio_read_blastn_refseq_bacteria

'''
def pacbio_read_blastn_refseq_bacteria(subFastaFile, log, blastDbPath=None):
    ## verify the fastq file
    if not os.path.isfile(subFastaFile):
        log.error("Failed to find fastq file for blastn: %s", subFastaFile)
        return RQCExitCodes.JGI_FAILURE

    log.info("Read level contamination analysis using blastn")

    PB_OUTPUT_PATH = RQCPacbioQcConfig.CFG["output_path"]

    ## output dir
    megablastDir = "blast"
    megablastPath = os.path.join(PB_OUTPUT_PATH, megablastDir)
    make_dir(megablastPath)
    change_mod(megablastPath, "0755")

    ##
    ## Run blastn
    ##
    ## 11032015 RQC-741: Removed refseq.microbial and added archaea and bacteria
    ##
    if blastDbPath:
        db = os.path.join(blastDbPath, "refseq.bacteria/refseq.bacteria")
    else:
        db = RQCReferenceDatabases.REFSEQ_BACTERIA

    log.info("Blast database used: %s", db)


    log.info("----------------------------------")
    log.info("Start blastn search against %s", db)
    log.info("----------------------------------")

    ## final output ==> PB_OUTPUT_PATH/megablast
    ## ex) /usr/common/jgi/aligners/blast+/2.2.28/bin/blastn -query .../megablast/reads.fa -db /scratch/rqc/refseq.bacteria/refseq.bacteria -num_threads 8 -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -outfmt 0 -show_gis -dust yes -soft_masking true > .../megablast/megablast.reads.fa.v.refseq.bacteria.FmLD2a8p90E30JFfTITW45 2> .../megablast/megablast.reads.fa.v.refseq.bacteria.FmLD2a8p90E30JFfTITW45.log
    retCode, megablastHitFile = run_blastplus_py(subFastaFile, megablastPath, "refseq.bacteria", log)

    retCode = None

    if retCode != RQCExitCodes.JGI_SUCCESS:
        if megablastHitFile is None:
            log.error("Failed to run blastn against %s. Ret = %s", db, retCode)
            retCode = RQCExitCodes.JGI_FAILURE
        elif megablastHitFile == -143:
            log.error("Blast overtime. Skip the search against %s.", db)
            retCode = -143 ## blast overtime
    else:
        log.info("Successfully ran blastn of reads against %s", db)
        retCode = RQCExitCodes.JGI_SUCCESS


    return retCode



'''===========================================================================
    pacbio_read_blastn_nt

'''
def pacbio_read_blastn_nt(subFastaFile, log, blastDbPath=None):
    ## verify the fastq file
    if not os.path.isfile(subFastaFile):
        log.error("Failed to find fastq file for blastn: %s", subFastaFile)
        return RQCExitCodes.JGI_FAILURE

    log.info("Read level contamination analysis using blastn.")

    PB_FILES_FILE = RQCPacbioQcConfig.CFG["files_file"]
    PB_OUTPUT_PATH = RQCPacbioQcConfig.CFG["output_path"]

    ## output dir
    megablastDir = "blast"
    megablastPath = os.path.join(PB_OUTPUT_PATH, megablastDir)
    make_dir(megablastPath)
    change_mod(megablastPath, "0755")

    ##
    ## Run megablast
    ##
    if blastDbPath:
        db = os.path.join(blastDbPath, "nt/nt")
    else:
        db = RQCReferenceDatabases.NT_maskedYindexedN_BB

    log.info("Blast database used: %s", db)


    log.info("----------------------------------")
    log.info("Start blastn search against %s", db)
    log.info("----------------------------------")

    ## final output ==> PB_OUTPUT_PATH/megablast
    ## ex) /usr/common/jgi/aligners/blast+/2.2.28/bin/blastn -query .../megablast/reads.fa -db /scratch/rqc/nt/nt -num_threads 8 -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -outfmt 0 -show_gis -dust yes -soft_masking true > .../megablast/megablast.reads.fa.v.nt.FmLD2a8p90E30JFfTITW45 2> .../megablast/megablast.reads.fa.v.nt.FmLD2a8p90E30JFfTITW45.log
    retCode, blastOutFileNamePrefix = run_blastplus_py(subFastaFile, megablastPath, "nt", log)

    if retCode != RQCExitCodes.JGI_SUCCESS:
        if blastOutFileNamePrefix is None:
            log.error("Failed to run blastn against %s. Ret = %s", db, retCode)
            return RQCExitCodes.JGI_FAILURE
        elif blastOutFileNamePrefix == -143:
            log.error("Blast overtime. Skip the search against %s.", db)
            return -143 ## blast overtime
    else:
        log.info("Successfully ran blastn of reads against %s", db)


    ## Skipped! 10282016
    ##
    ## megan -------------------------------------------------------------------------------------------------
    ##
    # log.info("--------------------")
    # log.info("Start running megan.")
    # log.info("--------------------")
    #
    # formattedFasta, exitCode = safe_basename(subFastaFile, log)
    # assert exitCode == 0
    # formattedFasta = file_name_trim(formattedFasta)
    #
    # label, exitCode = safe_basename(formattedFasta, log)
    # assert exitCode == 0
    #
    # meganOutDirName = label + ".megan"
    # meganOutFullPath = os.path.join(PB_OUTPUT_PATH, meganOutDirName)
    # make_dir(meganOutFullPath)
    # change_mod(meganOutFullPath, "0755")
    #
    # fullReadsFastaFileName = os.path.join(megablastPath, subFastaFile)
    # fullMeganOutDirName = meganOutFullPath
    #
    # ## ex) megan.pl -f .../megablast/reads.fa -b .../megablast/megablast.reads.fa.v.nt.FmLD2a8p90E30JFfTITW45 -t megablast -w .../7601.1.77813.CTTGTA.s0.01.s0.01.s0.0588142575171.n50000.megan -l 7601.1.77813.CTTGTA.s0.01.s0.01.s0.0588142575171.n50000
    # ## ex) .../pmgw/bin/meganWrapper.pl .../megablast/megablast.reads.fa.v.nt.FmLD2a8p90E30JFfTITW45 .../7601.1.77813.CTTGTA.s0.01.s0.01.s0.0588142575171.n50000.megan/reads.fa .../7601.1.77813.CTTGTA.s0.01.s0.01.s0.0588142575171.n50000.megan/7601.1.77813.CTTGTA.s0.01.s0.01.s0.0588142575171.n50000.rma 7601.1.77813.CTTGTA.s0.01.s0.01.s0.0588142575171.n50000
    # ## ex) source .../pmgw/bin/../config/megan_settings.sh;xvfb-run -a MEGAN +g false -x "import blastfile=.../megablast/megablast.reads.fa.v.nt.FmLD2a8p90E30JFfTITW45 readfile=.../7601.1.77813.CTTGTA.s0.01.s0.01.s0.0588142575171.n50000.megan/reads.fa meganfile=.../7601.1.77813.CTTGTA.s0.01.s0.01.s0.0588142575171.n50000.megan/7601.1.77813.CTTGTA.s0.01.s0.01.s0.0588142575171.n50000.rma maxmatches=100 minscore=35.0 toppercent=10.0 winscore=0.0 minsupport=5 summaryonly=false usecompression=true usecogs=true usegos=true useseed=false;quit;"
    # retCode = run_megan(fullReadsFastaFileName, blastOutFileNamePrefix + "_bbdedupe_bbmasked_formatted.parsed", fullMeganOutDirName, label, log)
    # log.debug("retCode=%s from run_megan()" % (retCode))
    #
    # if retCode != RQCExitCodes.JGI_SUCCESS:
    #     log.error("Failed to run megan")
    #
    #     ## Just return SUCESS to skip megan
    #     log.warning("Just return JGI_SUCCESS to skip the megan analysis")
    #
    #     return RQCExitCodes.JGI_SUCCESS
    #
    # else:
    #     log.info("Successfully ran megan.")
    #
    #     ## reporting
    #     ## Look for timage files with the containing the following keys.
    #     ## The value=description of file.
    #
    #     megan_img_files, stdErr, exitCode = run_sh_command("ls %s/*.jpg" % (meganOutFullPath), True, log)
    #
    #     ## ex)
    #     ## .../out/7325.1.66082.AACAGGTTCGC.s0.01.megan/7325.1.66082.AACAGGTTCGC.s0.01.phylo.jpg
    #     ## .../out/7325.1.66082.AACAGGTTCGC.s0.01.megan/7325.1.66082.AACAGGTTCGC.s0.01.phylo2.jpg
    #     for f in megan_img_files.split('\n'):
    #         f = f.strip()
    #
    #         ##PACBIO_READ_LEVEL_MEGAN_TAXA_DIST = 'read level MEGAN: Taxa Distribution of contigs vs. NT'
    #         ##PACBIO_READ_LEVEL_MEGAN_NT = 'read level MEGAN: Phylogeny of contigs vs NT'
    #         ##PACBIO_READ_LEVEL_MEGAN_FILTERED = 'read level MEGAN: Phylogeny of contigs vs NT filtered by class'
    #         ##PACBIO_READ_LEVEL_GC_HISTO = 'read level GC Histogram of contigs'
    #
    #         if f and f.endswith(".taxa.jpg"):
    #             append_rqc_file(PB_FILES_FILE, PacbioQcStats.PACBIO_READ_LEVEL_MEGAN_TAXA_DIST, f, log)
    #         elif f and f.endswith(".phylo.jpg"):
    #             append_rqc_file(PB_FILES_FILE, PacbioQcStats.PACBIO_READ_LEVEL_MEGAN_NT, f, log)
    #         elif f and f.endswith(".phylo2.jpg"):
    #             append_rqc_file(PB_FILES_FILE, PacbioQcStats.PACBIO_READ_LEVEL_MEGAN_FILTERED, f, log)
    #         elif f and f.endswith(".megan.jpg"):
    #             append_rqc_file(PB_FILES_FILE, PacbioQcStats.PACBIO_READ_LEVEL_GC_HISTO, f, log)


    return RQCExitCodes.JGI_SUCCESS



""" For STEP3 & STEP4
run_blastplus_py

Call run_blastplus.py
"""
def run_blastplus_py(queryFastaFile, blastOutPath, db, log):
    queryFastaFileBaseName, exitCode = safe_basename(queryFastaFile, log)
    dbFileBaseName, exitCode = safe_basename(db, log)

    # runBlastnCmd = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/run_blastplus_taxserver.py"
    # runBlastnCmd = get_tool_path("run_blastplus_taxserver.py", "run_blastplus_taxserver.py")
    runBlastnCmd = get_tool_path("run_blastplus_taxserver.py", "jgi-rqc")
    timeoutCmd = get_tool_path("timeout", "timeout")

    blastOutFileNamePrefix = os.path.join(blastOutPath, "megablast." + queryFastaFileBaseName + ".vs." + dbFileBaseName)

    cmd = "%s 21600s " % (timeoutCmd) + runBlastnCmd + " -d " + db + " -o " + blastOutPath + " -q " + queryFastaFile + " -s > " + blastOutFileNamePrefix + ".log 2>&1 "

    _, _, exitCode = run_sh_command(cmd, True, log, True)

    ## Added timeout to terminate blast run manually after 6hrs
    ## If exitCode == 124 or exitCode = 143, this means the process exits with timeout.
    ## Timeout exits with 128 plus the signal number. 143 = 128 + 15 (SGITERM)
    ## Ref) http://stackoverflow.com/questions/4189136/waiting-for-a-command-to-return-in-a-bash-script
    ##      timeout man page ==> If the command times out, and --preserve-status is not set, then exit with status 124.
    ##
    if exitCode in [124, 143]:
        ## BLAST timeout
        ## Exit with succuss so that the blast step can be skipped.
        log.info("##################################")
        log.info("BLAST TIMEOUT. JUST SKIP THE STEP.")
        log.info("##################################")
        return RQCExitCodes.JGI_FAILURE, -143

    elif exitCode != 0:
        log.error("Failed to run_blastplus_py. Exit code != 0")
        return RQCExitCodes.JGI_FAILURE, None

    else:
        log.info("run_blastplus_py complete.")


    return RQCExitCodes.JGI_SUCCESS, blastOutFileNamePrefix



"""

fast_subsample_fastq_sequences

 Title    : fast_subsample_fastq_sequences

 Function : This function subsamples the data from a specified fastq file.

 Usage    : fast_subsample_fastq_sequences( $seq_unit, sub_fastq, $samplePer, $max_count, \$basecountTotal, $totalReadNum, log)

 Args     : 1) The source fastq file.
            2) The destination fastq file.
            3) The percentage of read subsampling.
            4) The maximum number of reads at which to stop subsampling.
            5) A reference to the variable that will store the
               basecount.
            6) A reference to the variable that will store the
               number of reads.
            7) A reference to a JGI_Log object.

 Returns  : JGI_SUCCESS: The fastq data was successfully sampled.
            JGI_FAILURE: The fastq data could not be sampled.

 Comments : Pass as parameters both the subsample_rate and the read_count
            in order to stop subsampling at the read_count.
            The read_count can also be null, in which case the number
            of reads corresponding to the percentage subsample_rate will be subsampled.

@param fastq:  source fastq file (full path)
@param outputFileName: subsampled output file name (basename)
@param samplePer: sample rate < 1.0
@param statGen: boolean -> generate stats output or not
@param log

@return retCode: success or failure
@return sampledFastaFile: subsampled output file name (full path)
@return basecountTotal: total #bases (to be added to readqc_stats.txt)
@return totalReadNum: total #reads (to be added to readqc_stats.txt)
@return sampledReadNum: total #reads sampled (to be added to readqc_stats.txt)

"""
# # def fast_subsample_fastq_sequences(fasta, outputFileName, samplePer, statGen, log):
# #     ## Tools
# #     reformatCmd = RQCCommands.BBTOOLS_REFORMAT_CMD
# #
# #     outputPath = RQCPacbioQcConfig.CFG["output_path"]
# #     log.info("Sampling %s at %.2f rate", fasta, samplePer)
# #
# #     basecountTotal = 0
# #     totalReadNum = 0
# #     sampledReadNum = 0
# #
# #     sequnitName, exitCode = safe_basename(fasta, log)
# #     assert exitCode == 0
# #     sequnitName = file_name_trim(sequnitName)
# #
# #     subsample_dir = "subsample"
# #     subsample_path = os.path.join(outputPath, subsample_dir)
# #     make_dir(subsample_path)
# #     change_mod(subsample_path, "0755")
# #
# #     sampledFastaFile = os.path.join(subsample_path, outputFileName)
# #
# #     fileSize = os.path.getsize(fasta)
# #     log.info("Source fastq file size = %s", fileSize)
# #
# #     ## Subsampling
# #     sourceFastaFile = fasta
# #
# #     ## Replaced subsampler with reformat.sh
# #     ## subsample with bbtools_reformat_cmd:
# #     ## $ reformat.sh in=7348.8.68143.fastq out=subsample.fastq samplerate=0.01 qout=33
# #     ## - 21G == 180.399 seconds ~ 6x faster than subsample_fastq_pl
# #     ## new subampler from BBTOOLS
# #     ## without qin=33 then it uses auto detect, Illumina is phread64 but we need to convert to phred33
# #     ##reformat.sh in=7257.1.64419.CACATTGTGAG.s1.0.fastq out=temp.out samplerate=0.02 qin=33 qout=33 overwrite
# #
# #     ## 20140820
# #     ## bhist=<file>     Write a base composition histogram to file.             ## Cycle Nucleotide Composition
# #     ## gchist=<file>    Write a gc content histogram to file.                   ## Read GC, mean, std
# #     ## qhist=<file>     Write a quality histogram to file.                      ## Average Base Position Quality
# #     ## bqhist=<file>    Write a quality histogram designed for box plots.       ## Average Base Position Quality Boxplot
# #     ## obqhist=<file>   Write a base quality histogram to file.                 ## Base quality histogram; *.base_qual.stats
# #
# #     reformatLogFile = os.path.join(subsample_path, sequnitName + ".reformat.log")
# #
# #     if statGen:
# #         reformatGchistFile = os.path.join(subsample_path, sequnitName + ".reformat.gchist.txt") ## Read GC
# #         reformatBhistFile = os.path.join(subsample_path, sequnitName + ".reformat.bhist.txt")   ## Cycle Nucleotide Composition
# #         reformatQhistFile = os.path.join(subsample_path, sequnitName + ".reformat.qhist.txt")   ## Average Base Position Quality
# #         reformatBqhistFile = os.path.join(subsample_path, sequnitName + ".reformat.bqhist.txt") ## Average Base Position Quality Boxplot
# #         reformatObqhistFile = os.path.join(subsample_path, sequnitName + ".reformat.obqhist.txt") ## Base quality histogram
# #
# #         subsamplingCmd = " %s in=%s out=%s samplerate=%s qin=33 qout=33 ow=t bhist=%s qhist=%s gchist=%s bqhist=%s obqhist=%s > %s 2>&1 " % \
# #                          (reformatCmd, sourceFastaFile, sampledFastaFile, samplePer,
# #                           reformatBhistFile, reformatQhistFile, reformatGchistFile, reformatBqhistFile, reformatObqhistFile,
# #                           reformatLogFile)
# #
# #     else:
# #         subsamplingCmd = " %s in=%s out=%s samplerate=%s qin=33 qout=33 ow=t > %s 2>&1 " % \
# #                          (reformatCmd, sourceFastaFile, sampledFastaFile, samplePer, reformatLogFile)
# #
# #     _, _, exitCode = run_sh_command(subsamplingCmd, True, log, True)
# #
# #     if exitCode == 0:
# #         ## java -ea -Xmx200m -cp /usr/common/jgi/utilities/bbtools/prod-v33.69/lib/BBTools.jar jgi.ReformatReads in=/global/pro
# #         ## jectb/shared/pacbio/jobs/025/025688/data/filtered_subreads.fastq out=null qin=33 qout=33 ow=t gchist=/global/project
# #         ## b/scratch/sulsj/2014.10.16-pacbioqc-2.0-test/out-pbio-383.4163.fastq/qual/pbio-383.4163.reformat.gchist.txt
# #         ## Executing jgi.ReformatReads [in=/global/projectb/shared/pacbio/jobs/025/025688/data/filtered_subreads.fastq, out=nul
# #         ## l, qin=33, qout=33, ow=t, gchist=/global/projectb/scratch/sulsj/2014.10.16-pacbioqc-2.0-test/out-pbio-383.4163.fastq
# #         ## /qual/pbio-383.4163.reformat.gchist.txt]
# #         ##
# #         ## Set GC histogram output to /global/projectb/scratch/sulsj/2014.10.16-pacbioqc-2.0-test/out-pbio-383.4163.fastq/qual/
# #         ## pbio-383.4163.reformat.gchist.txt
# #         ## Input is being processed as unpaired
# #         ## Input:                   72623 reads             202312769 bases
# #         ## Output:                  72623 reads (100.00%)   202312769 bases (100.00%)
# #         ##
# #         ## Time:                            4.179 seconds.
# #         ## Reads Processed:       72623     17.38k reads/sec
# #         ## Bases Processed:        202m     48.41m bases/sec
# #
# #
# #         ## NEW
# #         if os.path.isfile(reformatLogFile):
# #
# #             with open(reformatLogFile) as LOG_FH:
# #                 for l in LOG_FH.readlines():
# #                     if l.startswith("Input:"):
# #                         toks = l.split()
# #                         assert len(toks) == 5
# #                         totalReadNum = int(toks[1])
# #                         basecountTotal = int(toks[3])
# #                     elif l.startswith("Output:"):
# #                         toks = l.split()
# #                         assert len(toks) == 7
# #                         sampledReadNum = int(toks[1])
# #
# #             log.info("Total base count of input fastq = %s", basecountTotal)
# #             log.info("Total num reads of input fastq = %s", totalReadNum)
# #             log.info("Total num reads of sampled = %s", sampledReadNum)
# #
# #             if totalReadNum > 0 and sampledReadNum > 0:
# #                 ##
# #                 ## TODO: deal with sequnits with small number of reads
# #                 ## How to record the status in readqc.log
# #                 ##
# #                 retCode = RQCExitCodes.JGI_SUCCESS
# #                 log.info("pacbioqc_subsampling complete: output file = %s", sampledFastaFile)
# #
# #             elif totalReadNum > 0 and sampledReadNum <= 0:
# #                 retCode = RQCExitCodes.JGI_FAILURE
# #                 log.error("pacbioqc_subsampling failure. sampledReadNum <= 0.")
# #
# #             else:
# #                 retCode = RQCExitCodes.JGI_FAILURE
# #                 log.error("pacbioqc_subsampling failure. totalReadNum <= 0 and sampledReadNum <= 0.")
# #
# #         else:
# #             retCode = RQCExitCodes.JGI_FAILURE
# #             log.error("pacbioqc_subsampling failure. Can't find stat file from subsampling.")
# #
# #     else:
# #         retCode = RQCExitCodes.JGI_FAILURE
# #         log.error("pacbioqc_subsampling failure, BBTOOLS_REFORMAT_CMD's exit code != 0")
# #
# #
# #     return retCode, sampledFastaFile, basecountTotal, totalReadNum, sampledReadNum



"""
 Title      : read_megablast_hits
 Function   : This function generates tophit list of megablast against different databases.
 Usage      : read_megablast_hits($analysis, $summary_file_dir, $db_name)
 Args       : 1) A reference to an JGI_Analysis object
              2) The reads working folder
              3) Megablast reference sequence database name
 Returns    : JGI_SUCCESS:
              JGI_FAILURE:
 Comments   :

"""
def read_megablast_hits(db, log):
    PB_OUTPUT_PATH = RQCPacbioQcConfig.CFG["output_path"]
    megablastDir = "blast"
    megablastPath = os.path.join(PB_OUTPUT_PATH, megablastDir)

    PB_STATS_FILE = RQCPacbioQcConfig.CFG["stats_file"]
    PB_FILES_FILE = RQCPacbioQcConfig.CFG["files_file"]

    ##
    ## Process blast output files
    ##
    hitCount = 0
    parsedFile = os.path.join(megablastPath, "megablast.*.%s*.parsed" % (db))
    matchings, _, exitCode = run_sh_command("grep -v '^#' %s 2>/dev/null | wc -l " % (parsedFile), True, log, True)

    if exitCode == 0: ## if parsed file found.
        t = matchings.split()

        if len(t) == 1 and t[0].isdigit():
            hitCount = int(t[0])

        append_rqc_stats(PB_STATS_FILE, PacbioQcStats.PACBIO_READ_MATCHING_HITS + " " + db, hitCount, log)

        ##
        ## find and add .parsed file
        ##
        parsedFileFound, _, exitCode = run_sh_command("ls %s" % (parsedFile), True, log)

        if parsedFileFound:
            parsedFileFound = parsedFileFound.strip()
            append_rqc_file(PB_FILES_FILE, PacbioQcStats.PACBIO_READ_PARSED_FILE + " " + db, os.path.join(megablastPath, parsedFileFound), log)

        else:
            log.error("Failed to add megablast parsed file of %s.", db)
            return RQCExitCodes.JGI_FAILURE

        ##
        ## wc the top hits
        ##
        topHit = 0
        tophitFile = os.path.join(megablastPath, "megablast.*.%s*.parsed.tophit" % (db))
        tophits, _, exitCode = run_sh_command("grep -v '^#' %s 2>/dev/null | wc -l " % (tophitFile), True, log, True)

        t = tophits.split()

        if len(t) == 1 and t[0].isdigit():
            topHit = int(t[0])

        append_rqc_stats(PB_STATS_FILE, PacbioQcStats.PACBIO_READ_TOP_HITS + " " + db, topHit, log)


        ##
        ## wc the top 100 hits
        ##
        top100Hit = 0
        top100hitFile = os.path.join(megablastPath, "megablast.*.%s*.parsed.top100hit" % (db))
        top100hits, _, exitCode = run_sh_command("grep -v '^#' %s 2>/dev/null | wc -l " % (top100hitFile), True, log, True)

        t = top100hits.split()

        if len(t) == 1 and t[0].isdigit():
            top100Hit = int(t[0])

        append_rqc_stats(PB_STATS_FILE, PacbioQcStats.PACBIO_READ_TOP_100_HITS + " " + db, top100Hit, log)

        ##
        ## wc the taxonomic species
        ##
        spe = 0
        taxlistFile = os.path.join(megablastPath, "megablast.*.%s*.parsed.taxlist" % (db))
        species, _, exitCode = run_sh_command("grep -v '^#' %s 2>/dev/null | wc -l " % (taxlistFile), True, log, True)

        t = species.split()

        if len(t) == 1 and t[0].isdigit():
            spe = int(t[0])

        append_rqc_stats(PB_STATS_FILE, PacbioQcStats.PACBIO_READ_TAX_SPECIES + " " + db, spe, log)

        ##
        ## find and add taxlist file
        ##
        taxlist, _, exitCode = run_sh_command("ls %s" % (taxlistFile), True, log)
        taxlist = taxlist.strip()

        if taxlist:
            append_rqc_file(PB_FILES_FILE, PacbioQcStats.PACBIO_READ_TAXLIST_FILE + " " + db, os.path.join(megablastPath, taxlist), log)

        else:
            log.error("Failed to add megablast taxlist file of %s.", db)
            return RQCExitCodes.JGI_FAILURE

        ##
        ## find and add tophit file
        ##
        tophitFound, _, exitCode = run_sh_command("ls %s" % (tophitFile), True, log)
        tophitFound = tophitFound.strip()

        if tophitFound:
            append_rqc_file(PB_FILES_FILE, PacbioQcStats.PACBIO_READ_TOPHIT_FILE + " " + db, os.path.join(megablastPath, tophitFound), log)

        else:
            log.error("Failed to add megablast tophit file of %s.", db)
            return RQCExitCodes.JGI_FAILURE

        ##
        ## find and add tophit 100 file
        ##
        top100hitFound, _, exitCode = run_sh_command("ls %s" % (top100hitFile), True, log)
        top100hitFound = top100hitFound.strip()

        if top100hitFound:
            append_rqc_file(PB_FILES_FILE, PacbioQcStats.PACBIO_READ_TOP100HIT_FILE + " " + db, os.path.join(megablastPath, top100hitFound), log)

        else:
            log.error("Failed to add megablast top100hit file of %s.", db)
            return RQCExitCodes.JGI_FAILURE

    else:
        log.warning("No blast hits for %s.", db)


    return RQCExitCodes.JGI_SUCCESS



""" For STEP5
run_megan

@param fullReadsFastaFileName: source fasta file (full path)
@param megablastHitFile: source megablast output file (full path)
@param fullMeganOutDirName: output full path
@param label: input fastq file name for label
@param log

@return retCode: success or failure

"""
# # def run_megan(fullReadsFastaFileName, megablastHitFile, fullMeganOutDirName, label, log):
# #     log.info("meganOutDirName " + str(fullMeganOutDirName));
# #     log.info("megan label " + str(label));
# #     log.info("megan reads " + str(fullReadsFastaFileName) );
# #     log.info("megan megablastHitFile " + str(megablastHitFile) );
# #
# #     ## Add timeout for terminate megan manually after 30min
# #     meganCmd = " timeout -v -t 1800s %s -f %s -b %s -t megablast -w %s -l %s " \
# #                % (RQCBlast.MEGAN_CMD, fullReadsFastaFileName, megablastHitFile, fullMeganOutDirName, label)
# #
# #     stdOut, stdErr, exitCode = run_sh_command(meganCmd, True, log, True)
# #
# #     ## If exitCode == 124 or exitCode = 143, this means the process exits with timeout.
# #     ## Timeout exits with 128 plus the signal number. 143 = 128 + 15 (SGITERM)
# #     ## Ref) http://stackoverflow.com/questions/4189136/waiting-for-a-command-to-return-in-a-bash-script
# #     ##      timeout man page ==> If the command times out, and --preserve-status is not set, then exit with status 124.
# #     ##
# #     if exitCode in [124, 143]:
# #         ## Regard this as Megan timeout case
# #         ## Exit with succuss so that megan can be skipped.
# #         log.warning("MEGAN TIMEOUT. Just skip the megan part.")
# #         retCode = RQCExitCodes.JGI_SUCCESS
# #
# #     elif exitCode == 0:
# #
# #         if os.path.isfile(megablastHitFile):
# #             retCode = RQCExitCodes.JGI_SUCCESS
# #         else:
# #             retCode = RQCExitCodes.JGI_FAILURE
# #             log.error("megan_obj.run_megan failed to create %s", megablastHitFile)
# #
# #     else:
# #         retCode = RQCExitCodes.JGI_FAILURE
# #         log.error("megan_obj.run_megan failure, exit code != 0")
# #
# #
# #     return retCode



""" STEP7 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

cleanup_pacbioqc

Cleaning up the pacbioqc analysis directory with unwanted files.

@param log

@return retCode: always return success

"""
def cleanup_pacbioqc(log):
    outputPath = RQCPacbioQcConfig.CFG["output_path"]

    ## Purge subsampled fasta
    #cmd = " %s %s/%s/*.fasta " % (RQCCommands.RM_CMD, outputPath, "subsample")
    #stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)
    #if exitCode != 0:
    #    log.error("Failed to execute %s; may be already purged." % (cmd))

    ## Delete Blast files
    cmd = "rm %s/%s/megablast*v*JFfTIT " % (outputPath, "blast")
    _, _, exitCode = run_sh_command(cmd, True, log)
    if exitCode != 0:
        log.error("Failed to execute %s; may be already purged.", cmd)

    ## Delete megablast.reads.fa.v.nt.FmLD2a10p90E30JFfTITW45; megablast.reads.fa.v.refseq.microbial.FmLD2a10p90E30JFfTITW45
    cmd = "rm %s/%s/megablast*v*FfTITW45 " % (outputPath, "blast")
    _, _, exitCode = run_sh_command(cmd, True, log)
    if exitCode != 0:
        log.error("Failed to execute %s; may be already purged.", cmd)

    ## Delete Megan files
    cmd = "rm %s/%s/*.rma " % (outputPath, "*.megan")
    _, _, exitCode = run_sh_command(cmd, True, log)
    if exitCode != 0:
        log.error("Failed to execute %s; may be already purged.", cmd)

    cmd = "rm %s/%s/*.fasta " % (outputPath, "*.megan")
    _, _, exitCode = run_sh_command(cmd, True, log)
    if exitCode != 0:
        log.error("Failed to execute %s; may be already purged.", cmd)

    cmd = "rm %s/%s/*.fa " % (outputPath, "*.megan")
    _, _, exitCode = run_sh_command(cmd, True, log)
    if exitCode != 0:
        log.error("Failed to execute %s; may be already purged.", cmd)

    ## Delete fasta from pb_triangle_read_finder
    cmd = "rm %s/triangle/*.fasta " % (outputPath)
    _, _, exitCode = run_sh_command(cmd, True, log)
    if exitCode != 0:
        log.error("Failed to execute %s; may be already purged.", cmd)

    ## Delete 2nd subsampled fasta from subsample
    cmd = "rm %s/subsample/*.fasta " % (outputPath)
    _, _, exitCode = run_sh_command(cmd, True, log)
    if exitCode != 0:
        log.error("Failed to execute %s; may be already purged.", cmd)


    return RQCExitCodes.JGI_SUCCESS


## EOF
