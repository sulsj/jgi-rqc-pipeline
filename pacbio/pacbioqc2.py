#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
PacBioqc pipeline II

Command
    $ pacbioqc2.py --fastq 6145.5.40009.fastq --output-path in-progress/00/00/00/05/

Outputs
  - stats_log = pacbioqc_stats.txt
  - file_log = pacbioqc_files.txt
  - status_log = pacbioqc_status.log


Created: Oct 15 2014

sulsj (ssul@lbl.gov)


History:

    20141015 0.0.9: init

    20141029 1.0.0: Tested and reviewed

    20141030 1.1.0: Added Json file parsing
    20141104 1.1.1: Removed subsampling option
    20141107 1.1.2: Added subsampling option (sample rate = 25%)
    20141117 1.1.3: Removed "rqc-" from the staged sequnit name

    20150720 1.2.0: Added timeout for megan (timeout -v -t 1800s megan.... ) and blast (timeout -v -t 21600s)
    20150817 1.2.1: Replaced smartcell folder

    20150904 1.3.0: Added option flags for skipping Blast search against Refseq.microbial (-b) and Nt (-n)

    20150916 2.0.0: Cleaned!
    20151008 2.0.1: Replaced run_blastn() with run_blast.pl using Jigsaw
    20151016 2.0.2: Added module load jigsaw for run_blast.pl
    20151021 2.0.3: Released! c52e0b8
    20151022 2.0.4: Updated blast step

    20151109 2.1.0: Updated to do the Blast search against refseq.archaea and refseq.bacteria instead of refseq.microbial
    20151111 2.1.0: Released! 0585e15

    20160329 2.2.0: Replaced Jigsaw's run_blast.pl with run_blastplus.py
    20160728 2.2.1: Added color log
    20160931 2.2.2: Removed unnecessary localizations in blast run; Updated with new localize_dir2()

    20161020 2.3.0: Added triangle read analysis (pb_triangle_read_finder.py)

    20161027 3.0.0: Combined with qcLongReads.pl (Enable Blast against NT in pacbio2 and skip it from qcLongReads.pl; Skip megan)
    20161130 3.0.1: Changed to run fastqc in qcLongReads.pl by qsub b/c of Java out-of-mem issue
    20161212 3.0.2: Changed the subsampling rate from rate 0.25 to number 50,000
    20170518 3.1.0: Updated to process Sequel data (--sequel flag)
    20170522 3.2.0: Updated to run on Cori

    20170531 3.3.0: Updated to use run_blastplus_taxserver.py
    20170601 3.3.1: Added 'script.concatFastqFiles' to qcLongReads config

    20170627 3.4.0: Added sketch vs nt
    20170710 3.4.1: Fixed fasta file name setting in sequel

    20170723 4.0.0: Removed qcLongReads.pl for running on Cori (using pacbioSubreadQc.pl)
    20170810 4.0.0: Released! 95c4269

    20170814 4.1.0: Added sketch vs refseq and silva

    20170814 4.2.0: Added Chip eval call (pacbioChipEval.py)
    20170906 4.2.1: Fixed skip blast option
    20170928 4.2.2: Added "printtaxa=t" for sendsketch.sh
    20171020 4.2.3: Added depth depth2 minprob=0 to sendsketch.sh
    20171106 4.2.4: Added unique2 to sendsketch.sh

    20171105 5.0.0: Rewrote perl tools to python
    20171121 5.0.1: Cleaned up pacbio read qc
    20171205 5.0.2: Skip the creation of qual by pos plot if fails
    20171214 5.0.3: Added normalized nucleotide composition plot
    20180207 5.0.4: Skip the creation of qual by pos plot if fails (2)
    20180416 5.1.0: Enabled triangular analysis for Sequel


"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use
import os
import sys
import argparse
import glob
import gzip
import re
import collections
import json

## plotting
import numpy as np
import matplotlib
matplotlib.use('Agg') ## This needs to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import mpld3
# from matplotlib.ticker import AutoMinorLocator, MultipleLocator, FormatStrFormatter
from pylab import *

## custom libs in "../lib/"
srcRootDir = os.path.dirname(__file__)
sys.path.append(os.path.join(srcRootDir, "tools"))  ## pacbio/tools
sys.path.append(os.path.join(srcRootDir, "../lib"))    ## rqc-pipeline/lib
sys.path.append(os.path.join(srcRootDir, "../tools"))  ## rqc-pipeline/tools

from common import *
from rqc_utility import *
from rqc_constants import *
from pacbioqc2_constants import *
from pacbioqc2_utils import *
from os_utility import make_dir, change_mod, run_sh_command

## Global
VERSION = "5.1.0"
DEBUG = False
color = {}
color = set_colors(color, True)
scriptName = __file__
TYPES = {'raw': 'Full_Read', 'subread': 'Subread', 'longestSubread': 'Longest_Subread'}



"""
STEP1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_collect_smrtcell_results(run_folder, isSequel, log):
    log.info("\n\n%sSTEP1 - Collecting PacBio SMRTCELL outputs <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    
    status = "1_pacbio_pacbioqc_data_pulling in progress"
    log.info(status)
    checkpoint_step_wrapper(status)
    
    filesFile = RQCPacbioQcConfig.CFG["files_file"]
    statsFile = RQCPacbioQcConfig.CFG["stats_file"]

    if not isSequel:
        ## NEW
        pngFileList = [PacbioQcStats.ADAPTER_OBSERVED_INSERT_LENGTH_DISTRIBUTION_PNG,
                       PacbioQcStats.POST_FILTER_READLENGTH_HISTOGRAM_PNG,
                       PacbioQcStats.PRE_FILTERREAD_SCORE_HISTOGRAM_PNG,
                       PacbioQcStats.POST_FILTERREAD_SCORE_HISTOGRAM_PNG,
                       PacbioQcStats.PRE_FILTER_READLENGTH_HISTOGRAM_PNG,
                       PacbioQcStats.FILTERED_SUBREAD_REPORT_PNG,
                       PacbioQcStats.CONTROL_NON_CONTROL_READQUALITY_PNG,
                       PacbioQcStats.CONTROL_NON_CONTROL_READLENGTH_PNG]

        for f in pngFileList:
            pngF = os.path.join(run_folder, "results/" + f)
            if os.path.isfile(pngF):
                append_rqc_file(filesFile, f, pngF, log)
            else:
                log.warning("Plot file, %s not found.\n", pngF)


        ## Parse json files
        # statsFile = RQCPacbioQcConfig.CFG["stats_file"]

        jsonFileList = [PacbioQcStats.OVERVIEW_JSON,
                        PacbioQcStats.FILTER_REPORTS_ADAPTERS_JSON,
                        PacbioQcStats.FILTER_REPORTS_LOADING_JSON,
                        PacbioQcStats.FILTER_REPORTS_FILTER_STATS_JSON,
                        PacbioQcStats.FILTER_REPORTS_FILTER_SUBREAD_STATS_JSON,
                        PacbioQcStats.CONTROL_REPORT_JSON]


        ## JSON file samples

        ## overview.json
        ##
        ##{u'_changelist': 132105,
        ##u'_version': u'2.3',
        ##u'attributes': [{u'id': u'overview.ncells',
        ##                 u'name': u'SMRT Cells',
        ##                 u'value': 1},
        ##                {u'id': u'overview.nmovies',
        ##                 u'name': u'Movies',
        ##                 u'value': 1}],
        ##u'id': u'overview',
        ##u'plotGroups': [],
        ##u'tables': []}

        ## FILTER_REPORTS_ADAPTERS_JSON
        ##
        ##{u'_changelist': 132105,
        ##u'_version': u'2.3',
        ##u'attributes': [{u'id': u'adapter.adapter_dimers',
        ##                 u'name': u'Adapter Dimers',
        ##                 u'value': 0.00012642056796103584},
        ##                 ...
        ##                {u'id': u'adapter.hq_medium_inserts',
        ##                 u'name': u'Medium Inserts',
        ##                 u'value': 0.000556166944750139}],
        ##u'id': u'adapter',
        ##u'plotGroups': [{u'id': u'adapter.observed_insert_length_distribution',
        ##                 u'legend': None,
        ##                 u'plots': [{u'caption': None,
        ##                             u'id': u'adapter.observed_insert_length_distribution.plot1',
        ##                             u'image': u'adapter_observed_insert_length_distribution.png'}],
        ##                 u'thumbnail': u'adapter_observed_insert_length_distribution_thumb.png',
        ##                 u'title': u'Observed Insert Length Distribution'}],
        ##u'tables': []}


        for f in jsonFileList:
            jsonF = os.path.join(run_folder, "results", f)

            if os.path.isfile(jsonF):
                append_rqc_file(filesFile, f, jsonF, log)

                with open(jsonF) as JF:
                    jsonData = json.load(JF)

                    ## stats
                    for i in jsonData['attributes']:
                        ##overview.ncells 1
                        ##overview.nmovies 1
                        ##adapter.adapter_dimers 0.000126420567961
                        ##adapter.short_inserts 3.99222846193e-05
                        ##adapter.medium_inserts 0.000385915417986
                        ##adapter.hq_adapter_dimers 0.00020116676725
                        ##adapter.hq_short_inserts 4.7333357e-05
                        ##adapter.hq_medium_inserts 0.00055616694475
                        ##filtering_report.mean_read_score_pre_filter 0.445186197535
                        ##filtering_report.base_n_post_filter 641454265
                        ##filtering_report.reads_n_post_filter 70972
                        ##filtering_report.mean_read_length_pre_filter 4823
                        ##filtering_report.mean_read_score_post_filter 0.82545796934
                        ##filtering_report.n50_read_length_post_filter 13129
                        ##filtering_report.mean_read_length_post_filter 9038
                        ##filtering_report.n50_read_length_pre_filter 12250
                        ##filtering_report.reads_n_pre_filter 150292
                        ##filtering_report.base_n_pre_filter 724884099
                        ##filter_subread.filter_subread_mean 6272
                        ##filter_subread.filter_subread_n50 9173
                        ##filter_subread.filter_subread_nbases 639749527
                        ##filter_subread.filter_subread_nreads 101986

                        append_rqc_stats(statsFile, i['id'], i['value'], log)

                    ## plots
                    #for i in jsonData['plotGroups']:
                    #    for image in i['plots']:
                    #        print ",".join([i['id'], i['title'], image['id'], image['image'], image['caption'] if image['caption'] else "N/A"])
        
            else:
                log.warning("Json file, %s not found.\n", jsonF)

    else:
        ## Sequel data import
        ## PNG
        # ./dataset-reports/control/readlength_plot.png
        # ./dataset-reports/control/concordance_plot.png
        # ./dataset-reports/loading_xml/raw_read_length_plot.png
        # ./dataset-reports/adapter_xml/interAdapterDist0.png
        # ./dataset-reports/filter_stats_xml/insertLenDist0.png
        # ./dataset-reports/filter_stats_xml/readLenDist0.png
        ## JSON
        # ./workflow/datastore.json
        # ./dataset-reports/datastore.json
        # ./datastore.json
        pngFileList = [
            "control/readlength_plot.png",
            "ontrol/concordance_plot.png",
            "loading_xml/raw_read_length_plot.png",
            "adapter_xml/interAdapterDist0.png",
            "filter_stats_xml/insertLenDist0.png",
            "filter_stats_xml/readLenDist0.png"]

        for f in pngFileList:
            pngF = os.path.join(run_folder, "dataset-reports", f)
            if os.path.isfile(pngF):
                append_rqc_file(filesFile, f.replace('/', '_').replace('.', '_'), pngF, log)
            else:
                log.warning("Plot file, %s not found.\n", pngF)
                
        jsonFileList = [
            "workflow/datastore.json",
            "dataset-reports/datastore.json",
            "datastore.json"]
        
        for f in jsonFileList:
            jsonF = os.path.join(run_folder, f)
            if os.path.isfile(jsonF):
                append_rqc_file(filesFile, f.replace('/', '_').replace('.', '_'), jsonF, log)
            else:
                log.warning("Json file, %s not found.\n", jsonF)
                

        append_rqc_stats(statsFile, "is_sequel", "1", log)

    
    status = "1_pacbio_pacbioqc_data_pulling complete"
    log.info(status)
    checkpoint_step_wrapper(status)


    return status



"""
STEP2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@param fastq: the input fastq file name (just used for creating output file name)
@param filteredFastqFile: actual data fastq for gc analysis ($SEQ_FS)

"""
def do_pacbio_read_gc(fastq, filteredFastqFile, log):
    ## Tools
    reformatShCmd = get_tool_path("reformat.sh", "bbtools")

    log.info("\n\n%sSTEP2 - Making read GC histograms <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    
    status = "2_pacbio_read_gc in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    filesFile = RQCPacbioQcConfig.CFG["files_file"]
    statsFile = RQCPacbioQcConfig.CFG["stats_file"]

    seqUnitName, exitCode = safe_basename(fastq, log)
    seqUnitName = file_name_trim(seqUnitName)

    qualOutDir = "qual"
    qualOutPath = os.path.join(outputPath, qualOutDir)
    make_dir(qualOutPath)
    change_mod(qualOutPath, "0755")

    reformatLogFile = os.path.join(qualOutPath, seqUnitName + ".reformat.log")
    reformatGchistFile = os.path.join(qualOutPath, seqUnitName + ".reformat.gchist.txt") ## Read GC

    totalBaseCount = 0
    totalReadNum = 0

    subsamplCmd = "%s in=%s out=null qin=33 qout=33 ow=t gchist=%s > %s 2>&1" % \
                  (reformatShCmd, filteredFastqFile, reformatGchistFile, reformatLogFile)
    _, _, exitCode = run_sh_command(subsamplCmd, True, log)

    if exitCode == 0:
        if os.path.isfile(reformatLogFile):
            ## java -ea -Xmx200m -cp /usr/common/jgi/utilities/bbtools/prod-v33.69/lib/BBTools.jar jgi.ReformatReads in=/global/pro
            ## jectb/shared/pacbio/jobs/025/025688/data/filtered_subreads.fastq out=null qin=33 qout=33 ow=t gchist=/global/project
            ## b/scratch/sulsj/2014.10.16-pacbioqc-2.0-test/out-pbio-383.4163.fastq/qual/pbio-383.4163.reformat.gchist.txt
            ## Executing jgi.ReformatReads [in=/global/projectb/shared/pacbio/jobs/025/025688/data/filtered_subreads.fastq, out=nul
            ## l, qin=33, qout=33, ow=t, gchist=/global/projectb/scratch/sulsj/2014.10.16-pacbioqc-2.0-test/out-pbio-383.4163.fastq
            ## /qual/pbio-383.4163.reformat.gchist.txt]
            ##
            ## Set GC histogram output to /global/projectb/scratch/sulsj/2014.10.16-pacbioqc-2.0-test/out-pbio-383.4163.fastq/qual/
            ## pbio-383.4163.reformat.gchist.txt
            ## Input is being processed as unpaired
            ## Input:                   72623 reads             202312769 bases
            ## Output:                  72623 reads (100.00%)   202312769 bases (100.00%)
            ##
            ## Time:                            4.179 seconds.
            ## Reads Processed:       72623     17.38k reads/sec
            ## Bases Processed:        202m     48.41m bases/sec

            with open(reformatLogFile) as LOG_FH:
                for l in LOG_FH.readlines():
                    if l.startswith("Input:"):
                        toks = l.split()
                        assert len(toks) == 5
                        totalReadNum = int(toks[1])
                        totalBaseCount = int(toks[3])
                    elif l.startswith("Output:"):
                        toks = l.split()
                        assert len(toks) == 7
                        #read_sampled = int(toks[1])

            log.info("Total base count of input fastq = %s", totalBaseCount)
            log.info("Total num reads of input fastq = %s", totalReadNum)

            append_rqc_stats(statsFile, PacbioQcStats.PACBIO_TOTAL_BASE_COUNT, totalBaseCount, log)
            append_rqc_stats(statsFile, PacbioQcStats.PACBIO_TOTAL_READ_COUNT, totalReadNum, log)


        ##
        ## MPLD3 plotting
        ##
        meanVal = None
        medVal = None
        modeVal = None
        stdevVal = None

        ## File format
        ## #Mean    46.992
        ## #Median  48.000
        ## #Mode    49.000
        ## #STDev   6.221
        ## #GC  Count
        ## 0.0  18
        ## 1.0  0
        ## 2.0  0
        ## 3.0  0

        with open(reformatGchistFile, "r") as STAT_FH:
            for l in STAT_FH.readlines():
                if l.startswith("#Mean"):
                    meanVal = l.strip().split("\t")[1]
                elif l.startswith("#Median"):
                    medVal = l.strip().split("\t")[1]
                elif l.startswith("#Mode"):
                    modeVal = l.strip().split("\t")[1]
                elif l.startswith("#STDev"):
                    stdevVal = l.strip().split("\t")[1]


        rawDataMatrix = np.loadtxt(reformatGchistFile, delimiter="\t", comments='#', skiprows=1)
        assert len(rawDataMatrix[1][:]) == 2

        fig, ax = plt.subplots()

        markerSize = 5.0
        lineWidth = 1.5

        p1 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 1], 'r', marker='o', markersize=markerSize, linewidth=lineWidth, alpha=0.5)

        ax.set_xlabel('%GC', fontsize=12, alpha=0.5)
        ax.set_ylabel('Reads count', fontsize=12, alpha=0.5)
        #ax.set_xlim([0.0, 1.0])
        ax.grid(color='gray', linestyle=':')

        ## Add tooltip
        mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p1[0], labels=list(rawDataMatrix[:, 1])))

        pngFileName = os.path.join(qualOutPath, seqUnitName + ".gchist.png")
        htmlFileName = os.path.join(qualOutPath, seqUnitName + ".gchist.html")

        ## Save D3 interactive plot in html format
        mpld3.save_html(fig, htmlFileName)

        ## Save Matplotlib plot in png format
        plt.savefig(pngFileName, dpi=fig.dpi)

        append_rqc_stats(statsFile, PacbioQcStats.PACBIO_READ_GC_MEAN, meanVal, log)
        append_rqc_stats(statsFile, PacbioQcStats.PACBIO_READ_GC_MEDIAN, medVal, log)
        append_rqc_stats(statsFile, PacbioQcStats.PACBIO_READ_GC_MODE, modeVal, log)
        append_rqc_stats(statsFile, PacbioQcStats.PACBIO_READ_GC_STD, stdevVal, log)

        append_rqc_file(filesFile, PacbioQcStats.PACBIO_READ_GC_TEXT, reformatGchistFile, log)
        append_rqc_file(filesFile, PacbioQcStats.PACBIO_READ_GC_PLOT, pngFileName, log)
        append_rqc_file(filesFile, PacbioQcStats.PACBIO_READ_GC_D3_HTML_PLOT, htmlFileName, log)
        
        status = "2_pacbio_read_gc complete"
        log.info(status)

    else:
        log.error("illumina_readqc_subsampling failure, exit code != 0")        
        status = "2_pacbio_read_gc failed"
        log.info(status)



    ## Normalized Nucleotide Composition
    ret = normalized_comp(fastq, filteredFastqFile, totalBaseCount, log)
    assert ret == 0, "Failed to create nbhist plot."



    checkpoint_step_wrapper(status)


    return status, totalReadNum



def normalized_comp(fastq, pb_file, totalBaseCnt, log):
    log.info("Creating normalized nucleotide composition data: fastq file = %s", pb_file)

    seqUnitName, exitCode = safe_basename(fastq, log)
    seqUnitName = file_name_trim(seqUnitName)
    qualOutPath = os.path.join(outputPath, "qual")

    filesFile = RQCPacbioQcConfig.CFG["files_file"]
    nbhistFile = os.path.join(qualOutPath, seqUnitName + ".reformat.nbhist.txt")

    read_size = 100.0

    fh = None
    if pb_file.endswith(".gz"):
        fh = gzip.open(pb_file, "r")
    else:
        fh = open(pb_file, "r")

    cnt = 0
    r_cnt = 0

    acgt_dict = {} # pos: { "a" : #, "c" : #, ...}

    for line in fh:
        line = line.strip()
        cnt += 1

        if cnt == 2:
            r_cnt += 1
            #line = line[0:300]
            #print len(line), len(line)/read_size
            w_size = int(math.ceil(len(line)/read_size))

            r = int(math.ceil(len(line)/float(w_size)))

            # print "Read: %s, length: %s, w_size: %s, r: %s" % (r_cnt, len(line), w_size, r)
            #print line

            w_cnt = 0
            x = w_cnt
            y = x + w_size
            pos = 1

            for i in range(r):
                sub_read = line[x:y]
                a_cnt, c_cnt, g_cnt, t_cnt = acgt_cnt(sub_read)
                #print "pos: %s, sub_read: %s, A: %s, C: %s, G: %s, T: %s" % (pos, sub_read, a_cnt, c_cnt, g_cnt, t_cnt)
                p = "p%s" % i
                if p in acgt_dict:
                    acgt_dict[p]['a'] += a_cnt
                    acgt_dict[p]['c'] += c_cnt
                    acgt_dict[p]['g'] += g_cnt
                    acgt_dict[p]['t'] += t_cnt
                else:
                    acgt_dict[p] = {"a": a_cnt, "c": c_cnt, "g": g_cnt, "t": t_cnt}

                x = y
                y = x + w_size
                pos += 1

        if cnt == 4:
            cnt = 0

    fh.close()

    with open(nbhistFile, 'w') as bhistFH:
        bhistFH.write("#POS\tA\tC\tG\tT\tA%\tC%\tG%\tT%\n")
        # for i in range(1, 101):
        for i in range(1, 100):
            p = "p%s" % i
            # p = "%s" % i
            a = 0
            c = 0
            g = 0
            t = 0
            s = 0
            a_p = 0.0
            c_p = 0.0
            g_p = 0.0
            t_p = 0.0
            if p in acgt_dict:
                a = acgt_dict[p]['a']
                c = acgt_dict[p]['c']
                g = acgt_dict[p]['g']
                t = acgt_dict[p]['t']
                s = a + c + g + t
                a_p = float(a) / s
                c_p = float(c) / s
                g_p = float(g) / s
                t_p = float(t) / s

            # print "%s|%s|%s|%s|%s" % (p, a, c, g, t)
            bhistFH.write("%s\t%s\t%s\t%s\t%s\t%.4f\t%.4f\t%.4f\t%.4f\n" % (p, a, c, g, t, a_p, c_p, g_p, t_p))

    ## Plotting
    log.info("Creating normalized nucleotide composition plot: nbhist file = %s", nbhistFile)
    pngFile = os.path.join(qualOutPath, seqUnitName + ".nbhist.png")

    rawDataMatrix = np.loadtxt(nbhistFile, delimiter='\t', comments='#', dtype='str')
    assert len(rawDataMatrix[1][:]) == 9

    fig, ax = plt.subplots()

    markerSize = 2.5
    lineWidth = 1.0
    xData = [x.replace('p', "") for x in rawDataMatrix[:, 0]]
    ax.plot(xData, rawDataMatrix[:, 5], 'r', marker='o', markersize=markerSize, linewidth=lineWidth, label='A', alpha=0.5)
    ax.plot(xData, rawDataMatrix[:, 6], 'm', marker='d', markersize=markerSize, linewidth=lineWidth, label='C', alpha=0.5)
    ax.plot(xData, rawDataMatrix[:, 7], 'b', marker='*', markersize=markerSize, linewidth=lineWidth, label='G', alpha=0.5)
    ax.plot(xData, rawDataMatrix[:, 8], 'g', marker='s', markersize=markerSize, linewidth=lineWidth, label='T', alpha=0.5)

    ax.set_xlabel("Normalized Read Position", fontsize=12, alpha=0.5)
    ax.set_ylabel("Fraction", fontsize=12, alpha=0.5)
    # ax.set_ylim([0, np.max(np.max(rawDataMatrix[:, 5]), np.max(rawDataMatrix[:, 6]), np.max(rawDataMatrix[:, 7]), np.max(rawDataMatrix[:, 8]))])
    ax.set_ylim([0, 0.1 + max(max([float(i) for i in rawDataMatrix[:, 5]]), max([float(i) for i in rawDataMatrix[:, 6]]), max([float(i) for i in rawDataMatrix[:, 7]]), max([float(i) for i in rawDataMatrix[:, 8]]))])

    fontProp = FontProperties()
    fontProp.set_size("small")
    fontProp.set_family("Bitstream Vera Sans")

    ax.legend(loc=0, prop=fontProp)
    ax.grid(color="gray", linestyle=':')

    ## Save Matplotlib plot in png format
    plt.savefig(pngFile, dpi=fig.dpi)

    if os.path.isfile(nbhistFile):
        append_rqc_file(filesFile, "PACBIO_READ_nucleotide_comp_TEXT", nbhistFile, log)
    else:
        log.error("Failed to create nbhist data file")
        return -1

    if os.path.isfile(pngFile):
        append_rqc_file(filesFile, "PACBIO_READ_nucleotide_comp_PLOT", pngFile, log)
    else:
        log.error("Failed to create nbhist plot file")
        return -1


    return 0



def acgt_cnt(my_str):
    a_cnt = 0
    c_cnt = 0
    g_cnt = 0
    t_cnt = 0
    my_str = my_str.upper()
    for base in my_str:
        if base == "A":
            a_cnt += 1
        elif base == "C":
            c_cnt += 1
        elif base == "G":
            g_cnt += 1
        elif base == "T":
            t_cnt += 1

    return a_cnt, c_cnt, g_cnt, t_cnt



"""
STEP3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_pacbio_subsampling_read_blastn(firstSubsampledFastaFile, skip_subsampling, sampledReadNum, log):
    log.info("\n\n%sSTEP3 - Run subsampling for Blast search <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    
    status = "3_pacbio_subsampling_read_megablast in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    statsFile = RQCPacbioQcConfig.CFG["stats_file"]

    subsampleDir = "subsample"
    subsamplePath = os.path.join(outputPath, subsampleDir)
    make_dir(subsamplePath)
    change_mod(subsamplePath, "0755")

    subsampledFastaFile = None
    totalReadNum = 0
    readNumToReturn = 0
    readNum = 0

    ## If restarted for some reason
    if sampledReadNum == 0:
        cmd = " ".join(["grep", "-c", "'^>'", firstSubsampledFastaFile])
        stdOut, _, exitCode = run_sh_command(cmd, True, log)

        if exitCode != 0:
            log.error("Failed to run cmd, %s", cmd)
            return "3_pacbio_subsampling_read_megablast failed", None, None, None

        else:
            readNum = int(stdOut)

    else:
        readNum = sampledReadNum


    # subsamplingRate = RQCPacbioQc.PACBIO_SAMPLE_PERCENTAGE  ## = 0.25 (25% subsampling)
    subasmplingMaxReadNum = RQCPacbioQc.PACBIO_SAMPLE_COUNT ## = 50000

    subsampledReadNum = -1
    bIsPaired = False

    log.info("Read num = %d", readNum)

    if skip_subsampling:
        log.debug("No subsampling for blastn. Use fastq file, %s (readnum = %s) as query for megablast.", firstSubsampledFastaFile, readNum)
        subsampledFastaFile = firstSubsampledFastaFile
        totalReadNum = readNum
        readNumToReturn = readNum
        log.info("3_pacbio_subsampling_read_megablast skipped.\n")

    else:
        if readNum > subasmplingMaxReadNum:
            log.debug("Run the 2nd subsampling for running blastn.")

            seqUnitName, exitCode = safe_basename(firstSubsampledFastaFile, log)

            bbtoolsReformatShCmd = get_tool_path("reformat.sh", "bbtools")

            reformatLogFile = os.path.join(subsamplePath, seqUnitName + ".reformat2.log")
            secondSubsampledFastaFile = os.path.join(subsamplePath, seqUnitName + ".n" + str(subasmplingMaxReadNum) + ".fasta")
            seqUnitName = file_name_trim(seqUnitName)

            subsampleCmd = "%s in=%s out=%s ow=t samplereadstarget=%s > %s 2>&1" % \
                           (bbtoolsReformatShCmd, firstSubsampledFastaFile, secondSubsampledFastaFile, subasmplingMaxReadNum, reformatLogFile)
            stdOut, _, exitCode = run_sh_command(subsampleCmd, True, log, True)

            if exitCode == 0:
                if os.path.isfile(reformatLogFile):
                    with open(reformatLogFile) as STAT_FH:
                        for l in STAT_FH.readlines():
                            if l.startswith("Input:"):
                                toks = l.split()
                                totalBaseCount = int(toks[3])
                                totalReadNum = int(toks[1])
                            elif l.startswith("Output:"):
                                toks = l.split()
                                subsampledReadNum = int(toks[1])
                            elif l.startswith("Input is being processed as"):
                                toks = l.split()
                                if toks[-1].strip() == "paired":
                                    bIsPaired = True

                    log.info("Total base count of input fastq = %s", totalBaseCount)
                    log.info("Total num reads of input fastq = %s", totalReadNum)
                    log.info("Total num reads of sampled = %s", subsampledReadNum)

                    readLength = int(totalBaseCount / totalReadNum)

                    log.info("Read length = %d", readLength)
                    log.info("Paired = %s", bIsPaired)

                subsampledFastaFile = secondSubsampledFastaFile
                readNumToReturn = subsampledReadNum

            else:
                log.error("Second subsampling failed.")
                return "3_pacbio_subsampling_read_megablast failed", None, None, None

        else:
            log.debug("The readNum is smaller than subasmplingMaxReadNum. The 2nd sampling skipped.")
            subsampledFastaFile = firstSubsampledFastaFile
            totalReadNum = readNum
            readNumToReturn = readNum


    append_rqc_stats(statsFile, PacbioQcStats.PACBIO_SAMPLED_READS_NUMBER, readNumToReturn, log)
    
    status = "3_pacbio_subsampling_read_megablast complete"
    log.info(status)
    checkpoint_step_wrapper(status)


    return status, subsampledFastaFile, totalReadNum, readNumToReturn



"""
STEP4 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_pacbio_read_blastn_refseq_archaea(subsampledFastaFile, log, blastDbPath=None):
    log.info("\n\n%sSTEP4 - Run read blastn against refseq archaea <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    
    status = "4_pacbio_read_blastn_refseq_archaea in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    retCode = pacbio_read_blastn_refseq_archaea(subsampledFastaFile, log, blastDbPath=blastDbPath)

    if retCode == RQCExitCodes.JGI_FAILURE:        
        status = "4_pacbio_read_blastn_refseq_archaea failed"
        log.info(status)

    elif retCode == -143: ## timeout        
        status = "4_pacbio_read_blastn_refseq_archaea complete"
        log.info("4_pacbio_read_blastn_refseq_archaea timeout.")

    else:
        ret2 = read_megablast_hits("refseq.archaea", log)

        if ret2 != RQCExitCodes.JGI_SUCCESS:
            log.error("Failed to run read_megablast_hits() of refseq.archaea")
            log.info("4_pacbio_read_blastn_refseq_archaea reporting failed.")
            status = "4_pacbio_read_blastn_refseq_archaea failed"

        else:            
            status = "4_pacbio_read_blastn_refseq_archaea complete"
            log.info(status)

    checkpoint_step_wrapper(status)


    return status



"""
STEP5 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_pacbio_read_blastn_refseq_bacteria(subsampledFastaFile, log, blastDbPath=None):
    log.info("\n\n%sSTEP5 - Run read blastn against refseq bacteria <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    
    status = "5_pacbio_read_blastn_refseq_bacteria in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    retCode = pacbio_read_blastn_refseq_bacteria(subsampledFastaFile, log, blastDbPath=blastDbPath)

    if retCode == RQCExitCodes.JGI_FAILURE:        
        status = "5_pacbio_read_blastn_refseq_bacteria failed"
        log.info(status)

    elif retCode == -143: ## timeout
        log.info("5_pacbio_read_blastn_refseq_bacteria timeout.")
        status = "5_pacbio_read_blastn_refseq_bacteria complete"

    else:
        ret2 = read_megablast_hits("refseq.bacteria", log)

        if ret2 != RQCExitCodes.JGI_SUCCESS:
            log.error("Failed to run read_megablast_hits() of refseq.bacteria")
            log.info("5_pacbio_read_blastn_refseq_bacteria reporting failed.")
            status = "5_pacbio_read_blastn_refseq_bacteria failed"

        else:            
            status = "5_pacbio_read_blastn_refseq_bacteria complete"
            log.info(status)

    checkpoint_step_wrapper(status)


    return status



"""
STEP6 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_pacbio_read_blastn_nt(subsampledFastaFile, log, blastDbPath=None):
    log.info("\n\n%sSTEP6 - Run read blastn against nt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    
    status = "6_pacbio_read_blastn_nt in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    retCode = pacbio_read_blastn_nt(subsampledFastaFile, log, blastDbPath=blastDbPath)

    if retCode == RQCExitCodes.JGI_FAILURE:        
        status = "6_pacbio_read_blastn_nt failed"
        log.info(status)

    elif retCode == -143: ## timeout
        log.info("6_pacbio_read_blastn_nt timeout.")
        status = "6_pacbio_read_blastn_nt complete"

    else:
        ret2 = read_megablast_hits("nt", log)

        if ret2 != RQCExitCodes.JGI_SUCCESS:
            log.error("Failed to run read_megablast_hits() of nt")
            log.info("6_pacbio_read_blastn_nt reporting failed.")
            status = "6_pacbio_read_blastn_nt failed"

        else:            
            status = "6_pacbio_read_blastn_nt complete"
            log.info(status)

    checkpoint_step_wrapper(status)


    return status



"""
STEP7 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Run pb_triangle_read_finder.py for triangle reads analysis and record stats

pb_triangle_read_finder.py
Run example:
  pb_triangle_read_finder.py -i /global/dna/shared/data/functests/assembly/Microbe/Actinomadura_rifamycini_DSM_43936/0010/Analysis_Results/m140619_000846_42175_c000583142559900001500000112311631_s1_p0.3.subreads.fasta -o test.txt -s summary.txt

output files:
  summary.txt
  test.txt          : blat hit list
  test.whitelist    : list of ZMWs lacking triangle reads
  test.blacklist2   : list of ZMWs contains triangle reads (long format)
  test.blacklist    : list of ZMWs contains triangle reads (short format)

ex) summary.txt
summary:
========
files processed:
 - /global/dna/shared/data/functests/assembly/Microbe/Actinomadura_rifamycini_DSM_43936/0010/Analysis_Results/m14061
9_000846_42175_c000583142559900001500000112311631_s1_p0.3.subreads.fasta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 cellid: m140619_000846_42175_c000583142559900001500000112311631_s1_p0

total_reads triangle_reads  triangle_read_pct
-------------------------------------------------
10491       2               0.01906
-------------------------------------------------

total_zmw   zmw_with_triangle_reads pct
-----------------------------------------------------------------
10067       0                       0.0000
-----------------------------------------------------------------

-----------------------------------------------------------------
zmwid       total_reads triangle_reads  triangle_read_pct
-----------------------------------------------------------------
139605      5           1               20.00
110999      2           1               50.00
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


m140619_000846_42175_c000583142559900001500000112311631_s1_p0/110999/0_11666
m140619_000846_42175_c000583142559900001500000112311631_s1_p0/139605/0_4767

"""
def do_pb_triangle_read_finder(fastq, log):
    log.info("\n\n%sSTEP7 - triangle read analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "7_pb_triangle_read_finder in progress"
    checkpoint_step_wrapper(status)

    triangleOutDir = "triangle"
    triangleOutPath = os.path.join(outputPath, triangleOutDir)
    make_dir(triangleOutPath)
    change_mod(triangleOutPath, "0755")

    oldDir = os.getcwd()
    os.chdir(triangleOutPath)

    formattedFasta, exitCode = safe_basename(fastq, log)
    fastaFileName = formattedFasta.replace(".fastq", ".fasta")
    fastaFilePath = os.path.join(triangleOutPath, fastaFileName)

    ## fastq => fasta
    seqfsFasta = fastq.replace(".fastq", ".fasta")
    if not os.path.isfile(seqfsFasta):
        reformatShCmd = get_tool_path("reformat.sh", "bbtools")
        reformatCmd = "%s in=%s out=%s ow=t" % (reformatShCmd, fastq, fastaFilePath)
        stdOut, stdErr, exitCode = run_sh_command(reformatCmd, True, log, True)

        if exitCode != 0:
            log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
            status = "7_pb_triangle_read_finder failed"
            checkpoint_step_wrapper(status)
            return status
    else:
        fastaFilePath = seqfsFasta

    ## run pb_triangle_read_finder
    ## cmd = "/global/homes/s/sulsj/work/bitbucket-repo/jgi-rqc-pipeline/tools/pb_triangle_read_finder.py -i %s -o test.txt -s summary.txt" % (fastaFilePath) ## test
    pbTriangleReadFinderCmd = get_tool_path("pb_triangle_read_finder.py", "pb_triangle_read_finder.py")
    cmd = "%s -i %s -o test.txt -s summary.txt" % (pbTriangleReadFinderCmd, fastaFilePath)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)

    if exitCode != 0:
        log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
        status = "7_pb_triangle_read_finder failed"
        checkpoint_step_wrapper(status)
        return status

    assert os.path.isfile("summary.txt"), "ERROR: summary.txt file not found."

    if os.path.isfile("summary.txt"):
        append_rqc_file(filesFile, "TRIANGLE_SUMMARY_TXT", os.path.join(triangleOutPath, "summary.txt"), log)
    if os.path.isfile("test.blacklist"):
        append_rqc_file(filesFile, "TRIANGLE_BLACK_LIST", os.path.join(triangleOutPath, "test.blacklist"), log)
    if os.path.isfile("test.blacklist2"):
        append_rqc_file(filesFile, "TRIANGLE_BLACK_LIST2", os.path.join(triangleOutPath, "test.blacklist2"), log)
    if os.path.isfile("test.whitelist"):
        append_rqc_file(filesFile, "TRIANGLE_WHITE_LIST", os.path.join(triangleOutPath, "test.whitelist"), log)
    if os.path.isfile("test.txt"):
        append_rqc_file(filesFile, "TRIANGLE_TEST_TXT", os.path.join(triangleOutPath, "test.txt"), log) ## blat output

    os.chdir(oldDir)
    
    status = "7_pb_triangle_read_finder complete"
    log.info(status)
    checkpoint_step_wrapper(status)

    return status



"""
STEP8 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_qclongreads

"""
def do_qclongreads(filteredFastq, log):
    log.info("\n\n%sSTEP8 - qcLongRreads <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCPacbioQcConfig.CFG["files_file"]

    status = "8_qclongreads in progress"
    checkpoint_step_wrapper(status)

    qcLongReadsOutDir = "qclongreads"
    qcLongReadsOutPath = os.path.join(outputPath, qcLongReadsOutDir)
    make_dir(qcLongReadsOutPath)
    change_mod(qcLongReadsOutPath, "0755")

    ## -------------------------------------------------------------------------
    ## 3. pacbio_read_qc()
    ##
    ## Plotting
    ##  createReadQualPlot(\%qcDatas, $outputDir, $optOutputFilePrefix, $fastqFile);
    ##  createGcPlot(\%qcDatas, $outputDir, $optOutputFilePrefix, $fastqFile);
    ##  createQualByPosPlot(\%qcDatas, $outputDir, $optOutputFilePrefix, $windowSize);

    pacbioSubreadQcOutNamePath = os.path.join(qcLongReadsOutPath, "pacbioSubreadQc2")
    make_dir(pacbioSubreadQcOutNamePath)
    change_mod(pacbioSubreadQcOutNamePath, "0755")
    pacbio_read_qc(filteredFastq, pacbioSubreadQcOutNamePath, log)

    histFiles = glob.glob(pacbioSubreadQcOutNamePath + "/*.hist")
    csvFiles = glob.glob(pacbioSubreadQcOutNamePath + "/*.csv")
    pngFiles = glob.glob(pacbioSubreadQcOutNamePath + "/*.png")

    for f in histFiles:
        bName, exitCode = safe_basename(f, log)
        keyName = "pacbioSubreadQc." + bName
        append_rqc_file(filesFile, keyName, f, log)

    for f in csvFiles:
        cName, exitCode = safe_basename(f, log)
        keyName = "pacbioSubreadQc." + cName
        append_rqc_file(filesFile, keyName, f, log)

    for f in pngFiles:
        pName, exitCode = safe_basename(f, log)
        keyName = "pacbioSubreadQc." + pName + ".img_file"
        append_rqc_file(filesFile, keyName, f, log)

    
    status = "8_qclongreads complete"
    log.info(status)    
    checkpoint_step_wrapper(status)    
    

    return status



"""
STEP9 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_sketch_vs_nt_refseq_silva

"""
def do_sketch_vs_nt_refseq_silva(fasta, log):
    log.info("\n\n%sSTEP9 - Run sketch vs nt, refseq, silva <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    status = "9_sketch_vs_nt_refseq_silva in progress"
    checkpoint_step_wrapper(status)

    sketchOutDir = "sketch"
    sketchOutPath = os.path.join(outputPath, sketchOutDir)
    make_dir(sketchOutPath)
    change_mod(sketchOutPath, "0755")

    seqUnitName, exitCode = safe_basename(fasta, log)
    seqUnitName = file_name_trim(seqUnitName)

    filesFile = RQCPacbioQcConfig.CFG["files_file"]

    ## sendsketch examples
    ## sendsketch.sh in=file.fa out=results.txt colors=f nt
    ## sendsketch.sh in=file.fa out=results.txt colors=f refseq
    ## sendsketch.sh in=file.fa out=results.txt colors=f silva

    comOptions = "ow=t colors=f printtaxa=t depth depth2 minprob=0 unique2"

    ## NT ##########################
    sketchOutFile = os.path.join(sketchOutPath, seqUnitName + ".sketch_vs_nt.txt")
    sendSketchShCmd = get_tool_path("sendsketch.sh", "bbtools")
    cmd = "%s in=%s out=%s %s nt" % (sendSketchShCmd, fasta, sketchOutFile, comOptions)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)

    if exitCode != 0:
        log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
        status = "9_sketch_vs_nt_refseq_silva failed"
        checkpoint_step_wrapper(status)
        return status
    append_rqc_file(filesFile, "sketch_vs_nt_output", sketchOutFile, log)

    ## Refseq ##########################
    sketchOutFile = os.path.join(sketchOutPath, seqUnitName + ".sketch_vs_refseq.txt")
    cmd = "%s in=%s out=%s %s refseq" % (sendSketchShCmd, fasta, sketchOutFile, comOptions)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)

    if exitCode != 0:
        log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
        status = "9_sketch_vs_refseq failed"
        checkpoint_step_wrapper(status)
        return status
    append_rqc_file(filesFile, "sketch_vs_refseq_output", sketchOutFile, log)

    ## Silva ##########################
    sketchOutFile = os.path.join(sketchOutPath, seqUnitName + ".sketch_vs_silva.txt")
    cmd = "%s in=%s out=%s %s silva" % (sendSketchShCmd, fasta, sketchOutFile, comOptions)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)

    if exitCode != 0:
        log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
        status = "9_sketch_vs_silva failed"
        checkpoint_step_wrapper(status)
        return status

    append_rqc_file(filesFile, "sketch_vs_silva_output", sketchOutFile, log)
    
    status = "9_sketch_vs_nt_refseq_silva complete"
    log.info(status)
    checkpoint_step_wrapper(status)

    return status



"""
STEP9 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_chip_eval

"""
def do_chip_eval(fasta, bRs2, log):
    log.info("\n\n%sSTEP10 - Chip Eval <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    status = "10_chip_eval in progress"
    checkpoint_step_wrapper(status)

    sketchOutDir = "chipeval"
    sketchOutPath = os.path.join(outputPath, sketchOutDir)
    make_dir(sketchOutPath)
    change_mod(sketchOutPath, "0755")

    seqUnitName, exitCode = safe_basename(fasta, log)
    seqUnitName = file_name_trim(seqUnitName)

    filesFile = RQCPacbioQcConfig.CFG["files_file"]

    chipEvalPngFile = os.path.join(sketchOutPath, seqUnitName + ".chip_eval.png")

    ## python pacbioChipEval.py -i pbio-1570.14364.fasta -o pbio-1570.14364.fasta.png -rs2
    hipEvalCmd = get_tool_path("pacbioChipEval.py", "pacbioChipEval.py")
    cmd = "%s -i %s -o %s" % (hipEvalCmd, fasta, chipEvalPngFile)

    if bRs2:
        cmd += " -rs2 "

    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)

    if exitCode != 0:
        log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
        status = "10_chip_eval failed"
        checkpoint_step_wrapper(status)
        return status

    append_rqc_file(filesFile, "chip_eval_out_png_plot_file", chipEvalPngFile, log)
    
    status = "10_chip_eval complete"
    log.info(status)
    checkpoint_step_wrapper(status)

    return status



"""
STEP12 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_cleanup_pacbioqc

"""
def do_cleanup_pacbioqc(log):
    log.info("\n\n%sSTEP12 - Cleanup <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    status = "12_cleanup_readqc in progress"
    checkpoint_step_wrapper(status)

    ## Get option
    skipCleanup = RQCPacbioQcConfig.CFG["no_cleanup"]

    if not skipCleanup:
        retCode = cleanup_pacbioqc(log)

    else:
        log.info("File cleaning is skipped.")
        retCode = RQCExitCodes.JGI_SUCCESS


    if retCode != RQCExitCodes.JGI_SUCCESS:        
        status = "12_cleanup_readqc failed"
        log.info(status)
        checkpoint_step_wrapper(status)

    else:        
        status = "12_cleanup_readqc complete"
        log.info(status)
        checkpoint_step_wrapper(status)


    return status



"""
pacbioReadQc.py

"""
def pacbio_read_qc(inputFastqFile, outDir, log):
    log.info("\n\n%sSTEP8 - pacbio_read_qc <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    log.info("input file: %s", inputFastqFile)

    fh = None
    if inputFastqFile.endswith(".gz"):
        fh = gzip.open(inputFastqFile, "r")
    else:
        fh = open(inputFastqFile, "r")

    index = None
    seq = None
    comment = None
    qual = None

    oldRawReadName = ""
    rawSeq = ""
    rawQual = ""
    longestReadBlock = [None] * 4
    maxLength = 0
    longestSeq = ""
    longestQual = ""
    lineCount = 0

    qcDatas = nested_dict()

    for l in fh:
        lineCount += 1
        readBlock = [None] * 4
        if lineCount == 1:
            index = l.strip()
            continue
        elif lineCount == 2:
            seq = l.strip()
            continue
        elif lineCount == 3:
            comment = l.strip()
            continue
        elif lineCount == 4:
            qual = l.strip()
            lineCount = 0
            readBlock[0] = index
            readBlock[1] = seq
            readBlock[2] = comment
            readBlock[3] = qual

            ## Add qc stats for this read
            ##
            add_qc_data(qcDatas, seq, qual, TYPES['subread'])

            p = re.compile(r'^(.+)\/\d+_\d+$')
            m = p.match(index)
            if m:
                rawReadName = m.group(1)
            else:
                rawReadName = index

            ## If new raw read is found, store qc stats for the raw and longest subread
            ## from the previous raw read set.
            if oldRawReadName and len(oldRawReadName) and oldRawReadName != rawReadName:
                longestSeq = longestReadBlock[1]
                longestQual = longestReadBlock[3]

                ## Add qc stats for longest read.
                add_qc_data(qcDatas, longestSeq, longestQual, TYPES['longestSubread'])

                ## Add qc stats for raw read.
                add_qc_data(qcDatas, rawSeq, rawQual, TYPES['raw'])

                maxLength = 0
                rawSeq = ''
                rawQual = ''
                longestReadBlock = readBlock

            ## If same subread, check if longest subread and store read block if longest.
            elif oldRawReadName == rawReadName:
                seqLen = len(seq)
                if seqLen >= maxLength:
                    longestReadBlock = readBlock
                    maxLength = seqLen

            else:
                longestReadBlock = readBlock

            rawSeq += seq
            rawQual += qual
            oldRawReadName = rawReadName


    fh.close()

    # Add remaining read entry.
    #
    longestSeq = longestReadBlock[1]
    longestQual = longestReadBlock[3]

    # Add qc stats for longest read.
    #
    add_qc_data(qcDatas, longestSeq, longestQual, TYPES['longestSubread'])

    # Add qc stats for raw read.
    #
    add_qc_data(qcDatas, rawSeq, rawQual, TYPES['raw'])

    ## debug
    # with open(os.path.join(outDir, "qcData2.json"), 'w') as JFH:
        # JFH.write(json.dumps(qcDatas))

    log.info("Create read qual histogram plot...")
    create_read_qual_hist_plot(qcDatas, outDir, log)
    log.info("Create read gc plot...")
    create_gc_plot(qcDatas, outDir, log)
    log.info("Create read qual by pos plot...")
    create_qual_by_pos_plot(qcDatas, outDir, log)



def create_read_qual_hist_plot(dataDict, outputDir, log):
    ## Create csv file.
    ##
    outCsvFile = os.path.join(outputDir, "readQual.csv")
    create_read_qual_data_file(dataDict, outCsvFile, log)

    ## Create histogram txt files
    ##
    binSz = 0.5
    colNum = 2

    ## TODO: remove text histograms
    histCmd = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/pacbio/tools/drawHistogram.pl"

    for t in sorted(dataDict):
        outHistFile = os.path.join(outputDir, "readQual.%s.hist" % (t))

        cmd = "export PATH=~qc_user/miniconda/miniconda2/bin:$PATH && perl %s -c 1 -cn %s -b %s %s > %s" %\
              (histCmd, colNum, binSz, outCsvFile, outHistFile)
        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)

        if exitCode == 0:
            assert os.path.isfile(outHistFile)
            log.info("Read qual text hist file created: %s" % (outHistFile))
        else:
            log.error("Failed to create text hist file: %s %s %s" % (outHistFile, stdOut, stdErr))
            sys.exit(-1)

        colNum += 1

    ## Create plot
    ##
    outImgFile = os.path.join(outputDir, "readQual.png")
    plotData = load_data(outCsvFile)
    # xdata = plotData[:, 0]
    xdata = [float(i) for i in plotData[:, 0]]
    fullReadPlotData = plotData[:, 1]
    longestSubreadPlotData = plotData[:, 2]
    subreadPlotData = plotData[:, 3]

    opacity = 0.2
    width = 0.1
    fig, ax = plt.subplots()
    plt.bar(xdata, [float(i) * 5 if i else 0 for i in subreadPlotData], width=width, color='b', label='Subread', alpha=opacity)
    plt.bar(xdata, [float(i) * 5 if i else 0 for i in longestSubreadPlotData], width=width, color='g', label='Longest Subread', alpha=opacity)
    plt.bar(xdata, [float(i) * 5 if i else 0 for i in fullReadPlotData], width=width, color='r', label='Full Read', alpha=opacity)

    ax.set_xlabel("Read Quality", fontsize=12, alpha=0.5)
    ax.set_ylabel("Number of Reads", fontsize=12, alpha=0.5)
    ax.set_title("Read Quality Histogram", fontsize=13, alpha=0.8)

    fontProp = FontProperties()
    fontProp.set_size("small")
    fontProp.set_family("Bitstream Vera Sans")
    ax.legend(loc=1, prop=fontProp)
    ax.grid(color="gray", linestyle=":")
    plt.legend(loc="best", prop=fontProp)

    plt.savefig(outImgFile, dpi=fig.dpi)

    if not os.path.isfile(outImgFile):
        log.error("Failed to create read qual histogram plot, %s" % (outImgFile))
        sys.exit(-1)
    else:
        log.info("Read qual hist plot is created successfully: %s" % (outImgFile))



def create_read_qual_data_file(d, outFile, log):
    types = []
    readQuals = nested_dict()

    for t in sorted(d):
        types.append(t)
        for avgQual, v in d[t]['readQual'].iteritems():
            readQuals[avgQual][t] = int(v)

    types.sort()

    with open(outFile, 'w') as OF:
        OF.write("#Qual\t" + '\t'.join(types) + '\n')

        for avgQual, _ in sorted(readQuals.iteritems(), key=lambda (x, y): float(x)):
            strToWrite = "%s" % (avgQual)
            for t in types:
                if avgQual in readQuals and t in readQuals[avgQual]:
                    strToWrite += "\t%s" % (readQuals[avgQual][t])
                else:
                    strToWrite += "\t"
            OF.write("%s\n" % (strToWrite))

    if not os.path.isfile(outFile):
        log.error("Failed to create read qual data file: %s" % (outFile))
        sys.exit(-1)
    else:
        log.info("Read qual data is created successfully: %s" % (outFile))



def create_gc_plot(dataDict, outputDir, log):
    ## Create csv file.
    ##
    outCsvFile = os.path.join(outputDir, "gc.csv")
    create_gc_data_file(dataDict, outCsvFile, log)

    ## Create histogram txt files.
    ##
    binSz = 0.5
    colNum = 2

    ## TODO: remove text histograms
    histCmd = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/pacbio/tools/drawHistogram.pl"

    for t in sorted(dataDict):
        outHistFile = os.path.join(outputDir, "gc.%s.hist" % (t))

        cmd = "export PATH=~qc_user/miniconda/miniconda2/bin:$PATH && perl %s -c 1 -cn %s -b %s %s > %s" %\
              (histCmd, colNum, binSz, outCsvFile, outHistFile)
        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)

        if exitCode == 0:
            assert os.path.isfile(outHistFile)
            log.info("Read qual text hist file created: %s" % (outHistFile))
        else:
            log.error("Faield to create text hist file: %s %s %s" % (outHistFile, stdOut, stdErr))
            sys.exit(-1)

        colNum += 1

    ## Create plot
    ##
    outImgFile = os.path.join(outputDir, "gc.png")
    plotData = load_data(outCsvFile)
    # xdata = plotData[:, 0]
    xdata = [float(i) for i in plotData[:, 0]]
    fullReadPlotData = plotData[:, 1]
    longestSubreadPlotData = plotData[:, 2]
    subreadPlotData = plotData[:, 3]

    opacity = 0.2
    width = 0.1
    fig, ax = plt.subplots()
    plt.bar(xdata, [float(i) * 10 if i else 0 for i in subreadPlotData], width=width, color='b', label='Subread', alpha=opacity)
    plt.bar(xdata, [float(i) * 10 if i else 0 for i in longestSubreadPlotData], width=width, color='g', label='Longest Subread', alpha=opacity)
    plt.bar(xdata, [float(i) * 10 if i else 0 for i in fullReadPlotData], width=width, color='r', label='Full Read', alpha=opacity)

    ax.set_xlabel("GC Percent", fontsize=12, alpha=0.5)
    ax.set_ylabel("Number of Reads", fontsize=12, alpha=0.5)
    ax.set_title('GC Histogram', fontsize=13, alpha=0.8)

    fontProp = FontProperties()
    fontProp.set_size("small")
    fontProp.set_family("Bitstream Vera Sans")
    ax.legend(loc=1, prop=fontProp)
    ax.grid(color="gray", linestyle=":")
    plt.legend(loc="best", prop=fontProp)

    plt.savefig(outImgFile, dpi=fig.dpi)

    if not os.path.isfile(outImgFile):
        log.error("Failed to create gc histogram plot, %s" % (outImgFile))
        sys.exit(-1)
    else:
        log.info("Gc hist plot is created successfully: %s" % (outImgFile))



def create_gc_data_file(d, outFile, log):
    types = []
    gcBins = nested_dict()

    for t in sorted(d):
        types.append(t)
        for gc, v in d[t]['gc'].iteritems():
            gcBins[gc][t] = int(v)

    types.sort()

    with open(outFile, 'w') as OF:
        OF.write("#Gc\t" + '\t'.join(types) + '\n')

        for gc, v in sorted(gcBins.iteritems(), key=lambda (x, y): float(x)):
            strToWrite = "%s" % (gc)
            for t in types:
                if gc in gcBins and t in gcBins[gc]:
                    strToWrite += "\t%s" % (gcBins[gc][t])
                else:
                    strToWrite += "\t"
            OF.write("%s\n" % (strToWrite))

    if not os.path.isfile(outFile):
        log.error("Failed to create read qual data file: %s" % (outFile))
        sys.exit(-1)
    else:
        log.info("Read qual data file is created successfully: %s" % (outFile))



def create_qual_by_pos_plot(dataDict, outputDir, log):
    outCsvFile = os.path.join(outputDir, "qualByPos.csv")

    ## Normalize base position quality scores.
    ##
    maxQual = normalize_base_pos_qual(dataDict)

    ## Create csv file.
    ##
    log.info("Create qual by pos data file...")
    create_qual_by_pos_data_file(dataDict, outCsvFile, log)

    ## Create csv file containing sliding window average of quals by base pos.
    ##
    csvAvgFile = os.path.join(outputDir, "qualByPos.w50.csv")
    create_sliding_window_csv(outCsvFile, csvAvgFile)

    ## Create plot
    ##
    outImgFile = os.path.join(outputDir, "qualByPos.png")
    title = "Average Quality Scores By Base Position"
    y2ticsLabel = get_qualbypos_ytick_label(maxQual)

    gnuplotCmd = """#!/usr/bin/gnuplot
set terminal png truecolor size 640,480;
set output "%s";
set grid;
set key left;
set title '%s';
set xlabel 'Base Position (bp)';
set ylabel 'Average Quality Score';
set y2label 'Average Accuracy';
set yrange [0:15];
set y2range [0:15];
set y2tics %s;
set style line 1 lc rgb "red";
set style line 2 lc rgb "#008000";
set style line 3 lc rgb "blue";
""" % (outImgFile, title, y2ticsLabel)

    gnuplotCmd += "plot "

    col = 2
    lineStyle = 1
    for t, _ in TYPES.iteritems():
        label = TYPES[t]
        gnuplotCmd += """"%s" using 1:%s:%s ls %s title "%s" with yerrorbars,""" % (csvAvgFile, str(col), str(col + 1), str(lineStyle), label)
        col += 2
        lineStyle += 1

    col = 2
    lineStyle = 1
    for t, _ in TYPES.iteritems():
        gnuplotCmd += """"%s" using 1:%s:%s ls %s notitle with lines,""" % (csvAvgFile, str(col), str(col + 1), str(lineStyle))
        col += 2
        lineStyle += 1

    gnuplotCmd = gnuplotCmd[:-1] ## remove the last comma

    log.info("Read qual by pos Gnuplot command: %s" % (gnuplotCmd))

    gnuplotCmdFile = os.path.join(outputDir, "qualByPos.gnuplot")
    gnuplotLog = os.path.join(outputDir, "qualByPos.gnuplot.log")
    gnuplotTool = get_tool_path("gnuplot", "gnuplot")

    ## save plot command
    with open(gnuplotCmdFile, 'w') as GF:
        GF.write(gnuplotCmd + '\n')

    cmd = "%s %s > %s 2>&1" % (gnuplotTool, gnuplotCmdFile, gnuplotLog)
    _, _, exitCode = run_sh_command(cmd, True, log, True)

    if exitCode == 0:
        if not os.path.isfile(outImgFile):
            log.error("Cannot find read qual by pos plot: %s, %s" % (outImgFile, gnuplotLog))
            sys.exit(-1)
        log.info("Read qual by pos plot is created successfully: %s" % (outImgFile))
    else:
        log.error("Failed to create read qual by pos plot: %s" % (gnuplotLog))
        # sys.exit(-1) ## Just skip it even if the plot is not created successfully. Sequel data has no quality scores



def get_next_log_window_pos(nextWindowPos):
    logVal = math.log(nextWindowPos) / math.log(10)
    nextWindowPos = nextWindowPos + nextWindowPos * logVal
    return nextWindowPos



def create_sliding_window_csv(csvFile, csvAvgFile):
    windowSize = 50
    entries = []
    windowPos = 0
    vals = [[] for i in xrange(3)]

    logWindowSize = 50
    nextLogWindowPos = get_next_log_window_pos(logWindowSize)
    logWindowSize += windowSize
    stdColumn = ["Stdev"]

    with open(csvAvgFile, 'w') as OF:
        with open(csvFile, 'r') as IF:
            for l in IF:
                l = l.strip()
                if l.startswith('#'):
                    ## #Pos, Full_Read, Subread, Longest_Subread ==> '#Pos\tFull_Read\tFull_ReadStdev\tSubread\tSubreadStdev\tLongest_Subread\tLongest_SubreadStdev'
                    OF.write('\t'.join(a + '\t' + a + b if i != 0 else a for i, (a, b) in enumerate(zip(l.split('\t'), stdColumn*4))) + '\n')
                    continue

                entries = l.split('\t')
                windowPos = int(entries[0])

                for i in range(1, 4):
                    try:
                        vals[i - 1].append(int(entries[i]))
                    except:
                        vals[i - 1].append(0) ## catch if the value is not int > 0

                if windowPos >= nextLogWindowPos:
                    nextLogWindowPos = get_next_log_window_pos(logWindowSize)
                    logWindowSize += windowSize

                    print_qual_by_pos_for_window(windowPos, vals, OF)
                    vals = [[] for i in xrange(3)]

        ## Process the rest of vals
        print_qual_by_pos_for_window(windowPos, vals, OF)



def print_qual_by_pos_for_window(wp, v, of):
    outStr = str(wp) + '\t'

    for i in range(len(v)):
        if len(v[i]):
            avgV = sum(v[i]) / float(len(v[i]))
            stdevV = np.std(v[i])

            if avgV == 0:
                outStr += "\t\t"
            else:
                outStr += "%.2f\t%.2f\t" % (avgV, stdevV)

    of.write("%s\n" % outStr.rstrip())



def get_qualbypos_ytick_label(maxQual):
    ticstep = maxQual / 7
    y2ticsLabel = """("0" 0"""
    for i in range(1, 8):
        y2qpos = "%.2f" % (float(i) * ticstep)
        y2lab = "%.2f" % (1-10**(-float(y2qpos) / 10))
        y2ticsLabel += ',"' + y2lab + '"  ' + y2qpos

    y2ticsLabel += ')'

    return y2ticsLabel



def normalize_base_pos_qual(d):
    ## normalize the position qual
    ##
    maxQual = -1

    for t in sorted(d):
        for i in range(0, len(d[t]['posQual']) + 1):
            if i not in d[t]['posQualCnt']:
                continue

            d[t]['posQual'][i] /= d[t]['posQualCnt'][i]

            if maxQual < d[t]['posQual'][i]:
                maxQual = d[t]['posQual'][i]

    return maxQual



def create_qual_by_pos_data_file(d, outFile, log):
    ttypes = []
    qualByPos = {}

    for t in sorted(d):
        ttypes.append(t)
        for i in range(0, len(d[t]['posQual']) + 1):
            if i in d[t]['posQual']:
                if i in qualByPos:
                    qualByPos[i][t] = d[t]['posQual'][i]
                else:
                    qualByPos[i] = {}
                    qualByPos[i][t] = d[t]['posQual'][i]

    ttypes.sort()

    with open(outFile, 'w') as OF:
        OF.write("#Pos\t" + '\t'.join(ttypes) + '\n')

        for b, _ in sorted(qualByPos.iteritems(), key=lambda (x, y): int(x)):
            strToWrite = "%s" % str(int(b) + 1)
            for t in ttypes:
                if b in qualByPos and t in qualByPos[b]:
                    strToWrite += "\t%s" % (qualByPos[b][t])
                else:
                    strToWrite += "\t"

            OF.write("%s\n" % (strToWrite))

    if not os.path.isfile(outFile):
        log.error("Failed to create read pos qual data file: %s" % (outFile))
        sys.exit(-1)
    else:
        log.info("Read qual qual by pos data file is created successfully: %s" % (outFile))



def add_qc_data(dataDict, seq, qual, aType):
    ## gc content
    ##
    gcCon = compute_avg_gc_content(['g', 'c'], seq.lower())

    if gcCon in dataDict[aType]['gc']:
        dataDict[aType]['gc'][gcCon] += 1
    else:
        dataDict[aType]['gc'][gcCon] = 0


    ## Read length distribution
    ##
    readLen = len(seq)

    if readLen in dataDict[aType]['readLength']:
        dataDict[aType]['readLength'][readLen] += 1
    else:
        dataDict[aType]['readLength'][readLen] = 1


    ## Read quality distribution
    ##
    sumQual = 0

    for i, b in enumerate(qual):
        baseQual = ord(b) - 33 # sanger offset
        sumQual += baseQual

        ## qual by base distribution
        if i in dataDict[aType]['posQual']:
            dataDict[aType]['posQual'][i] += baseQual
        else:
            dataDict[aType]['posQual'][i] = baseQual

        if i in dataDict[aType]['posQualCnt']:
            dataDict[aType]['posQualCnt'][i] += 1
        else:
            dataDict[aType]['posQualCnt'][i] = 1

    avgQual = round(float(sumQual) / readLen, 1)

    if avgQual in dataDict[aType]['readQual']:
        dataDict[aType]['readQual'][avgQual] += 1
    else:
        dataDict[aType]['readQual'][avgQual] = 1



def compute_avg_gc_content(targetList, seq):
    # add a 1 for each nucleotide in target_list
    targetCount = sum(1 for x in seq if x in targetList)
    return round(float(targetCount) / len(seq) * 100, 1)



def nested_dict():
    return collections.defaultdict(nested_dict)



"""
load the file using std open
"""
def load_data(fname):
    f = open(fname, 'r')
    data = []
    for line in f.readlines():
        if line.startswith('#'):
            continue
        data.append(line.replace('\n', '').split('\t'))
    f.close()

    return np.array(data)



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program
if __name__ == "__main__":
    desc = 'pacbioreadqc'
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-c", '--skip-cleanup', action='store_true', help='Skip temporary file cleanup.', dest='skipCleanup', default=False, required=False)
    parser.add_argument("-f", "--fastq", dest="fastq", help="Set input fastq file (full path to fastq)", required=True)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Set output path to write to", required=True)
    parser.add_argument("-q", "--sequel", action="store_true", help="Enable Sequel data processing", dest="sequel", default=False, required=False)
    parser.add_argument("-r", "--folder", dest="run_folder", help="Set run folder for Pacbio smrtcells", required=False)
    parser.add_argument("-s", '--skip-subsample', action='store_true', help='Skip read subsampling.', dest='skipSubsample', default=False, required=False)
    parser.add_argument('-v', '--version', action='version', version=VERSION)

    ## For running on Crays
    parser.add_argument("-b", "--skip-blast", action="store_true", help="Skip the blast run", dest="skipBlast", default=False, required=False)
    parser.add_argument("-bn", "--skip-blast-nt", action="store_true", help="Skip the blast run against nt", dest="skipBlastNt", default=False, required=False)
    parser.add_argument("-br", "--skip-blast-refseq", action="store_true", help="Skip the blast run against refseq.archaea and refseq.bateria", dest="skipBlastRefseq", default=False, required=False)
    parser.add_argument("-d", "--blast-db-path", dest="blastDbPath", help="Set path to custom blast db location", required=False)
    parser.add_argument("-z", '--skip-localization', action='store_true', help='Skip database localization', dest='skipLocalization', default=False, required=False)

    parser.add_argument("-rs2", "--rs2", action="store_true", help="Set PacBio data type as RS2 for ChipEval", dest="rs2", default=False, required=False)

    options = parser.parse_args()

    outputPath = None ## output path, defaults to current working directory
    pacbioSmrtcellRunFolder = None
    inputFastq = None ## full path to input filteredFastqFile
    pacbioQid = None
    filteredFastqFile = None
    filteredFastaFile = None

    #logLevel = "INFO"
    logLevel = "DEBUG"

    status = ""
    nextStepToDo = 0

    if options.outputPath:
        outputPath = options.outputPath
    else:
        outputPath = os.getcwd()

    if options.fastq:
        inputFastq = options.fastq

    ## create output_directory if it doesn't exist
    if not os.path.isdir(outputPath):
        os.makedirs(outputPath)

    if options.run_folder:
        pacbioSmrtcellRunFolder = options.run_folder

    ## create output_directory if it doesn't exist
    if not os.path.isdir(outputPath):
        os.makedirs(outputPath)

    blastDbPath = None ## for blast run
    bSkipBlast = None
    bSkipBlastRefseq = None
    bSkipBlastNt = None
    bSkipSubsampling = None
    bSequel = None
    bRs2 = False

    if options.blastDbPath:
        blastDbPath = options.blastDbPath

    if options.skipBlast:
        bSkipBlast = options.skipBlast

    if options.skipBlastRefseq:
        bSkipBlastRefseq = options.skipBlastRefseq

    if options.skipBlastNt:
        bSkipBlastNt = options.skipBlastNt

    if options.skipSubsample:
        bSkipSubsampling = options.skipSubsample

    if options.sequel:
        bSequel = options.sequel

    if options.rs2:
        bRs2 = options.rs2

    ## Set pacbioqc config
    RQCPacbioQcConfig.CFG["status_file"] = os.path.join(outputPath, "pacbioqc_status.log")
    RQCPacbioQcConfig.CFG["files_file"] = os.path.join(outputPath, "pacbioqc_files.tmp")
    RQCPacbioQcConfig.CFG["stats_file"] = os.path.join(outputPath, "pacbioqc_stats.tmp")
    RQCPacbioQcConfig.CFG["output_path"] = outputPath
    RQCPacbioQcConfig.CFG["no_cleanup"] = options.skipCleanup
    RQCPacbioQcConfig.CFG["run_path"] = get_run_path()
    RQCPacbioQcConfig.CFG["skip_localization"] = options.skipLocalization
    RQCPacbioQcConfig.CFG["log_file"] = os.path.join(outputPath, "pacbioqc.log")

    print "Started pacbioqc pipeline, writing log to: %s" % (RQCPacbioQcConfig.CFG["log_file"])

    log = get_logger("pacbioqc", RQCPacbioQcConfig.CFG["log_file"], logLevel, False, True)

    log.info("=================================================================")
    log.info("   Pacbio Qc Analysis (version %s)", VERSION)
    log.info("=================================================================")

    log.info("Starting %s...", scriptName)

    if not pacbioSmrtcellRunFolder:
        pacbioQid = get_pacbio_qid(inputFastq, log)
        assert pacbioQid > 0
        log.info("PacBio QID: %s", str(pacbioQid))

        ## padding to 6 chars: 25882 => 025882
        ## 787 => 000787
        ## 1111 => 001111
        # pacbioQid = "0" + str(pacbioQid) if len(str(pacbioQid)) == 5 else str(pacbioQid)
        pacbioQid = '0' * (6 - len(str(pacbioQid))) + str(pacbioQid)

        ## 025/025882
        subDir = pacbioQid[0:3]


    if not bSequel:
        # if not pacbioSmrtcellRunFolder:
        #     pacbioQid = get_pacbio_qid(inputFastq, log)
        #     assert pacbioQid > 0
        #     log.info("PacBio QID: %s", str(pacbioQid))
        #
        #     ## padding to 6 chars: 25882 => 025882
        #     ## 787 => 000787
        #     ## 1111 => 001111
        #     # pacbioQid = "0" + str(pacbioQid) if len(str(pacbioQid)) == 5 else str(pacbioQid)
        #     pacbioQid = '0' * (6 - len(str(pacbioQid))) + str(pacbioQid)
        #
        #     ## 025/025882
        #     subDir = pacbioQid[0:3]

        pacbioSmrtcellRunFolder = os.path.join(RQCPacbioQc.PACBIO_SMRTCELL_RUN_FOLDER, subDir, pacbioQid) ## /global/seqfs/sdm/prod/pacbio/runs
        log.debug("pacbioSmrtcellRunFolder: %s", pacbioSmrtcellRunFolder)

        ## check for fastq file from pacbioSmrtcellRunFolder
        if os.path.exists(pacbioSmrtcellRunFolder):
            filteredFastqFile = os.path.join(pacbioSmrtcellRunFolder, "data/filtered_subreads.fastq")
            assert os.path.isfile(filteredFastqFile)
            filteredFastaFile = os.path.join(pacbioSmrtcellRunFolder, "data/filtered_subreads.fasta")
            assert os.path.isfile(filteredFastaFile)
        else:
            log.error("Failed to access SMRTCell folder, %s. aborting!", pacbioSmrtcellRunFolder)
            exit(2)

    elif bSequel:
        pacbioSmrtcellRunFolder = os.path.join(RQCPacbioQc.PACBIO_SMRTCELL_RUN_FOLDER_SEQUEL, subDir, pacbioQid) ## /global/seqfs/sdm/prod/pacbio/jobs_smrtlink
        log.debug("Sequel pacbioSmrtcellRunFolder: %s", pacbioSmrtcellRunFolder)

        ## check for fastq file from pacbioSmrtcellRunFolder
        if os.path.exists(pacbioSmrtcellRunFolder):
            aList = glob.glob(os.path.join(pacbioSmrtcellRunFolder, "data", "*.fasta.gz"))
            qList = glob.glob(os.path.join(pacbioSmrtcellRunFolder, "data", "*.fastq.gz"))
            filteredFastqFile = os.path.join(qList[0])
            assert os.path.isfile(filteredFastqFile)
            filteredFastaFile = os.path.join(aList[0])
            assert os.path.isfile(filteredFastaFile)
        else:
            log.error("Failed to access Sequel SMRTCell folder, %s. aborting!", pacbioSmrtcellRunFolder)
            exit(2)

    elif inputFastq is not None and os.path.isfile(inputFastq):
        filteredFastqFile = inputFastq
        filteredFastaFile = inputFastq.replace(".fastq.gz", ".fasta.gz") ## for subsampling to run blast and for sketch vs nt
        assert os.path.isfile(filteredFastaFile), "Input fasta file not found."

    else:
        log.error("No input fastq available.")
        exit(2)


    log.info("Found %s, starting processing.", filteredFastqFile)

    if os.path.isfile(RQCPacbioQcConfig.CFG["status_file"]):
        status = get_status(RQCPacbioQcConfig.CFG["status_file"], log)
        log.info("Latest status = %s", status)

        if status == "start":
            pass

        elif status != "complete":
            nextStepToDo = int(status.split("_")[0])
            if status.find("complete") != -1:
                nextStepToDo += 1
            log.info("Next step to do = %s, %s", nextStepToDo, status)

        else:
            ## already done. just exit
            log.info("Completed %s: %s", scriptName, filteredFastqFile)
            exit(0)

    else:
        checkpoint_step_wrapper("start")
        status = get_status(RQCPacbioQcConfig.CFG["status_file"], log)
        log.info("Latest status = %s", status)


    done = False
    cycle = 0
    totalReadNum = 0
    secondSubsampledFastaFile = None
    secondTotalReadNum = 0
    secondSampledReadNum = 0
    firstSubsampledLogFile = os.path.join(outputPath, "subsample", "first_subsampled.txt")
    secondSubsampledLogFile = os.path.join(outputPath, "subsample", "second_subsampled.txt")

    filesFile = RQCPacbioQcConfig.CFG["files_file"]
    statsFile = RQCPacbioQcConfig.CFG["stats_file"]

    if status == "complete":
        log.info("Status is complete, not processing.")
        done = True

    ## Now we can collect smrtcell data for sequel(01262018)
    # if bSequel:
    #     if nextStepToDo < 2:
    #         nextStepToDo = 2
    #         status = "1_pacbio_pacbioqc_data_pulling complete"

    while not done:
        cycle += 1

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 1. Collect data from PacBio smrtcells run folder
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        ## To get the run folder for Pacbio smrtcells, use seq_units.pb_job_id
        ## e.g. for pb_job_id = 29856
        ## pad pb_job_id to 6 digits: 029856
        ## left(pb_job_id, 3) = 029
        ## the run folder is:
        ## /global/projectb/shared/pacbio/jobs/029/029533

        if nextStepToDo == 1 or status == "start":
            if os.path.exists(pacbioSmrtcellRunFolder):
                log.info("pacbio Smrtcell Run Folder: %s", pacbioSmrtcellRunFolder)
                log.info("Fasta: %s", filteredFastaFile)
                log.info("Fastq: %s", filteredFastqFile)

                ## Pull results/*.xml and *.png
                status = do_collect_smrtcell_results(pacbioSmrtcellRunFolder, bSequel, log)


        if status == "1_pacbio_pacbioqc_data_pulling failed":
            done = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 2. generate read GC histograms: pacbio_read_gc
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 2 or status == "1_pacbio_pacbioqc_data_pulling complete":
            if os.path.isfile(filteredFastqFile):
                log.info("Fasta: %s", filteredFastaFile)
                log.info("Fastq: %s", filteredFastqFile)

                ## Gen gc histogram data/plot by reformat.sh
                status, totalReadNum = do_pacbio_read_gc(inputFastq, filteredFastqFile, log)

        if status == "2_pacbio_read_gc failed":
            done = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 3. pacbio_subsampling_read_megablast
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 3 or status == "2_pacbio_read_gc complete":
            if not bSkipBlast:
                status, secondSubsampledFastaFile, secondTotalReadNum, secondSampledReadNum = do_pacbio_subsampling_read_blastn(filteredFastaFile, bSkipSubsampling, totalReadNum, log)
                log.info("Subsampled file: %s" % (secondSubsampledFastaFile))

            else:
                append_rqc_stats(statsFile, PacbioQcStats.PACBIO_SKIP_BLAST, "1", log)
                log.info("\n\n%sSTEP3 - Run subsampling for Blast search <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
                log.info("3_pacbio_subsampling_read_megablast skipped.\n")
                status = "3_pacbio_subsampling_read_megablast complete"
                checkpoint_step_wrapper(status)

        if status == "3_pacbio_subsampling_read_megablast failed":
            done = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 4. pacbio_read_blastn_refseq_archaea
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 4 or status == "3_pacbio_subsampling_read_megablast complete":
            if not bSkipBlast and not bSkipBlastRefseq:
                if secondSubsampledFastaFile == "" and not os.path.isfile(secondSubsampledLogFile):
                    nextStepToDo = 3
                    continue

                if secondSubsampledFastaFile != "" and not os.path.isfile(secondSubsampledLogFile):
                    make_dir(os.path.join(outputPath, "subsample"))
                    with open(secondSubsampledLogFile, "w") as SAMP_FH:
                        SAMP_FH.write(secondSubsampledFastaFile + " " + str(secondTotalReadNum) + " " + str(secondSampledReadNum) + "\n")
                else:
                    with open(secondSubsampledLogFile, "r") as SAMP_FH:
                        toks = SAMP_FH.readline().strip().split()
                        secondSubsampledFastaFile = toks[0]
                        secondTotalReadNum = int(toks[1])
                        secondSampledReadNum = int(toks[2])

                assert secondSubsampledFastaFile
                log.info("Read file for blastn = %s", secondSubsampledFastaFile)

                status = do_pacbio_read_blastn_refseq_archaea(secondSubsampledFastaFile, log, blastDbPath=blastDbPath)

            else:
                # append_rqc_stats(statsFile, PacbioQcStats.PACBIO_SKIP_BLAST, "1", log)
                log.info("\n\n%sSTEP4 - Run read blastn against refseq archaea <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
                log.info("4_pacbio_read_blastn_refseq_archaea skipped.\n")
                status = "4_pacbio_read_blastn_refseq_archaea complete"
                checkpoint_step_wrapper(status)

        if status == "4_pacbio_read_blastn_refseq_archaea failed":
            done = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 5. pacbio_read_blastn_refseq_bacteria
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 5 or status == "4_pacbio_read_blastn_refseq_archaea complete":
            if not bSkipBlast and not bSkipBlastRefseq:
                if secondSubsampledFastaFile == "" and not os.path.isfile(secondSubsampledLogFile):
                    nextStepToDo = 3
                    continue

                if secondSubsampledFastaFile != "" and not os.path.isfile(secondSubsampledLogFile):
                    make_dir(os.path.join(outputPath, "subsample"))
                    with open(secondSubsampledLogFile, "w") as SAMP_FH:
                        SAMP_FH.write(secondSubsampledFastaFile + " " + str(secondTotalReadNum) + " " + str(secondSampledReadNum) + "\n")
                else:
                    with open(secondSubsampledLogFile, "r") as SAMP_FH:
                        toks = SAMP_FH.readline().strip().split()
                        secondSubsampledFastaFile = toks[0]
                        secondTotalReadNum = int(toks[1])
                        secondSampledReadNum = int(toks[2])

                assert secondSubsampledFastaFile
                log.info("Read file for blastn = %s", secondSubsampledFastaFile)

                status = do_pacbio_read_blastn_refseq_bacteria(secondSubsampledFastaFile, log, blastDbPath=blastDbPath)

            else:
                # append_rqc_stats(statsFile, PacbioQcStats.PACBIO_SKIP_BLAST, "1", log)
                log.info("\n\n%sSTEP5 - Run read blastn against refseq bacteria <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
                log.info("5_pacbio_read_blastn_refseq_bacteria skipped.\n")
                status = "5_pacbio_read_blastn_refseq_bacteria complete"
                checkpoint_step_wrapper(status)

        if status == "5_pacbio_read_blastn_refseq_bacteria failed":
            done = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 6. pacbio_read_blastn_nt
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 6 or status == "5_pacbio_read_blastn_refseq_bacteria complete":
            ##
            ## 2015020 Skip Blast against NT
            ##
            # bSkipBlastNt = True
            # status = "6_pacbio_read_blastn_nt in progress"
            # checkpoint_step_wrapper(status)
            # log.debug("2nd subsampled file: %s", secondSubsampledFastaFile)

            if not bSkipBlast and not bSkipBlastNt:
                if secondSubsampledFastaFile is None and not os.path.isfile(secondSubsampledLogFile):
                    nextStepToDo = 3
                    continue

                else:
                    with open(secondSubsampledLogFile, "r") as SAMP_FH:
                        toks = SAMP_FH.readline().strip().split()
                        secondSubsampledFastaFile = toks[0]
                        secondTotalReadNum = int(toks[1])
                        secondSampledReadNum = int(toks[2])

                assert secondSubsampledFastaFile, "2nd subsampled file not found."
                log.info("Read file for blastn = %s", secondSubsampledFastaFile)

                status = do_pacbio_read_blastn_nt(secondSubsampledFastaFile, log, blastDbPath=blastDbPath)

            else:
                log.info("\n\n%sSTEP6 - Run read blastn against nt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
                log.info("6_pacbio_read_blastn_nt skipped.\n")
                status = "6_pacbio_read_blastn_nt complete"
                checkpoint_step_wrapper(status)

        if status == "6_pacbio_read_blastn_nt failed":
            done = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 7. pb_triangle_read_finder
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 7 or status == "6_pacbio_read_blastn_nt complete":
            # if bSequel:
            #     status = "7_pb_triangle_read_finder complete"
            # else:
            #     status = do_pb_triangle_read_finder(filteredFastqFile, log)
            status = do_pb_triangle_read_finder(filteredFastqFile, log)

        if status == "7_pb_triangle_read_finder failed":
            done = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 8. qcLongReads
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 8 or status == "7_pb_triangle_read_finder complete":
            status = do_qclongreads(filteredFastqFile, log)

        if status == "8_qclongreads failed":
            done = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 9. sketch vs nt, refseq, silva
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 9 or status == "8_qclongreads complete":
            status = do_sketch_vs_nt_refseq_silva(filteredFastaFile, log)

        if status == "9_sketch_vs_nt_refseq_silva failed":
            done = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 10. chip eval
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 10 or status == "9_sketch_vs_nt_refseq_silva complete":
            status = do_chip_eval(filteredFastaFile, bRs2, log)

        if status == "10_chip_eval failed":
            done = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 11. postprocessing & reporting
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 11 or status == "10_chip_eval complete":
            ## 20131023 skip this step
            log.info("\n\n%sSTEP11 - Postprocessing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
            status = "11_pacbioqc_report_postprocess in progress"
            checkpoint_step_wrapper(status)

            ## move rqc-files.tmp to rqc-files.txt
            newFilesFile = os.path.join(outputPath, "pacbioqc_files.txt")
            newStatsFile = os.path.join(outputPath, "pacbioqc_stats.txt")

            cmd = " mv %s %s " % (RQCPacbioQcConfig.CFG["files_file"], newFilesFile)
            log.info("mv cmd: %s", cmd)
            run_sh_command(cmd, True, log)

            cmd = " mv %s %s " % (RQCPacbioQcConfig.CFG["stats_file"], newStatsFile)
            log.info("mv cmd: %s", cmd)
            run_sh_command(cmd, True, log)
            
            status = "11_pacbioqc_report_postprocess complete"
            log.info(status)
            checkpoint_step_wrapper(status)


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 12. Cleanup
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 12 or status == "11_pacbioqc_report_postprocess complete":
            status = do_cleanup_pacbioqc(log)

        if status == "12_cleanup_readqc failed":
            done = True

        if status == "12_cleanup_readqc complete":
            status = "complete" ## FINAL COMPLETE!
            done = True

        ## don't cycle more than 10 times ...
        if cycle > 10:
            done = True


    if status != "complete":
        log.info("Status %s", status)

    else:
        log.info("\nCompleted %s: %s", scriptName, filteredFastqFile)
        checkpoint_step_wrapper("complete")
        log.info("Done.")


    exit(0)


## EOF

