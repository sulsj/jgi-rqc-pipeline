#!/bin/sh

#$ -S /bin/bash
#$ -V
#$ -cwd
#$ -notify
#$ -P gentech-rqc.p
#$ -j y -o <log_file>_$TASK_ID.log
#$ -l normal.c
#$ -N tfmq_mycocosm_blast_<job_name>
#$ -l h_rt=11:59:59
#$ -pe pe_slots 32

module load python
module unload tfmq
module load tfmq

#for i in {1..4} ## for blastn and blastx run in 8-thread mode
for i in {1..1} ## for diamond; one diamond process per a compute node
do
    ## wait 60sec for client, allow empty output files
    tfmq-worker -q <queue_name> -t 60 -z -ld <worker_log_dir> & 
done
wait
