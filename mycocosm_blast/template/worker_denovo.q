#!/bin/sh -l

#$ -S /bin/bash
###SBATCH -q genepool
###SBATCH -q jgi
###SBATCH --qos=genepool 
###SBATCH -A gtrqc
##SBATCH -A fungalp
##SBATCH -t 11:59:59
##SBATCH -c 32
##SBATCH -J tfmq_mycocosm_blast_<job_name>
###SBATCH --job-name=tfmq_mycocosm_blast_<job_name>
###SBATCH -j y -o <log_file>_$TASK_ID.log
##SBATCH -o <log_file>_$TASK_ID.log
##SBATCH -e <log_file>_$TASK_ID.err
###SBATCH --mem=120G
###SBATCH -N 1

#SBATCH -t 06:00:00
#SBATCH -c 16
#SBATCH --job-name=tfmq_mycocosm
#SBATCH --mem=32G
##SBATCH -A gtrqc
#SBATCH -A fungalp

module unload python
module unload perl
export PATH=/global/projectb/sandbox/rqc/qc_user/miniconda/miniconda2/envs/qaqc2/bin:$PATH
source activate /global/projectb/sandbox/rqc/qc_user/miniconda/miniconda2/envs/qaqc2

#for i in {1..4} ## for blastn and blastx run in 8-thread mode
for i in {1..1} ## for diamond; one diamond process per a compute node
do
    ## wait 60sec for client, allow empty output files
    tfmq-worker -q <queue_name> -t 60 -z -ld <worker_log_dir> & 
done
wait
