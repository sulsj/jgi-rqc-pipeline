#!/usr/bin/env python
import os
from os_utility import run_sh_command

with open("db.list.final", 'r') as IFH:
    
    totalSize = 0
    
    for l in IFH:
        #print l
        cmd = "ls -al %s" % (l.strip() + ".*")
        print cmd
        stdOut, stdErr, exitCode = run_sh_command(cmd, True, None, True)
        # print stdOut, stdErr, exitCode
        if exitCode == 0:
            # print stdOut
            subLines = stdOut.strip().split('\n')
            # print subLines
            
            for l2 in subLines:
                t = l2.split()
                s = int(t[4])
                totalSize += s
            

print "Total = %d" % (totalSize)
