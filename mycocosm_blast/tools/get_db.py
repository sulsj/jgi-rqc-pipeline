#!/usr/bin/env python
import json
import requests

data = requests.get('http://genome.jgi.doe.gov/ext-api/mycocosm/catalog/all-portals').json()

for items in data:
    if 'blastable' in items:
        for entry in items['blastable']:
            # if items['blastable'][entry]['category'] == 'GeneModels--Transcripts':
            if items['blastable'][entry]['category'] == 'GeneModels--Proteins--Default':
                #print items['name'] + " " + items['blastable'][entry]['fastaDb']
                print items['blastable'][entry]['fastaDb']
            # else:
            #     print items['name'] + " No blastable data"
