import os
import sys


TFMQ_TEMPLATE = "%s::0"
BLASTN_TEMPLATE = "mkdir -p %s; module unload blast+; module load blast+/2.6.0; blastn -query %s -db %s -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -num_alignments 5 -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles' -num_threads 8 > %s 2> %s.log"
#RUNBLASTPLUS_TEMPLATE = "module load jgi-rqc; run_blastplus.py -q %s -d %s -o %s > %s 2> %s.log"
DB_LIST = "../db.list.final"
OUT_DIR = "/global/projectb/scratch/sulsj/2017.02.24-mycocosm-blast/tfmq_test/out"
# QUERY = sys.argv[1]
QUERY = "/global/projectb/scratch/sulsj/2017.02.24-mycocosm-blast/10898.8.183776.ACTTGA.fasta"

with open(DB_LIST, 'r') as DLFH:
    for l in DLFH:
        # print l
        l = l.strip()
        outFileName = QUERY.split('/')[-1] + ".vs." + l.split('/')[-1] + ".bout"
        outFiles = os.path.join(OUT_DIR, outFileName)
        blastCmd = BLASTN_TEMPLATE % (OUT_DIR, QUERY, l.strip(), outFiles, outFiles)
        tfmqCmd = TFMQ_TEMPLATE % (blastCmd)
        
        print tfmqCmd



## $ tfmq-worker -q blastmq -t 0 -z
## $ tfmq-client -i test.list -q blastmq -w
