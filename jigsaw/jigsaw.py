#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
*** SCRIPT IS DEPRECATED: BF 2017-06-19 ***

    RQC wrapper for jigsaw pipeline
    Version 2.6.3
    Shijie Yao

    20161010 - jigsaw 2.6.4  SPAdes 3.9.0
    20160508 - jigsaw 2.6.3  SPAdes 3.7.1
    20160112 - jigsaw 2.6.1 : use latest spades
    20151023 - jigsaw 2.6.0
    20150504 - added SAG1 & SAG2 options (for decontamination)
    20150415 - passed jigsaw 2.5.0 tests (include cell-enrichment)
    20150407 - jigsaw 2.5.0; support -wf cell-enrichment option
    20140801 - jigsaw 2.4.0; with sag jobs with its own executable of run_jigsaw_sag.pl; no spades/allpath option for sag anymore
    20140303 - match jigsaw 2.3.2: sag require -orgname NCBI_ORGANISM_NAME option
    20131120 - match jigsaw 2.3.1: use relative path in rqc.txt; use yaml format of jigsaw_stats, pdf desc file.
    20131105 - jigsaw 2.3.0 use -noopt CML option to turn -local mendel OFF, default is ON
    20131018 - jigsaw 2.3.0 with -module option
    20131011 - use lib.os_utility.get_tool_path() for jigsaw command; add option for running on genepool cluster
    20130813 - initial implementation

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import os
import sys
import re
import logging
import argparse
from jigsaw_post_processing import post_prep, find_value_from_jigsaw_log

# append the pipeline lib and tools relative path:
rqc_jigsaw_bin = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))
pipe_root = rqc_jigsaw_bin + '/' + os.pardir
sys.path.append(pipe_root + '/lib')   # common
sys.path.append(pipe_root + '/tools') # rqc_fastq

import rqc_fastq as fastqUtil
import os_utility as osUtil
from common import get_logger, get_status, run_command, checkpoint_step

#2013-10-14 : velvet fails if we do not have "module load jigsaw"
# bring in jigsaw lib
#import EnvironmentModules as envmod
#envmod.module(['unload', 'bwa'])        # jigsaw has its own bwa version
#envmod.module(['unload', 'velvet'])     # jigsaw has its own velvet version
#envmod.module(['load', 'jigsaw'])

# the following constants needs work - some central location ?
TOOLS_BIN = pipe_root + '/tools'

#
# TODO : for testing a specific verision of jigsaw, the following version value need to be changed
#        jigsaw/dev IS ONLY FOR TESTING !!!
#LOAD_JIGSAW = 'module unload bwa velvet jigsaw; module load jigsaw/2.4.1'
#LOAD_JIGSAW = 'module unload bwa velvet jigsaw; module load jigsaw/2.5.0'
#LOAD_JIGSAW = 'module unload bwa velvet jigsaw; module load jigsaw/2.5.1'
#LOAD_JIGSAW = 'module unload bwa velvet jigsaw; module load jigsaw/2.6.0'      # jigsaw 2.6.0 : RQC-691 and RQC-573
#LOAD_JIGSAW = 'module unload bwa velvet jigsaw; module load jigsaw/2.6.1'
#LOAD_JIGSAW = 'module unload bwa velvet jigsaw; module load jigsaw/2.6.2'
#LOAD_JIGSAW = 'module unload bwa velvet jigsaw; module load jigsaw/2.6.3'
LOAD_JIGSAW = 'module unload bwa velvet jigsaw; module load jigsaw/2.6.4'   # spades 3.9.0


CMD_JIGSAW = None

# the following constatns are specific to THIS pipeline
STATUS_LOG = 'status.log';

PIPE_START = 'start'
JIGSAW_START = 'start jigsaw'
JIGSAW_END = 'end jigsaw'
POST_START = 'start post process'
POST_END = 'end post process'
PIPE_COMPLETE = 'complete'
PIPE_FAILED = 'failure'

STEP_ORDER = {
    PIPE_START      : 0,
    JIGSAW_START    : 10,
    JIGSAW_END      : 20,
    POST_START      : 30,
    POST_END        : 40,
    PIPE_COMPLETE   : 100,
    PIPE_FAILED     : 200
}

ACTION = True

'''
    The wrapper to run jigsaw pipeline
    @fastq      : input fastq file (full path)
    @outDir     : the jigsaw output directory
    @runType   : the data type (iso|sag|sag-spades)
    @status     : status value from the checkpoint file
    Return      : Boolean
'''
#jigsaw_pipeline(options.fastq, options.outputPath, options.runType, status, log, True, False, options.orgname)
def jigsaw_pipeline(fastq, outDir, runType, status, log, local=True, mendel = True, orgname = None, debug = False):
    log_and_print('RUN JIGSAW', log)

    checkpoint(JIGSAW_START, status)

    #print('status=%s' % status)
    #print('JIGSAW_START=%s' % STEP_ORDER[JIGSAW_START])
    if not debug and (STEP_ORDER[status] <= STEP_ORDER[JIGSAW_START]):

        jversion = get_jigsaw_version()

        ##
        ##-- TODO : COMMENTS THE FOLLOWING LINE OUT FOR PRODUCTION  !
        ##
        #jversion = '2.6.2'


        if CMD_JIGSAW:
            cmd = '%s -module' % CMD_JIGSAW
        else:
            cmd = '%s; run_jigsaw.pl -module' % LOAD_JIGSAW

            ##-- jigsaw 2.4.0 sag
            if jversion >= '2.4.0' and runType.startswith('sag'):
                cmd = '%s; run_jigsaw_sag.pl -module' % LOAD_JIGSAW

        #print('local=%s' % local)
        if not local:
            cmd += ' -sge genepool.gentech-rqc'

        #print('mendel=%s' % mendel)
        if mendel:
            if runType == 'iso-spades-nofilter':
                #cmd += ' -opt mendel-iso-spades'   # spades 120G with 16 thread
                pass # so spades to use 40G with 8 threads;
            elif runType == 'cell-enrich-nofilter':
                cmd += ' -opt mendel-cell-enrich'
            else:
                cmd += ' -local mendel'

        j_log = os.path.join(outDir, "jigsaw-%s.log" % runType) # capture jigsaw output

        if orgname and jversion >= '2.3.2' and runType == 'sag':
            cmd += ' -orgname \"%s\"' % orgname

        if runType.startswith('sag-nofilter'):  # jigsaw 2.6.0 : use filtered fastq as input
            cmd += ' -rerun -wf %s -fe -od %s %s' % (runType, outDir, fastq)
        elif runType.startswith('sag'):
            if jversion >= '2.4.0':
                cmd += ' -rerun -fe -od %s %s' % (outDir, fastq)
            else:
                cmd += ' -wf sag-spades -fe -od %s %s' % (outDir, fastq)
        elif runType.startswith('iso'): # jigsaw 2.6.0 : use filtered fastq as input
            cmd += ' -rerun -wf %s -fe -od %s %s' % (runType, outDir, fastq)
        elif runType.startswith('cell'): # cell enrichment
            cmd += ' -rerun -wf %s -fe -od %s %s > %s 2>&1' % (runType, outDir, fastq, j_log)
        elif runType.startswith("dsag1"): # single cell decontam - step 1
            #cmd += " -rerun -wf spades -fe -od %s %s > %s 2>&1" % (outDir, fastq, j_log)
            cmd += " -rerun -wf spades-nofilter -fe -od %s %s > %s 2>&1" % (outDir, fastq, j_log)
        elif runType.startswith("dsag2"):
            # run_jigsaw_sag runs the reporting for screened and unscreened
            cmd = "%s;run_jigsaw_sag.pl -module -rerun -wf decontam -fe -od %s %s" % (LOAD_JIGSAW, outDir, fastq)

        if orgname:
            cmd += ' -orgname \"%s\"' % orgname
            cmd += " > %s 2>&1" % j_log

        # 2.3.0 uses -local mendel BUT 2.3.1+ uses -opt mendel
        if jversion != '2.3.0':
            cmd = cmd.replace('-local', '-opt')
        log_and_print(' - cmd=[%s]' % cmd, log)
        stdOut, stdErr, exitCode = run_command(cmd, ACTION, log)    # exitCode of 0 is success
        if exitCode != 0:
            log_and_print(' - cmd %s failed [exitCode=%d][stdErr=%s]' % (cmd, exitCode, str(stdErr)), log)
            return False
    else:
        log_and_print(' - no need to run jigsaw', log)

    checkpoint(JIGSAW_END, status)
    log_and_print('RUN JIGSAW - completed', log)

    return True

'''
    Perform full post processing on a jigsaw pipeline output directory
    @seqUnitName      : the seq unit name
    @runType   : iso | sag
    @workingDir : the jigsaw output directory
    @status     : status value from the checkpoint file
    Return      : Boolean
'''
def jigsaw_post_processing(seqUnitName, runType, workingDir, status):
    'jigsaw pipeline post processing '

    log_and_print('RUN POST PROCESSING', log)
    checkpoint(POST_START, status)
    #if (status == PIPE_START or status == JIGSAW_END or status == POST_START):
    if (STEP_ORDER[status] <= STEP_ORDER[POST_START]):
        '''
        # The following two values are the values in jigsaw_XYZ_config file:
        stats_log = rqc_stats.txt
        file_log = rqc_files.txt
        '''
        statsLog = 'rqc_stats.txt'  #
        fileLog = 'rqc_files.txt'   #

        # dsag1 just needs to be set as complete
        if runType.startswith("dsag1"):
            checkpoint(POST_END, status)
            log_and_print('RUN POST PROCESSING - completed', log)
            return

        cmd = '%s/jigsaw_post_processing.py -o %s -s %s -d %s -p full' % (rqc_jigsaw_bin, workingDir, seqUnitName, runType)
        log_and_print(' - cmd=[%s]' % cmd, log)
        stdOut, stdErr, exitCode = run_command(cmd, ACTION, log)    # exitCode of 0 is success
        if exitCode != 0:
            errmsg = get_error_message(stdOut)
            log_and_print(' - failed [%s]' % errmsg, log)

            if not errmsg:
                errmsg = 'Unknown cause of run_command() failure on jigsaw_post_processing.py'
            return errmsg
        else:
            print(stdOut)
    else:
        log_and_print(' - no need to run jigsaw post processing', log)

    checkpoint(POST_END, status)
    log_and_print('RUN POST PROCESSING - completed', log)

    return

def get_error_message(txt):
    msg = []
    for line in txt.splitlines():
        if line.lower().find('error:') == 0:
            msg.append(line[6:].strip())
    if msg:
        msg = 'ERROR:' + str(msg)
    else:
        msg = ''

    return msg


#==========================================================================
def checkpoint(status, fromStatus=PIPE_START):
    if status == PIPE_START or STEP_ORDER[status] > STEP_ORDER[fromStatus]:
        checkpoint_step(STATUS_LOG, status)

# helper function to construct a formated message string
def msg_str(key, val):
    return '%15s      %s' % (key, val)

def list_opt(opts, log):
    dash = '-' * 90
    log_and_print(dash, log)

    log_and_print(msg_str('fastq', opts.fastq), log)
    log_and_print(msg_str('outputPath', opts.outputPath), log)
    log_and_print(msg_str('type', opts.runType), log)
    log_and_print(msg_str('postProcessing', opts.postProcessing), log)
    log_and_print(msg_str('sge', not opts.sge), log)
    log_and_print(msg_str('mendel', not opts.noopt), log)
    log_and_print(msg_str('jigsaw', LOAD_JIGSAW), log)
    log_and_print(msg_str('jigsaw ver', get_jigsaw_version()), log)

    log_and_print(dash, log)

def log_and_print(msg, log = None):
    if log:
        log.info(msg)
    print(msg)

def err_log_and_print(msg, log):
    if log:
        log.error(msg)
    print(msg)
    exit(2)

def get_jigsaw_version():
    match = re.match('.* jigsaw/(\d+(\.\d+)*).*', LOAD_JIGSAW)  # so 1.2, 1.2.3, 1.2.3.4 are all valid version values
    if match:
        jversion = match.group(1)
        return jversion
    return None


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## main program

script_name = __file__


if __name__ == "__main__":

    my_name = "RQC JIGSAW Pipeline"
    version = "2.6.0"
    #global CMD_JIGSAW

    # Parse options
    usage = "* Jigsaw Pipeline RQC wrapper, version %s\n" % (version)
    DATA_TYPE = ['iso', 'iso-nofilter', 'iso-spades-nofilter', 'sag', 'sag-nofilter', 'cell', 'cell-enrich-nofilter', 'dsag1', 'dsag2'] # cell : cell enrichment, sag1 & 2 = single cell part1 & part 2
    oriCmd = ' '.join(sys.argv)

    # command line options
    parser = argparse.ArgumentParser(description=usage, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-f", "--fastq", dest="fastq", help="Fastq file path (can be zipped)", required=True)
    parser.add_argument("-o", "--output-path", dest="outputPath", help = "Output path to write to", required=True)
    parser.add_argument("-e", "--executable", dest="executable", help = "Specify jigsaw executable to overwrite the default")
    parser.add_argument("-t", "--type", dest="runType", choices=DATA_TYPE, help = "data type : [iso | sag]", required=True)
    parser.add_argument("-p", "--post", dest="postProcessing", action='store_true', default=False, help = 'perform post processing')
    parser.add_argument("-q", "--sge", dest="sge", action='store_true', default=False, help = 'run job on HPC cluster.')
    parser.add_argument("-g", "--orgname", dest="orgname", help = 'ncbi organism name.')
    parser.add_argument("-n", "--noopt", dest="noopt", action='store_true', default=False, help = 'run job without medel optimization option.')
    parser.add_argument('-v', '--version', action='version', version=version)

    logLevel = "INFO"

    options = parser.parse_args()


    if not options.outputPath:
        options.outputPath = os.getcwd()

    options.outputPath = os.path.realpath(options.outputPath)

    # create output_directory if it doesn't exist
    if not os.path.isdir(options.outputPath):
        print("Cannot find %s, creating as new" % options.outputPath)
        os.makedirs(options.outputPath)

    if not os.path.isdir(options.outputPath):
        err_log_and_print('Cannot work with directory: %s' % options.outputPath, log)

    # initialize my logger
    logFile = "%s/%s" % (options.outputPath, "rqc_jigsaw_pipeline.log")
    log = get_logger("filter", logFile, logLevel)

    if options.executable:
        executable = options.executable.strip()
        if not os.path.isfile(executable):
            print("Can not find the given jigsaw executable [%s]" % executable)
            sys.exit(1)

        CMD_JIGSAW = os.path.realpath(executable)

    #--------------------------------
    # init log
    log_and_print('#' * 50, log)
    log_and_print("Started pipeline, writing log to: %s" % logFile, log)
    log_and_print("CMD=%s" % oriCmd, log)
    list_opt(options, log)

    # check for fastq file
    if options.fastq:
        if os.path.isfile(options.fastq):
            log_and_print('Found input file [%s], start processing.' % options.fastq, log)
            options.fastq = os.path.realpath(options.fastq)
        else:
            err_log_and_print('%s not found, aborting!' % options.fastq, log)
    else:
        err_log_and_print('No fastq defined, aborting!', log)

    os.chdir(options.outputPath)

    done = False
    cycle = 0
    cycleMax = 1
    status = get_status(STATUS_LOG, log)
    log_and_print('Starting %s on input [%s] at [%s]' %(script_name, options.fastq, status), log)

    if options.runType != 'sag' and options.orgname:
        print('Warning: -g NCBI_ORGANISM_NAME option are only for sag pipeline. Ignored!')


    if status == PIPE_COMPLETE:
        log_and_print('Pipeline has been competed previously. Do nothing!', log)
        sys.exit(0)

    if status == PIPE_START:
        checkpoint(PIPE_START, status)

    # main loop: retry upto cycleMax times
    while cycle < cycleMax:
        cycle += 1
        log_and_print('ATEMPT [%d]' % cycle, log)

        if cycle > 1:
            reRun = '' # when implemented, reRun = '-r'
            status = get_status(STATUS_LOG, log)

        # jigsaw pipeline
        rtn = jigsaw_pipeline(options.fastq, options.outputPath, options.runType, status, log, (not options.sge), (not options.noopt), options.orgname)
        '''
        if options.runType.find('iso') > -1 and not options.sge:
            rtn = jigsaw_pipeline(options.fastq, options.outputPath, options.runType, status, log, True, False, options.orgname)
        else:
            rtn = jigsaw_pipeline(options.fastq, options.outputPath, options.runType, status, log, (not options.sge), (not options.noopt), options.orgname)
        '''
        # post processgin
        err = False
        if rtn and options.postProcessing:
            err = jigsaw_post_processing(os.path.basename(options.fastq), options.runType, options.outputPath, status)

            if not err:
                checkpoint(PIPE_COMPLETE, status)
                cycle = cycleMax
            else:
                log_and_print('%s' % err, log)
        elif cycle >= cycleMax:
            break;
            #checkpoint(PIPE_FAILED, status)



