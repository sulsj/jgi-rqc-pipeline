#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
*** SCRIPT IS DEPRECATED: BF 2017-06-19 ***

    jigsaw pipeline post processing functions.

    Version 2.6.3
    Shijie Yao

    20161010 - jigsaw 2.6.4 (spades 3.9.0)
    20160728 - add megablast bacteria and archaea stats collaction  (RQCSUPPORT-772)
    20160508 - jigsaw 2.6.3
    20160112 - jigsaw 2.6.1 : use latest spades
    20151117 - added DSAG-1, DSAG-2 data
    20150928 - match jigsaw 2.6.0 (with filtered fastq as input); use jigsaw tax tool for NCBI name lookup;
    20150415 - passed jigsaw 2.5.0 tests (include cell-enrichment)
    20150408 - match jigsaw 2.5.0 with cell-enrichment run type; display JGI Project ID and PMO Project value in pdf only if they have non-zero values;
    20140714 - use DB data for QD instrument type and platform;
               can rerun to fix QD file when instrument type and platform value are "No value in database";
               fix jigsaw_stats.yaml file (logo.jpg and contam config path) for older jobs;
               * still use the rqc_tax.taxtonames table for NCBI name look-up, should change that to use NCIB WS (by taxid)
    20131120 - match jigsaw 2.3.1: use relative path in rqc.txt; use yaml format of jigsaw_stats, pdf desc file.
    20131011 - use jigsaw path from job log for utility scripts.

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import MySQLdb
import os
import sys
import argparse
import re
import time, datetime
import yaml
import shutil

dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir, '../lib'))
from db_access import jgi_connect_db
from os_utility import run_sh_command

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# global constants
NO_VAL = 'No value in database'
JIGSAW_STATS_FILE = 'jigsaw_stats.yaml'
exitCode = 0

#TODO:
DEBUG = False
#LOAD_JIGSAW = 'module unload bwa velvet jigsaw; module load jigsaw/2.6.0'      # jigsaw 2.6.0 : RQC-691 and RQC-573
#LOAD_JIGSAW = 'module unload bwa velvet jigsaw; module load jigsaw/2.6.3'
LOAD_JIGSAW = 'module unload bwa velvet jigsaw; module load jigsaw/2.6.4'   # spades 3.9.0

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

def get_db():
    db = None
    if DEBUG:
        print('DEBUG DB: rqc_dump.')
        db = jgi_connect_db("rqc-dev")
    else:
        print('PROD DB: rqc.')
        db = jgi_connect_db("rqc")

    return db



def fix_stats(fslocation):
    fname = '%s/%s' % (fslocation, JIGSAW_STATS_FILE)

    lines = []
    with open(fname, 'r') as fh:
        lines = fh.readlines()

    #save the original jisaw_stats.yaml file
    cmd = 'mv %s %s.ori' % (fname, fname)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True)

    with open(fname, 'w') as fh:
        for line in lines:
            line = line.rstrip()
            match = re.match('^    image:.*logo.jpg', line)
            if match:
                line = '    image: /usr/common/jgi/qaqc/jigsaw/2.3.2-prod_2.3.2_2-15-ga6634e6/config/logo.jpg'
            else:
                match = re.match('^      configFile: .*config/run_contam_filter_iso.config', line)
                if match:
                    line = '      configFile: /usr/common/jgi/qaqc/jigsaw/2.3.2-prod_2.3.2_2-15-ga6634e6/config/run_contam_filter.config'
            fh.write('%s\n' % line)

def clean_str(my_str):
    #re.sub("[^A-Za-z0-9\ \-\:\.]+", " ", row['ncbi_organism_name'])
    if my_str:
        if type(my_str) is str:
            my_str = "".join(c for c in my_str if ord(c) >=32 and ord(c) <=127)

    return my_str

'''
main jigsaw post process function
@dataType               : 'sag' / 'iso'
@seqUnitName            : the seq uint name
@pipelineFSLocation     : jigsaw job directory
@rqcOutputFileLogName   : the file-list file name that framework will use to do archiving (basename only)
@rqcOutputStatsLogName  : the stats file name that frameeowkr will use to do archiving (basename only)
@jigsawBin              : jigsaw's bin directory (needed to locate the jigsaw_pdf.pl)
@desc                   : the pdf description file in yaml format
@postLog                : the post processing logger

return value: Boolean/str
'''
def post_prep(dataType, seqUnitName, pipelineFSLocation, rqcOutputFileLogName, rqcOutputStatsLogName, jigsawBin, desc=None, postLog=None):
    ''
    # create QC.finalReport.pdf
    rs = jigsaw_pdf_gen(dataType, seqUnitName, pipelineFSLocation, True, jigsawBin, desc, postLog)

    # fill in meta data to QD.finalReport.txt
    if rs:
        rs = jigsaw_qd_file_prep(seqUnitName, pipelineFSLocation, postLog)

    # collect key:value for contig megablast and write to the stats log file
    if rs:
        rs = jigsaw_file_list_prep(seqUnitName, pipelineFSLocation, rqcOutputFileLogName, rqcOutputStatsLogName, postLog)

    return rs


def jigsaw_pdf_gen(dataType, seqUnitName, pipelineFSLocation, addToRQCtxt=False, jigsawBin=None, desc=None, postLog=None):
    log_and_print('  - enter pdf gen ...')
    if not os.path.isdir(pipelineFSLocation):
        err_log_and_print('jigsaw project dir not exists [%s]' % pipelineFSLocation, postLog)
        return None

    if not jigsawBin:
        err_log_and_print('Do not have jigsaw pipeline bin directory to continue', postLog)
        return None
    elif not seqUnitName:
        err_log_and_print('Do not have seq unit name to continue', postLog)
        return None
    elif not dataType:
        err_log_and_print('Do not have data type to continue', postLog)
        return None

    jigsawLogFile = os.path.join(pipelineFSLocation, 'run_jigsaw.pl.log')

    cmd = 'grep "^Command: " %s' % jigsawLogFile
    stdOut, stdErr, exitCode = run_sh_command(cmd, True)
    if exitCode == 0:
        lines = stdOut.strip().split('\n')
        match = re.match('Command: (\S+) .* (\S+)$', lines[-1])
        if match:
            if not jigsawBin:
                jigsawBin = os.path.dirname(match.group(1))
            if not seqUnitName:
                seqUnitName = standardize_seq_unit_name(os.path.basename(match.group(2)))

        if not dataType:
            match = re.match('Command: .* -wf (\S+) .*$', lines[-1])
            if match:
                dataType = match.group(1)
    else:
        err_log_and_print('Failed to check jigsaw runlog [%s] [%s]' % (cmd, exitCode), postLog)
        return None

    jigsawStatsFile = os.path.join(pipelineFSLocation, JIGSAW_STATS_FILE)
    if not os.path.isfile(jigsawStatsFile):
        err_log_and_print('jigsaw jobs is incomplete [%s] [%s is missing]' % (pipelineFSLocation,JIGSAW_STATS_FILE), postLog)
        return None

    # prep for pdf file gen:
    projDescFile = os.path.join(pipelineFSLocation, 'project_pdf_desc.yaml')
    if desc:
        projDescFile = desc
    else:
        jigsaw_pdf_desc(dataType, seqUnitName, pipelineFSLocation, projDescFile, postLog)
    pdfFileName = os.path.join(pipelineFSLocation, 'final', 'QC.finalReport.pdf')
    #cmd = 'module load texlive; %s/jigsaw_pdf.pl -d %s -of %s %s %s' % (jigsawBin, pipelineFSLocation, pdfFileName, projDescFile, JIGSAW_STATS_FILE)

    # the PDF generator needs to be run within the project dir
    cDir = os.getcwd()
    os.chdir(pipelineFSLocation)
    log_and_print('  - run pdf gen cmd [%s] ...' % cmd)

    ##- TODO  the following call is needed for older jigsaw job report fixes (eg jobs completed before June 2014)
    #fix_stats(pipelineFSLocation)

    ##- TODO: need the following hard coded path for jigsaw_pdf.pl when re-run on older jigsaw jobs
    #jigsawBin = '/usr/common/jgi/qaqc/jigsaw/2.3.2-prod_2.3.2_2-15-ga6634e6/bin/'
    cmd = 'module load texlive; %s/jigsaw_pdf.pl -d %s -of %s %s %s' % (jigsawBin, pipelineFSLocation, pdfFileName, projDescFile, JIGSAW_STATS_FILE)
    print('CMD=%s' % cmd)

    stdOut, stdErr, exitCode = run_sh_command(cmd, True)    # exitCode of 0 is success
    if exitCode != 0:
        err_log_and_print('failed to create the QC report [%s]' % stdErr, postLog)
        return None

    os.chdir(cDir)
    log_and_print('  - done with run pdf gen cmd.')
    if addToRQCtxt:
        flistFile = os.path.join(pipelineFSLocation, 'rqc.txt')
        log_and_print('  - add pdf file entry to [%s]' % flistFile)

        if os.path.isfile(flistFile):
            pdfKey = 'finalQcReport'
            findKey = False
            with open(flistFile, 'r') as fh:
                lines = fh.readlines()

            with open(flistFile, 'w') as fh:
                for line in lines:
                    line = line.strip()
                    if line.find(pdfKey) < 0:
                        fh.write(line + '\n')
                    else:
                        fh.write('%s=%s\n' % (pdfKey, pdfFileName))
                        findKey = True
                if not findKey:
                    fh.write('%s=%s\n' % (pdfKey, pdfFileName))
            log_and_print('  - done with adding pdf file entry to file.')
        else:
            if(postLog):
                err_log_and_print('Cannot add [%s] to file list : file not found [%s]' % (pdfFileName, flistFile), postLog)

    return pdfFileName


'''
The current jigsaw pipeline is not in compliance with rqc framework in that it does not product
the stats and file-list files in format that rqc framework requires. This funciton is used to
fix that descripancy.
@seqUnitName            : the seq uint name
@pipelineFSLocation     : jigsaw job directory
@rqcOutputFileLogName   : the file-list file name that framework will use to do archiving
@rqcOutputStatsLogName  : the stats file name that frameeowkr will use to do archiving
@postLog                : the post processing logger

return value: Boolean
'''
def jigsaw_file_list_prep(seqUnitName, pipelineFSLocation, rqcOutputFileLogName, rqcOutputStatsLogName, postLog=None):
    # megablast file kyes in rqc.txt
    megablastKeys = [
        'megablastParsedContigsVsNt',
        'megablastTophitContigsVsNt',
        'megablastTophitSummaryContigsVsNt',
        'megablastParsedTaxlistContigsVsNt',

        'megablastParsedContigsVsLSSURef_tax_silva',
        'megablastTophitContigsVsLSSURef_tax_silva',
        'megablastTophitSummaryContigsVsLSSURef_tax_silva',
        'megablastParsedTaxlistContigsVsLSSURef_tax_silva',

        'megablastParsedContigsVsLSURef_tax_silva',
        'megablastTophitContigsVsLSURef_tax_silva',
        'megablastTophitSummaryContigsVsLSURef_tax_silva',
        'megablastParsedTaxlistContigsVsLSURef_tax_silva',

        'megablastParsedContigsVsSSURef_tax_silva',
        'megablastTophitContigsVsSSURef_tax_silva',
        'megablastTophitSummaryContigsVsSSURef_tax_silva',
        'megablastParsedTaxlistContigsVsSSURef_tax_silva',

        'megablastParsedContigsVsRefseq.microbial',
        'megablastTophitContigsVsRefseq.microbial',
        'megablastTophitSummaryContigsVsRefseq.microbial',
        'megablastParsedTaxlistContigsVsRefseq.microbial',

        'megablastParsedContigsVsRefseq.bacteria',
        'megablastTophitContigsVsRefseq.bacteria',
        'megablastTophitSummaryContigsVsRefseq.bacteria',
        'megablastParsedTaxlistContigsVsRefseq.bacteria',

        'megablastParsedContigsVsRefseq.archaea',
        'megablastTophitContigsVsRefseq.archaea',
        'megablastTophitSummaryContigsVsRefseq.archaea',
        'megablastParsedTaxlistContigsVsRefseq.archaea',

        'megablastParsedContigsVsRefseq.mitochondrion',
        'megablastTophitContigsVsRefseq.mitochondrion',
        'megablastTophitSummaryContigsVsRefseq.mitochondrion',
        'megablastParsedTaxlistContigsVsRefseq.mitochondrion',

        'megablastParsedContigsVsRefseq.plant',
        'megablastTophitContigsVsRefseq.plant',
        'megablastTophitSummaryContigsVsRefseq.plant',
        'megablastParsedTaxlistContigsVsRefseq.plant',

        'megablastParsedContigsVsRefseq.plasmid',
        'megablastTophitContigsVsRefseq.plasmid',
        'megablastTophitSummaryContigsVsRefseq.plasmid',
        'megablastParsedTaxlistContigsVsRefseq.plasmid',

        'megablastParsedContigsVsRefseq.plastid',
        'megablastTophitContigsVsRefseq.plastid',
        'megablastTophitSummaryContigsVsRefseq.plastid',
        'megablastParsedTaxlistContigsVsRefseq.plastid',

        'megablastParsedContigsVsRefseq.fungi',
        'megablastTophitContigsVsRefseq.fungi',
        'megablastTophitSummaryContigsVsRefseq.fungi',
        'megablastParsedTaxlistContigsVsRefseq.fungi',

        'megablastParsedContigsVsRefseq.invertebrate',
        'megablastTophitContigsVsRefseq.invertebrate',
        'megablastTophitSummaryContigsVsRefseq.invertebrate',
        'megablastParsedTaxlistContigsVsRefseq.invertebrate',

        'megablastParsedContigsVsRefseq.protozoa',
        'megablastTophitContigsVsRefseq.protozoa',
        'megablastTophitSummaryContigsVsRefseq.protozoa',
        'megablastParsedTaxlistContigsVsRefseq.protozoa',

        'megablastParsedContigsVsRefseq.vertebrate_mammalian',
        'megablastTophitContigsVsRefseq.vertebrate_mammalian',
        'megablastTophitSummaryContigsVsRefseq.vertebrate_mammalian',
        'megablastParsedTaxlistContigsVsRefseq.vertebrate_mammalian',

        'megablastParsedContigsVsRefseq.vertebrate_other',
        'megablastTophitContigsVsRefseq.vertebrate_other',
        'megablastTophitSummaryContigsVsRefseq.vertebrate_other',
        'megablastParsedTaxlistContigsVsRefseq.vertebrate_other',

        'megablastParsedContigsVsRefseq.viral',
        'megablastTophitContigsVsRefseq.viral',
        'megablastTophitSummaryContigsVsRefseq.viral',
        'megablastParsedTaxlistContigsVsRefseq.viral',

        'megablastParsedContigsVsGreen_genes16s.insa_gg16S',
        'megablastTophitContigsVsGreen_genes16s.insa_gg16S',
        'megablastTophitSummaryContigsVsGreen_genes16s.insa_gg16S',
        'megablastParsedTaxlistContigsVsGreen_genes16s.insa_gg16S',

        'megablastParsedContigsVsSagtaminants.ribomask',
        'megablastTophitContigsVsSagtaminants.ribomask',
        'megablastTophitSummaryContigsVsSagtaminants.ribomask',
        'megablastParsedTaxlistContigsVsSagtaminants.ribomask',

        'megablastParsedContigsVsJGIContaminants',
        'megablastTophitContigsVsJGIContaminants',
        'megablastTophitSummaryContigsVsJGIContaminants',
        'megablastParsedTaxlistContigsVsJGIContaminants'
    ]

    # local constants
    ASSEMBLY_MATCHING_HITS = 'assembly matching hits of'
    ASSEMBLY_TOP_HITS      = "assembly top hits of"
    ASSEMBLY_TAX_SPECIES   = "assembly tax species of"

    param = {}  # a dict for key=value to be saved to DB
    flist = {}  # a dict for key=file to be saved to DB
    rqc_txt = '%s/rqc.txt' % pipelineFSLocation

    if os.path.isfile(rqc_txt):
        with open(rqc_txt, 'r') as fh:
            lines = fh.readlines()

        megab_files = {}
        for line in lines:
            line = line.strip()
            key, fpath = line.split('=')

            if os.path.isfile(fpath):
                flist[key] = os.path.realpath(fpath)
            else:
                continue

            basename = os.path.basename(fpath)
            #log_and_print('[%s][%s]' % (key, basename))

            if key.startswith('finalAssemblyFile'):
                cmd = 'grep -c "^>" %s' % fpath
                stdOut, stdErr, exitCode = run_sh_command(cmd, True)    # exitCode of 0 is success
                if exitCode == 0:
                    cnt = stdOut.strip()
                    if key == 'finalAssemblyFile':
                        param['assembly contig number'] = cnt
                    else:
                        param['assembly contig number unscreen'] = cnt
            elif key in megablastKeys or (key.endswith('Unscreen') and key[:-8] in megablastKeys):
                megab_files[key] = fpath
                #log_and_print('    -- [%s][%s]' % (key, fpath))

        # gather contig megablast key:value data for db update
        step = 4    # group size of megablastKeys
        for idx in [n for n in range(len(megablastKeys)) if n % step == 0]:
            mkey = megablastKeys[idx]

            # the '.parsed' file
            fpath = None
            tag = mkey
            match = re.match('megablastParsedContigsVs(.+)', tag)
            if match:
                tag = match.group(1)

            if mkey in megab_files:
                fpath = megab_files[mkey]
                _jigsaw_megablast_param(fpath, ASSEMBLY_MATCHING_HITS, tag, param)

            ##- jigsaw 2.4.0 sag:
            mkey_uscreen = mkey + 'Unscreen'
            if mkey_uscreen in megab_files:
               fpath = megab_files[mkey_uscreen]
               _jigsaw_megablast_param(fpath, ASSEMBLY_MATCHING_HITS, tag + ' unscreen', param)

            # the ".parsed.tophit" file
            mkey = megablastKeys[idx + 1]
            if mkey in megab_files:
                fpath = megab_files[mkey]
                _jigsaw_megablast_param(fpath, ASSEMBLY_TOP_HITS, tag, param)

            ##- jigsaw 2.4.0 sag:
            mkey = megablastKeys[idx + 1]
            mkey_uscreen = mkey + 'Unscreen'
            if mkey_uscreen in megab_files:
                fpath = megab_files[mkey_uscreen]
                _jigsaw_megablast_param(fpath, ASSEMBLY_TOP_HITS, tag + ' unscreen', param)

            # the ".parsed.taxlist" file
            mkey = megablastKeys[idx + 3]
            if mkey in megab_files:
                fpath = megab_files[mkey]
                _jigsaw_megablast_param(fpath, ASSEMBLY_TAX_SPECIES, tag, param)

            ##- jigsaw 2.4.0 sag:
            mkey = megablastKeys[idx + 3]
            mkey_uscreen = mkey + 'Unscreen'
            if mkey_uscreen in megab_files:
                fpath = megab_files[mkey_uscreen]
                _jigsaw_megablast_param(fpath, ASSEMBLY_TAX_SPECIES, tag + ' unscreen', param)
    else:
        err_log_and_print('Can not find jigsaw output rqc.txt file', postLog)

    # write the collected key:file in flist to the rqc_files output file
    fileLog = '%s/%s' % (pipelineFSLocation, rqcOutputFileLogName)
    with open(fileLog, 'w') as fh:
        for key in flist:
            fh.write('%s=%s\n' % (key, flist[key]))

    # write the collected key:value in param to the stats output file
    statLog = '%s/%s' % (pipelineFSLocation, rqcOutputStatsLogName)
    with open(statLog, 'w') as fh:
        for key in param:
            fh.write('%s=%s\n' % (key, param[key]))

    return True

'''
helper function to store proper megablast output KEY:VALUE data into the dictionary
@fpath   : a megablast output file (whose line count will be evaluated as the VLAUE)
@cntType : leading-part of the KEY
@tag     : tailing-part of the KEY
@pdict   : the dictionary container

return value : tuple of (KEY, VALUE)
'''
def _jigsaw_megablast_param(fpath, cntType, tag, pdict):
    ' helper to return (dbKey, count) for the given megablast output file'
    cnt = 0
    if os.path.isfile(fpath):
        # count lines in the given file, excluding blank lines and lines with leading #.
        cmd = 'cat %s | grep -v "^#" | grep -v "^$" | wc -l' % fpath
        stdOut, stdErr, exitCode = run_sh_command(cmd, True)    # exitCode of 0 is success
        if exitCode == 0 and stdOut:
            cnt = int(stdOut.strip().split()[0])

    dbKey = '%s %s' % (cntType, tag)
    pdict[dbKey] = cnt

    return dbKey, cnt

def seq_unit_id_for_fname(sth, fname):
    sql = 'SELECT seq_unit_id FROM seq_unit_file WHERE file_name like %s group by seq_unit_id'

    sth.execute(sql, (fname+'%',))
    rs = sth.fetchone()
    if rs:
        return int(rs['seq_unit_id'])
    else:
        return None


'''
After jigsaw produced the QD.finalReport.txt file, certain info need to fill in.
in raw file :
  1 QD/SD JGI ISOLATES QC AND ASSEMBLY REPORT - <PROJECT_ID> <BIOCLASS_NAME>
  2
  3 1) RAW DATA:
  4
  5 LibraryName NumReads    RunType ReadType    FileName
  6 LIB 11555154    2x150   Illumina Std PE (cassava 1.8)   7850.3.85572.GTGAAA.anqdp.fastq
  7
  8 2) ILLUMINA STD PE READ FILTERING STATS

after postprocess:
  1 QD/SD JGI ISOLATES QC AND ASSEMBLY REPORT -
  2 1030752
  3 Pseudomonas aeruginosa DSM 50071
  4
  5 1) RAW DATA:
  6
  7 LibraryName NumReads    RunType ReadType    FileName    Platform    Model
  8 TSPH    12033826    2x150   Illumina Std PE (cassava 1.8)   7850.3.85572.GTGAAA.fastq   Illumina    HiSeq-2500 1TB
  9
  10 2) ILLUMINA STD PE READ FILTERING STATS

@seqUnitName        : the seq uint name
@pipelineFSLocation : jigsaw job dir
@postLog            : the post processing logger

return value : file name OR None
'''
def jigsaw_qd_file_prep(seqUnitName, pipelineFSLocation, postLog=None):
    ' modify the QD.finalReport.txt file with meta data info.'

    rqcTxtFile = '%s/rqc.txt' % pipelineFSLocation
    reportTxt = 'QD.finalReport.txt'
    cmd = 'grep "%s" %s' % (reportTxt, rqcTxtFile)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True)
    if exitCode != 0:
        err_log_and_print('Failed to execute command [%s]' % cmd, postLog)
        return None
    elif not stdOut:
        err_log_and_print('Cannot find entry in [%s] for [%s]' % (rqcTxtFile, reportTxt), postLog)
        return None

    key, fpath = stdOut.strip().split('=')

    if not os.path.isfile(fpath):
        err_log_and_print('File not exists [%s]' % fpath, postLog)
        return None

    seqProjId = NO_VAL
    ncbiOrgName = NO_VAL
    libName = NO_VAL
    seqUnitInDB = False

    sql = """
        select
            lib.seq_proj_id,
            lib.ncbi_tax_id,
            lib.library_name,
            seq.instrument_type,
            seq.platform_name
        from library_info lib
        inner join seq_units seq on lib.library_id = seq.rqc_library_id
        where
            seq.seq_unit_id =%s
    """

    platformName = NO_VAL # platform name
    instType = NO_VAL # instrument type -> model

    # database connection
    db = get_db()

    if db == None:
        err_log_and_print("Cannot open database connection", postLog)
        return None
    sth = db.cursor(MySQLdb.cursors.DictCursor)

    suid = seq_unit_id_for_fname(sth, seqUnitName)
    if not suid:
        err_log_and_print("File not in RQC db: %s" % seqUnitName, postLog)
        return None

    sth.execute(sql, (suid,))
    rs = sth.fetchone()
    if rs:
        seqProjId = rs['seq_proj_id']
        ncbiOrgName = get_ncbi_name_for_taxid(sth, rs['ncbi_tax_id'])
        libName = rs['library_name']
        instType = rs['instrument_type']
        platformName = rs['platform_name']
        seqUnitInDB = True
        #print('DEBUG rs=%s', str(rs))
    else:
        log_and_print("No db record for seq unit [%s]. Use default values in report ..." % seqUnitName, postLog)
        seqProjId = NO_VAL
        ncbiOrgName = NO_VAL
        libName = NO_VAL

        #return None

    # update the file
    with open(fpath, 'r') as fh:
        lines = fh.readlines()

    with open(fpath, 'w') as fh:
        modDone = False
        idx = 0
        while idx < len(lines):
            line = lines[idx].rstrip()
            if idx == 0:   # title line
                match = re.match('(.*)<PROJECT_ID>', line)
                if match:
                    title = match.group(1)
                    fh.write('%s\n' % title)
                    fh.write('%s\n' % seqProjId)
                    fh.write('%s\n' % ncbiOrgName)
                else:
                    #the next 2 lines has been already updated to be SPID and NCBI Name respectively:
                    fh.write('%s\n' % line)
                    fh.write('%s\n' % seqProjId)
                    fh.write('%s\n' % ncbiOrgName)
                    idx += 2

            elif idx == 2 and line.startswith('None'):  # ncbi organism line that has been processed to be None
                fh.write('%s\n' % ncbiOrgName)
            elif not modDone and line.startswith('LibraryName'):
                if line.find('\tPlatform\tModel') == -1:
                    fh.write('%s\tPlatform\tModel\n' % line)
                else:
                    fh.write('%s\n' % line)
                idx += 1
                line = lines[idx].rstrip()
                match = re.match('^LIB(.*)', line)
                if match:
                    fh.write('%s%s\t%s\t%s\n' % (libName, match.group(1), platformName, instType))
                else:
                    match = re.match('^(.*)\tNo value in database\tNo value in database', line)
                    if match:
                        fh.write('%s\t%s\t%s\n' % (match.group(1), platformName, instType))
                    else:
                        fh.write('%s\n' % line)
                modDone = True
            else:
                fh.write('%s\n' % line)

            idx += 1

    return fpath # succeeded


def get_ncbi_name_for_taxid(sth, taxid):
    name = 'No Tax ID assigned'
    taxid = int(taxid)
    if taxid:
        exe = '%s; $JIGSAW_DIR/external_tools/taxMapper/DEFAULT/lookupTax.pl' % LOAD_JIGSAW
        cmd = '%s -lf -taxid %d' % (exe, taxid)

        stdOut, stdErr, exitCode = run_sh_command(cmd, True)
        if exitCode != 0:
            name = 'No NCBI name found for ID=%d' % taxid
        else:
            tok = stdOut.split(';')
            tok.reverse()
            for m in tok:   # return the lowest tax name
                m = m.strip()
                if m != '':
                    name = m
                    break;
    print('>> DEBUG: taxid=%d; lowest name=%s' % (taxid, name))
    return name



'''
To generate the descriptor file for jigsaw pdf gen
@dataType           : ['iso', 'sag', 'cell', 'dsag1', 'dsag2', 'iso-nofilter', 'sag-nofilter', 'cell-enrich-nofilter']
@seqUnitName        : the seq uint name
@pipelineFSLocation : path to the jigsaw pipeline job directory,
@ofname             : the output pdf desc file name (yaml format)

return value : True/False
'''
def jigsaw_pdf_desc(dataType, seqUnitName, pipelineFSLocation, ofname, postLog):
    log_and_print('  - enter pdf descriptor file prep ...')
    ' to generate the descriptor file for jigsaw pdf gen'

    organism      = NO_VAL  # goes to document.title
    subtitle      = NO_VAL  # goes to document.subtitle

    program       = NO_VAL  # goes to project.Program
    seqProjName   = NO_VAL  # goes to project.Sequencing Project Name
    seqProjId     = NO_VAL  # goes to project.Seq Proj ID
    pmoProjId     = NO_VAL  # goes to project.PMO Project
    jgiProjId     = NO_VAL  # goes to project.JGI Project ID

    libName       = NO_VAL  # goes to readStatistics.std.Library

    instType      = NO_VAL  #

    # the PDF file document title text
    # print('dataType=[%s]' % dataType)
    if dataType.startswith('sag') or dataType.startswith("dsag"):
        subtitle = 'Single-cell Assembly QC Report'
        if dataType.startswith('sag-nofilter') or dataType.startswith("dsag"):
            dataType = 'sag-rqcfilter'
    elif dataType.startswith('iso'):
        subtitle = 'Isolate Assembly QC Report'
        if dataType.find('nofilter') > 0:
            dataType = 'iso-rqcfilter'
    elif dataType.startswith('cell'):
        if dataType == 'cell':
            dataType = 'cell-enrich'
        subtitle = 'Cell Enrichment Assembly QC Report'

    # database connection
    db = get_db()

    if db == None:
        postLog.error("Cannot open database connection.")
        return False

    sth = db.cursor(MySQLdb.cursors.DictCursor)

    # incase the input is the filtered fastq (e.g. jigsaw 2.6.0+), retrieve the raw seq unit name

    sql = '''
select S.seq_unit_name from seq_units S inner join seq_unit_file F on S.seq_unit_id=F.seq_unit_id where F.file_name=%s
    '''
    sth.execute(sql, (seqUnitName,))
    rs = sth.fetchone()
    if rs:
        seqUnitName = rs['seq_unit_name']

    sql = """
select
    lib.library_name,
    lib.seq_proj_name,
    lib.seq_proj_id,
    lib.pmo_project_id,
    lib.directory_number,
    lib.ncbi_tax_id,

    lib.account_jgi_sci_prog,
    lib.account_jgi_user_prog,
    lib.account_year,

    seq.instrument_type

from library_info lib
inner join seq_units seq on lib.library_id = seq.rqc_library_id
where
    seq.seq_unit_name = %s
    """


    sth.execute(sql, (seqUnitName,))
    rs = sth.fetchone()
    if rs:
        libName     = str(rs['library_name'])
        seqProjName = str(rs['seq_proj_name'])
        seqProjId   = str(rs['seq_proj_id'])
        pmoProjId   = str(rs['pmo_project_id'])
        jgiProjId   = str(rs['directory_number'])
        instType    = str(rs['instrument_type'])

        organism = get_ncbi_name_for_taxid(sth, rs['ncbi_tax_id'])

        # program = "account_jgi_sci_prog/account_jgi_user_prog account_year"
        program = str(rs['account_jgi_sci_prog'])
        if rs['account_jgi_user_prog'] and program:
            program = '%s/%s' % (program, str(rs['account_jgi_user_prog']))
        if rs['account_year']:
            program = '%s %s' % (program, str(rs['account_year']))

        # test the special chars for pdf gen, and found that no escape is needed (the '\' will be printed if used)
        #program += '1@1 2*2 3^3 4&4 5$5'
        #program = _escape_special_char(program)


    project = {'Program' : program,
                'Seq Proj ID' : seqProjId,
                'Sequencing Project Name' : seqProjName,
                }
    if pmoProjId and pmoProjId != '0':
        project['PMO Project'] = pmoProjId
    if jgiProjId and jgiProjId != '0':
        project['JGI Project ID'] = jgiProjId

    desc = {
        'document' : {'title' : organism, 'subtitle' : subtitle},
        'project' : project,
        'readStatistics' : {'std' : [{'Library': libName}]},    # jigsaw_pdf.pl expects LIST item for Library
        'seqAvailability' : {'desc' : 'The following sequence fasta files can be downloaded from our JGI portal website.',
                             'url' : 'http://www.jgi.doe.gov/genome-projects'},
        'annotationAvailability' : {'desc' : 'The annotation of the assembled contigs can be found within IMG.',
                                    'url': 'http://img.jgi.doe.gov'},
        'method' : dataType,
        'illuminaPlatform': instType
    }

    # A string value line break in the yaml file causes the pdf gen to fail.
    # use width=1024 to avoid line breaking for long string values. String value of len <1000 will be safe this way
    with open(ofname, 'w') as fh:
        fh.write( yaml.dump(desc, default_flow_style=False, width=1024) )

    # yaml.dump produces yaml file with LIST data ('  - VALUE') without indentation relative to its header
    # but jigsaw_pdf.pl expects LIST data to be indented. To cope with this perl code, i need to sed the yaml file
    cmd = 'sed -i \"s/- /    - /\" %s' % ofname
    stdOut, stdErr, exitCode = run_sh_command(cmd, True)

    log_and_print('  - done with pdf descriptor file prep')
    return True

# return (VALUE, ERROR)
def _get_data_from_yaml(fpath, *args):
    # copy to an temp file
    nfile = fpath + '.tmp'
    shutil.copy2(fpath, nfile)
    cmd = 'sed -i \'s/\t/  /g\' %s' % nfile
    stdOut, stdErr, exitCode = run_sh_command(cmd, True)    # exitCode of 0 is success
    if exitCode != 0:
        return None, 'ERROR: ' + stdErr

    ydata = yaml.load(open(nfile, 'r'))
    #os.remove(nfile)
    val = ydata
    for arg in args:
        if arg in val:
            val = val[arg]
        else:
            return None, 'ERROR: yaml file does not have object %s' % str(args)
    return val, None

'''
helper to escape special chars in the giving string to make pdf gen happy
@subj : the given string
'''
def _escape_special_char(subj):
    ' return a new string with the special characters escaped '
    spec = r'@*^&$'
    for char in spec:
        subj = subj.replace(char, '\%s' % char)
    return subj


def msg_and_exit(msg, exitCode=1):
    err_log_and_print(msg)
    sys.exit(exitCode)

def standardize_seq_unit_name(suName):
    if suName:
        if suName.find(r'.fastq') < 0: # assume the seq unit name given in format of 6248.1.39959.ACAGTG
            suName = '%s.fastq.gz' % suName
        elif suName.find(r'.gz') < 0: # assume the seq unit name given in format of 6248.1.39959.ACAGTG.fastq
            suName = '%s.gz' % suName
    return suName

def timestamp():
    return datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

def timed_log(msg, log=None):
    log_and_print('%s - %s' % (timestamp(), msg))

def err_log_and_print(msg, log=None):
    global exitCode
    if log:
        log.error('ERROR: ' + msg)
    print('ERROR: ' + msg)
    exitCode = 1

def log_and_print(msg, log=None):
    if log:
        log.debug(msg)
    print(msg)


'''
    @pipelineFSLocation: jigsaw job directory
'''
def find_value_from_jigsaw_log(pipelineFSLocation, log=None):
    ' Find the jigsaw bin, seq unit name, and data type from jigsaw run log, and return the tuple in that order'
    jigsawBin = None
    seqUnitName = None
    dataType = None

    jigsawLogFile = '%s/run_jigsaw.pl.log' % pipelineFSLocation
    if not os.path.isfile(jigsawLogFile):
        err_log_and_print('jigsaw run log not exists [%s]' % jigsawLogFile, log)
    else:
        cmd = 'grep "^Command: " %s' % jigsawLogFile
        stdOut, stdErr, exitCode = run_sh_command(cmd, True)
        if exitCode == 0:
            lines = stdOut.strip().split('\n')
            match = re.match('Command: (\S+) .* (\S+)$', lines[-1])
            if match:
                jigsawBin = os.path.dirname(match.group(1))
                seqUnitName = standardize_seq_unit_name(os.path.basename(match.group(2)))

            match = re.match('Command: .* -wf (\S+) .*$', lines[-1])
            if match:
                dataType = match.group(1)
        else:
            err_log_and_print('Failed to check jigsaw runlog [%s] [%s]' % (cmd, exitCode), log)
    return jigsawBin, seqUnitName, dataType

if __name__ == "__main__":
    my_name = "RQC JIGSAW Post Processing"
    version = "2.6.0"

    # Parse options
    usage = "* RQC JIGSAW Post Processing, version %s\n" % (version)
    DATA_TYPE = ['iso', 'sag', 'cell', 'iso-nofilter', 'iso-spades-nofilter', 'sag-nofilter', 'cell-enrich-nofilter', 'dsag1', 'dsag2']
    POST_TYPE = ['pdf', 'txt', 'reports', 'full']
    oriCmd = ' '.join(sys.argv)

    # command line options
    parser = argparse.ArgumentParser(description=usage)

    parser.add_argument("-o", "--wdir", dest="workingDir", help = "jigsaw pipeline directory", required=True)
    parser.add_argument("-s", "--seq-unit-name", dest="seqUnitName", help="seq Unit Name (with or with out fastq.gz)")
    parser.add_argument("-d", "--data-type", dest="dataType", choices=DATA_TYPE, help ="data type : [iso | sag | cellF]")
    parser.add_argument("-p", "--post-type", dest="postType", choices=POST_TYPE, help ="data type : [pdf|txt|reports|dbfiles|full] for gen pdf only | txt only | both | full post process", required=True)
    parser.add_argument("-y", "--yaml-desc", dest="yamlDesc", help="external pdf description file in yaml format")
    parser.add_argument('-v', '--version', action='version', version=version)

    dataType    = None
    workingDir  = None # output path, defaults to current working directory
    seqUnitName = None # full path to input fastq
    postType    = None
    pdfDesc     = None

    options = parser.parse_args()


    if options.workingDir:
        workingDir = options.workingDir
    if not workingDir:
        workingDir = os.getcwd()

    workingDir = os.path.realpath(workingDir)

    os.chdir(workingDir)

    # create output_directory if it doesn't exist
    if not os.path.isdir(workingDir):
        msg_and_exit("jigsaw pipeline job directory does not exist: [%s]" % workingDir, 2)

    if options.postType:
        postType = options.postType.lower()
        #if not POST_TYPE.has_key(postType):
        #    msg_and_exit('Invalide option value for -p --post-type: %s' % postType)

    if options.seqUnitName:
        seqUnitName = standardize_seq_unit_name(options.seqUnitName)

    if options.dataType:
        dataType = options.dataType.lower()
        #if dataType and not DAT_TYPE.has_key(dataType):
        #    msg_and_exit('Invalide option value for -d date-type: %s' % dateType)

    if options.yamlDesc:
        if os.path.isfile(options.yamlDesc):
            pdfDesc = options.yamlDesc
        else:
            msg_and_exit("The given pdf description file does not exist : [%s]" % options.yamlDesc, 2)

    dash = '-' * 80
    print('%s' % dash)
    timed_log('Start JIGSAW POST PROCESSING')
    print('CMD=%s' % oriCmd)
    jigsawBin, suFromRunLog, dtFromRunLog = find_value_from_jigsaw_log(workingDir)
    if not seqUnitName:
        seqUnitName = suFromRunLog
    if not dataType:
        dataType = dtFromRunLog

    canRun = True
    if not seqUnitName:
        err_log_and_print('Cannot find seq unit name to continue the process')
        canRun = False

    if not dataType:
        err_log_and_print('Cannot find data type to continue the process')
        canRun = False

    if not jigsawBin:
        err_log_and_print('Cannot find jigsaw job bin directory to continue the process')
        canRun = False

    statsLog = 'rqc_stats.txt'
    fileLog = 'rqc_files.txt'

    if canRun:
        if postType == 'pdf':
            log_and_print('Only create QC.finalReport.pdf file ...')
            pdfFileName = jigsaw_pdf_gen(dataType, seqUnitName, workingDir, False, jigsawBin, pdfDesc)
            if pdfFileName:
                log_and_print('  -- pdf file is created [%s]' % pdfFileName)
            else:
                err_log_and_print('pdf file creation failed')

        elif postType == 'txt':
            log_and_print('Only create QD.finalReport.txt file ...')
            txtFileName = jigsaw_qd_file_prep(seqUnitName, workingDir)
            if txtFileName:
                log_and_print('  -- QD report file is preped [%s]' % txtFileName)
            else:
                err_log_and_print('QD report creation failed')

        elif postType == 'reports':
            log_and_print('Create both QC.finalReport.pdf and QD.finalReport.txt files:')
            log_and_print('  -- creating QC.finalReport.pdf ...')
            pdfFileName = jigsaw_pdf_gen(dataType, seqUnitName, workingDir, False, jigsawBin, pdfDesc)
            log_and_print('  -- creating QD.finalReport.txt ...')
            txtFileName = jigsaw_qd_file_prep(seqUnitName, workingDir)

            if pdfFileName:
                log_and_print('  -- pdf file is created [%s]; ' % pdfFileName)
            else:
                err_log_and_print('pdf file creation failed')

            if txtFileName:
                log_and_print('QD report file is preped [%s]' % txtFileName)
            else:
                err_log_and_print('QD report creation failed')

        elif postType == 'dbfiles':
            log_and_print('Create log files for db update files:')

            rs = jigsaw_file_list_prep(seqUnitName, workingDir, fileLog, statsLog)
            if rs:
                log_and_print('  -- log files are created [%s][%s]' % (statsLog, fileLog))
            else:
                err_log_and_print('failed to create the log files')

        elif postType == 'full':
            log_and_print('Perform full post processing ...')
            rs = post_prep(dataType, seqUnitName, workingDir, fileLog, statsLog, jigsawBin, pdfDesc)
            if rs:
                log_and_print('  -- full post processing completed')
            else:
                err_log_and_print('failed to perform full post processing')


    timed_log('JIGSAW POST PROCESSING END. [exit code=%d]' % exitCode)
    log_and_print('%s' % dash)
    sys.exit(exitCode)



