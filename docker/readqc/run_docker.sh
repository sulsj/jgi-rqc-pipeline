docker run --rm -it -w $PWD \
    -v $HOME:$HOME \
    -v /var/run/docker.sock:/var/run/docker.sock \    
    -v /Users/sulsj/work/docker/readqc-alpine-openjdk/out:/out \
    -v /Users/sulsj/work/docker/readqc-alpine-openjdk/data:/data:ro \
    -v /Users/sulsj/work/docker/readqc-alpine-openjdk/input:/input:ro \
    -v /Users/sulsj/work/docker/readqc-alpine-openjdk/wdl:/wdl2 \
    sulsj/readqc-alpine-openjdk sh -c "apk add docker; docker pull sulsj/readqc-alpine-openjdk; java -jar cromwell-33.1.jar run /wdl2/readqc2.wdl"

