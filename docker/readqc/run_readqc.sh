docker run \
    -v /Users/sulsj/work/docker/readqc-alpine-openjdk/input:/input:ro \
    -v /Users/sulsj/work/docker/readqc-alpine-openjdk/out:/out \
    -v /Users/sulsj/work/docker/readqc-alpine-openjdk/data:/data:ro \
    -it --rm sulsj/readqc-alpine-openjdk /readqc.sh -f /input/CTZOX/12540.1.263352.GAAGGAA-GAAGGAA.fastq.gz -o /out/ctzox -r 0 -l CTZOX --skip-blast -m 0
