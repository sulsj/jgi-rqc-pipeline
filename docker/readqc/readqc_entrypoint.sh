#!/bin/bash
set -e

if [ "${1}" = "init" ] ; then
    echo "Initialize module"

    mkdir -p /data
    cd /data

    ## Databases ##
    ## BBTOOLS
    # /global/projectb/sandbox/gaag/bbtools/data/Illumina.artifacts.2013.12.no_DNA_RNA_spikeins.fa.gz
    # /global/projectb/sandbox/gaag/bbtools/commonMicrobes/fusedERPBBmasked.fa.gz
    # /global/projectb/sandbox/gaag/bbtools/data/adapters.fa # included in bbmap
    # /global/projectb/sandbox/gaag/bbtools/data/kapatags.L40.fa # included in bbmap
    
    ## MISC
    # /global/dna/shared/rqc/ref_databases/qaqc/databases/illumina.artifacts/Illumina.artifacts.2013.12.no_DNA_RNA_spikeins.fa
    # /global/dna/shared/rqc/ref_databases/qaqc/databases/illumina.artifacts/DNA_spikeins.artifacts.2012.10.fa.bak
    # /global/dna/shared/rqc/ref_databases/qaqc/databases/illumina.artifacts/RNA_spikeins.artifacts.2012.10.NoPolyA.fa
    # /global/dna/shared/rqc/ref_databases/qaqc/databases/JGIContaminants.fa
    # /global/dna/shared/rqc/ref_databases/qaqc/databases/pCC1Fos.ref.fa
    # /global/dna/shared/rqc/ref_databases/qaqc/databases/phix174_ill.ref.fa # included in bbmap    
    # /global/dna/shared/rqc/ref_databases/qaqc/databases/rRNA.fa
    # /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/refseq.plastid/refseq.plastid
    # /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/refseq.mitochondrion/refseq.mitochondrion
    

    ## Need a place to store the ref files to download
    #echo "Downloading bbmap reference files..."
    ## BBTOOLS_VER = 38.12
    ## wget http://downloads.sourceforge.net/project/bbmap/BBMap_${BBTOOLS_VER}.tar.gz
    ## The ref files for bbtools can also be found from bbmap/resources in the bbtools distribution
    # /global/projectb/scratch/sulsj/sealsh-refs/bbmap-refs.tar.gz
    # wget ... |tar zxvf -
    #echo "Downloading contam database files..."
    # /global/projectb/scratch/sulsj/sealsh-refs/ref.tar.gz
    # wget ... |tar zxvf -
    
    ## readqc ref + bbtools ref files
    echo "Downloading reference database files..."
    git clone http://gitlab+deploy-token-7962:e7obLrJbusxcaewmaFio@gitlab.com/sulsj/jgi-rqc-readqc-ref.git
    tar zxvf jgi-rqc-readqc-ref/readqc_ref.tar.gz .
    rm -rf jgi-rqc-readqc-ref

    if [ -f Illumina.artifacts.2013.12.no_DNA_RNA_spikeins.fa.gz ] && \
       [ -f fusedERPBBmasked.fa.gz ] && \
       [ -f adapters.fa ] && \
       [ -f Illumina.artifacts.2013.12.no_DNA_RNA_spikeins.fa ] && \
       [ -f DNA_spikeins.artifacts.2012.10.fa.bak ] && \
       [ -f RNA_spikeins.artifacts.2012.10.NoPolyA.fa ] && \
       [ -f JGIContaminants.fa ] && \
       [ -f pCC1Fos.ref.fa ] && \
       [ -f refseq.mitochondrion ] && \
       [ -f rRNA.fa ] && \
       [ -f refseq.plastid ] && \
       [ -f phix174_ill.ref.fa ] && \
       [ -f kapatags.L40.fa ] ; then
        touch __READY__
    else
        echo "Init failed"
    fi
fi

exec "$@"

## EOF