#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Alignment QC (RNA QC)
* replaces jgi-rqc-legacy/support_scripts/qc_report_illumina_rna.pl
- updated: 2013-12-06 (v1.2.1)

External Dependencies:
bbtools

Alignment 1.2.1
- subsample rate changes based on reads (250kb min read count)
- using bbtools.reformat for subsampling
- using machine readable format for parsing output
- insert size from bbmap - no, using old insert size calc for now

Alignment 1.2.2
- use get_read_count_fastq instead of calculating filtered fastq read count, minor logic fixes for subsampling


Alignment 1.2.5
- applied new bbtools: rqcfilter.sh

Alignment 1.2.8
- bbtools changes, added ihist=1 to bbmap

Alignment 1.3
- change to pass --prod-type instead of filter params to pass to new filter command

Alignment 1.4
- Remove interleaved validation step for subsampling, adapter trim
- Use bbduk to trim adapters (not yet)
- mapping
--- Replace "out" with "nodisk" option - no, need sam file
--- add "rcs=f" only for LMP data
--- add minscaf=500 (don't use scaffolds < 500bp for mapping)
--- add ihist=FILE_NAME for insert size histogram, capture mean, median, mode, stdev

Alignment 1.5
- Use bbmap ihist instead of older insert size calc (faster)
- skip steps where the file exists (do this)
- replaced gnuplot with matplotlib
* 1.5.1
- fix for insert size when nothing maps, missing filtered fastq on rerun
* 1.5.2
- transcriptome mapping updated to use: strictmaxindel=4 and minid=0.9.

Alignment 1.6 - 2016-05-06
- skip filtering step option
- skip subsampling option
- work with fastq.gz files
- removed adapter trim function, old insert size function (using jigsaw), do_initalize function

Alignment 1.6.1 - 2016-05-17
- bug with using the -nf -ns flags not setting properly (Kurt)

Alignment 1.6.2 - 2016-05-23
- sequencing depth (coverage) = # reads mapped x read length / # bases in reference
- (1) stats.sh on input fastq to get bases
- (2) stats.sh on reference
- seq depth = (1) * mapped % / (2)
- read length varies because using filtered fastq, need the fastq stats (stats.sh)
--- got to the point of saving (1) and (2), need to add to interpolated stats, what if it was filtered after subsampling?
--- decision to still use (1) for input bases

Alignment 1.6.3 - 2016-08-23
- Kurt found a bug where re-running and -no-filter option is on will cause the pipeline to fail, fixed by removing filtered fq symlink

Alignment 1.6.4 - 2016-09-09
- Jasmyn found a bug with passing a different subsampling rate
- fixed Paired-end check which fails if filtered fastq is input

Alignment 1.6.5 - 2016-09-14
- Jasmyn found "sys.exit(14)" from my testing which shant have been there

Alignment 1.6.6 - 2016-11-11
- Kurt asked to use relative paths for output path option

Alignment 1.6.7 - 2017-02-21
- keep sam files to generate pileup data

Alignment 1.6.8 - 2017-02-27
- gzip sam files, not storing in RQC

Alignment 1.6.9 - 2017-03-29
- updated insert size title (RQC-940)
- only do insert size plot if 2 or more data points in isize_bucket

Alignment 1.7: 2017-04-26
- remove EnvironmentModules
- do not do db call to look up references if transcriptome or genome reference is passed to us
- use run_cmd to work on Denovo

Alignment 1.8: 2018-02-21
- removed old commented out code
- updated subsampled code to be more efficient
- shred references (bbmap bug if scaffolds > 500m - RQC-1082)

Alignment 1.8.1: 2018-04-10
- subsampling fails sometimes leaving partially complete fastq.  Instead write to a tmp file and then rename it, if the renamed doesn't exist then re-do it

Alignment 1.8.2: 2018-06-29
- added bbtools_log_check to watch for bbtools memory issues when running bbmap (do_mapping step only)


required params:
--fastq
--output-path
optional params: (optional because we try to look up the reference from RQC's database, normally required)
--genome-ref
--transcriptome-ref
--read-count



for testing use ./align_test.sh

# AHGPS - simple test, file is already subsampled to 1%
./alignment.py -o t1
/global/projectb/sandbox/rqc/analysis/qc_user/alignment/AHGPS.fastq
reformat.sh in=/global/projectb/sandbox/rqc/analysis/qc_user/alignment/AHGPS.fastq out=/global/projectb/sandbox/rqc/analysis/qc_user/alignment/AHGPS-0.01.fastq samplerate=0.01 overwrite
* time = 2 minutes

$ ~/git/jgi-rqc/rqc_system/test/compare.py -rf /global/projectb/sandbox/rqc/analysis/qc_user/alignment/AHGPS-20150428,/global/projectb/sandbox/rqc/analysis/qc_user/alignment/AHGPS-20150519 -p alignment
$ ~/git/jgi-rqc/rqc_system/test/compare.py -rf /global/projectb/sandbox/rqc/analysis/qc_user/alignment/AAZPS-20150428,/global/projectb/sandbox/rqc/analysis/qc_user/alignment/AAZPS-20150519 -p alignment


./alignment.py -o t1
-- subsampled filtered fastq for quick testing

Alignment QC Steps:

    (optional: look up reference files for fastq in RQC database -- in align_lib.py)

    subsample fastq to 1% (possibly 10%) - optional

    filter (RQC's fastq_filter pipeline) - optional


    if transcriptome reference exists:
        align to transcriptome with bbmap
        sam stats from transcriptome.sam

    if transcriptome reference exists and paired-ended:
        calc insert size info


    if genome reference exists:
        align to genome with bbmap
        sam stats from genome.sam

    if genome reference exists and paired-ended:
        calc insert size info

    clean up intermediate files (--no-purge prevents this)


9080.1.120056.TGACCA.fastq.gz: ANHHG = smRNA

qsub -b yes -j yes -m as -now no -w e -N a-ANHHG -l h_rt=43195,ram.c=120G -P gentech-rqc.p -o /global/scratch2/sd/brycef/align-ANHHG.log "/global/homes/b/brycef/git/jgi-rqc-pipeline/alignment/alignment.py -o /global/scratch2/sd/brycef/ANHHG -f /global/dna/dm_archive/sdm/illumina/00/90/80/9080.1.120056.TGACCA.fastq.gz --read-count 8480112 --transcriptome-ref /global/dna/shared/rqc/sequence_repository/Zea_mays/Zea_mays.reference_transcriptome_draft.fasta --genome-ref /global/dna/shared/rqc/sequence_repository/Zea_mays/Zea_mays.reference_genome_draft.1060181.fasta --prod-type SMRNA"

./alignment.py -o /global/projectb/sandbox/rqc/analysis/qc_user/alignment/AHGPS -f /global/projectb/sandbox/rqc/analysis/qc_user/alignment/AHGPS.fastq.gz --read-count 3775018 --transcriptome-ref /global/dna/shared/rqc/sequence_repository/Oryza_sativa/Oryza_sativa.reference_transcriptome_draft.fasta --genome-ref /global/dna/shared/rqc/sequence_repository/Oryza_sativa/Oryza_sativa.reference_genome_draft.fasta --prod-type DNA


new test
$S/git/jgi-rqc-pipeline/alignment/alignment.py -pl -nf \
--output-path /global/projectb/scratch/brycef/align/CGWUZ \
--fastq $DM_ARCHIVE/rqc/analyses/AUTO-143603/12167.3.241625.CTTGTA.filter-RNA.fastq.gz \
--transcriptome-ref $BSCRATCH/qc_user/REF_TO_JAMO/Tr_syao_1515454749/Hvulgare_462_r1.transcript.fa \
--genome-ref $BSCRATCH/qc_user/REF_TO_JAMO/Ge_syao_1515454591/Hvulgare_462_r1.fa


"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import glob # find files

import numpy
from scipy import stats # mode
import matplotlib
matplotlib.use("Agg") ## This needs to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
import mpld3


# custom libs in "../lib/"
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir, '../lib'))
sys.path.append(os.path.join(dir, '../tools'))
from common import get_logger, run_cmd, human_size, checkpoint_step, append_rqc_file, append_rqc_stats, get_run_path, get_read_count_fastq, get_subsample_rate, bbtools_log_check


from rqc_fastq import read_length_from_file
from rqc_constants import RQCConstants
from align_lib import get_lib_info, get_reference_location, check_reference_fasta


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions



'''
Subsample the fastq
determine paired ended or single ended
- 1% to 5% is fine
- converts quality to q33 also

subsample with bbtools_reformat_cmd:
$ reformat.sh in=7348.8.68143.fastq out=subsample.fastq samplerate=0.01 qout=33
- 21G == 180.399 seconds ~ 6x faster than subsample_fastq_pl

@param output_path: path to write all output for the pipeline
@param fastq: /path/to/fastq
@param fastq_name: name of the fastq
@param parent_run_path: top level path for pipelines
@param subsample_rate: rate for subsampling the fastq
@param read_count: count of reads from the fastq
@param subsample_flag: perform subsample (true) or use original file (false)

@return status: new status
@return subsample_fastq: /path/to/subsampled fastq
@return paired_flag: false = fastq is single read, true = fastq is paired read


'''
def do_subsampling(fastq, fastq_name, subsample_rate, read_count = 0, subsample_flag = True):

    log.info("do_subsampling: %s", fastq_name)
    step_name = "subsample"

    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)


    # get paired ended, read1 length, read2 length from fastq (1st 100 lines)
    paired_flag = False

    r1, r2, is_pe = read_length_from_file(fastq, log, 100)
    paired_flag = is_pe
    log.info("- fastq paired? %s  (r1 = %s, r2 = %s)", paired_flag, r1, r2)

    # creates 7242.2.63850.CTTGTA.subsampled_0.01.fastq
    #subsample_fastq = get_modified_fastq_name("subsample", fastq_name, subsample_rate)
    # lets make it simple ...
    subsample_fastq = os.path.join(output_path, fastq_name.replace(".fastq.gz", ".subsample.fastq.gz"))

    # calculate read count
    if read_count == 0:
        #read_count = get_read_count_fastq(fastq, log)
        # get number of bases in the original fastq - needed for seq depth calculation
        fastq_log = os.path.join(output_path, "fastq_stats.log")
        cmd = "#bbtools;stats.sh in=%s format=3 ow=t > %s 2>&1" % (fastq, fastq_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        fh = open(fastq_log, "r")
        for line in fh:
            if line.startswith("n_scaffolds"):
                continue
            else:
                arr = line.split()
                read_count = int(arr[0])
        fh.close()
        log.info("- read count: %s", "{:,}".format(read_count))

    # use default subsample rate if not passed (or == 0)
    if not subsample_rate:
        subsample_rate = 0.01

        if read_count > 0 and subsample_flag:
            # use read count to determine subsampling rate
            # - if sample_rate * read_count < 250k then calculate a new_subsample_rate
            new_subsample_rate = get_subsample_rate(read_count)
            if new_subsample_rate != subsample_rate:
                subsample_rate = new_subsample_rate
                log.info("- adjusted subsample rate to %s based on read count", "{:.2%}".format(subsample_rate))


    # without qin=33 then it uses auto detect, Illumina is phread64 but we need to convert to phred33
    if os.path.isfile(subsample_fastq):
        log.info("- already subsampled, not subsampling again: %s", subsample_fastq)
        exit_code = 0

    else:

        if subsample_flag:
            reformat_log = os.path.join(output_path, "subsample.log")
            subsample_fastq_tmp = os.path.join(output_path, "subsample.fastq.tmp.gz")
            cmd = "#bbtools;reformat.sh in=%s out=%s samplerate=%s qin=33 qout=33 ow=t > %s 2>&1" % (fastq, subsample_fastq_tmp, subsample_rate, reformat_log)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            # do this if the subsampling completes as expected otherwise can leave a partially complete subsampled fastq (BWYSW) - 2018-04-10
            cmd = "mv %s %s" % (subsample_fastq_tmp, subsample_fastq)
            std_out, std_err, exit_code = run_cmd(cmd, log)

        else:
            log.info("- NOT subsampling, sym-linking to original fastq")
            cmd = "ln -s %s %s" % (fastq, subsample_fastq)
            std_out, std_err, exit_code = run_cmd(cmd, log)


    subsample_read_count = 0

    status = "%s failed" % step_name

    if exit_code == 0:
        status = "%s complete" % step_name

        # check that we created a subsampled fastq
        subsample_size = 0

        if os.path.isfile(subsample_fastq):


            # get number of reads in subsample fastq
            subsample_read_count = get_read_count_fastq(subsample_fastq, log)

        else:
            status = "%s failed" % step_name
            log.error("- subsampled fastq does not exist!  %s", subsample_fastq)

    else:
        log.error("- subsample failed!")
        status = "%s failed" % step_name
        subsample_fastq = None


    # record actual subsample rate used
    if status == "subsample complete":

        append_rqc_stats(rqc_stats_log, "raw read count", read_count)
        append_rqc_stats(rqc_stats_log, "subsample read count", subsample_read_count)

        # actual subsample rate
        subsample_rate = subsample_read_count / float(read_count)
        log.info("- real subsample rate = %s/%s = %s", subsample_read_count, read_count, "{:.2%}".format(subsample_rate))

        append_rqc_stats(rqc_stats_log, "subsample rate", subsample_rate)

        if paired_flag:
            append_rqc_stats(rqc_stats_log, "read count", 2)
        else:
            append_rqc_stats(rqc_stats_log, "read count", 1)

    checkpoint_step(status_log, status)


    return status, subsample_fastq, paired_flag



'''
Run quality filtering on subsampled fastq using RQC's filtering pipeline (wrapper for bbtools.rqcfilter.sh) [Bioinformatics Bestus]

@param output_path: path to write all output for the pipeline
@param subsample_fastq: /path/to/subsampled.fastq
@param fastq_name: name of the fastq
@param filter_prod_type: filtering product type (DNA, RNA, CLIP-PE, CLRS)
@param parent_run_path: top level path for pipelines (output_path normally)
@param filter_flag: filter the fastq (True) or sym-link it (False)

@return status: new status
@return trim_fastq: /path/to/filtered fastq

'''
def do_filter(fastq, fastq_name, filter_prod_type, parent_run_path, filter_flag = True):

    log.info("do_quality_filter: %s", fastq_name)

    step_name = "filter"
    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)


    filter_fastq = get_modified_fastq_name("filter", fastq_name)
    filter_path = os.path.join(output_path, "filter")
    if not os.path.isdir(filter_path):
        log.info("- cannot find path %s, creating path", filter_path)
        os.makedirs(filter_path)


    # RQC's Fastq filter program
    filter_bin = os.path.join(parent_run_path, "filter/fastq_filter2.py")

    filter_log = os.path.join(output_path, "filter-cmd.log")

    #[pipeline_bin_path]/filter/fastq_filter2.py -o [output_path] -f [fastq] --prod-type [prod_type]
    # --skip_qc: don't need the qc metrics

    if filter_flag:
        cmd = "%s -o %s -f %s --prod-type %s --skip-subsampleqc > %s 2>&1" % (filter_bin, filter_path, fastq, filter_prod_type, filter_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    else:

        filtered_fastq = os.path.basename(fastq).replace(".fastq", ".no-filter.fastq")
        filtered_fastq = os.path.join(filter_path, filtered_fastq)

        log.info("- NOT Filtering, sym-linking file instead: %s", filtered_fastq)

        # remove old sym-link if exists ...
        if os.path.isfile(filtered_fastq):
            cmd = "rm %s" % filtered_fastq
            std_out, std_err, exit_code = run_cmd(cmd, log)


        cmd = "ln -s %s %s" % (fastq, filtered_fastq)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # create a fake filtered file-list.txt
        fh = open(os.path.join(filter_path, "file-list.txt"), "w")
        fh.write("# Skipped filtering, fake file\n")
        fh.write("filtered_fastq=%s\n" % filtered_fastq)
        fh.close()

    status = "%s failed" % step_name

    if exit_code == 0:
        status = "%s complete" % step_name

        # filter pipeline creates file-list.txt which has the name of the filtered fastq
        fh = open(os.path.join(filter_path, "file-list.txt"), "r")
        for line in fh:
            line = line.strip()
            if line.startswith("filtered_fastq"):
                try:
                    filter_fastq = line.split("=")[1]

                    # might have file without full path
                    if not filter_fastq.startswith("/"):
                        filter_fastq = os.path.join(output_path, "filter", filter_fastq)
                        log.info("- updated filter file path to: %s", filter_fastq)

                except:
                    log.error("- file-list.txt not formatted correctly: %s", line)

        fh.close()

        # check if filtered fastq exists
        if not os.path.isfile(filter_fastq):

            status = "%s failed" % step_name
            log.error("- filtered fastq does not exist!  %s", filter_fastq)

    else:
        filter_fastq = None
        status = "%s failed" % step_name

    checkpoint_step(status_log, status)

    return status, filter_fastq


'''
Alignment Mapping
- works for transcriptome or genome
- not doing pacbio reads yet (use mapPacBio8k.sh -- part of bbtools)
- bbmap: short read aligner for DNA, RNASeq
-- added ihist=t option to bbmap but still using older pe_stats.pl for histogram (2014-10-23)

@param output_path: path to write all output for the pipeline
@param trimmed_fastq: /path/to/trimmed & subsampled.fastq
@param fastq_name: name of the fastq
@param ref_fasta: reference fasta
@param ref_type: "transcriptome" or "genome"

@return status: new status

'''
def do_mapping(fastq, fastq_name, ref_fasta, ref_type, filter_prod_type):

    log.info("do_mapping: %s, %s", ref_type, ref_fasta)

    step_name = "%s mapping" % ref_type
    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)


    # add directory for alignment output
    align_path = os.path.join(output_path, ref_type)
    if not os.path.isdir(align_path):
        log.info("- cannot find path %s, creating path", align_path)
        os.makedirs(align_path)

    # "bbmap.sh ref=<assembly.fa> in=<reads.fq> out=<mapped.sam>").
    # For PacBio reads (as used in James's circle detection) the script is /house/homedirs/b/bushnell/mapPacBio8k.sh instead of bbmap.sh.

    mapped_sam = os.path.join(align_path, "%s-%s.sam" % (fastq_name, ref_type))
    log_file = os.path.join(align_path, "bbmap-%s.log" % (ref_type))
    ihist_file = os.path.join(align_path, "insert-hist-%s.txt" % (ref_type))


    output_type = "machineout"

    # "rcs=f" only for LMP data
    if str(filter_prod_type).lower() in ("clrs", "lmp"):
        output_type += " rcs=f"

    # 2015-08-05 Vasanth:
    # I had a brief chat with Erika today and we decided to have the mapping to report only high-quality results. So the bbmap and bbsplit parameters with transcritome
    # references should use: strictmaxindel=4 and minid=0.9.
    if ref_type.startswith("trans"):
        output_type += " strictmaxindel=4 minid=0.9"



    if os.path.isfile(log_file) and os.path.isfile(ihist_file):
        exit_code = 0
        log.info("- log_file and ihist files exist, skipping bbmap: %s", log_file)
    else:

        # 2018-02-21 Brian:
        # need to shred references so max size of scaffolds is 500mb, might not need xmx of 32gb
        tmp_ref_fasta = ref_fasta
        tmp_ref_fasta = os.path.join(align_path, os.path.basename(tmp_ref_fasta + ".shred.fa"))
        shred_log = os.path.join(align_path, "shred-%s.log" % ref_type)

        cmd = "#bbtools;shred.sh -Xmx32000m in=%s out=%s length=500M ow=t > %s 2>&1" % (ref_fasta, tmp_ref_fasta, shred_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


        cmd = "#bbtools;bbmap.sh ref=%s qin=33 ihist=%s in=%s out=%s path=%s %s log > %s 2>&1" % (tmp_ref_fasta, ihist_file, fastq, mapped_sam, align_path, output_type, log_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)
        # minscaf=500 - 2018-01-22 - removed, there are short genes

        # Replace "out" with "nodisk" option - No, need sam output file
        # minscaf=500 (don't use scaffolds < 500bp for mapping)
        # add ihist=FILE_NAME for insert size histogram, capture mean, median, mode, stdev

        bb_exit_code = bbtools_log_check(log_file)
        if bb_exit_code > 0:
            log.info("- bb_exit_code: %s", bb_exit_code)
            sys.exit(bb_exit_code)


        # get the stats for the reference (just need assembly size in bases)
        ref_log = os.path.join(align_path, "ref_stats-%s.log" % ref_type)
        cmd = "#bbtools;stats.sh in=%s format=3 ow=t > %s 2>&1" % (ref_fasta, ref_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # remove shreded ref, not needed and takes up lots of disk space
        cmd = "rm %s" % tmp_ref_fasta
        std_out, std_err, exit_code = run_cmd(cmd, log)


    status = "%s complete" % step_name

    # failed
    if exit_code > 0:
        status = "%s failed" % step_name

    else:

        stats_key = "%s reference file" % ref_type
        append_rqc_stats(rqc_stats_log, stats_key, ref_fasta)


        # collect output into stats & files
        # -- to change when Brian has update
        save_alignment_stats(ref_type, log_file)

        append_rqc_stats(rqc_stats_log, "%s:output type" % (ref_type), output_type)
        append_rqc_file(rqc_file_log, "%s:insert_histogram" % (ref_type), ihist_file)

    checkpoint_step(status_log, status)

    return status, mapped_sam



'''
Collect results from bbmap's output and store in rqc_stats.txt
- assumes "machineout" param is set

@param ref_type: "transcriptome" or "genome"
@param ref_stats_file: path/to/bbmap-(ref_type).log

@return number of stats saved

'''
def save_alignment_stats(ref_type, ref_stats_file):

    log.info("save_alignment_stats: %s, %s", ref_type, ref_stats_file)
    stats_cnt = 0

    r1_mapped_pct = None # 61.25%
    r2_mapped_pct = None # 62.24%
    # R1_Mapped_Percent
    # R2_Mapped_Percent

    if os.path.isfile(ref_stats_file):

        # parse log_file: bbmap-%s.log
        fh = open(ref_stats_file, "r")

        store_flag = False # skip lines if false, try to store if true

        for line in fh:

            if "Results" in line:
                store_flag = True
            if line.startswith("Total time:"):
                store_flag = False

            if store_flag == True:
                line = line.strip()
                #print line

                if line:
                    stats_list = line.split("=")

                    stats_key = None
                    stats_val = None

                    if len(stats_list) == 2:
                        stats_key = str(stats_list[0]).strip()
                        stats_val = str(stats_list[1]).strip()

                        if stats_key.lower() == "r1_mapped_percent":
                            r1_mapped_pct = stats_val.replace("%","")

                        if stats_key.lower() == "r2_mapped_percent":
                            r2_mapped_pct = stats_val.replace("%","")


                        # prefix key with ref_type
                        stats_key = "%s:%s" % (ref_type, stats_key)

                        if stats_val:
                            append_rqc_stats(rqc_stats_log, stats_key, stats_val)
                            log.info("- stat: %s = %s", stats_key, stats_val)
                            stats_cnt += 1

        fh.close()


        # save output file
        file_key = "bbmap %s" % (ref_type)
        append_rqc_file(rqc_file_log, file_key, ref_stats_file)

        # coverage (sequencing depth) calc:
        ref_bbstats = os.path.join(output_path, ref_type, "ref_stats-%s.log" % ref_type)
        ref_bases = 0 # bases in the reference
        if os.path.isfile(ref_bbstats):
            fh = open(ref_bbstats, "r")
            for line in fh:
                if line.startswith("n_scaffolds"):
                    continue
                else:
                    arr = line.split()
                    if len(arr) > 3:
                        ref_bases = arr[3] # scaffolds = 3, contigs = 2
            fh.close()
        else:
            log.error("- %s does not exist!", ref_bbstats)

        fq_bbstats = os.path.join(output_path, "fastq_stats.log")
        fq_bases = 0 # bases in the input fastq

        if os.path.isfile(fq_bbstats):
            fh = open(fq_bbstats, "r")
            for line in fh:
                if line.startswith("n_scaffolds"):
                    continue
                else:
                    arr = line.split()
                    if len(arr) > 3:
                        fq_bases = arr[3] # scaffolds = 3, contigs = 2
            fh.close()

        seq_depth = 0.0
        mapped_pct = 0.0
        try:
            # avg of r1 + r2
            mapped_pct = (float(r1_mapped_pct) + float(r2_mapped_pct)) / 2.0 / 100.0
        except:
            log.error("- failed to calc mapped_pct: (%s + %s) / 2.0 / 100", r1_mapped_pct, r2_mapped_pct)

        #- (1) stats.sh on input fastq to get bases
        #- (2) stats.sh on reference
        #- seq depth = (1) * mapped % / (2)
        try:
            if ref_bases > 0:
                seq_depth = (int(fq_bases) * mapped_pct) / float(ref_bases)
            log.info("- sequencing depth: (%s * %s) / %s = %s", fq_bases, mapped_pct, ref_bases, seq_depth)
        except:
            log.error("- failed to calc seq_depth: (%s * %s) / %s", fq_bases, mapped_pct, ref_bases)


        append_rqc_stats(rqc_stats_log, "%s:sequencing_depth" % ref_type, seq_depth)


    else:
        log.error("- %s does not exist!", ref_stats_file)

    return stats_cnt



'''
Generate stats from the sam file

@param sam_file: /path/to/sam/file
@param ref_type: reference type (for storing stats)
@param paired_flag: if paired-ended then True, else False

@return status: new status

* results validated against code in qc_report_illumina_rna.pl 2013-08-05 for a paired ended seq unit

'''
def do_calc_sam_stats(sam_file, ref_type, paired_flag):

    log.info("do_calc_sam_stats %s: %s", ref_type, sam_file)

    step_name = "%s sam stats" % ref_type
    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)


    pair_no = 1 # 1 = single read, 2 = paired read (to fix...)
    if paired_flag:
        pair_no = 2

    # results
    read_cnt = 0 # total reads
    pair_cnt = 0 # count of paired reads that mapped
    single_cnt = 0 # count of single reads that mapped
    pair_same_cnt = 0 # count of paired reads that map to the same reference
    pair_diff_cnt = 0 # count of pairs that map to a different reference
    bad_pair_cnt = 0 # count of bad pairs, should always be 0

    if os.path.isfile(sam_file):

        # read file and process flag field
        fh = open(sam_file, "r")

        seq_flag_list = []

        flag_pos = 1

        for line in fh:
            line = line.strip()

            # skip header lines
            if line[0] == "@":
                continue

            read_cnt += 1

            sam_val_list = line.split() # split on whitespace

            # expect more than 12 fields in a sam file
            if len(sam_val_list) >= 11:

                if str(sam_val_list[flag_pos]).find(":"):
                    flag_pos = 2

                seq_flag_list.append(sam_val_list[flag_pos])

            if read_cnt % pair_no == 0:

                pe_cnt = 0 # are we paired?
                map_cnt = 0 # count of mapped reads (0, 1 = single, 2 = paired)
                flag_cnt = 0 # count of number of flags processed
                proper_pair = 0 # = 2 if read1 & read2 are a pair

                for seq_flag in seq_flag_list:

                    flag_cnt += 1

                    # bin(77) = 0b1001101 = 0b 100 1101 (to be readable)
                    # 77 & 1 =  0b 100 1101 & 0b 000 0001 = 0000 0001 (returns only the bits that match)

                    # bin(0x1) = 0b1
                    # bin(0x4) = 0b100


                    v = int(seq_flag) & 0x1

                    if v == 1:
                        pe_cnt += 1


                    v = int(seq_flag) & 4 # 0x4 = segment unmapped

                    if v != 4:
                        map_cnt += 1

                    # only for paired ended
                    if flag_cnt == 1 and pair_no == 2:
                        proper_pair = int(seq_flag) & 2



                # 1 = 1 read was mapped
                if map_cnt == 1:
                    single_cnt += 1
                # 2 = both reads were mapped, count as paired
                elif map_cnt == 2:
                    pair_cnt += 2 # since its paired

                    # mapped to the same genome or different genomes
                    if proper_pair == 2:
                        pair_same_cnt += 2
                    else:
                        pair_diff_cnt += 2

                # test if paired correctly
                if pe_cnt != pair_no:
                    bad_pair_cnt += 1


                seq_flag_list = []


        fh.close()


        num_mapped = single_cnt + pair_cnt



        stats_key = "%s:number mapped" % (ref_type)
        append_rqc_stats(rqc_stats_log, stats_key, num_mapped)
        log.info("- stat: %s = %s", stats_key, num_mapped)

        stats_key = "%s:single ended reads mapped count" % (ref_type)
        append_rqc_stats(rqc_stats_log, stats_key, single_cnt)
        log.info("- stat: %s = %s", stats_key, single_cnt)

        stats_key = "%s:paired ended reads mapped count" % (ref_type)
        append_rqc_stats(rqc_stats_log, stats_key, pair_cnt)
        log.info("- stat: %s = %s", stats_key, pair_cnt)

        stats_key = "%s:total read count" % (ref_type)
        append_rqc_stats(rqc_stats_log, stats_key, read_cnt)
        log.info("- stat: %s = %s", stats_key, read_cnt)

        stats_key = "%s:paired read same map count" % (ref_type)
        append_rqc_stats(rqc_stats_log, stats_key, pair_same_cnt)
        log.info("- stat: %s = %s", stats_key, pair_same_cnt)

        stats_key = "%s:paired read diff map count" % (ref_type)
        append_rqc_stats(rqc_stats_log, stats_key, pair_diff_cnt)
        log.info("- stat: %s = %s", stats_key, pair_diff_cnt)

        # manually calculate overall pct matched based on reads used
        stats_key = "%s:map ratio" % (ref_type)
        if read_cnt > 0:
            stats_val = num_mapped / float(read_cnt)
        else:
            stats_val = 0
        append_rqc_stats(rqc_stats_log, stats_key, stats_val)
        log.info("- stat: %s = %s", stats_key, stats_val)



        # SAM = sequence alignment map format
        #  1 QNAME: HISEQ08:318:C24JVACXX:7:1101:11223:2227#TGCTGG
        #           HISEQ06:270:C2ABNACXX:3:1101:6989:90923 1:N:0:ATTCCT * sometimes can be this
        #  2 FLAG: 83
        #  3 RNAME: supercont10.1
        #  4 POS: 2031349
        #  5 MAPQ: 75
        #  6 CIGAR: 40M6D21M1D39M
        #  7 MRNM/RNEXT: =
        #  8 MPOS/PNEXT: 2030989
        #  9 ISIZE/TLEN: -466
        # 10 SEQ: ACGCGATCCAATGCTGGGGTGATGGGTTTTGCTGCAAAAAGAATGGTTTTAGGTTCACACCTTGGAAAAAGGGCTCAAAACCTGTATCATCAATCGAGAC
        # 11 QUAL: DDDDDDDDDCCDDDDBDEEEEEFFFHHFHHJJIIJJJJJJJJIGJJJJIIJJJIGHDGBJJJJJJIJJJJJJIHGIJIHHJHIHIJJHHHHHFFFFFCCC
        # 12 TAGs: XT:A:U NM:i:12 SM:i:75 ...

        # flag bits:
        # 0x1 template having multiple segments in sequencing
        # 0x2 each segment properly aligned according to the aligner
        # 0x4 segment unmapped
        # 0x8 next segment in the template unmapped
        # 0x10 SEQ being reverse complemented
        # 0x20 SEQ of the next segment in the template being reversed
        # 0x40 the first segment in the template
        # 0x80 the last segment in the template
        # 0x100 secondary alignment
        # 0x200 not passing quality controls
        # 0x400 PCR or optical duplicate
        # 0x800 supplementary alignment


    status = "%s complete" % step_name
    checkpoint_step(status_log, status)

    return status



'''
Extrapolate subsampled results to full fastq results
- run after do_calc_sam_stats

@param ref_type: "transcriptome" or "genome"

@return new status
'''
def extrapolate_sam_stats(ref_type):

    log.info("extrapolate_sam_stats: %s", ref_type)

    step_name = "%s extrapolation" % ref_type
    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)


    stats_dict = {}

    if os.path.isfile(rqc_stats_log):

        # pull values from alignment-stats.tmp
        fh = open(rqc_stats_log, "r")

        for line in fh:
            line = line.strip()

            if line:
                arr = line.split("=")
                if len(arr) == 2:
                    key = str(arr[0]).lower().strip()
                    value = str(arr[1]).strip()

                    stats_dict[key] = value

        fh.close()

        # calculate subsample ratio (total reads / subsample reads)
        sub_ratio = float(stats_dict['raw read count']) / float(stats_dict['subsample read count'])

        # list of params to extrapolate
        calc_list = ["reads_used", "number mapped", "single ended reads mapped count", "paired ended reads mapped count", "paired read same map count", "paired read diff map count"]

        for calc in calc_list:

            key = "%s:%s" % (ref_type, calc)
            val = 0
            if key in stats_dict:
                val = stats_dict[key]

            # extrapolated = (value / subsampled reads) * total reads used == value * sub_ratio (do the math yourself)
            try:
                stats_val = int(sub_ratio * float(val))
            except:
                stats_val = 0

            stats_key = "%s:100:%s" % (ref_type, calc)

            append_rqc_stats(rqc_stats_log, stats_key, stats_val)
            log.info("- stat: %s = %s", stats_key, stats_val)


        status = "%s complete" % step_name


    else:

        status = "%s failed" % step_name
        log.error("- cannot find %s!", rqc_stats_log)


    checkpoint_step(status_log, status)

    return status


'''
bbmap already does the ihist.txt when the alignment is done so we just need to parse ihist.txt and save the values


'''
def do_insert_size_bb(ref_type, filter_prod_type):

    log.info("do_insert_size_bb: %s, %s", ref_type, filter_prod_type)

    isize_path = os.path.join(output_path, ref_type)
    ihist_file = os.path.join(isize_path, "insert-hist-%s.txt" % ref_type)

    step_name = "%s insert size" % ref_type
    status = "%s in progress" % step_name
    checkpoint_step(status_log, status)

    i_avg = 0.0
    i_std = 0.0
    i_median = 0
    i_mode = 0
    i_pct_of_pairs = 0.0

    # calculate lmp (>2kb) vs frag seperately because std dev can be bias
    max_size = 2000

    i_lmp_avg = 0.0
    i_lmp_std = 0.0
    i_lmp_median = 0
    i_lmp_mode = 0
    lmp_list = []

    i_frag_avg = 0.0
    i_frag_std = 0.0
    i_frag_median = 0
    i_frag_mode = 0
    frag_list = []

    hist_dict = {} # histogram
    hbin_size = 10 # histogram bin size
    x_max = 1000 # x-axis maximum

    # LMP libraries need larger scale
    if filter_prod_type.lower() == "clrs" or filter_prod_type.lower() == "nextera":
        hbin_size = 100
        x_max = 10000

    status = "%s failed" % step_name

    if os.path.isfile(ihist_file):

        isize_max = 0


        fh = open(ihist_file, "r")
        for line in fh:
            if line.startswith("#"):

                arr = line.strip().replace("#", "").split()

                if str(arr[0]).lower().startswith("mean"):
                    i_avg = arr[1]
                elif str(arr[0]).lower().startswith("median"):
                    i_median = arr[1]
                elif str(arr[0]).lower().startswith("stdev"):
                    i_std = arr[1]
                elif str(arr[0]).lower().startswith("mode"):
                    i_mode = arr[1]

                elif str(arr[0]).lower().startswith("percentofpairs"):
                    i_pct_of_pairs = arr[1]
            else:
                arr = line.strip().split()
                isize = int(arr[0])
                cnt = int(arr[1])

                #hbin = int(hbin_size * round(isize / float(hbin_size)))
                hbin = int(hbin_size * int(isize / float(hbin_size)))

                # for histogram
                bucket = str(hbin).zfill(6)
                if bucket in hist_dict:
                    hist_dict[bucket] += cnt
                else:
                    hist_dict[bucket] = cnt

                # Yes, its probably the last number in the file ...
                if isize > isize_max:
                    isize_max = isize

                if cnt >= 5:
                    for _ in range(cnt):
                        if isize < max_size:
                            frag_list.append(isize)
                        else:
                            lmp_list.append(isize)

        fh.close()

        #for k in sorted(hist_dict.iterkeys()):
        #    print "%s|%s" % (k, hist_dict[k])


        insert_histogram_bucket = os.path.join(isize_path, "isize_bucket.txt")
        fh = open(insert_histogram_bucket, "w")
        for k in sorted(hist_dict.iterkeys()):
            fh.write("%s %s\n" % (int(k), hist_dict[k]))
        fh.close()

        append_rqc_file(rqc_file_log, "%s:insert_histogram_bucket" % ref_type, insert_histogram_bucket)

        isize_max = min(x_max, isize_max) # for plotting



        if len(frag_list) > 0:
            i_frag_avg = numpy.average(frag_list)
            i_frag_std = numpy.std(frag_list)
            i_frag_median = numpy.median(frag_list)
            i_frag_mode = stats.mode(frag_list)[0][0]


        if len(lmp_list) > 0:
            i_lmp_avg = numpy.average(lmp_list)
            i_lmp_std = numpy.std(lmp_list)
            i_lmp_median = numpy.median(lmp_list)
            i_lmp_mode = stats.mode(lmp_list)[0][0]

        # frag = inward, lmp = outward - waiting on bbtools to give me these numbers
        log.info("- Frag: %s +/- %s, median = %s, mode = %s, cnt = %s", i_frag_avg, i_frag_std, i_frag_median, i_frag_mode, len(frag_list))
        log.info("- LMP: %s +/- %s, median = %s, mode = %s, cnt = %s", i_lmp_avg, i_lmp_std, i_lmp_median, i_lmp_mode, len(lmp_list))
        log.info("- bbmap: %s +/- %s, median = %s, mode = %s", i_avg, i_std, i_median, i_mode)

        append_rqc_stats(rqc_stats_log, "%s:insert_size_avg" % ref_type, i_avg)
        append_rqc_stats(rqc_stats_log, "%s:insert_size_std" % ref_type, i_std)
        append_rqc_stats(rqc_stats_log, "%s:insert_size_median" % ref_type, i_median)
        append_rqc_stats(rqc_stats_log, "%s:insert_size_mode" % ref_type, i_mode)

        append_rqc_stats(rqc_stats_log, "%s:insert_size_frag_avg" % ref_type, i_frag_avg)
        append_rqc_stats(rqc_stats_log, "%s:insert_size_frag_std" % ref_type, i_frag_std)
        append_rqc_stats(rqc_stats_log, "%s:insert_size_frag_median" % ref_type, i_frag_median)
        append_rqc_stats(rqc_stats_log, "%s:insert_size_frag_mode" % ref_type, i_frag_mode)

        append_rqc_stats(rqc_stats_log, "%s:insert_size_lmp_avg" % ref_type, i_lmp_avg)
        append_rqc_stats(rqc_stats_log, "%s:insert_size_lmp_std" % ref_type, i_lmp_std)
        append_rqc_stats(rqc_stats_log, "%s:insert_size_lmp_median" % ref_type, i_lmp_median)
        append_rqc_stats(rqc_stats_log, "%s:insert_size_lmp_mode" % ref_type, i_lmp_mode)



        if len(hist_dict) > 1:

            # matplotlib chart
            raw_data = numpy.loadtxt(insert_histogram_bucket, comments='#', usecols=(0,1))
            #raw_data = numpy.loadtxt(ihist_file, comments='#', usecols=(0,1))
            fig, ax = plt.subplots()

            marker_size = 5.0
            line_width = 1.5


            p1 = ax.plot(raw_data[:,0], raw_data[:,1], 'r', marker='o', markersize=marker_size, linewidth=line_width, alpha=0.5)
            plt.xlim(0, isize_max)
            #plt.title("%s Insert Size (%sbp bin size)" % (ref_type.title(), hbin_size))
            plt.title("%s Alignment-based Insert Size Estimate (%sbp bin size)" % (ref_type.title(), hbin_size)) # RQC-940

            #n, bins, patches = plt.hist(x, 50, normed=1, facecolor='g', alpha=0.75)

            ax.set_xlabel("Insert Size (bp)", fontsize=12, alpha=0.5)
            ax.set_ylabel("Number of Reads", fontsize=12, alpha=0.5)

            ax.grid(color="gray", linestyle=':')


            png_file = os.path.join(isize_path, "insert_size_histogram-%s.png" % ref_type)
            html_file = png_file.replace(".png", ".html")

            # html format
            mpld3.save_html(fig, html_file)
            ## Save Matplotlib plot in png format
            plt.savefig(png_file, dpi=fig.dpi)

            append_rqc_file(rqc_file_log, "%s:insert_histogram_png" % ref_type, png_file)
            if html_file:
                append_rqc_file(rqc_file_log, "%s:insert_histogram_html" % ref_type, html_file)


        else:
            log.error("- no data for insert size!")

        status = "%s complete" % step_name

    else:
        log.error("- missing file: %s", ihist_file)



    checkpoint_step(status_log, status)

    return status


'''
Creates a new reference fasta in the output folder that trims out any lines less than (trim_size)

@param ref_fasta: /path/to/fasta
@param output_path: path to write all output for the pipeline
@param trim_size: keep sequences with at least this many bases

@return /path/to/new/ref_fasta

'''
def trim_reference_fasta(ref_fasta, trim_size = 750):

    log.info("trim_reference_fasta: %s (%s)", ref_fasta, trim_size)

    new_ref_fasta = ref_fasta
    if ref_fasta:

        if os.path.isfile(ref_fasta):

            new_ref_fasta_name = "%s.%s.trim" % (os.path.basename(ref_fasta), trim_size)
            new_ref_fasta = os.path.join(output_path, new_ref_fasta_name)

            if os.path.isfile(new_ref_fasta):
                log.info("- using existing ref fasta: %s", new_ref_fasta)
            else:

                fh = open(ref_fasta, "r")

                fh_new = open(new_ref_fasta, "w")

                header = None # stores current header line
                seq_buffer = "" # stores sequence

                cnt = 0 # count of header lines
                good_cnt = 0 # count of header lines that have sequences > trim_size

                for line in fh:
                    line = line.strip()

                    if line.startswith(">"):
                        cnt += 1
                        # write previous line if >= 750
                        if len(seq_buffer) >= trim_size:
                            if header:
                                fh_new.write(header+"\n")
                                fh_new.write(seq_buffer+"\n")
                                good_cnt += 1


                        # reset lines
                        header = line
                        seq_buffer = ""

                    else:
                        seq_buffer += line+"\n"




                # check if last line passes filter
                if len(seq_buffer) >= trim_size:
                    if header:
                        fh_new.write(header+"\n")
                        fh_new.write(seq_buffer+"\n")
                        good_cnt += 1


                fh.close()
                fh_new.close()

                log.info("- kept %s/%s sequences", good_cnt, cnt)
                log.info("- created new ref fasta: %s", new_ref_fasta)


        else:
            log.error("- cannot find ref_fasta!  %s", ref_fasta)
            new_ref_fasta = ref_fasta


    return new_ref_fasta



'''
Determines the next step to run
- make sure "* in progress" restarts ...
@param status: status of alignment pipeline

@return: next step/status to run

'''
def get_next_step(status):

    log.info("get_next_step: %s", status)

    next_status = None

    status_dict = {
        "start" : "start",
        "initialization complete" : "subsample",

        "subsample complete" : "filter",
        "filter complete" : "transcriptome mapping",
        "transcriptome mapping complete" : "transcriptome sam stats",
        "transcriptome mapping skipped" : "genome mapping",
        "transcriptome sam stats complete" : "transcriptome extrapolation",
        "transcriptome extrapolation complete" : "transcriptome insert size",
        "transcriptome insert size complete" : "genome mapping",

        "genome mapping complete" : "genome sam stats",
        "genome mapping skipped" : "complete",
        "genome sam stats complete" : "genome extrapolation",
        "genome extrapolation complete" : "genome insert size",
        "genome insert size complete" : "complete",

        # restarting if they fail on this step
        "subsample in progress" : "subsample",
        "filter in progress" : "filter",
        "transcriptome mapping in progress" : "transcriptome mapping",
        "transcriptome sam stats in progress" : "transcriptome sam stats",
        "transcriptome extrapolation in progress" : "transcriptome extrapolation",
        "transcriptome insert size in progress" : "transcriptome insert size",
        "genome mapping in progress" : "genome mapping",
        "genome sam stats in progress" : "genome sam stats",
        "genome extrapolation in progress" : "genome extrapolation",
        "genome insert size in progress" : "genome insert size",

        "failed" : "failed",
        "complete" : "complete",
    }



    if status:
        status = status.lower()

        if status in status_dict:
            next_status = status_dict[status]
        else:
            if status.endswith("failed"):
                next_status = "failed"
            else:
                next_status = "failed: next step missing for '%s'" % (status)

    log.info("- next status = %s", next_status)
    return next_status



'''
Delete files or paths in a list (path doesn't need to be empty)

@param file_list: list of /path/to/file and /full/path to remove from file system
@return count of files successfully purged

'''
def fs_purge(file_list, zip_list):

    purge_cnt = 0

    for purge_file in file_list:

        if purge_file:

            # file
            if os.path.isfile(purge_file):
                cmd = "rm %s" % (purge_file)
                std_out, std_err, exit_code = run_cmd(cmd, log)

                if exit_code == 0:
                    log.info("- purged file: %s", purge_file)
                    purge_cnt += 1
                else:
                    log.error("- failed to remove file: %s", purge_file)

            # directory
            if os.path.isdir(purge_file):
                cmd = "rm -rf %s" % (purge_file)
                std_out, std_err, exit_code = run_cmd(cmd, log)

                if exit_code == 0:
                    purge_cnt += 1
                    log.info("- purged path: %s", purge_file)
                else:
                    log.error("- failed to remove path: %s", purge_file)


    for zip_file in zip_list:
        if zip_file:
            if os.path.isfile(zip_file):
                cmd = "#pigz;pigz %s" % zip_file
                std_out, std_err, exit_code = run_cmd(cmd, log)

                if exit_code == 0:
                    log.info("- gzipped file: %s", zip_file)
                    purge_cnt += 1
                else:
                    log.error("- failed to gzip file: %s", zip_file)


    return purge_cnt


'''
Rename stats log and file log from *.tmp to *.txt

@param rqc_stats_log: /path/to/alignment-stats.tmp
@param rqc_file_log: /path/to/alignment-files.tmp

@return new status
'''
def process_log_files(rqc_stats_log, rqc_file_log):

    status = "complete"

    log.info("process_log_files: %s, %s" , rqc_stats_log, rqc_file_log)

    if os.path.isfile(rqc_stats_log):

        rqc_new_stats_log = os.path.join(output_path, "alignment-stats.txt")

        cmd = "mv %s %s" % (rqc_stats_log, rqc_new_stats_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(rqc_file_log):
        rqc_new_file_log = os.path.join(output_path, "alignment-files.txt")

        cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    return status



'''
Function to return standardized name for: trimmed, adapter trimmed and subsampled fastqs
- moves .fastq.gz to the end of the new name

@param file_type: "trim", "adapter_trim", "subsample"
@param output_path: path to write all output for the pipeline
@param fastq_name: original name of the fastq
@param extra_param: extra data for the fastq name

@return /path/to/fastq-<type>.fastq
'''
def get_modified_fastq_name(file_type, fastq_name, extra_param = None):

    fastq_file = os.path.join(output_path, fastq_name)
    new_fastq_name = fastq_name
    if fastq_name.endswith(".fastq"):
        new_fastq_name = fastq_name.replace(".fastq", "")
    elif fastq_name.endswith(".fastq.gz"):
        new_fastq_name = fastq_name.replace(".fastq.gz", "")

    if file_type == "trim":
        fastq_file = os.path.join(output_path, "%s-trim.fastq" % (new_fastq_name))

    if file_type == "filter":
        fastq_file = os.path.join(output_path, "%s-filter.fastq" % (new_fastq_name))

    elif file_type == "adapter_trim":
        fastq_file = os.path.join(output_path, "%s-adapter.fastq" % (new_fastq_name))

    elif file_type == "subsample":
        fastq_file = os.path.join(output_path, "%s.subsampled_%s.fastq" % (new_fastq_name, extra_param))


    if fastq_name.endswith(".gz"):
        if not fastq_file.endswith(".gz"):
            fastq_file += ".gz"


    return fastq_file




## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "RQC Alignment"
    version = "1.8.2"


    # Parse options
    usage = "prog [options]\n"
    usage += "* RQC Alignment Pipeline, version %s\n" % (version)


    parser = ArgumentParser(usage = usage)
    # -s 1
    parser.add_argument("-f", "--fastq", dest="fastq", type=str, help="fastq file (full path to fastq)")
    parser.add_argument("-o", "--output-path", dest="output_path", help="Output path to write to, uses pwd if not set")
    parser.add_argument("-r", "--reset", dest="reset", default = False, action="store_true", help="Full reset of run")
    parser.add_argument("-s", "--subsample-rate", dest="subsample", help="Subsampling rate (default = 0.01 = 1%%)", type = float)
    parser.add_argument("-g", "--genome-ref", dest="genome", help="genome reference fasta")
    parser.add_argument("-t", "--transcriptome-ref", dest="transcriptome", help="transcriptome reference fasta")
    parser.add_argument("-nf", "--no-filter", dest="filter_flag", action="store_false", help="Do not filter input fastq")
    parser.add_argument("-ns", "--no-subsample", dest="subsample_flag", action="store_false", help = "Do not subsample input fastq")
    parser.add_argument("-rc", "--read-count", dest="read_count", help="read count of fastq", type = int)

    # filter params for filter pipeline
    parser.add_argument("-fpt", "--prod-type", dest="prod_type", help="Filtering product type, default = DNA (DNA, RNA, CLIP-PE, CLRS, smRNA)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-n", "--no-purge", dest="nopurge", default = False, action="store_true", help="do not remove intermediate files")
    parser.add_argument("-v", "--version", action="version", version=version)


    # variable initialization
    reset_flag = False # reset this run
    output_path = None
    log_level = "INFO"
    fastq = None # /full/path/to/fastq file (hopefully unzipped, but works with gzipped)
    fastq_name = None # simple fastq name
    purge_flag = True # purge file (false = don't purge)
    read_count = 0
    filter_flag = True # false = do not run filtering
    subsample_flag = True # false = do not subsample

    paired_flag = False # True = fastq is paired ended, false = single read fastq (determined from subsampling, not an input)



    status = None # pipeline status

    subsample_rate = 0 # 1% to 5% subsampling is fine - set to 0.01 in the subsample function

    # transcriptome and genome reference fasta files
    transcriptome_ref = None
    genome_ref = None
    # transcriptome and genome mapping flags (True = run mapping, False = skip)
    transcriptome_flag = False
    genome_flag = False
    # transcriptome and genome output sam files
    transcriptome_sam_file = None
    genome_sam_file = None

    subsample_fastq = None # path to subsampled fastq
    filter_fastq = None # path to trimmed fastq
    print_log = False

    # filter params
    filter_prod_type = "DNA"


    # parse args
    args = parser.parse_args()

    print_log = args.print_log

    if args.output_path:
        output_path = args.output_path

    if args.fastq:
        fastq = args.fastq

    if args.read_count:
        read_count = args.read_count

    if args.reset:
        reset_flag = True

    if args.subsample_flag == False:
        subsample_flag = args.subsample_flag

    if args.filter_flag == False:
        filter_flag = args.filter_flag


    if args.nopurge:
        purge_flag = False

    if args.subsample:
        try:
            subsample_rate = float(args.subsample)
        except:
            subsample_rate = 0

    # x passed from pipeline_controller if there is no reference
    if args.transcriptome:
        transcriptome_ref = args.transcriptome
        if transcriptome_ref == "x":
            transcriptome_ref = None

    if args.genome:
        genome_ref = args.genome
        if genome_ref == "x":
            genome_ref = None


    if args.prod_type:
        filter_prod_type = args.prod_type

    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # Bryce's tests
    if output_path.startswith("t"):

        tmp_path = "/global/projectb/scratch/brycef/align"
        if output_path == "t1":
            output_path = os.path.join(tmp_path, "AHGPS")
            transcriptome_ref = "/global/dna/shared/rqc/sequence_repository/Oryza_sativa/Oryza_sativa.reference_transcriptome_draft.fasta"
            genome_ref = "/global/dna/shared/rqc/sequence_repository/Oryza_sativa/Oryza_sativa.reference_genome_draft.fasta"

            filter_prod_type = "DNA"
            # 3.5 minutes, 2 minutes w/o filtering
            fastq = "/global/projectb/sandbox/rqc/analysis/qc_user/alignment/AHGPS-0.01.fastq.gz"

            subsample_flag = False
            filter_flag = False
            #read_count = 37920
            #read_count = 3775018
            # Filter on:
            # transcriptome:R1_Match_Rate = 95.2262%
            # transcriptome:R2_Match_Rate = 95.2017%
            # genome:R1_Match_Rate = 91.9970%
            # genome:R2_Match_Rate = 87.0138%
            # Filter off:
            # transcriptome:R1_Match_Rate = 95.4183%
            # transcriptome:R2_Match_Rate = 94.2055%
            # genome:R1_Match_Rate = 90.1886%
            # genome:R2_Match_Rate = 84.0412%

        if output_path == "t2":
            output_path = os.path.join(tmp_path, "AHGPS-not-subsampled")
            transcriptome_ref = "/global/dna/shared/rqc/sequence_repository/Oryza_sativa/Oryza_sativa.reference_transcriptome_draft.fasta"
            genome_ref = "/global/dna/shared/rqc/sequence_repository/Oryza_sativa/Oryza_sativa.reference_genome_draft.fasta"

            filter_prod_type = "DNA"
            # filtered fastq:
            fastq = "/global/projectb/sandbox/rqc/analysis/qc_user/alignment/AHGPS.fastq.gz"
            filter_flag = False

        if output_path == "t3":
            output_path = os.path.join(tmp_path, "AZXGB")
            # cp $SDM_FILTERED_FASTQ/00/01/04/85/10485.4.163938.GAATTCG-GTACTGA.anqrpht.fastq.gz  /global/projectb/sandbox/rqc/analysis/qc_user/alignment/AZXGB.filter.fastq.gz
            transcriptome_ref = "/global/dna/shared/rqc/sequence_repository/Brachypodium_sylvaticum/Brachypodium_sylvaticum.reference_transcriptome_draft.1105308.fasta"
            genome_ref = "/global/dna/shared/rqc/sequence_repository/Brachypodium_sylvaticum/Brachypodium_sylvaticum.reference_genome_draft.1105308.fasta"

            filter_prod_type = "DNA"
            # filtered fastq:
            fastq = "/global/projectb/sandbox/rqc/analysis/qc_user/alignment/AZXGB.filter.fastq.gz"
            filter_flag = False



    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    # initialize my logger
    log_file = os.path.join(output_path, "alignment.log")


    # log = logging object
    log = get_logger("align", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s: %s", script_name, fastq)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "fastq", fastq)
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "subsample_rate", subsample_rate)
    log.info("%25s      %s", "transcriptome_ref", transcriptome_ref)
    log.info("%25s      %s", "genome_ref", genome_ref)
    log.info("%25s      %s", "read_count", read_count)
    log.info("%25s      %s", "filtering prod_type", filter_prod_type)
    log.info("%25s      %s", "filter_flag", filter_flag)
    log.info("%25s      %s", "subsample_flag", subsample_flag)
    log.info("%25s      %s", "reset_flag", reset_flag)
    log.info("%25s      %s", "purge_flag", purge_flag)
    log.info("")




    ## Validate output path and fastq

    if not fastq:
        print "Missing fastq!"
        log.error("Missing fastq!")
        sys.exit(2)


    if not os.path.isfile(fastq):
        fastq = os.path.realpath(fastq)
        if not os.path.isfile(fastq):
            log.error("Cannot find fastq: %s!", fastq)
            sys.exit(2)


    fastq_name = os.path.basename(fastq)


    # parent_run_path = top level path for jgi-rqc-pipeline
    parent_run_path = get_run_path()
    log.info("run path = %s", parent_run_path)



    # create a name by stripping .fastq from the fastq name
    rqc_file_log = os.path.join(output_path, "alignment-files.tmp")
    rqc_stats_log = os.path.join(output_path, "alignment-stats.tmp")


    # RQC System log
    status_log = os.path.join(output_path, "alignment_status.log")


    ## test area
    #extrapolate_sam_stats("genome")
    #extrapolate_sam_stats("transcriptome")
    #trim_reference_fasta("/global/dna/shared/rqc/sequence_repository/Penicillium_raistrickii/Penicillium_raistrickii.reference_genome_draft.fasta", "/global/projectb/scratch/brycef/tmp2/NTAY")
    #do_insert_size_new("genome", "/global/dna/shared/rqc/pipelines/alignment/archive/02/87/03/74/")
    #do_insert_size_new("genome", "/global/dna/shared/rqc/pipelines/alignment/archive/02/86/28/43/")
    #do_insert_size_new("genome", "/global/projectb/sandbox/rqc/analysis/qc_user/alignment/AHGPS-20150702/")
    #save_alignment_stats("genome", "/global/projectb/scratch/brycef/align/AHGPS/genome/bbmap-genome.log")
    #sys.exit(44)


    # get the status for the pipeline
    if reset_flag == True:
        # reset rqc_file_log, rqc_stats_log
        status = "start"

    else:

        status = "start" # force restart every time
        if status not in ("start", "complete"):
            # if the status isn't start, check for a trimmed and subsampled fastq

            # look for subsampled file

            subsample_fastq = get_modified_fastq_name("subsample", fastq_name, subsample_rate)
            if not os.path.isfile(subsample_fastq):
                subsample_fastq = None

            # look for filtered file
            filter_fastq = get_modified_fastq_name("filter", fastq_name)
            if not os.path.isfile(filter_fastq):
                filter_fastq = None

            # if files are missing, reset status properly
            if not subsample_fastq:
                status = "subsample"

            if subsample_fastq and not filter_fastq:
                status = "filter"

            # get paired ended flag
            if subsample_fastq:
                r1, r2, is_pe = read_length_from_file(fastq, log, 10000)
                if r1 == r2:
                    paired_flag = True


    # look up reference files & make sure they are on disk
    spid = 0
    ncbi_organism_name = ""


    if not transcriptome_ref and not genome_ref:
        spid, ncbi_organism_name = get_lib_info(fastq_name, log)

        # cannot find *.fastq.gz, try to find *.srf
        if spid == 0 and ncbi_organism_name is None:
            spid, ncbi_organism_name = get_lib_info(fastq_name.replace(".fastq.gz", ".srf"), log)

        if not transcriptome_ref:
            transcriptome_ref = get_reference_location("transcriptome", ncbi_organism_name, spid, log)

        if not genome_ref:
            genome_ref = get_reference_location("genome", ncbi_organism_name, spid, log)


    transcriptome_flag = check_reference_fasta(transcriptome_ref, log)
    genome_flag = check_reference_fasta(genome_ref, log)


    # no reference files
    if not transcriptome_flag and not genome_flag:
        log.info("No genome and no transcriptome reference.")
        checkpoint_step(status_log, "failed: no reference fasta")
        sys.exit(2)

    done = False
    cycle = 0 # number of steps ran, limit to 10


    # set umask
    umask = os.umask(RQCConstants.UMASK)

    if status == "complete":
        log.info("Status is complete for %s, exiting.", fastq_name)
        done = True


    while done == False:

        cycle += 1


        # determine the next step
        status = get_next_step(status)

        # initialize
        # - paired ended or single ended
        if status == "start":
            status = "subsample"


        # subsample
        if status == "subsample":
            status, subsample_fastq, paired_flag = do_subsampling(fastq, fastq_name, subsample_rate, read_count, subsample_flag)

        # Filter Sequences
        if status == "filter":
            status, filter_fastq = do_filter(subsample_fastq, fastq_name, filter_prod_type, parent_run_path, filter_flag)


        # Transcriptome Mapping
        if status == "transcriptome mapping":

            if transcriptome_flag == True:
                status, transcriptome_sam_file = do_mapping(filter_fastq, fastq_name, transcriptome_ref, "transcriptome", filter_prod_type)
            else:
                status = "transcriptome mapping skipped"

        # Transcriptome sam stats
        if status == "transcriptome sam stats":
            status = do_calc_sam_stats(transcriptome_sam_file, "transcriptome", paired_flag)

        if status == "transcriptome extrapolation":
            status = extrapolate_sam_stats("transcriptome")

        # transcriptome insert size
        if status == "transcriptome insert size":
            status = do_insert_size_bb("transcriptome", filter_prod_type)

        # Genome Mapping
        if status == "genome mapping":
            if genome_flag == True:
                status, genome_sam_file = do_mapping(filter_fastq, fastq_name, genome_ref, "genome", filter_prod_type)
            else:
                status = "genome mapping skipped"

        # Genome sam stats
        if status == "genome sam stats":
            status = do_calc_sam_stats(genome_sam_file, "genome", paired_flag)

        if status == "genome extrapolation":
            status = extrapolate_sam_stats("genome")

        # Genome insert size
        if status == "genome insert size":
            status = do_insert_size_bb("genome", filter_prod_type)


        if status == "complete":
            done = True

        if status.startswith("failed"):
            done = True

        # don't cycle more than 10 times ...
        if cycle > 20:
            done = True


    log.info("Finished alignment steps for %s", fastq_name)

    if status == "complete":

        # move rqc-files.tmp to rqc-files.txt
        status = process_log_files(rqc_stats_log, rqc_file_log)


    # remove intermediate files
    if status == "complete":
        if purge_flag == True:

            log.info("Purging temp files")

            file_list = []
            zip_list = [] # gzip these files
            # base path
            file_list = glob.glob(os.path.join(output_path, "*.fastq"))

            # transcriptome
            zip_list = zip_list + glob.glob(os.path.join(output_path, "transcriptome/*.sam"))

            file_list = file_list + glob.glob(os.path.join(output_path, "transcriptome/ref"))
            #file_list = file_list + glob.glob(os.path.join(output_path, "transcriptome_insert_size/*.trim"))
            #file_list = file_list + glob.glob(os.path.join(output_path, "transcriptome_insert_size/*.fastq"))
            # genome
            zip_list = zip_list + glob.glob(os.path.join(output_path, "genome/*.sam"))
            file_list = file_list + glob.glob(os.path.join(output_path, "genome/ref"))
            #file_list = file_list + glob.glob(os.path.join(output_path, "genome_insert_size/*.trim"))
            #file_list = file_list + glob.glob(os.path.join(output_path, "genome_insert_size/*.fastq"))

            # filtering
            file_list = file_list + glob.glob(os.path.join(output_path, "filter/*.fastq"))

            # remove 'em all
            purge_cnt = fs_purge(file_list, zip_list)
            log.info("- purged %s files", purge_cnt)



    checkpoint_step(status_log, status)


    os.umask(umask)

    log.info("Completed %s: %s", script_name, fastq_name)

    sys.exit(0)

"""

BBMAP Output: (Brian Bushnell provided this info)

Reads Used:             Number of input reads
Minimum Score Ratio:    Describes sensitivity as worst mapping allows as fraction of maximum mapping score
Mapping Mode:           Modes are 'normal', 'pacbio', 'perfect', and 'semiperfect'
Genome:                 Irrelevant
Key Length:             Length of kmers used as seeds
Max Indel:              Longest insertion or deletion that will be explicitly searched for when mapping


mapped:                 Percent of input reads that mapped with adequate mapping score
unambiguous:         Percent of input reads that mapped with adequate mapping score AND whose best mapping site was  substantially better than the next-best mapping site

Mated pairs:             Percent of input reads such that both mapped to the same scaffold with the correct strand orientation and within the allowed pairing distance
bad pairs:              Percent of input reads such that both mapped, but violated pairing criteria
rescued:                 Percent of input reads that only mapped by searching around the mapping location of their mate
avg insert size:        Estimated insert size from mapping

perfect best site:      Percent of input reads mapping with all bases fully defined (not 'N'), and all bases matching reference
semiperfect site:       Percent of input reads mapping with all bases fully defined, and all bases matching reference except where the reference is undefined.  There are no mismatches and at least 50% of the reference must be defined.
ambiguousMapping:        Percent of input reads mapping to multiple locations. "(Kept)" means that these were included in the output as mapped to one of these locations.
low-Q discards:          Percent of input reads with quality too low to map.  This includes reads that are all Ns.
Match Rate:             Percent and number of cigar string operations indicating bases exactly matching the reference.
Error Rate:              Percent and number of cigar string operations indicating substitutions, deletions, or insertions.
Sub Rate:                Percent and number of cigar string operations indicating substitutions.
Del Rate:                Percent and number of cigar string operations indicating deletions.
Ins Rate:                Percent and number of cigar string operations indicating insertions.
N Rate:                  Percent and number of cigar string operations over symbols where either the read or the reference base was undefined.






                                     OOOOOO
                                   OOOOOOOOOO
                                  OOOOOOOOOOOO
                                 OO OO OO OO OO
                                OOOOOOOOOOOOOOOO
                                  OOO  OO  OOO
                                   O        O




   G      G
    G    G
   GGGGGGGG
  GG GGGG GG
 GGGGGGGGGGGG
 G GGGGGGGG G
 G G      G G
    GG  GG

       YYYY
    YYYYYYYYYY
   YYYYYYYYYYYY
   YYY  YY  YYY
   YYYYYYYYYYYY
      YY  YY
     YY YY YY
   YY        YY

          ZZ
         ZZZZ
        ZZZZZZ
       ZZ ZZ ZZ
       ZZZZZZZZ
         Z  Z
        Z ZZ Z
       Z Z  Z Z




  Bryce was here

"""