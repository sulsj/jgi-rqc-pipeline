#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Alignment QC library

Functions to support alignment qc
- looks up reference information in RQC database if not passed by command line

To Do:
- stop processing .srf files

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import sys
import MySQLdb
import os
import re

# custom libs in "../lib/"
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir, '../lib'))
from db_access import jgi_connect_db



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions


'''
Look up the seq_project_id, bio_name from the library_info table
- to look up the references

@param seq_unit_name: name of the seq unit in the seq_units.seq_unit_name field

@return seq_project_id, ncbi_organism_name
'''
def get_lib_info(seq_unit_name, log = None):

    if log:
        log.info("get_lib_info: %s", seq_unit_name)

    # these fields are neeeded to look up the reference files in the seq_location_repos db
    seq_project_id = 0
    ncbi_organism_name = None

    if seq_unit_name:


        # look up gzipped fastq info
        if seq_unit_name.endswith(".fastq"):
            seq_unit_name = "%s.gz" % seq_unit_name

        # handle filtered fastqs
        my_list = seq_unit_name.split(".")
        if len(my_list) > 6:
            seq_unit_name = "%s.%s.%s.%s.fastq.gz" % (my_list[0], my_list[1], my_list[2], my_list[3])

        # read only connection
        db = jgi_connect_db("rqc")
        sth = db.cursor(MySQLdb.cursors.DictCursor)

        sql = """
select
    su.seq_unit_id,
    lib.ncbi_organism_name,
    lib.seq_proj_id
from seq_units su
inner join library_info lib on su.rqc_library_id = lib.library_id
where
    su.seq_unit_name = %s
        """

        sth.execute(sql, (seq_unit_name,))
        rs = sth.fetchone()

        if rs:
            seq_project_id = rs['seq_proj_id']
            ncbi_organism_name = rs['ncbi_organism_name']

            if log:
                log.info("- seq_proj_id: %s, ncbi: %s", seq_project_id, ncbi_organism_name)
        else:
            if log:
                log.error("- cannot find library information for seq_unit: %s", seq_unit_name)


        db.close()


    return seq_project_id, ncbi_organism_name


'''
Gets the best reference for the genome from seq_location_repos.seq_locations table
- look for seq_proj_id first (very few of these)
- look for bio_name (space -> underscore) if not seq_proj_id (1016311)
* the logic here is similar in rqc_setup.py:getReferenceLocation, please keep in sync

@param type: "transcriptome" or "genome"
@param bio_name: name of the specimen
@param seq_proj_id: sequencing project id

@return location of repo
'''
def get_reference_location(ref_type, ncbi_organism_name, seq_proj_id = 0, log = None):

    if log:
        log.info("get_reference: %s", ref_type)

    reference_file = None

    if ref_type.lower() == "genome":
        ref_type = "reference_genome"

    elif ref_type.lower() == "transcriptome":
        ref_type = "reference_transcriptome"
    else:
        if log:
            log.info("- ref_type unsupported: %s", ref_type)
        ref_type = None


    if ref_type:


        bio_name = None # just for logging


        # read only connection
        db = jgi_connect_db("seq_location_repos")
        sth = db.cursor(MySQLdb.cursors.DictCursor)

        if seq_proj_id > 0:

            sql = """
select
    sl.id,
    sl.bio_name,
    sl.current_location
from seq_locations sl
inner join seq_types st on sl.type_id = st.type_id
where
    sl.seq_proj_id = %s
    and st.type = %s
            """

            sth.execute(sql, (seq_proj_id, ref_type))

            rs = sth.fetchone()

            if rs:
                reference_file = rs['current_location']
                bio_name = rs['bio_name']
                if log:
                    log.info("- repo: %s, bio_name: %s", reference_file, bio_name)
                #print "sl.id = %s" % rs['id']

        # search by ncbi name
        if not reference_file and ncbi_organism_name:

            # filter special chars to "_"
            ncbi_organism_name = re.sub("[^A-Za-z0-9_:\#.\-]+", "_", ncbi_organism_name)
            #ncbi_organism_name = re.sub("\_+", "_", ncbi_organism_name)

            #print "NCBI: '%s'" % (ncbi_organism_name)

            sql = """
select
    sl.id,
    sl.bio_name,
    sl.current_location
from seq_locations sl
inner join seq_types st on sl.type_id = st.type_id
where
    %s REGEXP sl.bio_name
    and st.type = %s
order by sl.id desc
            """

            sth.execute(sql, (ncbi_organism_name, ref_type))

            rs = sth.fetchone()

            if rs:
                reference_file = rs['current_location']
                bio_name = rs['bio_name']
                if log:
                    log.info("- repo: %s, bio_name: %s", reference_file, bio_name)
                #print "sl.id2 = %s" % rs['id']

        db.close()



    return reference_file


'''
Checks if reference fasta is on the file system

@param ref_fasta: file system location for fasta

@return: True or False

'''
def check_reference_fasta(ref_fasta, log = None):

    if log:
        log.info("check_ref_fasta: %s", ref_fasta)

    flag = False

    if ref_fasta:
        if os.path.isfile(ref_fasta):
            flag = True
            if log:
                log.info("- found reference fasta : %s " % ref_fasta)
        else:
            if log:
                log.error("- ref fasta not on file system : %s", ref_fasta)
    else:
        if log:
            log.error("- no reference file")

    return flag

'''
for a given seq unit file name (can be filtered): search RQC DB and return SPID
'''
def spid_from_file_name(fname, log = None):
    sql = '''
SELECT L.seq_proj_id
FROM seq_unit_file F INNER JOIN seq_units S ON F.seq_unit_id=S.seq_unit_id
    INNER JOIN library_info L ON S.rqc_library_id=L.library_id
WHERE F.file_name = %s
    '''

    # read only connection
    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)
    
    if fname.endswith(".fastq"):
        fname = "%s.%s" % (fname , "gz")
            
    sth.execute(sql, (fname,))

    rs = sth.fetchone()

    spid = None
    if rs:
        spid = int(rs['seq_proj_id'])
        if log:
            log.info("- find SPID from RQC: %d, fname: %s" % (spid, fname) )

    db.close()
    return spid

'''
    return all ref genome and transcriptomes in seq_locations table for a given spid
'''
def all_refs_for_spid(spid):
    sql = '''
SELECT id, bio_name, current_location, type_id
FROM seq_locations
WHERE seq_proj_id = %s
ORDER BY entry_time DESC
    '''
    
    # read only connection
    db = jgi_connect_db("seq_location_repos")
    sth = db.cursor(MySQLdb.cursors.DictCursor)
    
    sth.execute(sql, (spid,))
    
    rows = sth.fetchall()
    genomes = []    # list of tuples of (BioName_G/T_ID, FILE_PATH)
    
    tlist = []
    for r in rows:
        fpath = r['current_location']
        if not os.path.isfile(fpath):
            print('ERROR : ref file in RQC db but not on disk : %s' % fpath)
        else:
            tag = ''
            if r['type_id'] == 16:  # genome
                tag = '%s_G' % r['bio_name']
            elif r['type_id'] == 18:    # transcriptome
                tag = '%s_T' % r['bio_name']
                
            #if bio_name and ref type are the same, only keep the most recently uploaded record
            if tag not in tlist:
                tlist.append(tag)
                genomes.append(('%s_%d' % (tag, r['id']), fpath))
                
    return genomes



def get_file_type(fname, log = None):
    
    if fname.endswith(".fastq"):
            fname = "%s.gz" % fname
        
    sql = "SELECT file_type FROM seq_unit_file WHERE file_name = %s"

    # read only connection
    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)
    
    sth.execute(sql, (fname,))

    rs = sth.fetchone()

    ftype = 'unknown'
    if rs:
        ftype = rs['file_type']
    
    if ftype and log:
        log.info("- find file type from RQC: %s, fname: %s" % (ftype, fname) )
    elif not ftype and log:
        log.info("- no file type in RQC : %s" % fname)

    db.close()
    return ftype

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":



    seq_unit_name = "7235.6.63512.CACTCA.fastq.gz"
    # temp - using srfs
    seq_unit_name = "7235.6.63512.CACTCA.srf"
    seq_unit_name = "7626.1.79184.ATTCCT.fastq.gz"
    seq_unit_name = "7626.1.79184.CCGTCC.fastq.gz"

    seq_unit_name = "9493.1.134445.ACAGTG.fastq.gz"
    seq_unit_name = "10424.6.160046.CGTACG.anqrpht.fastq.gz"
    print "Seq unit name: %s" % (seq_unit_name)
    seq_project_id, ncbi_organism_name = get_lib_info(seq_unit_name)

    print "Seq project id: %s" % (seq_project_id)
    print "NCBI organism name: %s" % (ncbi_organism_name)

    genome_ref_file = get_reference_location("genome", ncbi_organism_name, seq_project_id)
    trans_ref_file = get_reference_location("transcriptome", ncbi_organism_name, seq_project_id)

    print "Genome reference file: %s" % (genome_ref_file)


    if genome_ref_file:
        if check_reference_fasta(genome_ref_file):
            print "* Found genome reference file!"
        else:
            print "* Genome reference file not on file system"

    print "Transcriptome reference file: %s" % (trans_ref_file)
    if trans_ref_file:
        if check_reference_fasta(trans_ref_file):
            print "* Found transcriptome reference file!"
        else:
            print "* Transcriptome reference file not on file system"




    sys.exit(0)