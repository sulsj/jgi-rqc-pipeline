#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Alignment to multiple references (genome and or transcriptom)
* https://issues.jgi-psf.org/browse/RQC-646
MUST use RQC filtered fastq as the fastq input!

~~~~~~~~~~~~~~~~~~~~~~~~~~
If you have two reference say X.fa and Y.fa you can create the index for these two reference first.

CMD: bbsplit.sh ref_X=X.fa ref_Y=Y.fa

The names ref_X, ref_Y is used in the output where you will see reads mapping to X and Y presented.
Once the indexing is DONE (created in the current working directory), you can then map each of the input libraries to this.

CMD: bbsplit.sh in=LIB1.fq refstats=LIB1_stats.txt outu=LIB1_unmapped.fq minhits=1 maxindel=100 minratio=0.56

the file LIB1_stats.txt provides the output of mapping to each of the reference. The output will look like this:

#name   %unambiguousReads   unambiguousMB   %ambiguousReads ambiguousMB unambiguousReads    ambiguousReads
X   31.03680    44.74100    0.00000 0.00000 310368  0
Y   0.00180 0.00268 0.00000 0.00000 18  0

It will be great if unbiguousReads and ambiguousReads stats are captured in the RQC DB.
These parameters: minhits=1 maxindel=100 minratio=0.56  (provide BBmap like sensitivity)

Also you can find out the number of unmapped reads from the LIB1_unmapped.fq

Let me know if you have more questions. We can discuss in person.

Thanks,
Vasanth


    Version 1.0.0
    Shijie Yao
    2015-04-24 - initial implementation
    2015-07-13 - Per Vasanth:
                for transcriptome mapping, use bbsplit.sh in=%s refstats=%s outu=%s minhits=1 strictmaxindel=4 minid=0.9
                for genome mapping, use bbsplit.sh in=%s refstats=%s outu=%s minhits=1 minid=0.9
    2015-07-28 - remove minid=0.9 from genome mapping:
                for genome mapping, use bbsplit.sh in=%s refstats=%s outu=%s minhits=1

    2017-12-19 - works with Refs in JAMO (defined in rqc.refseq_spid table)

    subsample: total used reads
    count of reads in unmapped.fq : total_unmapped_reads
    map_stats.txt unambiguousReads : unambiguousReads_#name



    run examples:
    1) user provides genome references:
        alignment/multi_alignment.py -f FASTQ -g gFASTA1,gFASTA2 -o ./case1

    2) user provides both genome and transcriptome references:
        alignment/multi_alignment.py -f FASTQ -g gFASTA1,gFASTA2 -t tFASTA1,tFASTA2 -o ./case2

    3) user provides spid (but no -g and -t. -g and/or -t overwrites -i option] [use ref files in DB for -i ]:
        alignment/multi_alignment.py -f FASTQ -i 1062887 -o ./case3

    4) user only provides input file name [try to get spid by fname, and then ref files by spid in RQC DB]:
        multi_alignment.py -f /global/dna/dm_archive/sdm/illumina/00/89/97/8997.6.117647.TGCTGG.fastq.gz -o case4
"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import json
from pprint import pprint

# custom libs in "../lib/"
CUR_DIR = os.path.dirname(__file__)
sys.path.append(os.path.join(CUR_DIR, '../lib'))
sys.path.append(os.path.join(CUR_DIR, '../tools'))

from common import get_logger, get_status, run_command, human_size, checkpoint_step, append_rqc_stats, get_run_path, post_mortem_cmd, get_read_count_fastq

from rqc_fastq import read_length_from_file
from rqc_constants import RQCCommands, RQCConstants
import align_lib

PIPE_START = 'start'
INIT_START = 'start initialization'
INIT_END = 'end initialization'
SAMPLE_START = 'start subsample'
SAMPLE_END = 'end subsample'
FILTER_START = 'start filter'
FILTER_END = 'end filter'
G_INDEXING_START = 'start indexing genome ref'
G_INDEXING_END = 'end indexing genome ref'
G_MAPPING_START = 'start genome mapping'
G_MAPPING_END = 'end genome mapping'
T_INDEXING_START = 'start indexing transcriptome ref'
T_INDEXING_END = 'end indexing transcriptome ref'
T_MAPPING_START = 'start transcriptome mapping'
T_MAPPING_END = 'end transcriptome mapping'
POST_START = 'start post task'
POST_END = 'end post task'
FILES_STATS_START = 'start pipeline files and stats'
FILES_STATS_END = 'end pipeline files and stats'
PIPE_COMPLETE = 'complete'

STEP_ORDER = {PIPE_START : 0,
              INIT_START : 10,
              INIT_END : 20,
              SAMPLE_START : 50,
              SAMPLE_END : 60,
              FILTER_START : 70,
              FILTER_END : 80,
              G_INDEXING_START : 90,
              G_INDEXING_END : 91,
              G_MAPPING_START : 95,
              G_MAPPING_END : 96,
              T_INDEXING_START : 100,
              T_INDEXING_END : 101,
              T_MAPPING_START : 110,
              T_MAPPING_END : 120,
              FILES_STATS_START : 130,
              FILES_STATS_END : 140,
              PIPE_COMPLETE : 1000}

# the following constatns are specific to THIS pipeline
STATUS_LOG = 'status.log'
PURGE_ITEMS = []
JOB_STATS = {}

DOCKER_BBTOOLS_IMG = 'bryce911/bbtools'
DOCKER_BBTOOLS_REFORMAT = '/bbmap/reformat.sh'
DOCKER_BBTOOLS_BBSPLIT = '/bbmap/bbsplit.sh'

HOST = os.environ.get('NERSC_HOST', 'unknown')

## TODO - comments out the DEBUG line for production !!
DEBUG = False
# DEBUG = True

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

def add_stats(kname, value):
    'save stats to file and the dictionary'
    append_rqc_stats(RQC_STATS_LOG, kname, value)
    JOB_STATS[kname] = value

def load_stats():
    'load content in the job stats file into the JOB_STATS dictionaly'
    if os.path.isfile(RQC_STATS_LOG):
        with open(RQC_STATS_LOG, 'r') as fileh:
            for line in fileh:
                line = line.strip()
                tok = line.split('=')
                if len(tok) == 2:
                    JOB_STATS[tok[0].strip()] = tok[1].strip()

def write_stats():
    'write data in JOB_STATS dictionary to the job stats file'
    with open(RQC_STATS_LOG, 'w') as fileh:
        for kname in JOB_STATS.keys():
            buffer = "%s = %s\n" % (kname, JOB_STATS[kname])
            fileh.write(buffer)

# checkpoint logging
def checkpoint(step, last_step=PIPE_START):
    'update the pipeline check point log with the new step (step). last_step is the last pipeline step from the pipeline checkpoint file'
    if step == PIPE_START or STEP_ORDER[step] > STEP_ORDER[last_step]:
        checkpoint_step(STATUS_LOG, step)

'''
Subsample the fastq
determine paired ended or single ended
- 1% to 5% is fine
- converts quality to q33 also

subsample with bbtools_reformat_cmd:
$ reformat.sh in=7348.8.68143.fastq out=subsample.fastq samplerate=0.01 qout=33
- 21G == 180.399 seconds ~ 6x faster than subsample_fastq_pl

@param output_path: path to write all output for the pipeline
@param fastq: /path/to/fastq
@param fastq_name: name of the fastq
@param subsample_rate: rate for subsampling the fastq
@param read_count: count of reads from the fastq

@return status: new status
@return subsample_fastq: /path/to/subsampled fastq
@return paired_flag: false = fastq is single read, true = fastq is paired read


'''
def do_subsampling(last_step, oput_path, fastq_in, fq_name, sample_rate=0.01, read_count=0):
    'perform subsampling on the fastq file. input read_count overwrites input sample_rate'
    log_and_print(SAMPLE_START)
    checkpoint(SAMPLE_START, last_step)

    # creates 7242.2.63850.CTTGTA.subsampled_0.01.fastq
    subsample_fastq = get_modified_fastq_name("subsample", oput_path, fq_name, sample_rate)
    sample_read_count = 0
    PURGE_ITEMS.append(subsample_fastq)
    err = None

    if STEP_ORDER[last_step] <= STEP_ORDER[SAMPLE_START]:
        log_and_print("do_subsampling on file : %s" % fq_name)

        if os.path.isfile(subsample_fastq):
            log_and_print("  - deleting existing file : %s" % subsample_fastq)
            if not rm_cmd(subsample_fastq):
                log_and_print("  - failed to delete the existing file!", True)

        # get paired ended, read1 length, read2 length from fastq (1st 10k lines)
        paired = False
        r_one, r_tow, is_pe = read_length_from_file(fastq_in, LOG, 10000)
        if r_one == r_tow:
            paired = True

        if paired:
            log_and_print("  - fastq is paired ended")
        else:
            log_and_print("  - fastq is single read")

        # get file size, not sure why the subsampler can't do this itself...
        fastq_file_size = os.path.getsize(fastq_in)

        log_and_print("  - fastq file size: %s" % human_size(fastq_file_size))

        err_log = os.path.join(oput_path, "subsample.err")

        PURGE_ITEMS.append(err_log)

        # use read count to determine subsampling rate
        # - if sample_rate * read_count < 250k then calculate a new_sample_rate
        raw_reads = get_read_count_fastq(fastq_in, LOG)
        if read_count > 0:
            if read_count >= raw_reads:
                sample_rate = 1.0
            else:
                sample_rate = 1.0 * read_count / raw_reads

                log_and_print("  - adjusted subsample rate to %s based on read count" % "{:.2%}".format(sample_rate))


        acmd = None
        if HOST == 'genepool':
            acmd = "%s in=%s out=%s samplerate=%s qin=33 qout=33 overwrite > %s" % (RQCCommands.BBTOOLS_REFORMAT_CMD, fastq_in, subsample_fastq, sample_rate, err_log)
        elif HOST in ['denovo', 'cori']:
            ## TODO - what if the in/out dirs are not auto mounted??
            acmd = 'shifter --image=%s %s in=%s out=%s samplerate=%s qin=33 qout=33 overwrite > %s' % (DOCKER_BBTOOLS_IMG, DOCKER_BBTOOLS_REFORMAT, fastq_in, subsample_fastq, sample_rate, err_log)

        if acmd:
            log_and_print("  - RUN cmd: %s" % acmd)

            std_out, std_err, exit_code = run_command(acmd, True)
            post_mortem_cmd(acmd, exit_code, std_out, std_err, LOG)

            if exit_code == 0:
                # check that we created a subsampled fastq
                subsample_size = 0

                if os.path.isfile(subsample_fastq):
                    # not sure what the limit should be
                    subsample_size = os.path.getsize(subsample_fastq)
                    log_and_print("  - subsample fastq file size: %s" % human_size(subsample_size))

                    # get number of reads in subsample fastq
                    sample_read_count = get_read_count_fastq(subsample_fastq, LOG)
                else:
                    err = 'subsampled fastq does not exist'

                if subsample_size < 100:
                    err = 'subsampled fastq is too small [subsample size=%d]' % subsample_size
                else:
                    # check if paired fastq is still paired correctly
                    if paired == True:
                        if do_pair_check(oput_path, subsample_fastq, "subsample") == 0:
                            err = 'ubsampled fastq is not paired correctly'
            else:
                err = 'subsample failed'
                subsample_fastq = None
        else:
            err = 'Unsupported host : %s' % HOST

        # record actual subsample rate used
        if err == None:
            add_stats("raw read count", raw_reads)
            log_and_print("  - raw read count : %d" % raw_reads)

            add_stats("subsample read count", sample_read_count)
            log_and_print("  - subsample read count : %d" % sample_read_count)

            # actual subsample rate
            sample_rate = sample_read_count / float(raw_reads)
            log_and_print("  - real subsample rate = %d/%d = %s" % (sample_read_count, raw_reads, "{:.2%}".format(sample_rate)))

            add_stats("subsample rate", sample_rate)

            if paired == True:
                add_stats("paired", 1)
            else:
                add_stats("paired", 0)
        if not err:
            log_and_print('subsample completed')
            checkpoint(SAMPLE_END, last_step)
        else:
            log_and_print(err, True)
    else:
        log_and_print('  - no need to perform this step')

    return err, subsample_fastq, sample_read_count




'''
Check if paired ended fastq is still interleaved (read1,2,1,2,1,2,...,1,2) correctly

@param output_path: path to write all output for the pipeline
@param fastq: /path/to/file.fastq to check for interleaving
@param fastq_type: step which we are checking - for logging purposes

@return 0 (not paired correctly) or 1 (looks good)

'''
def do_pair_check(oput_path, fastq_in, fastq_type="raw"):
    'check pairness'
    paired_test_flag = 0 # 1 = true if interleaved correctly

    paired_test_log = os.path.join(oput_path, "%s-paired.log" % (fastq_type))
    PURGE_ITEMS.append(paired_test_log)

    # reformat.sh in=7377.1.69448.fastq.gz-trim.fastq qin=33 out=null int vint
    acmd = None
    if HOST == 'genepool':
        acmd = "%s in=%s qin=33 out=null int vint > %s" % (RQCCommands.BBTOOLS_REFORMAT_CMD, fastq_in, paired_test_log)
    elif HOST in ['denovo', 'cori']:
        ## TODO - what if the in/out dirs are not auto mounted??
        acmd = "shifter --image=%s %s in=%s qin=33 out=null int vint > %s 2>&1" % (DOCKER_BBTOOLS_IMG, DOCKER_BBTOOLS_REFORMAT, fastq_in, paired_test_log)

    if acmd:
        log_and_print("  - do pair check RUN cmd: %s" % acmd)

        std_out, std_err, exit_code = run_command(acmd, True)
        post_mortem_cmd(acmd, exit_code, std_out, std_err, LOG)

        if exit_code == 0:
            # writes to std_err ...
            if os.path.isfile(paired_test_log):
                fileh = open(paired_test_log, "r")
                for line in fileh:

                    if line.strip() == "Names appear to be correctly paired.":
                        paired_test_flag = 1 # looks good!

                fileh.close()

    return paired_test_flag

'''
Run bbsplit.sh to index ref genomes
@param status: the step of the pipeline when it is started, from status.log
@param output_path: path to write all output
@param wkdir: the index working directory
@param rlist: list of ref genomes in form of [(TAG, FILE_PATH),]
@param atype: the index type (genome or transcriptome)

'''

def do_indexing(last_step, oput_path, wkdir, rlist, atype):
    'perform bbsplit indexing on the given reference list (rlist)'
    start = T_INDEXING_START
    end = T_INDEXING_END
    if atype.lower().startswith('genome'):
        start = G_INDEXING_START
        end = G_INDEXING_END

    log_and_print(start)
    checkpoint(start, last_step)

    PURGE_ITEMS.append(wkdir)
    err = None
    if STEP_ORDER[last_step] <= STEP_ORDER[start]:
        # save current dir
        cdir = os.getcwd()

        if os.path.isdir(wkdir):
            log_and_print("  - deleting existing dir %s" % wkdir)
            if not rm_cmd(wkdir):
                log_and_print("  - failed to delete the existing dir!", True)
        log_and_print("  - create working dir of %s" % wkdir)
        os.makedirs(wkdir)

        # move into working dir
        log_and_print('  - move into %s' % wkdir)
        os.chdir(wkdir)

        # do indexing
        opt = ''
        for ref in rlist:   #ref: full path of a ref genome
            log_and_print('  -- ref file=%s; tag=%s' % (ref[1], ref[0]))
            if opt:
                opt += ' '
            opt += 'ref_%s=%s' % (ref[0], ref[1])

        acmd = None
        if HOST == 'genepool':
            acmd = 'module load bbtools; bbsplit.sh rebuild=t %s' % opt
        elif HOST in ['denovo', 'cori']:
            ## TODO - what if the in/out dirs are not auto mounted??
            tmpdir = wkdir
            if tmpdir.startswith('/chos'):  # remove /chos from leading path
                tmpdir = wkdir[5:]

            acmd = 'shifter --image=%s %s rebuild=t %s path=%s' % (DOCKER_BBTOOLS_IMG, DOCKER_BBTOOLS_BBSPLIT, opt, tmpdir)

        if acmd:
            log_and_print("  - RUN cmd: %s" % acmd)

            std_out, std_err, exit_code = run_command(acmd, True)
            post_mortem_cmd(acmd, exit_code, std_out, std_err, LOG)

            # move back to where we were
            os.chdir(cdir)

            if exit_code > 0:
                log_and_print('  - indexing failed')
                err = 'indexing failed'
        else:
            err = 'Unsupported host : %s' % HOST

        # move back to parent dir
        log_and_print('  - move back to %s' % oput_path)
        os.chdir(oput_path)

        if not err:
            log_and_print('indexing completed')
            checkpoint(end, last_step)
        else:
            log_and_print(err, True)

    else:
        log_and_print('  - no need to perform this step')

    return err

'''
Alignment Mapping to multip genome refs
- the ref dir generated by bbsplit.sh has to be created by do_indexing step
-

@param status: the current starting step of the pipeline
@param output_path: path to write all output for the pipeline
@param fastq: input for the mapping (ie, subsampl of RQC filtered fastq)
@param fastq_name: name of the fastq
@param ref_fasta: reference fasta
@param ref_type: "transcriptome" or "genome"

@return status: new status

'''
def do_multi_mapping(last_step, oput_path, wkdir, fq_in, atype):
    'perform bbsplit mapping'
    start = T_MAPPING_START
    end = T_MAPPING_END
    atype = atype.lower()
    stats = 'map_stats.txt'
    unmap = 'unmapped.fq'

    for k in os.environ:
        if k.lower().find('slurm'):
            print('%s : %s' % (k, str(os.environ[k])))

    # cmd default to genepool
    acmd = 'module load bbtools; bbsplit.sh in=%s refstats=%s outu=%s minhits=1 strictmaxindel=4 minid=0.9' % (fq_in, stats, unmap)
    if HOST in ['denovo', 'cori']:
        ## TODO - what if the in/out dirs are not auto mounted??
        acmd = 'shifter --image=%s %s in=%s refstats=%s outu=%s minhits=1 strictmaxindel=4 minid=0.9' % (DOCKER_BBTOOLS_IMG, DOCKER_BBTOOLS_BBSPLIT, fq_in, stats, unmap)

    if atype.startswith('genome'):
        start = G_MAPPING_START
        end = G_MAPPING_END
        acmd = 'module load bbtools; bbsplit.sh in=%s refstats=%s outu=%s minhits=1' % (fq_in, stats, unmap)
        if 'SLURM_JOB_ID' in os.environ:    # run docker image
        ## TODO - what if the in/out dirs are not auto mounted??
            acmd = 'shifter --image=%s %s in=%s refstats=%s outu=%s minhits=1' % (DOCKER_BBTOOLS_IMG, DOCKER_BBTOOLS_BBSPLIT, fq_in, stats, unmap)

    log_and_print(start)
    checkpoint(start, last_step)

    PURGE_ITEMS.append(stats)
    PURGE_ITEMS.append(unmap)

    err = None

    if STEP_ORDER[last_step] <= STEP_ORDER[start]:

        # move into working dir
        log_and_print('  - move into %s' % wkdir)
        os.chdir(wkdir)

        log_and_print("  - map file = %s" % fq_in)
        log_and_print("  - RUN cmd: %s" % acmd)

        std_out, std_err, exit_code = run_command(acmd, True)
        post_mortem_cmd(acmd, exit_code, std_out, std_err, LOG)

        # failed :P
        if exit_code > 0:
            err = 'multi mapping failed : %s' % std_err
        else:
            unmapped_read_count = get_read_count_fastq(unmap, LOG)
            if atype.startswith('genome'):
                add_stats("unmapped read count genome", unmapped_read_count)
            else:
                add_stats("unmapped read count transcriptome", unmapped_read_count)

            with open(stats) as filen:
                for line in filen:
                    line = line.strip()
                    if line.startswith('#'):
                        continue
                    tok = line.split()

                    # name and unambiguousReads
                    # name in form of BIONAME_G/T_REFID,
                    # where BIONAME = seq_location_repos.seq_locations.bio_name; or ref file base name of the CML input (-g or -t)
                    #       G/T = Genome ref or Transcriptome Ref, determined by seq_location_repos.seq_locations.comments, or by CML -g|-t
                    #       REFID = seq_location_repos.seq_locations.id if refs are from DB, or 0 if provided by CML -g and/or -t
                    add_stats('%s_unambiguousReads'%tok[0], tok[-2])

        # move back to parent dir
        log_and_print('  - move back to %s' % oput_path)
        os.chdir(oput_path)

        if not err:
            log_and_print(end)
            checkpoint(end, last_step)
        else:
            log_and_print(err, True)

    else:
        log_and_print('  - no need to perform this step')

    return err


'''
Function to return standardized name for: trimmed, adapter trimmed and subsampled fastqs

@param file_type: "trim", "adapter_trim", "subsample"
@param output_path: path to write all output for the pipeline
@param fastq_name: original name of the fastq
@param extra_param: extra data for the fastq name

@return /path/to/fastq-<type>.fastq
'''
def get_modified_fastq_name(file_type, oput_path, fq_name, extra_param=None):
    'construct and return a new file name'
    fastq_file = os.path.join(oput_path, fq_name)

    if file_type == "trim":
        fastq_file = os.path.join(oput_path, "%s-trim.fastq" % (fq_name))

    if file_type == "filter":
        fastq_file = os.path.join(oput_path, "%s-filter.fastq" % (fq_name))

    elif file_type == "adapter_trim":
        fastq_file = os.path.join(oput_path, "%s-adapter.fastq" % (fq_name))

    elif file_type == "subsample":
        fastq_file = os.path.join(oput_path, "%s.subsampled_%s.fastq" % (fq_name, extra_param))

    return fastq_file


def log_and_print(msg, err=False):
    'log the msg to the log file, and also print to std out. if log is an error msg, exit.'

    if not err:
        print(msg)
        LOG.info(msg)
    else:
        msg = 'Error: %s' % msg
        print(msg)
        LOG.error(msg)
        sys.exit(1)

def log_err_and_print(msg):
    'log and print error msg, but do not exit'
    print(msg)
    LOG.error(msg)


def rm_cmd(items):
    'delete the given items [string] from disk'
    acmd = 'rm -rf %s' % items
    log_and_print("  - cmd = %s" % acmd)
    std_out, std_err, exit_code = run_command(acmd, True, LOG)
    post_mortem_cmd(acmd, exit_code, std_out, std_err, LOG)

    if not items or exit_code > 0:
        log_and_print("  - remove failed")
        return False
    else:
        log_and_print("  - remove completed")
        return True

def jat_refs(spid):
    host = 'https://rqc.jgi-psf.org'
    if DEBUG:
        host = 'https://rqc-4.jgi-psf.org'
    api = 'api/seq_jat_import/ref_files_status?spid=%s&multi=1' % spid
    cmd = 'curl "%s/%s"' % (host, api)
    # print(cmd)
    sout, serr, ecode = run_command(cmd, True)
    if ecode == 0:
        sout = sout.strip()
        if sout:
            try:
                jobj = json.loads(sout.strip())
                if 'multi' in jobj:
                    return jobj['multi']
                else:
                    return {'errors' : 'no multi key found in json [%s]' % str(jobj)}
            except Exception as e:
                return {'errors' : '[%s] failed to convert to json : %s' % (sout, e)}
        else:
            return {'errors' : '%s returned empty content' % cmd}
    else:
        return {'errors' : 'CMD %s encountered problem : %s' % (cmd, serr.strip())}

def driver(args, output_path):
    'the driver funtion'
    # variable initialization
    fastq = None # /full/path/to/fastq file (hopefully unzipped, but works with gzipped)
    fastq_name = None # simple fastq name
    purge_flag = True # purge file (false = don't purge)
    read_limit = 0
    last_step = None # pipeline status

    subsample_rate = 0.01  # 1% to 5% subsampling is fine, (maybe 10%?)


    if args.fastq:
        fastq = args.fastq

    ## Validate output dir and fastq
    if not fastq:
        log_and_print("Missing fastq!", True)

    if not os.path.isfile(fastq):
        log_and_print("Cannot find fastq: %s!" % fastq, True)

    if os.path.islink(fastq):
        fastq = os.readlink(fastq)
    else:
        fastq = os.path.abspath(fastq)

    if args.nopurge:
        purge_flag = False

    if args.subsample:
        try:
            subsample_rate = float(args.subsample)
        except:
            subsample_rate = 0.1

    if args.read_limit:
        try:
            read_limit = int(args.read_limit)
        except:
            read_limit = 0


    log_and_print(80 * "~")
    log_and_print("Command : %s" % JOB_CMD)

    log_and_print("")

    log_and_print("Job settings:")
    log_and_print("%25s      %s" % ("fastq", fastq))
    log_and_print("%25s      %s" % ("output_path", output_path))
    log_and_print("%25s      %s" % ("subsample_rate", subsample_rate))
    log_and_print("%25s      %s" % ("read limit", read_limit))
    if args.genome:
        log_and_print("%25s      %s" % ("genome_ref", args.genome))
    if args.tgenome:
        log_and_print("%25s      %s" % ("transcriptome_ref", args.tgenome))
    if args.spid:
        log_and_print("%25s      %s" % ("spid", args.spid))
    log_and_print("%25s      %s" % ("purge_flag", purge_flag))
    log_and_print("")

    g_refs = []
    t_refs = []

    def parse_ref(refstr, tag):
        'helper to return the refs list of (TAG, REF_PATH)'
        refs = []
        #alist = re.split(r'[,;]', args.genome) # input can be XYZ,ABC;LMN for >1 ref genomes
        #No, can't use ';' to separate the genomes in command line!
        alist = refstr.split(',')

        ref_list = []
        if alist:
            # convert links and rel paths into abs paths for all ref files
            for fname in alist:

                if not os.path.isfile(fname):
                    log_and_print('Find non-existing genome ref file %s ' % fname, True)

                fpath = None
                if os.path.islink(fname):
                    fpath = os.readlink(fname)
                else:
                    fpath = os.path.abspath(fname)

                if fpath in ref_list:
                    log_and_print('Find duplicated ref file %s [%s]' % (fname, tag), True)
                else:
                    ref_list.append(fpath)

            for ref in ref_list:
                bname = os.path.basename(ref)
                tagname = '%s_%s_0' % ('.'.join(bname.split('.')[:-1]), tag)    # use the file name as the tag
                refs.append((tagname, ref))
            return refs

    # parse genomes refs in arg into a list
    if args.genome:
        g_refs += parse_ref(args.genome, 'G')

    # parse transcriptome refs in arg into a list
    if args.tgenome:
        t_refs += parse_ref(args.tgenome, 'T')

    fastq_name = os.path.basename(fastq)
    ftype = align_lib.get_file_type(fastq_name)
    log_and_print("INPUT SEQ FILE NAME : %s" % fastq_name)
    log_and_print("INPUT SEQ FILE TYPE : %s" % ftype)

    def reset_refs(refs):
        'helper: parse the mixed ref list and return separate list'
        grefs = []
        trefs = []
        for item in refs:
            if item[0].find('_T_') > -1:
                trefs.append(item)
            elif item[0].find('_G_') > -1:
                grefs.append(item)
        return grefs, trefs

    if args.spid:
        #get ref from db
        refs = align_lib.all_refs_for_spid(args.spid)
        g_refs, t_refs = reset_refs(refs)
    elif not args.genome and not args.genome:
        # try to find ref from db for the given fastq_name
        log_and_print("Try to find reference files in RQC DB for %s " % fastq_name)
        spid = align_lib.spid_from_file_name(fastq_name)
        if spid:
            log_and_print("  - found spid %s " % spid)
            g_refs = t_refs = None

            refs = jat_refs(spid)
            # pprint(refs)
            if 'errors' in refs and refs['errors'].find('not assigned') > -1:
                refs = align_lib.all_refs_for_spid(spid)
                g_refs, t_refs = reset_refs(refs)
            elif 'errors' in refs:
                log_and_print("  - error in finding refs in jamo [%s]" % refs['errors'], True)
            else:
                g_refs, t_refs = reset_refs(refs)

            if not g_refs and not t_refs:
                log_and_print("  - found no refs", True)
        else:
            log_and_print('\n - not found spid for %s' % fastq_name, True)


    log_and_print("run path = %s" % get_run_path())


    if g_refs:
        log_and_print("GENOME REFs (%d) %s" % (len(g_refs), str(g_refs)))
    if t_refs:
        log_and_print("TRANSCRIPTOME REFs (%d) %s" % (len(t_refs), str(t_refs)))
    if not g_refs and not t_refs:
        log_and_print("No genome reference nor transcriptome reference found. Nothing to run againt!", True)


    # move into the working dir
    log_and_print('Move into %s ..' % output_path)
    os.chdir(output_path)

    last_step = get_status(STATUS_LOG, LOG)

    done = False
    cycle = 0 # number of steps ran
    cycle_max = 1
    infastq = fastq

    # set umask
    umask = os.umask(RQCConstants.UMASK)

    if last_step == "complete":
        log_and_print("Status is complete for %s, Do nothing." % fastq_name)
        sys.exit(0)

    load_stats()    # in case of a rerun
    log_and_print("SARTING step @ %s" % last_step)
    while done == False:
        cycle += 1
        log_and_print("[ Attempt %d ]" % cycle)

        if cycle > 1:
            last_step = get_status(STATUS_LOG, LOG)

        if last_step != PIPE_COMPLETE:

            # subsample
            error, infastq, subsample_read_count = do_subsampling(last_step, output_path, infastq, fastq_name, subsample_rate, read_limit)

            if subsample_read_count:
                add_stats('to-map read count', subsample_read_count)    #the reads used for multi-align

            if not error:
                if os.path.isfile(infastq):
                    # indexing
                    if g_refs:
                        log_and_print("DO GENOME MAPPING")
                        wkdir = os.path.join(output_path, 'genome')
                        error = do_indexing(last_step, output_path, wkdir, g_refs, 'genome')

                        if not error:
                            error = do_multi_mapping(last_step, output_path, wkdir, infastq, 'genome')
                            if error:
                                log_and_print("  -%s" % error, True)
                            log_and_print("GENOME MAPPING COMPLETED")
                        else:
                            log_and_print("  -%s" % error, True)

                    if t_refs:
                        log_and_print("DO TRANSCRIPTOME MAPPING")

                        wkdir = os.path.join(output_path, 'transcriptome')

                        error = do_indexing(last_step, output_path, wkdir, t_refs, 'transcriptome')
                        if not error:
                            error = do_multi_mapping(last_step, output_path, wkdir, infastq, 'transcriptome')
                            if error:
                                log_and_print("  -%s" % error)
                            log_and_print("TRANSCRIPTOME MAPPING COMPLETED")

                    if not error:
                        done = True
                        last_step = PIPE_COMPLETE
                        write_stats()   # re-generate the stats file - in case of rerun
                else:
                    log_and_print("subsampling file not exists : %s." % infastq)
            else:
                log_and_print("  -%s" % error)

        # don't cycle more than cycle_max times ...
        if not done and cycle >= cycle_max:
            log_and_print("[ Meet the max number of attempts allowed (%d). Quit ]" % cycle_max)
            done = True

    if last_step == PIPE_COMPLETE:
        if purge_flag == True:

            log_and_print("Purging temp files and dirs")

            rmlist = ''
            for it in PURGE_ITEMS:
                rmlist += '%s ' % it

            if rm_cmd(rmlist):
                log_and_print("Purge completed")
            else:
                log_and_print("Purge cmd failed")

        log_and_print("Pipeline Completed %s: %s" % (__file__, fastq_name))
        checkpoint(PIPE_COMPLETE)

    else:
        log_and_print("Failed %s: %s" % (__file__, fastq_name))

    os.umask(umask)

    sys.exit(0)



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program
'''
required params:
--fastq RQC filtered fastq file
--type :
'''


if __name__ == "__main__":

    NAME = "RQC Multi_Alignment"
    VERSION = "1.0.0"


    # Parse options
    USAGE = "prog [options]\n"
    USAGE += "* RQC Multi_Alignment Pipeline, version %s\n" % (VERSION)

    # JOB_CMD += "/global/homes/b/brycef/git/jgi-rqc/framework/test/pipeline_test.py --fastq %s --output-path %s"
    JOB_CMD = ' '.join(sys.argv)
    PARSER = ArgumentParser(usage=USAGE)

    PARSER.add_argument("-f", "--fastq", dest="fastq", type=str, help="RQC filtered fastq file (full path to fastq)", required=True)
    PARSER.add_argument("-o", "--output-path", dest="output_path", help="Output path to write to, uses pwd if not set")
    PARSER.add_argument("-s", "--subsample-rate", dest="subsample", help="Subsampling rate (default = 0.01 = 1%%)", type=float)
    PARSER.add_argument("-c", "--read-limit", dest="read_limit", help="Subsampling to the read count.", type=int)
    PARSER.add_argument("-g", "--genome-ref", dest="genome", help="genome reference fasta files,  [,] separated, no space in string is allowed")
    PARSER.add_argument("-t", "--tgenome-ref", dest="tgenome", help="transcriptome reference fasta files,  [,] separated, no space in string is allowed")
    PARSER.add_argument("-i", "--spid", dest="spid", help="seq proj ids")
    PARSER.add_argument("-n", "--no-purge", dest="nopurge", default=False, action="store_true", help="do not remove intermediate files")
    PARSER.add_argument("-v", "--version", action="version", version=VERSION)

    ARGS = PARSER.parse_args()
    # use current directory if no output path
    if ARGS.output_path:
        OUTPUT_PATH = ARGS.output_path
    else:
        OUTPUT_PATH = os.getcwd()

    OUTPUT_PATH = os.path.abspath(OUTPUT_PATH)

    # create output_directory if it doesn't exist
    if not os.path.isdir(OUTPUT_PATH):
        #log_and_print("Cannot find %s, creating as new", OUTPUT_PATH)
        os.makedirs(OUTPUT_PATH)

    # create a name by stripping .fastq from the fastq name
    RQC_STATS_LOG = os.path.join(OUTPUT_PATH, "rqc-stats.txt")

    # initialize my logger
    LOG_FILE = os.path.join(OUTPUT_PATH, "rqc_multi_alignment.log")
    LOG = get_logger("multi_align", LOG_FILE, "INFO")

    driver(ARGS, OUTPUT_PATH)
