#!/bin/bash -l

#set -e

# Alignment pipeline tests

folder="/global/projectb/sandbox/rqc/analysis/qc_user/alignment"
DATE=`date +"%Y%m%d"`


#dev for Bryce
cmd="/global/homes/b/brycef/git/jgi-rqc-pipeline/alignment/alignment.py"


# AHGPS (DNA) - 8990.8.117456.ACAAAC.fastq.gz 70.3% trans, 24.23% genome
qsub -b yes -j yes -m as -now no -w e -N a-AHGPS -l h_rt=43195 -P gentech-rqc.p -o $folder/align-AHGPS.log "$cmd -o $folder/AHGPS-$DATE -f $folder/AHGPS.fastq.gz --read-count 3775018 --transcriptome-ref /global/dna/shared/rqc/sequence_repository/Oryza_sativa/Oryza_sativa.reference_transcriptome_draft.fasta --genome-ref /global/dna/shared/rqc/sequence_repository/Oryza_sativa/Oryza_sativa.reference_genome_draft.fasta --prod-type DNA"

# AAZPS 8965.1.117362.GTGAAA.fastq.gz - 99.82% trans, 87.84% genome (RNA)
qsub -b yes -j yes -m as -now no -w e -N a-AAZPS -l h_rt=43195 -P gentech-rqc.p -o $folder/align-AAZPS.log "$cmd -o $folder/AAZPS-$DATE -f $folder/AAZPS.fastq.gz --read-count 62246522 --transcriptome-ref /global/dna/shared/rqc/sequence_repository/Neurospora_crassa_OR74A/Neurospora_crassa_OR74A.reference_transcriptome_draft.fasta --genome-ref /global/dna/shared/rqc/sequence_repository/Neurospora_crassa_OR74A/Neurospora_crassa_OR74A.reference_genome_draft.fasta --prod-type RNA"

# ANUPU - CLRS
qsub -b yes -j yes -m as -now no -w e -N a-ANUPU -l h_rt=43195 -P gentech-rqc.p -o $folder/align-ANUPU.log "$cmd -o $folder/ANUPU-$DATE -f $folder/ANUPU.fastq.gz --read-count 231420162 --transcriptome-ref /global/dna/shared/rqc/sequence_repository/Oryza_sativa/Oryza_sativa.reference_transcriptome_draft.fasta --genome-ref /global/dna/shared/rqc/sequence_repository/Oryza_sativa/Oryza_sativa.reference_genome_draft.fasta --prod-type CLRS"

# ~/git/jgi-rqc/rqc_system/test/compare.py -p align -rf /global/projectb/sandbox/rqc/analysis/qc_user/alignment/AAZPS-20150806,/global/projectb/sandbox/rqc/analysis/qc_user/alignment/AAZPS-20150702
