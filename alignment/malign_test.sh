#!/bin/bash -l

#set -e

# Long mate pair testing (CLRS)

folder="/global/projectb/sandbox/rqc/analysis/qc_user/malign"
DATE=`date +"%Y%m%d%H%M"`

#test data 
fa="/global/projectb/sandbox/rqc/syao/pipelineTests/jigsaw/data/6740.1.52790.GCACTT.iso.10M.fastq"

refgA="/global/dna/shared/rqc/sequence_repository/Zea_mays/Zea_mays.reference_genome_draft.1063885.fasta"
refgB="/global/dna/shared/rqc/sequence_repository/Panicum_virgatum/Panicum_virgatum.reference_genome_draft.1072312.fasta"
reftA="/global/dna/shared/rqc/sequence_repository/Panicum_virgatum/Panicum_virgatum.reference_transcriptome_draft.1072312.fasta"
reftB="/global/dna/shared/rqc/sequence_repository/Populus_trichocarpa/Populus_trichocarpa.reference_transcriptome_draft.1047026.fasta"

#dev for Shijie 
cmd="/global/homes/s/syao/src/jgi-rqc-pipeline/alignment/multi_alignment.py"




# use SPID of 1062887 as ref 
qsub -b yes -j yes -m as -now no -w e -N lmp-spid -l exclusive.c -P gentech-rqc.p -js 401 -o $folder/lmp-SPID1062887.log "$cmd -f $fa -i 1062887 -o $folder/SPID1062887-malign-$DATE"
 

# use 2g and 2t 
qsub -b yes -j yes -m as -now no -w e -N lmp-2g2t -l exclusive.c -P gentech-rqc.p -js 401 -o $folder/lmp-2g2t.log "$cmd -f $fa -g $refgA,$refgB -t $reftA,$reftB -o $folder/r2g2t-malign-$DATE"


