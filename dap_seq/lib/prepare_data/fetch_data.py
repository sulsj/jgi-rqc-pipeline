#! /usr/bin/python

# Written by Denis Tolkunov

import pandas as pd
import os
import subprocess
import argparse

parser = argparse.ArgumentParser("Fetch fastq files")
parser.add_argument('--csv', required=True, help='File with input data')
parser.add_argument('--dir', required=True, help='Analysis directory')
parser.add_argument('--flt', required=True, help='Fetch filtered data (y/n)')
args = parser.parse_args()
input_data = args.csv
analysis_dir = args.dir
flt = args.flt

# Prepare directories
if not os.path.exists(analysis_dir):
    os.makedirs(analysis_dir)
fastq_dir = analysis_dir + "/fastq"
if not os.path.exists(fastq_dir):
    os.makedirs(fastq_dir)
else:
    # Empty the directory before linking the new files
    map(os.unlink, [os.path.join(fastq_dir, f) for f in os.listdir(fastq_dir)])

df = pd.read_csv(input_data,sep="\t")
df['file_fastq'] = "N"

# Link the data
os.chdir(fastq_dir)

for i in range(df.shape[0]):
    lib = df.library[i]
    if flt == "y":
        params = ["jamo", "link", "filtered", "library", lib]
    else:
        params = ["jamo", "link", "library", lib]

    p = subprocess.Popen(params, stdout=subprocess.PIPE)
    # Extracting the file name here (need to attach the lib name)
    info = p.communicate()[0]
    assert p.returncode == 0
    
    ## for the case when jamo returns more than one sequnits (ex ATAC-SEQ)
    ## take the lastest one
    info = info.strip().split('\n')[-1]    
    info1 = info.strip().split()[1]
    fqname = lib + "." + info1.split('/')[-1]
    df.set_value(i, "file_fastq", fqname)
    print "Fetching: ", fqname

df.to_csv(analysis_dir + "/data.csv", sep="\t", index=False)

