#!/bin/bash -l

# PATH2FASTQC, CSV_IN, DIR_FQ
# Creates fastqc directory with results of the analysis

CSV_OUT=${CSV_IN%.*}.out.csv
DIR_FQC=${DIR_FQ}"fastqc"
mkdir -p $DIR_FQC
# 
cd $DIR_FQ
# Skip the header
tail -n +2 $CSV_OUT | while read SAMP LIB RUN FQ
do
	echo "Running FastQC on "$SAMP
	${PATH2FASTQC}/fastqc --outdir $DIR_FQC --extract --nogroup $FQ
	echo "done!"
	echo
done
