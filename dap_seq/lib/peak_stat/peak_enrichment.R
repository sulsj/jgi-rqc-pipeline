
# --> Estimate peak enrichment in TSS+-delta, TTS+-delta, Gene
#
# --> INPUT: Sname, File_summits, File_gff, L_genome, delta 
# --> Sname - sample name
# --> File_summits - MACS2 output

#source("./TSS.distr.R")
#source("./TTS.distr.R")
#source("./Gene.distr.R")

args <- commandArgs(trailingOnly = TRUE)
Sname = args[1]
File_summits = args[2]
File_gff = args[3]
L_genome = as.numeric(args[4])
delta = as.numeric(args[5])
File_narrowPeaks = args[6]
File_broadPeaks = args[7]

srcdir = paste(args[8], "/TSS.distr.R", sep="")
source(srcdir)
srcdir = paste(args[8], "/TTS.distr.R", sep="")
source(srcdir)
srcdir = paste(args[8], "/Gene.distr.R", sep="")
source(srcdir)

#
AA = read.table(File_gff, sep="\t", stringsAsFactors=FALSE, header=FALSE)
idx = which(AA$V3=="gene")
M_genes = data.frame(Chr=AA$V1[idx], Pos.start=AA$V4[idx], Pos.end=AA$V5[idx], Strand=AA$V7[idx])

#
L_tss = (2*delta+1)*dim(M_genes)[1]		# total size of tss region
L_tts = L_tss							# total size of tts region
L_gene = (M_genes$Pos.end-M_genes$Pos.start+1-2*delta)
L_gene = sum(L_gene[L_gene>0])			# total gene body size (delta caps removed)


A = read.table(File_summits, sep="\t", stringsAsFactors=FALSE, header=FALSE)
M_targ = data.frame(Chr=A$V1, Pos.start=A$V2, Pos.end=A$V3, Strand="*")
N_total = dim(M_targ)[1]	# number of peaks

#distr_tss = TSS.distr(M_genes, M_targ,delta)
distr_tss = TSS.distr(M_genes, M_targ,delta, args[8])
N_tss = length(distr_tss)
N_tss_expected = N_total*L_tss/L_genome
FE_tss = N_tss/N_tss_expected		# Fold enrichment (TSS)

#distr_tts = TTS.distr(M_genes, M_targ, delta)
distr_tts = TTS.distr(M_genes, M_targ,delta, args[8])
N_tts = length(distr_tts)
N_tts_expected = N_total*L_tts/L_genome
FE_tts = N_tts/N_tts_expected		# Fold enrichment (TTS)

#distr_genes = Gene.distr(M_genes, M_targ, delta)
distr_genes = Gene.distr(M_genes, M_targ, delta, args[8])
N_genes = length(distr_genes)
N_genes_expected = N_total*L_gene/L_genome
FE_genes = N_genes/N_genes_expected	# Fold enrichment (Gene)

# Number of broad peaks
T_broad = read.table(File_broadPeaks, sep="\t", stringsAsFactors=FALSE, header=FALSE)
N_broad = dim(T_broad)[1]
T_narrow = read.table(File_narrowPeaks, sep="\t", stringsAsFactors=FALSE, header=FALSE)
N_narrow = dim(T_narrow)[1]

# Output
cat(Sname, "\t", N_narrow, "\t", N_broad, "\t", FE_tss, "\t", FE_tts, "\t", FE_genes, "\n")

