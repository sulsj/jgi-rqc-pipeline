#! /usr/bin/python

# Written by Denis Tolkunov

import pandas as pd
import os
import subprocess
import argparse
import sys

SRCDIR = os.path.dirname(__file__) ## sulsj

# os.chdir("./peak_stat")
os.chdir(SRCDIR) ## need it?

parser = argparse.ArgumentParser("Run genome alignment")
parser.add_argument('--csv', required=True, help='File with input data')
parser.add_argument('--dir', required=True, help='Analysis directory')
parser.add_argument('--gsize', required=True, help='Genome size')
parser.add_argument('--gff', required=True, help='Gene annotations')
args = parser.parse_args()

input_data = args.csv
analysis_dir = args.dir
genome_size = args.gsize
gff_file = args.gff

# Prepare directories
peaks_dir = analysis_dir + "/peaks"
if not os.path.exists(peaks_dir):
    print "The peaks directory does not exist !"
    sys.exit()

df = pd.read_csv(analysis_dir + "/data.csv",sep="\t")
df_chipseq = df[df.paired_control_lib!="N"]
df_chipseq = df_chipseq.reset_index(drop=True)

df['num_narrow_peaks'] = 'N'
df['num_broad_peaks'] = 'N'
df['fe_tss'] = 'N'
df['fe_tts'] = 'N'
df['fe_genes'] = 'N'
df['number_in_peaks'] = 'N'
df['fraction_in_peaks'] ='N'

# Peak statistics
for i in range(df_chipseq.shape[0]):
    lib_chip = df_chipseq.library[i]
    lib_control = df_chipseq.paired_control_lib[i]
    sname = lib_chip + ".vs." + lib_control
    peaks_dir_smpl = peaks_dir + "/" + sname
    if not os.path.exists(peaks_dir_smpl):
        print "I cannot find ", peaks_dir_smpl
        sys.exit()
    summits_file = peaks_dir_smpl + "/" + sname + "_summits.bed"
    peaks_narrow_file = peaks_dir_smpl + "/" + sname + "_peaks.narrowPeak"
    peaks_broad_file = peaks_dir_smpl + "/" + sname + "_peaks.broadPeak"
    delta = 500
    
    rscriptCmd = "Rscript"
    if os.environ['NERSC_HOST'] in ("denovo", "cori"):
        rscriptCmd = "shifter --image=sulsj/rocker:latest Rscript"

    cmd = " ".join([rscriptCmd, "--vanilla", "peak_enrichment.R", sname, summits_file, gff_file, str(genome_size), str(delta), peaks_narrow_file, peaks_broad_file])
    print cmd    
    # p1 = subprocess.Popen(["Rscript", "--vanilla", "peak_enrichment.R", sname, summits_file, gff_file, str(genome_size), str(delta), peaks_narrow_file, peaks_broad_file], stdout=subprocess.PIPE)
    p1 = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    myline = p1.communicate()[0]
    assert p1.returncode == 0
    result = myline.strip().split()

    # record to the data frame
    j = df[df.library==lib_chip].index.tolist()
    df['num_narrow_peaks'][j] = result[1]
    df['num_broad_peaks'][j] = result[2]
    df['fe_tss'][j] = result[3]
    df['fe_tts'][j] = result[4]
    df['fe_genes'][j] = result[5]

    # generate plots with distributions
    chip_name = df.sample_name[j].max()
    ## sulsj
    cmd = " ".join([rscriptCmd, "--vanilla", os.path.join(SRCDIR, "plot_distributions.R"), sname, summits_file, gff_file, str(delta), peaks_dir_smpl, chip_name])
    print cmd
    # subprocess.call([rscriptCmd, "--vanilla", os.path.join(SRCDIR, "plot_distributions.R"), sname, summits_file, gff_file, str(delta), peaks_dir_smpl, chip_name])
    subprocess.call(cmd)

    # Estimate fraction of reads in peak regions
    print "Estimating number of reads in peaks ...", lib_chip
    bam_file = analysis_dir + "/bam/" + lib_chip + "/" + df.file_bam[j].max()
    ## sulsj
    p2 = subprocess.Popen([os.path.join(SRCDIR, "num_reads_in_peaks.sh"), bam_file, peaks_narrow_file], stdout=subprocess.PIPE)
    cnt = p2.communicate()[0]
    assert p2.returncode == 0
    df['number_in_peaks'][j] = cnt.strip()
    df['fraction_in_peaks'][j] = float(cnt.strip()) / int(df['reads_mapped'][j])

df.to_csv(analysis_dir + "/data.csv", sep="\t",index=False)

