# --> Generate distribution of targets in the specified intervals 
# --> [gene_start+delta, gene_end-delta]
# --> !!! THE INTERVALS ARE DIFFERENT FOR ALL THE GENES !!!
# --> Genes.distr(M_genes,M_targ,delta)
# --> M_genes=df(Chr,Pos.start.Pos.end,Strand) 		Genes 
# --> M_targ=df(Chr,Pos.start.Pos.end,Strand)    	For example modifications stc.

# --> D.Tolkunov

Gene.distr <- function(M_genes,M_targ,delta, src) {

	#source("./intervals.overlap.R")
    #source("/global/homes/s/sulsj/work/bitbucket-repo/jgi-rqc-pipeline/dap_seq/lib/peak_stat/intervals.overlap.R")
    source(paste(src, "/intervals.overlap.R", sep=""))

	# --> Generate delta-intervals around TSS's
	idxp = which(M_genes$Strand=="+")
	M_range_p = data.frame(	Chr=M_genes$Chr[idxp],
							Pos.start = M_genes$Pos.start[idxp]+delta,
							Pos.end = M_genes$Pos.end[idxp]-delta,
							Strand = M_genes$Strand[idxp],
							Gene_start=M_genes$Pos.start[idxp]+delta)
							
	idxn = which(M_genes$Strand=="-")
	M_range_n = data.frame(	Chr=M_genes$Chr[idxn],
							Pos.start=M_genes$Pos.start[idxn]+delta,
							Pos.end=M_genes$Pos.end[idxn]-delta,
							Strand=M_genes$Strand[idxn],
							Gene_start=M_genes$Pos.start[idxn]+delta)
	  
	M_range = rbind(M_range_p,M_range_n) 
	# Handle the negative coordinates
	M_range = M_range[which(M_range$Pos.start>0 & M_range$Pos.end>0 & M_range$Pos.end>M_range$Pos.start),]
	# --> Find overlap and distribution in TSS coordinates

	G = intervals.overlap(M_range,M_targ)
	Pos = G$Pos.start				# --> Absolute positions of modifications
	Gene_start = M_range$Gene_start[G$idx1] 		# --> Corresponding positions
	idxp = which(G$Strand=="+")
	idxn = which(G$Strand=="-")
	distr_p = Pos[idxp]-Gene_start[idxp]	# --> Positions relatively to TSS 
	distr_n = Pos[idxn]-Gene_start[idxn]
	distr = c(distr_p,-distr_n)     

	return(distr)
}





