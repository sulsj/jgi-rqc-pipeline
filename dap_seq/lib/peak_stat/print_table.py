#! /usr/bin/python

# Written by Denis Tolkunov

import pandas as pd
import argparse

parser = argparse.ArgumentParser("Run genome alignment")
parser.add_argument('--dir', required=True, help='Analysis directory')
args = parser.parse_args()
analysis_dir = args.dir
df = pd.read_csv(analysis_dir+"/data.csv",sep="\t")

# Ignore these for now

print "Generating summary table ..."
df_table = pd.concat([ 	df.library, \
						df.reads_raw, \
						df.reads_mapped, \
						df.reads_mapped_perc/100, \
						df.reads_mtdna, \
						df.reads_chlor, \
						df.reads_mapped_unq, \
						df.reads_mapped_unq_perc/100, \
						df.reads_mapped_unq_dedup, \
						df.reads_mapped_unq_dedup_perc/100, \
						df.num_narrow_peaks, \
						df.fe_tss, \
						df.fe_tts, \
						df.fe_genes, \
						df.fraction_in_peaks \
					],axis=1)

df_table.to_csv(analysis_dir+"/table_stats.csv",sep="\t",index=False)



# #! /usr/bin/python
# 
# # Written by Denis Tolkunov
# 
# import pandas as pd
# import argparse
# 
# parser = argparse.ArgumentParser("Run genome alignment")
# parser.add_argument('--dir', required=True, help='Analysis directory')
# args = parser.parse_args()
# analysis_dir = args.dir
# df = pd.read_csv(analysis_dir + "/data.csv", sep="\t")
# 
# # Ignore these for now
# df['nsc'] = 'N'
# df['rsc'] = 'N'
# 
# print "Generating summary table ..."
# df_table = pd.concat([  df.library, \
#                         df.reads_raw, \
#                         df.reads_mapped, \
#                         df.reads_mapped_perc/100, \
#                         df.reads_mtdna, \
#                         df.reads_chlor, \
#                         df.reads_mapped_unq, \
#                         df.reads_mapped_unq_perc/100, \
#                         df.reads_mapped_unq_dedup, \
#                         df.reads_mapped_unq_dedup_perc/100, \
#                         df.nsc, \
#                         df.rsc, \
#                         df.num_narrow_peaks, \
#                         df.num_broad_peaks, \
#                         df.fe_tss, \
#                         df.fe_tts, \
#                         df.fe_genes, \
#                         df.fraction_in_peaks \
#                     ],axis=1)
# 
# df_table.to_csv(analysis_dir + "/table_stats.csv", sep="\t", index=False)
# 
# 

