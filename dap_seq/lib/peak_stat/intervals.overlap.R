# --> intervals_overlap(M1,M2)
# --> M1 = df(Chr,Pos.start,Pos.end,Strand>, to ignore strand use "*"
# --> OUTPUT: 	idx1 - indexes of M1 that overlapped
# --> 			idx2 - indexes of M2 that overlapped

# --> D.Tolkunov

intervals.overlap <- function(M1,M2) {

	require(GenomicRanges)

# --> Debugging
#M1 = t(matrix(c("chr1",1,104,"+",
#				"chr1",210,301,"-",
#				"chr2",38,123,"-"),ncol=3))

#M2 = t(matrix(c("chr1",50,150,"+",
#				"chr2",70,305,"-",
#				"chr1",34,113,"-"),ncol=3))

	m1_chr = M1$Chr
	m1_start = as.numeric(M1$Pos.start)
	m1_end = as.numeric(M1$Pos.end)
	m1_str = M1$Strand

	m2_chr = M2$Chr
	m2_start = as.numeric(M2$Pos.start)
	m2_end = as.numeric(M2$Pos.end)
	m2_str = M2$Strand

	ir1 = IRanges(start = m1_start, end = m1_end)
	gr1 = GRanges(seqnames = m1_chr, ranges=ir1, strand=m1_str)
	ir2 = IRanges(start = m2_start, end = m2_end)
	gr2 = GRanges(seqnames = m2_chr, ranges=ir2, strand=m2_str)
	ov = suppressWarnings(findOverlaps(gr1,gr2,ignore.strand = FALSE))

	ov_starts = cbind(m1_start[queryHits(ov)],m2_start[subjectHits(ov)])
	ov_start = apply(ov_starts,1,max)
	ov_ends = cbind(m1_end[queryHits(ov)],m2_end[subjectHits(ov)])
	ov_end = apply(ov_ends,1,min)

#	M_ov = cbind(m1_chr[ov@queryHits],ov_start,ov_end,m1_str[ov@queryHits])
	M = data.frame(Chr=m1_chr[queryHits(ov)],
					Pos.start=ov_start,
					Pos.end=ov_end,
					Strand=m1_str[queryHits(ov)],
					idx1=queryHits(ov),
					idx2=subjectHits(ov))
	return(M)
}