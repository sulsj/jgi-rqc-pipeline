# --> Generate distribution of targets in the specified intervals 
# --> TSS.distr(M_genes,M_targ,delta)
# --> M_genes=df(Chr,Pos.start.Pos.end,Strand) 		Genes 
# --> M_targ=df(Chr,Pos.start.Pos.end,Strand)    	For example modifications stc.

# --> D.Tolkunov

TTS.distr <- function(M_genes,M_targ,delta,src) {

	#source("/global/u1/d/dtolk/Projects/Generic_scripts/intervals.overlap.R")
    #source("./intervals.overlap.R")
    source(paste(src, "/intervals.overlap.R", sep=""))

#	# --------------------------------------------------------------------------
#	FILE_IN_genloc = "/media/dtolk/DATAPART1/Genomes/Chlamy/chlamy_TSS.csv"
#	FILE_IN_targets = "/media/dtolk/DATAPART1/Data/Chlamy_m6A/6mA/targ.m6A.48H.rep12.bed"
#	delta = 1000

#	A = read.table(FILE_IN_genloc, 
#				  sep="\t", 
#				  stringsAsFactors=FALSE,
#				  header=TRUE)
#	M_genes = data.frame(Chr=A$chromosome,Pos.start=A$start,Pos.end=A$end,Strand=A$strand)

#	AA = read.table(FILE_IN_targets, 
#				   sep="\t", 
#				   stringsAsFactors=FALSE,
#				   header=TRUE)

#	M_targ = data.frame(Chr=AA$chromosome,Pos.start=AA$start,Pos.end=AA$end,Strand=rep('*',length(AA$chromosome)))
#	# --------------------------------------------------------------------------

	# --> Generate delta-intervals around TTS's
	idxp = which(M_genes$Strand=="+")
	M_range_p = data.frame(	Chr=M_genes$Chr[idxp],
							Pos.start = M_genes$Pos.end[idxp]-delta,
							Pos.end = M_genes$Pos.end[idxp]+delta,
							Strand = M_genes$Strand[idxp],
							TTS=M_genes$Pos.end[idxp])
							
	idxn = which(M_genes$Strand=="-")
	M_range_n = data.frame(	Chr=M_genes$Chr[idxn],
							Pos.start=M_genes$Pos.start[idxn]-delta,
							Pos.end=M_genes$Pos.start[idxn]+delta,
							Strand=M_genes$Strand[idxn],
							TTS=M_genes$Pos.start[idxn])
	  
	M_range = rbind(M_range_p,M_range_n) 
	 
	# --> Find overlap and distribution in TSS coordinates
	G = intervals.overlap(M_range,M_targ)
	Pos = G$Pos.start				# --> Absolute positions of modifications
	TTS = M_range$TTS[G$idx1] 		# --> Corresponding positions of TSS's
	idxp = which(G$Strand=="+")
	idxn = which(G$Strand=="-")
	distr_p = Pos[idxp]-TTS[idxp]	# --> Positions relatively to TSS 
	distr_n = Pos[idxn]-TTS[idxn]
	distr = c(distr_p,-distr_n)      
	return(distr)
}
