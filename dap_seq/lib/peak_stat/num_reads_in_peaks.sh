#! /bin/bash

# Convert BAM to BigWig format
#
# D.Tolkunov

#INPUT: BAM, REF

#BED="/global/projectb/scratch/dtolk/ChIP_Seq/Bdistachyon.2/peaks/BGHGH.vs.BGHGC/BGHGH.vs.BGHGC_peaks.narrowPeak"
#BED="/global/projectb/scratch/dtolk/ChIP_Seq/Bdistachyon.2/peaks/BGHGH.vs.BGHGC/test.narrowPeak"
#BAM="/global/projectb/scratch/dtolk/ChIP_Seq/Bdistachyon.2/bam/BGHGH/BGHGH.10859.1.182170.ACAGT.srt.unq.md.bam"

BAM=$1
BED=$2
intersectBed -abam $BAM -b $BED -u -bed | wc -l


