#! /usr/bin/python

# Written by Denis Tolkunov

import pandas as pd
import os
import subprocess
import argparse
import sys

SRCDIR = os.path.dirname(__file__) ## sulsj
# os.chdir("./motif_analysis")

parser = argparse.ArgumentParser("Run motif analysis")
parser.add_argument('--csv', required=True, help='File with input data')
parser.add_argument('--dir', required=True, help='Analysis directory')
parser.add_argument('--ref', required=True, help='Reference genome')

args = parser.parse_args()

input_data = args.csv
analysis_dir = args.dir
ref = args.ref

# Prepare directories
bam_dir = analysis_dir+"/bam"
if not os.path.exists(bam_dir):
    print "The analysis directory does not exist !"
    sys.exit()
peaks_dir = analysis_dir+"/peaks"
if not os.path.exists(peaks_dir):
    print "The peaks directory does not exist !"
    sys.exit()

df = pd.read_csv(analysis_dir+"/data.csv",sep="\t")

# Peak calling
for i in range(df.shape[0]):
    lib_chip = df.library[i]
    peaks_dir_smpl = peaks_dir+"/"+lib_chip
    file_peaks = peaks_dir_smpl+"/"+lib_chip+"_peaks.narrowPeak"
    # Extract sequences
    print
    print "Extracting sequences from peaks ..."
    # subprocess.call(["Rscript", "--vanilla", "./extract_seq.R",file_peaks,ref])

    ## sulsj
    rscriptCmd = "Rscript"
    if os.environ['NERSC_HOST'] in ("denovo", "cori"):
        rscriptCmd = "shifter --image=sulsj/rocker:latest Rscript"

    # cmd = " ".join(["module load R; Rscript", "--vanilla", os.path.join(SRCDIR, "extract_seq.R"), file_peaks, ref])
    cmd = " ".join([rscriptCmd, "--vanilla", os.path.join(SRCDIR, "extract_seq.R"), file_peaks, ref])
    print cmd
    p1 = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    stdOut = p1.communicate()[0]
    assert p1.returncode == 0

    # Run MEME
    seqs = file_peaks+".fasta"
    meme_dir = peaks_dir_smpl+"/motif_analysis"
    print
    print "Analyzing motifs ..."
    # subprocess.call(["./run_meme.sh",seqs,meme_dir])

    ## sulsj
    ## rmonti: Don't run this step if the number of lines in "file_peaks" is <2 !
    ## e.g.:
    run_meme = False
    with open(file_peaks) as pfile:
        for i, l in enumerate(pfile):
            if i + 1 > 1:
                run_meme = True
                break
    if run_meme:
        cmd = " ".join([os.path.join(SRCDIR, "run_meme.sh"), seqs, meme_dir])
        p2 = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        stdOut = p2.communicate()[0]
        assert p2.returncode == 0
    else:
        print "Warning: Not able to run MEME because only one peak was found."
    # print stdOuts



