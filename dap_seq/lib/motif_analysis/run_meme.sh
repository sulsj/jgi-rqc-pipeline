#! /bin/bash

# Run motif discovery
# INPUT:
#		Fasta file with sequences extracted from peaks
#		Output directory

SEQ_FA=$1
OUTDIR=$2

meme $SEQ_FA \
		-oc $OUTDIR \
		-dna \
		-time 18000 \
		-maxsize 1000000 \
		-mod zoops \
		-nmotifs 3 \
		-minw 6 \
		-maxw 20 \
		-revcomp \
		-nostatus
		
