#! /usr/bin/python

# Written by Denis Tolkunov

import pandas as pd
import os
import subprocess
import argparse
import sys

parser = argparse.ArgumentParser("Produce read alignment statistics")
parser.add_argument('--csv', required=True, help='File with input data')
parser.add_argument('--dir', required=True, help='Analysis directory')
args = parser.parse_args()
input_data = args.csv
analysis_dir = args.dir

# Prepare directories
fastq_dir = analysis_dir + "/fastq"
bam_dir = analysis_dir + "/bam"
if not os.path.exists(bam_dir):
    print "The analysis directory does not exist !"
    sys.exit()

df = pd.read_csv(analysis_dir + "/data.csv", sep="\t")
df['reads_raw'] = 'N'
df['reads_unmap'] = 'N'
df['reads_mapped'] = 'N'
df['reads_mapped_perc'] = 'N'
df['reads_mapped_unq'] = 'N'
df['reads_mapped_unq_perc'] = 'N'
df['reads_mapped_unq_dedup'] = 'N'
df['reads_mapped_unq_dedup_perc'] = 'N'
df['reads_mtdna'] = 'N'
df['reads_chlor'] = 'N'

for i in range(df.shape[0]):
    lib = df.library[i]
    fq = fastq_dir + "/" + df.file_fastq[i]
    bam = bam_dir + "/" + lib + "/" + df.file_fastq[i][:-8] + "srt.bam"
    bamu = bam_dir + "/" + lib + "/" + df.file_fastq[i][:-8] + "srt.unq.bam"
    bamud = bam_dir + "/" + lib + "/" + df.file_fastq[i][:-8] + "srt.unq.md.bam"
    print "Calculating alignment statistics: ", lib

    # Total number of raw reads (found from FASTQ file)
    p1 = subprocess.Popen(["zcat", fq], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(["wc", "-l"], stdin=p1.stdout, stdout=subprocess.PIPE)
    p1.stdout.close()
    num_raw = p2.communicate()[0]
    assert p2.returncode == 0
    num_raw = int(num_raw.strip()) / 4
    df['reads_raw'][i] = num_raw

    # Number of unmapped reads
    p3 = subprocess.Popen(["samtools", "idxstats", bam], stdout=subprocess.PIPE)
    info = p3.communicate()[0]
    assert p3.returncode == 0
    lastline = info.splitlines()[-1]
    num_unmap = int(lastline.split()[3])
    df['reads_unmap'][i] = num_unmap

    # Number of mitochondria and chloroplast reads
    num_mito = "NA"
    num_chlor = "NA"
    for line in info.splitlines():
        if line.strip().startswith("chlor"):
            num_chlor = int(line.strip().split()[2])
        if line.strip().startswith("mito"):
            num_mito = int(line.strip().split()[2])
        if line.strip().startswith("ChrC"):
            num_chlor = int(line.strip().split()[2])
        if line.strip().startswith("ChrM"):
            num_mito = int(line.strip().split()[2])
    df['reads_mtdna'][i] = num_mito
    df['reads_chlor'][i] = num_chlor

    # Number of mapped reads
    num_map=num_raw-num_unmap
    df['reads_mapped'][i] = num_map
    df['reads_mapped_perc'][i] = 100 * float(num_map) / num_raw

    # Number of unique reads
    p4 = subprocess.Popen(["samtools","view",bamu], stdout=subprocess.PIPE)
    p5 = subprocess.Popen(["wc", "-l"], stdin=p4.stdout, stdout=subprocess.PIPE)
    p4.stdout.close()
    num_uniq = p5.communicate()[0]
    assert p5.returncode == 0
    num_uniq = int(num_uniq.strip())
    df['reads_mapped_unq'][i] = num_uniq
    df['reads_mapped_unq_perc'][i] = 100 * float(num_uniq) / num_map

    # Number of unique deduplicated reads
    p6 = subprocess.Popen(["samtools","view",bamud], stdout=subprocess.PIPE)
    p7 = subprocess.Popen(["wc", "-l"], stdin=p6.stdout, stdout=subprocess.PIPE)
    p6.stdout.close()
    num_uniq_dedup = p7.communicate()[0]
    assert p7.returncode == 0
    num_uniq_dedup = int(num_uniq_dedup.strip())
    df['reads_mapped_unq_dedup'][i] = num_uniq_dedup
    df['reads_mapped_unq_dedup_perc'][i] = 100 * float(num_uniq_dedup) / num_map

df.to_csv(analysis_dir + "/data.csv", sep="\t", index=False)















