#! /usr/bin/perl

# --> Split paired-end single fastq in two files

# D. Tolkunov

use warnings;
use strict;

($#ARGV!=0) and die "Wrong number of arguments \n";
my $fName_IN = $ARGV[0];
#(substr($fName_IN,-6,6) ne ".fastq") and die "Input should be .fastq\n";


my $fName_OUT_R1 = $fName_IN;
$fName_OUT_R1=~s/fastq.gz/R1.fastq/;
my $fName_OUT_R2 = $fName_IN;
$fName_OUT_R2=~s/fastq.gz/R2.fastq/;

open my $IN, "gunzip -c $fName_IN |" or die "Cannot open $fName_IN\n";
open my $OUT1, ">", $fName_OUT_R1 or die "Cannot open $fName_OUT_R1\n";
open my $OUT2, ">", $fName_OUT_R2 or die "Cannot open $fName_OUT_R2\n";
print "Splitting fastq files ...\n";
while (<$IN>) {
	chomp(my $SeqID = $_);
	chomp(my $Seq = <$IN>);
	chomp(my $Plus = <$IN>);
	chomp(my $SeqQ = <$IN>);
	
	my @f = split/\s/,$SeqID;
	if (substr($f[1],0,1)==1) {
		print $OUT1 $SeqID."\n";
		print $OUT1 $Seq."\n";
		print $OUT1 $Plus."\n";
		print $OUT1 $SeqQ."\n";
	} elsif (substr($f[1],0,1)==2) {
		print $OUT2 $SeqID."\n";
		print $OUT2 $Seq."\n";
		print $OUT2 $Plus."\n";
		print $OUT2 $SeqQ."\n";
	} else {
		print "We've got a problem with Seq ID\n";
		last;
	}
}

close $IN;
close $OUT1;
close $OUT2;






























