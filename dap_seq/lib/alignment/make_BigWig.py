#! /usr/bin/python

# Written by Denis Tolkunov

import pandas as pd
import os
import subprocess
import argparse
import sys

SRCDIR = os.path.dirname(__file__) ## sulsj

parser = argparse.ArgumentParser("Run genome alignment")
parser.add_argument('--csv', required=True, help='File with input data')
parser.add_argument('--dir', required=True, help='Analysis directory')
parser.add_argument('--ref', required=True, help='Reference genome')
args = parser.parse_args()

input_data = args.csv
analysis_dir = args.dir
ref = args.ref

# Prepare directories
bam_dir = analysis_dir + "/bam"
if not os.path.exists(bam_dir):
    print "The analysis directory does not exist !"
    sys.exit()

df = pd.read_csv(analysis_dir + "/data.csv", sep="\t")

# Produce coverage profile
for i in range(df.shape[0]):
    lib = df.library[i]
    bam = bam_dir + "/" + lib + "/" + df.file_bam[i]
    # subprocess.call(["./alignment/bam2bw.sh", bam,ref])
    subprocess.call([os.path.join(SRCDIR, "bam2bw.sh"), bam,ref, SRCDIR]) ## sulsj

