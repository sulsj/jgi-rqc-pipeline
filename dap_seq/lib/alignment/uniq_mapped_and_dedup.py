#! /usr/bin/python

# Written by Denis Tolkunov

import pandas as pd
import os
import subprocess
import argparse
import sys

SRCDIR = os.path.dirname(__file__) ## sulsj

parser = argparse.ArgumentParser("Run genome alignment")
parser.add_argument('--csv', required=True, help='File with input data')
parser.add_argument('--dir', required=True, help='Analysis directory')
parser.add_argument('--stringent', required=True, help='Stringent (y/n). Samtools vs picard')
args = parser.parse_args()

input_data = args.csv
analysis_dir = args.dir
stringent = args.stringent

# Prepare directories
bam_dir = analysis_dir + "/bam"
if not os.path.exists(bam_dir):
    print "The analysis directory does not exist !"
    sys.exit()

df = pd.read_csv(analysis_dir + "/data.csv", sep="\t")
df['file_bam'] = "N"

# Deduplication
for i in range(df.shape[0]):
    lib = df.library[i]
    bam = bam_dir + "/" + lib + "/" + df.file_fastq[i][:-8] + "srt.bam"
    bamud = df.file_fastq[i][:-8] + "srt.unq.md.bam"
    df['file_bam'][i] = bamud
    if stringent == "y":
        print "Working on ", lib
        subprocess.call([os.path.join(SRCDIR, "dedup_samtools.sh"), bam]) ## sulsj
    else:
        # Not stringent (picard)
        subprocess.call([os.path.join(SRCDIR, "dedup_picard.sh"), bam]) ## sulsj

df.to_csv(analysis_dir+"/data.csv", sep="\t", index=False)

