#! /bin/bash

PICARD_EXEC="shifter --image=registry.services.nersc.gov/jgi/picard:latest picard"
BAM=$1
BAMU=${BAM/.bam/.unq.bam}
BAMUD=${BAM/.bam/.unq.md.bam}
echo $BAM
echo $BAMU
echo $BAMUD
samtools view -q5 -b $BAM > $BAMU
samtools index $BAMU
METF=${BAM/.bam/.unq.md.metrics}
#picard MarkDuplicates INPUT=$BAMU OUTPUT=$BAMUD METRICS_FILE=$METF REMOVE_DUPLICATES=true
$PICARD_EXEC MarkDuplicates INPUT=$BAMU OUTPUT=$BAMUD METRICS_FILE=$METF REMOVE_DUPLICATES=true
echo "Indexing ..." 
samtools index $BAMUD 


