#! /bin/bash -l

REF=$1
FQ=$2
BAM=$3
#IDX=$4
BWA_EXEC="shifter --image=registry.services.nersc.gov/jgi/bwa:latest bwa"

if [ ! -e ${REF}.bwt ]
then
    echo "Creating BWA index"
    #bwa index -p $IDX $REF || exit $?;
    #bwa index $REF || exit $?;
    $BWA_EXEC index $REF || exit $?;
fi

echo "Ref: "$REF
echo "FQ: "$FQ
echo "BAM: "$BAM

echo "Mapping ..."
#bwa mem -M -t 15 $REF $FQ | samtools view -Sb - > $BAM || exit $?;
$BWA_EXEC mem -M -t 15 $REF $FQ | samtools view -Sb - > $BAM || exit $?;

echo "Sorting ..."
BAMS=${BAM/.bam/.srt.bam}
samtools sort -m 900000000 $BAM -o $BAMS || exit $?;
echo "Indexing ..." 
samtools index $BAMS || exit $?;
echo "done!"

rm $BAM


