#! /bin/bash

# Convert BAM to BigWig format
#
# D.Tolkunov

#INPUT: BAM, REF


BAM=$1
REF=$2
DIR=$3 ## sulsj

REF_IDX=${REF}.fai
REF_CHR_SIZES=${REF}.chr.sizes
WIG=${BAM/.bam/.wig}
BW=${BAM/.bam/.bw}

echo "bam -> wig"
samtools mpileup -BQ0 $BAM | \
perl -pe '($c, $start, undef, $depth) = split;if ($c ne $lastC || $start != $lastStart+1) {print "fixedStep chrom=$c start=$start step=1 span=1\n";}$_ = $depth."\n";($lastC, $lastStart) = ($c, $start);' \
> $WIG

# If missing, generate genome stat file
if [ ! -e $REF_IDX ]
then
	echo "Indexing reference fasta file ..."
	samtools faidx $REF
fi
#
if [ ! -e $REF_CHR_SIZES ]
then
	cut -f1,2 $REF_IDX > $REF_CHR_SIZES
fi
echo "wig -> bw"
#./alignment/wigToBigWig   $WIG $REF_CHR_SIZES $BW
$DIR/wigToBigWig $WIG $REF_CHR_SIZES $BW
rm $WIG
echo "done!"
