#! /bin/bash

BAM=$1
BAMU=${BAM/.bam/.unq.bam}
BAMUD=${BAM/.bam/.unq.md.bam}
echo $BAM
echo $BAMU
echo $BAMUD
samtools view -q5 -b $BAM > $BAMU
samtools index $BAMU
samtools rmdup -S $BAMU $BAMUD
samtools index $BAMUD


