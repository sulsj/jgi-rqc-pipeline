#! /bin/bash -l

REF=$1
FQ_R1=$2
FQ_R2=$3
BAM=$4
#IDX=$5
BWA_EXEC="shifter --image=registry.services.nersc.gov/jgi/bwa:latest bwa"

if [ ! -e ${REF}.bwt ]
then
    echo "Creating BWA index"
    #bwa index -p $IDX $REF || exit $?;
    #bwa index $REF || exit $?;
    $BWA_EXEC index $REF || exit $?;
fi

echo "Ref: "$REF
echo "FQ1: "$FQ_R1
echo "FQ2: "$FQ_R2
echo "BAM: "$BAM

echo "Mapping ..."
#bwa mem -M -t 15 $REF $FQ_R1 $FQ_R2 | samtools view -Sb - > $BAM || exit $?;
$BWA_EXEC mem -M -t 15 $REF $FQ_R1 $FQ_R2 | samtools view -Sb - > $BAM || exit $?;

echo "Sorting ..."
BAMS=${BAM/.bam/.srt.bam}
samtools sort -m 900000000 $BAM -o $BAMS || exit $?;
echo "Indexing ..." 
samtools index $BAMS || exit $?;
echo "done!"

rm $FQ_R1
rm $FQ_R2
rm $BAM
