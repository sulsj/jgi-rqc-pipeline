#! /usr/bin/python

# Written by Denis Tolkunov

import pandas as pd
import os
import subprocess
import argparse
import sys

SRCDIR = os.path.dirname(__file__) ## sulsj

def run_sh_cmd(cmd):
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    stdOut = p.communicate()[0]
    assert p.returncode == 0
    # return stdOut, p.returncode

parser = argparse.ArgumentParser("Run genome alignment")
parser.add_argument('--csv', required=True, help='File with input data')
parser.add_argument('--dir', required=True, help='Analysis directory')
parser.add_argument('--ref', required=True, help='Reference genome')
parser.add_argument('--pe', required=True, help='Paired-end (y/n)')
args = parser.parse_args()

input_data = args.csv
analysis_dir = args.dir
ref = args.ref
pe = args.pe

# Prepare directories
if not os.path.exists(analysis_dir):
    print "The analysis directory does not exist !"
    sys.exit()
bam_dir = analysis_dir + "/bam"
if not os.path.exists(bam_dir):
    os.makedirs(bam_dir)

df = pd.read_csv(analysis_dir + "/data.csv", sep="\t")
fastq_dir = analysis_dir + "/fastq"

# Alignment
for i in range(df.shape[0]):
    lib = df.library[i]
    fq = fastq_dir + "/"+df.file_fastq[i]
    bam_dir_smpl = bam_dir + "/" + lib
    if not os.path.exists(bam_dir_smpl):
        os.makedirs(bam_dir_smpl)
    bam = bam_dir_smpl + "/" + df.file_fastq[i][:-8] + "bam"
    # idx_prefix = bam_dir_smpl + "/" + ref.split('/')[-1]
    if pe == "y":
        print "Working on ",lib
        # subprocess.call(["./alignment/split_fastq.gz.pl",fq]) # Split paired-end fastq file in two
        # subprocess.call([os.path.join(SRCDIR, "split_fastq.gz.pl"), fq])   # Split paired-end fastq file in two
        run_sh_cmd(" ".join([os.path.join(SRCDIR, "split_fastq.gz.pl"), fq]))
        fq_r1 = fq[:-8] + "R1.fastq"
        fq_r2 = fq[:-8] + "R2.fastq"
        # subprocess.call(["./alignment/BWA_PE.sh",ref,fq_r1,fq_r2,bam])
        # subprocess.call([os.path.join(SRCDIR, "BWA_PE.sh"), ref, fq_r1, fq_r2, bam]) ## sulsj
        # run_sh_cmd(" ".join([os.path.join(SRCDIR, "BWA_PE.sh"), ref, fq_r1, fq_r2, bam, idx_prefix]))
        run_sh_cmd(" ".join([os.path.join(SRCDIR, "BWA_PE.sh"), ref, fq_r1, fq_r2, bam]))
    else:
        # subprocess.call(["./alignment/BWA_SE.sh",ref,fq,bam])
        # subprocess.call([os.path.join(SRCDIR, "BWA_SE.sh"), ref, fq, bam]) ## sulsj
        # run_sh_cmd(" ".join([os.path.join(SRCDIR, "BWA_SE.sh"), ref, fq, bam, idx_prefix]))
        run_sh_cmd(" ".join([os.path.join(SRCDIR, "BWA_SE.sh"), ref, fq, bam]))

