#! /usr/bin/python

# Written by Denis Tolkunov

import pandas as pd
import os
import subprocess
import argparse
import sys

SRCDIR = os.path.dirname(__file__) ## sulsj

parser = argparse.ArgumentParser("Run genome alignment")
parser.add_argument('--csv', required=True, help='File with input data')
parser.add_argument('--dir', required=True, help='Analysis directory')
parser.add_argument('--gsize', required=True, help='Genome size')
args = parser.parse_args()

input_data = args.csv
analysis_dir = args.dir
genome_size = args.gsize

# Prepare directories
bam_dir = analysis_dir + "/bam"
if not os.path.exists(bam_dir):
    print "The analysis directory does not exist !"
    sys.exit()
peaks_dir = analysis_dir + "/peaks"
if not os.path.exists(peaks_dir):
    os.makedirs(peaks_dir)

df = pd.read_csv(analysis_dir + "/data.csv", sep="\t")

#df_chipseq = df[df.paired_control_lib!="N"]
#df_chipseq = df_chipseq.reset_index(drop=True)


# Peak calling
for i in range(df.shape[0]):
    lib_chip = df.library[i]
    bam_chip = bam_dir + "/" + lib_chip + "/" + df.file_bam[i]
    sname = lib_chip
    peaks_dir_smpl = peaks_dir + "/" + sname

    print bam_chip
    print

    if not os.path.exists(peaks_dir_smpl):
        os.makedirs(peaks_dir_smpl)
    ## Narrow peaks (without control sample)
    # subprocess.call([os.path.join(SRCDIR, "macs2_nocontrol.sh"), bam_chip, str(genome_size), sname, peaks_dir_smpl]) ## sulsj
    cmd = " ".join([os.path.join(SRCDIR, "macs2_nocontrol.sh"), bam_chip, str(genome_size), sname, peaks_dir_smpl])
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    stdOut = p.communicate()[0]
    assert p.returncode == 0



