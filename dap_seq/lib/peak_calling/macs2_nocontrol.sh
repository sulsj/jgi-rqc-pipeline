#! /bin/bash

# NEED: BAM_chip, GENOME_SIZE, SNAME, OUTDIR
# 
# D Tolkunov, 2016

# sulsj
# 06262018 removed -f BAM
# 06282018 removed nomode, extsize, -f BAM
#          added --cutoff-analysis ==> to generate additional plot
# 07022018 removed --cutoff-analysis; Added back "--nomodel" and "--extsize 250" to macs2

BAM_chip=$1
GENOME_SIZE=$2
SNAME=$3
OUTDIR=$4

echo
echo "BAM ChIP: "$BAM_chip
echo "Genome size: "$GENOME_SIZE
echo "Sample name: "$SNAME
echo "Output directory: "$OUTDIR
echo

#macs2 callpeak \
#		--nomodel \
#		--extsize 250 \
#		--call-summits \
#		-t $BAM_chip \
#		-f BAM \
#		-g $GENOME_SIZE \
#		-n $SNAME \
#		-q 0.05 \
#		--outdir $OUTDIR

macs2 callpeak \
		--nomodel \
		--extsize 250 \
		--call-summits \
		-t $BAM_chip \
		-g $GENOME_SIZE \
		-n $SNAME \
		-q 0.05 \
		--outdir $OUTDIR

