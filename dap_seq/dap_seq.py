#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
DAP-seq pipeline

# D.Tolkunov

module unload samtools
module unload picard
module load jamo
module load bwa/0.7.12
module load samtools/1.3
module load picard/1.92
module load macs/1.4.2-1
module load bedtools/2.17.0
module load meme/4.10.0_2

INPUT_DATA="/global/projectb/scratch/dtolk/Bryce/DAP_Seq_Pipeline/test/inputdata.csv"
DIR_ANALYSIS="/global/projectb/scratch/dtolk/Bryce/DAP_Seq_Pipeline/test/results"
FLT="y"         # Fetch filtered or raw data ('y'/'n')
PE="y"          # Paired-end data ('y'/'n')
REF="/global/projectb/scratch/dtolk/Bryce/DAP_Seq_Pipeline/test/reference/Arabidopsis_thaliana/TAIR10/TAIR_10.fasta"
GFF_FILE="/global/projectb/scratch/dtolk/Bryce/DAP_Seq_Pipeline/test/reference/Arabidopsis_thaliana/TAIR10/annotatio
n/Athaliana_167_TAIR10.gene.gff3"
PIPELINE="/global/projectb/scratch/dtolk/Bryce/DAP_Seq_Pipeline/scripts"
GENOME_SIZE=`cat $REF|grep -v ">"|wc -m`

MYDIR=`pwd`
cd $PIPELINE
1. Fetch the data
python ./prepare_data/fetch_data.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --flt $FLT

2. Align, sort, index
python ./alignment/align_BWA.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --ref $REF --pe $PE

3. Deduplication
python ./alignment/uniq_mapped_and_dedup.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --stringent "y"

4. Produce BigWig files
python ./alignment/make_BigWig.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --ref $REF

5. Reads statistics
python ./read_stat/get_read_stat.py --csv $INPUT_DATA --dir $DIR_ANALYSIS

6. Peak calling
python ./peak_calling/call_peaks_nocontrol.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --gsize $GENOME_SIZE

7. Peak statistics
python ./peak_stat/get_peak_stat_nocontrol.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --gsize $GENOME_SIZE --gff $GFF_FILE

8. Print stats table
python ./peak_stat/print_table.py --dir $DIR_ANALYSIS

9. Motif analysis
python ./motif_analysis/run_motif_analysis.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --ref $REF
cd $MYDIR

10. Post-processing

- collect stat data from data.csv and table_stats.csv
- collect *._occ_profile.png plot file
- collect *.html and *.png from motif_analysis folder

- *.srt.unq.md.bam
- *.srt.unq.md.bam.bai
- *.srt.unq.md.bw
- *.narrowPeak (peaks)
- motif_analysis directory path

- Rsync BWA index files to the original genome location and remove the source

11. Cleanup

- Remove
  . .bam
  . .bam.bai
  . .unq.bam
  . .unq.bam.bai



For testing:
$/global/homes/s/sulsj/work/bitbucket-repo/jgi-rqc-pipeline/dap_seq/dap_seq.py -i /global/projectb/scratch/sulsj/2017.01.09-dap-seq/denis/test/inputdata.csv -g /global/projectb/scratch/dtolk/Bryce/DAP_Seq_Pipeline/test/reference/Arabidopsis_thaliana/TAIR10/annotation/Athaliana_167_TAIR10.gene.gff3 -o /global/projectb/scratch/sulsj/2017.01.09-dap-seq/out -r /global/projectb/scratch/dtolk/Bryce/DAP_Seq_Pipeline/test/reference/Arabidopsis_thaliana/TAIR10/TAIR_10.fasta -f -p



Denis' Chip_Seq pipeline rpo
https://github.com/dtolk/ChIP_Seq_pipeline



GFFs
Regarding the GFF and reference FA files:
/global/dna/projectdirs/RD/rnaseq_store/annotations
/global/dna/projectdirs/RD/rnaseq_store/genomes


Created: Jan 9 2017

sulsj (ssul@lbl.gov)


Revisions:

    01092017 0.0.9: init
    02012017 1.0.0: Changed to create input.csv file; Updated to record stats and plots
    03102017 1.1.0: Changed to take "[gene]_[species]" and find latest annotaion and reference files; Released 643c532
    03092017 1.1.1: Changed plotting code peak_stat/print_table.py and peak_stat/plot_distributions.R

    03102017 1.2.0: Updated for collecting more stats and cleanup
    03102017 1.2.1: Fixed file recording in dap_seq; Released bed354b
    03242017 1.2.2: Added colored log
    05082017 1.2.3: Fixed to check correct file extensions in ref and gff

    04172018 2.0.0: Porting to Denovo
                    Changed wigToBigWig to the latest version, Version 1.04.00 (http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/)
    06252018 2.0.1: Updated to detect pair-ended or not; Fixed table_stats.csv parsing;
    06262018 2.0.2: Removed -f BAM
    06282018 2.0.3: Removed nomode, extsize, -f BAM; Added --cutoff-analysis ==> to generate additional plot
    07022018 2.0.4: Removed --cutoff-analysis; Added back "--nomodel" and "--extsize 250" to macs2; Updated to deal with ATAC-Seq;
    
    


- recommend docker with meme, macs2 installed
- recommend rewrite pipeline to remove multiple nested calls and bring it all together into one pipeline to see all the parameters in one place
--- e.g. python %s/lib/motif_analysis/run_motif_analysis.py -> Run R & run meme.sh -> run meme with params
--- e.g. meme -oc $output -dna -time 18000 -maxsize 10m -mod zoops ... put params in a peak_caller.cfg script and show in the peak_caller log



"""

import os
import sys
# import time
import argparse
import glob
import getpass # for rsync the generated index files, *.bwt and others (qc_user only)
import MySQLdb

## custom libs in "../lib/"
SRCDIR = os.path.dirname(__file__)
# sys.path.append(os.path.join(SRCDIR, 'lib'))   ## rqc-pipeline/dap_seq/lib
sys.path.append(os.path.join(SRCDIR, '../lib'))   ## rqc-pipeline/lib
sys.path.append(os.path.join(SRCDIR, '../tools')) ## rqc-pipeline/tools

from common import get_logger, get_status, append_rqc_stats, append_rqc_file, set_colors
from rqc_utility import *
from rqc_constants import *
from db_access import *
from os_utility import run_sh_command
import rqc_fastq as fastqUtil
from db_access import jgi_connect_db

SCRIPTNAME = __file__
VERSION = "2.0.4"

CFG = {}
#LOGLEVEL = "INFO"
LOGLEVEL = "DEBUG"
# MODULE_CMD = "module unload dbamtools;module unload picard;module load jamo;module load bwa/0.7.12;module load samtools/1.3;module load picard/1.92;module load macs/1.4.2-1;module load bedtools/2.17.0;module load meme/4.10.0_2"
MODULE_CMD = "module load jamo;module load samtools" # don't need this for every command!!!

## missing modules on Denovo
# module load bwa/0.7.12
# module load picard/1.92
# module load macs/1.4.2-1
# module load bedtools/2.17.0
# module load meme/4.10.0_2

GFF_ROOT = "/global/dna/projectdirs/RD/rnaseq_store/annotations"
REF_ROOT = "/global/dna/projectdirs/RD/rnaseq_store/genomes"

color = {}
color = set_colors(color, True)


"""
STEP 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Fetch data
lib/prepare_data/fetch_data.py
* could replace with jamo command in the pipeline, needs to create ./data.csv
Not sure if we need everything in data.csv ...

"""
def step_1(inputCsvFile, outputPath, useFiltered, log):
    log.info("\n\n%sSTEP1 - Fetch the data <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "1_step_1 in progress"
    checkpoint_step_wrapper(status)

    ##python ./prepare_data/fetch_data.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --flt $FLT
    cmd = "%s; python %s/lib/prepare_data/fetch_data.py --csv %s --dir %s --flt %s" % (MODULE_CMD, SRCDIR, inputCsvFile, outputPath, useFiltered)
    _, _, exitCode = run_sh_command(cmd, True, log)

    if exitCode != RQCExitCodes.JGI_SUCCESS:
        log.info("1_step_1 failed.")
        status = "1_step_1 failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("1_step_1 complete.")
        status = "1_step_1 complete"
        checkpoint_step_wrapper(status)


    return status


"""
STEP 2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Align, sort, index

lib/alignment/BWA_PE.sh
- BWA = shifter --image=registry.services.nersc.gov/jgi/bwa:latest bwa
- uses samtools also
"""
def step_2(inputCsvFile, outputPath, refDatabase, isPaired, log):
    log.info("\n\n%sSTEP2 - Align, sort, index <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "2_step_2 in progress"
    checkpoint_step_wrapper(status)

    ##python ./alignment/align_BWA.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --ref $REF --pe $PE
    cmd = "%s; python %s/lib/alignment/align_BWA.py --csv %s --dir %s --ref %s --pe %s" % (MODULE_CMD, SRCDIR, inputCsvFile, outputPath, refDatabase, isPaired)
    _, _, exitCode = run_sh_command(cmd, True, log)

    if exitCode != RQCExitCodes.JGI_SUCCESS:
        log.info("2_step_2 failed.")
        status = "2_step_2 failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("2_step_2 complete.")
        status = "2_step_2 complete"
        checkpoint_step_wrapper(status)


    return status


"""
STEP 3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Deduplication
uses samtools (stringent = y) otherwise uses picard
lib/alignment/dedup_samtools.sh

"""
def step_3(inputCsvFile, outputPath, log):
    log.info("\n\n%sSTEP3 - Deduplication <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "3_step_3 in progress"
    checkpoint_step_wrapper(status)

    ##python ./alignment/uniq_mapped_and_dedup.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --stringent "y"
    cmd = "%s; python %s/lib/alignment/uniq_mapped_and_dedup.py --csv %s --dir %s --stringent y" % (MODULE_CMD, SRCDIR, inputCsvFile, outputPath)
    _, _, exitCode = run_sh_command(cmd, True, log)

    if exitCode != RQCExitCodes.JGI_SUCCESS:
        log.info("3_step_3 failed.")
        status = "3_step_3 failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("3_step_3 complete.")
        status = "3_step_3 complete"
        checkpoint_step_wrapper(status)


    return status



"""
STEP 4 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Produce BigWig files
lib/alignment/bam2bw.sh  
"""
def step_4(inputCsvFile, outputPath, ref, log):
    log.info("\n\n%sSTEP4 - Produce BigWig files <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "4_step_4 in progress"
    checkpoint_step_wrapper(status)

   ##python ./alignment/make_BigWig.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --ref $REF
    cmd = "%s; python %s/lib/alignment/make_BigWig.py --csv %s --dir %s --ref %s" % (MODULE_CMD, SRCDIR, inputCsvFile, outputPath, ref)
    _, _, exitCode = run_sh_command(cmd, True, log)

    if exitCode != RQCExitCodes.JGI_SUCCESS:
        log.info("4_step_4 failed.")
        status = "4_step_4 failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("4_step_4 complete.")
        status = "4_step_4 complete"
        checkpoint_step_wrapper(status)


    return status


"""
STEP 5 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Reads statistics
- include in this script, its mapping stats from samtools and wc -l...
"""
def step_5(inputCsvFile, outputPath, log):
    log.info("\n\n%sSTEP5 - Reads statistics <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "5_step_5 in progress"
    checkpoint_step_wrapper(status)

   ##python ./read_stat/get_read_stat.py --csv $INPUT_DATA --dir $DIR_ANALYSIS
    cmd = "%s; python %s/lib/read_stat/get_read_stat.py --csv %s --dir %s" % (MODULE_CMD, SRCDIR, inputCsvFile, outputPath)
    _, _, exitCode = run_sh_command(cmd, True, log)

    if exitCode != RQCExitCodes.JGI_SUCCESS:
        log.info("5_step_5 failed.")
        status = "5_step_5 failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("5_step_5 complete.")
        status = "5_step_5 complete"
        checkpoint_step_wrapper(status)


    return status


"""
STEP 6 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Peak calling
lib/peak_calling/macs2_nocontrol.sh 
"""
def step_6(inputCsvFile, outputPath, genomeSz, log):
    log.info("\n\n%sSTEP6 - Peak calling <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "6_step_6 in progress"
    checkpoint_step_wrapper(status)

   ##python ./peak_calling/call_peaks_nocontrol.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --gsize $GENOME_SIZE
    cmd = "%s; python %s/lib/peak_calling/call_peaks_nocontrol.py --csv %s --dir %s --gsize %d" % (MODULE_CMD, SRCDIR, inputCsvFile, outputPath, genomeSz)
    _, _, exitCode = run_sh_command(cmd, True, log)

    if exitCode != RQCExitCodes.JGI_SUCCESS:
        log.info("6_step_6 failed.")
        status = "6_step_6 failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("6_step_6 complete.")
        status = "6_step_6 complete"
        checkpoint_step_wrapper(status)


    return status


"""
STEP 7 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Peak statistics
R script: peak_enrichment.R
plot_distributions.R
num_reads_in_peaks.sh -> intersectBed
"""
def step_7(inputCsvFile, outputPath, genomeSz, gffFile, log):
    log.info("\n\n%sSTEP7 - Peak statistics <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "7_step_7 in progress"
    checkpoint_step_wrapper(status)

    ##python ./peak_stat/get_peak_stat_nocontrol.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --gsize $GENOME_SIZE --gff $GFF_FILE
    cmd = "%s; python %s/lib/peak_stat/get_peak_stat_nocontrol.py --csv %s --dir %s --gsize %d --gff %s" % (MODULE_CMD, SRCDIR, inputCsvFile, outputPath, genomeSz, gffFile)
    _, _, exitCode = run_sh_command(cmd, True, log)

    if exitCode != RQCExitCodes.JGI_SUCCESS:
        log.info("7_step_7 failed.")
        status = "7_step_7 failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("7_step_7 complete.")
        status = "7_step_7 complete"
        checkpoint_step_wrapper(status)


    return status


"""
STEP 8 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Print stats table
create table_stats.csv from data.csv, concats lots of data

"""
def step_8(outputPath, log):
    log.info("\n\n%sSTEP8 - Print stats table <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "8_step_8 in progress"
    checkpoint_step_wrapper(status)

    ##python ./peak_stat/print_table.py --dir $DIR_ANALYSIS
    cmd = "%s; python %s/lib/peak_stat/print_table.py --dir %s" % (MODULE_CMD, SRCDIR, outputPath)
    _, _, exitCode = run_sh_command(cmd, True, log)

    if exitCode != RQCExitCodes.JGI_SUCCESS:
        log.info("8_step_8 failed.")
        status = "8_step_8 failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("8_step_8 complete.")
        status = "8_step_8 complete"
        checkpoint_step_wrapper(status)


    return status


"""
STEP 9 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Motif analysis
R: extract_seq.R
run_meme.sh -> meme (+params) - module on genepool but not denovo

shifter --image=sulsj/rocker:latest Rscript

"""
def step_9(inputCsvFile, outputPath, refDatabase, log):
    log.info("\n\n%sSTEP9 - Motif analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "9_step_9 in progress"
    checkpoint_step_wrapper(status)

    ##python ./motif_analysis/run_motif_analysis.py --csv $INPUT_DATA --dir $DIR_ANALYSIS --ref $REF
    cmd = "%s; python %s/lib/motif_analysis/run_motif_analysis.py --csv %s --dir %s --ref %s" % (MODULE_CMD, SRCDIR, inputCsvFile, outputPath, refDatabase)
    _, _, exitCode = run_sh_command(cmd, True, log)

    if exitCode != RQCExitCodes.JGI_SUCCESS:
        log.info("9_step_9 failed.")
        status = "9_step_9 failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("9_step_9 complete.")
        status = "9_step_9 complete"
        checkpoint_step_wrapper(status)


    return status


"""
STEP 10 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Post-processing

"""
def step_10(outputPath, libName, log):
    log.info("\n\n%sSTEP10 - Post-processing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "10_step_10 in progress"
    checkpoint_step_wrapper(status)

    retCode = RQCExitCodes.JGI_SUCCESS


    ## record output files under bam -------------------------------------------
    bamDir = os.path.join(outputPath, "bam", libName)
    bamOutputFiles = os.listdir(bamDir)
    unq_md_bam = [i for i in bamOutputFiles if i.endswith('.srt.unq.md.bam')]
    assert len(unq_md_bam) == 1
    unq_md_bam_bai = [i for i in bamOutputFiles if i.endswith('.srt.unq.md.bam.bai')]
    assert len(unq_md_bam_bai) == 1
    unq_md_bw = [i for i in bamOutputFiles if i.endswith('.srt.unq.md.bw')]
    assert len(unq_md_bw) == 1

    append_rqc_file(CFG["files_file"], "unq_md_bam", os.path.join(bamDir, unq_md_bam[0]), log)
    append_rqc_file(CFG["files_file"], "unq_md_bam_bai", os.path.join(bamDir, unq_md_bam_bai[0]), log)
    append_rqc_file(CFG["files_file"], "unq_md_bw", os.path.join(bamDir, unq_md_bw[0]), log)


    ## record *.narrowPeak file under peaks ------------------------------------
    peaksDir = os.path.join(outputPath, "peaks", libName)
    peaksFiles = os.listdir(peaksDir)
    narrowpeakFile = [i for i in peaksFiles if i.endswith('.narrowPeak')]
    assert len(narrowpeakFile) == 1
    append_rqc_file(CFG["files_file"], "narrowpeak", os.path.join(peaksDir, narrowpeakFile[0]), log)

    ## record motif_analysis path
    # motifanalysisDir = os.path.join(peaksDir, "motif_analysis")
    # append_rqc_stats(CFG["stats_file"], "motif_analysis_path", motifanalysisDir, log)


    ## data.csv -----------------------------------------------------------------
    dataCsvFile = os.path.join(outputPath, "data.csv")
    append_rqc_file(CFG["files_file"], "data_csv", dataCsvFile, log)

    if os.path.isfile(dataCsvFile):
        with open(dataCsvFile, "r") as DFH:
            header = []
            data = []
            for l in DFH:
                if l is None:
                    continue
                elif l.startswith("sample_name"):
                    # header = l.strip().split()
                    header = l.strip().split('\t')
                else:
                    # data = l.strip().split()
                    data = l.strip().split('\t')

        if len(header) != 24:
            log.error("Unexpected file format: %s, %s, %d", dataCsvFile, header, len(header))
            retCode = RQCExitCodes.JGI_FAILURE

        else:
            for i in range(len(header)):
                append_rqc_stats(CFG["stats_file"], "data_csv_" + header[i], data[i], log)

    else:
        log.error("data.csv file not found: %s", dataCsvFile)
        retCode = RQCExitCodes.JGI_FAILURE


    ## table_stats.csv ----------------------------------------------------------
    tableStatsCsvFile = os.path.join(outputPath, "table_stats.csv")
    append_rqc_file(CFG["files_file"], "table_stast_csv", tableStatsCsvFile, log)

    if os.path.isfile(tableStatsCsvFile):
        with open(tableStatsCsvFile, "r") as TFH:
            header = []
            data = []
            for l in TFH:
                if l is None:
                    continue
                elif l.startswith("library"):
                    # header = l.strip().split()
                    header = l.strip().split('\t')
                else:
                    # data = l.strip().split()
                    data = l.strip().split('\t')

        if len(header) != 15:
            log.error("Unexpected file format: %s, %s, %d", dataCsvFile, header, len(header))
            retCode = RQCExitCodes.JGI_FAILURE
        else:
            for i in range(len(header)):
                append_rqc_stats(CFG["stats_file"], "table_stats_csv_" + header[i], data[i], log)

    else:
        log.error("table_stats.csv file not found: %s", tableStatsCsvFile)
        retCode = RQCExitCodes.JGI_FAILURE


    ## occ profile
    occProfPngFile = os.path.join(outputPath, "peaks", libName, libName + "_occ_profile.png")
    if os.path.isfile(occProfPngFile):
        append_rqc_file(CFG["files_file"], "occ_prof_png", occProfPngFile, log)
    else:
        log.error("Occ profile plot file not found: %s", occProfPngFile)
        retCode = RQCExitCodes.JGI_FAILURE


    ## motif analysis
    motifAnalDir = os.path.join(outputPath, "peaks", libName, "motif_analysis")
    motifAnalHtmlFile = os.path.join(motifAnalDir, "meme.html")
    motifAnalTxtFile = os.path.join(motifAnalDir, "meme.txt")
    motifAnalXmlFile = os.path.join(motifAnalDir, "meme.xml")

    if os.path.isfile(motifAnalHtmlFile):
        append_rqc_file(CFG["files_file"], "motif_anal_html", motifAnalHtmlFile, log)
        append_rqc_file(CFG["files_file"], "motif_anal_txt", motifAnalTxtFile, log)
        append_rqc_file(CFG["files_file"], "motif_anal_xml", motifAnalXmlFile, log)

    else:
        log.error("Motif analysis html file not found: %s", motifAnalHtmlFile)
        retCode = RQCExitCodes.JGI_FAILURE


    if retCode != RQCExitCodes.JGI_SUCCESS:
        log.info("10_step_10 failed.")
        status = "10_step_10 failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("10_step_10 complete.")
        status = "10_step_10 complete"
        checkpoint_step_wrapper(status)


    return status



'''===========================================================================
    checkpoint_step_wrapper

'''
def checkpoint_step_wrapper(status):
    assert(CFG["status_file"])
    checkpoint_step(CFG["status_file"], status)



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

if __name__ == "__main__":
    desc = "RQC DAP-Seq Pipeline"
    parser = argparse.ArgumentParser(description=desc)
    # parser.add_argument("-g", "--gff", dest="gffFile", help="GFF file", required=True)
    # parser.add_argument("-i", "--csv", dest="inputCsvFile", help="Input CSV file to use", required=True)
    parser.add_argument("-l", "--library", dest="libName", help="Input library name to process", required=True)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Output path to write to", required=True)
    # parser.add_argument("-r", "--ref", dest="refDatabase", help="Reference database", required=True)
    parser.add_argument("-r", "--organism", dest="organismName", help="Organism name ([genus]_[species])", required=True)
    parser.add_argument('-v', '--version', action='version', version=VERSION)

    ## switch
    parser.add_argument("-f", "--flt", action="store_true", help="Fetch filtered or raw data", dest="useFiltered", default=True, required=True)
    parser.add_argument("-p", "--pe", action="store_true", help="Paired-end data or not", dest="isPaired", default=True, required=True)

    options = parser.parse_args()

    outputPath = options.outputPath

    ## create output_directory if it doesn't exist
    if not os.path.isdir(outputPath):
        os.makedirs(outputPath)

    libName = options.libName
    organismName = options.organismName
    # refDatabase = options.refDatabase
    # gffFile = options.gffFile
    # inputCsvFile = options.inputCsvFile

    gffFileDir = os.path.join(GFF_ROOT, organismName)
    refFileDir = os.path.join(REF_ROOT, organismName)
    refFileTempDir = os.path.join(refFileDir, "temp")

    assert os.path.isdir(gffFileDir), "GFF file directory does not exist: %s" % (gffFileDir)
    assert os.path.isdir(refFileDir), "REF file directory does not exist: %s" % (refFileDir)

    ## Get the latest fasta file
    try:
        gffFile = max(glob.iglob(os.path.join(gffFileDir, '*.gff3')), key=os.path.getctime)
    except ValueError:
        gffFile = max(glob.iglob(os.path.join(gffFileDir, '*.gff2')), key=os.path.getctime)

    try:
        refDatabase = max(glob.iglob(os.path.join(refFileDir, '*.fa')), key=os.path.getctime)
    except ValueError:
        refDatabase = max(glob.iglob(os.path.join(refFileDir, '*.fasta')), key=os.path.getctime)

    # if gffFile is None:
    #     gffFile = max(glob.iglob(os.path.join(gffFileDir, '*.gff2')), key=os.path.getctime)
    # if refDatabase is None:
    #     refDatabase = max(glob.iglob(os.path.join(refFileDir, '*.fasta')), key=os.path.getctime)

    assert gffFile, "Cannot find an annotation (*.gff3) file."
    assert refDatabase, "Cannot find a referece genome (*.fa) file."


    ## create input file
    inputCsvFile = os.path.join(outputPath, "inputdata.csv")
    with open(inputCsvFile, 'w') as IFH:
        IFH.write("sample_name\tlibrary\tpaired_control_lib\tcondition\tlib_type\n")
        IFH.write("%s\t%s\t%s\t%s\t%s\n" % ("sn", libName, "N", "cond", "lt"))

    useFiltered = "y" if options.useFiltered else "n"
    
    ## ignore command line option 06272018. Will detect it in runtime
    # isPaired = "y" if options.isPaired else "n"
    
            

    CFG["status_file"] = os.path.join(outputPath, "dap_seq_status.log")
    CFG["files_file"] = os.path.join(outputPath, "dap_seq_files.tmp")
    CFG["stats_file"] = os.path.join(outputPath, "dap_seq_stats.tmp")
    CFG["log_file"] = os.path.join(outputPath, "dap_seq.log")

    print "Started DAP-Seq pipeline, writing log to: %s" % (CFG["log_file"])

    log = get_logger("dap_seq", CFG["log_file"], LOGLEVEL)

    log.info("=================================================================")
    log.info("   DAP-Seq (version %s)", VERSION)
    log.info("=================================================================")

    log.info("Starting %s...", SCRIPTNAME)

    log.info("GFF3 file: %s", gffFile)
    log.info("REF file: %s", refDatabase)

    ## Stage gff and ref
    ## TODO: scratch?
    inputDir = os.path.join(outputPath, "input")
    if not os.path.isdir(inputDir):
        os.makedirs(inputDir)

    newGff = os.path.join(inputDir, safe_basename(gffFile, log)[0])
    newRef = os.path.join(inputDir, safe_basename(refDatabase, log)[0])

    cmd = "rsync -avh %s %s" % (gffFile, newGff)
    _, _, exitCode = run_sh_command(cmd, True, log)
    assert exitCode == 0
    cmd = "rsync -avh %s %s" % (refDatabase, newRef)
    _, _, exitCode = run_sh_command(cmd, True, log)
    assert exitCode == 0
    
    
    ## Check if it's single- or paired
    ## "isPaired" is only used in step 2 for align_BWA.py
    isPaired = "y"
    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)
    stmt = """SELECT s.run_configuration FROM seq_units s WHERE s.library_name = %s"""
    sth.execute(stmt, [libName])
    rs = sth.fetchone()
    log.info("sql: %s", stmt % (libName))
    
    if rs and rs['run_configuration']:
        runConf = rs['run_configuration']
        if runConf.startswith('1x'):
            isPaired = "n"
    else:
        log.error("- Cannot determine if the input is paired or not. aborting!")
        exit(2)
    
    db.close()

    log.info("Input fastq is pair-ended = %s", isPaired)


    gffFile = newGff
    refDatabase = newRef
    log.info("Staged GFF3 file: %s", gffFile)
    log.info("Staged REF file: %s", refDatabase)

    append_rqc_file(CFG["files_file"], "gff_file", gffFile, log)
    append_rqc_file(CFG["files_file"], "ref_file", refDatabase, log)




    status = None
    nextStep = 0

    if os.path.isfile(inputCsvFile):
        log.info("Found input CSV file, %s. Starting processing.", inputCsvFile)
        if os.path.isfile(CFG["status_file"]):
            status = get_status(CFG["status_file"], log)
            log.info("Latest status = %s", status)

            if status == "start":
                pass

            elif status != "complete":
                nextStep = int(status.split("_")[0])
                if status.find("complete") != -1:
                    nextStep += 1
                log.info("Next step to do = %s", nextStep)

            else:
                ## already Done. just exit
                log.info("Completed %s: %s", SCRIPTNAME, inputCsvFile)
                exit(0)
        else:
            checkpoint_step_wrapper("start")
            status = get_status(CFG["status_file"], log)
            log.info("Latest status = %s", status)

    else:
        log.error("- %s not found, aborting!", inputCsvFile)
        exit(2)



    done = False
    cycle = 0

    if status == "complete":
        log.info("Status is complete")
        done = True


    ## get genome size
    wcCmd = "cat %s | grep -v '>' | wc -m" % refDatabase
    stdOut, _, exitCode = run_sh_command(wcCmd, True, log)
    genomeSize = int(stdOut.rstrip()) if exitCode == 0 else -1
    log.info("Reference: %s", refDatabase)
    log.info("Genome size: %d", genomeSize)

    while not done:
        cycle += 1

        ## Fetch the data
        if nextStep == 1 or status == "start":
            status = step_1(inputCsvFile, outputPath, useFiltered, log)
        if status == "1_step_1 failed":
            done = True

        ## Align, sort, index
        if nextStep == 2 or status == "1_step_1 complete":
            status = step_2(inputCsvFile, outputPath, refDatabase, isPaired, log)
        if status == "2_step_2 failed":
            done = True

        ## Deduplication
        if nextStep == 3 or status == "2_step_2 complete":
            status = step_3(inputCsvFile, outputPath, log)
        if status == "3_step_3 failed":
            done = True

        ## Produce BigWig files
        if nextStep == 4 or status == "3_step_3 complete":
            status = step_4(inputCsvFile, outputPath, refDatabase, log)
        if status == "4_step_4 failed":
            done = True

        ## Reads statistics
        if nextStep == 5 or status == "4_step_4 complete":
            status = step_5(inputCsvFile, outputPath, log)
        if status == "5_step_5 failed":
            done = True

        ## Peak calling
        if nextStep == 6 or status == "5_step_5 complete":
            status = step_6(inputCsvFile, outputPath, genomeSize, log)
        if status == "6_step_6 failed":
            done = True

        ## Peak statistics
        if nextStep == 7 or status == "6_step_6 complete":
            status = step_7(inputCsvFile, outputPath, genomeSize, gffFile, log)
        if status == "7_step_7 failed":
            done = True

        ## Print stats table
        if nextStep == 8 or status == "7_step_7 complete":
            status = step_8(outputPath, log)
        if status == "8_step_8 failed":
            done = True

        ## Motif analysis
        if nextStep == 9 or status == "8_step_8 complete":
            status = step_9(inputCsvFile, outputPath, refDatabase, log)
        if status == "9_step_9 failed":
            done = True

        ## Post-processing
        if nextStep == 10 or status == "9_step_9 complete":
            status = step_10(outputPath, libName, log)
        if status == "10_step_10 failed":
            done = True

        ##----------------------------------------------------------------------
        ## Cleanup
        ##----------------------------------------------------------------------
        if nextStep == 11 or status == "10_step_10 complete":
            log.info("\n\n%sSTEP11 - Cleanup <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
            # status = do_cleanup_readqc(log)

            ## clean up input files
            cmd = "rm %s" % gffFile
            run_sh_command(cmd, True, log)
            cmd = "rm %s" % refDatabase
            run_sh_command(cmd, True, log)

            ## rsync generated BWA index files (*.bwt and others) if qc_user
            ## remove all input BWA index files
            uname = getpass.getuser()
            if uname == "qc_user" and not os.path.isdir(refFileTempDir): ## if others are not work on rsyncing BWT
                refFiles = os.listdir(refFileDir)
                bwtFile = [i for i in refFiles if i.endswith('.bwt')]

                if len(bwtFile) == 0: ## if bwt not found
                    log.info("Rsync BWA index files to %s", refFileDir)
                    rsyncCmd = "mkdir %s && rsync -avh %s/* %s && mv %s/* %s && rm -rf %s" \
                               % (refFileTempDir, inputDir, refFileTempDir, refFileTempDir, refFileDir, refFileTempDir) ## we need to rsync all index files

                    _, _, exitCode = run_sh_command(rsyncCmd, True, log)

                    if exitCode != RQCExitCodes.JGI_SUCCESS:
                        log.warning("BWA index file rsync failed.")
                    else:
                        log.info("BWA index file rsync complete.")


            ## remove bwa index files under input
            rmCmd = "rm %s" % (inputDir + "/*")
            _, _, exitCode = run_sh_command(rmCmd, True, log)
            if exitCode == RQCExitCodes.JGI_SUCCESS:
                log.info("BWA index files removed successfully.")


            ## move rqc-files.tmp to rqc-files.txt
            newFilesFile = os.path.join(outputPath, "dapseq_files.txt")
            newStatsFile = os.path.join(outputPath, "dapseq_stats.txt")


            cmd = "mv %s %s " % (CFG["files_file"], newFilesFile)
            log.info("mv cmd: %s", cmd)
            run_sh_command(cmd, True, log)

            cmd = "mv %s %s " % (CFG["stats_file"], newStatsFile)
            log.info("mv cmd: %s", cmd)
            run_sh_command(cmd, True, log)

            status = "11_cleanup complete"

        if status == "11_cleanup failed":
            done = True

        if status == "11_cleanup complete":
            status = "complete" ## FINAL COMPLETE!
            done = True


        ## don't cycle more than 10 times ...
        if cycle > 10:
            done = True



    if status != "complete":
        log.info("Status %s", status)

    else:
        log.info("\n\nCompleted %s", SCRIPTNAME)
        checkpoint_step_wrapper("complete")
        log.info("Done.")
        print "Done."



## EOF
