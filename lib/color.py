class COLOR:
    RED = '\033[1;31;40m'
    GREEN = '\033[1;32;40m'
    YELLOW = '\033[1;33;40m'
    BLUE = '\033[1;34;40m'
    PURPLE = '\033[1;35;40m'
    CYAN = '\033[2;36;40m'
    WHITE = '\033[1;37;40m'
    BLACK = '\033[2;30;47m'
    ENDC = '\033[0m'
    
    COLOR_MAP = {
        'red'       : RED,
        'green'     : GREEN,
        'yellow'    : YELLOW,
        'blue'      : BLUE,
        'purple'    : PURPLE,
        'cyan'      : CYAN,
        'white'     : WHITE,
        'black'     : BLACK
    }
    
def color_str(txt, color):
    if color in COLOR.COLOR_MAP:
        txt = '%s%s%s' % (COLOR.COLOR_MAP[color], txt, COLOR.ENDC)
    return txt