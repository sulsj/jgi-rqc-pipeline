#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Library to look up taxonomy ids (ncbi or gi) using BBTools taxonomy server

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import os
import sys
import json
import time

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

from curl3 import Curl, CurlHttpException

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions


'''
Return taxonomy
'''
def get_taxonomy(my_tax_id, tax_type):

    tax_dict = {
        "superkingdom" : "-",
        "domain" : "-", # aka kingdom
        "phylum" : "-",
        "class" : "-",
        "order" : "-",
        "family" : "-",
        "genus" : "-",
        "species" : "-"
    }

    if my_tax_id: # > 0:

        # problem with "-" for values
        # Viruses;phylum Viruses;class Viruses;order Viruses;Parvovirinae;Erythroparvovirus;Primate erythroparvovirus 1
        # D,P,C,O,F,G,S
        # Viruses;-;-;-;Parvovirinae;Erythroparvovirus;Primate erythroparvovirus 1
        # Viruses D -> F
        my_tax_id = str(my_tax_id).replace(" ", "_").replace("/", "_")
        r = get_taxonomy_service(my_tax_id, tax_type)
        
        if not r:
            return tax_dict
        #print my_tax_id
        #print r, type(r)
        my_json = {}
        if type(r) == dict:
            my_json = r
        else:
            my_json = json.loads(r)

        # look for not found error
        #print my_tax_id
        #print my_json
        err = my_json[my_tax_id].get("error")


            
        for tax_type in tax_dict:
            if tax_type in my_json[my_tax_id]:
                tax_dict[tax_type] = my_json[my_tax_id][tax_type]['name']

        if tax_dict['species'] == "-" and tax_dict['genus'] != "-":
            tax_dict['species'] = tax_dict['genus'] + " sp."

        if err:
            tax_dict['error'] = err

    return tax_dict

'''
Return json of taxonomy
@tax_id = ncbi taxonomy id or ncbi gi
@tax_type = gi = use gi service, otherwise use ncbi tax id service

- look up taxonomy:curl https://taxonomy.jgi-psf.org/tax/gi/1001624332

'''
def get_taxonomy_service(my_tax_id, tax_type):


    response = None
    curl_cnt = 0 # retry up to 3 times
    exit_code = 0
    
    #https://taxonomy.jgi-psf.org/tax/taxid/9606
    #https://taxonomy.jgi-psf.org/tax/gi/1234
    
    tax_url = "https://taxonomy.jgi-psf.org"
    #tax_url = "https://taxonomy-dev.jgi-psf.org"
    
    tax_api = "tax/taxid/[tax_id]"
    if tax_type == "gi":
        tax_api = "tax/gi/[tax_id]"
    elif tax_type.startswith("acc"):
        tax_api = "tax/accession/[tax_id]"
        #curl taxonomy.jgi-psf.org/tax/accession/NT_165936.1 -> Aspergillus terreus
    elif tax_type == "ssu":
        #/silvaheader/ will accept a Silva sequence header such as KC415233.1.1497
        tax_api = "tax/silvaheader/[tax_id]"
    elif tax_type == "name":
        tax_api = "tax/name/[tax_id]"
        #http://taxonomy.jgi-psf.org/tax/name/Chitinophaga_sp._str._KP01_AB278570.1

    

    if my_tax_id > 0:
        done = False
        
        while not done:

            curl = Curl(tax_url)
            my_api = tax_api.replace("[tax_id]", str(my_tax_id))
            
            curl_cnt += 1
            
            try:
                response = curl.get(my_api)
                done = True
                exit_code = 0
                
            except CurlHttpException as e:
    
                print "- Failed HTTPS TAXONOMY API Call: %s/%s, %s: %s" % (tax_url, my_api, e.code, e.response)
                exit_code = 1
                # e.code = http status code
    
            except Exception as e:
                print "- Failed TAXONOMY API Call: %s/%s, %s" % (tax_url, my_api, e.args)
                exit_code = 1
                
                #sys.exit(1)

            if curl_cnt > 3:
                done = True
                
            # wait 5 minutes to see if Brian brings it back up
            if not done:
                print "- waiting 300 seconds to try again, attempt %s" % curl_cnt
                time.sleep(300)
                #time.sleep(3)


    if exit_code > 0:
        print "- tax server %s timeout!" % tax_url
        sys.exit(exit_code)

    return response


'''
Convert dict to an ordered list based on the taxonomy lineage
'''
def format_taxonomy(tax_dict, sep = ";"):

    tax_arr = ['superkingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    tax_list = []
    for tax in tax_arr:
        tax_val = "-" # nothing specified
        if tax in tax_dict:
            tax_val = tax_dict[tax]
        else:
            if tax == "superkingdom":
                if 'domain' in tax_dict:
                    tax_val = tax_dict['domain']

        tax_list.append(tax_val)

    #print tax_list

    return sep.join(tax_list)


def kurt_test():
    
    tax_url = "https://taxonomy.jgi-psf.org"
    
    tax_api = "stax/sc_taxid/9685"
    
    curl = Curl(tax_url)
    
    try:
        response = curl.get(tax_api, output="raw")
        print "OK"
        
    except CurlHttpException as e:

        print "- Failed HTTPS TAXONOMY API Call: %s/%s, %s: %s" % (tax_url, tax_api, e.code, e.response)
        print "CURL FAILURE"
        # e.code = http status code

    except Exception as e:
        print "- Failed TAXONOMY API Call: %s/%s, %s" % (tax_url, tax_api, e.args)
        print "API FAILURE"

    
    
    print response
    


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__


if __name__ == "__main__":


    # SSu test
    print "Bacteria_Firmicutes_Clostridia_Clostridiales_Clostridiaceae 1_Clostridium sensu stricto 11_Clostridium acetobutylicum"
    print format_taxonomy(get_taxonomy("tid|1488|SSU_LZYX01000005.18.1511", "ssu"))
    print


    print "Rhizobium huautlense OS-49.b AM237359.1 1..1409"
    print format_taxonomy(get_taxonomy("Rhizobium huautlense OS-49.b AM237359.1 1..1409", "name"))
    print

    # Felix Catus
    print "Cat: ncbi 9685"
    print format_taxonomy(get_taxonomy(9685, "ncbi"), "|")
    print

    #1001624332 = E.Coli
    print "E.Coli: gi 1001624332"
    print format_taxonomy(get_taxonomy(1001624332, "gi"))
    print
    # 0
    print "Nothing: 0"
    print format_taxonomy(get_taxonomy(0, "ncbi"))
    print

    # Not sure
    print "? gi: 1104307744"
    print format_taxonomy(get_taxonomy(1104307744, "gi"))
    print

    # Shark week (Tiger Shark)
    print "Galeocerdo cuvier"
    print format_taxonomy(get_taxonomy("Galeocerdo cuvier", "name"))

    print
    print "Kurt Test"
    kurt_test()

    sys.exit(0)


