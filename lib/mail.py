#!/usr/bin/env python
'''
	Send email to a given address 

'''
import os
from smtplib import SMTP
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders

'''
	dest: a single email address string, or a list of email addresses
	sub: email subject string
	body: email body string
	afiles: list of attached files
'''
def send_email(dest, subj, body, afiles=None):
	
	#print('\n send_email dest type=%s' % type(dest))
	
	s = SMTP('localhost')
	
	# must use this, registered with the email list
	from_addr = 'rqc-auto-sender@jgi-psf.org'
	to_addr_list = None
	
	if isinstance(dest, list):
		to_addr_list = dest 
	elif isinstance(dest, str):
		to_addr_list = [dest]

	if not to_addr_list:
		print('No destination is provided!')
		exit(1)

	msg = MIMEMultipart() 

	msg['From'] = from_addr
	msg['To'] = str(to_addr_list)
	msg['Subject'] = subj 
	msg.attach(MIMEText(body, 'plain'))

	#print('\nsend email from [%s]' % msg['From'])
	#print('send email to [%s]' % msg['To'])
	#print('send email subj [%s]' % msg['Subject'])
	
	if afiles:
		for f in afiles:
			part = MIMEBase('application', "octet-stream")
			part.set_payload( open(f,"rb").read() )
			Encoders.encode_base64(part)
			part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
			msg.attach(part)
	
	s.sendmail(from_addr, to_addr_list, msg.as_string())
	
	s.quit()


if __name__ == '__main__':
	sendto = ['syao@lbl.gov', 'syao@lbl.gov']
	
	print('Testing the email function.')
	#send_email(sendto[0], 'SEGQC-COMPLETE Microbial', 'test subject string')
	#send_email(sendto[0], 'SEGQC-NEEDS WORK Microbial', 'test subject string')
	#send_email(sendto[0], 'SEGQC-NO QC NEEDED Microbial', 'test subject string')
	#send_email(sendto[0], 'SEGQC - COMPLETED Microbial', 'test subject string')
	send_email(sendto[0], 'QC-COMPLETED Microbial', 'test subject string with COMPLETED')
	
	
	#print('   sending email to %s with attachment' % str(sendto))
	#send_email(sendto, 'Test 1: with attachment', 'This is a test with attachment fine', afiles=['%s' % __file__])
	#print('   sending email to %s without attachment' % sendto[0])
	#send_email(sendto[0], 'Test 2: no attachment', 'This is a new test')
	print('Test Completed!')
