#!/usr/bin/env python
import gzip
import os
import pprint

#-------------
# constants:
FT_FASTQ = 1
FT_TASTA = 2

'''
return a file handle and file type
@fname: fastq/fasta file name (full path)
'''
def seq_file_handle(fname):
    if not fname or not os.path.isfile(fname):
        return None, None

    ftype = FT_FASTQ
    fh = None
    if fname.endswith('fasta.gz') or fname.endswith('.fasta'):
        ftype = FT_TASTA

    if fname.endswith('.gz'):
        fh = gzip.open(fname, 'rb')
    else:
        fh = open(fname)

    return (fh, ftype)

def kmer_generator(seq,k):
    for i in xrange(len(seq)-k+1):
        yield seq[i:i+k]

'''
read generator of an input file
@fh : file handle
@fname : file name, for type detection (fastq vs fasta)
return read sequence one by one
'''
def read_generator(fname):

    fh = None
    lstep = 4   # fastq and fasta.gz

    fh, ftype = seq_file_handle(fname)

    if fh:
        lcnt = 0
        read = ''
        for line in fh: #PE vs SE? fastq vs fasta?
            line = line.strip()
            lcnt += 1
            if ftype == FT_FASTQ and lcnt % lstep == 2:
                read = line
                yield read
            elif ftype != FT_FASTQ:
                if line.startswith('>'):
                    if read:
                        yield read
                    read = ''
                    continue
                else:
                    read += line

        #- last read for fasta format
        if ftype != FT_FASTQ and read:
            yield read

        fh.close()

'''
fasta record generator of an input file
@fname : file name, for type detection (fastq vs fasta)
return fasta record sequence one by one
'''
def fasta_record_generator(fname):

    fh = None
    lstep = 4   # fastq and fasta.gz

    fh, ftype = seq_file_handle(fname)

    if fh and ftype != FT_FASTQ:
        lcnt = 0
        read = ''
        header = ''
        for line in fh: #PE vs SE? fastq vs fasta?
            line = line.strip()
            lcnt += 1

            if line.startswith('>'):
                if read:
                    yield '%s\n%s' % (header, read)
                read = ''
                header = line
            else:
                read += line

        #- last read for fasta format
        if read:
            yield '%s\n%s' % (header, read)

        fh.close()

'''
a generator
for a given kmer sequence, produces all the possible kmers that extends kmer by 1 base in forward direction.
'''
def forward_kmer_generator(km):
    for x in 'ACGT':
        yield km[1:]+x

'''
a generator
for a given kmer sequence, produces all the possible kmers that extends kmer by 1 base in backward direction.
'''
def backward_kmer_generator(km):
    for x in 'ACGT':
        yield x + km[:-1]

def reverse_complement(seq):
    for base in seq:
        if base not in 'ATCGatcg':
            return None

    #seq1 = 'ATCGTAGCatcgtagc'
    #seq_dict = { seq1[i]:seq1[i+4] for i in range(16) if i < 4 or 8<=i<12 }
    #pprint.pprint(seq_dict)

    seq_dict = {    'A': 'T',
                    'C': 'G',
                    'G': 'C',
                    'T': 'A',
                    'a': 't',
                    'c': 'g',
                    'g': 'c',
                    't': 'a'}

    return "".join([seq_dict[base] for base in reversed(seq)])


'''
return PE, READ_1_LEN, READ_2_LEN (None|True|False, 0|VAL, 0|VAL)
'''
def read_type_length(fname):
    fh, ftype = seq_file_handle(fname)

    def fastq_one_rec(fh):
        header = (fh.readline()).strip()
        seq = (fh.readline()).strip()
        fh.readline()
        qual = (fh.readline()).strip()
        return header, seq, qual

    pe = None
    r1len = 0
    r2len = 0

    if fh:
        if ftype == FT_FASTQ:
            r1_h, r1_s, r1_q = fastq_one_rec(fh)
            r2_h, r2_s, r2_q = fastq_one_rec(fh)
            r1len = len(r1_s)
            r2len = len(r2_s)

            #- find pair-wise differences in the 2 record headers
            #- assumption: for interleaved PE fastq, the record header strings for a pair must differ by only '1' in 1st, and '2' in 2nd
            hdiff = [(a, b) for a, b in zip(r1_h, r2_h) if a != b]
            if len(hdiff) == 1 and hdiff[0] == ('1', '2'):
                pe = True
            else:
                pe = False
        elif ftype == FT_TASTA:
            ## fasta format - not PE, seq in multiple lines
            line = (fh.readline()).strip()
            seq = ''
            if line.startswith('>'):
                while True:
                    line = (fh.readline()).strip()
                    if line.startswith('>'):
                        break
                    else:
                        seq += line
            pe = False
            r1len = len(seq)
        fh.close()

    return pe, r1len, r2len

'''
merge kmers into contigs in the de Bruijn graph in forward direction
@kdict: the dictionary with keys are the colleciton of the kmers
@kmer: a starting kmer
'''
def contig_forward(kdict, kmer):
    c_fw = [kmer]   # a list of kmers in the contig

    while True:
        # sum produces the forward degree of the last kmer. if not 1 then it is the end this contig
        if sum(x in kdict for x in forward_kmer_generator(c_fw[-1])) != 1:
            break

        # picks the single k-mer that is the forward neighbor
        cand = [x for x in forward_kmer_generator(c_fw[-1]) if x in kdict][0]
        if cand in c_fw:    # avoid cycles
            break

        # check the forward neighbor has a single backwards neighboer (the last kimer in c_fw)
        if sum(x in kdict for x in backward_kmer_generator(cand)) != 1:
            break

        c_fw.append(cand)

    return c_fw

'''
merge kmers into contigs in the de Bruijn graph in both directions
@kdict: the dictionary with keys are the colleciton of the kmers
@kmer: a starting kmer
'''
def get_contig(kdict, km):
    c_fw = contig_forward(kdict, km)    # forward neighbors
    c_bw = contig_forward(kdict, reverse_complement(km))    #backworkd neighbors

    kmers = [reverse_complement(x) for x in c_bw[-1:0:-1]] + c_fw
    contig = kmers[0] + ''.join(x[-1] for x in kmers[1:])       #create the contig out of the component kmers
    return contig, kmers

def all_contigs(kdict):
    used_kmers = set()  # use set here is much faster than list !
    contigs = []
    for x in kdict:
        if x not in used_kmers:
            contig, kmers = get_contig(kdict, x)
            for y in kmers:
                used_kmers.add(y)
                used_kmers.add(reverse_complement(y))
            contigs.append(contig)
    return contigs, used_kmers

if __name__ == '__main__':
    'unit test'

    fname = '/global/projectb/sandbox/rqc/syao/pipelineTests/jigsaw/data/6740.1.52790.GCACTT.iso.10000reads.fastq'

    #pe, len1, len2 = read_type_length(fname)
    #print('pe=%s; len1=%d, len2=%d' % (str(pe), len1, len2))

    #seq = 'GGCCGCTGAGGGTGACGCGGCATGCT'
    seq = 'CCCGCGCGCGCTGGGGCCCGGCGCGCCCTGGTGGGCGCGCTCGCGCTCTGTTCGGCGCCCATCGACCGCG'
    print('TEST RC seq')
    print('f:%s' % seq)
    print('r:%s' % reverse_complement(seq))
    exit(0)

    print('TEST forward and backword kmer extension generators:')
    print('  %s' % seq)
    for x in forward_kmer_generator(seq):
        print('   %s' % x)

    for x in backward_kmer_generator(seq):
        print(' %s' % x)


    d = {'CGGCCGCTG':2,
         'GCCGCTGAC':1,
         'GCCGCTGAT':2,
         'CCGCTGACA':1,
         'GGCCGCTGA':1,
         }
    c_fw = ['GGCCGCTGA']
    val = [x in d for x in forward_kmer_generator(c_fw[-1])]

    print(val)


