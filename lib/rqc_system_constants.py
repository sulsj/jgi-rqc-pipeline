#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Constants for RQC Framework
- mostly for *_cv tables
- things which can change per user are in configuration/rqc_system.config
- things which can change dynamically should be in the database tables


"""


# Constants for command line binaries
class RQCBinConstants():

    # should use "load module python", #! /usr/bin/env python

    GUNZIP_BIN = "/bin/gunzip"
    CP_BIN = "/bin/cp"
    UNPIGZ_BIN = "module load pigz;unpigz"

    # genepool/sge/uge settings
    CLUSTER_SETTINGS_BIN = "source /opt/uge/genepool/uge/genepool/common/settings.sh"
    QSUB_BIN = "/opt/uge/genepool/uge/bin/lx-amd64/qsub"
    QDEL_BIN = "/opt/uge/genepool/uge/bin/lx-amd64/qdel"

    #IS_JOB_COMPLETE_BIN = "/usr/common/usg/bin/isjobcomplete"
    IS_JOB_COMPLETE_BIN = "/usr/common/usg/bin/isjobcomplete.new"
    QQACCT_BIN = "/usr/common/usg/bin/qqacct"


# Constants for status updates to ITS
class ITSConstants():

    ANALYSIS_TASK_IN_PROGRESS = "Automatic Analysis In Progress"
    ANALYSIS_TASK_COMPLETE = "Automatic Analysis Complete"

    SEGMENT_TASK_IN_PROGRESS = "Rolling QC In Progress"
    SEGMENT_TASK_COMPLETE = "Rolling QC Complete"

    # new - replaces analysis and segment status
    SOW_ITEM_TASK_IN_PROGRESS = "RQC In Progress"
    SOW_ITEM_TASK_COMPLETE = "Awaiting QA/QC Analysis"

# Constants for RQC_Task_List_Status_Cv
class RQCTaskStatusConstants():

    QUEUED = 0
    SUBMITTED = 1
    RUNNING = 2
    FINISHED = 3
    COMPLETE = 4
    FAILED = 5
    TASK_FAILED = 6
    ON_HOLD = 7
    KILLED = 8


# Constants for seq_unit_file.file_type
class RQCSeqUnitFileConstants():

    ILLUMINA_COMPRESSED_RAW_FASTQ = "ILLUMINA COMPRESSED RAW FASTQ"

    # Pacbio
    PACBIO_RAW_FASTQ = "PACBIO RAW FASTQ"
    PACBIO_HDF5_FILE = "PACBIO HDF5"
    PACBIO_RAW_FASTA = "PACBIO FASTA"

    # RQC File types (usable by RQC pipelines)
    RAW_FASTQ = "RAW FASTQ"
    FILTERED_FASTQ = "FILTERED FASTQ"
    COMPRESSED_FILTERED_FASTQ = "COMPRESSED FILTERED FASTQ"

    # ReadQC PDF Report for Portal
    READQC_PDF_REPORT = "READQC PDF REPORT"

    # Misc file types (not used)
    SRF = "SRF"
    SAM = "SAM"
    BAM = "BAM" # Binary SAM
    FASTQ = "FASTQ"

# Constants for seq_unit_status_cv
class RQCSeqUnitConstants():

    REGISTERED = 1
    REGISTRATION_FAILED = 2
    ON_HOLD = 3
    CANCEL = 4
    SETUP_IN_PROGRESS = 5
    TRANSFER_READY = 6
    TRANSFER_SUBMITTED = 7
    TRANSFER_IN_PROGRESS = 8
    TRANSFER_COMPLETE = 9
    TRANSFER_FAILED = 10
    SETUP_PIPELINE_IN_PROGRESS = 11
    SETUP_COMPLETE = 12
    SETUP_FAILED = 13
    RQC_IN_PROGRESS = 14
    RQC_PARTIALLY_COMPLETE = 15
    RQC_COMPLETE = 16
    RQC_FAILED = 17
    LEGACY = 18
    REPROCESS = 19

# Constants for rqc_pipeline_type_cv
class RQCPipelineTypeConstants():

    SETUP = 1
    TEST_PIPELINE = 2

    READ_QC = 3
    ASSEMBLY_QC = 4
    FILTERING = 5
    ALIGNMENT = 6
    FUNGAL_INTEGRITY = 7

    JIGSAW_SINGLE_CELL = 8
    JIGSAW_ISOLATE = 9
    HACKSAW = 10

    PACBIO = 11 # OFF
    PHIX = 12
    METAGENOME = 13
    HEISENBERG = 14
    CLRS_REDUNDANCY = 15
    METATRANSCRIPTOME_FILTER = 16
    PACBIO_READ_QC = 17
    METATRANSCRIPTOME = 18
    FUNGAL_MIN = 19
    CELL_ENRICHMENT = 20
    MULTIALIGNMENT = 21
    JIGSAW_SAG_1 = 22
    JIGSAW_SAG_2 = 23
    LMP_INSERT_SIZE = 24
    SAG_DECONTAMINATION = 25
    FALCON = 26
    ITAGS = 27
    SAG_ASM = 28
    SAG_POOL = 29
    BLAST_ON_DEMAND = 39


# Constants for rqc_pipeline_status_cv.rqc_pipeline_status_id
class RQCPipelineStatusConstants():

    READY = 1
    SUBMITTED = 2
    IN_PROGRESS = 3
    COMPLETE = 4
    FAILED = 5
    ON_HOLD = 6
    CANCELLED = 7
    POST_PROCESS_READY = 8
    POST_PROCESS_SUBMITTED = 9
    POST_PROCESS_IN_PROGRESS = 10
    POST_PROCESS_COMPLETE = 11
    POST_PROCESS_FAILED = 12
    PIPELINE_FAILED = 13
    PIPELINE_COMPLETE = 14
    RESET = 15
    RESET_FAILED = 16
    STATS_READY = 17
    STATS_IN_PROGRESS = 18
    STATS_FAILED = 19
    DATASTORE_READY = 20
    DATASTORE_FAILED = 21
    MANUAL_COMPLETE = 22

# For readqc.py
class RQCStatTable():

    STATS_TABLE = {

        ##
        ## JGI_Analysis_Utility_Illumina.pm
        ##
        "ILLUMINA_READ":"read level"
        ,"ILLUMINA_READ_Q":"read Q"
        ,"ILLUMINA_BASE_Q":"base Q"
        ,"ILLUMINA_BASE_C":"base C"
        ,"ILLUMINA_READ_Q20_READ1":"read q20 read1"
        ,"ILLUMINA_READ_Q20_READ2":"read q20 read2"
        ,"ILLUMINA_READ_QHIST_TEXT":"read qhist text"
        ,"ILLUMINA_READ_QHIST_PLOT":"read qhist plot"
        ,"ILLUMINA_READ_QUAL_POS_PLOT_1":"read qual pos plot 1"
        ,"ILLUMINA_READ_QUAL_POS_PLOT_2":"read qual pos plot 2"
        ,"ILLUMINA_READ_QUAL_POS_PLOT_MERGED":"read qual pos plot merged"
        ,"ILLUMINA_READ_QUAL_POS_QRPT_1":"read qual pos qrpt 1"
        ,"ILLUMINA_READ_QUAL_POS_QRPT_2":"read qual pos qrpt 2"

        ,"ILLUMINA_FAKE_READ_QUAL_POS_PLOT":"fake read qual pos plot"
        ,"ILLUMINA_FAKE_READ_QUAL_POS_QRPT":"fake read qual pos qrpt"

        ,"ILLUMINA_READ_21MER_TEXT":"read 21mer window sliding text data"
        ,"ILLUMINA_READ_21MER_PLOT":"read 21mer window sliding plot"
        ,"ILLUMINA_READ_BASE_COUNT_TEXT_1":"read base count text 1"
        ,"ILLUMINA_READ_BASE_COUNT_TEXT_2":"read base count text 2"
        ,"ILLUMINA_READ_BASE_COUNT_PLOT_1":"read base count plot 1"
        ,"ILLUMINA_READ_BASE_COUNT_PLOT_2":"read base count plot 2"


        ,"ILLUMINA_READ_BASE_PERCENTAGE_TEXT_1":"read base percentage text 1"
        ,"ILLUMINA_READ_BASE_PERCENTAGE_TEXT_2":"read base percentage text 2"
        ,"ILLUMINA_READ_BASE_PERCENTAGE_PLOT_1":"read base percentage plot 1"
        ,"ILLUMINA_READ_BASE_PERCENTAGE_PLOT_2":"read base percentage plot 2"
        ,"ILLUMINA_READ_20MER_SAMPLE_SIZE":"read 20mer sample size"
        ,"ILLUMINA_READ_20MER_PERCENTAGE_STARTING_MERS":"read 20mer percentage starting mers"
        ,"ILLUMINA_READ_20MER_PERCENTAGE_RANDOM_MERS":"read 20mer percentage random mers"
        ,"ILLUMINA_READ_20MER_UNIQUENESS_TEXT":"read 20mer uniqueness text"
        ,"ILLUMINA_READ_20MER_UNIQUENESS_PLOT":"read 20mer uniqueness plot"
        ,"ILLUMINA_READ_BWA_ALIGNED":"read bwa aligned"
        ,"ILLUMINA_READ_BWA_ALIGNED_DUPLICATE":"read bwa aligned duplicate"
        ,"ILLUMINA_READ_BWA_ALIGNED_DUPLICATE_PERCENT":"read bwa aligned duplicate percent"
        ,"ILLUMINA_READ_SCREENED":"read screend"
        ,"ILLUMINA_READ_ARTIFACT":"read artifact"
        ,"ILLUMINA_READ_ARTIFACT_PERCENT":"read artifact percent"
        ,"ILLUMINA_READ_TAGDUST_COVERAGE_CUTOFF":"read tagdust coverage cutoff"
        ,"ILLUMINA_READ_N_FREQUENCE":"read N frequence"
        ,"ILLUMINA_READ_N_PATTERN":"read N pattern"
        ,"ILLUMINA_READ_GC_MEAN":"read GC mean"
        ,"ILLUMINA_READ_GC_STD":"read GC std"
        ,"ILLUMINA_READ_GC_PLOT":"read GC plot"
        ,"ILLUMINA_READ_GC_TEXT":"read GC text hist"
        ,"ILLUMINA_READ_LENGTH_1":"read length 1"
        ,"ILLUMINA_READ_LENGTH_2":"read length 2"
        ,"ILLUMINA_READ_BASE_COUNT":"read base count"
        ,"ILLUMINA_READ_COUNT":"read count"

        ,"ILLUMINA_READ_TOPHIT_FILE":"read tophit file of"
        ,"ILLUMINA_READ_TAXLIST_FILE":"read taxlist file of"
        ,"ILLUMINA_READ_TAX_SPECIES":"read tax species of"
        ,"ILLUMINA_READ_TOP_HITS":"read top hits of"
        ,"ILLUMINA_READ_PARSED_FILE":"read parsed file of"
        ,"ILLUMINA_READ_MATCHING_HITS":"read matching hits of"
        ,"ILLUMINA_READS_NUMBER":"reads number" ## sampled num reads


        ##
        ## JGI_Constants.pm
        ##
        ,"ILLUMINA_CLIP_TRIM_READ_LEN_TXT":"clip trim read length text"
        ,"ILLUMINA_CLIP_TRIM_READ_LEN_PLOT":"clip trim read length plot"
        ,"ILLUMINA_CLIP_TRIM_READ1_ONLY":"clip trim read1 only"
        ,"ILLUMINA_CLIP_TRIM_READ2_ONLY":"clip trim read2 only"
        ,"ILLUMINA_CLIP_TRIM_BOTH_READS":"clip trim both reads"
        ,"ILLUMINA_CLIP_TRIM_NEITHER_READ":"clip trim neither read"
        ,"ILLUMINA_CLIP_TRIM_UNKNOWN":"clip trim unknown"
        ,"ILLUMINA_CLIP_TRIM_TOTAL_READS":"clip trim total reads"
        ,"ILLUMINA_READ_DEMULTIPLEX_FILTERED_STATS":"demultiplex filtered stats"
        ,"ILLUMINA_READ_DEMULTIPLEX_STATS":"demultiplex stats"
        ,"ILLUMINA_READ_DEMULTIPLEX_STATS_PLOT":"demultiplex stats plot"

        ,"ILLUMINA_READ_BASE_QUALITY_STATS":"read base quality stats"
        ,"ILLUMINA_READ_BASE_QUALITY_STATS_PLOT":"read base quality stats plot"

        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_ARTIFACT":"illumina read percent contamination artifact"
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_ARTIFACT_50BP":"illumina read percent contamination artifact 50bp" ## 20131203 RQC-319
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_DNA_SPIKEIN":"illumina read percent contamination DNA spikein"
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_RNA_SPIKEIN":"illumina read percent contamination RNA spikein"
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_CONTAMINANTS":"illumina read percent contamination contaminants"
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_ECOLI_B":"illumina read percent contamination ecoli b"
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_ECOLI_K12":"illumina read percent contamination ecoli k12"
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_ECOLI_COMBINED":"illumina read percent contamination ecoli combined"
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_FOSMID":"illumina read percent contamination fosmid"
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_MITOCHONDRION":"illumina read percent contamination mitochondrion"
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_PLASTID":"illumina read percent contamination plastid"
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_PHIX":"illumina read percent contamination phix"
        ,"ILLUMINA_READ_PERCENT_CONTAMINATION_RRNA":"illumina read percent contamination rrna"
        ,"ILLUMINA_READ_SCICLONE_DNA_COUNT_FILE":"illumina read sciclone DNA count file"
        ,"ILLUMINA_READ_SCICLONE_RNA_COUNT_FILE":"illumina read sciclone RNA count file"
        ,"ILLUMINA_READ_SCICLONE_RNA_STRAND_INPUT_READ":"illumina read sciclone RNA strand input read"
        ,"ILLUMINA_READ_SCICLONE_RNA_STRAND_SENSE_READ":"illumina read sciclone RNA strand sense read"
        ,"ILLUMINA_READ_SCICLONE_RNA_STRAND_ANTISENSE_READ":"illumina read sciclone RNA strand mapped read"
        ,"ILLUMINA_READ_SCICLONE_RNA_STRAND_MAPPED_READ":"illumina read sciclone RNA strand mapped read"
        ,"ILLUMINA_READ_SCICLONE_RNA_STRAND_SENSE_PCT":"illumina read sciclone RNA strand sense percent"

        ,"ILLUMINA_BASE_Q30":'base Q30'
        ,"ILLUMINA_BASE_Q25":'base Q25'
        ,"ILLUMINA_BASE_Q20":'base Q20'
        ,"ILLUMINA_BASE_Q15":'base Q15'
        ,"ILLUMINA_BASE_Q10":'base Q10'
        ,"ILLUMINA_BASE_Q5":'base Q5'

        ,"ILLUMINA_BASE_C30":'base C30'
        ,"ILLUMINA_BASE_C25":'base C25'
        ,"ILLUMINA_BASE_C20":'base C20'
        ,"ILLUMINA_BASE_C15":'base C15'
        ,"ILLUMINA_BASE_C10":'base C10'
        ,"ILLUMINA_BASE_C5":'base C5'

        ,"ILLUMINA_READ_Q30":'read Q30'
        ,"ILLUMINA_READ_Q25":'read Q25'
        ,"ILLUMINA_READ_Q20":'read Q20'
        ,"ILLUMINA_READ_Q15":'read Q15'
        ,"ILLUMINA_READ_Q10":'read Q10'
        ,"ILLUMINA_READ_Q5":'read Q5'

        ## sulsj
        ,"ILLUMINA_BASE_Q30_SCORE_MEAN":'Q30 bases Q score mean'
        ,"ILLUMINA_BASE_Q30_SCORE_STD":'Q30 bases Q score std'
        ,"ILLUMINA_BASE_OVERALL_BASES_Q_SCORE_MEAN":'overall bases Q score mean'
        ,"ILLUMINA_BASE_OVERALL_BASES_Q_SCORE_STD":'overall bases Q score std'

        ,"ILLUMINA_READ_LEVEL_MEGAN_TAXA_DIST":'read level MEGAN: Taxa Distribution of contigs vs NT'
        ,"ILLUMINA_READ_LEVEL_MEGAN_NT":'read level MEGAN: Phylogeny of contigs vs NT'
        ,"ILLUMINA_READ_LEVEL_MEGAN_FILTERED":'read level MEGAN: Phylogeny of contigs vs NT filtered by class'
        ,"ILLUMINA_READ_LEVEL_GC_HISTO":'read level GC Histogram of contigs'

        ##
        ## assemblyqc
        ##
        ,"ILLUMINA_ASSEMBLY":"assembly level"
        ,"ILLUMINA_ASSEMBLY_ASSEMBLER":"assembly assembler"
        ,"ILLUMINA_ASSEMBLY_ASSEMBLER_VERSION":"assembly assembler version"
        ,"ILLUMINA_ASSEMBLY_KMER":"assembly kmer"
        ,"ILLUMINA_ASSEMBLY_COV_CUTOFF":"assembly coverage cutoff"
        ,"ILLUMINA_ASSEMBLY_MIN_CONTIG_LENGTH":"assembly min contig length"
        ,"ILLUMINA_ASSEMBLY_NODE_NUMBER":"assembly node number"
        ,"ILLUMINA_ASSEMBLY_NODE_LENGTH":"assembly all node length"
        ,"ILLUMINA_ASSEMBLY_N50":"assembly n50"
        ,"ILLUMINA_ASSEMBLY_MAX_CONTIG_LENGTH":"assembly max contig length"
        ,"ILLUMINA_ASSEMBLY_READ_NUMBER":"assembly read number"
        ,"ILLUMINA_ASSEMBLY_CONTIG_STATS_SUMMARY":"assembly contig stats summary"
        ,"ILLUMINA_ASSEMBLY_TOP5_CONTIG_FILE":"assembly top 5 contig file"
        ,"ILLUMINA_ASSEMBLY_GC_MEAN":"assembly GC mean"
        ,"ILLUMINA_ASSEMBLY_GC_STD":"assembly GC std"
        ,"ILLUMINA_ASSEMBLY_GC_PLOT":"assembly GC plot"
        ,"ILLUMINA_ASSEMBLY_GC_TEXT":"assembly GC text hist"

        ,"ILLUMINA_ASSEMBLY_DEPTH_PLOT":"assembly depth hist plot"
        ,"ILLUMINA_ASSEMBLY_DEPTH_TEXT":"assembly depth text hist"
        ,"ILLUMINA_ASSEMBLY_GENOME_SIZE":"assembly genome size"
        ,"ILLUMINA_ASSEMBLY_DEPTH_MEAN":"assembly depth mean"
        ,"ILLUMINA_ASSEMBLY_DEPTH_STD":"assembly depth std"

        ,"ILLUMINA_ASSEMBLY_DEPTH_BIN":"assembly depth bin"
        ,"ILLUMINA_ASSEMBLY_CONTIG_NUMBER":"assembly contig number"
        ,"ILLUMINA_ASSEMBLY_MATCHING_HITS":"assembly matching hits of"
        ,"ILLUMINA_ASSEMBLY_TOP_HITS":"assembly top hits of"
        ,"ILLUMINA_ASSEMBLY_TAX_SPECIES":"assembly tax species of"
        ,"ILLUMINA_ASSEMBLY_PARSED_FILE":"assembly parsed file of"
        ,"ILLUMINA_ASSEMBLY_TAXLIST_FILE":"assembly taxlist file of"
        ,"ILLUMINA_ASSEMBLY_TOPHIT_FILE":"assembly tophit file of"
        ,"ILLUMINA_ASSEMBLY_READ_USED":"assembly read used"
        ,"ILLUMINA_READ_LENGTH_1":"read length 1"
        ,"ILLUMINA_READ_LENGTH_2":"read length 2"
        ,"ILLUMINA_CONTIGS_FILESIZE":"assembly contigs filesize"

        ## sulsj
        ,"contig level MEGAN: Phylogeny of contigs vs NT filtered by class":"contig level MEGAN: Phylogeny of contigs vs NT filtered by class"
        ,"contig level MEGAN: Phylogeny of contigs vs NT":"contig level MEGAN: Phylogeny of contigs vs NT"
        ,"contig level MEGAN: Taxa Distribution of contigs vs. NT":"contig level MEGAN: Taxa Distribution of contigs vs. NT"
        ,"contig level tetramer plot":"contig level tetramer plot"
        ,"ILLUMINA_TOO_SMALL_NUM_READS":"ILLUMINA_TOO_SMALL_NUM_READS"
        ,"ILLUMINA_READ_END_OF_READ_ADAPTER_CHECK_PLOT":"ILLUMINA_READ_END_OF_READ_ADAPTER_CHECK_PLOT"
        ,"ILLUMINA_READ_END_OF_READ_ADAPTER_CHECK_DATA":"ILLUMINA_READ_END_OF_READ_ADAPTER_CHECK_DATA"
    }