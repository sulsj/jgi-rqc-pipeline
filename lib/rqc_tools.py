#! /usr/bin/env python
"""
    Convenience methods for rqc web 2.0
    7/4/2014: use rqc_tax.taxtonames database table lookup for ncbi name
    4/23/2014: cell-enrich report link

""" 

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
#import sys
import re
#import MySQLdb
import cx_Oracle # dw connection
import base64
import time
import zipfile
#import pprint

from rqc_system_constants import RQCPipelineTypeConstants
from os_utility import run_sh_command

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## global constants: 
ARCHIVE_PATH = "/global/dna/shared/rqc/archive"

# pipeline name constant, value must be in rqc.rqc_pipeline_type_cv.pipeline_name
READ_QC         = 'Read QC'
READ_QC_PB      = 'PB ReadQC'
ASSEMBLY_QC     = 'Assembly QC'
JIGSAW_SAG      = 'JIGSAW Single Cell'
JIGSAW_SAG2     = 'JIGSAW SAG 2'
JIGSAW_ISO      = 'JIGSAW Isolate'
JIGSAW_AMP_CE   = 'Amp Cell Enrichment'
FILTER          = 'Filter'
ALIGNMENT       = 'Alignment'
MULTI_ALIGN     = 'Multiple Alignment'
PACBIO          = 'PacBio'
PACBIOREADQC    = 'PB ReadQC'
PHIX            = 'PhiX'
METAGENOME      = 'Metagenome'
CLRS_REDUNCANCY = 'CLRS Redundancy'
FUNGAL_MIN      = 'Fungal Min Asm'
METATRANSCRIPTOMEAA = 'Metatranscriptome AA'
MT_FILTER       = 'Metatranscriptome Filter'
LMP_INSERT_SIZE = 'LMP Insert Size'
BLAST_ON_DEMAND = "Blast on demand"


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions


'''
Format to p digits
'''
def format_num(number, format_type = "int", dcnt = 4):

    fnumber = number

    format_style = "{:,}" # int

    if format_type == "pct":
        format_style = "{:.%s%%}" % (dcnt)
        try:
            fnumber = float(number)
        except:
            pass

    elif format_type == "float":
        format_style = "{0:,.%sf}" % (dcnt)

        try:
            fnumber = float(number)
        except:
            pass

    else:
        try:
            fnumber = int(float(number))
        except:
            pass

    try:

        fnumber = format_style.format(fnumber)
    except:
        pass

    #print format_style, fnumber
    return fnumber

'''    
returns human readable file size
@param num = file size (e.g. 1000)
  
@return: readable float e.g. 1.5 KB
'''
def human_size(num):

    if num:
        for x in ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'XB']:
            if (num < 1024.0):
                return "%3.1f %s" % (num, x)
            num /= 1024.0

        return "%3.1f %s" % (num, 'ZB')
    else:
        return "0 bytes"

'''
Return pipeline_queue_id for files & stats
data_id can be a seq_unit_id, seq_unit_name, or library_name, or data_unit_id (with data_unit_flag = True flag)
- some libraries have more than one seq unit (e.g. M0860) 
'''
def get_queue_id(sth, data_id, pipeline_type_id, data_unit_flag = False):

    # rqc_path = "/global/dna/shared/rqc/"
    redirect = False # not used

    #if pipeline_type_id == RQCPipelineTypeConstants.HACKSAW or pipeline_type_id == RQCPipelineTypeConstants.HEISENBERG
    #or pipeline_type_id == RQCPipelineTypeConstants.FALCON or pipeline_type_id == RQCPipelineTypeConstants.ITAGS:
    if data_unit_flag:

        sql = "select d.data_name as seq_unit_name, d.str_param2 as library_name, rpq.rqc_pipeline_queue_id, rpq.fs_location from data_units d inner join rqc_pipeline_queue rpq on rpq.data_unit_id = d.data_unit_id"

        if data_id.isdigit():
            sql += " where d.data_id = %s and rpq.rqc_pipeline_type_id = %s"

        else:
            sql += " where d.str_param2 = %s and rpq.rqc_pipeline_type_id = %s"

        sql += " and d.owner = 'rqc'"

    else:

        sql = "select s.seq_unit_name, s.library_name, rpq.rqc_pipeline_queue_id, rpq.fs_location from seq_units s inner join rqc_pipeline_queue rpq on rpq.seq_unit_id = s.seq_unit_id"

        if data_id.endswith(".fastq.gz") or data_id.endswith(".fastq"):
            sql += " where s.seq_unit_name = %s and rpq.rqc_pipeline_type_id = %s"

        elif data_id.isdigit():
            sql += " where s.seq_unit_id = %s and rpq.rqc_pipeline_type_id = %s"

        else:
            sql += " where s.library_name = %s and rpq.rqc_pipeline_type_id = %s"

    #print('DEBUG sql=%s [%s]' % (sql, data_id))
    #print('DEBUG id=%d' % pipeline_type_id)
    sth.execute(sql, (data_id, pipeline_type_id))

    queue_id = 0
    seq_unit_name = None
    library_name = None
    run_folder = None

    rs = sth.fetchone()
    #print('DEBUG %s\n\n' % str(rs))
    if rs:
        queue_id = rs['rqc_pipeline_queue_id']
        seq_unit_name = rs['seq_unit_name']
        library_name = rs['library_name']

        run_folder = clean_rqc_path(rs['fs_location']) # mask out full path for rqc

    else:
        # convert analysis_type_id
        analysis_type_id = 0
        if pipeline_type_id == RQCPipelineTypeConstants.READ_QC:
            analysis_type_id = 1 # read
        elif pipeline_type_id == RQCPipelineTypeConstants.ASSEMBLY_QC:
            analysis_type_id = 2
        elif pipeline_type_id == RQCPipelineTypeConstants.ALIGNMENT:
            analysis_type_id = 3
        elif pipeline_type_id == RQCPipelineTypeConstants.JIGSAW_SINGLE_CELL:
            analysis_type_id = 4
        elif pipeline_type_id == RQCPipelineTypeConstants.JIGSAW_ISOLATE:
            analysis_type_id = 5
        elif pipeline_type_id == RQCPipelineTypeConstants.FILTERING:
            analysis_type_id = 10
        elif pipeline_type_id == RQCPipelineTypeConstants.BLAST_ON_DEMAND:
            analysis_type_id = 39
        else:
            analysis_type_id = 0

        # try the legacy data
        sql = "select s.seq_unit_name, s.library_name, a.analysis_id, a.analysis_dir from m2m_analysis_seq_unit m inner join rqc_analysis a on m.analysis_id = a.analysis_id inner join seq_units s on m.seq_unit_name = s.seq_unit_name"

        if data_id.endswith(".srf"):
            sql += " where s.seq_unit_name = %s and a.analysis_type_id = %s"

        elif data_id.isdigit():
            sql += " where s.seq_unit_id = %s and a.analysis_type_id = %s"

        else:
            sql += " where s.library_name = %s and a.analysis_type_id = %s"

        sth.execute(sql, (data_id, analysis_type_id))

        rs = sth.fetchone()

        if rs:
            queue_id = -1 * int(rs['analysis_id']) # set to negative so get_pipeline_files/stats pulls from correct tables
            seq_unit_name = rs['seq_unit_name']
            library_name = rs['library_name']
            run_folder = clean_rqc_path(rs['analysis_dir'])




        # redirect to error page - this will break if you are on /api/(app)/(method) instead of /(app)/(method)
        #print "q = %s" % queue_id
        if queue_id == 0:

            link = "/error/404/%s" % (data_id)
            #link = "/error/404/"
            if redirect == True:
                raise cherrypy.HTTPRedirect(link)
                exit(0)

    return queue_id, seq_unit_name, library_name, run_folder

'''
Returns the file type and full file path
- trims out the /path/to/archive
'''
def get_pipeline_files(sth, **kwargs): 

    data_dict = {}

    prod_version = 1
    queue_id = 0 # *.fastq.gz
    analysis_id = 0 # *.srf

    if 'queue_id' in kwargs:
        queue_id = kwargs['queue_id']

    if 'analysis_id' in kwargs:
        analysis_id = kwargs['analysis_id']

    if 'prod_version' in kwargs:
        prod_version = kwargs['prod_version']

    if queue_id == 0 and analysis_id == 0:
        return data_dict


    if queue_id < 0:
        analysis_id = abs(queue_id)
        queue_id = 0


    if queue_id > 0:
        sql = "select file_type, concat(fs_location,'/',file_name) as fname, file_name, file_size from rqc_pipeline_files where rqc_pipeline_queue_id = %s and is_production = %s"
        sth.execute(sql, (queue_id, prod_version))

    elif analysis_id > 0:
        sql = "select file_description as file_type, concat(fs_location,'/',file_name) as fname, file_name, file_size from rqc_analysis_files where analysis_id = %s"
        sth.execute(sql, (analysis_id))

    row_cnt = int(sth.rowcount)

    for i in range(row_cnt):
        rs = sth.fetchone()
        # removed \. because {{ data.file.keyword_with._in_it_fails }}


        rs['file_type'] = re.sub('[^A-Za-z0-9]+', '_', rs['file_type'])
        rs['fname'] = re.sub(ARCHIVE_PATH, 'dna', rs['fname'])
        #data_dict[str(rs['file_type']).lower()] = rs['fname']
        #data_dict[str(rs['file_type']).lower()+"_size"] = rs['file_size']
        key = str(rs['file_type']).lower()

        data_dict[key] = { "file" : rs['fname'], "size" : human_size(rs['file_size']), "base_name" : rs['file_name'] }


    return data_dict


'''
Returns the stats_name, value dictionary for a pipeline queue id

get_pipeline_stats(sth, queue_id = x)

'''
def get_pipeline_stats(sth, **kwargs):

    prod_version = 1
    queue_id = 0 # *.fastq.gz
    analysis_id = 0 # *.srf
    data_dict = {}

    if 'queue_id' in kwargs:
        queue_id = kwargs['queue_id']

    if 'analysis_id' in kwargs:
        analysis_id = kwargs['analysis_id']

    if 'prod_version' in kwargs:
        prod_version = kwargs['prod_version']

    if queue_id == 0 and analysis_id == 0:
        return data_dict

    if queue_id < 0:
        analysis_id = abs(queue_id)
        queue_id = 0

    if queue_id > 0:
        sql = "select stats_name, stats_value from rqc_pipeline_stats where rqc_pipeline_queue_id = %s and is_production = %s"
        sth.execute(sql, (queue_id, prod_version))

    elif analysis_id > 0:
        sql = "select stats_name, stats_value from rqc_analysis_stats where analysis_id = %s"
        sth.execute(sql, (analysis_id))


    #row_cnt = int(sth.rowcount)

    #for i in range(row_cnt):
    #    rs = sth.fetchone()
    #    # removed \. because {{ data.file.keyword_with._in_it_fails }}
    #    rs['stats_name'] = re.sub('[^A-Za-z0-9]+', '_', rs['stats_name'])

    #    data_dict[str(rs['stats_name']).lower()] = rs['stats_value']

    rows = sth.fetchall() # fetch em all

    for rs in rows:
        rs['stats_name'] = re.sub('[^A-Za-z0-9]+', '_', rs['stats_name'])
        data_dict[str(rs['stats_name']).lower()] = rs['stats_value']

    return data_dict


'''
Return the link to the report for the seq unit based on the pipeline id
link = get_pipeline_link(seq_unit_name = seq_unit_name, pipeline_type_id = 3)
'''

def get_pipeline_link(**kwargs): #seq_unit_name, pipeline_type_id, lib = None):

    seq_unit_name = None
    pipeline_type_id = 0
    library_name = None
    data_name = None

    if 'seq_unit_name' in kwargs:
        seq_unit_name = kwargs['seq_unit_name']
    if 'pipeline_type_id' in kwargs:
        pipeline_type_id = kwargs['pipeline_type_id']
    if 'library_name' in kwargs:
        library_name = kwargs['library_name']
    if 'data_name' in kwargs:
        data_name = kwargs['data_name']

    link = "/" # home
    if seq_unit_name:
        #link = "/readqc/report/%s" % (seq_unit_name)
        link = "/fullreport/report/%s" % (seq_unit_name)


    if pipeline_type_id > 0:
        if pipeline_type_id == RQCPipelineTypeConstants.JIGSAW_SINGLE_CELL \
                       or pipeline_type_id == RQCPipelineTypeConstants.JIGSAW_ISOLATE \
                       or pipeline_type_id == RQCPipelineTypeConstants.CELL_ENRICHMENT \
                       or pipeline_type_id == RQCPipelineTypeConstants.JIGSAW_SAG_1 \
                       or pipeline_type_id == RQCPipelineTypeConstants.JIGSAW_SAG_2:
            link = "/jigsaw/report/%s" % (seq_unit_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.SAG_ASM:
                    link = "/sag-asm/report/%s" % (seq_unit_name)
                
        elif pipeline_type_id == RQCPipelineTypeConstants.READ_QC:
            link = "/readqc/report/%s" % (seq_unit_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.PACBIO:

            # /pacbioqc/report/SEQ_UNIT_NAME/SECTION
            link = "/pacbioqc/report/%s" % (seq_unit_name)

            if library_name:
                link += "/%s" % (library_name)
            link += "/"

        elif pipeline_type_id == RQCPipelineTypeConstants.PACBIO_READ_QC:
            link = "/pacbioreadqc/report/%s" % (seq_unit_name)

            if library_name:
                link += "/%s" % (library_name)

        #pacbioreadqc/report/pbio-806.7580.fastq
        elif pipeline_type_id == RQCPipelineTypeConstants.PHIX:
            link = "/phix/report/%s" % (seq_unit_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.METAGENOME:
            link = "/metagenome/report/%s" % (seq_unit_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.ASSEMBLY_QC:
            #link += "#h-2.2"
            link = "/assemblyqc/report/%s" % (seq_unit_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.ALIGNMENT:
            #link += "#h-4.1"
            link = "/alignment/report/%s" % (seq_unit_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.FILTERING:
            #link += "#h-0.0.0"
            link = "/filteredfastq/report/%s" % (seq_unit_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.CLRS_REDUNDANCY:
            link = "/clrs_redundancy/report/%s" % (seq_unit_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.HACKSAW:
            link = "/hacksaw/report/%s" % (library_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.HEISENBERG:
            link = "/heisenberg/report/%s" % (library_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.SAG_DECONTAMINATION:
            link = "/sag_decontam/report/%s" % (data_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.FUNGAL_MIN:
            link = "/fungal_min/report/%s" % (library_name)

        # multiple alignment link
        elif pipeline_type_id == RQCPipelineTypeConstants.MULTIALIGNMENT:
            link = "/multi_align/report/%s" % (seq_unit_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.LMP_INSERT_SIZE:
            #link = "/lmp_insert_size/report/%s" % (library_name)
            # using seq_unit because one library can be run multiple times
            link = "/lmp_insert_size/report/%s" % (seq_unit_name)

        elif pipeline_type_id == RQCPipelineTypeConstants.FALCON:
                link = "/falcon/report/%s" % (library_name)
        elif pipeline_type_id == RQCPipelineTypeConstants.ITAGS:
                link = "/itags/report/%s" % (library_name)
                
        # elif pipeline_type_id == RQCPipelineTypeConstants.BLAST_ON_DEMAND:
        #     link = "/blast_on_demand/report/%s" % (seq_unit_name)

    return link



# hide full path
# matches .bashrc.ext exports
def clean_rqc_path(my_path):

    rqc_path_dict = {
        "$RQC_SCRATCH/" : "/global/projectb/scratch/qc_user/rqc/prod/",
        "$RQC_DNA/" : "/global/dna/projectdirs/PI/rqc/prod/",
        "$RQC_SHARED/" : "/global/dna/shared/rqc/",
        "$RQC_SANDBOX/" : "/global/projectb/sandbox/rqc/",
        "$SDM_FASTQ/" : "/global/dna/dm_archive/sdm/illumina/",
        "$SDM_FILTERED_FASTQ/" : "/global/dna/dm_archive/rqc/filtered_seq_unit/",
        "$BRYCE_SCRATCH/" : "/global/projectb/sandbox/rqc/analysis/brycef/"
    }


    my_path = str(my_path)
    for key in rqc_path_dict:
        my_path = my_path.replace(rqc_path_dict[key], key)


    return my_path

# remove non-standard ascii chars that crash the jinja template
def clean_str(my_str):

    #re.sub("[^A-Za-z0-9\ \-\:\.]+", " ", row['ncbi_organism_name'])
    if my_str:
        if type(my_str) is str or type(my_str) is unicode:
            my_str = my_str.encode('ascii', 'ignore').decode('ascii')

            my_str = "".join(c for c in my_str if ord(c) >=32 and ord(c) <=127)

    return my_str


def value_or_unknown_str(value):

    if not value:
        value = "n.a."
    else:
        value = clean_str(value)

    return value

'''--------------------------------------------------------
return dict of basic project info for a given seq unit name
'''
def get_proj_info_meta(sqlObj, seqUnitName):
    sql = '''SELECT
        L.account_jgi_user_prog,
        L.library_name,
        L.genus,
        L.species,
        L.strain,
        L.isolate,
        L.ncbi_tax_id,
        L.seq_proj_name,
        L.seq_proj_id,
        L.proposal_id
        FROM library_info L INNER JOIN seq_units S ON L.library_id = S.rqc_library_id
        WHERE S.seq_unit_name=%s
    '''

    rows = sqlObj.query(sql, (seqUnitName,))
    metaDict = None
    if rows:
        metaDict = rows[0]
        metaDict['ncbi_tax_id'] = int(metaDict['ncbi_tax_id'])

        metaDict['account_jgi_user_prog'] = value_or_unknown_str(metaDict['account_jgi_user_prog'])
        metaDict['library_name'] = value_or_unknown_str(metaDict['library_name'])
        metaDict['genus'] = value_or_unknown_str(metaDict['genus'])
        metaDict['species'] = value_or_unknown_str(metaDict['species'])
        metaDict['strain'] = value_or_unknown_str(metaDict['strain'])
        metaDict['isolate'] = value_or_unknown_str(metaDict['isolate'])
        metaDict['seq_proj_name'] = value_or_unknown_str(metaDict['seq_proj_name'])
        metaDict['seq_proj_id'] = int(metaDict['seq_proj_id'])

        metaDict['proposal_id'] = value_or_unknown_str(metaDict['proposal_id'])
        metaDict['proposal_id'] = int(metaDict['proposal_id'])

        # replace with something else, we have the taxonomy in the library_info.tax* fields
        aid = metaDict['ncbi_tax_id']
        if aid:
            metaDict['ncbi_organism_name'] = lowest_known_ncbi_name_for_taxid(aid)
        else:
            metaDict['ncbi_organism_name'] = '(taxid not assigned)'

    return metaDict

def lowest_known_ncbi_name_for_taxid(taxid):
    name = 'No Tax ID assigned'
    taxid = int(taxid)
    if taxid:
        exe = 'module load jigsaw; $JIGSAW_DIR/external_tools/taxMapper/DEFAULT/lookupTax.pl'
        cmd = '%s -lf -taxid %d' % (exe, taxid)

        stdOut, stdErr, exitCode = run_sh_command(cmd, True)
        if exitCode != 0:
            name = 'No NCBI name found for ID=%d' % taxid
        else:
            tok = stdOut.split(';')
            tok.reverse()
            for m in tok:	# return the lowest tax name
                m = m.strip()
                if m != '':
                    name = m
                    break;
    return name

'''---------------------------------------------------
return the location of a pipeline job for a seq unit name
'''
def get_fs_location(sqlObj, seqUnitName, pipelineTypeId):
    sql = '''
        SELECT Q.fs_location
        from rqc_pipeline_queue Q
        inner join seq_units S on S.seq_unit_id = Q.seq_unit_id
        WHERE
        seq_unit_name = %s AND
        Q.rqc_pipeline_type_id = %s
    '''
    rows = sqlObj.query(sql, (seqUnitName, pipelineTypeId))
    if rows:
        return rows[0]['fs_location']
    else:
        return None


'''---------------------------------------------------
return boolean for if a pipeline produced file exists in RQC database, for the given pipeline queue ID and the file name
'''
def has_pipeline_file(sqlObj, pQueueId, fName):
    'seqObj is an instance of MySQLRestful that provide the query() function'
    sql = 'select fs_location, file_name from rqc_pipeline_files where is_production = 1 and rqc_pipeline_queue_id = %s and file_name like %s'
    rows = sqlObj.query(sql, (pQueueId, '%' + fName))     #file name got pre-fixed in database
    return len(rows) > 0


'''---------------------------------------------------
return the full path of a given RQC pipeline produced file, by given the pipeline queue ID and the file name
'''
def get_full_path_by_fname(sqlObj, pQueueId, fName):
    'seqObj is an instance of MySQLRestful that provide the query() function'
    sql = 'select fs_location, file_name from rqc_pipeline_files where is_production = 1 and rqc_pipeline_queue_id = %s and file_name like %s'
    rows = sqlObj.query(sql, (pQueueId, '%' + fName))     #file name got pre-fixed in database
    if rows:
        return os.path.join(rows[0]['fs_location'], rows[0]['file_name'])
    else:
        return None

def get_full_path_by_key(sqlObj, pQueueId, kName):
    'seqObj is an instance of MySQLRestful that provide the query() function'
    sql = 'select fs_location, file_name from rqc_pipeline_files where is_production = 1 and rqc_pipeline_queue_id = %s and file_type = %s'
    rows = sqlObj.query(sql, (pQueueId, kName))
    if rows:
        return os.path.join(rows[0]['fs_location'], rows[0]['file_name'])
    else:
        return None

def get_pipeline_stats_value_by_key(sqlObj, pQueueId, kName):
    'seqObj is an instance of MySQLRestful that provide the query() function'
    sql = 'SELECT stats_value FROM rqc_pipeline_stats WHERE is_production = 1 and rqc_pipeline_queue_id = %s and stats_name = %s'
    rows = sqlObj.query(sql, (pQueueId, kName))
    if rows:
        return rows[0]['stats_value']
    else:
        return None


def get_seq_unit_name_by_lib_name(sqlObj, libName):
    'seqObj is an instance of MySQLRestful that provide the query() function'
    sql = '''
SELECT su.seq_unit_name
FROM seq_units su
WHERE
    su.library_name = %s
'''
    rows = sqlObj.query(sql, (libName,))
    su = None
    if rows:
        # use .fastq.gz or .fastq name, if more names exist
        for row in rows:
            item = row['seq_unit_name']
            if item.endswith('.fastq.gz') or item.endswith('.fastq'):
                su = item
                break

        # if no .fastq.gz or .fastq name exist, use the 1st one found
        if not su:
            su = rows[0]['seq_unit_name']

    return su

def get_lib_name_by_seq_unit_name(sqlObj, setUnitName):
    'seqObj is an instance of MySQLRestful that provide the query() function'
    sql = '''
SELECT l.library_name l_name, s.library_name s_name
FROM seq_units s
LEFT JOIN library_info l ON s.rqc_library_id = l.library_id
WHERE s.seq_unit_name = %s
'''
    rows = sqlObj.query(sql, (setUnitName,))
    libname = None
    if rows:
        if rows[0]['l_name']:
            libname = rows[0]['l_name']
        elif rows[0]['s_name']:
            libname = rows[0]['s_name']
    return libname

'''---------------------------------------------------
for a given pipeline file with full path, return the encoded path
'''
def get_encoded_pipeline_file(pfilePath):
    'for a given pipeline file with full path, return the encoded path'
    if not pfilePath:
        return None
    return re.sub(ARCHIVE_PATH, 'dna', pfilePath)

'''---------------------------------------------------
return a list of ncbi organism names in RQC database
'''
def get_unique_ncbi_organism_names(sqlObj):
    'seqObj is an instance of MySQLRestful that provide the query() function'
    sql = 'select distinct(ncbi_organism_name) from library_info where ncbi_organism_name is not NULL order by ncbi_organism_name'
    rows = sqlObj.query(sql)
    alist = []
    if rows:
        for row in rows:
            alist.append(row['ncbi_organism_name'].strip())
        return alist
    else:
        return None


'''---------------------------------------------------
return a list of seq project names in RQC database
'''
def get_unique_seq_proj_names(sqlObj):
    'seqObj is an instance of MySQLRestful that provide the query() function'
    sql = 'select distinct(seq_proj_name) from library_info where seq_proj_name is not NULL order by seq_proj_name'
    rows = sqlObj.query(sql)
    alist = []
    if rows:
        for row in rows:
            alist.append(row['seq_proj_name'].strip())
        return alist
    else:
        return None


'''--------------------------------------------------
for a given set unit name, return RQC pipeline status 
'''
def get_pipeline_status(sqlObj, seqUnitName=None, seqUnitId=None, excludeType=None):
    'Need either seqUnitName, or seqUnitId AND seqUnitNameType'
    if not seqUnitName and not seqUnitId:
        return None

    if not seqUnitName and seqUnitId:
        sql = 'SELECT seq_unit_name FROM seq_units WHERE seq_unit_id = %s'
        rows = sqlObj.query(sql, values=(seqUnitId, ))
        if not rows or len(rows) == 0:
            return None
        else:
            seqUnitName = rows[0]['seq_unit_name']

    #print('DEBUG seqUnitName=%s' % seqUnitName)
    ## if arrieved here, seqUnitName must have value
    sql = '''
SELECT
    pscv.pipeline_status,
    pt.pipeline_name as name,
    su.segment_id,
    su.library_name,
    su.gls_physical_run_unit_id,
    pq.rqc_pipeline_queue_id as qid
FROM seq_units su
INNER JOIN rqc_pipeline_queue pq ON pq.seq_unit_id = su.seq_unit_id
INNER JOIN rqc_pipeline_type_cv pt ON pq.rqc_pipeline_type_id = pt.rqc_pipeline_type_id
INNER JOIN rqc_pipeline_status_cv pscv ON pq.rqc_pipeline_status_id = pscv.rqc_pipeline_status_id
WHERE
    su.seq_unit_name = %s
'''
    if seqUnitName.endswith('srf'): # the legacy data
        sql = '''
SELECT
    A.analysis_type_id,
    A.analysis_status_id,
    V.analysis_status_name,
    su.segment_id,
    su.library_name,
    su.gls_physical_run_unit_id,
    su.is_multiplex
FROM rqc_analysis A
INNER JOIN m2m_analysis_seq_unit M ON A.analysis_id = M.analysis_id
INNER JOIN seq_units su ON M.seq_unit_name = su.seq_unit_name 
INNER JOIN rqc_analysis_status_cv V ON A.analysis_status_id = V.analysis_status_id
WHERE
    su.seq_unit_name = %s
ORDER BY A.analysis_type_id
'''

    import html_tools as html
    def dummy_link(seqUnitName, label, section=None, target='_blank'):
        return None

    PIPELINE_LINK = {   READ_QC             : html.read_qc_report_link,
                        READ_QC_PB          : dummy_link,
                        ASSEMBLY_QC         : html.assemblyqc_report_link,
                        JIGSAW_SAG          : html.jigsaw_report_link,
                        JIGSAW_SAG2         : html.jigsaw_report_link,
                        JIGSAW_ISO          : html.jigsaw_report_link,
                        JIGSAW_AMP_CE       : html.jigsaw_report_link,
                        FILTER              : html.filter_report_link,
                        ALIGNMENT           : html.align_report_link,
                        PACBIO              : html.pacbio_report_link,
                        PHIX                : html.phix_report_link,
                        METAGENOME          : html.metagenome_report_link,
                        CLRS_REDUNCANCY     : html.clrs_redundancy_link,
                        FUNGAL_MIN          : html.fungal_min_link,
                        MULTI_ALIGN         : html.multi_align_link,
                        METATRANSCRIPTOMEAA : None,
                        MT_FILTER           : None,
                        LMP_INSERT_SIZE     : html.lmp_insert_size_link,
                    }

    def cov_key_name(name):
        return name.lower().replace(' ', '_') + '_pipeline'

    def cov_value(seqUnitName, pipeName, value, valType='message', qid=None):
        ret = {'qid': qid}
        ret['name'] = pipeName
        if value == 'Pipeline Complete':
            if pipeName in PIPELINE_LINK:
                if PIPELINE_LINK[pipeName]:
                    ret['value'] = PIPELINE_LINK[pipeName](seqUnitName, pipeName)
                    ret['value_type'] = 'link'
                else:
                    ret['value'] = 'Complete but no web report'
                    ret['value_type'] = 'message'
            else:
                ret['value'] = 'unknown pipeline'
                ret['value_type'] = 'error'
        else:
            ret['value'] = value
            ret['value_type'] = valType

        return ret

    rows = sqlObj.query(sql, values=(seqUnitName,))

    if not rows or len(rows) == 0:
        return None

    # segmentID = rows[0]['segment_id']   # query return must have this field by now!
    libname = rows[0]['library_name']
    gls_purid = rows[0]['gls_physical_run_unit_id']
    ret = []

    if seqUnitName.endswith('srf'):	# old rqc system pipeline type
        pipe_type = []
        for row in rows:
            status = 'Pipeline Complete'
            if row['analysis_status_id'] != 3:
                status = row['analysis_status_name']

            if row['analysis_type_id'] == 1:
                pipe_type.append([READ_QC, status])
            elif row['is_multiplex'] != 1:  # if is not poolhead seq unit
                if row['analysis_type_id'] == 2 or row['analysis_type_id'] == 3:
                    if [ASSEMBLY_QC, status] not in pipe_type:
                        pipe_type.append([ASSEMBLY_QC, status])
                    if [FILTER, status] not in pipe_type:
                        pipe_type.append([FILTER, status])
                    if [ALIGNMENT, status] not in pipe_type:
                        pipe_type.append([ALIGNMENT, status])

                elif row['analysis_type_id'] == 4:
                    pipe_type.append([JIGSAW_SAG, status])
                elif row['analysis_type_id'] == 5:
                    pipe_type.append([JIGSAW_ISO, status])

        #print('DEBUG pipe_type: %s' % str(pipe_type))
        for pipe in pipe_type:
            ret.append(cov_value(seqUnitName, pipe[0], pipe[1]))

    else:	# new rqc_pipeline_type_cv
        for row in rows:
            if 'name' in row:
                if row['name'] == READ_QC_PB:    # Don't know which report is for this pipeline name
                    continue

                if row['pipeline_status'] == 'Pipeline Complete' and row['name'] == READ_QC and (not excludeType or excludeType.lower() != 'full'):   # if Read QC report is ready, full report is also ready
                    fullreport_link = '<a href=\'/fullreport/report/%s\' target=\'_blank\'>Full Report</a>' % seqUnitName
                    ret.append(cov_value(seqUnitName, 'Full Report', fullreport_link, valType='link'))

                if row['name'] in PIPELINE_LINK.keys() and row['name'] != excludeType:
                    ret.append( cov_value(seqUnitName, row['name'], row['pipeline_status'], qid=str(row['qid'])) )


    '''
    if segmentID and segmentID > 0:
        seg_qc_link = '<a href=\'https://rqc-legacy.jgi-psf.org/cgi-bin/display_segmentQC.cgi?segmentId=%s\' target=\'_blank\'>Segment QC</a>' % segmentID
        ret.append(cov_value(seqUnitName, 'Segment QC', seg_qc_link, valType='link'))
    '''

    if libname and gls_purid:
        seg_qc_link = '<a href=\'/sow_qc/page/%s/%s\' target=\'_blank\'>Rework QC</a>' % (libname, gls_purid)
        ret.append(cov_value(seqUnitName, 'Segment QC', seg_qc_link, valType='link'))

    #pprint.pprint(ret)
    return ret



'''---------------------------------------------------
return True if any item in string list starts with the given search string, in case insensitive way
'''
def start_with_substr_in_list(alist, searchFor):
    sstr = searchFor.lower()
    for item in alist:
        if item.lower().startswith(sstr):
            return True
    return False

'''---------------------------------------------------
return True if any item in string list contains substring of the searchFor string, in case insensitive way
'''
def contain_substr_in_list(alist, searchFor):
    sstr = searchFor.lower()
    for item in alist:
        if item.lower().find(sstr) > -1:
            return True
    return False


def is_num(value):
    if not value:
        return False
    else:
        svalue = str(value)
        match = re.match(r'^[0-9\.]*$', svalue)
        if match and svalue.count('.') < 2:
            return True

'''---------------------------------------------------
return boolean, or the int value (or None), of a given value
'''
def is_int(param, value=False):
    # private helper
    rtnval = lambda v1, v2, test: v1 if test else v2
    if is_num(param) and str(param).count('.') == 0:
        return rtnval(int(param), True, value == True)
    else:
        return rtnval(None, False, value == True)


'''---------------------------------------------------
return boolean, or the float value (or None), of a given value
'''
def is_float(param, value=False):
    # private helper
    rtnval = lambda v1, v2, test: v1 if test else v2
    if is_num(param) and str(param).count('.') == 1:
        return rtnval(float(param), True, value == True)
    else:
        return rtnval(None, False, value == True)


def get_lane_from_seq_unit_name(seqUnitName):
    mat = re.match(r'^\d{4,}\.(\d)\.\d+', seqUnitName)  # runid >= 4 digits
    if mat:
        return mat.group(1)
    else:
        return None

def rqc_qc_data_usable(sqlObj, sun):
    'sqlObj is the MySQLRestful object, if QCed, return True for data usable (True) and unusable (False). None if not QCed.'
    sql = 'SELECT qc_state FROM seq_unit_qc WHERE seq_unit_name = %s'
    rows = sqlObj.query(sql, values=(sun,))
    #print('rqc_tools: rqc_qc_data_usable=%s' % str(rows))
    if rows and len(rows) > 0:
        return rows[0]['qc_state'] == 1
    else:
        return None

# to be deprecated, use analyst_name_by_email
def analyst_name(sqlObj, emailName):
    'Return FIRST LAST name in rqc_analyst table. emailName is the gmail address without @lbl.gov'
    sql = 'SELECT CONCAT(first_name, \' \', last_name) AS user FROM rqc_analyst where email = %s'
    rows = sqlObj.query(sql, values=('%s@lbl.gov' % emailName, ))
    if rows:
        return rows[0]['user']
    else:
        return None

# to replace the above analyst_name
def analyst_name_by_email(sqlObj, emailName):
    'Return FIRST LAST name in rqc_analyst table. emailName is the gmail address without @lbl.gov'
    sql = 'SELECT CONCAT(first_name, \' \', last_name) AS user FROM rqc_analyst where email = %s'
    rows = sqlObj.query(sql, values=('%s@lbl.gov' % emailName, ))
    if rows:
        return rows[0]['user']
    else:
        return None

def analyst_name_by_id(sqlObj, aid):
    'Return FIRST LAST name in rqc_analyst table. aid is the rqc_analyst.analyst_id'
    sql = 'SELECT CONCAT(first_name, \' \', last_name) AS user FROM rqc_analyst where analyst_id = %s'
    rows = sqlObj.query(sql, values=(aid, ))
    if rows:
        return rows[0]['user']
    else:
        return None

def analyst_caliban_by_email(sqlObj, emailName):
    'Return caliban id in rqc_analyst table. emailName is the gmail address without @lbl.gov'
    sql = 'SELECT caliban_id FROM rqc_analyst where email = %s'
    rows = sqlObj.query(sql, values=('%s@lbl.gov' % emailName, ))
    if rows:
        return int(rows[0]['caliban_id'])
    else:
        return None

def analyst_caliban_by_name(sqlObj, fname, lname):
    'Return caliban id in rqc_analyst table. emailName is the gmail address without @lbl.gov'
    sql = 'SELECT caliban_id FROM rqc_analyst where  first_name = %s and last_name=%s'
    rows = sqlObj.query(sql, values=(fname, lname))
    if rows:
        return int(rows[0]['caliban_id'])
    else:
        return None

def analyst_id(sqlObj, emailName):
    'emailName is the gmail address without @lbl.gov'
    sql = 'SELECT analyst_id FROM rqc_analyst where email = %s'
    rows = sqlObj.query(sql, values=('%s@lbl.gov' % emailName, ))
    if rows:
        return rows[0]['analyst_id']
    else:
        return None

def analyst_id_by_name(sqlObj, fname, lname):
    'return analyst id for the given first name and last name'
    sql = 'SELECT analyst_id FROM rqc_analyst where first_name = %s and last_name=%s'
    rows = sqlObj.query(sql, values=(fname, lname))
    if rows:
        return int(rows[0]['analyst_id'])
    else:
        return None

'''
For a given seq unit name, return the dict data of seq_unit_qc, or None if not in RQC db.
'''
def get_seq_qc_meta(sqlObj, seqUnitName):
    'For a given seq unit name, return a metadata dictionary for seq unit qc record'
    sql = '''
        SELECT seq_unit_name, analyst_name, qc_state, failure_mode_where, failure_mode_what, comments, update_date
        FROM seq_unit_qc
        WHERE seq_unit_name=%s;
    '''

    rows = sqlObj.query(sql, values=(seqUnitName, ))
    if rows:
        row = rows[0]

        usable = False    # data not usable
        if row['qc_state'] == 1:
            usable = True
            del row['qc_state']

        # delete the empty data
        for key in row.keys():
            if not row[key]:
                del row[key]

        row['usable'] = usable

        row['update_date'] = str(row['update_date'])

        return row
    else:
        return None

'''
seqQcObj : seq unit qc dict with ['usable': Boolean, 'comments': SRT, 'analyst': STR] 
sqlObj : the MySQLRestful object
seqUnitName : seq unit name
dataUsable : (Boolean)
'''
def update_seq_unit_qc_uri(sqlObj, seqUnitName, seqQcObj):

    sql = '''
    INSERT INTO seq_unit_qc (seq_unit_name, analyst_name, qc_state, comments, uri) VALUES (%s, %s, %s, %s, %s)
    ON DUPLICATE KEY UPDATE analyst_name=%s, qc_state=%s, comments=%s, uri=%s
    '''

    state = '1'
    if not seqQcObj['usable']:
        state = '0'

    param = [seqUnitName, seqQcObj['analyst'], state, seqQcObj['comments'], seqQcObj['uri']]

    sqlObj.query(sql, values=(param + param[1:]))

'''
    return the string format of the given number.
'''
def number_with_commas(num):
    if num:
        try:
            num = "{:,}".format(num)
        except:
            pass
    return num

'''
Connect to Data warehouse/Oracle
'''
def dw_connection():

    db_name = "dwprd1" # TNS name, don't need farina
    dw_its_username = "dw_user"
    dw_its_passwd = "ZHdfdXNlcg=="

    conn_str = "%s/%s@%s" % (dw_its_username, base64.decodestring(dw_its_passwd), db_name)

    db = cx_Oracle.connect(conn_str)

    return db

# for a given string
# 1) strip of non-ASCII and non-printable chars;
# 2) replace single and double quots with *;
# 3) replace newline with space
def clean_str(my_str):
    if my_str:
        if type(my_str) is str:
            my_str = "".join(c for c in my_str if ord(c) >=32 and ord(c) <=127)
            my_str = my_str.replace('\'', '*').replace('\"', '*').replace('\n', ' ')

    return my_str


# return sql formatted date from inputted date, if not inputted date use now - day_diff days
def get_input_date(kwargs, date_key, day_diff = 0):

    input_date = None
    sql_date = None

    if date_key in kwargs:
        input_date = kwargs[date_key]
    else:
        input_date = time.strftime("%m-%d-%Y", time.localtime(time.time() - day_diff * 24 * 3600))


    try:
        sql_date = time.strftime("%Y-%m-%d 00:00:00", time.strptime(input_date, "%m-%d-%Y"))
    except:
        print "bad date param: %s" % input_date
        input_date = time.strftime("%m-%d-%Y", time.localtime(time.time() - day_diff * 24 * 3600))
        sql_date = time.strftime("%Y-%m-%d 00:00:00", time.strptime(input_date, "%m-%d-%Y"))


    return input_date, sql_date

# translate real name to the name we all know
def get_instrument_name(real_instrument_name):


    ins_dict = {
        "M00127" : "MiSeq01",
        "M00137" : "MiSeq02",
        "M00772" : "MiSeq03",
        "M00773" : "MiSeq04",
        "M01094" : "MiSeq05",
        "M01421" : "MiSeq06",
        "M01629" : "MiSeq07",
        "M01883" : "MiSeq08",
        "M02100" : "MiSeq09",
        "SN133" : "HiSeq01",
        "SN190" : "HiSeq02",
        "SN440" : "HiSeq03",
        "SN445" : "HiSeq04",
        "SN451" : "HiSeq05",
        "SN681" : "HiSeq06",
        "SN683" : "HiSeq07",
        "SN692" : "HiSeq08",
        "7001443" : "HiSeq09",
        "700308R" : "HiSeq10",
        "700813F" : "HiSeq11",
        "700951L" : "HiSeq12",
        "700706F" : "HiSeq13",
        # HiSeq14 is missing
        "700605F" : "HiSeq15",
        "NS500302" : "NextSeq01"

    }

    instrument_name = "unknown"

    if real_instrument_name in ins_dict:
        instrument_name = ins_dict[real_instrument_name]

    return instrument_name


def get_file_content_in_zip(filePath):
    'for a given rqc archived file(fullpath), try to retrieve its content from zip file'

    fs_location = os.path.dirname(filePath)
    basename = os.path.basename(filePath)
    zipFile = os.path.join(fs_location, str(int(re.sub("[^0-9]", "", fs_location))) + ".zip")

    my_buffer = None
    if os.path.isfile(zipFile):
        z_obj = zipfile.ZipFile(zipFile)
        try:
            my_buffer = z_obj.read(basename).strip()
        except Exception as e:
            print("Error in call get_file_content_in_zip(), cannot find %s in %s [%s]" % (basename, zipFile, str(e)))
    else:
        print("Error in call get_file_content_in_zip(), cannot find the zip file [%s][looking for %s]" % (zipFile, filePath))
    return my_buffer



""" ===========================================================================
    construct a temp file name to restore'
"""
def tmp_file_name(fileInDBName):
    # fpath = fileInDBName
    tmpdir = os.environ.get('SCRATCH')
    if not tmpdir:
        tmpdir = '/tmp'
    fpath = os.path.join(tmpdir, os.path.basename(fileInDBName))
    return fpath



def reverse_complement(seq):
    my_dictionary = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'}
    return "".join([my_dictionary[base] for base in reversed(seq)])



if __name__ == '__main__':
    values = ['12.3', '12', '12a', 12, 12.3]
    for val in values:
        print('Is {0} int? {1}'.format(val, is_int(val)))
        print('Is {0} float? {1}'.format(val, is_float(val)))
        print('int value of {0} : {1}'.format(val, is_int(val, True)))
        print('float value of Is {0} : {1}'.format(val, is_float(val, True)))

