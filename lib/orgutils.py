#!/usr/bin/env python

import re
import requests
import subprocess
import pprint

#============================================================================#
class Utils(object):
    def __init__(self, debug=None):
        self.__debug = debug

    #============================================================================#
    def getNumBlastParsedHits(self, blastfile=None, stream=None, matchString=None, maxExpectScore=0, minAlignLength=500, minPctId=98):
        '''
        Returns the number of blast hits for a given criteria.
        The required in put is either the blastfile or a string containing the output of the blastfile defined as the stream.
        The optional matchString regular expression may be used to match the hit name to the given string.
        '''
        numHits = 0
        blastEntries = 0
        if blastfile is not None:
            stream = self.getFromFileApi(blastfile)
            if stream.startswith('The file can not be found.'):
                return -1

        for v in stream.split('\n'):
            if blastEntries == 1 and not v.startswith('#'):
                cols = v.split('\t')
                if len(cols) > 1:
                    hit = cols[1]
                    expect = float(cols[3])
                    alignLen = int(cols[4])
                    pctId = float(cols[5])

                    if self.__debug:
                        print "%s, expect=%s, alignLen=%s, pctId=%s"%(hit, expect, alignLen, pctId)

                    if matchString:
                        rex = re.search(matchString, hit, re.IGNORECASE)
                        if rex and expect <= maxExpectScore and alignLen >= minAlignLength and pctId >= minPctId:
                            if self.__debug:
                                print "found"
                            numHits += 1
                    elif expect <= maxExpectScore and alignLen >= minAlignLength and pctId >= minPctId:
                        numHits += 1
            if v.startswith('<pre>') or v.startswith('#query'):
                blastEntries = 1

        return numHits

    #============================================================================#
    def getFromFileApi(self, filename):
        url = "https://rqc.jgi-psf.org/api/file/file/%s"%filename
        c = requests.get(url)
        return c.text

    #============================================================================#
    def getSynonymNames(self, taxId):
        '''
        Lookup synonomous names for given taxId using NCBI's url
        '''
        foundSyn = 0
        orgnames = []
        cmd = "curl -s https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=%s"%taxId
        output, _, _ = self.runCommand(cmd)
        for line in output.split('\n'):
            if 'synonym:' in line:
                foundSyn = 1
            elif foundSyn:
                rex = re.search(r'>([^\<]+)<*', line)
                if rex:
                    orgnames.append(rex.group(1))
                foundSyn = 0
        return orgnames

    #============================================================================#
    def isRightOrganism(self, files, organismName, taxId=None, maxExpectScore=0, minAlignLength=500, minPctId=98):
        isRightOrganism = 0
        orgnames = [organismName]

        # If taxId is defined, lookup synonymous names using NCBI url call
        if taxId:
            orgnames.extend(self.getSynonymNames(taxId))
        # if organism name has spaces, change name as regex to look for either space or underscore
        for idx, orgname in enumerate(orgnames):
            # If organism contains more than two words, get only first two words.
            if orgname.count(' ') > 1:
                orgname = ' '.join(organismName.split()[:2])
            # If organism ends with ' sp.', remove that from name.
            if orgname.endswith(' sp.'):
                orgname = orgname[:-4]
            # set orgname regular expression to look for either spaces or underscores
            orgnames[idx] = orgname.replace(' ', '[ _]')
        # if more than one organism name exists, set regex expression to look for each organism with an or condition.
        if len(orgnames) > 1:
            matchString = "|".join(orgnames)
        else:
            matchString = orgnames[0]

        if self.__debug:
            print "Organisms names to lookup: %s"%orgnames
            print "matchstring=%s"%matchString

        for filename in files:
            if self.__debug:
                print "Searching %s"%filename
            numHits = self.getNumBlastParsedHits(blastfile=filename, matchString=matchString, maxExpectScore=maxExpectScore, minAlignLength=minAlignLength, minPctId=minPctId)
            if numHits: # if organism is found in blastfile, don't need to look at other files.
                isRightOrganism = 1
                break
        return isRightOrganism

    #============================================================================#
    def runCommand(self, cmd):
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        exitcode = process.returncode
        return stdout.strip(), stderr.strip(), exitcode

    #============================================================================#
    