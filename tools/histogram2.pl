#!/usr/bin/env perl
# pulled in from Jigsaw
# requested to be included in RQC: 2017-11-20

$n_args = @ARGV;
if ( ($n_args != 3) && (($n_args%3 != 0) || ($n_args == 0)) ) {
    die "Usage: ./histogram2.pl datafile input_column binsize <<discrim_column min_data max_data>> ...\n";
}

open(F,$ARGV[0]);
my $bincolumn = $ARGV[1];
my $binsize = $ARGV[2];

die "(histogram2) binsize cannot be '0'" if ($binsize == 0) ;

my %discriminators = ();
my $bounded = 0;

if ($n_args > 3) {
    $bounded = 1;
    for (my $i = 3; $i < $n_args; $i+=3) {
	$discriminators{$ARGV[$i]} = $ARGV[$i+1] . "_" . $ARGV[$i+2];
    }
}

my $total_values = 0;
my $sum_of_values = 0;
my $sum_of_squared_values = 0;


my %histogram = ();
while (my $i = <F>) {
    chomp $i;
    $i =~ s/^\s+//;
    # ACC -07Dec04
    next if /^#/ ;
    my @entries = split(/\s+/,$i);
    my $value = $entries[$bincolumn-1];

    # ACC -07Dec04
    next unless $value =~ /\d+/ ;

    my $good_value = 1;

    if ($bounded == 1) {

	while (my ($disc_col,$minmax) = each(%discriminators)) {
	    
	    my $discriminator = $entries[$disc_col-1];
	    my ($d_min,$d_max) = $minmax =~ /(.+)_(.+)/;

	    if ( ($discriminator < $d_min) || ($discriminator > $d_max) ) {
		$good_value = 0;
	    }
	}
    }

    if ($good_value == 1) {

	if ($total_values == 0) {
	    $max_value = $value;
	    $min_value = $value;
	} else {
	    if ($value > $max_value){$max_value = $value;}
	    if ($value < $min_value){$min_value = $value;}
	}
	$total_values++;
	$sum_of_values += $value;
	$sum_of_squared_values += $value*$value;
	$bin = sprintf("%.0f", $value/$binsize - 0.4999);
	if ($bin eq "-0") {$bin = "0"};

	if (!exists($histogram{$bin})) {
	    $histogram{$bin} = 1;
	} else {
	    $histogram{$bin}++;
	}
    }
}
close F;

my $max_counts = 1;
my $most_likely_bin = "NULL";

while (($bin, $value) = each(%histogram)) {

    if ($value > $max_counts) {
	$max_counts = $value;
	$most_likely_bin = $bin;
    }
}

$total_values = -1 if $total_values == 0 ;
$average = $sum_of_values/$total_values;
$std_dev = sqrt($sum_of_squared_values/$total_values - $average*$average);

printf("#Found %d total values totalling %.4f. <%.6f +/- %.6f>\n", 
       $total_values, $sum_of_values, $average, $std_dev);

print "#Range: [ $min_value - $max_value ]\n";
printf("#Most likely bin: [ %s - %s ] $max_counts counts\n",
       $most_likely_bin*$binsize,($most_likely_bin+1)*$binsize);

my @sorted_bins = sort {$a <=> $b} keys(%histogram);
my $so_far = 0;
my $bin_index=-1;
while ($so_far < $total_values/2.0) {
    $bin_index++;
    $so_far += $histogram{$sorted_bins[$bin_index]};
}

my $median_bin = $sorted_bins[$bin_index];
printf("#Median bin: [ %s - %s ] %s counts\n",
       $median_bin*$binsize, ($median_bin+1)*$binsize, $histogram{$median_bin});

my $max_bin = sprintf("%.0f", $max_value/$binsize - 0.4999);
my $min_bin = sprintf("%.0f", $min_value/$binsize - 0.4999);
if ($min_bin eq "-0") {$min_bin = "0"};
if ($max_bin eq "-0") {$max_bin = "0"};

# ACC 07Dec04  print column headers
printf("#%-40s %-9s %-s %-s %-s\n","Histogram","Bins","Count","Fraction","Cum_Fraction");

$running_total = 0;
$last_bin_occupied = 0;
for ($bin = $min_bin; $bin <= $max_bin; ++$bin)
{
    if ($histogram{$bin})
    {
	$min = $bin*$binsize;
	$max = $min + $binsize;
#	if ( ($bounded == 1) && ($bincolumn == $disc_column) && ($min < $min_data) ) {$min = $min_data;}
#	if ( ($bounded == 1) && ($bincolumn == $disc_column) && ($max > $max_data) ) {$max = $max_data;}
	$running_total += $histogram{$bin};
	$cumul = sprintf("%.2f",$running_total/$total_values);
	$instant = sprintf("%.2f",$histogram{$bin}/$total_values);
#	$picture = "|" . "X" x sprintf("%.0f",40*$histogram{$bin}/$total_values);
	$picture = "|" . "X" x sprintf("%.0f",40*$histogram{$bin}/$max_counts);

#	$space = " " x sprintf("%.0f",40*(1-$histogram{$bin}/$total_values));
	$space = " " x sprintf("%.0f",40*(1-$histogram{$bin}/$max_counts));


	print "$picture$space $min - $max : [ $histogram{$bin} $instant $cumul ]\n"; 
	$last_bin_occupied = 1;
    } else {
	if ($last_bin_occupied == 1) {
	    print "#...\n";
	    $last_bin_occupied = 0;
	}
    }
}




