#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
jat2fa - pulls assemblies from JAMO and creates one large database
used for fungal assemblies

Dependencies
bbtools 
jamo

v 1.0: 2017-12-xx
- initial version
- works with genepool & denovo

To do:
- option to look up new ones and append to existing?  Difficult to know if we have the assembly or not ... 

* 2017-12-07: Kurt does not use the fungal main or mito dumps for anything!

find anything over 60 days and remove it - add to fungal_db.sh
how to copy to /global/dna from a qsub job?
- could run on denovo login node

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
import re
from argparse import ArgumentParser

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from common import run_cmd, get_colors, get_msg_settings

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions
        

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "JAT to Fasta"

    
    version = "1.0"
    
    
    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)

    # Parse options
    usage = "jat2fa.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
Pulls the online assemblies from JAMO to create a combined fasta
Runs makeblastdb on the combined fasta
    """

    parser = ArgumentParser(usage = usage)


    parser.add_argument("-f", "--fasta", dest="fasta", type = str, help = "Output fasta - concatination of all fastas found")
    parser.add_argument("-t", "--jamo-label", dest="jamo_label", type = str, help = "JAT label to use to pull out files (metadata.jat_label)")
    parser.add_argument("-v", "--version", action="version", version=version)

    output_path = None
    fasta = None
    jamo_label = None
    
    
    # parse args
    args = parser.parse_args()
    
    if args.fasta:
        fasta = args.fasta

    if args.jamo_label:
        jamo_label = args.jamo_label

    if args.jamo_label == "fungal_main":
        fasta = "/global/dna/shared/rqc/ref_databases/qaqc/databases/fungi/jgi_fungi.fa"
        fasta = "/global/projectb/scratch/brycef/t2/jgi_fungi.fa"
        jamo_label = "fungal_main_scaffolds"

    if args.jamo_label == "fungal_mito":
        fasta = "/global/dna/shared/rqc/ref_databases/qaqc/databases/fungi/jgi_fungi.fa"
        fasta = "/global/projectb/scratch/brycef/t2/jgi_fungi.fa"
        jamo_label = "fungal_main_scaffolds"
        
    if not fasta:
        print "Error: no fasta specified!  %s" % msg_fail
        sys.exit(2)

    if not jamo_label:
        print "Error: no jamo_label specified!  %s" % msg_fail
        
    my_path = os.path.dirname(fasta)
    if not my_path:
        my_path = os.getcwd()
        
    print "Writing to: %s" % my_path
    
    jamo_output = os.path.join(my_path, "jamo.txt")    
    jamo_cmd = "#jamo;jamo report select file_path, file_name, metadata.sequencing_project_id, metadata.gold_data.display_name, file_status where metadata.jat_label=%s > %s 2>&1" % (jamo_label, jamo_output)
    std_out, std_err, exit_code = run_cmd(jamo_cmd)
    #print jamo_cmd
    
    if os.path.isfile(fasta) and 1==2:
        print "- found %s, not re-creating  %s" % (fasta, msg_warn)
    
    else:
        
        fhw = open(fasta, "w")
        
        print
        print "~" * 170
        print "%-140s %10s %15s" % ("FILE_NAME", "CONTIGS", "ASM SIZE (BP)")
        print "~" * 170
        
        contig_cnt_ttl = 0
        base_cnt_ttl = 0
        
        fasta_cnt = 0
        
        fh_jamo = open(jamo_output, "r")
        for line in fh_jamo:
            line = line.strip()
            
            arr = line.split('\t')
            
            
            if len(arr) < 5:
                continue
            
            # skip offline stuff 
            if str(arr[4]).lower() == "purged":
                continue
        
            my_fasta = os.path.join(arr[0], arr[1])
            
            if not os.path.isfile(fasta):
                continue
            
            else:
                fasta_cnt += 1        
                fh = open(my_fasta)
                
                contig_cnt = 0
                base_cnt = 0
                for line in fh:
                    if line.startswith(">"):
                        contig_cnt += 1
                        contig_cnt_ttl += 1
                        # replace with ">(spid)_(gold_data.display_name)_(total contig id)
                        
                        line = "%s_%s_%s" % (arr[2], arr[3], contig_cnt_ttl)
                        line = re.sub(r'[^0-9A-Za-z\-\(\)\.]', '_', line) # anything not in here convert to "_"
                        line = ">" + line
                        #print line
                        #sys.exit(44)
                    else:
                        base_cnt += len(line.strip())
                        
                    fhw.write(line + "\n")
                base_cnt += len(line.strip())
                fh.close()
                print "%-140s %10s %15s" % (my_fasta, contig_cnt, "{:,}".format(base_cnt))
                base_cnt_ttl += base_cnt
        fhw.close()
        
        print "~" * 170
        print "Created: %s" % fasta
        print "- fastas: %s" % fasta_cnt
        print "- contigs: %s" % "{:,}".format(contig_cnt_ttl)
        print "- bases: %s" % "{:,}".format(base_cnt_ttl)
    
    
    # run blast on it
    #makeblastdb -dbtype nucl -parse_seqids -in $SCRATCH_PATH/jgi_fungi.fa
    print "- formatting with makeblastdb"
    make_log = os.path.join(my_path, "makedb.log")
    blast_db = os.path.join(my_path, os.path.basename(fasta))  
    cmd = "#blast;makeblastdb -dbtype nucl -parse_seqids -in %s -out %s > %s 2>&1" % (fasta, blast_db, make_log)
    std_out, std_err, exit_code = run_cmd(cmd)
    
    print
    print "Completion sucessful  %s" % msg_ok
    
    sys.exit(0)

