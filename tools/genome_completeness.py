#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
	Genome completeness check using checkm

	Version 1.0.0
	Shijie Yao
	2016-02-18 - initial implementation
	
	run examples:
'''

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
import shutil
from argparse import ArgumentParser
import time
import datetime
import pprint

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
	sys.path.append(LIB_DIR)

import os_utility as util 
if 'run_sh_command' in dir(util):
	run_command = util.run_sh_command 
elif 'run_command' in dir(util):
	run_command = util.run_command
	
LOG_FILE = None

def exit_on_err(msg):
	print('')
	print_and_log(' ERROR : %s.' % msg)
	print('')
	sys.exit(1)
	
def print_and_log(msg, addtime=False, logfile=None):
	'print and log message'
	global LOG_FILE
	
	if logfile:
		LOG_FILE = logfile
	elif not LOG_FILE:
		LOG_FILE = os.path.basename(__file__)
		LOG_FILE = LOG_FILE[:len(LOG_FILE) - 3] + '.log'
		LOG_FILE = os.path.abspath(LOG_FILE)
		
	'Print a message to log file as well as stdout.'
	fout = None
	if LOG_FILE:
		fout = open(LOG_FILE, 'a') # append to

	print(msg)
	if fout:
		if addtime:
			ts = time.time()
			st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
			fout.write('%s : %s\n' % (st, msg))
		else:
			fout.write('%s\n' % msg)
	fout.close()
	
	
def exec_cmd(cmd, addtime=True, msg=True):
	if msg:
		print_and_log('RUN %s' % cmd, addtime=addtime)
		
	std_out, std_err, exit_code = run_command(cmd, True)    # exit_code of 0 is success
	if exit_code == 0:
		sout = std_out.strip()
		serr = std_err.strip()
		if sout:
			return sout
		elif serr:
			return serr
		else:
			return True
	else:
		print_and_log('ERROR: %s' % std_err.strip(), addtime=True)
	return False

def add_hmmer_path():
	cmd = 'realpath ~jfroula/bin/pplacer-v1.1.alpha16-1-gf748c91-Linux-3.2.0'
	npath = exec_cmd(cmd, addtime=False, msg=False)
	
	cmd = 'realpath ~jfroula/bin/Prodigal-2.6.1'
	npath += ':' + exec_cmd(cmd, addtime=False, msg=False)
	
	cmd = 'realpath ~jfroula/bin/hmmer-3.1b1-linux-intel-x86_64/bin'
	npath += ':' + exec_cmd(cmd, addtime=False, msg=False)
	
	npath += ':' + os.environ['PATH'].strip()
	os.environ['PATH'] = npath
	
##--------------------------------
def run_driver(genome, wdir, marker):
		
	add_hmmer_path()
		
	gdir = os.path.join(wdir, 'genome')
	outdir = os.path.join(wdir, 'out')
	gbasename = os.path.basename(genome)
	ftype = gbasename.split('.')[-1]
	
	os.makedirs(wdir)
	os.chdir(wdir)
	os.makedirs(gdir)
	
	dest = os.path.join(gdir, gbasename)
	print_and_log('create symlink: %s %s' % (genome, dest), addtime=True)
	os.symlink(genome, dest)

	if not marker:
		cmd = 'checkm lineage_wf -t 8 -x %s %s %s' % (ftype, gdir, outdir)
		return exec_cmd(cmd)
		
	else:
		cmd = 'checkm analyze %s -t 8 -x %s %s %s' % (marker, ftype, gdir, outdir)
		rtn = exec_cmd(cmd)

		if rtn:
			cmd = 'checkm qa %s %s' % (marker, outdir)
			return exec_cmd(cmd)
		else:
			exit_on_err('analyze failed')
		
		
def parse_one(hitfile):
	if not os.path.isfile(hitfile):
		print('not a file %s' % hitfile)
		return None
	else:
		print('parsing %s' % hitfile)
		
	EVAL_CUTOFF = 1.0e-10 	# evalue cutoff; the checkm default value : checkm qa -h
	OL_CUTOFF = 0.7 		# percent overlap between target adn query; the checkm default value : checkm qa -h
	data = {}	# raw hit data
	cdata = {}	# hit count data
	with open(hitfile, 'r') as fh:
		
		last_tgname = None
		for line in fh:
			line = line.strip()
			if not line or line.startswith('#'):
				continue
			
			# line format
			# target name   acc  tlen query name   accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc
			#------------  ---- ----- ------------ ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ----
			#contig_8_346   -   205  Ribosomal_S4  PF00163.14   94   1.3e-10   40.4   0.9   1   1   3.2e-14   2.1e-10   39.7   0.9     6    90     7    87     2    91 0.77
			tok = line.split()
			
			tgname = tok[0]			# target gene name (the genome identified from the assembly)
			mgname = tok[3]			# marker gene (query) name
			evalue = float(tok[6])
			fscore = float(tok[7])	# full sequence score
			cvalue = float(tok[11])	# domain
			ivalue = float(tok[12])
			dscore = float(tok[13])
			acc = float(tok[21])
			
			if mgname == 'Ribosomal_L5_C':
				print(line)
				print('last_tgname=%s; tgname=%s' % (last_tgname, tgname))
			gdat = (evalue, fscore, cvalue, ivalue, dscore, acc)
			dlist = []
			if mgname not in data:
				data[mgname] = [gdat]
			else:
				data[mgname].append(gdat)
				
			if tgname != last_tgname:
				if evalue < EVAL_CUTOFF:
					
					if mgname not in cdata:
						print('assign 1 to %s' % mgname)
						cdata[mgname] = 1
					else:
						print('add 1 to %s' % mgname)
						cdata[mgname] += 1
					
			last_tgname = tgname
				
			
	pprint.pprint(cdata)
	
	print('find %d markers ' % len(data.keys()))
	singlehit = []
	multihit = []
	for mname in data:
		hitcnt = len(data[mname])
		if hitcnt == 1:
			singlehit.append(mname)
		else:
			multihit.append(mname)
			
	#print('\n 1 hit genes : %d' % len(singlehit))
	#pprint.pprint(singlehit)
	
	#print('\n >1 hit genes : %d' % len(multihit))
	#for mname in multihit:
	#	print('%20s : %d' % (mname, len(data[mname])))
		
	print('')
	print('-' * 60)
	for mname in cdata.keys():
		if cdata[mname] > 1:
			print('%20s : %d' % (mname, cdata[mname]))
	print('-' * 60)
	
	
def do_parsing(wdir):
	binpath = os.path.join(wdir, 'out/bins/')
	for iname in os.listdir(binpath):
		apath = os.path.join(wdir, 'out/bins/%s'%iname)
		#print('check %s' % apath)
		if not os.path.isdir(apath):
			continue
		hitfile = os.path.join(apath, 'hmmer.analyze.txt')
		parse_one(hitfile)
			
	
if __name__ == '__main__':

	NAME = 'Genome completeness'
	VERSION = '1.0.0'

	# Parse options
	USAGE = 'prog [options]\n'
	USAGE += '* %s, version %s\n' % (NAME, VERSION)

	JOB_CMD = ' '.join(sys.argv)
	PARSER = ArgumentParser(usage=USAGE)

	ASM_TYPE = ['trinity', 'rnnotator']
	PARSER.add_argument('-g', '--genome', dest='genome', help='genome fasta file')
	PARSER.add_argument('-o', '--wdir', dest='wdir', help='working directory', required=True)
	PARSER.add_argument('-m', '--marker', dest='marker', help='custom marker genes in hmm format')
	PARSER.add_argument('-p', '--parse', dest='parse', action='store_true', help='parse a finished run output dir for marker gene summary')
	PARSER.add_argument('-v', '--version', action='version', version=VERSION)

	ARGS = PARSER.parse_args()
	
	print_and_log('')
	print_and_log('-' * 60)
	print_and_log('CMD=%s' % JOB_CMD, addtime=True)
	
	wdir = os.path.abspath(ARGS.wdir)
	genome = ARGS.genome
	marker = ARGS.marker
	parse = ARGS.parse
	
	# required :
	if not parse and os.path.isdir(wdir):
		exit_on_err('directory exists [%s]' % wdir)
		
	if not parse:
		if not genome:
			exit_on_err('genome file is required if not with -p')
		if not os.path.isfile(genome):
			exit_on_err('genome file does not exist : %s' % genome)
			
		genome = os.path.abspath(genome)
			
		# optional
		if marker and not os.path.isfile(marker):
			exit_on_err('marker file does not exist : %s' % marker)
			
		if marker:
			marker = os.path.abspath(marker)
		
		rtn = run_driver(genome, wdir, marker)
		if rtn:
			print_and_log(rtn)
		else:
			print_and_log(failed, addtime=True)
			exit_on_err('failed')
			
		print('')
	elif not os.path.isdir(wdir):
		exit_on_err('no working directory found [%s]' % wdir)
	else:
		do_parsing(wdir)