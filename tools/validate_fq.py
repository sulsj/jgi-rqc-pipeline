#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Validate Fastq
- checks for read length = quality length, comment line there, index startswith(@)
- updates RQC through the RQC website: rqcws API
- 44000 reads per second - estimate
- doing a full validate on the fastq has found problems (Hudson Alpha)

1.0 - 2017-04-28
- initial version

1.0.1 - 2017-05-01
- don't check for valid bases in pooled fastq
- fix if only 1 read
- bug fixed in detect paired fastq (not all lines starting with "@" are index lines)

1.0.2 - 2017-05-15
- don't use jamo module, use API call instead (JAMO module/shifter image not on Cori)

1.0.3 - 2017-09-08
- minimal checks if pooled fastq (-p)

1.0.4 - 2018-01-11
- return run_type for rqc

1.0.5 - 2018-01-18
- fix for run_type

1.0.6 - 2018-03-08
- don't count indexes for Hudson alpha (-nic option)

./validate_fq.py -o /global/projectb/scratch/brycef/rqc-dev/pipelines/validate/in-progress/02/97/79/27/ -f 12050.6.235517.CAATCGA-GTCGATT.fastq.gz -pl

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import os
import sys
import re
from argparse import ArgumentParser
import json
import gzip
import getpass

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from common import get_logger, run_cmd, get_colors, get_msg_settings
from curl3 import Curl, CurlHttpException # curl3 has apptoken for pmos web services

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

def get_fs_location(my_file):
    log.info("get_fs_location: %s", my_file)
    my_new_file = my_file.strip()

    if my_file.startswith("/"):
        pass
    else:

        #cmd = "#jamo;jamo report select file_path, file_status where file_name = '%s' as json" % (my_new_file)
        #std_out, std_err, exit_code = run_cmd(cmd, log)
        cmd = "curl http://sdm2.jgi-psf.org/api/metadata/files?file_name=%s" % my_new_file
        std_out, std_err, exit_code = run_cmd(cmd, log)

        jamo_json = json.loads(std_out)
        for j in jamo_json:

            if 'file_path' in j:
                log.info("- path: %s", j['file_path'])
                #file_status = j['file_status']
                my_new_file = os.path.join(j['file_path'], my_new_file)


    if os.path.isfile(my_new_file):
        log.info("- file on file system: %s  %s", my_new_file, msg_ok)
    else:
        log.error("- file missing: %s  %s", my_new_file, msg_fail)
        my_new_file = ""

    return my_new_file


'''
Most illumina fastqs are paired
'''
def detect_paired_fastq():
    log.info("detect_paired_fastq")
    paired_flag = False

    fh = None
    if my_file.endswith(".gz"):
        fh = gzip.open(my_file, "r")
    else:
        fh = open(my_file, "r")

    line_cnt = 0
    index_list = []

    for line in fh:
        line_cnt += 1

        if line_cnt == 1 or line_cnt == 5:
            index_list.append(line.strip())
        if line_cnt > 7:
            break
    fh.close()

    i1 = "i1"
    i2 = "i2"
    if len(index_list) > 0:

        log.info("- index read 1: %s", index_list[0])
        i1 = index_list[0].split(" ")[0]

        # some have just 1 read (11192.2.195560.GTTCGGT-AACCGAA.fastq.gz)
        if len(index_list) > 1:
            log.info("- index read 2: %s", index_list[1])
            i2 = index_list[1].split(" ")[0]

    if i1 == i2:
        paired_flag = True
        log.info("- reads are paired  %s", msg_ok)
    else:
        log.info("- reads are not paired  %s", msg_warn)


    return paired_flag


'''
validate seq unit
- checks read_cnt == index_cnt == qual_cnt == comment_cnt
- checks index start with @
- counts indexes
- len(read) == len(qual)
- comment starts with +
- read pairing is correct (if paired_flag)
- read contains only ATCGN
- if index_cnt > 0 and pooled_flag = False then fail

@paired_flag: if the fastq is expected to be paired
@pooled_flag: if the fastq is expected to be pooled (2+ libraries/indexes, eg. unknown)

'''
def validate_seq_unit(paired_flag, pooled_flag):
    log.info("validate_seq_unit")

    fh = None
    if my_file.endswith(".gz"):
        fh = gzip.open(my_file, "r")
    else:
        fh = open(my_file, "r")

    index_cnt = 0
    read_cnt = 0
    base_cnt = 0
    comment_cnt = 0
    qual_cnt = 0

    err_cnt = 0
    last_index = ""
    index_dict = {} # dictionary of indexes

    # process entire file

    line_cnt = 0
    read_mod = 100000
    if pooled_flag:
        read_mod = 1000000
    # each line
    index = ""
    read = ""
    comment = ""
    qual = ""

    bcheck = re.compile(r'[ATCGN]+')

    if my_file.endswith(".gz"):
        fh = gzip.open(my_file, "r")
    else:
        fh = open(my_file, "r")

    for line in fh:

        line_cnt += 1

        if line_cnt == 1:
            index = line.strip()
        elif line_cnt == 2:
            read = line.strip()
        elif line_cnt == 3:
            comment = line.strip()
        elif line_cnt == 4:
            qual = line.strip()


        if line_cnt == 4:
            line_cnt = 0

            if pooled_flag:
                read_cnt += 1
                base_cnt += len(read)
                if read_cnt % read_mod == 0:
                    log.info("- reads: %s, bases: %s, errors: %s", read_cnt, base_cnt, err_cnt)
                continue


            if index:
                index_cnt += 1
                if index.startswith("@"):
                    pass
                else:
                    err_cnt += 1
                    log.error("- Bad Index! read: %s  %s", msg_fail)
                    log.error(index)


                if ":" in index:
                    read_index = index.split(":")[-1]
                    if read_index in index_dict:
                        index_dict[read_index] += 1
                    else:
                        index_dict[read_index] = 1

                    #print read_index

            if read:
                read_cnt += 1
                base_cnt += len(read)

                m = bcheck.match(read)
                if m:
                    if len(m.group(0)) < len(read):
                        err_cnt += 1
                        log.error("- Non-standard bases! read: %s  %s", read_cnt, msg_fail)
                        log.error(read)


            # comment check
            if comment:
                comment_cnt += 1
                if comment.startswith("+"):
                    pass
                else:
                    err_cnt += 1
                    log.error("- Comment error! read: %s  %s", msg_fail)
                    log.error(comment)



            if qual:
                # qual ok chars?
                qual_cnt += 1
                #
                if len(read) != len(qual):
                    log.error("- Read-Qual error!  Read: %s, read length = %s, qual length = %s  %s", read_cnt, len(read), len(qual), msg_fail)
                    err_cnt += 1

            # pair check?
            if read_cnt > 1 and read_cnt % 2 == 0:
                if paired_flag:
                    i1 = index.split(" ")[0]
                    i2 = last_index.split(" ")[0]
                    #print i1
                    #print i2

                    if i1 == i2:
                        pass
                    else:
                        # too many log lines - uncomment for debugging (caught run 12050.6.*)
                        #log.error("- Non-paired error!  Read: %s  %s", read_cnt, msg_fail)
                        #log.error(last_index)
                        #log.error(index)
                        err_cnt += 1

            #if read_cnt % 2 == 0:
            #    print last_index
            #    print index

            last_index = index

            index = ""
            read = ""
            comment = ""
            qual = ""


            if read_cnt % read_mod == 0:
                log.info("- reads: %s, bases: %s, errors: %s", "{:,}".format(read_cnt), "{:,}".format(base_cnt), "{:,}".format(err_cnt))

            # testing
            #if read_cnt > 300000:
            #    break

    fh.close()

    log.info("* reads: %s, bases: %s, errors: %s",  "{:,}".format(read_cnt),  "{:,}".format(base_cnt),  "{:,}".format(err_cnt))
    log.info("* distinct index count: %s", len(index_dict))

    if not pooled_flag:
        
        if len(index_dict) > 1:
            log.error("- expected indexes: 1  %s", msg_fail)
            if index_count_flag:
                err_cnt += 1

        if read_cnt != index_cnt:
            log.error("- read cnt: %s != index_cnt: %s  %s", read_cnt, index_cnt, msg_fail)
            err_cnt += 1

        if read_cnt != comment_cnt:
            log.error("- read cnt: %s != comment_cnt: %s  %s", read_cnt, comment_cnt, msg_fail)
            err_cnt += 1

        if read_cnt != qual_cnt:
            log.error("- read cnt: %s != qual_cnt: %s  %s", read_cnt, qual_cnt, msg_fail)
            err_cnt += 1

    if read_cnt == 0:
        log.error("- no reads!  %s", msg_fail)
        err_cnt += 1
    if base_cnt == 0:
        log.error("- no bases!  %s", msg_fail)
        err_cnt += 1

    run_type = "" # no reads - if passed to rqcws/su_validate then this field won't be updated

    if read_cnt > 0:
        run_type = "1x"
        if paired_flag:
            run_type = "2x"
        #run_type += "{:.1f}".format(base_cnt/float(read_cnt))
        run_type += str(int(base_cnt/float(read_cnt))) # 2x151
        
        
        

        log.info("- run type: %s", run_type)

    return read_cnt, base_cnt, run_type, err_cnt


'''
Post status to RQC about the validation results
(seq_unit_status = Transfer Complete or Transfer Failed)
'''
def update_rqc(valid_flag, read_cnt, base_cnt, run_type):
    log.info("update_rqc: %s, %s reads, %s bases, run_type: %s", valid_flag, read_cnt, base_cnt, run_type)

    status = "complete"
    if not valid_flag:
        status = "failed"

    rqc_body = { "seq_unit" : os.path.basename(my_file), "status" : status, "read_cnt" : read_cnt, "base_cnt" : base_cnt, "run_type" : run_type }
    post_page(RQC_URL, "api/rqcws/su_validate", rqc_body)


'''
Post status to RQC that we are "in progress"
'''
def rqc_in_progress(my_file):
    log.info("rqc_in_progress")

    rqc_body = { "seq_unit" : os.path.basename(my_file), "status" : "in progress" }
    post_page(RQC_URL, "api/rqcws/su_validate", rqc_body)


'''
Post using curl library
#$ curl -d '{"submitted-by-cid":123456, "sequencing-complete-library-names": [ "XXAH", "XXAC", "WTHW"]}' https://rqc-1.jgi-psf.org/api/rqcws/pacbio_ready
'''
def post_page(my_url, my_api, my_body):

    exit_code = 0
    response = None

    log.info("- post: %s/%s, %s", my_url, my_api, my_body)

    if my_url and my_api and my_body:

        curl = Curl(my_url)

        try:
            response = curl.post(my_api, my_body, "raw")

        except CurlHttpException as e:

            log.error("- %sFailed HTTPS API Call: %s/%s, %s: %s%s", color['red'], my_url, my_api, e.code, e.response, color[''])
            #code = e.code
            response = e.response
            exit_code = 10

        except Exception as e:
            log.error("- %sFailed PMOS API Call: %s/%s, %s%s", color['red'], my_url, my_api, e.args, color[''])
            exit_code = 10


    if response and exit_code == 0:
        log.info("- response: %s", json.dumps(response, indent=4, sort_keys=True))

    return response, exit_code

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__


if __name__ == "__main__":

    my_name = "Validate Fastq"
    version = "1.0.6"

    uname = getpass.getuser()

    # Parse options


    RQC_URL = "https://rqc.jgi-psf.org"
    if uname == "brycef":
        RQC_URL = "https://rqc-1.jgi-psf.org"

    usage = "%s, version %s\n" % (my_name, version)

    usage += """
Validates that the fastq is valid
- gets the base count
- gets the read count

    """


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)

    parser = ArgumentParser(usage = usage)

    parser.add_argument("-f", "--file", dest="file", type=str, help = "file to validate")
    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-nu", "--no-update", dest="no_rqc", action="store_false", help = "Do not update RQC")
    parser.add_argument("-p", "--pooled", dest="pooled", action="store_true", help = "indicates pooled fastq, expect multiple indexes")
    parser.add_argument("-nic", "--no-index-count", dest="index_count", action="store_false", help = "do not fail for multiple indexes (Hudson Alpha)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)




    my_file = None
    rqc_update_flag = True
    pooled_flag = False # only true for Pooled & Unknown files
    index_count_flag = False # don't fail if multiple indexes (HudsonAlpha data)
    print_log = False

    args = parser.parse_args()
    print_log = args.print_log
    rqc_update_flag = args.no_rqc
    pooled_flag = args.pooled
    index_count_flag = args.index_count

    if args.file:
        my_file = args.file

    if args.output_path:
        output_path = args.output_path



    if output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/t"
        rqc_update_flag = False

        if output_path == "t1":
            my_file = "11513.1.209186.CTTCGGCAGAA.fastq.gz"
            my_file = "11558.1.211283.TTGAACCAGCT.fastq.gz"
            #my_file = "bad16.fastq"
            #my_file = "bad2.fastq"
            #my_file = "bad0.fastq"
            #my_file = os.path.join(test_path, my_file)

            # should be 2x300
            # reads: 426704
            # bp: 128437904
            # bbtools: 426704, 128437904 (n_scaffods, scaf_bp)

        if output_path == "t2":
            # pacbio
            my_file = "pbio-1446.13272.fastq" # BOWHX
            my_file = os.path.join(test_path, my_file)

            # reads: 279850
            # bp: 820724995
            # bbttools: 279850, 820724995

        if output_path == "t3":
            #my_file = "11513.1.209186.fastq.gz"
            my_file = "30717.3.247457.ATTCAGAA.fastq.gz" # Hudson
            #pooled_flag = True
            
            # 2x301
            # reads: 47158308
            # bp: 14194650708

        if output_path == "t4":
            # 1x75
            my_file = "  11406.1.204531.AGTCTCA-AGTCTCA.fastq.gz "
            my_file = "11879.1.224416.UNKNOWN.fastq.gz"
            #pooled_flag = True

        output_path = os.path.join(test_path, output_path)





    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    log_file = os.path.join(output_path, "validate.log")

    # log = logging object
    log_level = "INFO"
    log = get_logger("gc_cov", log_file, log_level, print_log)



    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "file", my_file)
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "rqc_update_flag", rqc_update_flag)
    log.info("%25s      %s", "pooled_flag", pooled_flag)
    log.info("%25s      %s", "index_count", index_count_flag)

    log.info("")

    if rqc_update_flag:
        rqc_in_progress(my_file)

    validate_flag = False

    base_cnt = 0
    read_cnt = 0
    err_cnt = 0

    my_file = get_fs_location(my_file)

    if my_file:
        paired_flag = detect_paired_fastq()
        read_cnt, base_cnt, run_type, err_cnt = validate_seq_unit(paired_flag, pooled_flag)

    else:
        err_cnt += 1


    if err_cnt == 0:
        validate_flag = True
        log.info("- No errors, fastq passed all checks  %s", msg_ok)
    else:
        log.info("- %s errors, fastq does not pass  %s", err_cnt, msg_fail)


    if rqc_update_flag:
        update_rqc(validate_flag, read_cnt, base_cnt, run_type)


    log.info("%s Complete", my_name)

    sys.exit(0)



"""
                                                                                                   Excellent
                      `.--:::::::--.`     .-::////++/:-.
                `.:/+o++//:::::://+oooo+shsoo+++syyo++osso+:.
             .//+/:::--------------::////+syysohsoo+++++++++ss+.
          ./+/-----------------------:://////ohho+o+++o+oosssyhms.
        -+/-----------------------------:///////yho++oyyysooooo+oyo`
      -+/..------------------------------:://////+yyyyoo++++++++++oy.
    `o+..-------------------------------::-:///////sdooo+++++++++++oh.
   .s/..-------------------------------/o+s/:///////oh+o++++++++++o+oh`
  `y:..--------------------------------oo+y+-:///////mysyysso+++o++++y+
  o+..----------------------------------:::--:oo/////msoooosyyyso+o+++h
 .h-`.---------------------------------------+yys///sho++o+o+ooshyo+++d
 +s``.---------------------------------------://///omo+o++++++++oshs++y
 s/`..-------------------------------:+o/-------:/odyyys+oo+++o+++ohyy+
 y/`..-------------------------------/ys+-------:syo++syhooo+ooo++++yd`
 s/``.--------------------------------:/-------+yo/+ooosdyssyss++o++y:
 +o``.---------------------------------------:yo//++ooyo/:::/+sho++y:
 -h...---------------------------------------/d///+syd/-/oyso/:odss-
  o+`..---------------------------------------oy/:/+sd+:o++h////N/`
  `s/.`.----------------------------://--------ys///+ohs//+s///os
   `oo-..----------------------::/+oyy/--------:yo///+ohs////+s+`
    `yyo/::::-----------::///+yhy+:.+y----------:yo://+om++ys:.
    -y`.:omdssyysssssssoo+//::hd/   oo-----------:h+//shs/:h-
    .h`  `::`.yo://oh-`````    `  `+s:------------/ssso//:+y`
     /s:.`  `oo-----+s:.`      `./o+---------------://///:h.
      :dooosy/-------:+ooo+//+oo+/-------------------///:s/
      `s+:oy:.------------::::-----------------------:/:+y
       `+d+..----------------------------------------:/:o+
      `os-.-------------------------------------------//+s
     -y/..-:/+o+/------------------------------osoo/:-:/:s/```````
   `+s..--oo/::/yo---------------------------:+s/:hs+-://:yyyhhhyso/.`
  `oo..-------:+h+------------------------:+so::hs/----///:sdddhhhhhdho-`
 `s+..-----:/+yh+:---------------------:+so:-yss+-------///:+dNdyhdhhhhdmy+-`
 ++`----::/odh+::------------------:+oo+-++oso::---------:///:ohds+hdhhhhmdhho.
`h-.--::/ss:y+:---------------:+ooo+/so+os/::-------------://///odo:ymhhhhmmhhds-
-y.--:+yo.  s+---------:/+osss+:o+//yo::::------------------:////+h::hdhhhhmmhhhds.
-y.-/ys.    .+ooosssosdo+//y+//+/```:ooooos/------------------://+d:-/Nhhhhhmdhhhhd/
`o+o+.           `    .///:. ``           `/o+------------------:oy---NhhhhhhNdhhhhdo`
  `                                         `hhs/---------------:d-`--NhhhhhhdNhhhhhds`
                                            +yohhhs+:----------+h-   .Nhhhhhhhmmhhhhhds`
                                           -hosyyymsoooo/:-:/os+`    .NhhhhhhhhNhhhhhhds
                                           ysoyyyym:``-h/+oohs`      :myhhhhhhhmdhhhhhhmo
                                          :hoshyyyd:` +o.-:-/h/     `hyyhhhhhhhmdhhhhhhhm:
                                          -dosyyyhm/``m/.-::/ym.   -yNoyyhhhhdmdhhhhhhhhhd.
                                          :mhysyyymh++hy-:::+dos`:soodoyyhdmddhhhhhhhhhhhdy`
                                          hhohdysym+:/-:s+/oys-+o/-`yysymmdhhhhhhhhhhhhhhhm/
                                         :mosdyoyyds.`  :hohs-`    -doyydmdhhhhhhhhhhhhhhhhh`
                                         dhyhosyyydh.   s/:ys-`    syoyyyhdmhhdhhhhhhhhhhhhd+
                                        -Nodyoyyyyhm-  -s-:oy-`   -hoyyyyyymmhmhyhhhhhhhhhhhd`
                                        shohdoyyyyym+  o/-:+h:`  `yssyyyyyhNdydmyyhhhhhhhhhhm/
                                       `doshmyoyyyydy``h--:/d/.  +hoyyyyyhddhyhmhyyhhhhhhhhhhh
                                       :d+syhmoyyyyhm..y.-:/y+- .dosyyyyydmhyyymhyyhhhhhhhhhhN`
                        `.-::::::--:://yhshhhddsyyyyN/:s.--:oh-`yssyyyyydmhhyyyddyyyhhhhhhhhhm+
                      `+o+//:::://od:--::://+ohyhyyydh+o.:-:/h/syoyyyyydmhhyyyydmyyyyhhhhhhhhdy
                      s/--:/++++ooohsoo++/::---:sdhyyNso-:-:/sddoyyyyydmhhyyyyyhNyyyyhhhhhhhhhd`
                      oo-:/+yy--------ydhhhy/:--:mdhydNo-::-:omosyyyydmhhyyyyyyhNyyyyhhhhhhhhhN.
                    `-+doossyd/+++++++syhdddd/:--ymhyyNy::-::dyoyyyydmhhyyyyyyyhNyyyyhhhhhhhhhM+
                   -++:----::od/:--------/ohm+:--+Ndhydd:-::shoyyyymdhhyyyyyyyyhNyyyyhhhhhhhhhMh`
                  .y-.-:/++++smhyysosso+/:-:+/---:mNdyymo:-/dosyyhmdhhyyyyyyyyydmyyyyhhhhhhhhhMm-
                  -h--/+ooooo++ydd++yNmssh+:------smddyhd/:dssyyhmhhyyyyyyyyyyymdyyyyhhhhhhhhhNdo
                   /s//+++++++++ys+ohdmyood+:-----:mddmhhhyhsyyddhhyyyyyyyyyyyhmhyyyyhhhhhhhhdmhh`
                   `+ho/////+odyo+//::://+sy:------omhdmddNyyhmdhhyyyhyyyyyhhhdmdyyyyhhhhhhhhNdhd-
                  `s/.--://///my/////////::::-------smssyhdhdNdddddddddddhhhyyyyyyyyhhhhhhhhhNhhd+
                  .y--:/+sssssoossshyoohhys/::-------ss` ```/hsssssssssssssyyyyyyyyyhhhhhhhhmdhhhh
                   +s::///////////oh+..smhddo/:-------d.     +dyyyyyyyyyyyyyyyyyyyyhhhhhhhhdNhhhhd`
                   `/s////////////dN:..+mhhhms/:-----:d.     -myyyyyyyyyyyyyyyyyyhhhhhhhhhhmdhhhhd:
                     `+s+/////////do...ymhhhhms//::-:yo      :myyyyyyyyyyyyyyyyyhhhhhhhhhhmdhhhhhd+
                       `:+s+////+ys...+mhhhhhdhso+osyo.`````.ydhhhhhhhhhhhhhhhhhhhhhhhhhdmdhhhhhhds
                           -/+oyds/:/ymdhhyo/.  ./mmyo+//::+hdhhhhhhhhhhhhhhhhhhhddddmmmdhhhhhhhhhh
                                `..----.``       .dyhhhhddNddddddddddddddddddddddddhhhhhhhhhhhhhhhm
                                                 -moyyyyhhmhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhm`
                                                 -moyyyyyymsyyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhm.
                                                 :NoyyyyyyNoyyyyyyyyyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhm-
                                                 :NosyyyyyN+yyyyyyyyyyyyyyyyyyyyhhhhhhhhhhhhhhhhhhm-

$ jp2a image.jpg

"""
