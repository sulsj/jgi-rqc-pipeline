#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
For a fasta, split into individual fastas for each contig
SEQQC-9399

~/code/split_fa.py -f contamination.fasta -o /global/projectb/scratch/brycef/acdc/t1/s


"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
import re
from argparse import ArgumentParser



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

def fix_filename(my_file):
    new_file = my_file
    
    new_file = my_file.strip()
    new_file = re.sub(r'[^0-9A-Za-z\-\_\.]+', '.', new_file)
    if new_file.startswith("."):
        new_file = new_file[1:]
    new_file += ".fasta"
    
    return new_file
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program



if __name__ == "__main__":

    usage = "Use to split a fasta into one file per contig"

    parser = ArgumentParser(usage = usage)

    parser.add_argument("-f", "--fasta", dest="fasta", type = str, help = "Fasta to split")
    parser.add_argument("-o", "--output-path", dest="output_path", type = str, help = "Output path to write to, uses pwd if not set")

    fasta = ""
    output_path = ""

    args = parser.parse_args()


    if args.fasta:
        fasta = args.fasta

    if args.output_path:
        output_path = args.output_path

    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)
        
    print "Processing %s" % fasta
        
    #f = "/global/projectb/scratch/brycef/sag/BAGOX/pool_asm/pool_decontam_chaff.fasta"
    #output_path = "/global/projectb/scratch/brycef/sag/BAGOX/screen/chaff_align2"
    #suffix = "-crossblock-chaff.fasta"
    fof = os.path.join(output_path, os.path.basename(fasta).replace(".fasta", ".fof"))
    fh_fof = open(fof, "w")
                       
    fh = open(fasta, "r")
    bases = ""
    bcnt = 0 # base count
    bcnt_ttl = 0
    c_cnt = 0 # contig count
    
    my_header = None
    for line in fh:
        if line.startswith(">"):
            if bases:
                my_file = fix_filename(my_header)
                
                    
                new_f = os.path.join(output_path, my_file)
                fh_fof.write(os.path.basename(new_f) + "\n")
                fhw = open(new_f, "w")
                fhw.write(my_header)
                fhw.write(bases)
                fhw.close()
                bcnt = len(bases.replace("\n",""))
                print "%s|%s" % (my_file, bcnt)
                c_cnt += 1
                bcnt_ttl += bcnt
                bcnt = 0
            my_header = line
            bases = ""
        else:
            bases += line
            


    
    fh.close()

    if bases:
        my_file = fix_filename(my_header)

        new_f = os.path.join(output_path, my_file)
        bcnt = len(bases.replace("\n",""))
        fh_fof.write(os.path.basename(new_f) + "\n")
        fhw = open(new_f, "w")
        fhw.write(my_header)
        fhw.write(bases)
        print "%s|%s" % (my_file, bcnt)
        c_cnt += 1
        bcnt_ttl += bcnt
        fhw.close()
    
    fh_fof.close()
    
    print "-- %s contigs, %s bases" % (c_cnt, bcnt_ttl)
    
    sys.exit(0)
    
    
        
'''
___________________6666666___________________ 
____________66666__________66666_____________   
_________6666___________________666__________  Belial, Behemoth, Beelzebub
_______666__6____________________6_666_______  Asmodeus, Satanas, Lucifer
_____666_____66_______________666____66______ 
____66_______66666_________66666______666____   Count down together now
___66_________6___66_____66___66_______666___   and say the words that you will learn
__66__________66____6666_____66_________666__ 
_666___________66__666_66___66___________66__   Hail Satan, Archangelo
_66____________6666_______6666___________666_     Hail Satan, welcome Year Zero
_66___________6666_________6666__________666_       Hail Satan, Archangelo
_66________666__66_________66__666_______666_         Hail Satan, welcome Year Zero
_66_____666______66_______66______666____666_ 
_666__666666666666666666666666666666666__66__             Ghost - Year Zero
__66______________66____66______________666__ 
___66______________66___66_____________666___    
____66______________6__66_____________666____ 
_______666___________666___________666_______ 
_________6666_________6_________666__________ 
____________66666_____6____66666_____________ 
___________________6666666___________________

    
'''
