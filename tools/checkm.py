#!/usr/bin/env python
'''
    CheckM wrapper for RQC pipelines.

    Run example:
        checkm.py -f ASM_FASTA_ABS_PATH -o OUT_DIR -r d-bac                     (built-in domain geneset of bacteria)
        checkm.py -f ASM_FASTA_ABS_PATH -o OUT_DIR -r d-arc                     (built-in domain geneset of archaea)
        checkm.py -f ASM_FASTA_ABS_PATH -o OUT_DIR -r lwf                       (use lineage workflow)

    Return 1 if CheckM fails, else return 0.

    Shijie Yao
    Last Modified:
        10/11/2016 - initial implementation : produce report.txt file
        10/20/2016 - add summary.txt and contig_hits.txt files


2017-10-18: v1.1
- changed to use log.info format
- took out geneset & user option
- updated to use common.py run_cmd (handles Cori, Denovo, Genepool)
- simplified code and removed unused sections
- removed status tracking
* updates by Bryce



Bryce 2017-10-17:
latest checkm = 1.0.7 = Sept 2 2016, 1.0.5 = Nov 2, 2015
https://github.com/Ecogenomics/CheckM/wiki

Shifter:
shifter --image=registry.services.nersc.gov/jgi/checkm \
checkm taxonomy_wf -t 8 -x fasta domain Archaea \
/global/projectb/scratch/brycef/iso/BTHZO/trim \
/global/projectb/scratch/brycef/pbmid/CAHGT/miid/checkm/arc/out
-x = look for fasta files
-t = threads

helper - runs all 3 at once, 8 threads each (3*8 = 24)
./checkm_helper.py \
-i /global/projectb/scratch/brycef/iso/BTHZO/trim/scaffolds.trim.fasta \
-o /global/projectb/scratch/brycef/iso/BTHZO/iso --verbose

./checkm.py \
-f /global/projectb/scratch/brycef/iso/BTHZO/trim/scaffolds.trim.fasta \
-o /global/projectb/scratch/brycef/iso/BTHZO/iso/checkm \
-r bac -t 8 -pl

* lwf is much longer

'''
import os
import sys
from argparse import ArgumentParser
import numpy as np
import matplotlib
import re
import json

matplotlib.use("Agg") ## This needs to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import AutoMinorLocator, MultipleLocator, FormatStrFormatter

# custom libs in '../lib/'
CUR_DIR = os.path.dirname(os.path.realpath(__file__))   # so will work even file is linked
sys.path.append(os.path.join(CUR_DIR, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from common import get_logger, run_cmd


'''
    RUN CheckM
jdir : job directory
fasta : fasta file (abs path)
rtype : run type
thr : threads to use in checkm
- takes ~ 1 minute
'''
def do_checkm(jdir, fasta, rtype, thr):
    log.info("do_checkm: %s, %s, %s, %s", jdir, fasta, rtype, thr)

    #opt = None
    checkm_cmd = None
    if rtype == 'lwf':
        ##- checkm lineage_wf -t 10 GENOME_DIR OUT_DIR
        checkm_cmd = "lineage_wf -t %s" % thr
    elif rtype == 'd-bac':
        ##- checkm taxonomy_wf -t 10 domain Bacteria GENOME_DIR OUT_DIR
        checkm_cmd = "taxonomy_wf -t %s domain Bacteria" % thr
    elif rtype == 'd-arc':
        ##- checkm taxonomy_wf -t 10 domain Archaea GENOME_DIR OUT_DIR
        checkm_cmd = "taxonomy_wf -t %s domain Archaea" % thr

    checkm_path = os.path.join(jdir, "out")
    if not os.path.isdir(checkm_path):
        os.makedirs(checkm_path)
        log.info("- created path: %s", checkm_path)

    # symlink fasta to toplevel path in jdir
    fna = os.path.join(jdir, "contigs.fna")
    if not os.path.isfile(fna):
        log.info("- symlink fasta to .fna file")
        cmd = "ln -s %s %s" % (fasta, fna)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    run_log = os.path.join(jdir, "checkm_run.log")
    # checkm options input_folder output_folder
    checkm_cmd += " %s %s > %s 2>&1" % (os.path.dirname(fna), checkm_path, run_log)


    cmd = "#checkm;checkm %s" % checkm_cmd
    std_out, std_err, exit_code = run_cmd(cmd, log) # automatically handles genepool, denovo, cori

    ckmstats = os.path.join(checkm_path, "storage", 'bin_stats_ext.tsv') # the CheckM run produced stats file
    ctistats = os.path.join(checkm_path, "storage", 'marker_gene_stats.tsv') # the CheckM run produced stats file

    if exit_code == 0:
        if not os.path.isfile(ckmstats):
            log.error('checkm run did not produce the stats file : [%s]', ckmstats)
            sys.exit(1)
    else:
        log.error('checkm run failed : [%s]', std_err.strip())
        sys.exit(1)

    return ckmstats, ctistats


def process_bin_stats_ext(fasta, ckmstats, rptfile, sumfile):
    'helper. caller must be sure that ckmstats exists'

    with open(ckmstats, 'r') as FH:
        lines = FH.readlines()

    rslt = {}
    if lines:
        data = lines[0].split('\t')[1].strip().replace('\'', '"')
        try:
            rslt = json.loads(data)
        except Exception as e:
            log.error('  Error : failed to convert to json - %s', str(e))
            sys.exit(1)
    else:
        log.error('  Error : checkm stats file is empty [%s]', ckmstats)
        sys.exit(1)

    if rslt:
        rpt_header = ['Filename']
        rpt_data = [fasta]

        sum_header = ['#Filename','GenomeSize','FoundSCGenes','TotalSCGenes','EstGenomeComSimple','EstGenomeSizeSimple','EstGenomeComNormalized','EstGenomeSizeNormalized']
        sum_data = [fasta, str(rslt['Genome size'])]
        scg_total = 0
        scg_found = 0

        for idx, kname in enumerate(['GCN0', 'GCN1', 'GCN2', 'GCN3', 'GCN4', 'GCN5+']):
            for acn in rslt[kname]:
                rpt_header.append(str(acn))
                scg_total += 1
                if idx > 0:
                    scg_found += 1

                rpt_data.append(str(idx))

        ##- create the report file
        with open(rptfile, 'w') as FH:
            FH.write('%s\n' % '\t'.join(rpt_header))
            FH.write('%s\n' % '\t'.join(rpt_data))

        ##- create the summary file
        norm_factor = 0.9 # a const
        gsize = float(rslt['Genome size'])
        sum_data.append(str(scg_found))     # FoundSCGenes
        sum_data.append(str(scg_total))     # TotalSCGenes
        simp_comp_pct = float(scg_found) / float(scg_total)
        sum_data.append('%.4f' % simp_comp_pct)             # EstGenomeComSimple
        if simp_comp_pct > 0:
            sum_data.append('%d' % int(gsize / simp_comp_pct))   # EstGenomeSizeSimple
        else:
            sum_data.append('NA')
        norm_comp_pct = min(1.0, scg_found / (scg_total * norm_factor))
        sum_data.append('%.4f' % norm_comp_pct)             # EstGenomeComNormalized
        if norm_comp_pct > 0:
            sum_data.append('%d' % int(gsize / norm_comp_pct))  # EstGenomeSizeNormalized
        else:
            sum_data.append('NA')

        with open(sumfile, 'w') as FH:
            FH.write('%s\n' % '\t'.join(sum_header))
            FH.write('%s\n' % '\t'.join(sum_data))

'''
ctistats = storage/marker_gene_stats.tsv
output is sorted on the 1st marker gene name
'''
def process_marker_gene_stats(ctistats, rptfile):
    'helper. caller must be sure that ctistats exists'
    with open(ctistats, 'r') as FH:
        lines = FH.readlines()
    rslt = {}
    if lines:
        data = lines[0].split('\t')[1].strip().replace('\'', '"')
        try:
            rslt = json.loads(data)
        except Exception as e:
            log.error('  Error : failed to convert to json - %s', str(e))
            sys.exit(1)

        out = {}
        for ctg in rslt:
            rct = rslt[ctg]
            line = '%-2d %s - ' % (len(rct), ctg)
            gtag = None
            for idx, gname in enumerate(rct):
                line += '%s:%s; ' % (gname, str(rct[gname]))
                if idx == 0:
                    gtag = '%s:%s' % (gname, ctg)
            out[gtag] = line

        with open(rptfile, 'w') as FH:
            FH.write('#HIT_GENE_CNT     GENE_IN_CONTIG - GENE_NAMEs:MATCH_COORDs\n')
            for gname in sorted(out.keys()):
                FH.write('%s\n' % out[gname])

    else:
        log.error('  Error : checkm contig stats file is empty [%s]', ckmstats)
        sys.exit(1)

'''
ctistats = storage/marker_gene_stats.tsv
for each marker gene, how many different contigs matched
'''
def contigs_per_marker_gene(ctistats, rptfile):
    'helper. caller must be sure that ctistats exists'
    with open(ctistats, 'r') as FH:
        lines = FH.readlines()
    rslt = {}
    if lines:
        data = lines[0].split('\t')[1].strip().replace('\'', '"')
        try:
            rslt = json.loads(data)
        except Exception as e:
            log.error('  Error : failed to convert to json - %s', str(e))
            sys.exit(1)

        out = {}
        for ctg in rslt:
            rct = rslt[ctg]
            line = '%-2d %s - ' % (len(rct), ctg)

            m = re.match(r'(.*)_\d+_\d+', ctg)  # extract contig name : in format of CONTIG_NAME_#_#
            if m:
                ctg = m.group(1)

            for idx, gname in enumerate(rct):
                hitcoord = str(rct[gname])
                if gname not in out:
                    out[gname] = {ctg: [hitcoord]}
                elif ctg not in out[gname]:
                    out[gname][ctg] = [hitcoord]
                else:
                    out[gname][ctg].append(hitcoord)

        with open(rptfile, 'w') as FH:
            FH.write('#CONTIG_CNT     GENE_NAME     CONTIGs AND MATCH COORDs\n')
            for gname in sorted(out.keys()):
                cnt = len(out.get(gname))
                line = '%d\t%s\t' % (cnt, gname)
                for ctg in out[gname]:
                    line += '%s:%s; ' % (ctg, ','.join(out[gname][ctg]))
                FH.write('%s\n' % line)
    else:
        log.error('  Error : checkm contig stats file is empty [%s]', ckmstats)
        sys.exit(1)

'''
    data : hash with gene_name : copies
    gtype : gene set type, for graph label
    ofile : file name
'''
def create_histogram(data, gtype, ofile):
    y = []
    cnt = []
    ghash = {}
    def add2hash(x):
        if ghash.get(x):
            ghash[x] += 1
        else:
            ghash[x] = 1

    gene_count = data.values()

    for val in gene_count:
        if val in gene_count:
            add2hash(val)

    for n in reversed(xrange(max(gene_count)+1)):
        y.append(n)
        if ghash.get(n):
            cnt.append(ghash.get(n))
        else:
            cnt.append(0)

    ind = np.arange(len(y))

    width = 0.65 # the width of the bars
    fig, ax = plt.subplots()

    fig.set_figwidth(10)
    fig.set_figheight(3)

    rects1 = ax.barh(ind, cnt, width, color='b') #, align='center')
    ax.set_ylabel('Copy Number', fontsize=16)

    plt.yticks(ind + width/2, y)

    xlbs = 10
    if max(cnt) > 200:
        xlbs = 20
    xind = np.arange(0, max(cnt)+10, xlbs)
    ax.set_xticks(xind)
    ax.set_xticklabels(xind)
    ax.set_xlabel('Gene Count', fontsize=16)

    title = "%s Single-copy Genes Histogram (N=%d)" % (gtype, len(data))
    ax.set_title(title, fontsize=20, y=1.01)

    ax.grid(True)
    plt.xticks(size=14)
    plt.yticks(size=14)

    # show value on tip of bar
    for rect in rects1:
        val = rect.get_width()
        color = 'black'
        xshift = 2
        yshift = width / 4
        if val == max(cnt):
            xshift = - val * 0.05
            color = 'white'
        elif max(cnt) > 200:
            xshift = 10
        elif max(cnt) > 100:
            xshift = 5

        if len(cnt) > 5:
            yshift = 0.005
        elif len(cnt) > 4:
            yshift = width / 5

        if val > 0:
            x = rect.get_x() + val + xshift
            y = rect.get_y() + yshift
            #print('x0=%d; x=%d; y=%d; xshift=%d' % (rect.get_x(), x, y, xshift))
            ax.text(x, y,'%d' % val, ha='center', va='bottom', fontsize=14, color=color)

    plt.tight_layout()  # fix the missing x axis label issue
    plt.autoscale()
    plt.savefig(ofile, dpi=fig.dpi)


def create_bar_chart(data, gtype, ofile, small=False):
    # sort x label
    gnames = sorted(data)
    gcount = []
    for gn in gnames:
        gcount.append(data[gn])

    total_gene_count = len(gnames)

    if small:
        tgnames = []
        tgcount = []
        scgnames = []
        for idx, gn in enumerate(gnames):
            if gcount[idx] != 1:
                tgnames.append(gn)
                tgcount.append(gcount[idx])
            else:
                scgnames.append(gn)

        gnames = tgnames
        gcount = tgcount

    # png size
    fw = 120
    fh = 40
    ylabel_fsize = 100
    title = '%s Single-copy Genes (N=%d)' % (gtype, total_gene_count)
    title_fsize = 114
    xticks_size = 60
    yticks_size = 90
    if small and gnames:
        fw = 60
        ylabel_fsize = 60
        title_fsize = 60
        xticks_size = 40
        yticks_size = 60
        nsc = 'genes'
        if len(gnames) == 1:
            nsc = 'gene'
        title = "%s Single-copy Genes (N=%d; %d %s not in single copy; the other %d genes found in single copy]" % \
                    (gtype, total_gene_count, len(gnames), nsc, len(scgnames))
    elif total_gene_count > 110:
        if total_gene_count < 200:
            xticks_size = 50
        else:
            xticks_size = 40

    if gnames:
        ind = np.arange(len(gcount))
        width = 0.45 # the width of the bars
        fig, ax = plt.subplots()

        fig.set_figwidth(fw)
        fig.set_figheight(fh)

        rects1 = ax.bar(ind, gcount, width, color='b')
        ax.set_ylabel('Copy Number', fontsize=ylabel_fsize)

        ax.set_title(title, fontsize=title_fsize, y=1.005) # y= : move title up a little

        ax.set_xticks(ind + width/2)
        ax.set_yticks(np.arange(max(gcount)+1))
        ax.set_xticklabels(gnames, rotation='vertical')
        plt.xticks(size=xticks_size)
        plt.yticks(size=yticks_size)

        # plt.tight_layout()
        plt.autoscale()
        plt.savefig(ofile, dpi=fig.dpi)


'''
    Create files of well-known formats that other RQC reporting tools can use.
    ckmstats : OUT/storage/bin_stats_ext.tsv (copy count info)
    ctistats : OUT/storage/marker_gene_stats.tsv (contig hits info)
'''
def do_postprocessing(jdir, fasta, ckmstats, ctistats, rtype):
    log.info('Run postprocessing: %s, %s, %s, %s, %s', jdir, fasta, ckmstats, ctistats, rtype)

    ##- create contig hits report file
    if not os.path.isfile(ctistats):
        log.error('checkm contig gene stats file not exist : [%s]', ctistats)
        sys.exit(2)

    rptfile = os.path.join(jdir, 'contig_hits.txt')
    process_marker_gene_stats(ctistats, rptfile)

    rptfile = os.path.join(jdir, 'gene_contigs.txt')
    contigs_per_marker_gene(ctistats, rptfile)

    ##- create gene count report and summary files
    if not os.path.isfile(ckmstats):
        log.error('checkm stats file not exist : [%s]', ckmstats)
        sys.exit(2)

    log.info('- processing checkm stats file [%s]', ckmstats)

    rptfile = os.path.join(jdir, 'report.txt')
    sumfile = os.path.join(jdir, 'summary.txt')
    process_bin_stats_ext(fasta, ckmstats, rptfile, sumfile)

    # create png file
    if os.path.isfile(rptfile):
        with open(rptfile, 'r') as fh:
            lines = fh.readlines()

        gnames = lines[0].split()[1:]
        gcount = [int(x) for x in lines[1].split()[1:]]

        ghash = {}
        for idx, gname in enumerate(gnames):
            ghash[gname] = gcount[idx]

        gtype = 'lineage wf'
        if rtype == 'd-bac':
            gtype = 'Bacteria'
        elif rtype == 'd-arc':
            gtype = 'Archaea'

        pngfile = 'checkm_chart_%s.png' % rtype
        create_bar_chart(ghash, gtype, os.path.join(jdir, pngfile))

        pngfile = 'checkm_chart_%s_nsc.png' % rtype
        create_bar_chart(ghash, gtype, os.path.join(jdir, pngfile), True)

        pngfile = 'checkm_histogram_%s.png' % rtype
        create_histogram(ghash, gtype, os.path.join(jdir, pngfile))



if __name__ == '__main__':
    NAME = 'CheckM Wrapper'
    VERSION = 'version 1.1'

    # Parse options
    USAGE = '\n%s [options]\n' % sys.argv[0]
    USAGE += '\n* Single copy gene evaluation using CheckM v1.0.5 *\n'
    USAGE += '\nExample: %s -f /global/projectb/sandbox/gaag/TestData/Meta/mock_community/references/Clostridium_perfringensATCC_13124.fasta -o ckm_test -r d-bac\n' \
        % os.path.basename(sys.argv[0])

    JOB_CMD = ' '.join(sys.argv)
    PARSER = ArgumentParser(usage=USAGE)

    RUN_TYPE = ['lwf', 'd-bac', 'd-arc'] # lineage-wf, domain bacteria, domain archaea,
    PARSER.add_argument('-f', '--fasta', dest='fasta', type=str, help='The assembly to be evaluated (full path to fasta)', required=True)
    PARSER.add_argument('-o', '--output-dir', dest='outdir', help='Output dir name', required=True)
    PARSER.add_argument('-r', '--run-type', dest='rtype', choices=RUN_TYPE, help='Run type', required=True)
    PARSER.add_argument('-t', '--threads', dest='thread', help='Number of thread', default=16)
    PARSER.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    PARSER.add_argument('-v', '--version', action='version', version=VERSION)

    ARGS = PARSER.parse_args()
    print_log = ARGS.print_log # write to stdout or log file

    ## create the output dir and move into it
    PIPEDIR = os.path.abspath(ARGS.outdir)
    if not os.path.exists(PIPEDIR):
        os.makedirs(PIPEDIR)


    # initialize my logger
    log_level = 'INFO'
    log_file = os.path.join(PIPEDIR, "checkm-%s.log" % ARGS.rtype)

    log = get_logger("checkm", log_file, log_level, print_log)

    log.info("%s: %s", NAME, VERSION)
    log.info(JOB_CMD)
    log.info('---------------------------------------------')
    log.info('%15s : %s', 'JOB DIR', PIPEDIR)
    log.info('%15s : %s', 'INPUT', ARGS.fasta)
    log.info('%15s : %s', 'RUN TYPE', ARGS.rtype)
    log.info('%15s : %s', 'threads', ARGS.thread)
    log.info('%15s : %s', 'RUN LOG', log_file)
    log.info('---------------------------------------------')


    # I would use "bac", "arc" instead of "d-bac", "d-arc" but pipelines expect pngs to be named checkm_chart_d-bac.png ....
    job_path = os.path.join(PIPEDIR, ARGS.rtype.replace("d-", ""))

    fasta = os.path.realpath(ARGS.fasta)

    ckmstats, ctistats = do_checkm(job_path, fasta, ARGS.rtype, ARGS.thread)

    do_postprocessing(job_path, fasta, ckmstats, ctistats, ARGS.rtype)

    log.info('Pipeline complete for %s!', ARGS.rtype)

    sys.exit(0)
