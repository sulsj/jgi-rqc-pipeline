package main

/*
 
 Created by Bryce
 
v 1.0 - 2016-12-12
- might have problem with last base
v 1.1 - 2017-01-09
- read from fastq
v 1.2 - 2017-01-11
- handle gzipped fastq or fasta as query, handle gzipped fasta as reference
v 1.3 - 2017-01-12
- base hits calculation fixed, color changed
v 1.4 - 2017-02-07
- use integers to store kmers like bbtools
v 1.4.1 - 2017-06-28
- work with lower case bases
v 1.4.2 - 2017-07-12
- print header name in footer
- fix for color going too far

To do:
- multiple files for query with output with different colors
- mask middle should be an option

Tests:
./kviz -q BCWAY.fastq -r BCWAY.fa
./kviz -q BCWAY.fastq.gz -r BCWAY.fa
./kviz -q BCWAY.fastq -r BCWAY.fa.gz
-- total base hits: 2221/72579 = 3.06%
-- contigs with kmer hits: 4/5 = 80.00%

2016-02-07:
-- total base hits: 4163/72579 = 5.74%
-- contigs with kmer hits: 5/5 = 100.00%

./kviz -q /global/projectb/scratch/brycef/sag/tests/BCWAY/subsample/10846.1.181577.TAGGCAT-CTAGTCG.anqdpht.norm.subsample.fastq.gz -r BCWAY.fa

./kviz -q test.fasta -r test-ref.fasta -k 7
- 103/185
- k = 5 = 132/185

** test kmer wrapping over a line

$ module load golang/1.4.2
$ go build kviz.go

 */


import "strings"
import "fmt"
import "os"
import "bufio"
import "bytes"
import "flag"
import "compress/gzip"


/*
 * global vars
 */

var kmer_map = make(map[int64]int)
var base_to_num [128]int8
var num_to_base [128]string


/*
 * Functions
 */


/*
 * reverse compliment
 * - compliment is easy &kmer, reversing not so easy 
 */
func reverse_comp(kmer int64, kmer_size int) int64 {
    kmer = ^kmer // compliment
    // now reverse it
    var x int64 = 0
    var mini_mask = int64(3) // 00000011
    for i := 0; i < kmer_size; i++ {
        num := kmer & mini_mask
        x = x << 2 // shift by 2 to open lowest 2 bits to add num
        
        x = x | num
        kmer = kmer >> 2 // get next base to run 
    }
    
    return x
    
}


/*
 * return a/b as a percentage 
 */
func pct(a int, b int) string {

    var percent string = "0.00%"    
    if b > 0 {
        percent = fmt.Sprintf("%.2f%%", 100.0 * float64(a)/float64(b))
    }
    return percent

}


/*
 * take a line of bases and convert to int kmers
 * returns number of matching bases for mode = 1
 */
func get_seq_val(kmer string, kmer_size int, ref_id int, mode int8, base_len int, hdr string) int {
    
    var kmer_val int64 = 0
    var b int8 // byte = val of a base
    var x int8 = 0 // val of 0,1,2,3,-1
    
    // uint32 = amount to shift, cannot shift by a signed int only shift by a unsigned32
    // int64 = value of the kmer (2^31 = big number)
    var shift uint32 = uint32(2 * kmer_size) // how many 1 bits do we need? 11111111
    var mask int64 = ^(-1 << shift) // -1 = 1111111111111111, shifted = 11111111...1000000000, inverted^ = 000000..0011111111
    var middle_mask int64 = ^(int64(3) << uint32(kmer_size / 2)) // int64(3) = 11, shifted k/2 positions to mask the middle = 0000000110000000, inv = 11111111001111111     
    var length int = 0
    
    var buffer bytes.Buffer
    var base_cnt int = 0 // total number of bases
    var base_match = 0 // number of bases matching kmers
    var c string
    var found_id int = 0
    var mm_flag bool = true // mask middle
    
    var my_color string = "\033[1;37m\033[1;41m" // white text on red background (ANSI escape codes)
    //var pink string = "\033[1;35m"
    var no_color string = "\033[m"
    
    var print_map = make(map[int] int) // base id in the list and the reference id for printing

    
    for i :=0 ; i < len(kmer); i++ {
        
        b = int8(kmer[i]) // int8 byte (value of char, A = 65...)
        
        x = base_to_num[b] //(A/65 = 00)
        
        if x >= 0 {
            //  shift to left by 2  to open 2 lowest bits
            // mask saves the last 8 bits
            // | moves our new value into the 2 lowest bits
            kmer_val = ((kmer_val << 2) & mask) | int64(x)
            length++

            if length >= kmer_size {
                
                rcomp := reverse_comp(kmer_val, kmer_size)
                // store the highest kmer/rcomp kmer to not store duplicate info (e.g. AAAA -> TTTT)
                //if (rcomp > kmer_val) {
                //   kmer_val = rcomp
                //}
                //kmap[kmer_val]++
                
                // mask middle to an A
                if mm_flag {
                    mm := kmer_val & middle_mask
                    mm_rc := rcomp & middle_mask
                
                    // store greatest value
                    if mm_rc > mm {
                        mm = mm_rc
                    }

                
                    if mode == 0 {
                        kmer_map[mm] = ref_id
                    } else {
                        found_id = kmer_map[mm]
                        pos := i - kmer_size + 1
                        if pos < 0 {
                            pos = 0
                        }
                        print_map[pos] = found_id                        
                        
                    }
                    
                } else {
                    
                    if mode == 0 {
                        kmer_map[kmer_val] = ref_id
                    } else {
                        found_id = kmer_map[kmer_val]
                        //fmt.Printf("- %d = %d = %s, %s\n", kmer_val, found_id, get_kmer_str(kmer_val, kmer_size), string(kmer[i]))
                        // store the i for the kmer ...
                        pos := i - kmer_size + 1
                        if pos < 0 {
                            pos = 0
                        }
                        print_map[pos] = found_id
                    }
                }

            }
            
        } else {
            length = 0
        }

    }
    

    // print results
    if mode == 1 && len(kmer) > 0 {
        var kcnt int = 0
        var color_on bool = false
        
        for i := 0 ; i < len(kmer); i++ {
            base_cnt++
            
            if print_map[i] > 0 {
                // color next kmer bases
                kcnt = kmer_size
            }
            
            if kcnt > 0 {
                kcnt--
                base_match++
                if color_on == false {
                    buffer.WriteString(fmt.Sprintf("%s", my_color))
                    color_on = true
                }
            } else {
                if color_on == true {
                    buffer.WriteString(fmt.Sprintf("%s", no_color))
                    color_on = false
                }
                
            }
            
            buffer.WriteString(fmt.Sprintf("%s", string(kmer[i])))
            if base_cnt % base_len == 0 {
                buffer.WriteString(fmt.Sprintf("%s\n", no_color))
                color_on = false // turn it on next line
            }
        }

    

        buffer.WriteString(fmt.Sprintf("%s\n", no_color))
    
        // how much was matching?
        c = fmt.Sprintf("- %s base hits: %d/%d = %s\n", hdr, base_match, base_cnt, pct(base_match, base_cnt))
        buffer.WriteString(c)
        
        fmt.Println(buffer.String());        
        
    }
    
    return base_match
    
}


/*
 * Convert number to a string (773 = 11 00 00 01 01 = T A A C C)
 * - for debugging
 */
func get_kmer_str(kmer_val int64, kmer_size int) string {
        // convert i into string
        var s[] string
        var kmer_str string
        
        var mini_mask = int64(3) // 00000011
        
        for i := 0; i < kmer_size; i++ {
     
            // returns it backwards
            // this is how Brian does it
            n := kmer_val & mini_mask
            kmer_val = kmer_val >> 2
            
            
            // should work to get front to back
            //mini_mask = int64(3) << uint32(kmer_size * 2 - j * 2 - 2)
            //n := t & mini_mask
            
            b := num_to_base[n]
            s = append(s, b)
            // append to array
            //fmt.Printf("%s", b)

        }
        
        for i := 0; i < len(s)/2; i++ {
            j := len(s) - i - 1
            s[i], s[j] = s[j], s[i]
        }
        
        kmer_str = strings.Join(s[:], "")
        //fmt.Printf("\n")
        // 431 = 01 10 10 11 11 = C G G T T
        return kmer_str

}

/*
 * add kmers from query_file (fasta format) to our kmer_map
 * handles .fa or .fa.gz (or .fasta, .fasta.gz)
 */
func read_fasta(query_file string, kmer_size int, ref_id int) {

    var line string
    var lcnt int = 0 // count of lines in file
    var bases string

    
    f, err := os.Open(query_file)
    if err != nil {
        panic(err)
    }
    defer f.Close() // defer = close once function finishes
 
    scan := bufio.NewScanner(f)
    
    if strings.HasSuffix(query_file, ".gz") {
        gz, err := gzip.NewReader(f)
        if err != nil {
            panic(err)
        }
        
        defer gz.Close()

        scan = bufio.NewScanner(gz)
    }

    
    for scan.Scan() {
        lcnt ++
        
        line = scan.Text()

        if line[0] == '>' {
            get_seq_val(bases, kmer_size, ref_id, 0, 0, "")
            bases = ""
            
        } else {
            bases += line
        }        
        
    }


    if len(bases) > 0 {
        get_seq_val(bases, kmer_size, ref_id, 0, 0, "")
        bases = ""
    }
    

    fmt.Printf("- processed fasta query: %s, kmers = %d, lines = %d\n\n", query_file, len(kmer_map), lcnt)
    
}




/*
 * add kmers from query_file (fasta format) to our kmer_map
 * handles .fastq or .fastq.gz
 */
func read_fastq(query_file string, kmer_size int, ref_id int) {

    var line string
    var lcnt int = 0 // count of lines in file
    
    f, err := os.Open(query_file)
    if err != nil {
        panic(err)
    }
    defer f.Close() // defer = close once function finishes
    
    scan := bufio.NewScanner(f)
    
    if strings.HasSuffix(query_file, ".gz") {
        gz, err := gzip.NewReader(f)
        if err != nil {
            panic(err)
        }
        
        defer gz.Close()

        scan = bufio.NewScanner(gz)
    }



    
    for scan.Scan() {
        lcnt ++
        
        line = scan.Text()

        // 1 = header
        // 2 = bases
        // 3 = + comment
        // 4 = qual score
        
        if lcnt % 4 == 2 {

            //fmt.Printf("B: %s\n", line)
            // add to kmer map
            get_seq_val(line, kmer_size, ref_id, 0, 0, "")
        } 
        
    }

    fmt.Printf("- processed fastq query: %s, kmers = %d, lines = %d\n\n", query_file, len(kmer_map), lcnt)
    
}
    

/*
 * Main code
 *
 */
func main() {
    
    var version string = "KViz 1.4.2"
    fmt.Printf("%s[ %s ]%s\n", strings.Repeat("-", 35), version, strings.Repeat("-", 35))
    var q_file = flag.String("q", "", "path to query file")
    var ref_file = flag.String("r", "", "path to reference file")
    var kmer_size_ptr = flag.Int("k", 31, "kmer size (default 31)")
    
    flag.Parse()
    
    fmt.Printf("query file: %s\n", *q_file)
    fmt.Printf("ref file: %s (show matching kmers from query)\n", *ref_file)
    
    
    var kmer_size int = *kmer_size_ptr
    if (kmer_size > 31) {
        kmer_size = 31
    } else if (kmer_size < 1) {
        kmer_size = 1
    }
    fmt.Printf("kmer size: %v (actual: %v)\n", *kmer_size_ptr, kmer_size)
                       
    var base_len int = 70 // 70 bases per line in a fasta
    

    // Bases -> Binary and vice versa
    base_to_num['A'] = 0 // 'A' = 65
    base_to_num['C'] = 1 // 01
    base_to_num['G'] = 2 // 10
    base_to_num['T'] = 3 // 11
    base_to_num['N'] = -1 // no
    base_to_num['a'] = 0 // 'A' = 65
    base_to_num['c'] = 1 // 01
    base_to_num['g'] = 2 // 10
    base_to_num['t'] = 3 // 11
    base_to_num['n'] = -1 // no    
    num_to_base[0] = "A"
    num_to_base[1] = "C"
    num_to_base[2] = "G"
    num_to_base[3] = "T"
    
    var bm int
    var ref_id int = 1
    // loop through list of references, increment ref_id 
    
    if strings.HasSuffix(*q_file, "fastq.gz") || strings.HasSuffix(*q_file, "fastq") {
        read_fastq(*q_file, kmer_size, ref_id)
    } else if strings.HasSuffix(*q_file, ".fasta") || strings.HasSuffix(*q_file, ".fa") {
        read_fasta(*q_file, kmer_size, ref_id)    
    } else {
        fmt.Printf("Error: format not handled\n")
        os.Exit(2)
    }
    
    
    // list the kmers - debugging
    if 1==2 {
        //get_kmer_string(kmer_int, kmer_size)
        for i, v:= range kmer_map {
            kmer := get_kmer_str(i, kmer_size)
            rcomp := get_kmer_str(reverse_comp(i, kmer_size), kmer_size)
            fmt.Printf("- %d = %d = %s, rcomp = %s\n", i, v, kmer, rcomp)
        }

    }
    
    
    // read through the ref
    fr, err := os.Open(*ref_file)
    if err != nil {
        panic(err)
    }
    defer fr.Close()

    scanr := bufio.NewScanner(fr)
    // if gzipped, use gzip reader
    if strings.HasSuffix(*ref_file, ".gz") {
        gz, err := gzip.NewReader(fr)
        if err != nil {
            panic(err)
        }
        
        defer gz.Close()

        scanr = bufio.NewScanner(gz)
    }
    
    
    // go through reference fasta and show what parts have kmer matches
    var lcnt int = 0
    var total_base_match int = 0
    var total_base_cnt int = 0
    
    var contig_match int = 0
    var contig_cnt int = 0
    var bases string
    var line string
    var hdr string
    
    for scanr.Scan() {
        lcnt ++
        
        line = scanr.Text()
        if line[0] == '>' { // single quote changes '>' to byte which can be compared to string[x] too
            contig_cnt++
            
            bm = get_seq_val(bases, kmer_size, 0, 1, base_len, hdr)

            total_base_cnt += len(bases)
            total_base_match += bm
            if bm > 0 {
                contig_match++
            }
            
            bases = ""
            fmt.Printf("%s\n", line)
            hdr = line
            
        } else {
            bases += line
        }
    }
    
    
    if len(bases) > 0 {
        bm = get_seq_val(bases, kmer_size, 0, 1, base_len, hdr)
        total_base_cnt += len(bases)
        total_base_match += bm
        if bm > 0 {
            contig_match++
        }
    }

    
    fmt.Printf("\n")
    fmt.Printf("-- total base hits: %d/%d = %s\n", total_base_match, total_base_cnt, pct(total_base_match, total_base_cnt))
    fmt.Printf("-- contigs with kmer hits: %d/%d = %s\n", contig_match, contig_cnt, pct(contig_match, contig_cnt))
    fmt.Printf("%s\n", strings.Repeat("-", 80))

}
