#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Tool to save one-off files into JAMO
- JAMO missing a command line api for this ... Chris does not want to save files in JAMO without a data trail (spid, fdid, etc.)

1.0 - 2016-09-21
- initial production version

1.1 - 2016-10-26
- archive directories (Alex Spunde request)
- pylint cleanup

To do:
- jamo config file per user instead of defaults
- add multiple files?

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import os
import sys
from argparse import ArgumentParser
import json

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from curl3 import Curl, CurlHttpException # curl3 has apptoken for SDM web services



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__


if __name__ == "__main__":


    my_name = "JAMO File Saver"
    version = "1.1"

    # Parse options

    usage = "%s, version %s\n" % (my_name, version)
    usage += """
Saves files to JAMO
$ ./jamo_save.py -f /global/projectb/scratch/brycef/filtering_references-20160921.tar --file-type fasta -j '{"x":1}'
$ ./jamo_save.py -f /global/projectb/scratch/brycef/filtering_references-20160921.tar --file-type fasta,fa -j /global/projectb/scratch/brycef/filter_ref.metadata.json

Saves folders to JAMO
$ ./jamo_save.py -f /global/projectb/scratch/brycef/iso/BBOSU -j '{"x":1}' --file-type 'test run folder'

* add "-m prod" to save to JAMO's production server, otherwise uses JAMO's dev instance.
"""


    white = "\033[1;37m"
    green = "\033[1;32m"
    red = "\033[1;31m"
    yellow = "\033[1;33m"
    pink = "\033[1;35m"
    blue = "\033[1;34m"
    cyan= "\033[1;36m"
    nocolor = "\033[m"

    msg_ok = "[  \033[1;32mOK\033[m  ]"
    msg_fail = "[ \033[1;31mFAIL\033[m ]"
    msg_warn = "[ \033[1;33mWARN\033[m ]"

    parser = ArgumentParser(usage = usage)

    parser.add_argument("-f", "--file", dest="file", type=str, help = "/path/to/file/to/save or /path/to/folder/to/archive/")
    parser.add_argument("-j", "--metadata", dest="metadata", type=str, help = "metadata for the file")
    parser.add_argument("-ft", "--file-type", dest="jamo_file_type", type=str, help = "file type/folder type to add (fasta, 'run folder', etc.")
    parser.add_argument("-p", "--purge-days", dest="purge_days", type=int, help = "Days before JAMO purges file from file system, default = 180 days")
    parser.add_argument("-m", "--mode", dest="mode", type=str, help="run mode (dev, prod), default = dev, dev uses SDM's test server")
    parser.add_argument("-v", "--version", action="version", version=version)

    mode = "dev"
    jamo_file = "" # file or folder
    save_mode = "file" # file or folder, will determine automatically
    jamo_file_type = []
    jamo_metadata = "" # could be json or a file
    purge_days = 180
    metadata_json = {} # converted file or jamo_metadata to real metadata

    args = parser.parse_args()
    if args.file:
        jamo_file = args.file

    if args.metadata:
        jamo_metadata = args.metadata

    if args.jamo_file_type:
        jamo_file_type = args.jamo_file_type.split(",")

    if args.purge_days:
        purge_days = args.purge_days
    if args.mode:
        if str(args.mode).lower().startswith("prod"):
            mode = "prod"
        else:
            mode = "dev"


    # check if file exists, file type exists and metadata is not empty
    fail_cnt = 0
    if not os.path.isfile(jamo_file) and not os.path.isdir(jamo_file):
        print "- error: cannot find file or folder %s  %s" % (jamo_file, msg_fail)
        fail_cnt += 1

    if not jamo_file_type:
        print "- error: missing file_type  %s" % (msg_fail)
        fail_cnt += 1

    if not jamo_metadata:
        print "- error: missing metadata  %s" % (msg_fail)
        fail_cnt += 1

    else:
        if os.path.isfile(jamo_metadata):
            jfh = open(jamo_metadata)
            # pass in a file handle
            metadata_json = json.load(jfh)
        else:
            metadata_json = json.loads(jamo_metadata)



    if fail_cnt > 0:
        sys.exit(2)

    if os.path.isfile(jamo_file):
        save_mode = "file"

    if os.path.isdir(jamo_file):
        save_mode = "folder"


    #docs: https://sdm-dev.jgi-psf.org:8034/doc/metadata/file/post
    #docs: https://sdm-dev.jgi-psf.org:8034/doc/metadata/folder/post
    metadata_url = "https://sdm-dev.jgi-psf.org"
    metadata_token = "LJ91PGAORVMXOHZVOXFCP79GNZTFNLKZ"
    jamo_module = "jamo/dev"

    # prod
    if mode == "prod":
        metadata_url = "https://sdm2.jgi-psf.org"
        metadata_token = "HTH7PMYYQ49T1277DT144KHIDDKWTQFO"
        jamo_module = "jamo"


    metadata_archive_file_api = "api/metadata/file"
    metadata_archive_folder_api = "api/metadata/folder" # not in use ... yet

    service_list = [1,2] # backup to both hsi systems

    # error 401: You must authenticate your self to access this page: method post_file in module metadata
    #print "metadata_url: %s" % metadata_url
    #print "token: %s" % metadata_token
    curl = Curl(metadata_url, appToken = metadata_token)

    metadata_id = "failed"

    if save_mode == "file":

        try:
            response = curl.post(metadata_archive_file_api,
                file = jamo_file,
                file_type = jamo_file_type,
                metadata = metadata_json,
                local_purge_days = purge_days,
                backup_services = service_list
            )
            # put_mode = put_mode
            #put_mode = "Replace_If_Newer"

            print "Sucessful %s call  %s" % (jamo_module, msg_ok)
            if "metadata_id" in response:
                metadata_id = response['metadata_id']

        except CurlHttpException as e:
            print "- Failed HTTP Metadata API Call: %s/%s  %s" % (metadata_url, metadata_archive_file_api, msg_fail)
            print "- error: %s" % (e)


        except Exception as e:

            print "- Failed Metadata API Call: %s/%s  %s" % (metadata_url, metadata_archive_file_api, msg_fail)
            print "- error: %s" % (e.args)
            #print e.message



    elif save_mode == "folder":
        try:
            response = curl.post(metadata_archive_folder_api,
                folder = jamo_file,
                file_type = jamo_file_type,
                metadata = metadata_json,
                local_purge_days = purge_days,
                backup_services = service_list
            )
            # put_mode = put_mode
            # backup_services = [1,2]
            #put_mode = "Replace_If_Newer"

            print "Sucessful %s call  %s" % (jamo_module, msg_ok)
            if "metadata_id" in response:
                metadata_id = response['metadata_id']

        except CurlHttpException as e:
            print "- Failed HTTP Metadata API Call: %s/%s  %s" % (metadata_url, metadata_archive_file_api, msg_fail)
            print "- error: %s" % (e)


        except Exception as e:

            print "- Failed Metadata API Call: %s/%s  %s" % (metadata_url, metadata_archive_file_api, msg_fail)
            print "- error: %s" % (e.args)
            #print e.message

    print "%s metadata_id: %s" % (jamo_module, metadata_id)

    sys.exit(0)



"""

                      .-.
         .-._    _.../   `,    _.-.
         |   `'-'    \     \_'`   |
         \            '.__,/ `\_.--,      Not dead which eternal lie
          /                '._/     |     Stranger eons death may die
         /                    '.    /
        ;   _                  _'--;      Drain you of your sanity
     '--|- (_)       __       (_) -|--'   Face the thing that should not be
     .--|-          (__)          -|--.
      .-\-                        -/-.    Metallica - The Thing That Should Not Be
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`

"""