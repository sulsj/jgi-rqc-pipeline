#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
pacbioChipEval.py


Revisions:
    08.10.2017 0.0.9: sulsj's modification
    08.10.2017 1.0.0: Cleanup

"""

__date__ ="Created:  4/14/17"
__author__="Jerry Jenkins, jjenkins@hudsonalpha.org"

import os
import argparse
import gzip
import matplotlib
matplotlib.use('agg') ## for DISPLAY error
import matplotlib.pyplot as plt
import numpy as np
import sys

VERSION = "1.0.0"


#==============================================================
def real_main(pacbioFile, output_PNG, resolution, denom):
    if pacbioFile.endswith(".gz"):
        oh = gzip.open(pacbioFile, 'r')
    else:
        oh = open(pacbioFile)

    ## Pulling the p-reads
    sizeDict = {}
    maxRow = 0
    maxCol = 0

    for line in oh:
        if line.startswith('>'):
            t = line.rstrip().split('/')
            try:
                ## libID, wellID, start, end
                wellID = int(t[1])
                start = int(t[2].split('_')[0])
                end = int(t[2].split('_')[1])
            except (IndexError, ValueError):
                continue

            ## Computing the read size
            readSize = end - start if start <= end else start - end

            ## Parsing the wellID
            row = wellID / denom
            col = wellID - row * denom

            ## Finding the bounds on the rows
            if row > maxRow:
                maxRow = row

            if col > maxCol:
                maxCol = col

            ## Storing the read information
            if wellID in sizeDict:
                sizeDict[wellID].append((readSize, row, col))
            else:
                sizeDict[wellID] = [(readSize, row, col)]

    if oh:
        oh.close()

    ## Filling the data matrix
    dataMatrix = [[0 for m in xrange(maxRow)] for n in xrange(maxCol)]
    maxReadSize = 0
    readList = []

    for wellID, tmpList in sizeDict.iteritems():
        tmpList.sort(reverse=True)
        readSize, row, col = tmpList[0]
        dataMatrix[col-1][row-1] = readSize
        if readSize > maxReadSize:
            maxReadSize = readSize
        readList.append(readSize)

    normMatrix = [[0 for m in xrange(maxRow)] for n in xrange(maxCol)]

    c = 1.0 / float(0.40 * maxReadSize)

    for n in xrange(maxCol):
        for m in xrange(maxRow):
            normMatrix[n][m] = float(dataMatrix[n][m]) * c
            if normMatrix[n][m] > 1.0:
                normMatrix[n][m] = 1.0

    ## Making the plot
    # plt.imshow(normMatrix, aspect=0.618, cmap='viridis', interpolation='none', clim=(0.0, 0.50))
    plt.imshow(normMatrix, aspect='auto', interpolation='none', clim=(0.0, 0.50))
    plt.title('%s' % os.path.split(pacbioFile)[1], fontsize=10)
    plt.savefig(output_PNG, format='png', dpi=resolution)

    ## completion notify
    if os.path.isfile(output_PNG):
        print "PNG output file is successfully created: %s" % (output_PNG)
        return 0
    else:
        print "Failed to create PNG output file."
        return 1


#==============================================================
if __name__ == "__main__":
    desc = 'pacbioChipEval'
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-i", "--in-file", dest="inputFile", help="Set input fasta/fastq.gz file", required=True)
    parser.add_argument("-o", "--out-file", dest="outputFile", help="Set output png file name", required=True)
    parser.add_argument("-r", "--resolution", dest="imgResolution", help="Set resolution for output image", required=False, type=float)
    parser.add_argument("-rs2", "--rs2", action="store_true", help="Set data type as RS2", dest="rs2", default=False, required=False)
    parser.add_argument('-v', '--version', action='version', version=VERSION)

    options = parser.parse_args()

    inFile = options.inputFile
    outFile = options.outputFile

    imgResolution = 300
    denom = 65536

    if options.imgResolution:
        imgResolution = options.imgResolution

    if options.rs2:
        denom = 256

    sys.exit(real_main(inFile, outFile, imgResolution, denom))

## EOF
