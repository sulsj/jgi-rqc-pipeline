#!/usr/bin/env python

import os
import hashlib
from nose import *
from rqc_reference_upload import *


def setup_module(module):
    print ("") # this is to get a newline after the dots
    print ("setup_module before anything in this file")

def teardown_module(module):
    print ("teardown_module after everything in this file")

def my_setup_function():
    print ("my_setup_function")

def my_teardown_function():
    print ("my_teardown_function")

@with_setup(my_setup_function, my_teardown_function)
def test_sub_script_exists():
    assert os.path.isfile("/global/dna/projectdirs/PI/rqc/prod/rqc_software/support_scripts/add_seq_to_repo.pl")


@with_setup(my_setup_function, my_teardown_function)
def test_scratch_exists():
    assert os.path.exists("/global/projectb/scratch/andreopo/upload_refgenomes")
    assert os.path.exists("/global/projectb/scratch/andreopo/upload_refgenomes/scratch")
    assert os.path.exists("/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs")
    assert os.path.exists("/global/projectb/scratch/qc_user/bill/upload_refgenomes")
    assert os.path.exists("/global/dna/projectdirs/RD/rnaseq_store/")
    assert os.path.isdir("/global/projectb/scratch/andreopo/upload_refgenomes")
    assert os.path.isdir("/global/projectb/scratch/andreopo/upload_refgenomes/scratch")
    assert os.path.isdir("/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs")
    assert os.path.isdir("/global/projectb/scratch/qc_user/bill/upload_refgenomes")
    assert os.path.isdir("/global/dna/projectdirs/RD/rnaseq_store/")



@with_setup(my_setup_function, my_teardown_function)
def test_convertIDtoScratchPath_Myc_Gen_ID():
    ref_id = "Aspni7_JBEI"
    scratch = "/global/projectb/scratch/andreopo/upload_refgenomes/scratch"
    ref_type = RefType.GENOME
    seq_proj_id = "0"
    organism_name = "AspergillusNiger"
    location_source = "MYCOCOSM"
    ref_src = RefSource.MYCOCOSM
    dest_path = os.path.join(scratch , str(organism_name) )
    dest_file = os.path.join(dest_path, organism_name + "_spid" + str(seq_proj_id) + "_reftype" + RefType.ref_type.get(ref_type) + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + ".fa")

    assert convertIDtoScratchPath(ref_id, scratch, ref_type, seq_proj_id, organism_name, location_source) == ( ref_src, dest_file )
    assert os.path.isfile(dest_file)
    print dest_file

    datetime = get_timestamp()
    projectdirs = "/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs"
    projdirs = os.path.join(os.path.join(projectdirs, "genomes"), organism_name)
    path_scratch = dest_file
    comment = 'datetime: ' + datetime + ', \nseq_proj_id: ' + seq_proj_id + ', \ngenomeID: ' + ref_id + ', \npath_scratch: ' + path_scratch + ', \norganism_name: ' + organism_name + ', \nreference_source: ' + str(RefSource.ref_source.get(ref_src)) + ', \nreference_type: ' + RefType.ref_type.get(ref_type)
    newversion = True
    path_projectdirs = os.path.join(projdirs, organism_name + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + "_reftype" + RefType.ref_type.get(ref_type) + ".fasta")

    assert move_from_scratch_to_projectdirs(projdirs, path_scratch, organism_name, ref_type, comment, ref_src, datetime, newversion) == path_projectdirs
    assert os.path.isfile(path_projectdirs)
    print path_projectdirs


@with_setup(my_setup_function, my_teardown_function)
def test_convertIDtoScratchPath_Myc_Trans_ID():
    ref_id = "Aspni7_JBEI"
    scratch = "/global/projectb/scratch/andreopo/upload_refgenomes/scratch"
    ref_type = RefType.TRANSCRIPTOME
    seq_proj_id = "0"
    organism_name = "AspergillusNiger"
    location_source = "MYCOCOSM"
    ref_src = RefSource.MYCOCOSM
    dest_path = os.path.join(scratch , str(organism_name) )
    dest_file = os.path.join(dest_path, organism_name + "_spid" + str(seq_proj_id) + "_reftype" + RefType.ref_type.get(ref_type) + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + ".fa")

    assert convertIDtoScratchPath(ref_id, scratch, ref_type, seq_proj_id, organism_name, location_source) == ( ref_src, dest_file )
    assert os.path.isfile(dest_file)
    print dest_file

    datetime = get_timestamp()
    projectdirs = "/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs"
    projdirs = os.path.join(os.path.join(projectdirs, "transcriptome"), organism_name)
    path_scratch = dest_file
    comment = 'datetime: ' + datetime + ', \nseq_proj_id: ' + seq_proj_id + ', \ntranscriptomeID: ' + ref_id + ', \npath_scratch: ' + path_scratch + ', \norganism_name: ' + organism_name + ', \nreference_source: ' + str(RefSource.ref_source.get(ref_src)) + ', \nreference_type: ' + RefType.ref_type.get(ref_type)
    newversion = True
    path_projectdirs = os.path.join(projdirs, organism_name + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + "_reftype" + RefType.ref_type.get(ref_type) + ".fasta")

    assert move_from_scratch_to_projectdirs(projdirs, path_scratch, organism_name, ref_type, comment, ref_src, datetime, newversion) == path_projectdirs
    assert os.path.isfile(path_projectdirs)
    print path_projectdirs



@with_setup(my_setup_function, my_teardown_function)
def test_convertIDtoScratchPath_RQCVelvet_Gen_ID():
    ref_id = "UBUX"
    scratch = "/global/projectb/scratch/andreopo/upload_refgenomes/scratch"
    ref_type = RefType.GENOME
    seq_proj_id = "1027280"
    organism_name = "Aspergillus_homomorphus"
    location_source = "RQC_VELVET_ASSEMBLY"
    ref_src = RefSource.RQC_VELVET_ASSEMBLY
    dest_path = os.path.join(scratch , str(organism_name) )
    dest_file = os.path.join(dest_path, organism_name + "_spid" + str(seq_proj_id) + "_reftype" + RefType.ref_type.get(ref_type) + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + ".fa")

    assert convertIDtoScratchPath(ref_id, scratch, ref_type, seq_proj_id, organism_name, location_source) == ( ref_src, dest_file )
    assert os.path.isfile(dest_file)
    print dest_file

    datetime = get_timestamp()
    projectdirs = "/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs"
    projdirs = os.path.join(os.path.join(projectdirs, "genomes"), organism_name)
    path_scratch = dest_file
    comment = 'datetime: ' + datetime + ', \nseq_proj_id: ' + seq_proj_id + ', \ngenomeID: ' + ref_id + ', \npath_scratch: ' + path_scratch + ', \norganism_name: ' + organism_name + ', \nreference_source: ' + str(RefSource.ref_source.get(ref_src)) + ', \nreference_type: ' + RefType.ref_type.get(ref_type)
    newversion = True
    path_projectdirs = os.path.join(projdirs, organism_name + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + "_reftype" + RefType.ref_type.get(ref_type) + ".fasta")

    assert move_from_scratch_to_projectdirs(projdirs, path_scratch, organism_name, ref_type, comment, ref_src, datetime, newversion) == path_projectdirs
    assert os.path.isfile(path_projectdirs)
    print path_projectdirs

@with_setup(my_setup_function, my_teardown_function)
def test_convertIDtoScratchPath_RQCVelvet_Gen2_ID():
    ref_id = "UBUX"
    scratch = "/global/projectb/scratch/andreopo/upload_refgenomes/scratch"
    ref_type = RefType.GENOME
    seq_proj_id = "1027288"
    organism_name = "Aspergillus_indologenus"
    location_source = "RQC_VELVET_ASSEMBLY"
    ref_src = RefSource.RQC_VELVET_ASSEMBLY
    dest_path = os.path.join(scratch , str(organism_name) )
    dest_file = os.path.join(dest_path, organism_name + "_spid" + str(seq_proj_id) + "_reftype" + RefType.ref_type.get(ref_type) + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + ".fa")

    assert convertIDtoScratchPath(ref_id, scratch, ref_type, seq_proj_id, organism_name, location_source) == ( ref_src, dest_file )
    assert os.path.isfile(dest_file)
    print dest_file

    datetime = get_timestamp()
    projectdirs = "/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs"
    projdirs = os.path.join(os.path.join(projectdirs, "genomes"), organism_name)
    path_scratch = dest_file
    comment = 'datetime: ' + datetime + ', \nseq_proj_id: ' + seq_proj_id + ', \ngenomeID: ' + ref_id + ', \npath_scratch: ' + path_scratch + ', \norganism_name: ' + organism_name + ', \nreference_source: ' + str(RefSource.ref_source.get(ref_src)) + ', \nreference_type: ' + RefType.ref_type.get(ref_type)
    newversion = True
    path_projectdirs = os.path.join(projdirs, organism_name + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + "_reftype" + RefType.ref_type.get(ref_type) + ".fasta")

    assert move_from_scratch_to_projectdirs(projdirs, path_scratch, organism_name, ref_type, comment, ref_src, datetime, newversion) == path_projectdirs
    assert os.path.isfile(path_projectdirs)
    print path_projectdirs

@with_setup(my_setup_function, my_teardown_function)
def test_convertIDtoScratchPath_RQCVelvet_Trans_ID():
    ref_id = "UBUX"
    scratch = "/global/projectb/scratch/andreopo/upload_refgenomes/scratch"
    ref_type = RefType.TRANSCRIPTOME
    seq_proj_id = "1027280"
    organism_name = "Aspergillus_homomorphus"
    location_source = "RQC_VELVET_ASSEMBLY"
    ref_src = RefSource.RQC_VELVET_ASSEMBLY
    dest_path = os.path.join(scratch , str(organism_name) )
    dest_file = os.path.join(dest_path, organism_name + "_spid" + str(seq_proj_id) + "_reftype" + RefType.ref_type.get(ref_type) + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + ".fa")

    assert convertIDtoScratchPath(ref_id, scratch, ref_type, seq_proj_id, organism_name, location_source) == 2





@with_setup(my_setup_function, my_teardown_function)
def test_convertIDtoScratchPath_ABSPATH_GEN_ID():
    ref_id = "/global/dna/shared/rqc/pipelines/assembly_qc/archive/02/83/05/60/7892.4.86997.GCCAAT.subsampled15000000.fastq.velvet.k63/contigs.fa"
    scratch = "/global/projectb/scratch/andreopo/upload_refgenomes/scratch"
    ref_type = RefType.GENOME
    seq_proj_id = "1021563"
    organism_name = "Lachnum_sp"
    location_source = "ABS_PATH"
    ref_src = RefSource.ABS_PATH
    dest_path = os.path.join(scratch , str(organism_name) )
    dest_file = os.path.join(dest_path, organism_name + "_spid" + str(seq_proj_id) + "_reftype" + RefType.ref_type.get(ref_type) + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + ".fa")

    assert convertIDtoScratchPath(ref_id, scratch, ref_type, seq_proj_id, organism_name, location_source) == ( ref_src, dest_file )
    assert os.path.isfile(dest_file)
    print dest_file

    datetime = get_timestamp()
    projectdirs = "/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs"
    projdirs = os.path.join(os.path.join(projectdirs, "genomes"), organism_name)
    path_scratch = dest_file
    comment = 'datetime: ' + datetime + ', \nseq_proj_id: ' + seq_proj_id + ', \ngenomeID: ' + ref_id + ', \npath_scratch: ' + path_scratch + ', \norganism_name: ' + organism_name + ', \nreference_source: ' + str(RefSource.ref_source.get(ref_src)) + ', \nreference_type: ' + RefType.ref_type.get(ref_type)
    newversion = True
    path_projectdirs = os.path.join(projdirs, organism_name + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + "_reftype" + RefType.ref_type.get(ref_type) + ".fasta")

    assert move_from_scratch_to_projectdirs(projdirs, path_scratch, organism_name, ref_type, comment, ref_src, datetime, newversion) == path_projectdirs
    assert os.path.isfile(path_projectdirs)
    print path_projectdirs






@with_setup(my_setup_function, my_teardown_function)
def test_convertIDtoScratchPath_URL_GEN_ID():
    ref_id = "http://loblolly.ucdavis.edu/bipod/ftp/Genome_Data/genome/pinerefseq/Pita/v1.01/ptaeda.v1.01.scaffolds.fasta.gz"
    scratch = "/global/projectb/scratch/andreopo/upload_refgenomes/scratch"
    ref_type = RefType.GENOME
    seq_proj_id = "1026310"
    organism_name = "Pinus_pinaster"
    location_source = "URL"
    ref_src = RefSource.URL
    dest_path = os.path.join(scratch , str(organism_name) )
    dest_file = os.path.join(dest_path, organism_name + "_spid" + str(seq_proj_id) + "_reftype" + RefType.ref_type.get(ref_type) + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + ".fa")

    assert convertIDtoScratchPath(ref_id, scratch, ref_type, seq_proj_id, organism_name, location_source) == ( ref_src, dest_file )
    assert os.path.isfile(dest_file)
    print dest_file

    datetime = get_timestamp()
    projectdirs = "/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs"
    projdirs = os.path.join(os.path.join(projectdirs, "genomes"), organism_name)
    path_scratch = dest_file
    comment = 'datetime: ' + datetime + ', \nseq_proj_id: ' + seq_proj_id + ', \ngenomeID: ' + ref_id + ', \npath_scratch: ' + path_scratch + ', \norganism_name: ' + organism_name + ', \nreference_source: ' + str(RefSource.ref_source.get(ref_src)) + ', \nreference_type: ' + RefType.ref_type.get(ref_type)
    newversion = True
    path_projectdirs = os.path.join(projdirs, organism_name + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + "_reftype" + RefType.ref_type.get(ref_type) + ".fasta")

    assert move_from_scratch_to_projectdirs(projdirs, path_scratch, organism_name, ref_type, comment, ref_src, datetime, newversion) == path_projectdirs
    assert os.path.isfile(path_projectdirs)
    print path_projectdirs




@with_setup(my_setup_function, my_teardown_function)
def test_convertIDtoScratchPath_GENPORT_GEN_ID():
    ref_id = "http://genome.jgi.doe.gov/ClapyxStandDraft_P/download/_JAMO/53566e530d87855a8277cb6b/1031304.Clavicorona_pyxidata_HHB10654.standard.main.scaffolds.fasta"
    scratch = "/global/projectb/scratch/andreopo/upload_refgenomes/scratch"
    ref_type = RefType.GENOME
    seq_proj_id = "1031305"
    organism_name = "Clavicorona_pyxidata"
    location_source = "GENOME_PORTAL"
    ref_src = RefSource.GENOME_PORTAL
    dest_path = os.path.join(scratch , str(organism_name) )
    dest_file = os.path.join(dest_path, organism_name + "_spid" + str(seq_proj_id) + "_reftype" + RefType.ref_type.get(ref_type) + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + ".fa")

    assert convertIDtoScratchPath(ref_id, scratch, ref_type, seq_proj_id, organism_name, location_source) == ( ref_src, dest_file )
    assert os.path.isfile(dest_file)
    print dest_file

    datetime = get_timestamp()
    projectdirs = "/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs"
    projdirs = os.path.join(os.path.join(projectdirs, "genomes"), organism_name)
    path_scratch = dest_file
    comment = 'datetime: ' + datetime + ', \nseq_proj_id: ' + seq_proj_id + ', \ngenomeID: ' + ref_id + ', \npath_scratch: ' + path_scratch + ', \norganism_name: ' + organism_name + ', \nreference_source: ' + str(RefSource.ref_source.get(ref_src)) + ', \nreference_type: ' + RefType.ref_type.get(ref_type)
    newversion = True
    path_projectdirs = os.path.join(projdirs, organism_name + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + "_reftype" + RefType.ref_type.get(ref_type) + ".fasta")

    assert move_from_scratch_to_projectdirs(projdirs, path_scratch, organism_name, ref_type, comment, ref_src, datetime, newversion) == path_projectdirs
    assert os.path.isfile(path_projectdirs)
    print path_projectdirs




@with_setup(my_setup_function, my_teardown_function)
def test_convertIDtoScratchPath_PHYTOZOME_GEN_ID():
    ref_id = "ftp://ftp.jgi-psf.org/pub/compgen/phytozome/v9.0/Mtruncatula/assembly/Mtruncatula_198.fa.gz"
    scratch = "/global/projectb/scratch/andreopo/upload_refgenomes/scratch"
    ref_type = RefType.GENOME
    seq_proj_id = "1031118"
    organism_name = "Medicago_truncutula"
    location_source = "PHYTOZOME"
    ref_src = RefSource.PHYTOZOME
    dest_path = os.path.join(scratch , str(organism_name) )
    dest_file = os.path.join(dest_path, organism_name + "_spid" + str(seq_proj_id) + "_reftype" + RefType.ref_type.get(ref_type) + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + ".fa")

    assert convertIDtoScratchPath(ref_id, scratch, ref_type, seq_proj_id, organism_name, location_source) == ( ref_src, dest_file )
    assert os.path.isfile(dest_file)
    print dest_file

    datetime = get_timestamp()
    projectdirs = "/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs"
    projdirs = os.path.join(os.path.join(projectdirs, "genomes"), organism_name)
    path_scratch = dest_file
    comment = 'datetime: ' + datetime + ', \nseq_proj_id: ' + seq_proj_id + ', \ngenomeID: ' + ref_id + ', \npath_scratch: ' + path_scratch + ', \norganism_name: ' + organism_name + ', \nreference_source: ' + str(RefSource.ref_source.get(ref_src)) + ', \nreference_type: ' + RefType.ref_type.get(ref_type)
    newversion = True
    path_projectdirs = os.path.join(projdirs, organism_name + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + "_reftype" + RefType.ref_type.get(ref_type) + ".fasta")

    assert move_from_scratch_to_projectdirs(projdirs, path_scratch, organism_name, ref_type, comment, ref_src, datetime, newversion) == path_projectdirs
    assert os.path.isfile(path_projectdirs)
    print path_projectdirs



@with_setup(my_setup_function, my_teardown_function)
def test_convertIDtoScratchPath_IMG_GEN_ID():
    ref_id = "2519103102"
    scratch = "/global/projectb/scratch/andreopo/upload_refgenomes/scratch"
    ref_type = RefType.GENOME
    seq_proj_id = "1033769"
    organism_name = "Thermoanaerobacterium_saccharolyticum"
    location_source = "IMGTAXONOID"
    ref_src = RefSource.IMGTAXONOID
    dest_path = os.path.join(scratch , str(organism_name) )
    dest_file = os.path.join(dest_path, organism_name + "_spid" + str(seq_proj_id) + "_reftype" + RefType.ref_type.get(ref_type) + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + ".fa")

    assert convertIDtoScratchPath(ref_id, scratch, ref_type, seq_proj_id, organism_name, location_source) == ( ref_src, dest_file )
    assert os.path.isfile(dest_file)
    print dest_file

    datetime = get_timestamp()
    projectdirs = "/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs"
    projdirs = os.path.join(os.path.join(projectdirs, "genomes"), organism_name)
    path_scratch = dest_file
    comment = 'datetime: ' + datetime + ', \nseq_proj_id: ' + seq_proj_id + ', \ngenomeID: ' + ref_id + ', \npath_scratch: ' + path_scratch + ', \norganism_name: ' + organism_name + ', \nreference_source: ' + str(RefSource.ref_source.get(ref_src)) + ', \nreference_type: ' + RefType.ref_type.get(ref_type)
    newversion = True
    path_projectdirs = os.path.join(projdirs, organism_name + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + "_reftype" + RefType.ref_type.get(ref_type) + ".fasta")

    assert move_from_scratch_to_projectdirs(projdirs, path_scratch, organism_name, ref_type, comment, ref_src, datetime, newversion) == path_projectdirs
    assert os.path.isfile(path_projectdirs)
    print path_projectdirs




@with_setup(my_setup_function, my_teardown_function)
def test_convertIDtoScratchPath_IMG_TRANS_ID():
    ref_id = "2519103102"
    scratch = "/global/projectb/scratch/andreopo/upload_refgenomes/scratch"
    ref_type = RefType.TRANSCRIPTOME
    seq_proj_id = "1033769"
    organism_name = "Thermoanaerobacterium_saccharolyticum"
    location_source = "IMGTAXONOID"
    ref_src = RefSource.IMGTAXONOID
    dest_path = os.path.join(scratch , str(organism_name) )
    dest_file = os.path.join(dest_path, organism_name + "_spid" + str(seq_proj_id) + "_reftype" + RefType.ref_type.get(ref_type) + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + ".fa")

    assert convertIDtoScratchPath(ref_id, scratch, ref_type, seq_proj_id, organism_name, location_source) == ( ref_src, dest_file )
    assert os.path.isfile(dest_file)
    print dest_file

    datetime = get_timestamp()
    projectdirs = "/global/projectb/scratch/andreopo/upload_refgenomes/projectdirs"
    projdirs = os.path.join(os.path.join(projectdirs, "transcriptome"), organism_name)
    path_scratch = dest_file
    comment = 'datetime: ' + datetime + ', \nseq_proj_id: ' + seq_proj_id + ', \ntranscriptomeID: ' + ref_id + ', \npath_scratch: ' + path_scratch + ', \norganism_name: ' + organism_name + ', \nreference_source: ' + str(RefSource.ref_source.get(ref_src)) + ', \nreference_type: ' + RefType.ref_type.get(ref_type)
    newversion = True
    path_projectdirs = os.path.join(projdirs, organism_name + "_refsrc" + str(RefSource.ref_source.get(ref_src)) + "_reftype" + RefType.ref_type.get(ref_type) + ".fasta")

    assert move_from_scratch_to_projectdirs(projdirs, path_scratch, organism_name, ref_type, comment, ref_src, datetime, newversion) == path_projectdirs
    assert os.path.isfile(path_projectdirs)
    print path_projectdirs
