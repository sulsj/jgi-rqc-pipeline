#!/bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -notify
#$ -P gentech-rqc.p
#$ -N rqc_NOSETESTS_pdf_report
#$ -l h_rt=10:00:00
#$ -l ram.c=5G
#$ -M wandreopoulos@lbl.gov
#$ -m abe
./run.sh

