#!/usr/bin/env python


from nose import *

import sys
import os
import hashlib


dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir,'../../'))       ## rqc-pipeline/tools

from rqc_pdf_report import *
from time import time, strftime
import shutil


def setup_module(module):
    print ("") # this is to get a newline after the dots
    print ("setup_module before anything in this file")

def teardown_module(module):
    print ("teardown_module after everything in this file")

def my_setup_function():
    print ("my_setup_function")

def my_teardown_function():
    print ("my_teardown_function")


test_fastqs = ['8435.1.102092.ACAGTG.fastq.gz', '8066.5.90150.AAGAGG.fastq.gz', '7275.7.65880.CTTGTA.fastq.gz', '7275.7.65880.GCCAAT.fastq.gz']


@with_setup(my_setup_function, my_teardown_function)
def test_directory_exists():
    timestamp = strftime("%m%d%Y-%H%M%S")
    for fastq in test_fastqs:
        print 'testing_fastq %s  <============================ actual test code' % (fastq)
        ###output_path = '/global/projectb/scratch/andreopo'
        ###fastq = '7275.7.65880.GCCAAT.fastq.gz'
        output_path = os.path.join( os.getcwd() , fastq )
        
        if os.path.isdir(output_path) and os.path.exists(output_path):
            shutil.move(output_path, output_path + "_" + timestamp)
            
        rqc_version = 'v2'
        ###out_path = os.path.join(output_path, fastq)
        assert main(output_path, fastq, 0, rqc_version) == 0
        assert os.path.isdir(output_path)

'''
@with_setup(my_setup_function, my_teardown_function)
def test_directory_exists2():
    print 'test_directory_exists2  <============================ actual test code'
    ###output_path = '/global/projectb/scratch/andreopo'
    fastq = '7275.7.65880.CTTGTA.fastq.gz'
    output_path = os.path.join( os.getcwd() , fastq )
    rqc_version = 'v2'
    ###out_path = os.path.join(output_path, fastq)
    assert main(output_path, fastq, 0, rqc_version) == 0
    assert os.path.isdir(output_path)
'''
