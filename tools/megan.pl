#!/usr/bin/env perl

use strict;
use warnings;
use DBI;
use Getopt::Long;
use MIME::Base64;
use Cwd;
use Cwd 'abs_path';
use File::Copy;
use File::Basename;


use constant JGI_SUCCESS => 0;
use constant JGI_FAILURE => 1;
use constant JGI_UNDEFINED => 2;


use constant PERL_CMD => "perl";

# Global constant variables.
use constant JGI_MEGAN_VERSION => "version";
use constant JGI_MEGAN_LOGGER => "logger";

# Blast file types.
use constant MEGABLAST => 'megablast';
use constant BLASTN => 'blastn';
use constant BLASTX => 'blastx';

my %JGI_BLAST_FILE_TYPES = (
    (MEGABLAST) => 1,
    (BLASTN) => 1,
    (BLASTX) => 1,
);

# Current array index number to retrieve megan image files generated.
#
my $IMAGE_INDEX = 0;


my ($fasta_file, $blast_file, $blast_file_type, $working_dir, $label) = '';

#use Cwd 'abs_path';
#use File::Basename;
#my $fd = dirname(abs_path($0)) . "/megan_settings.sh";
#my $fd2 = dirname(abs_path($0)) . "/meganWrapper.pl";
# print $fd;
# Megan specific commands
# Megan settings not called - refers to /home, /jgi/tools that do not exist anymore - Bryce 2013-10-29
my $MEGAN_SETTINGS = dirname ( abs_path($0) ) . "/external_tools/pmgw/config/megan_settings.sh"; ###"/house/homedirs/q/qc_user/rqc_software/external_tools/pmgw/config/megan_settings.sh"; ###
my $MEGAN_WRAPPER = dirname ( abs_path($0) ) . "/external_tools/pmgw/bin/meganWrapper.pl"; ###"/house/homedirs/q/qc_user/rqc_software/external_tools/pmgw/bin/meganWrapper.pl"; ###
###use constant MEGAN_CMD => '/home/jazz4/rqc_test/megan/megan2_beta/MEGAN';

GetOptions(
    "f=s"     =>     \$fasta_file,
    "b=s"     =>     \$blast_file,
    "t=s"    =>     \$blast_file_type,
    "w=s"    =>     \$working_dir,
    "l=s"     =>     \$label
);
my $usage = <<"USAGE";

     Print the megan files as output

     $0 -f fasta_file -b blast_file -t blast_file_type -w working_dir -l label

     OPTIONS:
         REQUIRED:
         -f
         -b
         -t
         -w
         -l
USAGE

# make sure requried stuff present
unless ($fasta_file && $blast_file && $blast_file_type && $working_dir && $label) {
    print $usage;
    exit;
}




run_megan($fasta_file, $blast_file, $blast_file_type, $working_dir, $label);



=head2 run_megan

NAME            run_megan
TYPE            Executes megan
PURPOSE         To run the megan wrapper that will call MEGAN and R to generate
                taxonomy, phylogony and GC plots.
USAGE           $obj->run_megan(FILE_ATTRIBUTE)
PARAMETERS      $_[0]: This object.
                $_[1]: The fasta file.
                $_[2]: The blast file. Either BlastX, Blastn or MegaBlast.
                $_[3]: The blast file type: BLASTX, BLASTN, MEGABLAST constant
                       variable.
                $_[4]: the directory to store the generated megan files and
                       images.
                $_[5]: A label to define the megan outputs - typically the
                       library name.
RETURNS         JGI_SUCCESS or JGI_FAILURE
EXCEPTIONS      None.
DESCRIPTION     None.
COMMENTS        None.

=cut

sub run_megan
{
    my ($fasta_file, $blast_file, $blast_file_type, $working_dir,
        $label) = @_;

    # Variables to store the specified value.
    my $value;
    
    # Strip any trailing '/' from dir name.
    #
    $working_dir =~ s/\/$//;

    # A string to identify this method.
    my $function = (caller(0))[3];

    # Check if input files exist.
    #
    if ( _check_files($fasta_file, $blast_file) != JGI_SUCCESS )
    {
        return JGI_FAILURE;
    }
    
    # Make working dir if it doesn't exist.
    #
    mkdir $working_dir if !-e $working_dir;
    
    # Check for valid blast file types.
    #
    if ( !defined $JGI_BLAST_FILE_TYPES{$blast_file_type} )
    {
        print "Blast file type ($blast_file_type) is not supported.";
        return JGI_FAILURE;
    }
    
    # If blast file is a megablast file, convert header MEGABLAST to
    # BLASTN so that Megan will work properly.
    #
    if ( $blast_file_type eq MEGABLAST )
    {
	my $destination_fasta_file = $working_dir . '/' . basename($fasta_file);

        # make a copy of the megablast file
	if ($fasta_file ne $destination_fasta_file) {
	    copy $fasta_file, $destination_fasta_file;
        }

        $fasta_file = $destination_fasta_file;
        
        # Modify header.
        #
        if ( _convert_megablast_header_to_blastn($blast_file) != JGI_SUCCESS )
        {
         return JGI_FAILURE;
        }
    }
    
    # Make working dir if it doesn't exist.
    #
    if ( _make_working_dir($working_dir) != JGI_SUCCESS )
    {
        return JGI_FAILURE;
    }

    
#    my $log_file = $working_dir . "/" . basename(MEGAN_WRAPPER) . ".log";
    my $rma_file = "$working_dir/${label}.rma";
    
    my $cmd = "";
       #$cmd .= "source " . $MEGAN_SETTINGS . "; ";
#       $cmd .= "export LD_LIBRARY_PATH=/usr/lib:\$LD_LIBRARY_PATH; ";
       $cmd .= $MEGAN_WRAPPER . " ";
       $cmd .= "$blast_file $fasta_file $rma_file $label";
    
    my $return_value;
    
### FIXME
my $curdir = getcwd();
print "cwd=$curdir\n";
print "cmd=$cmd\n";
### FIXME
        eval {
                $return_value = system("$cmd");
        };
        if ($@)
        {
            print "Failed to run ". $MEGAN_WRAPPER;
            return JGI_FAILURE;
        }
    
    return JGI_SUCCESS;
    
    
} # End of the run_megan method.

=head2 _check_files

NAME            _check_files
TYPE            File checker
PURPOSE         Checks for existence of input files and logs an error if
                the input file does not exist or is zero size.
USAGE           $obj->_check_files(@_)
PARAMETERS      $_[0]: This object.
                @_: List of files to check.
RETURNS         JGI_SUCCESS or JGI_FAILURE
EXCEPTIONS      None.
DESCRIPTION     None.
COMMENTS        None.

=cut

sub _check_files
{
    my (@files) = @_;
    
    
    foreach my $file (@files)
    {
        if ( !-s $file ) {
            print "File ($file) does not exist or is zero size.";
            return JGI_FAILURE;
        }
    }
    
    return JGI_SUCCESS;
    
} # End of the _check_files method.

=head2 _convert_megablast_header_to_blastn

NAME            _convert_megablast_header_to_blastn
TYPE            Megablast file header converter
PURPOSE         Converts the file header of a MegaBlast file from MEGABLAST to
                BLASTN so that Megan will work properly with megablast files.
USAGE           $obj->_convert_megablast_header_to_blastn(megablast_file)
PARAMETERS      $_[0]: This object.
                $_[1]: megablast file.
RETURNS         JGI_SUCCESS or JGI_FAILURE
EXCEPTIONS      None.
DESCRIPTION     None.
COMMENTS        None.

=cut

sub _convert_megablast_header_to_blastn
{
    
    my ($blast_file) = @_;
    
    my $cmd = (PERL_CMD) . " -p -i -e s/MEGABLAST/BLASTN/g $blast_file";
    
    my $return_value;
    
	eval {
		$return_value = system("$cmd");
	};
	if ($@)
	{
            print "Failed to substitute MEGABLAST with BLASTN in file $blast_file";
	    return JGI_FAILURE;
	}

    return JGI_SUCCESS;
    
} # End of the _convert_megablast_header_to_blastn method.

=head2 _make_working_dir

NAME            _make_working_dir
TYPE            Directory generator
PURPOSE         Creates the working directory if it doesn't exist.
USAGE           $obj->_make_working_dir(working_dir)
PARAMETERS      $_[0]: This object.
                $_[1]: directory to create.
RETURNS         JGI_SUCCESS or JGI_FAILURE
EXCEPTIONS      None.
DESCRIPTION     None.
COMMENTS        None.

=cut
sub _make_working_dir
{
    my ($working_dir) = @_;
    
    if (! -d $working_dir) 
    {
	if (!mkdir $working_dir)
	{
	    print "Unable to create directory $working_dir: $!";
	    return JGI_FAILURE; 
	}
    }
    
    return JGI_SUCCESS;
    
}



1;
