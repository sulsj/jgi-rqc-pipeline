#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Run RQC Rework QC for a given seq unit
    Version 1.0.1
    Shijie Yao
    05/03/2017  - init coding

    1.0.1 - 2018-01-19
    - fixed runCommand function to use standard run_cmd
    - fixed db connection
    

    Usage:
        fail itag of <1K reads:
        % rework_qc_helper.py -s SUN -cla 0 -u 0 -FM 1 -fm 3 -com "auto fail itag due to low read count" --live --prod
        (always add --live --prod in production run)
"""
import os
import sys
import argparse
import json
import uuid
import MySQLdb

from pprint import pprint

my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from db_access import jgi_connect_db
from common import run_cmd

#ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
#LIB_DIR = '%s/lib' % ROOT_DIR
#if LIB_DIR not in sys.path:
#    sys.path.append(LIB_DIR)

#import os_utility as util
#if 'runCommand' in dir(util):
#    runCommand = util.runCommand
#elif 'run_command' in dir(util):
#    runCommand = util.run_command
#elif 'run_sh_command' in dir(util):
#    runCommand = util.run_sh_command

DEVELOP = False

LAB_ACTION_CV = ['No Lab Action','Make New Library','qPCR Existing Library','Sequence Existing Library','Use Existing Library in New Pool']
SOW_STATUS_CV = ['Needs Lab Work','Complete','Needs PMO Attention']
DATA_USABLE_CV = ['Unusable', 'Usable']

def print_cv(host):
    print(' Data Usable Map (-u/--data-usable):')
    for idx, val in enumerate(DATA_USABLE_CV):
        print('    %d - %s' % (idx, val))
    print('')

    print(' Lab Action Code Map (-la/--lab-action):')
    for idx, val in enumerate(LAB_ACTION_CV):
        print('    %d - %s' % (idx, val))
    print('')

    print(' SOW Status Code Map (-sow/--sow-status)')
    for idx, val in enumerate(SOW_STATUS_CV):
        print('    %d - %s' % (idx, val))
    print('')

    FM_MAJOR_CODE = failure_mode_data(host, 'failure_major_code')
    FM_DATA = failure_mode_data(host, 'failure_mode_illumina')
    print(' Failure Mode Code Map (-FM/--fm-major       -fm/--fm-minor)')
    for k in sorted(FM_MAJOR_CODE.keys()):
        print('       %d       - %s' % (k, FM_MAJOR_CODE[k]))
        for idx, val in enumerate(FM_DATA[FM_MAJOR_CODE[k]]):
            print('                  %d       - %s' % (idx, val))
        print('')
    print(' Multiple Failure Mode Example:')
    print('    -FM 100 -fm 1:3,3:5  [PMO : sample depth issue AND Sequencing : mix up]')
    print('')

def exit_on_err(msg):
    print('Error: %s' % msg)
    sys.exit(1)


'''
retrive FM definitions from the centralized code of seqqc.py
1) failure_major_code
2) failure_mode_illumina
'''
def failure_mode_data(host, api):
    cmd = 'curl %s/api/seqqc/%s' % (host, api)
    err = ''

    std_out, std_err, exit_code = run_cmd(cmd)

    if std_out.find('Error response') != -1:
        print('cmd [ %s ]\n' % cmd)
        print('obtained : ')
        print(std_out)
        exit_on_err('Server down ?')

    if exit_code == 0:
        jd = json.loads(std_out)
        if 'errors' in jd:
            err = '%s [%s]' % (cmd, ';'.join(jd['errors']))
        else:
            if api == 'failure_major_code':
                fmdat = {}  # change keys into int
                for k in jd:
                    fmdat[int(k)] = jd[k]
                jd = fmdat
            return jd
    else:
        err = '%s [%s][%s]' % (cmd, std_out.strip(), std_err.strip())

    if err:
        exit_on_err(err)

def get_tla(sun, host):
    cmd = 'curl %s/api/sow_qc/its_logical_amounts?seq_unit_name=%s' % (host, sun)
    print('%s ... \n' % cmd)
    stdOut, stdErr, exitCode = run_cmd(cmd)
    if exitCode == 0:
        try:
            js = json.loads(stdOut.strip())
            return float(js['its_tla'])
        except:
            exit_on_err('failed to convert str to json : %s' % stdOut)
    else:
        exit_on_err('failed to call ws for tla value [%s]' % cmd)


def get_host(prod=False):
    host = 'https://rqc-4.jgi-psf.org/' # Shijie's dev web server
    if prod:
        host = 'https://rqc.jgi-psf.org/'
    return host

def create_tmp_input(param):
    TDIR = '/scratch'
    fname = '%s/rqc_rework_qc_%s.in' % (TDIR, str(uuid.uuid4()))
    #pprint(param)

    plist = [
        param['sun'],
        str(param['tla']),
        str(param['cla']),
        param['ablib'],
        param['absam'],
        '%d' % param['labaction'],
        '%d' % param['usable'],
        '%d' % param['sow'],
        '%d' % param['fmajor'],
        str(param['fminor']),
        param['comments']
    ]

    DATA_HEADER = ['SU_NAME', 'TLA', 'CLA', 'AB_LIB', 'AB_SAM', 'LAB_ACT', 'USEABLE', 'SOW', 'FM_M', 'FM_m', 'COMMENTS']
    HEADER_LINE = '         '.join(DATA_HEADER)

    with open(fname, 'w') as FH:
        if param['analyst']:
            FH.write('Analyst=%s\n' % param['analyst'])
        else:
            FH.write('Analyst=Shijie Yao\n')
        #       SU_NAME  TLA  CLA  ABAN_LIB  ABAN_SAM   LAB_ACT  USEABLE   SOW  FM_M  FM_m   COMMENTS
        FH.write('#%s\n' % HEADER_LINE)
        FH.write('\t'.join(plist))

    return fname

def dbinfo(sth):
    'Return a string with currently connected database info.'
    sql = 'SELECT @@hostname AS hostname, database() AS dbname, user() AS uname'
    sth.execute(sql)
    rows = sth.fetchone()
    return 'DB host=%s, name=%s, user=%s' % (rows['hostname'], rows['dbname'], rows['uname'])

def conn_db(prod=False):
    db = None
    if prod:
        db = jgi_connect_db("rqc-prod-rw")
    else:
        db = jgi_connect_db("rqc-dev")
        
    sth = db.cursor(MySQLdb.cursors.DictCursor)
    dbparam = dbinfo(sth)
    print(dbparam)

    return sth

def check_sun(sth, sun):
    # this is bad, never use select *, select the actual fields
    sql = 'select * from seq_units where seq_unit_name = %s'
    sth.execute(sql, (sun,))
    rows = sth.fetchall()
    return rows

def parse_int(aVal, msg):
    try:
        return int(aVal)
    except:
        exit_on_err(msg)

def validate_value(val, obj, msg):
    if type(obj) == dict:
        if val not in xrange(len(obj.keys())):
            print('%s must be in the following:' % msg)
            for k in sorted(obj.keys()):
                print('    %s - %s' % (str(k), obj[k]))
            sys.exit(1)
    elif type(obj) == list:
        if val not in xrange(len(obj)):
            print('%s must be in the following:' % msg)
            for idx, val in enumerate(obj):
                print('    %d - %s' % (idx, val))
            sys.exit(1)

def validate_fm(host, fmajor, fminor):
    # failure mode validation
    FM_MAJOR_CODE = failure_mode_data(host, 'failure_major_code')
    FM_DATA = failure_mode_data(host, 'failure_mode_illumina')
    # pprint(FM_MAJOR_CODE)
    # pprint(FM_DATA)
    if fmajor != 100:
        validate_value(fmajor, FM_MAJOR_CODE, 'Failure Major Code')

        fminor = parse_int(fminor, 'failure mode minor code must be an int value [ %s ]' % fminor)    # failure mode minor has to be in int form
        validate_value(fminor, FM_DATA[FM_MAJOR_CODE[fmajor]], 'Failure Minor Code')

    else:   # multi-failure mode
        toks = fminor.split(',')
        for tk in toks:
            tmp = tk.split(':')
            if len(tmp) != 2:
                exit_on_err('Multiple Failure mode has wrong minor format [ %s ]' % fminor)
            else:
                tmp_fM = parse_int(tmp[0], 'need int in Multi Failure Mode major [%s] in [%s]' % (tk, fminor))
                tmp_fm = parse_int(tmp[1], 'need int in Multi Failure Mode minor [%s] in [%s]'%(tk, fminor))
                validate_value(tmp_fM, FM_MAJOR_CODE, 'Multi Failure Mode major in [%s]'%tk)
                validate_value(tmp_fm, FM_DATA[FM_MAJOR_CODE[tmp_fM]], 'Multi Failure Mode minor in [%s]'%tk)

    return fmajor, fminor


if __name__ == '__main__':
    VERSION = "1.0.1 (2018-01-19)"

    USAGE = "* rework qc helper, version %s\n" % (VERSION)

    # command line options
    parser = argparse.ArgumentParser(description=USAGE, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    TYPES = ['submit', 'save', 'validate']
    YESNO = ['N','Y', 'n', 'y']
    LAB_ACTION = xrange(len(LAB_ACTION_CV)) #
    SOW_STATUS = xrange(len(SOW_STATUS_CV))

    parser.add_argument("-cv", "--cv", dest="cv", action='store_true', help="print option code vocabularies")
    parser.add_argument("-s", "--seq-unit-name", dest="sun", help="seq unit name")
    parser.add_argument("-u", "--data-usable", dest="usable", choices=(0,1), default=1, type=int, help="data useable")
    parser.add_argument("-la", "--lab-action", dest="labaction", choices=LAB_ACTION, default=0, type=int, help="lab action")
    parser.add_argument("-sow", "--sow-status", dest="sow", choices=SOW_STATUS, default=1, type=int, help="sow status")
    parser.add_argument("-tla", "--tla", dest="tla", type=float, help="TLA, if different from ITS value")
    parser.add_argument("-cla", "--cla", dest="cla", type=float, default=0, help="CLA of this submission")
    parser.add_argument("-al", "--abandon-lib", dest="ablib", choices=YESNO, default='N', help="abandon library")
    parser.add_argument("-as", "--abandon-sam", dest="absam", choices=YESNO, default='N', help="abandon sample")
    parser.add_argument("-FM", "--fm-major", dest="fmajor", default=-1, type=int, help="failure mode major ")
    parser.add_argument("-fm", "--fm-minor", dest="fminor", default=-1, help="failure mode minor")
    parser.add_argument("-user", "--user", dest="user", default=None, help="optional user name")
    parser.add_argument("-t", "--type", dest="atype", choices=TYPES, default=TYPES[0], help="Action type.")
    parser.add_argument("-c", "--clean", dest="clean", action='store_true', help="clean up footprints")
    parser.add_argument("-com", "--comment", dest="comment", default='submit by rework_qc_helper tool', help="submission comments")
    parser.add_argument("-p", "--prod", dest="prod", action='store_true', help="Use production db? (default to dev db)")
    parser.add_argument("-l", "--live", dest="live", action='store_true', help="Live run? (default to debug run - do not execute RQC WS call)")
    parser.add_argument('-v', '--version', action='version', version=USAGE)

    OPTIONS = parser.parse_args()

    host = get_host(OPTIONS.prod)
    if OPTIONS.cv:
        print_cv(host)
        sys.exit(0)

    if not OPTIONS.sun:
        exit_on_err('argument -s/--seq-unit-name is required')
    if OPTIONS.cla == None:
        exit_on_err('argument -cla/--cla is required')

    sun = OPTIONS.sun
    sth = conn_db(OPTIONS.prod)

    if not host:
        exit_on_err('Host is None')

    if not check_sun(sth, sun):
        exit_on_err('Seq unit not found in RQC : %s' % sun)

    if OPTIONS.tla:
        tla = OPTIONS.tla
    else:
        tla = get_tla(sun, host)

    param = {'sun'      : sun,
             'tla'      : tla,
             'cla'      : OPTIONS.cla,
             'ablib'    : OPTIONS.ablib.upper(),
             'absam'    : OPTIONS.absam.upper(),
             'labaction': OPTIONS.labaction,
             'usable'   : OPTIONS.usable,
             'sow'      : OPTIONS.sow,
             'fmajor'   : OPTIONS.fmajor,
             'fminor'   : OPTIONS.fminor,
             'comments' : OPTIONS.comment,
             'analyst'  : OPTIONS.user,
             #'type'     : OPTIONS.atype,
             }


    if OPTIONS.fmajor != -1:
        fmajor, fminor = validate_fm(host, OPTIONS.fmajor, OPTIONS.fminor)
        param['fmajor'] = fmajor
        param['fminor'] = fminor

    tmpin = create_tmp_input(param)

    #cmd = 'module load jgi-rqc; auto_rework_qc.py -i %s -t %s' % (tmpin, OPTIONS.atype)
    cmd = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/auto_rework_qc.py -i %s -t %s" % (tmpin, OPTIONS.atype)
    if not OPTIONS.prod:
        cmd += ' --dev'

    if OPTIONS.live:
        cmd += ' -l'

    print('RUN : %s\n' % cmd)

    stdOut, stdErr, exitCode = run_cmd(cmd)
    if exitCode == 0:
        if OPTIONS.clean:
            print('rm %s ...\n' % tmpin)
            os.remove('%s' % tmpin)
            print('rm %s.log ...\n' % tmpin)
            os.remove('%s.log' % tmpin)
        print(stdOut)
    else:
        print('Error : failed to call ws for rework qc submission [%s]' % cmd)
        sys.exit(1)
    
    sys.exit(0)



