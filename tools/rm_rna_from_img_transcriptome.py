#!/usr/bin/env python
'''
	To remove RNA records from transcriptome fasta file downloaded from IMG	

	Author: Shijie Yao
	Update Date  : Oct 06, 2016 - initial implementation
'''

import sys
import os
import argparse
	
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
LOG_FILE = None

if LIB_DIR not in sys.path:
	sys.path.append(LIB_DIR)

import os_utility as util 
if 'run_sh_command' in dir(util):
	run_command = util.run_sh_command 
elif 'run_command' in dir(util):
	run_command = util.run_command






if __name__ == '__main__':
	VERSION = "1.0.0 (10/05/2015)"
	USAGE = '\n'.join([ '* remove RNA record from IMG transcriptome fasta, version %s\n' % (VERSION),
						'To retrieve the RNA gene ids, go to ',
						'https://img.jgi.doe.gov/cgi-bin/mer/main.cgi?section=TaxonDetail&page=taxonDetail&taxon_oid=IMG_OID',
						'scroll down to and click on the RNA link, check all the rna gene checkboxes and Export to spreadsheet.',
						'copy the gene id column into a text file as the --ifile for this program.',
						])
						

	# command line options
	parser = argparse.ArgumentParser(description=USAGE, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument("-f", "--fasta", dest="fasta", help="The fasta file that need to be cleaned.", required=True)
	parser.add_argument("-i", "--ifile", dest="ifile", help="The input file containing RNA gene IDs, one line per id", required=True)
	parser.add_argument('-v', '--version', action='version', version=VERSION)

	OPTIONS = parser.parse_args()

	fasta = OPTIONS.fasta
	ridfile = OPTIONS.ifile

	if not os.path.isfile(fasta):
		print('File not exits : %s' % fasta)
		sys.exit(1)

	if not os.path.isfile(ridfile):
		print('File not exits : %s' % ridfile)
		sys.exit(1)


	# rename fasta file
	ori = '%s.ori' % fasta 
	try:
		os.rename(fasta, ori) 
	except Exception as e:
		print('Error failed to save fasta file : %s' % e)
		sys.exit(1)

	cmd = '~jfroula/Tools/Jazz/screen_list.pl %s %s > %s' % (ridfile, ori, fasta)
	print('Run : %s' % cmd)

	stdo, stde, excode = run_command(cmd, True)
	if excode != 0:
		print('Error : %s' % stde.strip())
		print('\n   Reset \n')
	else:
		print('Done. Original file is saved to %s' % ori)

	

	


