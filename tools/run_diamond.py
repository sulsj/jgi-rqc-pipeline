#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
run_diamon: a wrapper for diamond

Created: Aug 16 2017
sulsj (ssul@lbl.gov)


Description
= Purpose:
    - a wrapper for diamond aligner to replace NCBI blastx (https://github.com/bbuchfink/diamond)
    - DIAMOND is a sequence aligner for protein and translated DNA searches and functions as a drop-in replacement
      for the NCBI BLAST software tools. It is suitable for protein-protein search as well as DNA-protein search on
      short reads and longer sequences including contigs and assemblies, providing a speedup of BLAST ranging up to
      x20,000.

= Reference formatting
    - Each reference fasta file is formatted by "diamond makedb" command on the fly

    $ diamond makedb --in <fasta> --db <diamond database name>

= Default Diamond options: -e 0.000001 --id 90 --threads 16 --outfmt 6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles
  - e: evalue cutoff (default: 0.001)
  - top: report alignments within this percentage range of top alignment score (overrides --max-target-seqs)
  - threads: number of threads
  - outfmt: default output formatting is tabular (6) with customization
  - id: percentage identity %

  - Can be overridden by user-specified options
  - To minimize the number of hits reported: -top 10 --max-target-seqs 1

= Output format of Diamond
    0   = BLAST pairwise
    5   = BLAST XML
    6   = BLAST tabular
    100 = DIAMOND alignment archive (DAA)
    101 = SAM

= Diamond "makedb" and "blastx" Command example
/global/dna/projectdirs/PI/rqc/prod/tools/diamond/diamond makedb --in /global/dna/shared/databases/Acema/Acema1/Acem
a1_GeneModels_FilteredModels1_aa.fasta --db /scratch/tmp/Acema1_GeneModels_FilteredModels1_aa.fasta

&&

/global/dna/projectdirs/PI/rqc/prod/tools/diamond/diamond blastx -q /global/projectb/scratch/sulsj/2017.02.24-mycocosm-blast/pipe
line-test/Orig_MegaHit_Default_AUHNU.fasta -d /scratch/tmp/Acema1_GeneModels_FilteredModels1_aa.fasta -o /global/pro
jectb/scratch/sulsj/2017.02.24-mycocosm-blast/pipeline-test/out-test-diamond/blastout/Orig_MegaHit_Default_AUHNU.fas
ta.vs.Acema1_GeneModels_FilteredModels1_aa.fasta.bout -e 0.000001 --max-target-seqs 1 --outfmt 6 qseqid sseqid bitsc
ore evalue length pident qstart qend qlen sstart send slen 2> /global/projectb/scratch/sulsj/2017.02.24-mycocosm-bla
st/pipeline-test/out-test-diamond/blastout/Orig_MegaHit_Default_AUHNU.fasta.vs.Acema1_GeneModels_FilteredModels1_aa.
fasta.bout.log && rm -rf /scratch/tmp/Acema1_GeneModels_FilteredModels1_aa.fasta


= Dependencies
- diamond diamond v0.9.9.110 (08162017)
- os_utility.py
- taxanomy server connection (taxonomy.jgi-psf.org) for adding lineage info


= Revisions
    08162017 0.0.9: Created
    08212017 1.0.0: Tested Beta released
    08222017 1.0.1: Added NR default database

    08232017 1.1.0: Updated to try to search taxonomy server with fasta header if there is no gi or accession
                    Updated to cope with header string like "1108099_Myxococcus_fulvus_DSM_16525" in get_taxa_by_gi_or_acc()


"""

import os
import sys
import argparse
import platform
import json
from tempfile import mkstemp
import re ## for pctQuery

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from common import get_logger
from rqc_utility import safe_basename, safe_dirname, localize_dir2
from rqc_constants import RQCExitCodes, RQCReferenceDatabases
from os_utility import get_tool_path, run_sh_command, make_dir_p, change_mod, back_ticks, remove_file, move_file

## Globals
VERSION = "1.1.0"
LOG_LEVEL = "DEBUG"
SCRIPT_NAME = __file__

## Outfmt
##
## ex)
## MISEQ04:278:000000000-ALA09:1:1101:5246:13068    gi|851289456|ref|NZ_AXDV01000017.1| 279 9e-74   151 100.00  1   151 151 3509    3659    5516    plus    N/A
## MISEQ04:278:000000000-ALA09:1:1101:5638:14198    gi|851289456|ref|NZ_AXDV01000017.1| 279 9e-74   151 100.00  1   151 151 2881    3031    5516    plus    N/A
##
## ex2)
## MISEQ04:278:000000000-ALA09:1:1101:15574:1382    gi|320006635|gb|CP002475.1| 274 3e-70   151 99.34   1   151 151 4576476 4576626 7337497 591167
## MISEQ04:278:000000000-ALA09:1:1101:15574:1382    gi|478743931|gb|CP003990.1| 268 1e-68   151 98.68   1   151 151 2895762 2895612 7526197 1265601
##
## ex3) with salltitles
## LSSURef, LSURef, SSURef, Collab16s, JGIContaminants do not have gis. Instead, salltitles will contain the lineage info.
##
## vs. LSSURef
## NODE_1_length_1097_cov_5.158615  AHJR01000001.1122280.1125317    2108    0.0 1159    99.482  11159   1159    1716    558 3038    N/A AHJR01000001.1122280.1125317_Archaea;Crenarchaeota;Thermoprotei;Sulfolobales;Sulfolobaceae;Sulfolobus;Sulfolobus_islandicus_M.16.43
##
## vs. collab16s
## NODE_10_length_278_cov_2.492806  38238_Sulfolobus_solfataricus_98/2_W2_resequencing_LIBS_OYPP_POOLS_OPXT 440 2.26e-124   238 100.000 103 340 340 1499    1262    1499    N/A 38238_Sulfolobus_solfataricus_98/2_W2_resequencing_LIBS_OYPP_POOLS_OPXT
##
# DEFAULT_OUTFMT = " --outfmt 6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles"
DEFAULT_OUTFMT = " --outfmt 6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen salltitles"
# DEFAULT_OPTIONS = " -e 0.000001 --id 90 -top 10 --max-target-seqs 1 --threads 16 %s " % (DEFAULT_OUTFMT)
DEFAULT_OPTIONS = " -e 0.000001 --id 90 %s " % (DEFAULT_OUTFMT)


"""
Run diamond and return diamond output in tabular format
"""
def run_diamond(queryFile, dbFile, diamondOptions, outDir, customOutputName, mode, log):
    fastaFileBasename, _ = safe_basename(queryFile, log)
    dbFileBasename, _ = safe_basename(dbFile, log)
    diamondOutDir = outDir
    make_dir_p(diamondOutDir)
    diamondCmd = get_tool_path("diamond", "diamond")

    ## if *.dmnd not exist, create Diamond db on the fly
    ## ex) diamond makedb --in nr_0.005sampled.fasta --db nr_0.005sampled.fasta
    if not os.path.isfile(dbFile + ".dmnd"):
        log.info("No Diamond database found. Try to create one for %s.", dbFile)
        makedbCmd = "%s makedb --in %s --db %s" % (diamondCmd, dbFile, dbFile)
        _, _, exitCode = run_sh_command(makedbCmd, True, log)
        if exitCode == 0:
            log.info("Diamond database successfully created: %s", dbFile + ".dmnd")
        else:
            log.error("Failed to create Diamond database for %s", dbFile)
            return RQCExitCodes.JGI_FAILURE, None
    else:
        log.info("Diamond database found: %s", dbFile + ".dmnd")

    if customOutputName:
        diamondOutFile = os.path.join(diamondOutDir, customOutputName + ".parsed")
    else:
        diamondOutFile = os.path.join(diamondOutDir, "blastx." + fastaFileBasename + ".vs." + dbFileBasename + ".parsed")

    ## diamond v0.9.9.110: /global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/diamond
    ## mode = blastx | blastp
    if queryFile.endswith(".gz"):
        cmd = "zcat %s | %s %s --db %s %s -o %s 2> %s.log " % (queryFile, diamondCmd, mode, dbFile, diamondOptions, diamondOutFile, diamondOutFile)
    else:
        cmd = "%s %s --query %s --db %s %s -o %s 2> %s.log " % (diamondCmd, mode, queryFile, dbFile, diamondOptions, diamondOutFile, diamondOutFile)

    _, _, exitCode = run_sh_command(cmd, True, log)


    if exitCode == 0:
        log.info("run_diamond complete.")
    else:
        log.error("Failed to run_diamond. Unable to run the run_diamond command! Exit code != 0")
        return RQCExitCodes.JGI_FAILURE, None


    return RQCExitCodes.JGI_SUCCESS, diamondOutFile


"""
Get organism name by gi or accession

@param gi_or_acc: gi or accession number (string type)
"""
def get_taxa_by_gi_or_acc(gi_or_acc, log):
    curlCmd = ""
    try:
        int(gi_or_acc)
        curlCmd = "curl https://taxonomy.jgi-psf.org/tax/gi/%s" % gi_or_acc
    except ValueError as e:
        curlCmd = "curl https://taxonomy.jgi-psf.org/tax/accession/%s" % (gi_or_acc)

    stdOut, _, exitCode = run_sh_command(curlCmd, True, log, False, False) ## not to print stdOut to log

    taxaJason = None
    oldKey = None
    newKey = None

    if exitCode == 0:
        ## Try to search taxonomy server with header string if failed to find a lineage info
        if stdOut.find("Not found") != -1:
            curlCmd = "curl https://taxonomy.jgi-psf.org/tax/name/%s" % (gi_or_acc)
            stdOut2, _, exitCode2 = run_sh_command(curlCmd, True, log, False, False)
            assert exitCode2 == 0

            ## ex) curl https://taxonomy.jgi-psf.org/tax/name/1108099_Myxococcus_fulvus_DSM_16525
            ## 1108099_Myxococcus_fulvus_DSM_16525 ==> Myxococcus fulvus DSM
            ## And search the name from taxonomy server
            ##
            ## Orig. gi_or_acc = 1108099_Myxococcus_fulvus_DSM_16525
            ## Searched lineage by "Myxococcus fulvus DSM" ==> {u'Myxococcus fulvus DSM': {u'superkingdom': {u'name': u'Bacteria', u'tax_id': u'2'}, u'name': u'Myxococcus fulvus', u'family': {u'name': u'Myxococcaceae', u'tax_id': u'31'}, u'level': u'species', u'class': {u'name': u'Deltaproteobacteria', u'tax_id': u'28221'}, u'order': {u'name': u'Myxococcales', u'tax_id': u'29'}, u'phylum': {u'name': u'Proteobacteria', u'tax_id': u'1224'}, u'suborder': {u'name': u'Cystobacterineae', u'tax_id': u'80811'}, u'subphylum': {u'name': u'delta/epsilon subdivisions', u'tax_id': u'68525'}, u'genus': {u'name': u'Myxococcus', u'tax_id': u'32'}, u'species': {u'name': u'Myxococcus fulvus', u'tax_id': u'33'}, u'tax_id': u'33'}}
            ##
            ## Note: Need to change the key name "Myxococcus fulvus DSM" to "1108099_Myxococcus_fulvus_DSM_16525" using newKey and oldKey
            ##
            if stdOut2.find("Not found") != -1:
                newKey = gi_or_acc
                oldKey = ' '.join([i for i in gi_or_acc.split('_') if not i.isdigit()])
                gi_or_acc = '%20'.join([i for i in gi_or_acc.split('_') if not i.isdigit()])

                curlCmd = "curl https://taxonomy.jgi-psf.org/tax/name/%s" % (gi_or_acc)
                stdOut3, _, exitCode3 = run_sh_command(curlCmd, True, log, False, False)
                assert exitCode3 == 0

                if stdOut3.find("Not found") != -1:
                    return RQCExitCodes.JGI_FAILURE, None
                else:
                    stdOut = stdOut3
            else:
                stdOut = stdOut2

        jstr = stdOut.strip()

        if jstr:
            try:
                taxaJason = json.loads(jstr)
            except ValueError as e:
                log.error("Failed to convert return string from tax server to json - [%s][%s] %s" % (gi_or_acc, jstr, e))
                return RQCExitCodes.JGI_FAILURE, None
    else:
        log.error("Failed to call curl. Exit code != 0")
        return RQCExitCodes.JGI_FAILURE, None

    try:
        if newKey is not None:
            taxaJason[newKey] = taxaJason.pop(oldKey)
    except KeyError as e:
        log.error("Invalid key name: %s %s %s", newKey, oldKey, e)


    return RQCExitCodes.JGI_SUCCESS, taxaJason



"""
Get full lineage from gi
"""
def get_lineage(gi, taxaDict):
    fl = "n.a."
    if gi in taxaDict and 'lineage' in taxaDict[gi] and taxaDict[gi]['lineage'] != "-1" and  taxaDict[gi]['lineage'] != "":
        fl = taxaDict[gi]['lineage'].replace(" ", "_")

    return fl


##==============================
if __name__ == '__main__':
    desc = "RQC Diamond wrapper"
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-c", "--output-name", dest="outputName", help="Custom output file name")
    parser.add_argument("-d", "--db", dest="dbFile", help="Reference Diamond db. Full path to Diamond DB or reference fasta", required=True)
    parser.add_argument("-f", "--add-salltitles", action="store_true", help="Append salltitles to subject field and skip collecting lineage info", dest="addSalltitles", default=False, required=False)
    parser.add_argument("-k", "--keep-orig-parsed", action="store_true", help="Keep original parsed output", dest="keepOrigParsed", default=False, required=False)
    parser.add_argument("-m", "--mode", dest="diamondMode", help="blastp or blastx", required=True)
    parser.add_argument("-n", "--add-pct-query", action="store_true", help="Add pctQuery value in taxlist output", dest="addPctQuery", default=False, required=False)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Target path to write output files to")
    parser.add_argument("-p", "--diamond-options", dest="diamondOptions", help="User-specified Diamond options. Default: --outfmt '6 qseqid sseqid \
                        bitscore evalue length pident qstart qend qlen sstart send slen salltitles'. Note: lineage info, top hits, top 100 hits, and tax list \
                        files will be generated only with outfmt = 6 (non-custom). Ex1) run_diamond.py -p=\"--evalue 0.001\", Ex2) run_diamond.py \
                        -p=\"--evalue 0.00001 --id 90 --outfmt 5\", Ex3) run_diamond.py -p=\"--evalue 1e-20 --outfmt '6 qseqid sseqid bitscore \
                        evalue pident'\"", required=False)
    parser.add_argument("-q", "--query", dest="queryFile", help="Input query file (full path to fasta or gzipped fasta)", required=True)
    parser.add_argument("-s", "--add-species", action="store_true", help="Add taxonomy lineage/organism name to subject field", dest="addLineage", default=False, required=False)
    parser.add_argument("-t", "--num-threads", dest="numThreads", help="Number of threads to run (default: 16)", default=16, required=False)
    parser.add_argument("-v", "--version", action="version", version=VERSION)

    options = parser.parse_args()

    ## Mandatory options
    outputPath = os.getcwd()
    queryFile = options.queryFile
    dbFile = options.dbFile
    assert options.diamondMode in ("blastx", "blastp"), "ERROR: Diamond mode should be either 'blastx' or 'blastp'"
    diamondMode = options.diamondMode

    ## Default dababase
    if dbFile == "nr":
        dbFile = "/global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nr/DIAMOND/nr"


    diamondOutfmtOption = None
    addLineage = False ## append lineage info to subject
    customOutputName = None
    addSalltitles = False ## append salltitles to subject
    keepOrigParsed = False
    addPctQuery = False

    if options.outputPath:
        outputPath = options.outputPath
        make_dir_p(outputPath)

    if options.diamondOptions:
        if options.diamondOptions.find("--outfmt") == -1: ## force to generate tabular output
            options.diamondOptions += DEFAULT_OUTFMT
            diamondOutfmtOption = "default"

        DEFAULT_OPTIONS = options.diamondOptions

    if options.numThreads:
        numThreads = options.numThreads

    if DEFAULT_OPTIONS.find("threads") == -1:
        DEFAULT_OPTIONS += " --threads %s " % (numThreads)

    if options.addLineage:
        addLineage = True

    if options.addSalltitles:
        addSalltitles = True

    if options.keepOrigParsed:
        keepOrigParsed = True

    if options.addPctQuery:
        addPctQuery = True

    if options.outputName:
        customOutputName = options.outputName


    logFileName = "run_diamond.log"
    log = get_logger("run_diamond", os.path.join(outputPath, logFileName), LOG_LEVEL, False, True)


    log.info("=================================================================")
    log.info("   RQC Diamond wrapper (version %s)", VERSION)
    log.info("=================================================================")

    log.info("Starting %s with %s query file...", SCRIPT_NAME, queryFile)


    ## Get the number of query to compte pctQuery value and set it in taxlist
    totalQuery = 0 ## total number of query

    if addPctQuery:
        reformatshCmd = get_tool_path("reformat.sh", "bbtools")
        cmd = "%s %s" % (reformatshCmd, queryFile)
        _, stdErr, exitCode = run_sh_command(cmd, True, None, True)
        assert exitCode == 0

        for l in stdErr.split('\n'):
            if l.startswith("Input:"):
                m = re.findall("(\d+.\d*)", l)
                log.info("Input query read and base cnt = %s, %s", m[0], m[1])
                totalQuery = int(m[0])
                break


    ############################################################################
    ## Run Diamond
    ############################################################################
    log.info("Diamond search options: %s", DEFAULT_OPTIONS)

    retCode, diamondOutputFile = run_diamond(queryFile, dbFile, DEFAULT_OPTIONS, outputPath, customOutputName, diamondMode, log)

    if retCode == 0:
        status = "run_diamond complete"
    else:
        status = "run_diamond failed"
        log.error("Failed to run_diamond. Unable to run the run_diamond command! Exit code != 0")
        exit(-1)

    assert os.path.isfile(diamondOutputFile), "ERROR: diamond output file not found"



    ############################################################################
    ## Post processing
    ############################################################################

    ##
    ## Check diamond output format options before running postprocessing
    if options.diamondOptions:
        optList = options.diamondOptions.strip().split()
        i = optList.index("--outfmt")
        if diamondOutfmtOption != "default":
            if optList[i + 1][0] in ('"', "'"):
                log.error("Diamond custom output format is not supported: %s", DEFAULT_OPTIONS)
                exit(-1)
            else:
                diamondOutfmtOption = int(optList[i + 1].replace("'", "").replace('"', ""))


    ##
    ## Add header line to the parsed output for default and --outfmt 10
    ##
    assert os.path.isfile(diamondOutputFile), "ERROR: diamond output not found."

    if os.path.isfile(diamondOutputFile):
        if not options.diamondOptions or diamondOutfmtOption == "default":
            headerStr = "#qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen"

            ## if "-s" is not set, the salltitles filed will be remained in the last column of blast output file, *.parsed
            ## if set, the salltitles will be appended to the subject field string.
            if not addLineage:
                headerStr += " salltitles"

        if diamondOutfmtOption == 6:
            headerStr = "seqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore"

        headerAddCmd = """echo "%s" | cat - %s | sed 's/:/_/g; s/;/_/g' > %s && mv %s %s"""\
                       % (headerStr, diamondOutputFile, os.path.join(outputPath, "temp.parsed"), os.path.join(outputPath, "temp.parsed"), diamondOutputFile)

        _, _, exitCode = run_sh_command(headerAddCmd, True, log)

        if exitCode != 0:
            log.error("Failed to add header to diamond output. Exit code != 0")
            exit(-1)

    assert os.path.isfile(diamondOutputFile), "ERROR: adding header to diamond output failed."

    log.info("Diamond outfmt option = %s", diamondOutfmtOption)
    log.info("Diamond option = %s", DEFAULT_OPTIONS)



    ##
    ## Collect unique gi from parsed output
    ## taxonomyInfoDict will be used to store lineage info per each gi
    ##
    log.info("Creating gi list...")
    taxonomyInfoDict = {} ## {unique gi: {lineage, salltitles, cnt, perc}} (here, gi can be real gi or a part of subject header string)

    with open(diamondOutputFile, 'r') as TOPFH:
        for l in TOPFH:
            if l.startswith('#'):
                continue

            try:
                (qseqid, sseqid, bitscore, evalue, length, pident, qstart, qend, qlen, sstart, send, slen, salltitles) = l.rstrip().split('\t')
            except ValueError:
                log.warning("No Diamond hits found or tophit file format error: %s", l)
                break

            ## gi|851289456|ref|NZ_AXDV01000017.1|
            ## or
            ## gi|472256744| gi|461490773| gi|71143482| gi|461490773|
            ## or
            ## 5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB
            ## or
            ## ADDN01001501.390762.393334
            ##
            ## or (since Apr 2017)
            ## ref|accession#...
            ## or
            ## accession|accession# or accession accession#
            ## or
            ## gbk|accession#
            ##
            if sseqid.startswith("gi|"):
                gi = sseqid.split('|')[1] ## string gi
                if gi not in taxonomyInfoDict:
                    taxonomyInfoDict[gi] = {} ## new entry

            elif sseqid.startswith("ref|") or sseqid.startswith("accession|") or sseqid.startswith("gbk|"):
                acc = sseqid.split('|')[1]
                if acc not in taxonomyInfoDict:
                    taxonomyInfoDict[acc] = {} ## new entry

            elif sseqid.startswith("accession "):
                ## ex) accession NC_001422.1 abcabc
                ## ex) accession NC_001422.1|abcabc
                acc = sseqid.split('|')[0].split(' ')[1]
                if acc not in taxonomyInfoDict:
                    taxonomyInfoDict[acc] = {} ## new entry

            else:
                ## HE582776.1.1512 Bacteria;Proteobacteria;Gammaproteobacteria;Pseudomonadales;Moraxellaceae;uncultured;Agitococcus lubricus
                ## To lookup the taxonomy server with header
                h = sseqid.split()[0]
                if h not in taxonomyInfoDict:
                    taxonomyInfoDict[h] = {} ## new entry


    log.info("Number of gi(s) and accession(s) collected from .parsed: %s", len(taxonomyInfoDict))


    ##
    ## Create the lineage
    ##
    log.info("Finding lineage...")

    # ranks = ['domain', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    ranks = ['superkingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    ranksLookup = dict([(r, idx) for idx, r in enumerate(ranks)])
    speciesNameDict = {}

    ## For collected gis from parsed output, create lineage info using gi2taxid nad NCBI tax tree
    for gi_or_acc, v in taxonomyInfoDict.iteritems():
        ret, taxJson = get_taxa_by_gi_or_acc(gi_or_acc, log)

        if ret == RQCExitCodes.JGI_SUCCESS:
            if taxJson is None:
                log.warning("tax id not found for gi_or_acc = %s", str(gi_or_acc))
                continue

            if gi_or_acc in taxJson:
                lineage = [""] * len(ranks)
                for k2, v2 in taxJson[gi_or_acc].iteritems():
                    if "name" in v2 and k2 in ranksLookup: ## ignore subspecies, kingdom
                        try:
                            lineage[ranksLookup[k2]] = v2["name"]
                        except (TypeError, ValueError, KeyError) as err:
                            log.warning("Exception: %s %s", err, err.args)

                if len(lineage[-1]) > 0: ## lineage[6]
                    speciesName = lineage[-1]

                    ## if species name exists in speciesNameDict, update the cnt and perc instead of creating new cnt and perc
                    ## 05242017 Now that we are using accession number, there are cases that the same species have different accession numbers
                    ## so disable this species name comparison.
                    if speciesName in speciesNameDict and False:
                        v['lineage'] = "-1" ## not to save this to taxlist file
                    else:
                        speciesNameDict[speciesName] = gi_or_acc
                        v['lineage'] = ";".join(lineage)

                else: ## if there is no species name found
                    v['lineage'] = ";".join(lineage)


    ##
    ## Processing parsed file to add taxonomy
    ##
    if addLineage:
        log.info("Adding lineage info to parsed file...")

        fh, tempFile = mkstemp()

        with open(tempFile, 'w') as NEW_PFH:
            with open(diamondOutputFile, 'r') as PFH:
                for l in PFH:
                    if l.startswith('#'):
                        NEW_PFH.write(l)
                        continue

                    try:
                        (qseqid, sseqid, bitscore, evalue, length, pident, qstart, qend, qlen, sstart, send, slen, salltitles) = l.rstrip().split('\t')
                    except ValueError as e:
                        log.error("diamond parsed file format error: %s, %s", diamondOutputFile, e)
                        break

                    ## gi|851289456|ref|NZ_AXDV01000017.1|
                    ## or
                    ## gi|472256744| gi|461490773| gi|71143482| gi|461490773|
                    ## or
                    ## 5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB
                    ## or
                    ## ADDN01001501.390762.393334
                    ##
                    ## or (since Apr 2017)
                    ## ref|accession#...
                    ## or
                    ## accession|accession# or accession accession#
                    ## or
                    ## gbk|accession#
                    ##
                    ## Silva
                    ## FJ009592.1.2044 Eukaryota_Opisthokonta_Nucletmycea_Fungi_Kickxellomycotina_Glomeromycota_Glomeromycetes_Glomerales_Rhizophagus_Rhizophagus intraradices

                    ## Request from Jasmyn
                    if addSalltitles:
                        ## ex) sseqid salltitles = ref|NC_015711.1| ref|NC_015711.1| Myxococcus fulvus HW-1, complete genome
                        if salltitles.startswith(sseqid):
                            salltitles2 = salltitles.replace(sseqid, '').strip()
                        else:
                            salltitles2 = salltitles.strip()

                        sseqid = sseqid + ' ' + salltitles2


                    if sseqid.startswith("gi|") or sseqid.startswith("ref|") or sseqid.startswith("accession|") or sseqid.startswith("accession ") or sseqid.startswith("gbk|"):
                        gi = sseqid.split('|')[1]
                        fullLineage = get_lineage(gi, taxonomyInfoDict)
                        lineageNameToPrint = "n.a."

                        if fullLineage != "n.a.":
                            lineageNameToPrint = fullLineage
                        elif salltitles != "":
                            # if salltitles.split()[0].startswith("gi|"): ## if salltitles contains subject gi field like "gi|910084296|ref|NZ_BBMP01000017.1| Halococcus sediminico ..."
                            if sseqid.startswith("gi|") or sseqid.startswith("ref|") or sseqid.startswith("accession|") or sseqid.startswith("accession ") or sseqid.startswith("gbk|"):
                                salltitles = " ".join(salltitles.split()[1:])
                            lineageNameToPrint = salltitles.replace(" ", '_').replace(':', '_') ## salltitles may have ':'

                        NEW_PFH.write("\t".join([qseqid, sseqid + ':' + lineageNameToPrint, bitscore, evalue, length, pident, qstart, qend, qlen, sstart, send, slen]) + '\n')

                    else:
                        ## TODO: is there any ';' left in salltitles?
                        if salltitles.find(';') != -1: ## if header contains lineage info
                            toks = salltitles.split(';')
                            toks[0] = toks[0].split(' ')[-1] ## remove subject name from the lineage ex) FJ231214.1.2349 Eukaryota;...;;;; -> Eukaryota;...;;;;
                            salltitles = ';'.join(toks)
                        else:
                            ## There is no lineage info but species name
                            ## Just append the species name after the subject title
                            ## ex) 5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB:5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB

                            ## 05242017
                            key = salltitles.split()[0]
                            fullLineage = get_lineage(key, taxonomyInfoDict)

                            # if fullLineage not in ("n.a.", ";;;;;;"):
                            if fullLineage != "n.a.":
                                lineageNameToPrint = fullLineage
                            else:
                                lineageNameToPrint = salltitles.replace(" ", '_').replace(':', '_')

                        ## This is the place to add lineage to subject (with ':' as a divider)
                        NEW_PFH.write("\t".join([qseqid, sseqid + ':' + lineageNameToPrint, bitscore, evalue, length, pident, qstart, qend, qlen, sstart, send, slen]) + '\n')


        os.close(fh)

        if not keepOrigParsed:
            remove_file(diamondOutputFile)
        else:
            move_file(diamondOutputFile, diamondOutputFile + ".orig")

        move_file(tempFile, diamondOutputFile)
        change_mod(diamondOutputFile, "0666")
        back_ticks(["chgrp", "genome", diamondOutputFile])



    ############################################################################
    ## Create .tophit and .top100hit files
    ############################################################################

    ## Input format (diamond output)
    ## OLD               3      4      5      6                 9                        13
    ## qseqid sseqid bitscore evalue length pident qstart qend qlen qstrand sstart send slen sstrand staxids
    ##
    ## NOTE: failed to get qstrand 02052016
    ##
    ## NEW              3       4      5      6                 9                12     13
    ## qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids
    ##
    ## NEW 2             3       4      5      6                 9                12     13     14
    ## qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles
    ##
    ## NEW 3             3       4      5      6                 9                12     13
    ## qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen salltitles

    ##
    ## Output tophit format
    ##   1       2       3      4       5
    ## query  subject  expect  length  %id  q_length  s_length
    ##
    ## ex)
    ## #query subject expect length perc_id q_length s_length
    ## MISEQ04:278:000000000-ALA09:1:2114:18141:5140 gi|851289456|ref|NZ_AXDV01000017.1| 5e-37 85 100.00 151 5516
    ## MISEQ04:278:000000000-ALA09:1:1104:14980:2927 gi|850494210|ref|NZ_JJQO01000102.1| 2e-31 91 94.51 151 2246
    ## MISEQ04:278:000000000-ALA09:1:1107:16838:26088 gi|850494210|ref|NZ_JJQO01000102.1| 2e-31 91 94.51 151 2246

    ## Sorting
    ## gensub(regexp, replacement, how [, target])
    ## 1. sort diamond output by query (asc) & by bitscore (asc) & by perc identity (asc)
    ## 2. replace " " with "  " in subject
    ## 3. replace " " with "_" in subject
    ## 4. collect best hits per query (by a[$1] dict)
    ## 4. sort by evalue (asc) & by percentage identity (desc)
    ##
    ## The original Diamond sorting criteria
    ## 1. Expect Value: default
    ## 2. Max Score: By the bit score of HSPs, similar to Expect Value
    ## 3. Total Score: By the sum of scores from all HSPs from the same database sequence
    ## 4. Query Coverage: By the percent of length coverge for the query
    ## 5. Max Identity: By the maximal percent ID of the HSPs
    ## Here sort by 1, 2, and 5 only

    ##
    ## 04.14.2016: Changed evalue sort param from "sort -k 4,4n -k 6,6n" to "sort -g -k3,3 -k5,5n"
    ## 04.20.2016: Changed evalue sort param: "sort -k3,3g -k5,5rn"
    ##

    ## if --outfmt=6
    ## ex)
    ## MISEQ04:278:000000000-ALA09:1:1101:10764:6462    gi|851289456|ref|NZ_AXDV01000017.1| 100.00  151 0   01151   2908    3058    9e-74   279
    ##

    if not options.diamondOptions or diamondOutfmtOption == "default":
        tophitCmd = """echo "#query subject expect length perc_id q_length s_length" > %s ;
grep -v '^#' %s 2>/dev/null |
sort -t$'\t' -k1,1 -k3,3n -k6,6n |
gawk -F'[\t]' '!/^#/{
s=gensub(" ", "  ", 1, $2); # creates new column:=3
t=gensub(" ", "_" , "g" ,s)
a[$1]=t" "$4" "$5" "$6" "$9" "$12} # a[$1]=t" "$4" "$5" "$6" "$9" "$12" "$13}
END{ for(e in a) print e,a[e] }' |
sort -k3,3g -k5,5rn >> %s""" % (diamondOutputFile + ".tophit", diamondOutputFile, diamondOutputFile + ".tophit")

        _, _, exitCode = run_sh_command(tophitCmd, True, log)

        if exitCode == 0:
            log.info("Diamond tophit file created: %s", diamondOutputFile + ".tophit")
        else:
            log.error("Failed to run_diamond. Failed to create tophit file. Exit code != 0")
            exit(-1)


        ## Create top100 hit file from *.parsed
        ## 1. ordered by bit score (desc) and evalue (asc) and %id (desc) and query_id
        ## 2. get unique query
        ##
        top100Cmd = """echo "#qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen" > %s;
grep -v '^#' %s 2>/dev/null |
sort -t$'\t' -k3,3rn -k4,4g -k6,6rn -uk1,1 |
head -n 100 >> %s""" % (diamondOutputFile + ".top100hit", diamondOutputFile, diamondOutputFile + ".top100hit")

        _, _, exitCode = run_sh_command(top100Cmd, True, log)

        if exitCode == 0:
            log.info("Diamond top100 hit file created: %s", diamondOutputFile + ".top100hit")
        else:
            log.error("Failed to run_diamond. Failed to create top100 hit file. Exit code != 0")
            exit(-1)

    elif diamondOutfmtOption == 6:
        ##    1           2           3             4              5           6        7         8       9         10      11       12
        ## query id, subject id, % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score
        tophitCmd = """echo "#query subject expect length perc_id q_length s_length" > %s ;
grep -v '^#' %s 2>/dev/null |
sort -t$%s -k1,1 -k12,12n -k3,3n |
gawk -F'[%s]' '!/^#/{
s=gensub(" ", "  ", 1, $2); # creates new column:=3
t=gensub(" ", "_" , "g" ,s)
a[$1]=t" "$11" "$4" "$3" "$8-$7+1" "sqrt(($10-$9)^2)+1" "$5}
END{ for(e in a) print e,a[e] }' |
sort -k3,3g -k5,5rn >> %s"""

        tophitCmd = tophitCmd % (diamondOutputFile + ".tophit", diamondOutputFile, "'\\t'", "\\t", diamondOutputFile + ".tophit")

        _, _, exitCode = run_sh_command(tophitCmd, True, log)

        if exitCode == 0:
            log.info("Diamond tophit file created: %s", diamondOutputFile + ".tophit")
        else:
            log.error("Failed to run_diamond. Failed to create tophit file. Exit code != 0")
            exit(-1)


        ## Create top100 hit file from *.parsed
        ## 1. ordered by bit score (desc) and evalue (asc) and %id (desc)
        ## 2. get unique query
        ##
        top100Cmd = """echo "#qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen" > %s;
grep -v '^#' %s 2>/dev/null |
gawk -F'[%s]' '{print $1"\t"$2"\t"$12"\t"$11"\t"$4"\t"$3"\t"$7"\t"$8"\t"$8-$7+1"\t"$9"\t"$10"\t"sqrt(($10-$9)^2)+1"\t"$5}' |
sort -t$'\t' -k3,3rn -k4,4g -k6,6rn -uk1,1 |
head -n 100 >> %s"""

        top100Cmd = top100Cmd % (diamondOutputFile + ".top100hit", diamondOutputFile, "\\t", diamondOutputFile + ".top100hit")

        _, _, exitCode = run_sh_command(top100Cmd, True, log)

        if exitCode == 0:
            log.info("Diamond top100 hit file created: %s", diamondOutputFile + ".top100hit")
        else:
            log.error("Failed to run_diamond. Failed to create top100 hit file. Exit code != 0")
            exit(-1)

    else:
        log.warning("The Diamond option is not supported yet: %s", DEFAULT_OPTIONS)
        log.warning("The top hit, top 100 hit, and taxlist output files will not be created.")
        exit(-1)



    ############################################################################
    ## Create .taxlist file
    ############################################################################

    totalTophits = 0
    taxonomyInfoDict.clear()
    speciesNameDict.clear()

    ## Collect the gis from tophit file
    with open(diamondOutputFile + ".tophit", 'r') as TOPFH:
        for l in TOPFH:
            if l.startswith('#'):
                continue

            try:
                (query, subject, expect, length, percId, qLength, sLength) = l.rstrip().split()
            except ValueError:
                log.warning("No Diamond hits found or tophit file format error: %s", l)
                break

            ## gi|9626372|ref|NC_001422.1|:Viruses;;;;Microviridae;Microvirus;Enterobacteria_phage_phiX174_sensu_lato
            ## or
            ## gi|472256744| gi|461490773| gi|71143482| gi|461490773|:Viruses;;;;Microviridae;Microvirus;Enterobacteria_phage_phiX174_sensu_lato
            ## or
            ## 5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB:5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB
            ## or
            ## ADDN01001501.390762.393334:Eukaryota;Viridiplantae;Streptophyta;Embryophyta;Tracheophyta;Spermatophyta;Magnoliophyta;Liliopsida;Poales;Poaceae;BEP_clade;Pooideae;Brachypodieae;Brachypodium;;Brachypodium_distachyon
            ## X86563.95161.96651:Bacteria;Cyanobacteria;Chloroplast;Zea_mays
            ## or
            ## ref|NC_001422.1|:;;;;;;

            ## NEW 07212016 using the subject title as key instead of using gi (for subject title w/o gi)
            ## Count the hit numbers

            ##
            ## or (since Apr 2017)
            ## ref|accession#...
            ## or
            ## accession|accession# or accession accession#
            ## or
            ## gbk|accession#
            ##

            ## Silva
            ## HQ191393.1.2397:HQ191393.1.2397_Eukaryota_Opisthokonta_Nucletmycea_Fungi_Chytridiomycota_Chytridiomycetes_uncultured_uncultured_Chytridiomycota
            ## EWC01043727.1704.3050_Eukaryota_Archaeplastida_Chloroplastida_Charophyta_Phragmoplastophyta_Streptophyta_Embryophyta_Tracheophyta_Spermatophyta_Magnoliophyta_Solanales_Solanum_tuberosum_(potato)

            gi_or_acc = subject.split(':')[0]
            if gi_or_acc not in taxonomyInfoDict: ## load new taxonomyInfoDict
                taxonomyInfoDict[gi_or_acc] = {} ## new entry
                taxonomyInfoDict[gi_or_acc]['cnt'] = 1
                if addLineage:
                    if subject.find(':') != -1:
                        taxonomyInfoDict[gi_or_acc]['salltitles'] = subject.split(':')[-1]

            else:
                taxonomyInfoDict[gi_or_acc]['cnt'] += 1

            totalTophits += 1

    log.info("Number of tophits processed: %s", totalTophits)
    log.debug("Number of unique subject: %s", len(taxonomyInfoDict))
    # log.debug("taxonomy Info Dict: %s", taxonomyInfoDict)


    ## Collect the lineage
    for gi_or_acc, v in taxonomyInfoDict.iteritems():
        if gi_or_acc.startswith("gi|") or gi_or_acc.startswith("ref|") or gi_or_acc.startswith("accession|") or gi_or_acc.startswith("accession ") or gi_or_acc.startswith("gbk|"):
            gi_or_acc2 = ""
            if gi_or_acc.startswith("gi|") or gi_or_acc.startswith("ref|") or gi_or_acc.startswith("accession|") or gi_or_acc.startswith("gbk|"):
                gi_or_acc2 = gi_or_acc.split('|')[1]
            elif gi_or_acc.startswith("accession "):
                ## ex) accession NC_001422.1 abcabc
                ## ex) accession NC_001422.1|abcabc
                gi_or_acc2 = gi_or_acc.split('|')[0].split(' ')[1]

            try:
                ## TODO: remove this and use appended lineage info from header line
                ret, taxJson = get_taxa_by_gi_or_acc(gi_or_acc2, log)

                if taxJson is None:
                    log.warning("tax id not found for gi from %s", str(gi_or_acc))

                    ## RQCSUPPORT-813
                    ## gi cannot be converted to taxid b/c it's new.
                    ## ex) gi|1042821167|gb|CP016182.1|:Escherichia_coli_strain_EC590,_complete_genome
                    ## --> use "Escherichia_coli_strain_EC590,_complete_genome"
                    if "salltitles" in v and v['salltitles']:
                        speciesName = v['salltitles'].split(';')[-1]

                        if speciesName in speciesNameDict:
                            taxonomyInfoDict[speciesNameDict[speciesName]]['cnt'] += int(v['cnt'])
                            taxonomyInfoDict[speciesNameDict[speciesName]]['perc'] += (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                            taxonomyInfoDict[speciesNameDict[speciesName]]['pct_query'] += (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0
                            # v['lineage'] = "-1" ## not to save this to taxlist file
                        else:
                            speciesNameDict[speciesName] = gi_or_acc
                            v['species'] = speciesName
                            v['lineage'] = ";;;;;;" + v['salltitles']
                            v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0 ## compute percentage of tophits
                            v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0 ## compute percentage of query

                else:
                    lineage = [""] * len(ranks)
                    for k2, v2 in taxJson[gi_or_acc.split('|')[1]].iteritems():
                        if "name" in v2 and k2 in ranksLookup: ## ignore subspecies, kingdom
                            try:
                                lineage[ranksLookup[k2]] = v2["name"]
                            except (TypeError, ValueError, KeyError) as err:
                                log.warning("Exception: %s %s", err, err.args)

                    if len(lineage[-1]) > 0: ## lineage[6]
                        speciesName = lineage[-1]

                        ## if species name exists in speciesNameDict, update the cnt and perc instead of creating new cnt and perc
                        if speciesName in speciesNameDict:
                            taxonomyInfoDict[speciesNameDict[speciesName]]['cnt'] += int(v['cnt'])
                            taxonomyInfoDict[speciesNameDict[speciesName]]['perc'] += (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                            taxonomyInfoDict[speciesNameDict[speciesName]]['pct_query'] += (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0
                            v['lineage'] = "-1" ## not to save this to taxlist file

                        else:
                            ## need to assign part of header string not the gi only for non-standard header format
                            speciesNameDict[speciesName] = gi_or_acc
                            v['species'] = speciesName
                            v['lineage'] = ";".join(lineage)
                            v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                            v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0

                    else: ## if there is no species name found
                        v['lineage'] = ";".join(lineage)
                        v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                        v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0

            except KeyError:
                v['lineage'] = ""
                v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0

        else: ## no gi found
            if 'salltitles' in v and v['salltitles']:
                speciesName = v['salltitles'].split(';')[-1]

                if speciesName in speciesNameDict:
                    taxonomyInfoDict[speciesNameDict[speciesName]]['cnt'] += int(v['cnt'])
                    taxonomyInfoDict[speciesNameDict[speciesName]]['perc'] += (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                    v['lineage'] = "-1" ## not to save this to taxlist file

                else:
                    speciesNameDict[speciesName] = gi_or_acc
                    v['species'] = speciesName
                    v['lineage'] = v['salltitles']
                    v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                    v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0

            else:
                v['lineage'] = ""
                v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0



    log.info("Creating taxlist file...")

    with open(diamondOutputFile + ".taxlist", 'w') as TLFH:
        TLFH.write("#name\tnumHits\tpctHit\tpctQuery\ttaxonomy\n")
        for gi_or_acc in sorted(taxonomyInfoDict.keys(), key=lambda y: (taxonomyInfoDict[y]['cnt']), reverse=True): ## sort dict by cnt
            if 'lineage' in taxonomyInfoDict[gi_or_acc] and taxonomyInfoDict[gi_or_acc]['lineage'] != "-1" and  taxonomyInfoDict[gi_or_acc]['lineage'] != "":
                ## For backward compatibility, add the unused field, pctQuery
                ## 08072017 compute pctQuery = numHits / numQuery * 100.0

                ## Output format
                ## speciesName  numHits   pctHit  pctQuery  taxonomyLineage

                ## For {'ref|NC_001422.1|': {'lineage': ';;;;;;', 'perc': 100.0, 'cnt': 5, 'salltitles': ';;;;;;'}}
                speciesName = taxonomyInfoDict[gi_or_acc]['lineage'].split(';')[-1].replace(" ", "_")
                if speciesName == "":
                    speciesName = gi_or_acc

                TLFH.write("%s\t%s\t%.2f\t%.2f\t%s\n" % (speciesName,
                                                         taxonomyInfoDict[gi_or_acc]['cnt'],
                                                         taxonomyInfoDict[gi_or_acc]['perc'],
                                                         taxonomyInfoDict[gi_or_acc]['pct_query'],
                                                         taxonomyInfoDict[gi_or_acc]['lineage'].replace(" ", "_")))

    if os.path.isfile(diamondOutputFile + ".taxlist"):
        log.info("Taxlist file created: %s", diamondOutputFile + ".taxlist")
    else:
        log.error("Failed to run_diamond. Failed to create taxlist file. Exit code != 0")
        exit(-1)


    log.info("run_diamond.py completed.")

    exit(0)


## EOF
