#!/usr/bin/env perl

use strict;

use Getopt::Long;


#option vars
my ($opt_kmer, $opt_unpaired, $opt_minrl, $opt_max_errors, $opt_debug, $opt_help);
my $max_errors_default = 1;
my $kmer_default = 21;
my $min_rl_default = 45;

GetOptions(
     'k=i'       => \$opt_kmer,
     's=i'       => \$opt_max_errors,
     'm=i'       => \$opt_minrl,
     'u'         => \$opt_unpaired,
     'd'         => \$opt_debug,
     'h'         => \$opt_help,
);

my $usage = <<"USAGE";
    removes linker or artifact sequences from 3' end of reads.
    

    $0 [options] <linker/artifact.fa> <fastq or - for STDIN>

    options:
        -k <int>    match kmers of <int> length. 
                      kmers at the ends of reads can be shorter. [$kmer_default]

        -s <int>    max number of errors allowed (Hamming). 
                      <=2 recommended due to speed. [$max_errors_default]

        -m <int>    min read length. both reads must be longer than
                      <int> in paired mode. [$min_rl_default]

        -u          ignore pairing info. [off]

        -d          debug. warning: very chatty
        -h          print this msg

USAGE

if ($opt_help) {
    print $usage;
    exit;
}

my $ref_fa = $ARGV[0] || '';
my $fastq = $ARGV[1] || '';

unless (-f $ref_fa && (-f $fastq || ($fastq eq '-' && !-t STDIN))) {
    #print "ref: '$ref_fa'\n";
    #print "fq: '$fastq'\n";
    print $usage;
    exit;
}

if($opt_debug){
    print "main: linker fa $ref_fa\n";
    print "main: input fq $fastq\n";
}

my %ref_mers = ();
$opt_kmer = $opt_kmer || $kmer_default;
$opt_max_errors = $opt_max_errors || $max_errors_default;
$opt_debug = $opt_debug || 0;
$opt_minrl = $opt_minrl || $min_rl_default;
$opt_unpaired = 1 if $opt_unpaired;

if($opt_debug){
    print "main: linker fa $ref_fa\n";
    print "main: input fq $fastq\n";
    print "main: minrl $opt_minrl\n";
}

# build mer database
load_ref_mers(\%ref_mers, \$ref_fa, \$opt_kmer, \$opt_max_errors, \$opt_debug);

# read and trim fastq
open(my $fq_fh, $fastq) or die $!;

my ($r1head, $r1read, $r1junk, $r1qual, $r2head, $r2read, $r2junk, $r2qual) = '';
my ($trimmed_read1, $trimmed_qual1, $trimmed_read2, $trimmed_qual2) = '';

while($r1head = <$fq_fh>){
  
    $r1read = <$fq_fh>;
    $r1junk = <$fq_fh>;
    $r1qual = <$fq_fh>;
    chomp($r1head, $r1read, $r1junk, $r1qual);

    unless($opt_unpaired) {
        $r2head = <$fq_fh>;
        $r2read = <$fq_fh>;
        $r2junk = <$fq_fh>;
        $r2qual = <$fq_fh>;
        chomp($r2head, $r2read, $r2junk, $r2qual);
    }

    #print "r2 $r2head $r2read $r2junk $r2qual\n" unless $opt_unpaired;
     
    #trim reads as specified
    ($trimmed_read1, $trimmed_qual1) = trim_seq(\$r1read, \$r1qual, \%ref_mers, \$opt_kmer, \$opt_debug); 
    ($trimmed_read2, $trimmed_qual2) = trim_seq(\$r2read, \$r2qual, \%ref_mers, \$opt_kmer, \$opt_debug) unless $opt_unpaired;

    if(!$opt_unpaired) {
    #print both pairs if they meet criteria, else don't print
        if(length($trimmed_read1) >= $opt_minrl && length($trimmed_read2) >= $opt_minrl){
        #both trimmed reads longer than or equal to min rl. print
            print $r1head . "\n" . $trimmed_read1 . "\n" . $r1junk . "\n" . $trimmed_qual1 . "\n";
            print $r2head . "\n" . $trimmed_read2 . "\n" . $r2junk . "\n" . $trimmed_qual2 . "\n";
            if($opt_debug){
                print "main: rl1 " . length($trimmed_read1) . "bp. rl2 " . length($trimmed_read2) . "bp. both reads longer than minrl $opt_minrl. keeping\n";
            }
        } else {
            if($opt_debug){
                print "main: rl1 " . length($trimmed_read1) . "bp. rl2 " . length($trimmed_read2) . "bp. either read shorter than minrl $opt_minrl. skipping\n";
            }
        }

    } else {
        if(length($trimmed_read1) >= $opt_minrl){
            print $r1head . "\n" . $trimmed_read1 . "\n" . $r1junk . "\n" . $trimmed_qual1 . "\n";
        } else {
            if($opt_debug){
                print "main: rl1 " . length($trimmed_read1) . "bp. read shorter than minrl $opt_minrl. skipping\n";
            }
        }
    }
}
close($fq_fh) or die $!;

############################# END MAIN ##################################################

###################################START#################################################
#
# take input reference fasta file and load mers 
# of length 1 to $opt_kmer. All mers that have a hamming distance 
# less than or equal to $opt_max_errors is loaded for each
# reference mer
#
# usage: load_ref_mers( \%ref_mers, \$fasta_path, \$kmer_length, \$max_errors, \$debug)
#           \%ref_mers = reference to hash to load. where key = mer value = 1
#	    \$fasta_path = reference to scalar containing path of fasta file
#           \$kmer_length = reference to scalar containing max kmer length to utilize
#           \$max_errors = reference to scalar containing max errors
#            \$debug = reference to scalar.
#                         value = 1 print debug statements
#                         value = 0 no debug
#

sub load_ref_mers {
    my $ref_mers_ref = $_[0];
    my $fasta_path_ref = $_[1];
    my $kmer_length_ref = $_[2];
    my $max_errors_ref = $_[3];
    my $debug_ref = $_[4];

    open(my $ref_fa_fh, $$fasta_path_ref) or die $!;
    my $header = '';
    my $read = '';

    #parse fasta 
    while ( my $line = <$ref_fa_fh> ) {
        chomp $line;
        if ($line eq '') {
            print "rfrd: Empty line in reference. Skpping.\n" if $$debug_ref;
            next;
        }
        if ($line =~ /^>/) {
            if ($read) {
                #load mers from read
                print "rfrd: $read\n" if $$debug_ref;
                read_to_mers($ref_mers_ref, \$read, $kmer_length_ref, $max_errors_ref, $debug_ref);
            }
            $read = '';
        } else {
            $read .= $line;
        }

    }

    #load last mers from last read
    print "rfrd: $read\n" if $$debug_ref;
    read_to_mers($ref_mers_ref, \$read, $kmer_length_ref, $max_errors_ref, $debug_ref);
   
    if ($$debug_ref) {
      print "printing existing db\n";
      foreach my $x (sort keys %$ref_mers_ref ){
      print "rfdb: $x $$ref_mers_ref{$x}\n";
      }
    } 

    close($ref_fa_fh) or die $!;

}

#######################################END###############################################

###################################START#################################################
# 
# takes read string, breaks it up into mers of defined length, then loads hash
# with mer, and all mers that have a hamming distance less than max defined length
# reverse complements are loaded for all mers
#
# usage: read_to_mers( \%ref_mers, \$read, \$kmer_length, \$max_errors, \$debug)
#           \%ref_mer = reference to mer hash to load mers
#           \$read = reference to scalar containing read strong
#           \$kmer_length = reference to scalar containing kmer length
#           \$max_errors = reference to scalar containing max errors
#           \$debug = reference to scalar.
#                         value = 1 print debug statements
#                         value = 0 no debug
#
sub read_to_mers {
    my $ref_mers_ref = $_[0];
    my $read_ref = $_[1];
    my $kmer_length_ref = $_[2];
    my $max_errors_ref = $_[3];
    my $debug_ref = $_[4];

    # load mers
    my $read_len = length($$read_ref);
    print "rd2m: refseq length $read_len\n" if $$debug_ref;

    foreach my $len (1..$$kmer_length_ref){
        for(my $i = 0; $i < $read_len; $i++ ) {
            my $substring = substr($$read_ref, $i, $len);
            print "rd2m: " if $$debug_ref;
            print ' ' x $i if $$debug_ref;
            print "$substring substr start: $i\n" if $$debug_ref;
            my %temp_mers = permute_mer(\$substring, $max_errors_ref, $debug_ref);
       
            foreach my $temp_mer ( keys %temp_mers ) {
                #only load if it doesn't exist already or the hamming dist is less
                #than an already existing entry
                if (defined($$ref_mers_ref{$temp_mer}) &&
                      $$ref_mers_ref{$temp_mer} <= $temp_mers{$temp_mer} ) {
                    print "rd2m: $temp_mer NOT added to mer db. " if $$debug_ref;
                    print "old error $$ref_mers_ref{$temp_mer}. " if $$debug_ref;
                    print "new error $$ref_mers_ref{$temp_mer}\n" if $$debug_ref;
                } elsif (defined($$ref_mers_ref{$temp_mer}) &&
	              $$ref_mers_ref{$temp_mer} >= $temp_mers{$temp_mer} ) {
                    print "rd2m: $temp_mer YES added to mer db. " if $$debug_ref;
                    print "old error $$ref_mers_ref{$temp_mer}. " if $$debug_ref;
                    $$ref_mers_ref{$temp_mer} = $temp_mers{$temp_mer};
                    print "new error $$ref_mers_ref{$temp_mer}\n" if $$debug_ref;
                } else {
                    $$ref_mers_ref{$temp_mer} = $temp_mers{$temp_mer};
                    print "rd2m: $temp_mer NEW added to mer db. error $$ref_mers_ref{$temp_mer}\n" if $$debug_ref;
                }
            }
        }

        # load reverse complements
        my $read_rev_comp = $$read_ref;
        $read_rev_comp =~ tr/ATGC/TACG/;
        $read_rev_comp = reverse($read_rev_comp);
        print "rd2m: $read_rev_comp read reverse complement\n" if $$debug_ref;
        for(my $i = 0; $i < $read_len; $i++ ) {
            my $substring = substr($read_rev_comp, $i, $len);
            print "rd2m: " if $$debug_ref;
            print ' ' x $i if $$debug_ref;
            print "$substring rc substr start: $i\n" if $$debug_ref;
            my %temp_mers = permute_mer(\$substring, $max_errors_ref, $debug_ref);
 
            foreach my $temp_mer ( keys %temp_mers ) {
                #only load if it doesn't exist already or the hamming dist is less
                #than an already existing entry
                if (defined($$ref_mers_ref{$temp_mer}) &&
                      $$ref_mers_ref{$temp_mer} <= $temp_mers{$temp_mer} ) {
                    print "rd2m: $temp_mer rc NOT added to mer db. " if $$debug_ref;
                    print "old error $$ref_mers_ref{$temp_mer}. " if $$debug_ref;
                    print "new error $$ref_mers_ref{$temp_mer}\n" if $$debug_ref;
                } elsif (defined($$ref_mers_ref{$temp_mer}) &&
                      $$ref_mers_ref{$temp_mer} >= $temp_mers{$temp_mer} ) {
                    print "rd2m: $temp_mer rc YES added to mer db. " if $$debug_ref;
                    print "old error $$ref_mers_ref{$temp_mer}. " if $$debug_ref;
                    $$ref_mers_ref{$temp_mer} = $temp_mers{$temp_mer};
                    print "new error $$ref_mers_ref{$temp_mer}\n" if $$debug_ref;
                } else {
                    $$ref_mers_ref{$temp_mer} = $temp_mers{$temp_mer};
                    print "rd2m: $temp_mer rc NEW added to mer db. error $$ref_mers_ref{$temp_mer}\n" if $$debug_ref;
                }
            }

        }
    }

}

######################################END################################################

#######################################START#############################################
#
# takes mer and returns all mers that are within defined hamming distance
# 
# usage: my %mers = permute_mer(\$mer, \$max_hamming_dist, \$debug);
#            \$mer = reference to scalar containing sequence of mer
#            \$max_hamming_dist = reference to scalar containing max hamming dist
#	     \$debug = reference to scalar. 
#                         value = 1 print debug statements
#                         value = 0 no debug 
#

sub permute_mer {
    my $mer_ref = $_[0];
    my $max_hamming_dist_ref = $_[1];
    my $debug_ref = $_[2];

    my @bases = qw (A T G C);
    my $total = 0;
    my %mismatch_seqs = ($$mer_ref => 0);
    print "perm: $$mer_ref original mer with error 0\n" if $$debug_ref;
      for ( my $j = 1; $j<=$$max_hamming_dist_ref; $j++ ) {
          foreach my $seq ( keys %mismatch_seqs ) {
          my $seq_len = length ($seq);
          my $i = 0;
          
         for (1..$seq_len){
            my @split_bases = split ('', $seq );
         
            foreach my $b ( @bases ) {
                my @working_bases = @split_bases;
                $working_bases[$i] = $b;
                my $join_work = join ('', @working_bases);
                next if defined( $mismatch_seqs{ $join_work } );
                $total++;
                $mismatch_seqs{ $join_work } = $j;
                print "perm: $join_work created mer with error $j\n" if $$debug_ref;
            }

          $i++;
          }
        }
    }
    return %mismatch_seqs;
}

######################################END################################################

#######################################START#############################################
#
# takes read and qual strings and trims linker and any sequence after it 
#
# usage: ($trimmed_read1, $trimmed_qual1) = trim_seq(\$r1read, \$r1qual, \%ref_mers, \$opt_kmer, \$opt_debug);
#          \$r1read, \$r1qual = reference to scalar containing raw read sequence and cooresponding quality
#          \%ref_mers = reference to hash containing linker mers
#          \$opt_kmer = reference to scalar containing length of mer to use for matching
#            \$debug = reference to scalar.
#                         value = 1 print debug statements
#                         value = 0 no debug
#

sub trim_seq {

    my $read_ref = $_[0];
    my $qual_ref = $_[1];
    my $kmers_ref = $_[2];
    my $lookup_length = $_[3];
    my $debug_ref = $_[4];

    my $length = length($$read_ref);
    my $read_mer = '';
    my $qual_mer = '';
    my $trimmed_read = '';
    my $trimmed_qual = '';
    if($$debug_ref) {
        print "trim: $$read_ref read\n";
        print "trim: $$qual_ref qual\n";
    }
    for(my $i = 0; $i < $length; $i++) {
         $read_mer = substr($$read_ref, $i, $$lookup_length);
         $qual_mer = substr($$qual_ref, $i, $$lookup_length);
         if(defined($$kmers_ref{$read_mer})) {
         #matching mer found, trim  
             $trimmed_read = substr($$read_ref, 0, $i);
             $trimmed_qual = substr($$qual_ref, 0, $i);
             if($$debug_ref) {
                 print "trim: ";
                 print ' ' x $i;
                 print "$read_mer matched mer read. index: $i error from db: $$kmers_ref{$read_mer}\n";
                 print "trim: ";
                 print ' ' x $i;
                 print "$qual_mer matched mer qual. index: $i error from db: $$kmers_ref{$read_mer}\n";
                 print "trim: $trimmed_read final trimmed read\n";
                 print "trim: $trimmed_qual final trimmed read\n";
             }
             return($trimmed_read, $trimmed_qual);
             last;
         } 
    }
   
    #no matching mers found in entire db
    if($$debug_ref){
        print "trim: $$read_ref read not trimmed\n";
        print "trim: $$qual_ref qual not trimmed\n";
    }
    return($$read_ref, $$qual_ref);
}

######################################END################################################





