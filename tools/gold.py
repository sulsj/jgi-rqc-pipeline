#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
GOLD:
- Update gold_tax_id and gold_name in library_info (RQC-1009)
- only prints the sql statements, still need to apply them to the db
- part of the rqc_system/sh/daily_update.sh

./gold.py > $S/gold.sql
"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys

import MySQLdb

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from db_access import jgi_connect_db

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program



if __name__ == "__main__":


    its_db = jgi_connect_db("dw")
    sth_its = its_db.cursor()


    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)


    gold = {}

    sql = "select lib_name, tax_gold_ncbi_tax_id, tax_gold_ncbi_tax_name from rqc_genealogy where tax_gold_ncbi_tax_id > 0"
    sth_its.execute(sql)
    for rs in sth_its:
        gold[rs[0]] = { "tax_id" : rs[1], "name" : rs[2] }

    sys.stderr.write("gold: %s records\n" % len(gold))

    # using seq_prod_name instead of actual_seq_prod_name because if missing from genealogy it won't have the actual_seq_prod_name record
    sql = """
select l.library_id, l.library_name, l.gold_tax_id
from library_info l
where
l.dt_created > date_sub(now(), interval 6 month)
    """
    #sql += " and s.run_id = 11273"
    #sql += " and s.run_id = 11710"
    #sql += " limit 0,500"


    update_cnt = 0
    cnt = 0
    sth.execute(sql)
    row_cnt = int(sth.rowcount)
    print "-- checking %s rows " % row_cnt
    for _ in range(row_cnt):
        rs = sth.fetchone()

        cnt += 1

        gold_tax_name = ""
        gold_tax_id = 0
        if rs['library_name'] in gold:
            gold_tax_id = gold[rs['library_name']]['tax_id']
            gold_tax_name = gold[rs['library_name']]['name']

        if gold_tax_name == "None":
            gold_tax_name = ""



        if gold_tax_id > 0 and gold_tax_id != rs['gold_tax_id']:
            #print gold_tax_id, gold_tax_name, rs['library_name']
            if gold_tax_name:
                gold_tax_name = gold_tax_name.replace("'", "\\'")
            sql = "update library_info set gold_tax_id = %s" % (gold_tax_id)
            if gold_tax_name:
                sql += ", gold_organism_name = '%s'" % gold_tax_name
            sql += ", tax_kingdom = null where library_id = %s;" % (rs['library_id'])
            print sql
            update_cnt += 1

        # taxonomy - if gold_tax_id != ncbi_tax_id
        if cnt % 100 == 0:
            sys.stderr.write("lib: %s, updates: %s\n" % (rs['library_name'], update_cnt))

    db.close()
    #its_db.close()

    sys.exit(0)
'''


'''
