#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
$ ./jat_check.py -f (path-to-file)
RQC-904

Tool to check metadata.json submissions
- does it have: library_name, sequencing_project_id, ap_id, at_id
- check "inputs" if file exists (or in jamo)
- check for blank/0 metadata fields (0 = warning, not an error) - Auto-QC meeting 2017-03-07 we agreed
- warns if ISO-* or UTF* file types

To do:
- look up target analysis type id from ap_tool using the at_id in the template?

Possible upgrade:
- check if labels match the template:
--- need to know what the template is we are checking against
--- need to be able to access the template files and read them to know what to expect
--- if a label/file is missing JAT should complain, this seems like duplicating work

if files are missing or keys are missing JAT will complain.
Can use jat validate template () to test if template is okay.

sag raw, screened
./jat_check.py -f /global/projectb/scratch/brycef/sag/tests/BAGOX/raw.metadata.json -t 47
./jat_check.py -f /global/projectb/scratch/brycef/sag/tests/BAGOX/screen.metadata.json -t 39
./jat_check.py -f /global/projectb/scratch/brycef/sag/tests/BAGOX/raw-bad.metadata.json -t 47

# no raw ap/at setup:
./jat_check.py -f /global/dna/shared/rqc/pipelines/sag_pool/archive/02/94/26/87/raw.metadata.json -t 47
# screened ap/at works
./jat_check.py -f /global/dna/shared/rqc/pipelines/sag_pool/archive/02/94/26/87/screen.metadata.json -t 39

iso
./jat_check.py -f /global/dna/shared/rqc/pipelines/microbe_iso/archive/02/93/98/33/iso.metadata.json -t 47

itags
./jat_check.py -f /global/dna/shared/rqc/pipelines/itag2/archive/00/00/27/39/jamo/metadata.json -t 58
58 = its2

filter
./jat_check.py -f /global/dna/shared/rqc/pipelines/filter/archive/02/93/82/77/metadata.json -t 72
# BOTWC - itag library - fails spid check - probably won't be checking itags
./jat_check.py -f $RQC_SHARED/pipelines/filter/archive/02/93/87/79/metadata.json -t 72
- fails APAT, needs to use -l

./jat_check.py -f /global/dna/shared/rqc/pipelines/microbe_iso/archive/02/94/52/75/iso.metadata.json -t 47
- fails ap test

ap test fail:
./jat_check.py -f /global/dna/shared/rqc/pipelines/sps/archive/02/93/82/44/sps.metadata.json -t 47

 ./jat_check.py -f $RQC_SHARED/pipelines/microbe_iso/archive/02/95/08/08/iso.metadata.json -t 47


RQC-1065
jat_check.py -f $RQC_SHARED/pipelines/metagenome/archive/02/97/23/46/metadata.json -t 47
jat_check.py -f $RQC_SHARED/pipelines/metagenome/archive/02/97/23/46/metadata.json -t 47 -p 72



1.0 - 2017-01-25
- initial version

1.1 - 2017-02-09
- checks all fields for 0's or "" values (check_outputs)
- checks if json file is valid json
- checks if file size > 0

1.2 - 2017-03-09
- check if JAT template is released using AP/AT combo
- show metadata mod time

1.2.1 - 2017-03-29
- need to check only Analysis Tasks that are not complete (9) RQCSUPPORT-1172

1.2.2 - 2017-05-11
- bug fix, if ap_id or at_id in metadata.json is an int jat_check didn't find it

1.2.3 - 2017-06-01
- bug fix: linking wrong ap/at to project reporting a failure, ap/at in "Abandoned" state (RQCSUPPORT-1359)
- return latest library name in check_metadata_lib (limit 0,1)

1.2.4 - 2017-06-20
- bug fix: no seq units in PBMID releases, does have inputs but those aren't in RQC (RQCSUPPORT-1468)

1.2.5 - 2017-11-27
- changed to jgi_connect_db

1.2.6 - 2017-11-30
- fixed to work on Denovo/Cori with conda

1.2.7 - 2017-12-06
- added parameter to use target analysis project id (-p)  RQC-1065

1.2.8 - 2018-05-08
- use ap_tool from conda bin path if it exists

SEQQC-11932
./jat_check.py -f $RQC_SHARED/pipelines/sps/archive/02/98/15/75/232152.sps.metadata.json -t 47 -p 78
- worked as expected

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import os
import re
import sys
from argparse import ArgumentParser
import json
import MySQLdb
import time

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from common import human_size, run_cmd, get_colors, get_msg_settings
from db_access import jgi_connect_db

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Check that the files exist in the output section
'''
def file_check(my_json):
    err_cnt = 0
    print "FILE CHECK"
    if 'outputs' in my_json:

        #print json.dumps(my_json['outputs'], indent=4, sort_keys=True)
        for f in my_json['outputs']:

            my_file = f['file']
            file_status = msg_fail
            file_size = 0
            file_type = ""

            if os.path.isfile(my_file):
                file_status = msg_ok
                file_size = os.path.getsize(my_file)
                cmd = "file -b %s" % my_file
                # -b = brief
                std_out, std_err, exit_code = run_cmd(cmd)

                file_type = std_out.strip().split(",")[0]
                # gzip compressed data, was "10547.1.164947.TACGCTG-TACTCCT.", from Unix, last modified: Sat May 28 22:44:27 2016
                # - TMI - just returned first section

            else:
                if os.path.isfile(os.path.join(output_path, my_file)):
                    file_status = msg_ok
                    file_size = os.path.getsize(os.path.join(output_path, my_file))
                    cmd = "file -b %s" % os.path.join(output_path, my_file)
                    std_out, std_err, exit_code = run_cmd(cmd)

                    file_type = std_out.strip().split(",")[0]


                else:
                    err_cnt += 1

            if file_size == 0:
                err_cnt += 1

            if file_type.startswith("UTF") or file_type.startswith("ISO-"):
                file_status = msg_warn

            print "- %s: %s (%s), %s  %s" % (f['label'], f['file'], human_size(file_size), file_type, file_status)



    else:
        print "* Error: no outputs section found in json file!  %s" % msg_fail
        err_cnt += 1

    print

    return err_cnt

'''
Get libraries, spids, ap & ats from metadata.json
'''
def get_metadata(my_json):

    print "METADATA IN JSON FILE"

    lib_list = []
    spid_list = []
    ap_list = []
    at_list = []
    seq_unit_list = []
    itag_primer_set_id = 0


    err_cnt = 0
    if 'metadata' in my_json:

        for mtype in ['library_name', 'sequencing_project_id', 'analysis_project_id', 'analysis_task_id', 'seq_unit_name']:
            tmp_list = []
            if mtype in my_json['metadata']:

                if type(my_json['metadata'][mtype]) == list:
                    tmp_list = my_json['metadata'][mtype]
                    # convert vals to strings
                    c = 0
                    for i in tmp_list:
                        tmp_list[c] = str(i)
                        c += 1

                elif type(my_json['metadata'][mtype]) == str:
                    metadata_list = clean_metadata(my_json['metadata'][mtype])


                    for i in metadata_list.split(","):
                        tmp_list.append(str(i))
                elif type(my_json['metadata'][mtype]) == int:
                    #metadata_list = clean_metadata()
                    tmp_list.append(str(my_json['metadata'][mtype]))

            #print tmp_list
            print "- %s: %s" % (mtype, ", ".join(tmp_list))

            if mtype == "library_name":
                lib_list = tmp_list
            if mtype == "sequencing_project_id":
                spid_list = tmp_list
            if mtype == "analysis_project_id":
                ap_list = tmp_list
            if mtype == "analysis_task_id":
                at_list = tmp_list
            if mtype == "seq_unit_name":
                seq_unit_list = tmp_list

        if 'itag_primer_set_id' in my_json['metadata']:
            itag_primer_set_id = my_json['metadata']['itag_primer_set_id']
            itag_primer_set = my_json['metadata']['itag_primer_set']

            print "- itag_primer_set: %s (id = %s)" % (itag_primer_set, itag_primer_set_id)
    else:
        print "* Error: no metadata section found in json file!  %s" % msg_fail
        err_cnt += 1

    print


    return ap_list, at_list, lib_list, spid_list, seq_unit_list, itag_primer_set_id

'''
Return cleaned metadata string
'''
def clean_metadata(my_data):

    new_data = re.sub(r'[^A-Za-z0-9]+', ',', my_data)
    return new_data


'''
Look for blank metadata fields
- runs recusively
"n_scaffolds",  <== invalid json
- not sure checking for 0's is cause for errors

'''
def check_outputs(my_thing, my_key = ""):

    if my_key == "":
        print "CHECKING FOR BLANK VALUES IN METADATA"

    err_cnt = 0

    # crawl through looking for metadata fields
    #print "---"

    if type(my_thing) == list:
        if len(my_thing) == 1:
            my_thing = my_thing[0]

    if type(my_thing) == dict:
        for x in my_thing:
            #print " * x = %s" % x
            err_cnt += check_outputs(my_thing[x], x)

    elif type(my_thing) == list:
        for x in my_thing:
            err_cnt += check_outputs(x, my_thing)

    else:
        my_thing = str(my_thing)
        if my_thing != "":

            # sometimes 0.0 is okay ... scaf_pct_gt50k
            # convert to a float and check if its really 0
            # might affect "0.0.0" = 0 also
            if my_thing.replace("-","").replace(".","").isdigit():
                try:
                    my_float = float(my_thing.replace("-","").replace(".",""))
                except:
                    my_float = -1

                if my_float == 0:
                    print "- key '%s' is 0  %s" % (my_key, msg_warn)
                    # warning, not error?
                    #err_cnt += 1

        else:
            print "- key '%s' is blank  %s" % (my_key, msg_fail)
            err_cnt += 1

    return err_cnt


'''
Check if the librarys match the expected ones from the spid list

lib_list: ABCD, EFGH
my_list: ABCD, IJKL
* need to go through lib list to see if each lib is in my_list
'''
def check_metadata_lib(spid_list, lib_list, itag_primer_set_id):
    err_cnt = 0
    print "LOOK UP LIBRARY FOR SPID: %s" % ",".join(spid_list)

    lib_check_list = [] # list of libs found from query

    for spid in spid_list:
        sql = "select library_name from library_info where seq_proj_id = %s"

        # check qc state, included seq_unit_id, seq_unit_name for debugging, not in use ... yet
        sql2 = """
select
    qc.qc_state,
    s.seq_unit_id,
    s.seq_unit_name,
    l.library_name
from library_info l
inner join seq_units s on l.library_id = s.rqc_library_id
left join seq_unit_qc qc on s.seq_unit_name = qc.seq_unit_name
where
    l.seq_proj_id = %s
        """
        my_val = (spid,)
        if itag_primer_set_id > 0:
            sql += " and l.itag_primer_set_id = %s"
            my_val = (spid, itag_primer_set_id)

        sql += " order by library_id desc limit 0,1"

        sth.execute(sql, my_val)
        row_cnt = int(sth.rowcount)
        for _ in range(row_cnt):
            rs = sth.fetchone()
            #if rs['qc_state'] == 0:
            #    continue

            lib_check_list.append(rs['library_name'])

            # is this library in the list?
            my_status = msg_fail
            if rs['library_name'] in lib_list:
                my_status = msg_ok
            else:
                err_cnt += 1

            print "- SPID: %s, Library: %s   %s" % (spid, rs['library_name'], my_status)



    # check for libraries not expected in the list
    for lib in lib_list:
        my_status = msg_fail
        if lib in lib_check_list:
            my_status = msg_ok
        else:
            err_cnt += 1

        if my_status == msg_fail:
            print "- Extra Library: %s   %s" % (lib, my_status)
            # library is in our list but did not return from the query



    print

    return err_cnt


'''
Check if the correct seq units are listed for the libraries
'''
def check_metadata_seq_units(lib_list, seq_unit_list):

    err_cnt = 0
    print "LOOK UP SEQ UNITS FOR LIBRARY: %s" % ",".join(lib_list)
    lib_check_list = [] # list of seq_units from the db query that might not be the same as seq_unit_list

    if len(seq_unit_list) == 0:
        # happens for PBMID
        print " - no seq units to check  %s" % (msg_warn)
        print
        return err_cnt

    for seq_unit in seq_unit_list:

        sql = """
select s.library_name, sf.file_type
from seq_unit_file sf
inner join seq_units s on sf.seq_unit_id = s.seq_unit_id
where
    sf.file_name = %s
        """

        sth.execute(sql, (seq_unit,))
        row_cnt = int(sth.rowcount)
        for _ in range(row_cnt):
            rs = sth.fetchone()

            lib_check_list.append(rs['library_name'])

            my_status = msg_fail
            if rs['library_name'] in lib_list:
                my_status = msg_ok
            else:
                err_cnt += 1

            print "- Seq Unit: %s, Library: %s   %s" % (seq_unit, rs['library_name'], my_status)

    # shouldn't have any librarys in lib_check_list that are extra ...
    for lib in lib_list:
        my_status = msg_fail
        if lib in lib_check_list:
            my_status = msg_ok
        else:
            err_cnt += 1

        if my_status == msg_fail:
            print "- Extra Library: %s   %s" % (lib, my_status)
            # library is in our list but did not return from the query

    print

    return err_cnt


'''

'''
def check_metadata_product_type(spid_list):

    err_cnt = 0
    sp_list = []  # list of product names, should be only 1
    print "LOOK UP SEQUENCING PRODUCT TYPE FOR SPID: %s" % ",".join(spid_list)

    for spid in spid_list:
        sql = "select seq_prod_name, actual_seq_prod_name from library_info where seq_proj_id = %s"
        sth.execute(sql, (spid,))
        row_cnt = int(sth.rowcount)
        for _ in range(row_cnt):
            rs = sth.fetchone()

            sp_name = rs['actual_seq_prod_name']
            if not sp_name:
                sp_name = rs['seq_prod_name']
            sp_list.append(sp_name)

    sp_list = list(set(sp_list))
    print "- Seq Prod Name: %s" % ",".join(sp_list)
    if len(sp_list) > 1:
        print "- Error: more than one sequencing product name!  %s" % msg_fail
        err_cnt += 1

    print

    return err_cnt


'''
Check if the AP/AT listed is correct based on the spid and target_at_id
- returns 0 in template if AP or AT is in a terminal state, we pull it even if its complete
- only check for 1 ap, 1 at?
- target_ap_id optional, needed for some metagenomes (CBOPP)
'''
def check_metadata_apat(lib_list, ap_list, at_list, target_at_id, target_ap_id):
    err_cnt = 0
    print "LOOK UP ANALYSIS PRODUCT ID AND ANALYSIS TASK ID FOR LIBRARY: %s" % ",".join(lib_list)

    ap_cmd = os.path.join(my_path, "ap_tool.py")
    
    if not os.path.isfile(ap_cmd):
        ap_cmd = "[pipeline_path]/tools/ap_tool.py"
        ap_cmd = ap_cmd.replace("[pipeline_path]", os.path.realpath(os.path.join(my_path, "..")))

    if not os.path.isfile(ap_cmd):
        print "Error: cannot find %s  %s" % (ap_cmd, msg_fail)
        sys.exit(1)

    my_ap_list = []
    my_at_list = []
    for lib in lib_list:
        cmd = "%s -l %s -tatid %s" % (ap_cmd, lib, target_at_id) 
        if target_ap_id > 0:
            cmd += " -tapid %s" % target_ap_id
        cmd += " -sa -m psv"
            
        # -sa or not -sa?
        print cmd
        # -sa = show all - even completed projects, -m psv = pipe separated values
        std_out, std_err, exit_code = run_cmd(cmd)


        for line in std_out.split("\n"):

            if line.lower().startswith("seq proj id"):
                continue
            if not line:
                continue

            arr = line.strip().split("|")
            #print arr
            ap_id = arr[4]
            at_id = arr[8]
            at_status_id = int(arr[10])

            if at_status_id != 9 and at_status_id != 10: # 9 == done, 10 == abandoned

                my_ap_list.append(ap_id)
                my_at_list.append(at_id)


                my_ap_status = msg_fail
                my_at_status = msg_fail
                #print ap_id, ap_list
                #print at_id, at_list

                if ap_id in ap_list:
                    my_ap_status = msg_ok
                else:
                    err_cnt += 1

                if at_id in at_list:
                    my_at_status = msg_ok
                else:

                    err_cnt += 1

                #if ap_id in ap_list and at_id in at_list:
                #    my_status = msg_ok
                #else:
                #    err_cnt += 1

                print "- LIBRARY: %s == AP: %s  %s, AT: %s  %s" % (lib, ap_id, my_ap_status, at_id, my_at_status)


    # look for unexpected AP or ATs in the metadata.json
    for ap in ap_list:
        my_status = msg_fail
        if ap in my_ap_list:
            my_status = msg_ok
        else:
            err_cnt += 1
        if my_status == msg_fail:
            print "- Extra AP id: %s  %s" % (ap, my_status)

    for at in at_list:
        my_status = msg_fail
        if at in my_at_list:
            my_status = msg_ok
        else:
            err_cnt += 1
        if my_status == msg_fail:
            print "- Extra AT id: %s  %s" % (at, my_status)



    print

    return err_cnt

'''
Look if this AP/AT combo has been released previously
- need to test actual list project ... ?
'''
def check_jat_release(ap_list, at_list):
    err_cnt = 0
    print "CHECK IF RELEASED IN JAT for AP: %s, AT: %s" % (ap_list, at_list)

    # check if AP ids & AT ids > 0
    ap_sum = 0
    for ap in ap_list:
        ap_sum += int(ap)
    at_sum = 0
    for at in at_list:
        at_sum += int(at)

    if ap_sum == 0 or at_sum == 0:
        print "- ERROR: AP ids or AT ids are 0!  %s" % msg_fail
        print
        err_cnt += 1
        return err_cnt



    mongo_cmd = "select metadata.jat_label, metadata.template_name, metadata.jat_key where metadata.analysis_project_id = %s and metadata.analysis_task_id = %s"
    mongo_cmd = mongo_cmd % (ap_list, at_list)

    cmd = "#jamo;jamo report %s as json" % mongo_cmd
    print cmd
    std_out, std_err, exit_code = run_cmd(cmd)

    if exit_code == 0:
        jat_key_dict = {}

        jat_json = json.loads(std_out)
        #print jat_json
        for j in jat_json:
            #print j
            if 'metadata.jat_key' in j:
                if j['metadata.jat_key'] in jat_key_dict:
                    jat_key_dict[j['metadata.jat_key']]['cnt'] += 1
                    jat_key_dict[j['metadata.jat_key']]['template'] += ",%s" % j['metadata.template_name']
                else:
                    jat_key_dict[j['metadata.jat_key']] = { "cnt" : 1, "template" : j['metadata.template_name'] }
        #print jat_key_dict
        if len(jat_key_dict) > 0:

            print "- JAT release FOUND for AP: %s, AT: %s  %s" % (ap_list, at_list, msg_fail)
            err_cnt += 1

            for j in jat_key_dict:
                try:
                    template = ", ".join(list(set(jat_key_dict[j]['template'].split(","))))
                except:
                    template = jat_key_dict[j]['template']

                #template = "x" # j['template'].split(",")
                print "--- JAT release: %s, template: %s, files: %s" % (j, template, jat_key_dict[j]['cnt'])

        else:
            print "- No previous JAT release for AP: %s, AT: %s  %s" % (ap_list, at_list, msg_ok)
    else:
        print "Error: JAMO failed!  %s" % msg_fail
        err_cnt += 1

    print


    return err_cnt


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__


if __name__ == "__main__":


    my_name = "JAT Metadata.json Checker"
    version = "1.2.8"

    # Parse options

    usage = "%s, version %s\n" % (my_name, version)

    usage += """
Jat metadata.json validator
- checks that AP/AT, SPID, Library name all match

    """



    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)

    parser = ArgumentParser(usage = usage)

    parser.add_argument("-f", "--file", dest="metadata_file", type=str, help = "path to metadata.json")
    parser.add_argument("-t", "--tatid", dest="tatid", type=int, help = "target analysis task type id")
    parser.add_argument("-p", "--tapid", dest="tapid", type=int, help = "target analysis project type id (optional)")
    parser.add_argument("-o", "--output-path", dest="output_path", type=str, help = "output path (if different than metadata.json, optional)")
    parser.add_argument("-v", "--version", action="version", version=version)

    metadata_file = None
    output_path = None
    tat_id = 47 # target analysis type id, default = 47 = assembly
    tap_id = 0 # optional
    
    args = parser.parse_args()

    if args.metadata_file:
        metadata_file = args.metadata_file
    if args.output_path:
        output_path = args.output_path
    else:
        output_path = os.path.dirname(metadata_file)

    if args.tatid:
        tat_id = args.tatid

    if args.tapid:
        tap_id = args.tapid

    #print output_path
    #print metadata_file

    if not os.path.isfile(metadata_file):
        print "* Error: cannot find file %s  %s" % (metadata_file, msg_fail)
        sys.exit(2)

    if tat_id == 0:
        print "* Error: need target analysis type id!  %s" % msg_fail



    #print metadata_json
    #print json.dumps(metadata_json, indent=4, sort_keys=True)
    print "-" * 80
    print "%s v %s" % (my_name, version)
    print "- Checking: %s" % (metadata_file)

    # get file date - last modified
    mtime = time.ctime(os.path.getmtime(metadata_file))
    print "- last modified: %s" % mtime


    metadata_json = {}
    my_err_cnt = 0

    fh = open(metadata_file, "r")

    try:
        metadata_json = json.load(fh)
    except:
        print "- %s is not valid json!  %s" % (metadata_file, msg_fail)
        my_err_cnt += 1

    fh.close()

    print

    if my_err_cnt == 0:
        ap_list, at_list, lib_list, spid_list, seq_unit_list, itag_primer_set_id = get_metadata(metadata_json)

        my_err_cnt += file_check(metadata_json)

        db = jgi_connect_db("rqc")
        if not db:
            print "Error: cannot connect to RQC's database  %s" % msg_fail
            sys.exit(4)
        sth = db.cursor(MySQLdb.cursors.DictCursor)

        my_err_cnt += check_metadata_lib(spid_list, lib_list, itag_primer_set_id)

        #metadata_json = { "a" : 1, "b" : 2, "c" : 0, "d" : "", "e" : "blackhole sun", "f" : ["a", "b", "", "d", 5] }
        my_err_cnt += check_outputs(metadata_json)
        print


        my_err_cnt += check_metadata_seq_units(lib_list, seq_unit_list)
        my_err_cnt += check_metadata_product_type(spid_list)
        my_err_cnt += check_metadata_apat(lib_list, ap_list, at_list, tat_id, tap_id)
        my_err_cnt += check_jat_release(ap_list, at_list)

        db.close()

    if my_err_cnt > 0:
        if my_err_cnt == 1:
            print "* Found %s error    %s" % (my_err_cnt, msg_fail)
        else:
            print "* Found %s errors   %s" % (my_err_cnt, msg_fail)
        sys.exit(3)
    else:
        print "* Passed metadata tests  %s" % msg_ok
        sys.exit(0)

    sys.exit(0)



"""

                      .-.                 My life's on time but again my sense is late.
         .-._    _.../   `,    _.-.       Feel a might unsteady but I still have to play.
         |   `'-'    \     \_'`   |       Six to one's the odds and we have the highest stakes.
         \            '.__,/ `\_.--,      And once again I gamble with my very life today.
          /                '._/     |     Highly polished metal, the oil makes it gleam.
         /                    '.    /     Fill the terror chamber, your mind begins to scream.
        ;   _                  _'--;      Your life is like a trigger, never troubled till you're squeezed.
     '--|- (_)       __       (_) -|--'   You crack a smile as you give the gun a squeeze.
     .--|-          (__)          -|--.
      .-\-                        -/-.                 Megadeth - My Last Words
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`




"""