#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Sketch reporter
- creates graphviz'd report based on sketch hits

dependencies
graphviz
bbtools
taxonomy server (bbtools)

1.0 - 2017-01-23
- initial version

1.0.1 - 2017-02-23
- minor fix for output_path for sketches
- color nodes green for max hits
- exit code = 4 = no sketch produced

1.0.2 - 2017-02-28
- use taxonomy web service for nt

1.0.3 - 2017-03-02
- minor bug fix for using NT

1.0.4 - 2017-04-07
- added silva, changed nt url
- options for sketch: minsize, mode, minhits (RQC-941)
- always overwrite older sketches

1.0.5 - 2017-06-15
- new comparesketch output format
- added refseq (curl), img databases
- new top tax picker

1.0.6 - 2017-06-21
- Brian changed output format again
- don't need to use servers, bbtools handles it automatically
- use sendsketch instead of comparesketch

1.0.7 - 2017-10-03
- dot file update
- code cleanup with pylint

1.0.8 - 2017-11-30
- updated to work on denovo

Sketch:
- to use someone else's sketches, you need the same hash function for the sketch db and sketech'd file
- BBsketch uses the smallest hash possible
- hash created by using 31kmer (64 bits) & 0xFF to get last 8 bits - then look up the 8 bit number in hash tables, then shift & look up again
--- hash tables created randomly


for using fastqs: (old)
$ reformat.sh in=11246.1.197565.fastq.gz out=11246.1.197565.subsample.fastq.gz samplereadstarget=500000
1000000 = 2m reads..., 500,000 = 1m reads
$ kmercountexact.sh in=11246.1.197565.subsample.fastq.gz sketch=11246.1.197565.sketch1 mincount=3
- slow/fast on subsampled fq
$ comparesketch.sh in=11246.1.197565.sketch /global/projectb/sandbox/gaag/bbtools/nt/*sketch* > 11246.1.197565.nt.fastq.txt

Subsample option (/2) - assumed paired read

ln -s $SDM_FASTQ/01/09/76/10976.3.186514.fastq.gz .
tadpole.sh in=10976.3.186514.fastq.gz out=10976.3.186514.ec.fastq.gz mode=correct pigz unpigz ow=t
tadpole.sh shave=t rinse=t ow=t in=10976.3.186514.ec.fastq.gz out=10976.3.186514.ec.fasta
sketch.sh in=10976.3.186514.ec.fasta out=10976.3.186514.ec.fasta.sketch
comparesketch.sh in=10976.3.186514.ec.fasta.sketch /global/projectb/sandbox/gaag/bbtools/nt/*sketch* > 10976.3.186514.ec.nt.fasta.txt
comparesketch.sh in=10976.3.186514.ec.fasta.sketch /global/projectb/sandbox/gaag/bbtools/refseq2/taxa*sketch* > 10976.3.186514.ec-refseq.fasta.txt

- if fastq need to do:
subsample
tadpole correction
tadpole assembly
trim contigs?
fuse contigs?
sketch
compare sketch


./sketch_rpt.py -f /global/projectb/scratch/brycef/sketch/11194.1.195488.ec.fasta
./sketch_rpt.py -f /global/projectb/scratch/brycef/sketch/11194.1.195488.ec.trim.fuse.fasta

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import re
import sys
from argparse import ArgumentParser



# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from common import run_cmd, get_colors, get_msg_settings
from taxonomy import get_taxonomy

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

def do_sketch(fasta, db_name, sketch_mode, sketch_minhits, sketch_size):

    sketch_out = os.path.join(output_path, os.path.basename(fasta) + ".%s.sketch" % (db_name))

    if os.path.isfile(sketch_out) and 1==2: # 1.0.4 change
        print "* skipping sketch step, %s exists  %s" % (sketch_out, msg_warn)
        return sketch_out


    # options to add:
    # minhits, minid, records, format, level, printtax, printoriginalname, samplerate, mode

    exit_code = 0


    sketch_opt = ['color=f'] # ['format=0'] # easier to parse, also how sendsketch.sh works
    if db_name == "img":
        #sketch_opt.append("index=f blacklist=img")
        pass
    if db_name == "silva":
        sketch_opt.append("local")

    if sketch_mode:
        sketch_opt.append("mode=%s" % sketch_mode)
    if sketch_size:
        sketch_opt.append("size=%s" % sketch_size)
    if sketch_minhits:
        sketch_opt.append("minhits=%s" % sketch_minhits)





    exit_code = 4
    if 1==2: # deprecated, BBtools automatically uses the sketch servers

        sketch_options = "" # problems with converting wkid with ctrl chars in it
        if len(sketch_opt) > 0:
            sketch_options = " ".join(sketch_opt) # usually joined by & but this is Brian's way
            sketch_options = "--data '%s'" % sketch_options


        if db_name == "nt":

            # --data ''
            # minhits, minid, records, format, level, printtax, printoriginalname, samplerate, mode
            cmd = "curl -k https://nt-sketch.jgi-psf.org/sketch/file/%s %s > %s" % (fasta, sketch_options, sketch_out)
            print cmd
            std_out, std_err, exit_code = run_cmd(cmd)

        elif db_name in ("silva", "ribo"):

            cmd = "curl -k https://ribo-sketch.jgi-psf.org/sketch/file/%s %s > %s" % (fasta, sketch_options, sketch_out)
            print cmd
            std_out, std_err, exit_code = run_cmd(cmd)

        elif db_name.startswith("refseq"):
            cmd = "curl -k https://refseq-sketch.jgi-psf.org/sketch/file/%s %s > %s" % (fasta, sketch_options, sketch_out)
            print cmd
            std_out, std_err, exit_code = run_cmd(cmd)


        else:
            exit_code = 4


    if exit_code > 0:


        sketch_options = " ".join(sketch_opt)

        #cmd = "comparesketch.sh in=%s %s %s > %s" % (fasta, sketch_options, db, sketch_out)
        cmd = "#bbtools;sendsketch.sh in=%s %s %s > %s" % (fasta, sketch_options, db_name, sketch_out)
        print cmd
        std_out, std_err, exit_code = run_cmd(cmd)
        # mode, minhits, size - to test and include

    if exit_code > 0:
        print "* sendsketch.sh failed!  Exit code: %s" % exit_code
        if std_out:
            print std_out
        if std_err:
            print std_err


        sys.exit(exit_code)


    return sketch_out


def do_plot(sketch_file, file_type, my_title, my_db_name):

    org_list = []
    count_dict = {} # key = organism name, matches, compared
    max_dict = {} # max value for each tax in the_list

    # none of these work too well
    max_key = "wkid" # look at this to determine the max
    #max_key = "match"
    #max_key = "comp"
    #max_key = "cnt"

    if os.path.isfile(sketch_file):
        tax_cnt = 0 # number of taxonomies found
        fh = open(sketch_file, "r")
        for line in fh:
            if line.startswith("Error"):
                print "Unknown Sketch error occurred."
                sys.exit(7)
            if line.startswith("<h1>"):
                print line
                sys.exit(8)



            arr = line.split('\t')
            #print arr

            # ignore header line
            if line.startswith("WKID"):
                continue


            if len(arr) > 10:
                tax_cnt += 1

                tax_id = re.sub(r'[^0-9]', '', str(arr[8]))
                #WKID    KID     ANI     Complt  Contam  Matches Unique  noHit   TaxID   gSize   gSeqs   taxName
                #['WKID', 'KID', 'ANI', 'complt', 'contam', 'matches', 'taxID', 'gSize', 'gSeqs', 'taxName\n']
                #['0.48%', '0.02%', '84.16%', '100.00%', '0.09%', '6', '100272', '9359227', '124316', 'uncultured eukaryote\n']



                #tax_dict = get_taxonomy(tax_id)
                tax_dict = get_taxonomy(tax_id, "ncbi")
                print "%s;%s;%s;%s;%s;%s;%s = %s" % (tax_dict['superkingdom'], tax_dict['phylum'], tax_dict['class'], tax_dict['order'],
                                                     tax_dict['family'], tax_dict['genus'], tax_dict['species'], arr[0])


                # wkid = mateches(2)/compared(3)

                org_list.append(tax_dict['superkingdom'])

                # point to next if not "-"
                the_list = ['phylum', 'class', 'order', 'family', 'genus', 'species']
                last_val = tax_dict['superkingdom']
                for i in the_list:

                    if tax_dict[i] == "-":
                        continue
                    add_graph(last_val, tax_dict[i])
                    last_val = tax_dict[i]

                the_list.append('superkingdom')
                for i in the_list:
                    matches = int(re.sub(r'[^0-9]','',arr[5]))
                    compares = int(re.sub(r'[^0-9]','',arr[5]))
                    #wkid = float(str(arr[0]).replace("%","").replace("WKID ",""))/100.0
                    #print "'%s', '%s'" % (arr[0], int(str(arr[0]).replace("%","")))
                    wkid = float(str(arr[0]).replace("%","").strip())/100.0

                    if tax_dict[i] in count_dict:
                        count_dict[tax_dict[i]]['match'] += matches
                        count_dict[tax_dict[i]]['comp'] += compares
                        count_dict[tax_dict[i]]['wkid'] += wkid
                        count_dict[tax_dict[i]]['cnt'] += 1

                    else:
                        count_dict[tax_dict[i]] = { 'match': matches, "comp" : compares, 'wkid' : wkid, 'cnt' : 1 }

                    if i in max_dict:
                        pass
                        #print "i: %s, tax_dict[i] = %s, wkid = %s, max[%s] = %s" % (i, tax_dict[i], count_dict[tax_dict[i]]['wkid'], i, max_dict[i])

                        #if count_dict[tax_dict[i]][max_key] > max_dict[i]['max']:
                        #    max_dict[i]['max'] = count_dict[tax_dict[i]][max_key]
                        #    max_dict[i]['thing'] = tax_dict[i]

                    else:
                        max_dict[i] = { "max" : count_dict[tax_dict[i]][max_key], "thing" : tax_dict[i] }

                    # the first one is the best match
                    #if len(max_dict) == 0:
                    #    max_dict[i] = { "max" : count_dict[tax_dict[i]]['wkid'], "thing" : tax_dict[i] }

                    #if 'Actinobacteria' in count_dict:
                    #    print "cd: %s" % count_dict['Actinobacteria']['wkid']
        fh.close()

        # get max org for each column
        max_tax = {}
        for tax in max_dict:
            my_key = max_dict[tax]['thing']
            max_tax[my_key] = { "max" : max_dict[tax]['max'], "thing" : tax }
            #max_dict[tax] = max_dict[tax]['thing']
        #print count_dict
        #print max_dict
        #print max_tax
        #sys.exit(44)

        if tax_cnt == 0:
            print "* Sketch found no taxonomies  %s" % msg_warn
            sys.exit(4)

        ## create dot file

        dot_file = sketch_file.replace(".sketch", ".dot")
        out_file = dot_file.replace(".dot", ".%s" % file_type)
        fh = open(dot_file, "w")

        fh.write("digraph sketch {\n") # directed graph
        fh.write("  rankdir=LR;\n")
        fh.write("  node [ shape = box ];\n")
        if sketch_title:
            fh.write("  label = \"BBTools Sketch Taxonomy: %s\";\n" % (my_title))
        else:
            fh.write("  label = \"BBTools Sketch Taxonomy: %s vs %s\";\n" % (sketch_file, my_db_name))

        label_dict = {}
        for i in count_dict:

            x = re.sub(r'[^A-Za-z0-9]+', '_', i)

            wkid = "0.0%"
            if i in count_dict:

                #wkid = "{:.1%}".format(count_dict[xx]['match'] / float(count_dict[xx]['comp']))
                wkid = "{:.1%}".format(count_dict[i]['wkid'])


            max_color = ",color=\"#CCCCCC\",style=filled"
            if i in max_tax:
                max_color = ",color=\"#AAFFAA\",style=filled"

            my_label = " label=<%s<br/><font point-size=\"10\">WKID: %s</font>>%s" % (i, wkid, max_color)

            fh.write("  %s [ %s ];\n" % (x, my_label))
            #label_dict[x] = " label=\"%s\nWKID: %s\"%s" % (i, wkid, max_color)

        #print label_dict



        # http://graphviz.org/Documentation/dotguide.pdf
        done = False
        cnt = 0
        q = list(set(org_list))
        if len(q) == 0:
            done = True


        while not done:
            cnt += 1
            x = q.pop(-1)
            #print cnt, x

            if x in graph:
                # list of what this is linked to

                x1 = re.sub(r'[^A-Za-z0-9]+', '_', x)
                if x1 in label_dict:
                    if label_dict[x1]:
                        fh.write("  %s [%s ];\n" % (x1, label_dict[x1]))
                        label_dict[x1] = ""

                for xx in list(set(graph[x])):
                    #print " - %s" % xx
                    # create ids for the nodes w/o spaces and other stuff

                    xx1 = re.sub(r'[^A-Za-z0-9]+', '_', xx)

                    # -> = arrow, -- = no arrow


                    if xx1 in label_dict:
                        if label_dict[xx1]:
                            fh.write("  %s [%s ];\n" % (xx1, label_dict[xx1]))
                            label_dict[xx1] = ""

                    #fh.write("  %s [ label=\"%s\nWKID: %s\"%s ];\n" % (xx1, xx, wkid, max_color)) # show the non-messed up name, if matches taxonomy set color


                    fh.write("  %s -> %s;\n" % (x1.strip(), xx1.strip()))
                    # a->b [color=blue] - set edge from a to b blue

                    q.append(xx)


            if len(q) == 0:
                done = True

            if cnt > 1000:
                done = True


        fh.write("}\n")
        fh.close()


        # dot -T png t.dot > t.png
        # make image

        cmd = "#graphviz;dot -T %s %s > %s" % (my_type, dot_file, out_file)
        std_out, std_err, exit_code = run_cmd(cmd)
        if exit_code > 0:
            print "* Error creating image with graphviz!  %s" % msg_fail
            print std_err
            sys.exit(exit_code)


        print
        print "Taxonomy file: %s" % out_file

    else:
        print "* Error - sketch file missing: %s  %s" % (sketch_file, msg_fail)


def add_graph(a, b):

    my_key = a

    #Bacteria;Actinobacteria;Actinobacteria;Micrococcales;Micrococcaceae;Kocuria;Kocuria rhizophila
    # skip setting Actinobacteria = Actinobacteria
    if a == b:
        pass
    else:
        if my_key in graph:
            graph[my_key].append(b)

        else:
            graph[my_key] = [b]


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__


if __name__ == "__main__":

    my_name = "Sketch Reporter"
    version = "1.0.8"

    # Parse options

    usage = "%s, version %s\n" % (my_name, version)

    usage += """
Creates an image of the taxonomy based on the sketch

    """

    print "-------------------------> %s: %s" % (my_name, version)


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-f", "--file", dest="file", type=str, help = "file to create sketch'd report")
    parser.add_argument("-t", "--type", dest="type", type=str, help = "output type (default png)")
    parser.add_argument("-ttl", "--title", dest="title", type=str, help = "Title for the plot")
    parser.add_argument("-o", "--output-path", dest="output_path", type=str, help = "output path")
    parser.add_argument("--mode", dest="sketch_mode", type=str, help = "sketch option: mode (single, sequence)")
    parser.add_argument("--minhits", dest="sketch_minhits", type=int, help = "sketch option: minimum number of hits for a subject to return")
    parser.add_argument("--size", dest="sketch_size", type=int, help = "sketch option: size of sketch to generate (10000 default)")
    parser.add_argument("-db", "--db", dest="db", type=str, help = "sketch database to use (nt, refseq.bacteria, silva/ribo, img or path to sketched files)")
    parser.add_argument("-v", "--version", action="version", version=version)

    my_file = None
    my_type = "png"
    output_path = None
    sketch_db_name = "nt"
    sketch_db = None
    search_tax_id = [] # highlight if we find any tax_ids in this list
    sketch_title = None

    # 0 = don't send value
    sketch_size = 0
    sketch_mode = ""
    sketch_minhits = 0

    args = parser.parse_args()

    if args.file:
        my_file = args.file

    if args.title:
        sketch_title = args.title

    if args.output_path:
        output_path = args.output_path
    else:
        output_path = os.path.dirname(my_file)

    if args.sketch_mode:
        sketch_mode = args.sketch_mode
    if args.sketch_minhits:
        sketch_minhits = args.sketch_minhits
    if args.sketch_size:
        sketch_size = args.sketch_size

    if args.type:
        my_type = args.type

    if args.db:
        sketch_db_name = args.db

    if not os.path.isfile(my_file):
        print "* Error: cannot find file %s  %s" % (my_file, msg_fail)
        sys.exit(2)

    if my_file.endswith(".fastq.gz"):
        print "fastqs not supported yet"
        sys.exit(3)

    """
    if sketch_db_name.lower() == "nt":
        sketch_db = "/global/projectb/sandbox/gaag/bbtools/nt/taxa*sketch*"
        # nt-sketch
    elif sketch_db_name.lower() in ("refseq", "refseq.bacteria"):
        sketch_db = "/global/projectb/sandbox/gaag/bbtools/refseq2/taxa*sketch*"
        # refseq-sketch
    elif sketch_db_name.lower() == "img":
        sketch_db = "/global/projectb/sandbox/gaag/bbtools/tax/img/img*.sketch"
        # not online
    else:
        sketch_db = sketch_db_name
    """

    my_file = os.path.realpath(my_file)


    graph = {} # dictionary for pointing nodes to other nodes ...

    sketch_file = do_sketch(my_file, sketch_db_name, sketch_mode, sketch_minhits, sketch_size)
    do_plot(sketch_file, my_type, sketch_title, sketch_db_name)

    sys.exit(0)



"""

                      .-.                 My life's on time but again my sense is late.
         .-._    _.../   `,    _.-.       Feel a might unsteady but I still have to play.
         |   `'-'    \     \_'`   |       Six to one's the odds and we have the highest stakes.
         \            '.__,/ `\_.--,      And once again I gamble with my very life today.
          /                '._/     |     Highly polished metal, the oil makes it gleam.
         /                    '.    /     Fill the terror chamber, your mind begins to scream.
        ;   _                  _'--;      Your life is like a trigger, never troubled till you're squeezed.
     '--|- (_)       __       (_) -|--'   You crack a smile as you give the gun a squeeze.
     .--|-          (__)          -|--.
      .-\-                        -/-.    Megadeth - My Last Words
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`


"""