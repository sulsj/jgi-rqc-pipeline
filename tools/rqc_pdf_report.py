#!/usr/bin/env python

'''
Print a PDF report given a seq unit name.
Prints the pdf file to an output directory with selected statistics and images.

*** DEPRECATED 2017-07-06 ***
USE ../report/rqc_report.py instead

To run first do:
export PYTHONPATH=/global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/assemblyqc/lib/:/global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/readqc/lib/:/global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/lib/:/global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/tools/:/global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc/
./tools/rqc_pdf_report.py -f 7675.5.80300.ACTTGA.fastq.gz -o /global/projectb/scratch/brycef -r 2.2.1 
./tools/rqc_pdf_report.py -s 2818555 -o /global/projectb/scratch/brycef -r 2.2.1 

For fast testing:
As andreopo@gpint108 go to:  /global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/tools/test/test_rqc_pdf_report/
cd /global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/tools/test/test_rqc_pdf_report/
check run.sh:
nosetests -w /global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/tools/test/test_rqc_pdf_report


Author: Bill Andreopoulos
Initial release: 2014/01/14
Date modified: October 8, 2014

TODO get data from ITS instead of RQC library? HOLD - Bryce
 URL for library info:
 $ curl http://geneusprod.jgi-psf.org:8180/pluss/sow-segments/8675/sow-segment-metadata
 ** 8675 is the seq_units.segment_id value
 code in jgi-rqc: rqc_system/bin/rqc_register.py: lines 844 - 891

v1.1 - 2017-02-21
- fix for no insert size (seq unit id 2941012 - small RNA)



'''

import MySQLdb

import os
import sys
import logging

import argparse
from collections import defaultdict
from subprocess import Popen, call, PIPE
from multiprocessing import Process, Queue
import operator
import re
import shutil

import time
import datetime

dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from db_access import jgi_connect_db
from common import get_logger, run_command, get_run_path, post_mortem_cmd
from os_utility import run_sh_command





'''
Get the preamble of the latex file, which will define the styles, fonts, etc.
'''
def getLatexPreamble():
    doc = '\\usepackage{fullpage} ' \
        '\\usepackage{url} ' \
        '\\usepackage{graphicx} ' \
        '\\usepackage{caption} ' \
        '\\usepackage{sectsty} ' \
        '\\usepackage{longtable} ' \
        '\\usepackage{multirow} ' \
        '\\usepackage{hyperref} ' \
        '\\hypersetup{ ' \
        '    colorlinks=false, ' \
        '    pdfborder={0 0 0}, ' \
        '}' \
        '\\usepackage{color} ' \
        '\\usepackage{pdfpages} ' \
        '\\usepackage{fontenc} ' \
        '\\usepackage{mathtools} ' \
        '\\usepackage{enumerate} ' \
        '\\usepackage{textcomp}' \
        '\\usepackage{array}' \
        '\\usepackage[absolute]{textpos}' \
        '\\newcolumntype{C}[1]{>{\\centering\\arraybackslash}p{#1}}' \
        '\\definecolor{sectionColor}{rgb}{0,0.41,0.55} ' \
        '\\definecolor{lineColor}{rgb}{0.85,0.85,0.85} % gainsboro ' \
        '\\renewcommand{\\rmdefault}{ptm}  % sets font to Adobe Times ' \
        '\\renewcommand{\\thesection}{\\color{sectionColor}\\arabic{section}.} % sets section numbering to alphanumeric ' \
        '\\newcommand{\\rr}{\\raggedright} % used to left justify table text ' \
        '\\newcommand{\\rl}{\\raggedleft}  % used to right justify table text ' \
        '\\newcommand{\\tn}{\\tabularnewline} % used for table text justification ' \
        '\\setlength{\\parindent}{0pt}     % turns off paragraph auto indent ' \
        '\\sectionfont{\\large\\bf\\color{sectionColor}\\usefont{OT1}{phv}{b}{n}\\selectfont} % sets the font size for the section ' \
        '\\newcommand{\\drawline}{ ' \
        '\\begin{center} ' \
        '\\color{lineColor} ' \
        '\\rule{15cm}{0.35mm} ' \
        '\\end{center} ' \
        '} ' \
        '\\newenvironment{changemargin}[2]{% ' \
        '\\begin{list}{}{% ' \
        '\\setlength{\\topsep}{0pt}% ' \
        '\\setlength{\\leftmargin}{#1}% ' \
        '\\setlength{\\rightmargin}{#2}% ' \
        '\\setlength{\\listparindent}{\\parindent}% ' \
        '\\setlength{\\itemindent}{\\parindent}% ' \
        '\\setlength{\\parsep}{\\parskip}% ' \
        '}% ' \
        '\\item[]}{\\end{list}} ' \
        '%------------------------------- ' \
        '%   General parameters, for ALL pages: ' \
        '%\\renewcommand{\\topfraction}{0.9}	% max fraction of floats at top ' \
        '%\\renewcommand{\\bottomfraction}{0.8}	% max fraction of floats at bottom ' \
        '%   Parameters for TEXT pages (not float pages): ' \
        '%\\setcounter{topnumber}{2} ' \
        '%\\setcounter{bottomnumber}{2} ' \
        '%\\setcounter{totalnumber}{2}     % 2 may work better ' \
        '%\\setcounter{dbltopnumber}{2}    % for 2-column pages ' \
        '%\\renewcommand{\\dbltopfraction}{0.9}	% fit big float above 2-col. text ' \
        '%\\renewcommand{\\textfraction}{0.07}	% allow minimal text w. figs ' \
        '%   Parameters for FLOAT pages (not text pages): ' \
        '%\\renewcommand{\\floatpagefraction}{0.7}	% require fuller float pages ' \
        '% N.B.: floatpagefraction MUST be less than topfraction !! ' \
        '%\\renewcommand{\\dblfloatpagefraction}{0.7}	% require fuller float pages ' \
        '%------------------------------- ' \
        '\\newenvironment{my_description} ' \
        '{\\begin{description} ' \
            '\\setlength{\\itemsep}{1pt} ' \
            '\\setlength{\\parskip}{0pt} ' \
            '\\setlength{\\parsep}{0pt}} ' \
        '{\\end{description}}'

    return doc

'''
Get the contents of the latex file that will be displayed in the pdf.
This method calls other helper functions.
'''
def getLatexContents(projDescFile, statsFile, imgFile, assemblyStatsFile, alignmentStatsFile, logo, rqc_version):
    ###objProj = projDescFile
    ###TODO comment out so it is always the current date
    ###currentDate = projDescFile.get("run_date")
    ###if currentDate is None or len(currentDate) < 2:
    currentDate = get_timestamp()

    #header = '';
    #title = objProj.get("seq_unit_name") + objProj.get("library_name")
    #if title :
    #    header = "\\textit{" + toLatex(title) + "}"
    
    #my ($logoImg, $logoWidth) = $objStats->getLogoAttrib();
    #if ( $logoImg !~ /^\// ) {
    #    $logoImg = abs_path("$RealBin/../config")."/logo.jpg";
    #}
    
    ###The table consists of 2 columns across the page from top-bottom
    doc = '\\begin{table}[h] \n ' \
        '\\centering \n ' \
        '\\begin{tabular}{p{8cm}p{8cm}} \n '
 
    ###Upper left with logo   
    if logo.endswith("jpg"):
        doc += '\\begin{tabular}{c} \\includegraphics[width=180pt]{' + logo + '} \\\\ \n \url{http://jgi.doe.gov} \\end{tabular} & \n '
    else:
        doc += '\\begin{tabular}{c} ' + logo + ' \\\\ \n \url{http://jgi.doe.gov}  \\end{tabular} & \n '

    ###Upper right with sequence and project info.
    ####\n\\vspace{2 cm} \\\\ \n '
    ###'{\\Large http:\/\/\/ } \\\\'
    ###"Date and Time: " + doc += str(currentDate)  
    ###doc += 'Fastq QC report \\newline \n '
    ###doc += "\\vspace{-3 cm}"
    doc += "\\begin{tabular}{p{7cm}}"

    doc += "{\\Large QC Report} \\\\ \n "

    ###Print the seq proj id
    doc += "{\\bf Sequencing Project ID:} \n "
    if ('seq_proj_id' in projDescFile):
            desc = ''
            value = str(projDescFile.get('seq_proj_id'))
            doc += "{\\bf " + toLatex(value) + "} \\\\ \n"
            ###doc += " \\\\ \\hline\n";
            ###hasDesc = 1;

    ###Print the seq proj name
    doc += "{\\bf Sequencing Project Name:} \\\\ \n "
    if ('seq_proj_name' in projDescFile):
            desc = ''
            value = projDescFile.get('seq_proj_name')
            doc += "{\\bf " + toLatex(value) + "} \\\\ \n"
            ###doc += " \\\\ \\hline\n";
            ###hasDesc = 1;

    ###Print the sequence unit name.
    #TODO instead of get use ...in... or has_key
    doc += "{\\bf File Name:} \\\\ \n "
    if ('seq_unit_name' in projDescFile):
            desc = ''
            value = projDescFile.get('seq_unit_name')
            doc += "{\\bf " + toLatex(value) + "} \n"
            ###doc += " \\\\ \\hline\n";
            ###hasDesc = 1;

    doc += "\\end{tabular}"

    ###Call the helper functions.
    doc += ' \\\\[1cm] \n '
    doc += '\n  \n \n  \n '
    doc += getLatexBasePosQuality(projDescFile, statsFile, imgFile) + " & " + getLatexProjectInfo(projDescFile, statsFile, imgFile) + ' \\\\[0.5cm] \n '
    doc += getLatexReadQuality(projDescFile, statsFile, imgFile) + " & " + getLatexContamStats(statsFile, imgFile) + ' \\\\[0.5cm] \n '
    ###doc += getLatexCycleNucleotide(projDescFile, statsFile, imgFile)
    doc += getLatexReadGcHistogram(statsFile, imgFile) + "\n & " + getLatexInsertSizeHistogram(statsFile, imgFile) + " \\hspace{1em} \\newline  \\hspace{1em} \\newline  \\hspace{1em} \\newline  \\hspace{1em} \\newline \\begin{tabular}{c}  " + str(currentDate) + " \n . RQC version " + rqc_version + " \n \\end{tabular}  \\\\ \n"
    
    doc += '\\end{tabular}\n ' \
        '\\end{table}\n '
        #'\\newline ' \
        #'\\newline '
    
    return doc;


'''
Content body - upper left
Return an image with base pos quality
'''
def getLatexBasePosQuality(projDescFile, objStats, imgFile):
    ###brackets are needed so the filename extension won't be misunderstood with other dots.
    ###doc += '\\begin{tabular}{c} ' \
    doc = '{\\bf Average Base Position Quality} \\newline \n'
    if 'read qual pos plot merged' in imgFile:
        doc += '\\includegraphics[width=200pt]{{' +  imgFile.get('read qual pos plot merged').replace(".png", "}.png")  + '}  \n'
        ###'\\\\ \\hline '
        ####'\\end{tabular} ' \
        ####'\\newline '
    return doc


'''
Content body - middle left
Return an image with read quality
'''
def getLatexReadQuality(projDescFile, objStats, imgFile):
    ###doc += ""

    ###brackets are needed so the filename extension won't be misunderstood with other dots.
    ###doc = '\\begin{tabular}{c} ' \
    doc = '{\\bf Average Read Quality} \\newline \n'
    if 'read qhist plot' in imgFile:
        doc += '\\includegraphics[width=200pt]{{' +  imgFile.get('read qhist plot').replace(".png", "}.png")  + '}  \n'
        
    return doc

'''
Content body - top right project info
'''
def getLatexProjectInfo(projDescFile, objStats, imgFile):
    
    ###tie my %params, "Tie::IxHash" or die "Couuld not tie %params\n";
    objProj = projDescFile;

    doc = ''
    #header = '\\vspace{2 cm}\n\n\n ' \
    ###doc += '\\begin{table}[h] \n '
    ###doc += '\\tiny \n '
    #        '\\centering ' \
    doc += "{\\bf Sequencing Information } \\newline \n"
    doc += '\\begin{tabular}{rl} ' ###\\hline \\hline 
    ###'\\section{Project Information} ' \

    if ('platform_name' in objProj):
            desc = 'Sequencing Platform:'
            value = objProj.get('platform_name')
            doc += toLatex(desc) + " & " + toLatex(value);
            doc += " \\\\ \n";
            ###hasDesc = 1;

    if ('instrument_type' in objProj):
            desc = 'Instrument Type:'
            value = str(objProj.get('instrument_type'))
            doc += toLatex(desc) + " & " + toLatex(value);
            doc += " \\\\ \n";
            ###hasDesc = 1;

    doc += getLatexBaseReadStats(projDescFile, objStats, imgFile)
    
    ###$doc .= "\\hline\n";
    doc += "\\end{tabular}\n"
    ###doc += "\\end{table}\n";
    
    return doc;


'''
Content body - middle right
Contamination statistics
'''
def getLatexContamStats(objStats, imgFile):
    doc = ''
    stats_acceptable_in_report = ['illumina read percent contamination artifact',
                                  'illumina read percent contamination artifact 50bp',
                                  'illumina read percent contamination DNA spikein',
                                  'illumina read percent contamination RNA spikein',
                                  'illumina read percent contamination ecoli combined',
                                  'illumina read percent contamination fosmid',
                                  'illumina read percent contamination mitochondrion',
                                  'illumina read percent contamination plastid',
                                  'illumina read percent contamination phix',
                                  'illumina read percent contamination rrna']
                                  ###'illumina read percent contamination contaminants']

    ###Mapping of db text fields to labels for the report
    ###Latex-ready stats labels
    stats_label_mapping = {}
    stats_label_mapping['illumina read percent contamination artifact'] = 'Adapters:'
    stats_label_mapping['illumina read percent contamination artifact 50bp'] = 'Adapters (first 50bp only):'
    stats_label_mapping['illumina read percent contamination DNA spikein'] = 'DNA Spike-ins:'
    stats_label_mapping['illumina read percent contamination RNA spikein'] = 'RNA Spike-ins:'
    stats_label_mapping['illumina read percent contamination ecoli combined'] = 'E. coli:'
    stats_label_mapping['illumina read percent contamination fosmid'] = 'Fosmid Vector:'
    stats_label_mapping['illumina read percent contamination mitochondrion'] = 'Mitochondria:'
    stats_label_mapping['illumina read percent contamination plastid'] = 'Chloroplast:'
    stats_label_mapping['illumina read percent contamination phix'] = 'PhiX:'
    stats_label_mapping['illumina read percent contamination rrna'] = 'rRNA:'
    stats_label_mapping['illumina read percent contamination contaminants'] = 'JGI contaminants:'

    ###doc += "\\vspace{-22mm} "
    doc += "{\\bf Reads Matching Potential Contaminants } \\newline \n"

    doc += "\\begin{tabular}{rl} \n";
    ####doc += "\\begin{longtable}{|l|l|} \\hline \\hline\n";

    for s in stats_acceptable_in_report:    
        if objStats.has_key(s):
            doc += stats_label_mapping.get(s)
            ###doc += "\\multicolumn{1}{C{4cm}}{" + toLatex( s ).replace("_"," ") + "}"
            doc += " & " + str( toLatex(    str( float( int(float(str(objStats.get(s)).strip("%")) * 100.0) ) / 100.0 )        ) )
            if s.find("ercent") > 0:
                doc += '\\%'
            doc += " \\\\ \n"

        ####else:
        ####    doc += "\\multicolumn{1}{C{4cm}}{" + toLatex( s ).replace("_"," ") + "}"
        ####    doc += " & none " + " \\\\ \\hline\n";

    #k = doc.rfind("\\hline\n")
    #doc = doc[:k]

    doc += "\\end{tabular}\n";

    return doc

'''
Content body - bottom left
Read GC histogram image.
TODO make more generic with label and image file path as arguments.
'''
def getLatexReadGcHistogram(objStats, imgFile):
    doc = ''
    
    ####doc += "\\newpage\n"
    header = "{\\bf Read GC}"


    ###brackets are needed so the filename extension won't be misunderstood with other dots.
    doc += header + '\\newline \n'
    if 'read GC plot' in imgFile:
        doc += '\\begin{tabular}{c} ' \
            '\\includegraphics[width=200pt]{{' +  imgFile.get('read GC plot').replace(".png", "}.png")  + '} ' \
            '\\\\ ' \
            '\\end{tabular} '
        ###'\\newline '
        ###'Read GC Histogram &  \\\\' \

    return doc






'''
Content body - bottom left
Insert Size histogram image.
'''
def getLatexInsertSizeHistogram(objStats, imgFile):
    doc = ''
    header = "{\\bf Insert Size Histogram}"


    ###brackets are needed so the filename extension won't be misunderstood with other dots.
    if 'ILLUMINA_READ_INSERT_SIZE_HISTO_PLOT' in imgFile:
        doc += header + '\\newline \n'
        doc += '\\begin{tabular}{c} ' \
            '\\includegraphics[width=200pt]{{' +  imgFile.get('ILLUMINA_READ_INSERT_SIZE_HISTO_PLOT').replace(".png", "}.png")  + '} ' \
            '\\\\ ' \
            '\\end{tabular} '
    else:
        doc = "\\hspace{1em} \\newline  \\hspace{1em} \\newline  \\hspace{1em} \\newline  \\hspace{1em} \\newline  \\hspace{1em} \\newline  \\hspace{1em} \\newline  \\hspace{1em} \\newline  \\hspace{1em} \\newline  \\hspace{1em} \\newline "

    return doc


'''
Content body - Upper right.
Statistics on read quality that will be displayed in the upper right.
'''
def getLatexBaseReadStats(projDescFile, objStats, imgFile):
    doc = ''
    stats_acceptable_in_report = ['read count',                                  
                                  'read Q30']

    ###Mapping of db text fields to labels for the report
    ###Latex-ready stats labels
    stats_label_mapping = {}
    stats_label_mapping['read count'] = 'Read Count:'
    stats_label_mapping['read Q30'] = 'Percent of Reads $\\geq$ Q30 Average:'

    ###doc += "\\begin{tabular}{|p{5cm}p{10cm}|} \\hline \\hline\n";
    ####doc += "\\begin{longtable}{|l|l|} \\hline \\hline\n";

    s1 = 'read length 1'
    s2 = 'read length 2'
    #TODO change to test if it is a number or not.
    if s1 in objStats and s2 in objStats:
        if objStats.get(s1) == objStats.get(s2) and objStats.get(s1).isdigit() and objStats.get(s2).isdigit():
            doc += 'Read Length:'
            doc += " & $2 \\times " + str( toLatex( objStats.get(s1) ) ) + "$" + " bp"
            doc += " \\\\ \n"
        elif not objStats.get(s1) == objStats.get(s2) and objStats.get(s1).isdigit() and objStats.get(s2).isdigit():
            doc += 'Read Length:'
            doc += " & $1 \\times " + str( toLatex( objStats.get(s1) ) ) + "$\\;\\;\\;  $1 \\times " + str( toLatex( objStats.get(s2) ) ) + "$" + " bp"
            doc += " \\\\ \n"
        elif not objStats.get(s1) == objStats.get(s2) and objStats.get(s1).isdigit() and not objStats.get(s2).isdigit():
            doc += 'Read Length:'
            doc += " & $1 \\times " + str( toLatex( objStats.get(s1) ) ) + "$" + " bp"
            doc += " \\\\ \n"
    elif s1 in objStats:
        if objStats.get(s1).isdigit():
            doc += 'Read Length:'
            doc += " & $1 \\times " + str( toLatex( objStats.get(s1) ) ) + "$" + " bp"
            doc += " \\\\ \n"

    for s in stats_acceptable_in_report:    
        if s in objStats:
            doc += stats_label_mapping.get(s)
            ###doc += "\\multicolumn{1}{C{4cm}}{" + toLatex( s ).replace("_"," ") + "}"
            doc += " & " + str( toLatex( objStats.get(s) ) )
            if s.find("ercent") > 0 or stats_label_mapping.get(s).find("ercent") > 0:
                doc += '\\%'
            doc += " \\\\ \n"
        ####else:
        ####    doc += "\\multicolumn{1}{C{4cm}}{" + toLatex( s ).replace("_"," ") + "}"
        ####    doc += " & none " + " \\\\ \\hline\n";


    s1 = 'overall bases Q score mean'
    s2 = 'overall bases Q score std'
    if s1 in objStats and s2 in objStats:
        doc += 'Average Base Quality Score:'
        doc += " & " + str( toLatex( str( float( int(float(str(objStats.get(s1)).strip("%")) * 100.0) ) / 100.0 )  ) ) + " +- " + str( toLatex( str( float( int(float(str(objStats.get(s2)).strip("%")) * 100.0) ) / 100.0 )  ) )
        doc += " \\\\ \n"

    s1 = 'ILLUMINA_READ_INSERT_SIZE_AVG_INSERT'
    s2 = 'ILLUMINA_READ_INSERT_SIZE_STD_INSERT'
    if s1 in objStats and s2 in objStats:
        doc += 'Insert Size:'
        
        if objStats.get(s1) == "NaN":
            objStats[s1] = 0.0        
        if objStats.get(s2) == "NaN":
            objStats[s2] = 0.0
        
        doc += " & " + str( toLatex( str( int( round(float(objStats.get(s1)) ) )  )  ) ) + " +- " + str( toLatex( str( int( round(float(objStats.get(s2)) ) )  )  ) )
        doc += " \\\\ \n"


    s1 = 'ILLUMINA_READ_INSERT_SIZE_MODE_INSERT'
    if s1 in objStats:
        doc += 'Insert Size Mode:'
        doc += " & " + str( toLatex( str( int( round(float(objStats.get(s1)) ) )  )  ) )
        doc += " \\\\ \n"

    s1 = 'ILLUMINA_READ_INSERT_SIZE_JOINED_PERC'
    if s1 in objStats:
        doc += 'Percent Read Pairs Merged:'
        doc += " & " + str( toLatex( str( int( round(float(objStats.get(s1).replace("%","")) ) )  )  ) ) + "\\%"
        doc += " \\\\ \n"

    s1 = 'read q20 read1'
    s2 = 'read q20 read2'
    if s1 in objStats and objStats.get(s1).isdigit():
        doc += 'Q20 Read Length, Read 1:'
        doc += " & " + str( toLatex( objStats.get(s1) ) ) + " bp"
        doc += " \\\\ \n"
    if s2 in objStats and objStats.get(s2).isdigit():
        doc += 'Q20 Read Length, Read 2:'
        doc += " & " + str( toLatex( objStats.get(s2) ) ) + " bp"
        doc += " \\\\ \n"

    
    s1 = 'read GC mean'
    s2 = 'read GC std'
    if s1 in objStats and s2 in objStats:
        doc += 'Read GC:'
        doc += " & " + str( toLatex( str( float( int(float(str(objStats.get(s1)).strip("%")) * 100.0) ) / 100.0 )  ) ) + " +- " + str( toLatex( str( float( int(float(str(objStats.get(s2)).strip("%")) * 100.0) ) / 100.0 )  ) ) + " \\%"
        doc += " \\\\ \n"

    #k = doc.rfind("\\hline\n")
    #doc = doc[:k]

    ###doc += "\\end{tabular}\n";
    return doc







#============================================================================#
def toLatex(name):    
    # Escape characters to make it latex friendly.
    #
    name = name.replace("\\", "\\textbackslash" )        # escape \
    name = name.replace("$", "\\$")                    # escape $
    name = name.replace("%", "\\%")                     # escape %
    name = name.replace("_", "\\_")                     # escape _
    name = name.replace("{", "\\{")                    # escape {
    name = name.replace("}", "\\}")                    # escape }
    name = name.replace("&", "\\&")                     # escape &
    name = name.replace("#", "\\#")                     # escape #
    name = name.replace("^", "\\textasciicircum")       # escape ^
    name = name.replace("~", "\\textasciitilde")        # escape ~
    name = name.replace("*", "\\textasteriskcentered ") # escape *
    name = name.replace("/", "\\slash ")                # escape /
    name = name.replace("|", "\\textbar ")             # escape |
    name = name.replace("-", "\\textendash ")           # escape -
    name = name.replace(">", "\\textgreater ")           # escape >
    name = name.replace("<", "\\textless ")              # escape <
    ###name = name.replace(":", "\\_" )        # escape :

    # handle double and single quoted strings.
    name = name.replace("\"(\w)", "``$1")
    name = name.replace("'(\w)", "`$1")
    name = name.replace("([\w\.?!])\"", "$1''")

    return name;


#============================================================================#
def get_timestamp():
    return time.strftime("%m/%d/%Y %H:%M:%S")


def getCurrentDate(): 
    # Returns current date and time in format:
    # 'MM/DD/YYYY HH24:MI:SS'
    #my ($sec,$min,$hour,$day,$mon,$year) = localtime(time);
    #$mon++;
    #$year+=1900;
    #datetime = time.strftime("%Y-%m-%d %H:%M:%S")
    #return datetime
    # 14 digits YYYYMMDDHHMMSS
    year = datetime.datetime.now().year
    month = datetime.datetime.now().month
    day = datetime.datetime.now().day
    hour = datetime.datetime.now().hour
    minute = datetime.datetime.now().minute
    second = datetime.datetime.now().second
    if (month < 10):
        month = "0" + str(month);
    if (day < 10):
        day = "0" + str(day);
    if (hour < 10):
        hour = "0" + str(hour);
    if (minute < 10):
        minute = "0" + str(minute);
    if (second < 10):
        second = "0" + str(second);
    res = str(month) + "/" + str(day) + "/" + str(year) + " " + str(hour) + ":" + str(minute) + ":" + str(second);
    return res;


'''
Create a timestamp with the format YYYYMMDDHHMMSS.
'''
def create_timestamp():
    # 14 digits YYYYMMDDHHMMSS
    year = datetime.datetime.now().year
    month = datetime.datetime.now().month
    day = datetime.datetime.now().day
    hour = datetime.datetime.now().hour
    minute = datetime.datetime.now().minute
    second = datetime.datetime.now().second
    if (month < 10):
        month = "0" + str(month);
    if (day < 10):
        day = "0" + str(day);
    if (hour < 10):
        hour = "0" + str(hour);
    if (minute < 10):
        minute = "0" + str(minute);
    if (second < 10):
        second = "0" + str(second);
    res = str(year) + str(month) + str(day) + str(hour) + str(minute) + str(second);
    return res;



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

'''
This is the main calling point for the export of a PDF report.
The PDF report will be saved under the /tmp directory.

Arguments:
-f is the fast.gz file name.
-o is the output_path where the PDF report will be saved.
-r is the RQC software version.

TODO Top:
check if pdflatex is installed.

change the import statements to use the pipeline specific lib code.

have a directory named after the spid and files named after
the sequnits without srf or fastq.
prefix the directory with rqc-pdf-spid
instead of srf or fastq suffix use .pdf for filenames

allow both srf and fastq

web service bug , change the fs_location from /tmp

instead of using the library_info tables use a curl call against ITS to get the latest data

use """ instead of long strings.
use ... in .... to check if key exists n a hash, since it is faster than has_key or get
do not use get without checking if key exists (when getting image fs_location)

### determine the scratch directory where the tex and pdf files will be written.
### determine a location on the filesystem for the logo image.
#########################

title is fastq qc report
put a link to external jgi website

page 1:
date at bottom
display rqc version number next to date.
data file name instead of seq unit name
no library
no header (sequencing info or metadata section instead of project info)
only 2-3 fields(platform, instrument, raw reads count) and no table: sequencing platform, type, no raw reads count
right justify and with colons

page 2: "read count" not "read number"
read length : 1 30 by 4 150 (\times latex symbol)
overall base +- std

name "percent of reads greater than Q30 average", get rid of the other fields

Q20 read length for read 1, do not show for read 2 if single ended (check if "N/A" value for read2)

page 3: get rid of "read 1/2...."
    
page 4: no JGI contaminants
round to 3 digits

no 20mer sampling plots

read GC show with +-

'''
my_name = "RQC pdf report"
version = "1.1"    

def main(output_path, fastq, seq_unit_id, rqc_version):
    ###startruntime = time()
    #logo_image_fs_location = "/global/dna/projectdirs/PI/rqc/prod/rqc_software/htdocs/img/JGI_logo_stacked_DOEtag_RGB.jpg"
    logo_image_fs_location = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/JGI_logo_stacked_DOEtag_RGB.jpg"
    if not os.path.isfile(logo_image_fs_location):
        logo_image_fs_location = "JGI DOE logo"

    ## Validate output dir. Create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        print("Cannot find %s, creating as new", output_path)
        os.makedirs(output_path)

    log_level = "INFO"
    
    # (options, args) = parser.parse_args()
    
    #if options.version:
    #    print "%s: %s" % (my_name, version)
    #    exit(0)

    # initialize my logger
    log_file = os.path.join(output_path, "rqc_pdf_report.log")
    #print "Started phix_error pipeline, writing log to: %s" % (log_file)
    
    # log = logging object
    log = get_logger("rqc_pdf_report", log_file, log_level)

    log.info("Starting %s: %s", my_name, fastq)

    # database connection
    db = jgi_connect_db("rqc")

    if db == None:
        print "Cannot open database connection" 
        sys.exit(4)
    
    sth = db.cursor(MySQLdb.cursors.DictCursor)

    if seq_unit_id > 0:
        sql = "select seq_unit_name from seq_units where seq_unit_id = %s"
        sth.execute(sql, (seq_unit_id))
        rs = sth.fetchone()
        
        if rs:
            fastq = rs['seq_unit_name']
        


    ###Read the sequnit and/or libname as parameters
    ###Then query the mySQL database to retrieve all project description info and statistics
    ###for the sequnit and/or libname. Put the data into dictionaries.
    ###Previously Stephan's version read the data from files, but now we read it from the database.
    ###TODO:should we use ORM?
    projDescFile = {}
    statsFile = {}
    imgFile = {}
    assemblyStatsFile = {}
    alignmentStatsFile = {}

    # get READQC stats from db
    if fastq.endswith(".fastq.gz"):
        version = 1 # Latest data from runs
        pipeline = 3 # ReadQC
        sql = """
select rps.stats_name, rps.stats_value
from rqc_pipeline_stats rps
inner join rqc_pipeline_queue rpq on rps.rqc_pipeline_queue_id = rpq.rqc_pipeline_queue_id 
inner join seq_units s on rpq.seq_unit_id = s.seq_unit_id
where
    s.seq_unit_name = %s
    and rps.is_production = %s
    and rpq.rqc_pipeline_type_id = %s
        """
        sth.execute(sql, (fastq, version, pipeline))
    
    elif fastq.endswith(".srf"):
        pipeline = 1 # ReadQC in legacy 
        sql = """        
select ras.stats_name, ras.stats_value
from m2m_analysis_seq_unit m
inner join rqc_analysis a on m.analysis_id = a.analysis_id
inner join seq_units s on m.seq_unit_name = s.seq_unit_name
inner join rqc_analysis_stats ras on ras.analysis_id = a.analysis_id
where
    a.analysis_type_id = %s
    and s.seq_unit_name = %s
        """
        sth.execute(sql, (pipeline, fastq))
    
    rowCnt = int(sth.rowcount)
        
    #print sql+"<br>"
    #print "aid = %s<br>" % (align_id)
        
    for i in range(rowCnt):
        rs = sth.fetchone()
        statsFile[str(rs['stats_name']).strip()] = str(rs['stats_value']).strip()

    ###print "statsFile " + str(statsFile)


    # get READQC image files from db
    if fastq.endswith(".fastq.gz"):
        version = 1
        pipeline = 3
        sql = """
select rpf.file_type, rpf.file_name, rpf.fs_location
from rqc_pipeline_files rpf
inner join rqc_pipeline_queue rpq on rpf.rqc_pipeline_queue_id = rpq.rqc_pipeline_queue_id
inner join seq_units s on rpq.seq_unit_id = s.seq_unit_id
where
    s.seq_unit_name = %s
    and rpf.is_production = %s
    and rpq.rqc_pipeline_type_id = %s
        """
        sth.execute(sql, (fastq, version, pipeline))
        
    elif fastq.endswith(".srf"):
        pipeline = 1
        sql = """
select ras.stats_name as file_type, raf.analysis_file_id as file_name, raf.fs_location 
from m2m_analysis_seq_unit m
inner join rqc_analysis a on m.analysis_id = a.analysis_id 
inner join seq_units s on m.seq_unit_name = s.seq_unit_name
inner join rqc_analysis_stats ras on ras.analysis_id = a.analysis_id
inner join rqc_analysis_files raf on ras.stats_value = raf.file_name 
where
    a.analysis_type_id = %s
    and s.seq_unit_name = %s
    and raf.file_name like %s
        """

        sth.execute(sql, (pipeline, fastq, '%png'))
    
    rowCnt = int(sth.rowcount)
    #print sql+"<br>"
    #print "aid = %s<br>" % (align_id)
        
    for i in range(rowCnt):
            rs = sth.fetchone()
            ###move the png file to the current output dir and give it a .png extension
            origin_img_file =  os.path.join( str(rs['fs_location']).strip() ,  str(rs['file_name']).strip() )
            ###If it does end with png latex has an issue parsing the tex file, so copy the image to the output dir with a png suffix and use that img file.
            if not origin_img_file.endswith(".png"):
                dest_img_file = os.path.join(output_path, str(rs['file_name']).strip() + ".png")
                shutil.copy2(origin_img_file, dest_img_file)
                imgFile[str(rs['file_type']).strip()] = dest_img_file
            else:
                imgFile[str(rs['file_type']).strip()] = origin_img_file

    ## get ASSEMBLYQC stats from db
    #version = 1
    #pipeline = 4
    #sql = 'select rps.stats_name, rps.stats_value from rqc_pipeline_stats rps inner join rqc_pipeline_queue rpq ' \
    #        'on rps.rqc_pipeline_queue_id = rpq.rqc_pipeline_queue_id ' \
    #        'inner join seq_units s on rpq.seq_unit_id = s.seq_unit_id where s.seq_unit_name = %s and ' \
    #        'rps.is_production = %s and rpq.rqc_pipeline_type_id = %s'
    #    
    #sth.execute(sql, (fastq, version, pipeline))
    #rowCnt = int(sth.rowcount)
    ##print sql+"<br>"
    ##print "aid = %s<br>" % (align_id)
    #    
    #for i in range(rowCnt):
    #        rs = sth.fetchone()
    #        assemblyStatsFile[str(rs['stats_name']).strip()] = str(rs['stats_value']).strip()
    #
    ## get RNAQC stats from db
    #version = 1
    #pipeline = 6
    #sql = 'select rps.stats_name, rps.stats_value from rqc_pipeline_stats rps inner join rqc_pipeline_queue rpq ' \
    #        'on rps.rqc_pipeline_queue_id = rpq.rqc_pipeline_queue_id ' \
    #        'inner join seq_units s on rpq.seq_unit_id = s.seq_unit_id where s.seq_unit_name = %s and ' \
    #        'rps.is_production = %s and rpq.rqc_pipeline_type_id = %s'
    #    
    #sth.execute(sql, (fastq, version, pipeline))
    #rowCnt = int(sth.rowcount)
    ##print sql+"<br>"
    ##print "aid = %s<br>" % (align_id)
    ##TODO remove assembly and alsignment stats
    #
    #for i in range(rowCnt):
    #        rs = sth.fetchone()
    #        alignmentStatsFile[str(rs['stats_name']).strip()] = str(rs['stats_value']).strip()

    ###GET PROJECT INFORMATION
    #TODO specify fields selecting
    sql = """
select
    l.library_name,
    s.seq_unit_name,
    l.ncbi_organism_name,
    l.ncbi_tax_id,
    l.seq_proj_id,
    l.seq_proj_name,
    s.analysis_task_id,
    s.raw_reads_count,
    s.filter_reads_count,
    s.instrument_type,
    s.platform_name,
    l.sow_item_type,
    l.account_jgi_sci_prog,
    l.account_jgi_user_prog,
    l.kingdom,
    l.species,
    l.strain,
    l.isolate,
    l.genus 
from library_info l
inner join seq_units s on s.rqc_library_id = l.library_id 
where
    s.seq_unit_name = %s
        """
        
    sth.execute(sql, (fastq))
    rowCnt = int(sth.rowcount)
    #print sql+"<br>"
    #print "aid = %s<br>" % (align_id)
    for i in range(rowCnt):
        rs = sth.fetchone()
        ###projDescFile['seq_proj_id'] = rs['seq_proj_id']
        projDescFile['library_name'] = rs['library_name']
        projDescFile['seq_unit_name'] = rs['seq_unit_name']
        projDescFile['ncbi_organism_name'] = rs['ncbi_organism_name']
        projDescFile['ncbi_tax_id'] = rs['ncbi_tax_id']
        projDescFile['seq_proj_id'] = rs['seq_proj_id']
        projDescFile['seq_proj_name'] = rs['seq_proj_name']
        projDescFile['analysis_task_id'] = rs['analysis_task_id']
        projDescFile['raw_reads_count'] = rs['raw_reads_count']
        projDescFile['filter_reads_count'] = rs['filter_reads_count']
        projDescFile['instrument_type'] = rs['instrument_type']
        projDescFile['platform_name'] = rs['platform_name']
        projDescFile['sow_item_type'] = rs['sow_item_type']
        projDescFile['account_jgi_sci_prog'] = rs['account_jgi_sci_prog']
        projDescFile['account_jgi_user_prog'] = rs['account_jgi_user_prog']
        projDescFile['kingdom'] = rs['kingdom']
        projDescFile['species'] = rs['species']
        projDescFile['strain'] = rs['strain']
        projDescFile['isolate'] = rs['isolate']
        projDescFile['genus'] = rs['genus']

    ###print "projDescFile " + str(projDescFile)

    doc = '';
    doc += "\\documentclass[10pt]{article}\n";
    doc += getLatexPreamble() + "\n";
    doc += "\\begin{document}\n";
    doc += "\\sloppy\n"; # fixes word wrapping when word is hyphenated.
    doc += getLatexContents(projDescFile, statsFile, imgFile, assemblyStatsFile, alignmentStatsFile, logo_image_fs_location, rqc_version) + "\n";
    ###doc += getLatexProjectInfo(projDescFile) + "\n";
    ###doc += getLatexQcResults(projDescFile, statsFile, imgFile, assemblyStatsFile, alignmentStatsFile) + "\n";
    doc += "\\end{document}\n";


    #
    # Create latex scripting file.
    #
    texFile = os.path.join( output_path, fastq.replace(".srf", ".QC.tex").replace(".fastq.gz", ".QC.tex").replace(".fastq", ".QC.tex") )
    ###defPdfFile = os.path.join( output_path, "rqc_report.pdf")

    PDF_LATEX_EXE = "module unload texlive; module load texlive; pdflatex";
    
    OUT = open(texFile, 'w')
    OUT.write(doc)
    OUT.close()

    # Create pdf file.
    #
    #pdfcommand = PDF_LATEX_EXE + " " + texFile
    cmd = "%s -output-directory=%s %s" % (PDF_LATEX_EXE, output_path, texFile)
    log.info("- cmd: %s", cmd)
    std_out, std_err, exit_code = run_command(cmd, True)
    if exit_code > 0:
        log.error("failed to run cmd: %s", cmd)
        return exit_code

    db.close()
    
    ###print "\nCreated file $optOutputPdfFile\n" if -e $optOutputPdfFile;
    log.info("Completed %s: %s", my_name, fastq)


    ###totalruntime = str(time() - startruntime)
    ###log.info( "Total wallclock runtime: " + totalruntime )
    ###print "Total wallclock runtime: " + totalruntime

    return 0


if __name__ == "__main__":

    ###my_name = "RQC pdf report"
    ###version = "1.0"    

    output_path = None
    fastq = None
    rqc_version = "unknown"
    seq_unit_id = 0
    
    desc = 'rqc_pdf_report'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-f", "--fastq", dest="fastq", help="Fastq file name (for querying the RQC database)")
    parser.add_argument("-s", "--seq-unit-id", dest="seq_unit_id", help="Seq Unit Id")
    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to", required=True)
    parser.add_argument("-r", "--rqc-version", dest="rqc_version", help = "RQC version number", required=True)
    parser.add_argument('-v', '--version', action='version', version=version)

    options = parser.parse_args()

    if options.output_path:
        output_path = options.output_path

    if options.fastq:
        fastq = options.fastq

    if options.seq_unit_id:
        seq_unit_id = options.seq_unit_id

    if options.rqc_version:
        rqc_version = options.rqc_version
        
    if not fastq and not seq_unit_id:
        sys.exit(4)

    sys.exit(main(output_path, fastq, seq_unit_id, rqc_version))

