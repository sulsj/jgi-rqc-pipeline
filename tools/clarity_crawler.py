#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Clarity Crawler
- downloads gel images (default, --cmd = download)
* need .clarity file in home directory to get the files
- pulls SOP version (--cmd sop)
 
1.0 - 2017-02-08
- initial version

1.0.1 - 2017-03-06
- bug fix to look up library name

1.0.2 - 2017-06-01
- fix for BNOBP - file named as bmp but actually a csv so not converted to an image

1.0.3 - 2017-06-13
- fix for mysql on Cori

1.1 - 2017-08-30
- added ability to pull SOP based on seq unit/library (--cmd sop)

1.1.1 - 2017-11-27
- changed to use jgi_connect_db

1.1.2 - 2017-11-30
- updated to work on denovo

#./clarity_crawler.py -l bpgas -nd
./clarity_crawler.py -s 11301.1.199564.fastq.gz,11287.4.199524.fastq.gz,BPBBP -nd

./clarity_crawler.py  -s /global/dna/dm_archive/sdm/illumina/01/15/74/11574.1.212401.fastq.gz -o /global/projectb/scratch/brycef/t


./clarity_crawler.py -s 11826.6.221641.CTATCGC-TGCGATA.fastq.gz --cmd sop
./clarity_crawler.py -s BYHNA --cmd sop

To do:
- ?


File types
- *.xad = Agilent Bioanalyzer data file

Pacbio run: looking for run config (1x240min, 1x120, etc)
ssh brycef@sequoia.jgi-psf.org 'curl -u rqcapi:rqcapi https://clarity-prd01.jgi-psf.org/api/v2/artifacts?type=Analyte\&name=BOYXC\&process-type=LC%20Library%20Creation'
ssh brycef@sequoia.jgi-psf.org 'curl -u rqcapi:rqcapi https://clarity-prd01.jgi-psf.org/api/v2/artifacts/2-1745907'
<udf:field type="String" name="Run Mode">PacBio RS 1 X 240</udf:field>

ssh brycef@sequoia.jgi-psf.org 'curl -u rqcapi:rqcapi https://clarity-prd01.jgi-psf.org/api/v2/processes/24-205171'


ssh brycef@sequoia.jgi-psf.org 'curl -u rqcapi:rqcapi https://clarity-prd01.jgi-psf.org/api/v2/artifacts/92-1734320'


To get sequencing sop:
alias crqc='curl -u rqcapi:rqcapi'
#BYHNA 2-2173998 = gls_id
$ crqc https://clarity-prd01.jgi-psf.org/api/v2/processes?inputartifactlimsid=2-2173998
# limsid="24-362088"
$ crqc https://clarity-prd01.jgi-psf.org/api/v2/steps/24-362088/reagentlots
- reagentlots points to the SOP in use

getting other values:
https://clarity-prd01.jgi-psf.org/api/v2/artifacts/2-2173998
* shows sequencing - samples included, lib conversion factor, phix spike in %,
part of container https://clarity-prd01.jgi-psf.org/api/v2/containers/27-246348
https://clarity-prd01.jgi-psf.org/api/v2/samples/BAR25793A2 = its info ...


https://clarity-prd01.jgi-psf.org/api/v2/artifacts?type=Analyte\&name=BOYXC\&process-type=LC%20Library%20Creation
https://clarity-prd01.jgi-psf.org/api/v2/artifacts/2-1745907
* shows info on lib creation (pacbio)


https://clarity-prd01.jgi-psf.org/api/v2/artifacts?type=Analyte\&name=BHYNA\&process-type=LC%20Library%20Creation
* illumina - shows PCR cycles, run mode, concentration ... volume in udfs

/global/projectb/scratch/brycef/git/jgi-rqc-pipeline/tools/clarity_crawler.py \
-s /global/dna/dm_archive/sdm/illumina/01/21/64/12164.6.241366.fastq.gz \
-o /global/projectb/scratch/brycef/report/t5



/global/projectb/scratch/brycef/git/jgi-rqc-pipeline/tools/clarity_crawler.py \
-s /global/dna/dm_archive/sdm/illumina/01/21/64/12164.6.241366.fastq.gz \
-o /global/projectb/scratch/qc_user/rqc/tmp/t5
"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import os
import re
import sys
from argparse import ArgumentParser
import MySQLdb
import base64
import getpass

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from common import human_size, run_cmd, get_colors, get_msg_settings
from db_access import jgi_connect_db

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Read clarity config
'''
def get_clarity_config():
    clarity_config = os.path.join(os.getenv("HOME"), ".clarity")

    ssh_host = ""
    sftp_user = ""
    sftp_pwd = ""
    api_user = ""
    api_pwd = ""

    if os.path.isfile(clarity_config):
        fh = open(clarity_config, "r")
        for line in fh:
            if line.startswith("#"):
                continue
            else:
                arr = line.split("=")
                if len(arr) > 1:
                    key = str(arr[0]).strip().lower()
                    val = str(arr[1]).strip()

                    if key == "sftp_user":
                        sftp_user = val
                    elif key == "sftp_pwd":
                        sftp_pwd = val
                    elif key == "ssh_host":
                        ssh_host = val
                    elif key == "api_user":
                        api_user = val
                    elif key == "api_pwd":
                        api_pwd = val


        fh.close()
    else:
        print "- ~/.clarity config file missing"
        sys.exit(2)

    #print base64.decodestring(sftp_pwd)
    return ssh_host, sftp_user, sftp_pwd, api_user, api_pwd


'''
Need to use curl through ssh because genepool cannot connect directly to clarity's url
'''
def get_page_ssh(my_api):

    my_api = my_api.replace("&", "\&")
    web_page = "%s/%s" % (clarity_url, my_api)

    api_auth = "%s:%s" % (api_user, base64.decodestring(api_pwd))


    cmd = "ssh %s@%s 'curl -u %s %s'" % (uname, ssh_host, api_auth, web_page)
    # -u = user:pwd for http auth

    if debug:
        print "%s%s%s" % (color['yellow'], cmd, color[''])

    std_out, std_err, exit_code = run_cmd(cmd)
    


    if exit_code == 0:
        return std_out

    return ""


'''
Search by library name to get the artifact id
'''
def get_artifact_id(lib):

    artifact_id = None

    api = "/api/v2/artifacts?type=Analyte&name=%s&process-type=LC%%20Library%%20Creation" % lib

    page = get_page_ssh(api)

    #<artifact limsid="2-1734327" uri="https://clarity-prd01.jgi-psf.org/api/v2/artifacts/2-1734327"/>
    artifact_re = re.compile(r'<artifact limsid="(.*?)" uri="(.*?)"/>', re.IGNORECASE)

    for line in page.split("\n"):
        line = line.strip()
        if line.startswith("<artifact"):
            m = artifact_re.match(line)
            if m:
                artifact_id = m.group(1)


    return artifact_id

'''
get the parent process id
'''
def get_parent_id(artifact_id):

    parent_id = None

    if not artifact_id:
        print "no artifact id!  %s" % msg_fail
        sys.exit(2)

    artifact_api = "/api/v2/artifacts/%s" % artifact_id
    page = get_page_ssh(artifact_api)


    parent_re = re.compile(r'<parent-process uri="(.*?)" limsid="(.*?)"/>', re.IGNORECASE)
    parent_id = None
    for line in page.split("\n"):
        line = line.strip()
        if line.startswith("<parent-process"):
            m = parent_re.match(line)
            if m:
                parent_id = m.group(2)

    return parent_id


'''
Find the outputs in the parent list
'''
def get_lims_list(parent_id):

    if not parent_id:
        print "no parent id!  %s" % msg_fail
        sys.exit(2)

    parent_api = "/api/v2/processes/%s" % parent_id
    page = get_page_ssh(parent_api)


    #print page
    #<output uri="https://clarity-prd01.jgi-psf.org/api/v2/artifacts/92-1734326?state=344393" output-generation-type="PerAllInputs" output-type="ResultFile" limsid="92-1734326"/>
    output_re = re.compile(r'<output uri="(.*?)" output-generation-type="PerAllInputs" output-type="ResultFile" limsid="(.*?)"/>', re.IGNORECASE)
    lims_list = [] # list of lims ids to check for files ...
    for line in page.split("\n"):
        line = line.strip()
        if line.startswith("<output "):
            m = output_re.match(line)
            if m:
                lims_list.append(m.group(2))

    lims_list = list(set(lims_list)) # flatten out the repeats

    return lims_list


'''
Look up artifacts for each lims id to find the files
'''
def get_file_dict(lims_list):
    if len(lims_list) == 0:
        print "No lims list!  %s" % msg_fail
        sys.exit(2)


    file_dict = {} # file_id -> file_name
    name_re = re.compile(r'<name>(.*?)</name>', re.IGNORECASE)
    #<file:file limsid="40-16186" uri="https://clarity-prd01.jgi-psf.org/api/v2/files/40-16186"/>
    file_re = re.compile(r'<file:file limsid="(.*?)" uri="(.*?)"/>', re.IGNORECASE)

    #print "File List:"
    for lims_id in lims_list:
        artifact_api = "/api/v2/artifacts/%s" % lims_id
        page = get_page_ssh(artifact_api)
        file_name = ""
        file_id = 0
        for line in page.split("\n"):
            line = line.strip()
            if line.startswith("<file:file"):
                m = file_re.match(line)
                if m:
                    file_id = m.group(1)

            if line.startswith("<name>"):
                m = name_re.match(line)
                if m:
                    file_name = m.group(1)


        if file_name and file_id:
            #print "- '%s': %s" % (file_name, file_id)
            #print "https://clarity-prd01.jgi-psf.org/api/v2/files/%s" % file_id
            file_dict[file_id] = file_name

            #file_list.append(file_id)

            #https://clarity-prd01.jgi-psf.org/clarity/api/files/16186
            #<content-location>sftp://clarity-prd01.jgi-psf.org/opt/gls/clarity/users/glsftp/clarity/2017/01/24-202417/92-1734322-40-16186.BMP</content-location>
            #<original-location>2017 01 10 14H 44M Gel.BMP</original-location>
        #sys.exit(4)
        #print api
    return file_dict


'''
Download images to output_path using sftp server

'''
def sftp_download_files(file_dict):

    download_cnt = 0

    if len(file_dict) == 0:
        print "no files in dict!  %s" % msg_fail
        sys.exit(2)

    sftp_re = re.compile(r'<content-location>(.*?)</content-location>', re.IGNORECASE)
    orig_re = re.compile(r'<original-location>(.*?)</original-location>', re.IGNORECASE)

    #print "File SFTP Locations:"

    for file_id in file_dict:
        file_api = "/api/v2/files/%s" % file_id
        page = get_page_ssh(file_api)
        sftp_fs = ""
        orig_name = ""
        for line in page.split("\n"):
            line = line.strip()
            if line.startswith("<content-location>"):
                m = sftp_re.match(line)
                if m:
                    sftp_fs = m.group(1)
            if line.startswith("<original-location>"):
                m = orig_re.match(line)
                if m:
                    orig_name = m.group(1)

        if sftp_fs and orig_name:

            my_file = str(file_dict[file_id]).lower()
            file_download_flag = False
            dl_color = color['red']
            if my_file.endswith(".bmp") or my_file.endswith(".jpg") or my_file.endswith(".jpeg") \
            or my_file.endswith(".gif") or my_file.endswith(".tif") or my_file.endswith(".png"):
                file_download_flag = True
                dl_color = color['green']

            if debug:
                print "- '%s%s%s', file_id = %s, sftp = %s" % (dl_color, file_dict[file_id], color[''], file_id, sftp_fs)
            else:
                print "- '%s%s%s', file_id = %s" % (dl_color, file_dict[file_id], color[''], file_id)


            if file_download_flag:
                output_filename = file_dict[file_id].lower()
                output_filename = re.sub(r'[^A-Za-z90-9\-\.]+', '_', output_filename)
                output_filename = "%s-%s" % (su_cache[lib], output_filename)
                output_filename = os.path.join(output_path, output_filename)

                sftp_login = "%s:%s" % (sftp_user, base64.decodestring(sftp_pwd))
                cmd = "curl -k -o %s -u %s %s" % (output_filename, sftp_login, sftp_fs)
                # -o output to this file
                # -k ignore https check
                # -u http auth (user:pwd)

                if debug:
                    print "%s%s%s" % (color['red'], cmd, color[''])



                if download_flag:
                    if not os.path.isfile(output_filename):
                        std_out, std_err, exit_code = run_cmd(cmd)
                        
                        download_cnt += 1

                    else:
                        print "- %s exists already, not downloading  %s" % (output_filename, msg_warn)

                    if os.path.isfile(output_filename):

                        if output_filename.endswith(".bmp") or output_filename.endswith(".tif"):
                            output_png = output_filename.replace(".bmp", ".png").replace(".tif", ".png")
                            # imagemagick - bmp -> png =~ 99% file size savings
                            cmd = "convert %s %s" % (output_filename, output_png)
                            std_out, std_err, exit_code = run_cmd(cmd)
                            cmd = "rm %s" % output_filename
                            std_out, std_err, exit_code = run_cmd(cmd)

                            output_filename = output_png
                        if os.path.isfile(output_filename):
                            print "* Downloaded %s (%s)  %s" % (output_filename, human_size(os.path.getsize(output_filename)), msg_ok)
                            # BNOBP - first file named as "Double SPRI gel.bmp" but actually the sample sheet
                            # - convert didn't create anything
                        else:
                            print "Error: %s not converted to png!  %s" % (output_filename, msg_fail)

                    else:
                        print "Error: Download failed for: %s  %s" % (output_filename, msg_fail)
                else:
                    pass
                    #print "%sNot downloading!%s" % (color['red'], color[''])
                    #print cmd

            #print "* https://clarity-prd01.jgi-psf.org/clarity/api/files/%s" % (str(file_id).replace("40-", ""))
            # * if logged into clarity
            #https://clarity-prd01.jgi-psf.org/clarity/api/files/16186

    return download_cnt


'''
get child library to use for the lookup
* assuming each pool uses the same library to figure out the library creation info
'''
def get_library_list_from_seq_units():


    lib_cache = {} # make sure only 1 library per pool so we don't redo the same work

    for seq_unit in seq_unit_list:

        pool_head = False
        run_id = 0
        run_section = 0
        lib_name = None


        if seq_unit.endswith(".fastq") or seq_unit.endswith(".fastq.gz"):
            sql = "select library_name, run_id, run_section, is_multiplex from seq_units where seq_unit_name = %s"
        else:
            sql = "select library_name, run_id, run_section, is_multiplex from seq_units where library_name = %s"

        # could be a seq_unit or library
        #print sql % (os.path.basename(seq_unit))
        sth.execute(sql, (os.path.basename(seq_unit),))
        rs = sth.fetchone()

        if rs:
            if rs['is_multiplex'] == 1:
                pool_head = True
            run_id = rs['run_id']
            run_section = rs['run_section'] # lane id
            lib_name = rs['library_name']

        if pool_head:

            # look up non-pool head library name
            sql = "select library_name, run_id, run_section, is_multiplex from seq_units where run_id = %s and run_section = %s and is_multiplex = 0 and library_name not in ('phix','unknown')"
            sql += " limit 0,1"
            sth.execute(sql, (run_id, run_section))
            rs = sth.fetchone()
            pool_head = False
            run_id = 0
            run_section = 0
            lib_name = None

            if rs:
                run_id = rs['run_id']
                run_section = rs['run_section'] # lane id
                lib_name = rs['library_name']

        my_key = "%s.%s" % (run_id, run_section)
        if my_key in lib_cache:
            pass
            # don't add to the list of libraries since its from the same pooled head
        else:
            lib_cache[my_key] = lib_name
            su_cache[lib_name] = my_key
            library_list.append(lib_name)




    #return lib_list

'''
Search for gls id in RQC db
'''
def get_gls_id(seq_unit):
    gls_id = None

    # gls_pru_id is the same for all libs as the pool

    sql = "select gls_physical_run_unit_id from seq_units where "

    if seq_unit.startswith("1"):
        sql += "seq_unit_name = %s"

    elif seq_unit.startswith("p"):
        sql += "seq_unit_name = %s"

    else:
        sql += "library_name = %s"

    sth.execute(sql, (seq_unit,))
    rs = sth.fetchone()
    if rs:
        gls_id = rs['gls_physical_run_unit_id']

    return gls_id

'''
get the latest process id accociated with this gls pru id

$ crqc https://clarity-prd01.jgi-psf.org/api/v2/processes?inputartifactlimsid=2-2173998
'''
def get_process_id(gls_id):

    process_id = None

    if not gls_id:
        print "no gls_id!  %s" % msg_fail
        sys.exit(2)

    process_api = "/api/v2/processes?inputartifactlimsid=%s" % gls_id
    page = get_page_ssh(process_api)

    #<process uri="https://clarity-prd01.jgi-psf.org/api/v2/processes/24-362088" limsid="24-362088"/>
    process_re = re.compile(r'<process uri="(.*?)" limsid="(.*?)"/>', re.IGNORECASE)

    for line in page.split("\n"):
        line = line.strip()
        if line.startswith("<process"):
            m = process_re.match(line)
            if m:
                process_id = m.group(2)


    return process_id

'''
get the reagent_id from the process_id
$ crqc https://clarity-prd01.jgi-psf.org/api/v2/steps/24-362088/reagentlots
'''
def get_reagent_id(process_id):

    reagent_id = None

    if not process_id:
        print "no processs_id!  %s" % msg_fail
        sys.exit(2)

    reagent_api = "/api/v2/steps/%s/reagentlots" % process_id
    page = get_page_ssh(reagent_api)
    #<reagent-lot limsid="124-1" uri="https://clarity-prd01.jgi-psf.org/api/v2/reagentlots/124-1"/>
    reagent_re = re.compile(r'<reagent-lot limsid="(.*?)" uri="(.*?)"/>', re.IGNORECASE)
    for line in page.split("\n"):
        line = line.strip()
        if line.startswith("<reagent-lot"):
            m = reagent_re.match(line)
            if m:
                reagent_id = m.group(1)

    return reagent_id

'''
Get the SOP info from the reagentlots
https://clarity-prd01.jgi-psf.org/api/v2/reagentlots/124-1
'''
def get_sop_id(reagent_id):

    sop_id = None
    sop_name = None

    if not reagent_id:
        print "no reagent id!  %s" % msg_fail
        sys.exit(2)

    reagent_lot_api = "/api/v2/reagentlots/%s" % reagent_id
    page = get_page_ssh(reagent_lot_api)

    sop_re = re.compile(r'<lot-number>(.*?)</lot-number>', re.IGNORECASE)
    sop_name_re = re.compile(r'<name>(.*?)</name>', re.IGNORECASE)

    for line in page.split("\n"):
        line = line.strip()
        m = sop_re.match(line)
        if m:
            sop_id = m.group(1)
        m = sop_name_re.match(line)
        if m:
            sop_name = m.group(1)

    return sop_id, sop_name

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__


if __name__ == "__main__":

    clarity_url = "https://clarity-prd01.jgi-psf.org"

    my_name = "Clarity Crawler"
    version = "1.1.2"

    # Parse options

    usage = "%s, version %s\n" % (my_name, version)

    usage += """
cmd: Download
Downloads gel & fragment images from Clarity for a library
 * assumes libraries in a pool have the same gel & fragment images
 * need auto-login to the sequoia server because Clarity api not connectable from genepool
 * pooled heads (libraries or seq units) are converted to ONE child library per pool for lookup
    """


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)

    parser = ArgumentParser(usage = usage)

    parser.add_argument("-c", "--cmd", dest="cmd", type=str, help = "command (download = default, sop)")
    parser.add_argument("-s", "--seq-unit", dest="seq_unit", type=str, help = "look for files for this seq unit or library")
    parser.add_argument("-nd", "--no-download", dest="download_flag", default=True, action='store_false', help = "Don't download file, just show sftp link")
    parser.add_argument("-d", "--debug", dest="debug_flag", default=False, action='store_true', help = "Show api commands and other uglyness")
    parser.add_argument("-o", "--output-path", dest="output_path", type=str, help = "output path (if different than metadata.json, optional)")
    parser.add_argument("-v", "--version", action="version", version=version)


    library_list = [] # child libraries for a pool - one per pool to look up
    seq_unit_list = [] # convert to a library list
    su_cache = {} # library -> run_id.lane_id
    output_path = None
    download_flag = True
    debug = False
    cmd = "download" # grab gel images

    ssh_host, sftp_user, sftp_pwd, api_user, api_pwd = get_clarity_config()
    uname = getpass.getuser()

    # for Seung-Jin & others to test, need id_rsa.pub for sequoia
    if uname != "brycef":
        uname = "qc_user"


    args = parser.parse_args()

    if args.cmd:
        cmd = str(args.cmd).lower()

    if args.seq_unit:
        seq_unit_list = args.seq_unit.split(",")

    if args.output_path:
        output_path = args.output_path

    # boolean flags
    debug = args.debug_flag
    download_flag = args.download_flag

    if not output_path:
        output_path = os.getcwd()

    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    print "-" * 80
    print "%s/%s" % (my_name, version)

    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)



    if cmd == "download":

        if not download_flag:
            print "- not downloading any files  %s" % msg_warn

        # get a child library for the seq unit, append onto library_list
        get_library_list_from_seq_units()

        print "- library list for unique run and lane ids:"
        for lib in library_list:
            print "  * %s: %s" % (lib, su_cache[lib])


        dl_cnt = 0 # files downloaded

        for lib in library_list:
            #print lib
            lib = lib.upper()

            print
            print "Pulling images from Clarity-Prd01 for library: %s%s%s" % (color['cyan'], lib, color[''])

            artifact_id = get_artifact_id(lib)
            parent_id = get_parent_id(artifact_id)
            lims_list = get_lims_list(parent_id)
            file_dict = get_file_dict(lims_list)

            # only grab images
            dl_cnt += sftp_download_files(file_dict)

        print

        print "- downloaded %s files" % dl_cnt


    if cmd == "sop":

        for seq_unit in seq_unit_list:

            #BYHNA 2-2173998 = gls_id

            ## limsid="24-362088"
            gls_pru_id = get_gls_id(seq_unit)
            print "- seq_unit: %s, gls_pru_id: %s" % (seq_unit, gls_pru_id)

            process_id = get_process_id(gls_pru_id)
            print "-- process_id: %s" % process_id

            reagent_id = get_reagent_id(process_id)
            print "-- reagent_id: %s" % reagent_id

            sop_id, sop_name = get_sop_id(reagent_id)
            print "-- SOP: %s, %s" % (sop_id, sop_name)



    db.close()
    sys.exit(0)



"""

                      .-.
         .-._    _.../   `,    _.-.       Change is the essential process
         |   `'-'    \     \_'`   |       of all existence.
         \            '.__,/ `\_.--,                           Spock
          /                '._/     |
         /                    '.    /
        ;   _                  _'--;
     '--|- (_)       __       (_) -|--'
     .--|-          (__)          -|--.
      .-\-                        -/-.
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`



"""