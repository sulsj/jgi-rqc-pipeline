#!/usr/bin/env python

import os
import string
import json
import yaml
import copy

def reduce_dict(dictionary):
    ret = copy.deepcopy(dictionary)
    for key in ret.keys():
        if isinstance(ret[key], list):
            if len(ret[key]) == 1:
                ret[key] = ret[key][0]
        elif isinstance(ret[key], set):
            ret[key] = list(ret[key])
    return ret
#end reduce_dict
    

class InvalidOutputException(Exception):
    def __init__(self,message):
        self.message = message
    def __str__(self):
        return message
#end InvalidOutputException

class JamoSubmissionFile:
    
    def __init__(self, file_type, file_name):
        self.file_type = file_type
        self.file_name = file_name
        self.data = dict()
        self.data['file'] = file_name
        self.data['label'] = file_type
        self.data['metadata'] = dict()
    #end __init__
    
    def add_metadata(self, key, value):
        if key not in self.data['metadata']:
            self.data['metadata'][key] = list()
        self.data['metadata'][key].append(value)
    #end add_metadata
    
    def get_dict(self):
        ret = copy.deepcopy(self.data)
        ret['metadata'] = reduce_dict(ret['metadata'])
        return ret
    #end get_dict
    
    def get_metadata(self):
        for key in self.metadata.keys():
            yield (key,self.metadata[key])
    #end get_metadata
    
    def metadata_size(self):
        return len(self.metadata)
    #end metadata_size
#end JamoSubmissionFile
    
def __add_email_key__(key):
    def outer(func):
        def inner(obj, address):
            if key not in obj.email:
                obj.email[key] = set()
            obj.email[key].add(address)
        #end inner
        return inner
    return outer
#end __add_email_key__

class JamoSubmissionExporter:
    
    def __init__(self):
        self.metadata = dict()
        self.outputs = dict()
        self.inputs = list()
        self.email = dict()
        self.release_to = set()
        self.send_email_flag = False
        self.replace_img_submission = None
        self.custom_keys = dict()
    #end __init__

    def add_metadata(self, key, value):
        if key not in self.metadata:
            self.metadata[key] = list()
        self.metadata[key].append(value)
    #end add_metadata

    def add_input(self, file_name):    
        self.inputs.append(file_name)
        
    def add_output(self, file_type, file_name):    
        self.outputs[file_name] = JamoSubmissionFile(file_type,file_name)
    #end add_output
    
    def add_output_metadata(self, file_name, key, value):
        if file_name not in self.outputs:
            raise InvalidOutputException(file_name)
        else:
            self.outputs[file_name].add_metadata(key, value)
    #end add_output_metadata

    def add_release_dest(self, dest):
        self.release_to.add(dest)
    #end add_release_to
    
    def send_email(self, flag):
        self.send_email_flag = flag
    #end send_email
    
    @__add_email_key__('to')
    def add_email_recipient():
        pass
    #end add_email_recipient
    
    @__add_email_key__('cc')
    def add_cc_email_recipient():
        pass
    #end add_cc_email_recipient
    
    @__add_email_key__('bcc')
    def add_bcc_email_recipient():
        pass
    #end add_bcc_email_recipient

    @__add_email_key__('reply_to')
    def add_email_reply_to():
        pass
    #end add_email_reply_to

    def set_email_subject(self,subject):
        self.email['subject'] = subject
    #end set_email_subject
    
    def add_email_content(self,content,is_file=False):
        if 'content' not in self.email:
            self.email['content'] = list()
        if is_file:
            self.email['content'].append({'file': content})
        else:
            self.email['content'].append({'string': content})
    #end add_email_content

    def add_custom_key(self,key,value):
        self.custom_keys[key] = value
    #end add_custom_key

    def set_portal_display_location(self,file_name,location):
        self.outputs[file_name].add_metadata('portal', { 'display_location': location.split("/") })
    #end set_portal_display_location
    
    def __get_dump_dict__(self):
        to_dump = dict()
        to_dump["metadata"] = reduce_dict(self.metadata)
        if len(self.inputs) > 0:
            to_dump["inputs"] = self.inputs
        to_dump["outputs"] = [ x.get_dict() for x in self.outputs.values() ]
        to_dump["email"] = reduce_dict(self.email)
        if len(self.release_to) > 0:
            to_dump["release_to"] = list(self.release_to)
        if self.send_email_flag:
            to_dump["send_email"] = True
        else:
            to_dump["send_email"] = False
        for key in self.custom_keys.keys():
            to_dump[key] = self.custom_keys[key]
        return to_dump
    #end __get_dump_dict__
        
    
    def export_json(self, outstream):
        outstream.write(json.dumps(self.__get_dump_dict__(),indent=4,separators=(',',':')))
    #end export_json

    def export_yaml(self, outstream):
        outstream.write(yaml.dump(self.__get_dump_dict__(), default_flow_style=False))
    #end export_yaml
