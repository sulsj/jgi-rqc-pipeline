#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Search through the parsed file to show the taxonomies found from nt blasting

1.0 - 2017-02-03
- initial production version

1.1 - 2017-04-28
- use filtered nt results instead of read qc

1.1.1 - 2017-08-11
- use gold_tax_id when possible

Notes:
- the first count in the summary table only counts those projects that have a ncbi_tax_id and the blast taxonomy does not match it
- the count in the second table includes all hits (metagenome, metatranscriptome, etc)
- itags are not included because RQC does not run blast vs itags

To do:
- run monthly, include in to the RQC website
--- store in a folder and look for a yyyymm file to display...
- handle zip files
- option to use full tax list
- not show each seq unit (debug option?)
- check if taxonomy website is up

Counts
- only look up hit if pct identity > 90%
- if the seq unit has a ncbi_tax_id/gold_tax_id, all hits to the kingdom/phylum/class to the same thing are not counted
- if no tax id then nothing is counted as a contaminant (metagenome)
- hits to the JGI Contamination list are always counted


./contam.py -m 3 -y 2017 > $S/contam-201703.txt
grep -v "\-\ " contam-201703.txt > contam-report-201703.txt
put files in
$ cd /global/dna/shared/rqc/misc


$ give -u qc_user contam-report-201702.txt

as qc_user:
$ take -u brycef *-report
/global/dna/shared/rqc/misc/contam-report-201702.txt
"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import os
import re
import sys
from argparse import ArgumentParser
import MySQLdb

# custom libs in "../lib/"
this_dir = os.path.dirname(__file__)
sys.path.append(os.path.join(this_dir, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from db_access import jgi_connect_db
from taxonomy import get_taxonomy, format_taxonomy

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

def sum_blast_parsed(blast_file, target_tax_id):


    print "- blast file: %s" % blast_file

    pct_id_limit = 90.0 # must be > 90% identity

    target_tax = format_taxonomy(get_taxonomy(target_tax_id, "ncbi"))

    print "- target taxonomy: %s (%s)" % (target_tax, target_tax_id)



    gi_dict = {} # gi -> count

    fh = open(blast_file)
    for line in fh:
        if line.startswith("#"):
            continue

        arr = line.split()
        #print arr
        pct_id = 0.0
        try:
            pct_id = float(arr[-8])
        except:
            pass
            # bare except ...

        #print line
        gi = ""
        if pct_id > pct_id_limit:
            m = blast_pattern.match(line)
            if m:
                gi = str(m.group(1)) # treat as string even though its an int

        if gi:
            if gi in gi_dict:
                gi_dict[gi] += 1
                #print "%s += 1" % gi
            else:
                gi_dict[gi] = 1

        """
        #qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxid
        ['HISEQ15_291_CA8R0ANXX_2_1105_3943_100316',
        'gi|1061795588|dbj|LC086814.1|:Oryza_sativa_Indica_Group_genes_for_18S_rRNA,_ITS1,_5.8S_rRNA,_ITS2,_26S_rRNA,_complete_sequence,_strain__N16',
        '187',
        '2.84e-44',
        '101',
        '100.000',
        '1',
        '101',
        '101',
        '1563',
        '1463',
        '5773',
        'N/A']
        """

    gi_sum = {} # tax lineage -> count
    # takes a while
    print "- getting taxonomy for %s gi ids" % len(gi_dict)
    for gi in gi_dict:
        tax = ""
        if gi in super_gi:
            tax = super_gi[gi]
        else:
            #print "* %s" % gi
            tax = format_taxonomy(get_taxonomy(gi, "gi"))
            #print tax, gi
            #sys.exit(44)
            #tax = "%s (%s)" % (tax, gi)
            super_gi[gi] = tax

        if tax in gi_sum:
            gi_sum[tax] += gi_dict[gi]
        else:
            gi_sum[tax] = gi_dict[gi]


        if tax in jgi_contam_dict:
            jgi_contam_dict[tax]['read_cnt'] += gi_sum[tax]
            jgi_contam_dict[tax]['seq_unit_cnt'] += 1

        #print "%s= %s: %s" % (gi, gi_dict[gi], tax)

    tt_list = target_tax.split(";") # target tax list

    #print len(gi_dict)
    # this can take a while
    for tax in sorted(gi_sum.iterkeys()):
        # only if first 3 don't match tax
        tax_arr = tax.split(";")

        #print "%s/%s, %s/%s, %s/%s" % (tax_arr[0], tt_list[0], tax_arr[1], tt_list[1], tax_arr[2], tt_list[2])
        if tax_arr[0] == tt_list[0] and tax_arr[1] == tt_list[1] and tax_arr[2] == tt_list[2]:
            pass
        else:
            print "--- %s: %s reads" % (tax, gi_sum[tax])

            if target_tax_id >= 0:
                if tax in contam_dict:
                    contam_dict[tax]['seq_unit_cnt'] += 1
                    contam_dict[tax]['read_cnt'] += gi_sum[tax]
                else:
                    contam_dict[tax] = {"seq_unit_cnt" : 1, "read_cnt" : gi_sum[tax]}




    #print "Target: %s" % target_tax



'''
Get parsed tax list for seq unit and list everything not ncbi_tax_id

to do: send blast buffer
blast_buffer = ""
        if os.path.isfile(nt_file):
            fh = open(nt_file, "r")
            for line in fh:
                blast_buffer += line
            fh.close()

        else:
            zip_file = os.path.join(rs['fs_location'], "%s.zip" % (rs['rqc_pipeline_queue_id']))
            if os.path.isfile(zip_file):
                #print zip_file
                z_obj = zipfile.ZipFile(zip_file, "r")
                blast_buffer = z_obj.read(rs['file_name'])
                #print blast_buffer

'''
def sum_taxonomy(seq_unit_id, ncbi_tax_id):

    sth2 = db.cursor(MySQLdb.cursors.DictCursor)

    sql = """
select rpf.fs_location, rpf.file_name, rpf.file_type
from seq_units s
inner join rqc_pipeline_queue rpq on s.seq_unit_id = rpq.seq_unit_id and rpq.rqc_pipeline_type_id = 5 and rpq.rqc_pipeline_status_id = 14
inner join rqc_pipeline_files rpf on rpq.rqc_pipeline_queue_id = rpf.rqc_pipeline_queue_id
where
    s.seq_unit_id = %s
    and rpf.file_type = %s
    """
    #in ('read parsed file of ntx', 'read top100hit file of nt')
    file_type = "read top100hit file of nt" # quicker
    sth2.execute(sql, (seq_unit_id, file_type))
    row_cnt = int(sth2.rowcount)
    for _ in range(row_cnt):
        rs = sth2.fetchone()
        blast_file = os.path.join(rs['fs_location'], rs['file_name'])
        # what if in zip file?
        if os.path.isfile(blast_file):

            #if rs['file_type'].lower() == "read parsed file of nt":
            sum_blast_parsed(blast_file, ncbi_tax_id)


'''
Convert tax ids into something we can match
'''
def get_jgi_contam_tax():

    for tax_id in jgi_contam_list:
        tax = format_taxonomy(get_taxonomy(tax_id, "ncbi"))
        #print "%s = %s" % (tax_id, tax)
        jgi_contam_dict[tax] = {"read_cnt" : 0, "seq_unit_cnt" : 0}



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__


if __name__ == "__main__":


    my_name = "Contam Summary"
    version = "1.1.1"

    # Parse options

    usage = "%s, version %s\n" % (my_name, version)

    usage += """
Summarize contaminating reads from Filter QC blasting
    """


    # global
    blast_pattern = re.compile(r'.*gi\|(\d+)\|.*', re.IGNORECASE)
    super_gi = {} # gi -> taxonomy lookup
    contam_dict = {} # taxonomy -> read count & project count


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-m", "--month", dest="my_month", type=int, help="Month id (1..12)")
    parser.add_argument("-y", "--year", dest="my_year", type=int, help="4 digit year (2011..2020)")
    parser.add_argument("-l", "--limit", dest="limit", type=int, help="seq unit limit")
    parser.add_argument("-v", "--version", action="version", version=version)

    # contams we identified at JGI (ncbi tax ids)
    jgi_contam_list = [305, 1280, 511145, 190485, 267608, 216595, 272558, 176279, 205922, 232721, 398578, 391008, 62928, 522373, 688245,
                       757424, 742013, 1114970, 1144306, 358220, 745310, 1265504, 1217690, 983594, 348824, 1495331, 1333852, 68895,
                       80878, 1267562, 1242245, 470, 1125630, 300267, 198217]



    my_month = 1
    my_year = 2017
    limit = 0

    args = parser.parse_args()

    if args.my_month:
        my_month = args.my_month
        if my_month > 12 or my_month < 1:
            my_month = 1

    if args.my_year:
        my_year = args.my_year

    if args.limit:
        limit = args.limit


    jgi_contam_dict = {} # taxonomy -> read & seq unit count
    get_jgi_contam_tax()



    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)

    # list of seq units we check
    sql = """
select s.seq_unit_id, s.seq_unit_name, s.library_name, s.raw_reads_count,
l.ncbi_tax_id, l.actual_seq_prod_name, l.seq_prod_name, l.gold_tax_id
from seq_units s
inner join library_info l on s.rqc_library_id = l.library_id
where
    month(s.dt_created) = %s and year(s.dt_created) = %s
    and s.is_multiplex = 0
    and s.model_id = 9
    """

    if limit > 0:
        sql += " limit 0,%s" % limit

    sth.execute(sql, (my_month, my_year))
    row_cnt = int(sth.rowcount)
    print "- processing %s seq units" % row_cnt
    seq_unit_cnt = 0
    for _ in range(row_cnt):
        rs = sth.fetchone()
        #print "~" * 80
        if not rs['actual_seq_prod_name']:
            rs['actual_seq_prod_name'] = rs['seq_prod_name']
        
        
            
        print "- Seq Unit: %s, Library: %s, Target Tax: %s, Product: %s" % (rs['seq_unit_name'], rs['library_name'], rs['ncbi_tax_id'], rs['actual_seq_prod_name'])
        #print rs['seq_unit_id'], rs['seq_unit_name'], rs['ncbi_tax_id'], rs['actual_seq_prod_name']

        # summarize taxonomy
        tax_id = rs['ncbi_tax_id']
        if rs['gold_tax_id'] > 0:
            tax_id = rs['gold_tax_id']
        sum_taxonomy(rs['seq_unit_id'], rs['ncbi_tax_id'])
        seq_unit_cnt += 1

    print
    print "CONTAM SUMMARY for %s-%s: %s organisms" % (my_month, my_year, len(contam_dict))
    print "Examined %s Illumina seq units" % seq_unit_cnt
    print "|Taxonomy|Seq Unit Count|Read Count|"
    read_ttl = 0
    seq_unit_ttl = 0
    for tax in sorted(contam_dict.iterkeys()):
        print "|%s|%s|%s|" % (tax, contam_dict[tax]['seq_unit_cnt'], contam_dict[tax]['read_cnt'])
        read_ttl += contam_dict[tax]['read_cnt']
        seq_unit_ttl += contam_dict[tax]['seq_unit_cnt']

    print "|TOTAL|%s|%s|" % (seq_unit_ttl, read_ttl)

    print
    print "JGI CONTAMINATION HITS for %s-%s" % (my_month, my_year)
    read_ttl = 0
    seq_unit_ttl = 0
    print "|Taxonomy|Seq Unit Count|Read Count|"
    for tax in sorted(jgi_contam_dict.iterkeys()):
        print "|%s|%s|%s|" % (tax, jgi_contam_dict[tax]['seq_unit_cnt'], jgi_contam_dict[tax]['read_cnt'])
        read_ttl += jgi_contam_dict[tax]['read_cnt']
        seq_unit_ttl += jgi_contam_dict[tax]['seq_unit_cnt']
    print "|TOTAL|%s|%s|" % (seq_unit_ttl, read_ttl)

    db.close()

    sys.exit(0)
