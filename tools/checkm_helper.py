#!/usr/bin/env python
'''
    run checkm.py on multi threads.

    ----------
    Shijie Yao
    11/01/2016 : initial implementation
    03/27/2017 : return 1 if any of the 3 CheckM job fail
    05/17/2017 : use /global/dna instead of module load (Bryce)
    10/18/2017 : v1.0.1,  removed -p, -c options for checkm v 1.1, switched to run_cmd, don't include checkm/(bac/arc/lwf) when calling checkm (Bryce)
    
~/git/jgi-rqc-pipeline/tools/checkm_helper.py \
-i /global/projectb/scratch/brycef/iso/BTHZO/trim/scaffolds.trim.fasta \
-o /global/projectb/scratch/brycef/iso/BTHZO/iso/checkm \
--verbose     
bac & arc are fast, lwf longer

'''
import sys
import os
import time

from argparse import ArgumentParser
from threading import Thread
import threading

ROOT_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), os.path.pardir)
LIB_DIR = os.path.join(ROOT_DIR, 'lib')
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from common import run_cmd

RUN_SUCCESS = True
DEBUG = False
DEBUG_EXE = '/global/homes/s/syao/src/jgi-rqc-pipeline/tools/checkm.py'
#PROD_EXE = 'module load jgi-rqc; checkm.py'
PROD_EXE = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/checkm.py"

def do_checkm(fasta, rtype, odir, verbose):
    global RUN_SUCCESS
    #cmd = PROD_EXE
    cmd = os.path.join(os.path.dirname(__file__), 'checkm.py')
    if DEBUG:
        cmd = DEBUG_EXE

    #opt = '-f %s -o %s -r %s -p -c -t 8' % (fasta, odir, rtype)
    opt = '-f %s -o %s -r %s -t 8' % (fasta, odir, rtype) # -p, -c not used in v1.1
    cmd = '%s %s' % (cmd, opt)

    
    

    if verbose:
        print('CMD : %s\n' % cmd)
    stdOut, stdErr, exitCode = run_cmd(cmd)
    if exitCode == 0:
        if verbose:
            print('  + [%s] succeeded\n' % cmd)
    else:
        if verbose:
            print('  - [%s] failed [%s]\n' % (cmd, stdErr))
        RUN_SUCCESS = False

def run_thread(fasta, rtype, odir, verbose):
    t = Thread(name=rtype, target=do_checkm, args=(fasta, rtype, odir, verbose))
    t.start()
    return t

def do_driver(fasta, odir, verbose):
    
    #domain bac
    #jdir = os.path.join(odir, 'checkm/bac')
    run_thread(fasta, 'd-bac', odir, verbose)

    #domain arc
    #jdir = os.path.join(odir, 'checkm/arc')
    run_thread(fasta, 'd-arc', odir, verbose)

    #domain lwf
    #jdir = os.path.join(odir, 'checkm/lwf')
    run_thread(fasta, 'lwf', odir, verbose)


    while True:
        tcnt = threading.active_count()
        if tcnt == 1: # main thread
            break
        if verbose:
            main_thread = threading.currentThread()
            for t in threading.enumerate():
                if t != main_thread:
                    print('  - wait on thread %s to complete' % t.getName())
            print('\n')
        time.sleep(5)

    if verbose:
        print('All 3 checkm threads completed.\n')

def err_exit(msg):
    print('\n%s\n' % msg)
    sys.exit(1)


if __name__ == '__main__':
    NAME = 'Run checkm.py on multi threads'
    VERSION = '1.0.1'

    USAGE = 'prog [options]\n'
    USAGE += '* %s, version %s\n' % (NAME, VERSION)

    JOB_CMD = ' '.join(sys.argv)
    PARSER = ArgumentParser(usage=USAGE)
    PARSER.add_argument('-i', '--in', dest='input', help='fasta file', required=True)
    PARSER.add_argument('-o', '--odir', dest='odir', help='run directory (default to cwd)')
    PARSER.add_argument('-d', '--debug', dest='debug', action='store_true',  help='run in debug mode (ie use dev version checkm.py)')
    PARSER.add_argument('-', '--verbose', dest='verbose', action='store_true', default=False, help='print run status')
    PARSER.add_argument('-v', '--version', action='version', version=VERSION)

    ARGS = PARSER.parse_args()

    if not os.path.isfile(ARGS.input):
        err_exit('Need a fasta file as the input [%s]' % ARGS.input)

    DEBUG = ARGS.debug

    odir = os.getcwd()

    if ARGS.odir:
        odir = ARGS.odir
        if not os.path.isdir(odir):
            os.makedirs(odir)
           

    do_driver(ARGS.input, odir, ARGS.verbose)

    if not RUN_SUCCESS:
        sys.exit(1)
