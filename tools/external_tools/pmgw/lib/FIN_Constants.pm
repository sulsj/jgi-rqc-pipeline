#
# FIN_Constants.pm
#
# Initial Version:  2010-05-13
#
=cut
Most of these paths do not exist anymore
Bryce - 2013-10-29
=cut

#####################################################################
# Module Header                                                     #
#####################################################################

#
# Some boilerplate statements.
#
package FIN_Constants;

use Exporter;
@ISA = qw(Exporter);

%EXPORT_TAGS = (
		Dir => [qw(
			   BINDIR
			   GALAXY_DIR
			   PREFIN_BINDIR
                           RQC_SW_BIN_DIR
			   )],
		);

Exporter::export_tags('Dir');


use FindBin '$RealBin';

use constant BINDIR => "$RealBin/";
use constant GALAXY_DIR => ""; #'/house/homedirs/g/galaxy';
use constant PREFIN_BINDIR => ""; #'/home/prefin/bin';
use constant RQC_SW_BIN_DIR => BINDIR . "../../../";

$ENV{TILDEN_ENV} = 'PRODUCTION';
#$ENV{TILDEN_PATH} = '/jgi/tools/Tilden';
$ENV{CHROMAT_PATH} = 'tilden.jgi-psf.org';
$ENV{TILDEN_COMMON_SERVER} = 'venonat.jgi-psf.org'; 
$ENV{TILDEN_SERVER} = 'clay.jgi-psf.org';
$ENV{RQC_SW_TOP_LEVEL_DIR} = RQC_SW_BIN_DIR;


if (!defined($ENV{PATH})) {
    $ENV{PATH} = '/bin:/usr/bin';
}

#$ENV{PATH} = "$ENV{TILDEN_PATH}/bin:/jgi/tools/bin:$ENV{PATH}:/jgi/tools/454/rig/bin:/home/copeland/scripts:/home/analytics/metagenomeQC:/home/analytics/ruby/gems/bin:/jgi/tools/ruby/gems/1.8/bin:/var/lib/gems/1.8/bin";

if (!defined($ENV{LD_LIBRARY_PATH})) {
    $ENV{LD_LIBRARY_PATH} = '/lib:/usr/lib';
}

#$ENV{LD_LIBRARY_PATH} = "$ENV{TILDEN_PATH}/lib:/jgi/tools/lib:$ENV{LD_LIBRARY_PATH}";

if (!defined($ENV{PERL5LIB})) {
    $ENV{PERL5LIB} = '';
}

#$ENV{PERL5LIB} = "$ENV{TILDEN_PATH}/lib/perl5/site_perl/5.10.1:$ENV{PERL5LIB}";

if (!defined($ENV{PYTHONPATH})) {
    #$ENV{PYTHONPATH} = "$ENV{TILDEN_PATH}/lib/python2.5/site-packages";
}

#$ENV{GEM_PATH} = '/home/analytics/ruby/gems:/jgi/tools/ruby/gems/1.8:/var/lib/gems/1.8';
#$ENV{RUBYLIB} = '/jgi/tools/ruby/lib:/home/analytics/metagenomeQC';
#$ENV{MQC_PATH} = '/home/analytics/metagenomeQC';

$ENV{SGE_ROOT} = '/opt/sge';

$ENV{SGE_CELL} = 'hyperion';
$ENV{SGE_CLUSTER_NAME} = 'p536';

$ENV{SGE_QMASTER_PORT} = '';
$ENV{SGE_EXECD_PORT} = '';

$ENV{SGE_SETTINGS} = '/opt/sge/hyperion/common/settings';

my $ARCH = `$ENV{SGE_ROOT}/util/arch`;
my $DEFAULTMANPATH = `$ENV{SGE_ROOT}/util/arch -m`;
my $MANTYPE = `$ENV{SGE_ROOT}/util/arch -mt`;

if (defined($ENV{MANPATH})) {
    $ENV{MANPATH} = "$ENV{SGE_ROOT}/${MANTYPE}:${MANTYPE}";
} else {
    $ENV{MANPATH} = "$ENV{SGE_ROOT}/${MANTYPE}:${DEFAULTMANPATH}";
}

1;
