use strict;

=pod

This module contains functions that do general parsing and may be needed across
scripts.

sam rash
5-20-2002


=cut

############################
#BEGIN parseMod
{

#directive stuff
package parseMod;
require Exporter;
#add any paths for use/require directives

#any other modules needed
use genMod;

#exporting not all that important with objects/classes (I *think*)
our @ISA = qw(Exporter);
our @EXPORT = qw(parseMod_getName parseMod_getDesc parseMod_getTaxCode 
	parseMod_getArgs parseMod_loadGBtaxFile parseMod_loadTax2NameFile
	parseMod_loadSPtaxFile parseMod_getFName); #export by default
our @EXPORT_OK = qw(); #export by request

#symbol list: list reelvant symbols here

sub BEGIN()
{
#METHOD PROTOTYPES - put method prototypes here in order to have
#them executed at the time the module is compiled
sub parseMod_getName($;\$); #($defLine, $errorRef)
sub getAccnum($); #($defLine)
sub parseMod_getDesc($); #($defLine)
sub parseMod_getTaxCode($); #($defLine)
sub parseMod_getArgs($\@;$$$$$); #($pStr, @ARGV, $reqOptStr, $usageStr, $errorFatal, 
								 #$dispVal, $verbose)
sub procReqOptStr($\%); #($reqOptStr, %opts)
sub procArgStr($\%); #($pStr, %optInfo)
sub depStrToExpr($); #($depStr)
sub checkOpts(\%\%$); #(%opts, %optInfo, $usageStr)
sub dispParams(\%\@); #(%opts)
sub dispOptInfo($); #($pStr)
sub dispOptInfoHash(\%); #(%optInfo)

sub parseMod_loadGBtaxFile($\%); #($infile, %gi2ti)
sub parseMod_loadTax2NameFile($\%\%); #($infile, %tax2name, %name2tax)
sub parseMod_loadSPtaxFile($\%); #($infile, %sp2ti)
sub parseMod_getFName($); #($path)
sub getDirName($); #($path)

#related to parsing tax info and sw alignments
sub procItems($$$$$\%\%\%\%);#$parseDb, $nameDb,$taxCode, $hitDesc, $defLine, %gi2ti, %sp2ti, 
						   #%name2tax, %tax2name)
sub alignToInsDel($$$$); #($qAlign, $hAlign, $qStart, $hStart)
sub extractProtId($); #($defLine)
sub escapeString($$;$); #($str, $c, $ec)
}


########################

#put actual functions here

#must have a new function
sub new()
{
my $self = {};
bless $self;
return($self);
}

#must have a DESTROY function 
sub DESTROY()
{
}

#parse a defline and pull out name/id based on a fix set of rules (used across scripts
#to genererate a common name for a sequence from a defline)
sub parseMod_getName($;\$) #($defLine, $error)
{
my ($defLine, $errorRef) = @_;
my $name = $defLine;
if(defined($errorRef))
	{
	$$errorRef = 0;
	}
if(!((($name) = ($defLine =~ /^\s*(jgi\|.*?\|.*?)(?:\||\s+|$)/i)) || #internal JGI ID
	 (($name) = ($defLine =~ /^gi\|(\d+)\|/i)) || #genbank gi 
     (($name) = ($defLine =~ /^IPI:([a-zA-Z]+\d+)/i)) || #IPI
	 (($name) = ($defLine =~ /^.*?\(([OPQ]\d\w\w\w\d)\)/)) || #swissprot
	 (($name) = ($defLine =~ /ti\|(\d+)/)) || #ncbi trace repository
	 (($name) = ($defLine =~ /gi\|(\d+)\|/i)) || #funky genbank from yeast ppl
	 (($name) = ($defLine =~ /^(at.*?\-\d+\.[a-z]\d+)/i)) || #TIGR arabidopsis
	 (($name) = ($defLine =~ /^(\w+),/i)) || #TIGR microbes
	 (($name) = ($defLine =~ /^ref\|(.*?)\|/i)) || #ref seq
	 (($name) = ($defLine =~ /^\s*(scaffold_?\d+)/i)) || #scaffold defline
	 (($name) = ($defLine =~ /protein_id\s*(?:=|:)\s*([a-z]{3}\d{5})/i)) || #trembl protein id
	 (($name) = ($defLine =~ /^\s*(\w{3}:[A-Za-z.0-9_]+)\W+/)) || #kegg defline
	 (($name) = ($defLine =~ /([A-Z]{3}\d+\.[xy]\d)/)) || #custom diego 1 (reads)
	 (($name) = ($defLine =~ /^\s*gi:(\d+)/)) || #cog custom defline
	 (($name) = ($defLine =~ /^\s*gi:\s*NA\s*gene:\s*(.*?)\s+/)) || #cog custom defline, no gi
	 (($name) = ($defLine =~ /^(ENS\w+(?:\.\d+)?)\|/)) || #ensembl
	 (($name) = 
	 	($defLine =~ /(?:\W+|^)([a-z]{3}\d{5})(?:\W+|$)/i)) || #alt trembl
	 (($name) = ($defLine =~ /gnl\|(.*?\|.*?)(?:\||$|\s+)/)) || #general id
	 (($name) = ($defLine =~ /^\s*(\w+(?:\.\w+)?)\b/)) #read
	))
    {
	if($name =~ /jgi/i)
		{
		$name =~ s/jgi//g;
		$name =~ s/\|/\./g;
		}
	if(length($defLine) < 30)
		{
		#if it doesn't match any rule, but length < 30, assume it's unique
		$name = $defLine;
		}
	else
		{
		#take the first 255 chars as the name and raise an error
	    $name = $defLine;
    	$name = substr($name, 0, 255)
	    	if length($name) > 255;
		if(defined($errorRef))
			{
			$$errorRef = 1;
			}
		}
    } 
else
	{
	#deal with shredded data--if the orig defline has unshredding info, we need 
	#to preserve
	if($defLine =~ /^.*(:\d+-\d+(?::\d+)?)\s*$/ && 
		!($name =~ /^.*(:\d+-\d+(?::\d+)?)\s*$/))
		{
		$name .= $1;
		}

         
          
	}
return($name);
}

#given a genbank defline, get the accession number
sub getAccnum($) #($defLine)
{
my ($defLine) = @_;
my ($accnum) = ($defLine =~ /^\s*gi\|\d+\|\w+?\|(.*?)\..*?\|/);
return($accnum);
}

#parse a defline and pull out description (again, systematic to make scripts consistent)
sub parseMod_getDesc($) #($defLine)   
{
my ($defLine) = @_;
my $desc;
if(!((($desc) = ($defLine =~ /^.*[^\\]\|(.*)/)) || #genbank gi 
	 (($desc) = ($defLine =~ /.*\s+\(.*?\)\s+(.*)/)) || #swissprot
     (($desc) = ($defLine =~ /^(.*?)\|/)) ||
     (($desc) = ($defLine =~ /^(.*?)\s+/)) ))
    {
    $desc = $defLine
    } 
return($desc);
}

#parse a defline and pull out tax code (used in map to get tax id)
sub parseMod_getTaxCode($) #($defLine)   
{
my ($defLine) = @_;
my $tc;
if(!((($tc) = ($defLine =~ /^>?gi\|(\d+)\|/i)) || #genbank gi 
	 (($tc) = ($defLine =~ /^>?\w+?_(\w+?)\s+/))  #swissprot is a string
	)
  )
    {
    $tc = $defLine
    } 
return($tc);
}



#returns an array with ref to hash and ref to array. 1st element is ref to hash that
#has each dash-option as a key and then its value.  the ref to array is a list of non-dash
#options;  
#	$pStr is "[+[;depStr;]]<str>[:[<type>]][;<depStr]|..."
#	+ indicates a required option.  if specified w/o a depStr, it is an unconditional
#	requirement.  Otherwise, the option is required only if the depStr evals to true.
#	(see below for definition of depStr).  <str> is the option you want w/o the dash 
#	(ex: x for -x).  If a : is present, it means a value must be specified.  If <type>
#	follows the value, it may be either "p", "i", "n", "w", "o" which means the arg MUST be
#	a POSITIVE integer, an integer, number (int or float), or word only (a-zA-z0-9_-), or 
#	optional value (of any type) respectively.  No <type> means it can be anything 
#	(type "a", also allowed). 
#	If a ; <depStr> appears following <str>, this is an expression on the other options 
#	that must eval to true  other options are referred to simply by their <str> The 
#	expression may be a logical expression using *,+,! to mean and,or,not. it may 
#	also include parens ().  the expression is evaluated as being an expression on 
#	whether or not other options are defined
#	example:
#	"+;!n;p:w|d:w;i*o|i;d*o|o;d*i|n"
#	means -p is required if -n is not used, and must be only word chars.  
#	-d is word only and if it is defined, -i and -o must be also.  -i and -o have 
#	no type but must be defined if either other one is or -d is.
#
#	$reqOptStr is a string of the form <defNum>|<num>;<depStr>|...
#	ie, pipe separated "clauses".  At most one clause with only a num should appear
#	as this is the default number of required opts.  if more than one appears, the
#	last will be used as default.  Then, each clause is eval'd.  The first one
#	found to be true is used (the <num>).  else, the default value is used.  no
#	default means a default of 0
#
#	access to optional values via %opts.  if a value is boolean, it will be undef/1.
#	If it is a valued type, i,n,w,p then $opts->{key} will be the value.  If type
#	is "o", and the option is present, $opts->{key} will be undef, but true 
#	when tested with exists().  If a value is present, it will be the same as i,n,w,p
sub parseMod_getArgs($\@;$$$$$) #($pStr, @ARGV, $reqOpt, $usageStr, $errorFatal, 
								#$dispVal, $verbose)
{
my ($pStr, $argv, $reqOptStr, $usageStr, $errorFatal, $dispVal, $verbose) = @_;
$errorFatal = 1
	unless defined($errorFatal);
$reqOptStr = -1 #no required args unless specified
	unless defined($reqOptStr);
#first, remove whitespace from the string
$pStr =~ s/\s+//g;
my $numReqOpt;
my (@nonOpts, %opts) = ( (), ());
#if we are in verbose mode, concatenate info about option string to usage
if(defined($verbose) && ($verbose == 1))
	{
	$usageStr .= "option Info:\n" . dispOptInfo($pStr);
	}
my %optInfo;
my $opt;
#bulld up info about each option into a hash from arg string
procArgStr($pStr, %optInfo);
#scan args and put in approriate hash
my $i;
my $alen = scalar(@$argv);

for($i=0; $i < $alen; $i++)
	{
	#if this is a dash-option
	if(($argv->[$i] =~ /^-[a-zA-z][a-zA-Z0-9]*$/))
		{
		my $opt = $argv->[$i];
		$opt =~ s/^-//; #get actual option by removing the dash
		if(defined($optInfo{$opt}) && $optInfo{$opt}{type} ne "b") #this option takes a value
			{
			if($optInfo{$opt}{type} eq "o")
				{
				if($i < $alen-1 && !($argv->[$i+1] =~ /^-/))
					{
					#optionally takes a value 
					$opts{$opt} = $argv->[$i+1];
					$i++;
					}
				else
					{
					$opts{$opt} = undef;
					}
				}
			else
				{
				#this is an option requiring an argument
				die "$usageStr\noption [$argv->[$i]] requires a value of type " . 
					"[$optInfo{$opt}{type}] afterwards\n"
					if $i == $#{$argv};
				$opts{$opt} = $argv->[$i+1];
				$i++;
				}
			}
		elsif(defined($optInfo{$opt})) #option valid, but no value needed
			{
			#this is a boolean option (exists or not)
			$opts{$opt} = 1;
			}
		else #invalid option
			{
			die "$usageStr\nunrecognized option: [$argv->[$i]]\n";
			}
		}
	else
		{
		push(@nonOpts, $argv->[$i]);
		}
	}
#figure out if we are doing any required option checking
my $cond;
($cond, $numReqOpt) = procReqOptStr($reqOptStr, %opts);
# if $numReqOpt == -1, we aren't requiring any options (ie, do no checking)
#check options first
if(checkOpts(%opts, %optInfo, $usageStr) == 0)
	{
	exit(1) 
		if $errorFatal;
	return; #return undef to indicate failure
	}
#check nonOption count
my $numOptGiven = scalar(@nonOpts);
if(($numReqOpt != -1) && !eval("$numOptGiven $cond $numReqOpt"))
	{
	print $usageStr;
	if(@nonOpts > $numReqOpt)
		{
		print "too many arguments [need: $numReqOpt, got: ", scalar(@nonOpts), "]\n";
		}
	else #(@nonOpts < $numReqOpts)
		{
		print "not enough arguments [need: $cond $numReqOpt]\n";
		}
	exit(1)
		if($errorFatal);
	return; #return undef on error
	}
dispParams(%opts, @nonOpts)
	if(defined($dispVal) && ($dispVal == 1));
return(\%opts, \@nonOpts); #all checks okay, return values
}

#processes a required option string to determine how many required options there are
#given which options are defined
sub procReqOptStr($\%) #($reqOptStr, %opts)
{
my ($reqOptStr, $opts) = @_;
my $clause;
my $retNum = -1; #ie, no required number of args
my $retCond = "==";
foreach $clause (split(/\|/, $reqOptStr))
	{
	my ($num, $depStr) = split(/\;/, $clause);
	my $cond;
	if($num =~ /^\s*(>=|<=|>|<)\s*(\d+)\s*$/)
		{
		$num = $2;
		$cond = $1;
		}
	else
		{
		$cond = "==";
		}
	if(defined($depStr))
		{
		if(eval(depStrToExpr($depStr)))
			{
			$retNum = $num;
			$retCond = $cond;
			last;
			}
		}
	else #a default case
		{
		$retNum = $num;
		$retCond = $cond;
		}
	}
return($retCond, $retNum);
}

#processes an argument string and builds up 
#	%optInfo - hash with valid options (w/o dash) ask keys.  values are hashes
#			including 
sub procArgStr($\%) #($pStr, %optInfo)
{
my ($pStr, $optInfo) = @_;

my $item;
#for each option item
foreach $item (split(/\|/, $pStr))
	{
	#first, is this option required
	my $req = 0;
	if($item =~ /^\+/)
		{
		my ($reqStr) = ($item =~ /^\+\;(.*?)\;/);
		if(defined($reqStr)) #an expression has been passed in
			{
			$req = $reqStr; #store string for later evaluation
			}
		else #unconditional requirement
			{
			$req = 1;
			}
		$item =~ s/^\+;.*?;/+/;
		}
	$item =~ s/^\+//;
	#now parse out info
	my ($opt, $type, $depStr) = 
		($item =~ /(\w+)(\:[inwao]?)?(?:\;(.*))?/);
	$type = "b" #boolean--no value
		unless defined($type);
	#type may be empty which means "a"
	$type = ":a"
		if $type eq ":";
	$type =~ s/^://; #take of leading colon
	#store type as lc
	$type = lc($type);
	#now proc $depStr--change all *,+ to && and || and replaced option with defined(opt)
	$depStr = ""
		unless defined($depStr);
	if(!defined($optInfo->{$opt}))
		{
		#store hash with info about this option (required?  type?  dependency string?)
		%{$optInfo->{$opt}} = (req => $req, type => $type, depStr => $depStr);
		}
	else
		{
		print STDERR "ERROR: parseMod_getArgs(): option [$opt] redefined\n";
		}
	}
}


#takes a boolean expression on a set of keys in a hashref $opts and changes 
#from x*y+z -> defined($opts->{"x"}) && defined($opts->{"y"}) || defined($opts->{"z"})
sub depStrToExpr($) #($depStr)
{
my ($depStr) = @_;
#first replace *,+ with &&,||
$depStr =~ s/\*/ \&\& /g;
$depStr =~ s/\+/ \|\| /g;
#we assume a hash reference $opts exists when this string is eval'd
$depStr =~ s/([a-zA-z]+)/defined\(\$opts->\{"$1"\}\)/g;		
return($depStr);
}

#given a processed arg hash %optInfo (data struct defining opts/etc) and actual
#passed in options + values, checks for validity
sub checkOpts(\%\%$) #(%opts, %optInfo, $usageStr)
{
my ($opts, $optInfo, $usageStr) = @_;
my $opt;
#foreach possible option
foreach $opt (keys(%$optInfo))
	{
	#check if required and if so, if defined
	if(eval(depStrToExpr($optInfo->{$opt}{req})) && !exists($opts->{$opt}))
		{
		print $usageStr;
		print 
			"WARNING: given specified options/arguments,\noption [$opt] is required and not specified\n";
		return(0);
		}
	#else, if this option is present (required or not), check it
	if(defined($opts->{$opt}))
		{
		my $error = 0;
		#1st check type
		if(($optInfo->{$opt}{type} eq "i") &&  !($opts->{$opt} =~ /^\-?\d+$/))
			{
			print $usageStr;
			#required type is int and actual type does not match
			print "option [$opt] must be of type integer\n";
			$error = 1;
			}
		elsif(($optInfo->{$opt}{type} eq "p") &&  !($opts->{$opt} =~ /^\d+$/))
			{
			print $usageStr;
			#required type is int and actual type does not match
			print "option [$opt] must be of type integer\n";
			$error = 1;
			}
		elsif(($optInfo->{$opt}{type} eq "n") &&  
			  !($opts->{$opt} =~ /(^\d+\.?\d*$)|(^\d*.?\d+$)/))
			{
			print $usageStr;
			#required type is number and actual type does not match
			print "option [$opt] must be of type number (int or float)\n";
			$error = 1;
			}
		elsif(($optInfo->{$opt}{type} eq "w") &&  
			  !($opts->{$opt} =~ /^[a-zA-Z_\-]+$/))
			{
			print $usageStr;
			#required type is word and actual type does not match
			print "option [$opt] must be of type word [a-zA-z_-]\n";
			$error = 1;
			}
		#now check dependencies
		if(($optInfo->{$opt}{depStr} ne "") && !eval(depStrToExpr($optInfo->{$opt}{depStr})))
			{
			print $usageStr;
			print "option [$opt] fails dependencies\n";
			my %h = ($opt => $optInfo->{$opt});
			print dispOptInfoHash(%h);
			$error = 1;
			}
		return(0)
			if $error;
		}
	}
#if we make it here, everything is okay
return(1);
}

#prints out option values
sub dispParams(\%\@) #(%opts)
{
my ($opts, $nonOpts) = @_;
my $opt;
print "-" x 79 . "\n";
print "options specified:\n";
foreach $opt (keys %$opts)
	{
	print "$opt -> [$opts->{$opt}]\n";
	}
print "\n" . "-" x 79 . "\n";
print "non-options:\n";
print join("\n", @$nonOpts), "\n";
print "-" x 79 . "\n\n";
}

#given an option arg string, print out english readable version of it
sub dispOptInfo($) #($pStr)
{
my ($pStr) = @_;
my %optInfo;
procArgStr($pStr, %optInfo);
return(dispOptInfoHash(%optInfo));
}

#display english readable verison of option info hash
sub dispOptInfoHash(\%) #(%optInfo)
{
my ($optInfo) = @_;
my $retStr = "";
my $opt;
foreach $opt (keys(%$optInfo))
	{
	$retStr .=  "option [$opt]\n" . 
				"\trequired: [$optInfo->{$opt}{req}]\n" .
				"\ttype:     [$optInfo->{$opt}{type}]\n" .
				"\tdepStr:   [$optInfo->{$opt}{depStr}]\n";
	
	}
return($retStr);
}


#load genbank gi to ti map
sub parseMod_loadGBtaxFile($\%) #($infile, %gi2ti)
{
my ($infile, $gi2ti) = @_;
local(*INF);

genMod::genMod_openFile(*INF, $infile) or die;
my $line;
while (defined($line = <INF>))
	{
	chomp $line;
	next if $line =~ /^\s*$/;
	my ($gi,$ti) = split(/\t/, $line); 
	#${%$gi2ti}{$gi} = $ti; 
	$gi2ti->{$gi} = $ti; 
	}
close(INF);
}

#load tax Id -> tax name map
sub parseMod_loadTax2NameFile($\%\%) #($infile, %tax2name, %name2tax)
{
my ($infile, $tax2name, $name2tax) = @_;
local(*INF);

genMod::genMod_openFile(*INF, $infile) or die;
my $line;
while (defined($line = <INF>))
	{
	chomp $line;
	next if $line =~ /^\s*$/;
	if($line =~ /scientific name/i)
		{
		my ($id, $name) = split(/\s*\|\s*/, $line); 
		#${%$tax2name}{$id} = $name;
		#${%$name2tax}{$name} = $id;	
        $tax2name->{$id} = $name;
        $name2tax->{$name} = $id;
		}
	}
close(INF);
}


#load swissprot acc -> taxId map file
sub parseMod_loadSPtaxFile($\%) #($infile, %sp2ti)
{
my ($infile, $sp2ti) = @_;
local(*INF);
genMod::genMod_openFile(*INF, $infile) or die;
my $line;
while(defined($line = <INF>))
	{
	chomp $line;
	next if $line =~ /^\s*$/;
	my ($sp, $ti) = split(/\t/, $line);
	#${%$sp2ti}{$sp} = $ti;
    $sp2ti->{$sp} = $ti;
	}
}

#given a full path to a file (ex: /home/rash/perl/script.pl), extracts the file name
#if it exists (script.pl) or last directory as in /a/b/c/ gives c
sub parseMod_getFName($) #($path)
{
my ($path) = @_;
my ($ret) = ($path =~ /.*\/(.+?)(?:\/)?$/);
#in case $path is just a file
$ret = $path
	unless defined($ret);
return($ret);
}

#given a full/relative path to a file, returns the directory portion.  returns "."
#if only a "filename" is present
sub getDirName($) #($path)
{
my ($path) = @_;
my ($dir, $file, $slash) = ($path =~ /^(.*\/)(.+?)(\/)?$/);
if(defined($slash))
	{
	#input is of form /a/b/c/ so there is no file part
	$dir = $path;
	}
elsif(!defined($file) && ($path =~ /\/$/))
	{
	$dir = $path;
	}
elsif(!defined($file))
	{
	#input is just a filename� so dir is .
	$dir = ".";
	}
return($dir);
}



#compute $hitTaxId, $hitTaxName, and filter $hitDesc if need be
sub procItems($$$$$\%\%\%\%) #$parseDb, $nameDb, $taxCode, $hitDesc, $defLine, %gi2ti, %sp2ti, 
						   #%name2tax, %tax2name)
{
my ($parseDb, $nameDb, $taxCode, $hitDesc, $defLine, $gi2ti, $sp2ti, $name2tax, 
	$tax2name) = @_;
my ($hitTaxId, $hitTaxName) = (0,"no tax name");
$parseDb = lc($parseDb);
###first take care of defline-induced data; then process as normal due to precedence
#(parseDb will override later fields, so they become defaults if parseDb isn't anything
#useful
if($parseDb eq "defline")
	{
	#case for where $defLine has a custom set of data appended of the format
	#>.......|parsedb=...|taxid=...|taxname=...|namedb=...$
	#process and remove
	if($defLine =~ s/\|parsedb=(.*?)\|taxid=(.*?)\|taxname=(.*?)\|namedb=(.*)//i)
		{
		my ($pdb, $tid, $tname, $ndb) = ($1, $2, $3, $4);
		if($pdb ne "")
			{
			$nameDb = $parseDb = $pdb;
			}
		else
			{
			#set to "none" so that no parseDb processing takes place below
			$nameDb = $parseDb = "none";
			}
		if($tid ne "")
			{
			$hitTaxId = $tid;
			}
		if($tname ne "")
			{
			$hitTaxName = $tname;
			}
		if(defined($ndb) && $ndb ne "")
			{
			$nameDb = $ndb;
			}
		$hitTaxId = $gi2ti->{$taxCode} 
			if defined($gi2ti->{$taxCode});
		$hitTaxName = $tax2name->{$hitTaxId} 
			if defined($tax2name->{$hitTaxId});
		}
	else
		{
		#should be fatal?
		die "ERROR: parseDb=[defline] and defline [$defLine] does not conform!\n";
		}
	}
if($parseDb =~ /jgi_(\d+)$/)
	{
	#user has specified taxid at command line
	$hitTaxId = $1;
	$hitTaxName = "jgi tax id [$hitTaxId]";
	$hitTaxName = $tax2name->{$hitTaxId}
		if defined($tax2name->{$hitTaxId});
	
	}
elsif ($parseDb eq "nr" || $parseDb eq "swissprot.ncbi") 
	{ 
	$hitTaxId = $gi2ti->{$taxCode} 
		if defined($gi2ti->{$taxCode});
	$hitTaxName = $tax2name->{$hitTaxId} 
		if defined($tax2name->{$hitTaxId});

	if($hitDesc =~ /\[([^\[\]]*)\][^\[\]]*$/) 
		{
		my $t=$1;
		
		$hitDesc =~ s/\s+\[.*?\]\s*$//;
			#if ($name2tax->{$t});  
		if ($t =~ /Homo sapiens/ && $hitTaxId == 0) 
			{
			#in the current NR, quite a few gi's are not in the tax list 
			#most of which are human, so that is what this little extra is for.	
			$hitTaxId = 9606;
			$hitTaxName = "Homo sapiens";
			}	
		}
	if ($hitDesc =~ /\(([^\(\)]*)\)[^\(\)]*$/ ) 
		{
		my $t = $1;
		$hitDesc =~ s/\s+\($t\)\s*// 
			if ($name2tax->{$1});
		}
	}
elsif($parseDb eq "swissprot")
	{
	$hitTaxId =  $sp2ti->{$taxCode}
		if defined($sp2ti->{$taxCode});
	$hitTaxName = $tax2name->{$hitTaxId} 
		if defined($tax2name->{$hitTaxId});	
	}
elsif($parseDb eq "ipi")
	{
	($hitTaxId) = ($defLine =~ /Tax_Id=(\d+)/);
	$hitTaxName = $tax2name->{$hitTaxId};
	}
elsif($parseDb eq "dm")
	{
	$hitTaxId = 7227;
	$hitTaxName ="Drosophila melanogaster ";
	}
elsif($parseDb eq "ce")
	{
	$hitTaxId = 6239;
	$hitTaxName = "Caenorhabditis elegans";
	}
elsif($parseDb eq "sc")
	{
	$hitTaxId = 4932;
	$hitTaxName ="Saccharomyces cerevisiae ";
	}
elsif($parseDb eq "at")
	{
	$hitTaxId = 3702;
	$hitTaxName ="Arabidopsis thaliana";
	}
elsif($parseDb eq "spombe")
	{
	$hitTaxId = 4896;
	$hitTaxName ="Schizosaccharomyces pombe";
	}
elsif($parseDb eq "ciona")
	{
	$hitTaxId = 7719;
	$hitTaxName = "Ciona intestinalis";
	}
elsif($parseDb =~ /^custom_/i)
	{
	$defLine =~ s/taxid\s*:\s*(\d+)//i;
	$hitTaxId = $1;
	$defLine =~ s/taxname\s*:\s*\[(.*?)\]//i;
	$hitTaxName = $1;
	if(!defined($hitTaxId))
		{
		$hitTaxId = 0;
		print "missing taxid in defline [$defLine]\n";
		}
	if(!defined($hitTaxName))
		{
		$hitTaxName = "none";
		print "missing taxName in defline [$defLine]\n";
		}
	}
elsif($parseDb =~ /^taxid_(\d+)/i)
	{
	#user has specified taxid
	$hitTaxId = $1;
	$hitTaxName = "custom tax id [$hitTaxId]";
	$hitTaxName = $tax2name->{$hitTaxId}
		if defined($tax2name->{$hitTaxId});
	}
elsif($parseDb eq "none")
	{
	#nothing to do
	}
else
	{
	die "fatal error: invalid search database [$parseDb] defline [$defLine]\n";
	}
return($hitTaxId, $hitTaxName, $hitDesc, $defLine, $parseDb, $nameDb);
}

sub alignToInsDel($$$$) #($qAlign, $hAlign, $qStart, $hStart)
{
my ($qAlign, $hAlign, $qStart, $hStart) = @_;
my $qPos = $qStart;
my $hPos = $hStart;
my ($qIns, $qDel, $hIns, $hDel) = ("","","","");
my ($inQgap, $qSize, $inHgap, $hSize) = (0,0,0,0);
my ($qc, $hc);
warn "WARNING: strings of unequal length\nmAlign[$qAlign]\nhAlign[$hAlign]\n"
	if length($qAlign) != length($hAlign);
my $i;
for ($i = 0; $i <= length($qAlign); $i++)
	{
	$qc = substr($qAlign, $i, 1);
	$hc = substr($hAlign, $i, 1);
	#proc model align char
	if ($qc eq "-" && $inQgap == 0)
		{
		#start of a new gap in model 
		$qIns .= "$qPos:";
		$hDel .= "$hPos:";
		$inQgap = 1;
		$qSize = 1;
		}
	elsif($qc ne "-" && $inQgap == 0)
		{
		#update position in model
		$qPos++;
		}
	elsif($qc eq "-" && $inQgap == 1)
		{
		#update gap size
		$qSize++;
		}
	elsif($qc ne "-" && $inQgap == 1)
		{
		#end of model gap
		$qIns .= "$qSize,";
		$qSize = 0;
		$hDel .= "$hPos,";
		$inQgap = 0;
		}
	#proc hit align char
	if($hc eq "-" && $inHgap == 0)
		{
		#start of a new gap in hit (hit: ins, model: del
		$hIns .= "$hPos:";
		$qDel .= "$qPos:";
		$inHgap = 1;
		$hSize = 1;
		}
	elsif($hc ne "-" && $inHgap == 0)
		{
		#update position in model
		$hPos++;
		}
	elsif($hc eq "-" && $inHgap == 1)
		{
		#update gap size
		$hSize++;
		}
	elsif($hc ne "-" && $inHgap == 1)
		{
		#end of hit gap
		$hIns .= "$hSize,";
		$hSize = 0;
		$qDel .= "$qPos,";
		$inHgap = 0;
		}
	}
return($qIns, $qDel, $hIns, $hDel);
}

#look at a defline to see if it's JGI format to get just proteinId
#defline may or may not have the >.  regex deals with it
sub extractProtId($) #($defLine)
{
my ($defLine) = @_;
my $id = $defLine;
if($defLine =~ /\s*>?\s*jgi\|.*?\|(\d+)(?:$|\||\s+)/)
	{
	#just get proteinId;
	$id = $1;
	}
return($id);
}


#function takes a string in and escapes $c by putting a \ in front of any occurrence
#that has no \ before it or an even number of \'s (assuming the string will be interpreted
#only one time in the context of $c$str$c.
#may change \ to any single char if desired
sub escapeString($$;$) #($str, $c, $ec)
{
my ($str, $c, $ec) = @_;
$ec = genMod::defVal("\\", $ec);
my $clen = length($c);
my $slen = length($str);
if($clen != 1)
	{
	$@ = "ERROR:parseMod::escapeString(): \$c must be a single char!\n";
	return(undef);
	}
my $escCnt = 0;
my $newStr = "";
for(my $i=0; $i < $slen; $i++)
	{
	my $wc = substr($str, $i, 1);
	if($wc eq $ec)
		{
		$escCnt++;
		}
	else
		{
		if($wc eq $c && ($escCnt % 2 == 0))
			{
			$wc .= $ec
			}
		$escCnt = 0;
		}
	$newStr .= $wc;
	}
return($newStr);
}

}#end parseMod 
###########################

return 1;
