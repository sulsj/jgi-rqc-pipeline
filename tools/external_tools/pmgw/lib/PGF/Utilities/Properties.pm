package PGF::Utilities::Properties;

=head1 NAME

PGF::Utilities::Properties.pm

=head1 SYNOPSIS

use PGF::Utilities::Properties;

# use default config file
my %params = ( -configFile=>'myconfigfile');

# OR
#  %params = ( -environment=>'MY_CONFIG' )
#  # use this to specify config file in environment variable w/ key=MY_CONFIG

my $obj = PGF::Utilities::Properties->new(%params);
my $value = $obj->getProperty("myProperty");
...

=head1 DESCRIPTION

This module reads a config file containing one or more key=value pairs, one per
line and returns the value based on the specified key.  There are three ways to
specify the config file:

1. By default, the config file in ../../../config/polisher.config is used.
2. If environment variable POLISHER_CONFIG is set to the config file, then that
   file is used.
3. Pass in the config file as an argument to the constructor when instantiating
   the object.

If the specified key does not exist, an empty string is returned.

=head1 VERSION

$Revision: 1.1.1.1 $

$Date: 2010-12-09 22:34:29 $

=head1 AUTHOR(S)

Stephan Trong

=head1 HISTORY

=over 4

=item *

S.Trong 2008/08/13 Creation

=back

=cut

use strict;
use warnings;
use Carp;

#============================================================================#
sub new {
    
    my $class = shift;
    my %params = @_;
        
    # params must be one of the following:
    # -configFile = name of config file.
    # -environment = key of environment variable to specify config file
    #
    
    my @configFiles = ();
    
    if ( defined $params{-configFile} ) {
        push @configFiles, $params{-configFile};
    } elsif ( defined $params{-configFiles} ) {
        @configFiles = @{$params{-configFiles}};
    } elsif ( defined $params{-environment} ) {
        my $envKey = $params{-environment};
        confess "ERROR: environment variable $envKey is not defined for config file."
            if !defined $ENV{$envKey};
        push @configFiles, $ENV{$envKey};
    } else {
        confess "Need to specify either -configFile, -configFiles or -environment.";
    }
    
    foreach my $configFile ( @configFiles ) {
        confess "ERROR: can't find $configFile.\n" if !-e $configFile;
    }
        
    my $self = {};

    # Get config file data and store in object's attribute
    #
    $self->{_configData} = _getConfigData(@configFiles);
    
    # Throw exception if no data is found.
    #
    if ( !defined $self->{_configData} ) {
        confess "ERROR: failed to parse entries from config file(s).\n";
    }
    
    bless $self, $class;

    return $self;

}

#============================================================================#
sub _getConfigData {

    my @configFiles = @_;
    
    my %configs = ();
    
    foreach my $configFile (@configFiles) {
        open IFILE, $configFile or
            confess "ERROR: failed to open db config file $configFile: $!\n";
            
        while( my $line = <IFILE> ) {
            chomp $line;
    
            if ( $line !~/^#/ && $line =~/^([^\=]+)=(.+)\s*/ && !defined $configs{$1} ) {
                $configs{$1} = $2;
            }
        }
        close IFILE;
    }
    
    return \%configs;
    
}

#============================================================================#
sub setExceptionIfEntryNotFound {
    
    my $self = shift;
    my $value = shift || 0;
    
    $self->{_throwExceptionIfEntryNotFound} = $value;
    
}
    
#============================================================================#
sub getProperty {
    
    my $self = shift;
    my $key = shift;
    
    if ( defined $self->{_configData}{$key} ) {
        return $self->{_configData}{$key};
    } elsif ( $self->{_throwExceptionIfEntryNotFound} ) {
        confess "ERROR: Property entry '$key' is not defined in config file.\n";
    } else {
        return '';
    }

}
  
#============================================================================#

1;
