package PGF::Utilities::Logger;

=head1 NAME

PGF::Utilities::Logger.pm

=head1 SYNOPSIS

use PGF::Utilities::Logger;

my $objLogger = PGF::Utilities::Logger->new();
$objLogger->setLogOutFile($file); # sets log file for stdout
$objLogger->setLogErrorFile($file); # sets log file for stderr
$objLogger->setLogOutFileAppend($file); # appends stdout to log file
$objLogger->setLogErrorFileAppend($file); # appends stderr to log file
$objLogger->logOut("my stdout output"); # appends to log.out file
$objLogger->logError("my stderr output"); # appends to log.err file

=head1 DESCRIPTION

This utility appends an error message specified by the user to a log file.  The
log file named log.err is created in the data directory specified by the config
file key=dataDirectory.

=head1 VERSION

$Revision: 1.1.1.1 $

$Date: 2010-12-09 22:34:29 $

=head1 AUTHOR(S)

Stephan Trong

=head1 HISTORY

=over 4

=item *

S.Trong 2008/08/13 Creation

=item *

=back

=cut

use strict;
use warnings;
use Cwd;
use File::Basename;
use File::Path;
use Carp;

#============================================================================#
sub new {
    
    my $class = shift;
    my %params = @_;
    
    my $self = {};
    
    $self->{_logOutFile} = '';
    $self->{_logErrorFile} = '';
    $self->{_appendToLogOutFile} = 0;
    $self->{_appendToLogErrorFile} = 0;
    
    bless $self, $class;

    return $self;

}

#============================================================================#
sub setLogOutFile {
    
    my $self = shift;
    $self->{_logOutFile} = shift;
}

#============================================================================#
sub setLogErrorFile {
    
    my $self = shift;
    $self->{_logErrorFile} = shift;
}

#============================================================================#
sub setLogOutFileAppend {
    
    my $self = shift;
    my $file = shift;
    
    $self->{_appendToLogOutFile} = 1;
    $self->setLogOutFile($file);
    
}

#============================================================================#
sub setLogErrorFileAppend {
    
    my $self = shift;
    my $file = shift;
    
    $self->{_appendToLogErrorFile} = 1;
    $self->setLogErrorFile($file);
    
}

#============================================================================#
sub logOut {

    my $self = shift;
    my $message = shift;
    
    my ($package, $filename, $line) = caller;
    my $append = $self->{_appendToLogOutFile} ? ">" : "";
    my $logFile = $self->{_logOutFile};
    
    checkFilePath($logFile);
    
    open OFILE, "$append>$logFile" or
        confess "ERROR: failed to create log file $logFile: $!\n";
        
    print OFILE "DATE: ",getCurrentDateAndTime(), "\n";
    print OFILE "$message\n";
    print OFILE "\n";
    
    close OFILE;
    
}

#============================================================================#
sub logError {

    my $self = shift;
    my $message = shift;
    
    my ($package, $filename, $line) = caller;
    my $append = $self->{_appendToLogErrorFile} ? ">" : "";
    my $logFile = $self->{_logErrorFile};
    
    checkFilePath($logFile);
    
    open OFILE, "$append>$logFile" or
        confess "ERROR: failed to create log file $logFile: $!\n";
        
    print OFILE "DATE: ", getCurrentDateAndTime(), "\n";
    print OFILE "ERROR in $filename, line $line:\n\n";
    print OFILE $message;
    print OFILE "\n";
    
    close OFILE;
    
}

#============================================================================#
sub checkFilePath {
    
    my $logFile = shift;
    
    my $dirname = dirname($logFile);
    
    if ( !-e $dirname) {
        mkpath($dirname) or
            confess "ERROR: failed to create directory $dirname for log file $logFile: $!\n";
    }
}
    
#============================================================================#
sub getCurrentDateAndTime {

# Returns current date and time in format:
# 'MM-DD-YYYY HH24:MI:SS'

    my ($sec,$min,$hour,$day,$mon,$year) = localtime(time);

    $mon++;
    $year+=1900;

    my $time = sprintf( "%02d/%02d/%04d %02d:%02d:%02d", 
        $mon,$day,$year,$hour,$min,$sec );

    return $time;

}
    
#============================================================================#

1;
