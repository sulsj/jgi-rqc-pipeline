package PGF::Newbler::ParseNewblerMetricsFile;

=head1 NAME

PGF::GapResolution::ParseNewblerMetricsFile

=head1 SYNOPSIS

use PGF::GapResolution::ParseNewblerMetricsFile;
my $metricsFile = shift @ARGV;
my $metricsObj = PGF::GapResolution::ParseNewblerMetricsFile->new("$metricsFile");


#
#INPUTS TO NEWBLER
#
my @inputs = $metricsObj->getInputData();              #returns all inputs 
my @Stdinputs = $metricsObj->getInputData("standard"); #returns unpaired inputs only
my @Prdinputs = $metricsObj->getInputData("paired");   #returns paired inputs only

return format:
type=<pathtofile>;metric=value;metric2=value ...;

example output:
standard=/psf/project/fungi6/4086258/edit_dir.22May09.QB/qb.jt15.4086258_fasta.newbler;numberOfReads=53408,53408;numberOfBases=46054288,40282491;
paired=/psf/traces/fungi/4086258/sff/11483.1.TCAG.2009_10_20_01_08_50.F4CAF9I01.sff;numberOfReads=337120,540579;numberOfBases=119537668,108143714;numWithPairedRead=204150;



#
#RUN METRICS
#
my %metrics = $metricsObj->getRunMetrics();             #returns all metrics
my $stat = $metricsObj->getRunMetrics("seedHitsFound"); #returns specific metric

To return a specific metric pass the metric name as it is seen in 454NewblerMetrics.txt to the function.

example output:
numberSearches 1089668
overlapsFound 82819133,76.00,20.26%
overlapsReported 62634951,57.48,75.63%
overlapsUsed 6384449,5.86,10.19%
seedHitsFound 408784547,375.15
totalNumberOfBases 2190752221
totalNumberOfReads 7365253

example of specific output (seedHitsFound):
408784547,375.15



#
#ALIGNMENT RESULTS
#
my @readAlignmentResults = $metricsObj->getReadAlignmentResults(); #returns all read alignment results
my @stdResults = $metricsObj->getReadAlignmentResults('standard'); #return only unpaired
my @prdResults = $metricsObj->getReadAlignmentResults('paired');   #return only paired

output format:
type=<pathtofile>;metric=value;metric2=value ...;

example output:
standard=/psf/project/fungi6/4086258/edit_dir.22May09.QB/qb.jt15.4086258_fasta.newbler;numAlignedReads=53031,99.29%;numAlignedBases=39949246,99.17%;inferredReadError=0.30%,119199;
paired=/psf/traces/fungi/4086258/sff/11483.1.TCAG.2009_10_20_01_08_50.F4CAF9I01.sff;numAlignedReads=502302,92.92%;numAlignedBases=104652393,96.77%;inferredReadError=0.71%,740019;numberWithBothMapped=169642;numWithOneUnmapped=6923;numWithMultiplyMapped=27522;numWithBothUnmapped=63;



#
#ALIGNMENT DEPTHS
#
my @depths = $metricsObj->getAlignmentDepths("depths");              #return alignment depths histo
my $peakDepth = $metricsObj->getAlignmentDepths("peakDepth");        #return peak alignment depth
my $estSize = $metricsObj->getAlignmentDepths("estimatedGenomeSize");#return estimated genome size
my @allDepthInfo = $metricsObj->getAlignmentDepths();                #return all of the above in order

example all output:
1 106036
2 59203
3-4 70211
5-6 59731
7-8 55338
9-10 52841
11-13 92112
14-16 122005
17-19 177127
20-22 256701
23-25 348656
26-28 471312
29-31 676111
32-34 1028294
35-38 2187493
39-42 3338308
43-46 4426471
47-50 5041001
51-55 6133009
56-60 5107669
61-70 6391403
71-80 2991431
81-90 1469312
91-100 760631
101-140 881565
141-180 303079
181-240 210384
241-300 50510
301+ 97510
50.0
43.8MB



#
#CONSENSUS RESULTS
#

#
#read status
my %readStatus = $metricsObj->getConsensusResults("standard");                              #return unpaired read stats
my $readStatusX = $metricsObj->getConsensusResults("standard", "numberRepeat");             #return specific unpaired metric
my %pairedReadStatus = $metricsObj->getConsensusResults("paired");                          #return paired read stats
my $pairedReadStatusX = $metricsObj->getConsensusResults("paired", "numberWithBothMapped"); #return specific paired metric
my @pairedInsertInfo = $metricsObj->getConsensusResults("insertSize");                      #return all paired insert info

example unpaired stats output:
inferredReadError 0.62%,13311098
numAlignedBases 2142000342,97.77%
numAlignedReads 7085984,96.21%
numberAssembled 6736562
numberOutlier 35717
numberPartial 349422
numberRepeat 157684
numberSingleton 85868
numberTooShort 0

example specific unpaired metric output (numberRepeat):
157684

example paired stats output:
numberMultiplyMapped 140636
numberWithBothMapped 969665
numberWithBothUnmapped 4870
numberWithOneUnmapped 29853

example specific paired metric output (numberWithBothUnmapped):
969665

example paired insert info:
GBSF;pairDistanceAvg=38627.3;pairDistanceDev=9656.8;
11483.1.TCAG.2009_10_20_01_08_50.F4CAF9I01.sff;pairDistanceAvg=2955.8;pairDistanceDev=739.0;



#
#assembly metrics
my %scaffMetrics       = $metricsObj->getConsensusResults("scaffoldMetrics");     #return all scaffold metrics
my %largeContigMetrics = $metricsObj->getConsensusResults("largeContigMetrics");  #return all large contig metrics
my %allContigMetrics   = $metricsObj->getConsensusResults("allContigMetrics");    #return all all-contig metrics
my $scaff= $metricsObj->getConsensusResults("scaffoldMetrics","N50ScaffoldSize"); #return specific scaff metric
my $large= $metricsObj->getConsensusResults("largeContigMetrics","N50ContigSize");#return specific large metric
my $all= $metricsObj->getConsensusResults("allContigMetrics","numberOfBases");    #return specific all metric

example scaff output:
N50ScaffoldSize 1756547
avgScaffoldSize 52841
largestScaffoldSize 3737524
numberOfScaffolds 740

example large contig output:
N50ContigSize 153833
Q39MinusBases 76793,0.20%
Q40PlusBases 37785107,99.80%
avgContigSize 28968
largestContigSize 641966
numberOfBases 37861900
numberOfContigs 1307

example all contig output
numberOfBases 37917680
numberOfContigs 1644

example specific output
1756547   #N50ScaffoldSize
153833    #N50ContigSize
37917680  #numberOfBases




=head1 DESCRIPTION
This module parses a Newbler assemblies 454NewblewMetrics.txt file.  It has the ability to return all information in the metrics file in some way, but one must pay attention to the type of data structure each function returns and what output it returns.  See above for details and examples.  There is currently very little error handling (to do)


=head1 AUTHOR(s)

Kurt M. LaButti

=head1 HISTORY

=over

=item *

Kurt M. LaButti 27April2010 Creation

=back

=cut

#============================================================================#
use strict;
use Carp;
use Carp qw(cluck);
use FindBin qw($RealBin);
use lib "$FindBin::RealBin/../lib";


my $_DEBUG = 0;
                                             
#============================================================================#
sub new {
    
    my $class = shift;
    my $newblerMetricsFile = shift;
    
    if ( !-s $newblerMetricsFile ) {
        confess "ERROR: file $newblerMetricsFile does not exists or is zero size.\n";
    }
    
    my $self = {};
    
    bless $self, $class;

    $self->_parseFile($newblerMetricsFile);
    
    return $self;

}

#============================================================================#
sub _parseFile {

    my $self = shift;    
    my $file = shift;
#    my %hoa;
#    my %metrics;
#    my @array;
    my %hash;
# %hash = 
#       {input}{BLOCK_NAME} = @info
#
#
#
#       {metrics}{BLOCK_NAME}{metric} = value
#

        
    
    unless (open FH, $file ) {
        confess "ERROR: failed to open file $file: $!";
    }
    
    my $blockCt = 0;    
    my $ScndBlockCt = 0;
    my $blockName = '';
    my $secondaryBlockName = '';
    my $key;
    my $value;
    my $info;
    
    while (my $line = <FH>) {
	chomp $line;
	next if !length $line;


	
	# look for block name
	if ( $line =~ /^(\w+)$/ ) {
	    $blockName = $1;
	    #print STDERR "BLOCK $blockName\n";
	    next;
	} elsif ( $line =~ /^\s*(\w+)$/ ) {
	    $secondaryBlockName = $1;
	  #  #print STDERR "2ndBLOCK $secondaryBlockName\n";
	    next;
	    # look for start of block '{'
	} elsif ( $line =~ /\{$/ ) {
	    $blockCt++;
	    if ($secondaryBlockName) { $ScndBlockCt++;}
	  #  #print STDERR "START==\n";
	    next;
	    # look for end of block '}'
	} elsif ( $line =~ /\}$/ ) {
#	    #print STDERR "END==\n";
	    $blockCt--;
	    if ($ScndBlockCt > 0){
		$ScndBlockCt--;
		$secondaryBlockName = "";
	    }
	    
	    next;
	}
	
	


#
#HANDLE TOP LEVEL BLOCKS
#
	if ($blockCt > 0 && $ScndBlockCt == 0) {

	    #handle run metrics
	    if ($blockName =~ /runMetrics/) {
		$line =~ s/\s+//g; $line =~ s/\;//g;
		($key, $value) = split /\=/, $line;
		#print STDERR "\t$key=$value\n";
		$hash{'metrics'}{$blockName}{$key} = $value;		
		$info = <FH>;
		chomp $info;
		$info =~ s/\s+//g; $info =~ s/\;//g;
		($key, $value) = split /\=/, $info;
		#print STDERR "\t$key=$value\n";
		$hash{'metrics'}{$blockName}{$key} = $value;		
		<FH>;
		
		for (1..5) {
		    $info = <FH>;
		    chomp $info;
		    $info =~ s/\s+//g; $info =~ s/\;//g;
		    ($key, $value) = split /\=/, $info;
		    #print STDERR "\t$key=$value\n";
		    $hash{'metrics'}{$blockName}{$key} = $value;		
		}	
		
		#handle alignment depths
	    } elsif ($blockName =~ /alignmentDepths/){
		$line =~ s/\s+//g; $line =~ s/\;//g;
		($key, $value) = split /\=/, $line;
		#print STDERR"\t$key=$value\n";
		#$hash{'metrics'}{$blockName}{$key} = $value;
	
		push(@{$hash{'metrics'}{$blockName}}, "$key $value");
	
#		for (1..28) {
#		    $info = <FH>;
#		    chomp $info;
#		    $info =~ s/\s+//g; $info =~ s/\;//g;
#		    ($key, $value) = split /\=/, $info;
#		    #print STDERR"\t$key=$value\n";
#		    #$hash{'metrics'}{$blockName}{$key} = $value;	
#		    push(@{$hash{'metrics'}{$blockName}}, "$key $value");	
#		}
#		
#		<FH>;
#		for (1..2) {
#		    $info = <FH>;
#		    chomp $info;
#		    $info =~ s/\s+//g; $info =~ s/\;//g; $info =~ s/\"//g;
#		    ($key, $value) = split /\=/, $info;
#		    chomp $value;
#		    #print STDERR"\t$key=[$value]\n";
#		    #$hash{'metrics'}{$blockName}{$key} = $value;	
#		    push(@{$hash{'metrics'}{$blockName}}, "$value");	
#		}
		
		
	    }
	    
#
#HANDLE SUB BLOCKS
#


	} elsif ($blockCt > 0 && $ScndBlockCt > 0) {
	    	    
	    #handle run data
	    if ($blockName =~ /runData/) {
	        $info =  $line;
		<FH>;
		$info .= <FH>;
		$info .= <FH>;
		chomp $info;
		$info =~ s/\s+//g;
		$info =~ s/\"//g;
		$info =~ s/path\=//g;
		#print STDERR"$blockName $secondaryBlockName [$info]\n";
		push(@{$hash{'input'}{$blockName}}, "standard=$info");
		
#handle paired run data
	    } elsif ($blockName =~ /(pairedReadData|readAlignmentResults)/) {
	        $info =  $line;
		<FH>;
		$info .= <FH>;
		$info .= <FH>;
		$info .= <FH>;
		chomp $info;
		$info =~ s/\s+//g;
		$info =~ s/\"//g;	
		$info =~ s/path\=//g;	
		#print STDERR"$blockName $secondaryBlockName [$info]\n";
		if ($blockName =~ /paired/) {$info = "paired=".$info;} else { $info = "standard=".$info};
		push(@{$hash{'input'}{$blockName}}, "$info");
		
#handle consensus results
	    } elsif ($blockName =~ /consensusResults/ && $secondaryBlockName =~ /readStatus/) {
		$line =~ s/\s+//g;
		$line =~ s/\;//g;
		($key, $value) = split /\=/, $line;
		#print STDERR"\t$secondaryBlockName\t$key=$value\n";
		$hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		for (1..2) {
		    $info = <FH>;
		    chomp $info;
		    $info =~ s/\s+//g; $info =~ s/\;//g;
		    ($key, $value) = split /\=/, $info;
		    #print STDERR"\t$secondaryBlockName\t$key=$value\n";
		    $hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		}
		<FH>;
		for (1..6) {
		    $info = <FH>;
		    chomp $info;
		    $info =~ s/\s+//g; $info =~ s/\;//g;
		    ($key, $value) = split /\=/, $info;
		    #print STDERR"\t$secondaryBlockName\t$key=$value\n";
		    $hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;				
		}
		
	    } elsif ($blockName =~ /consensusResults/ && $secondaryBlockName =~ /pairedReadStatus/) {
		$line =~ s/\s+//g;
		$line =~ s/\;//g;
		($key, $value) = split /\=/, $line;
		#print STDERR"\t$secondaryBlockName\t$key=$value\n";
		$hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		
		for (1..3) {
		    $info = <FH>;
		    chomp $info;
		    $info =~ s/\s+//g; $info =~ s/\;//g;		
		    ($key, $value) = split /\=/, $info;
		    #print STDERR"\t$secondaryBlockName\t$key=$value\n";
		    $hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		}
		
		
	    } elsif ($blockName =~ /consensusResults/ && $secondaryBlockName =~ /library/) {
		$info = $line;
		for (1..2) {
		    $info .= <FH>;
		}
		chomp $info;
		$info =~ s/\s+//g;
		$info =~ s/\"//g;
		$info =~ s/libraryName\=//g;
		$info =~ s/\s+//g;
		push(@{$hash{'input'}{$blockName}}, "$info");
		#print STDERR"$blockName $secondaryBlockName [$info]\n";
	    } elsif ($blockName =~ /consensusResults/ && $secondaryBlockName =~ /scaffoldMetrics/) {
		$line =~ s/\s+//g;
		$line =~ s/\;//g;
		($key, $value) = split /\=/, $line;
		#print STDERR"\t$secondaryBlockName\t$key=$value\n";
		$hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		$info = <FH>;		
		$info =~ s/\s+//g; $info =~ s/\;//g;
		($key, $value) = split /\=/, $line;
		#print STDERR"\t$secondaryBlockName\t$key=$value\n";
		$hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		<FH>;
		for (1..3) {
		    $info = <FH>;
		    chomp $info;
		    $info =~ s/\s+//g; $info =~ s/\;//g;
		    ($key, $value) = split /\=/, $info;
		    #print STDERR"\t$secondaryBlockName\t$key=$value\n";
		    $hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		}
		
	    } elsif ($blockName =~ /consensusResults/ && $secondaryBlockName =~ /largeContigMetrics/) {
		$line =~ s/\s+//g;
		$line =~ s/\;//g;
		($key, $value) = split /\=/, $line;
		#print STDERR"\t$secondaryBlockName\t$key=$value\n";
		$hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		$info = <FH>;		
		chomp $info;
		$info =~ s/\s+//g; $info =~ s/\;//g;
		($key, $value) = split /\=/, $info;
		#print STDERR"\t$secondaryBlockName\t$key=$value\n";
		$hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		<FH>;
		for (1..3) {
		    $info = <FH>;
		    chomp $info;
		    $info =~ s/\s+//g; $info =~ s/\;//g;
		    ($key, $value) = split /\=/, $info;
		    #print STDERR"\t$secondaryBlockName\t$key=$value\n";
		    $hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		}
		<FH>;
		for (1..2) {
		    $info = <FH>;
		    chomp $info;
		    $info =~ s/\s+//g; $info =~ s/\;//g;
		    ($key, $value) = split /\=/, $info;
		    #print STDERR"\t$secondaryBlockName\t$key=$value\n";
		    $hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		}

	    } elsif ($blockName =~ /consensusResults/ && $secondaryBlockName =~ /allContigMetrics/) {
		$line =~ s/\s+//g;
		$line =~ s/\;//g;
		($key, $value) = split /\=/, $line;
		#print STDERR"\t$secondaryBlockName\t$key=$value\n";
		$hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		$info = <FH>;		
		chomp $info;
		$info =~ s/\s+//g; $info =~ s/\;//g;
		($key, $value) = split /\=/, $info;
		#print STDERR"\t$secondaryBlockName\t$key=$value\n";
		$hash{'metrics'}{$blockName}{$secondaryBlockName}{$key} = $value;		
		
	    } elsif ($blockName =~ /pairedReadResults/ && $secondaryBlockName =~ /file/) {
              	 $info =  $line;
		 for (1..9) {
		     $line = <FH>;		 
		     if ($line =~ /\w+/) {
			 chomp $line;
			 $info .= $line;			 
		     } 
		 }
		 chomp $info;
		 $info =~ s/\s+//g;
		 $info =~ s/\"//g;
		 $info =~ s/path\=//g;
		 #print STDERR "$blockName $secondaryBlockName [$info]\n";
		 push(@{$hash{'input'}{$blockName}}, "paired=$info");
	     }
	    
	    
	    
	}
	
	
	
	
	
    }
    
    close FH;
    
    
#stuff both data structures into a single data structure
#    @array = (%hoa, %metrics);   

    $self->{_hash} = \%hash;
#     $self->{_hoa} = \%hoa;
    
}



#============================================================================#
sub getInputData {
    
    my $self = shift;
    my $specific = shift;  #standard, paired, or empty=all
 
    if ($specific && $specific =~ /standard/ && defined  @{$self->{_hash}{'input'}{'runData'}} ) {
	return @{$self->{_hash}{'input'}{'runData'}};
    }
    elsif ($specific && $specific =~ /paired/ && defined @{$self->{_hash}{'input'}{'pairedReadData'}} ) {
	return @{$self->{_hash}{'input'}{'pairedReadData'}};       
    }
    elsif ($specific){
	warn "No $specific data found";
	return ();
    }
    elsif (defined @{$self->{_hash}{'input'}{'runData'}} && defined @{$self->{_hash}{'input'}{'pairedReadData'}} ) {
	return ( @{$self->{_hash}{'input'}{'runData'}} ,@{$self->{_hash}{'input'}{'pairedReadData'}} );       
    }
    elsif (defined @{$self->{_hash}{'input'}{'pairedReadData'}}){
	return (@{$self->{_hash}{'input'}{'pairedReadData'}});
    }
    elsif (defined @{$self->{_hash}{'input'}{'runData'}}){
	return @{$self->{_hash}{'input'}{'runData'}};       
    }
    else{
	warn "No Input Data returned";
	return ();
    }
}


#============================================================================#
sub getRunMetrics {
    
    my $self = shift;
    my $specific = shift; 
    my $blockName = "runMetrics";



    if ($specific) {
	return $self->{_hash}{'metrics'}{$blockName}{$specific};       
    } else {
	return %{$self->{_hash}{'metrics'}{$blockName}};       
    }
}


#============================================================================#
sub getReadAlignmentResults {
    
    my $self = shift;
    my $specific = shift;
    my $blockName = "runMetrics";


    
    if ($specific && $specific =~ /standard/) {
	return @{$self->{_hash}{'input'}{'readAlignmentResults'}};
    } elsif ($specific && $specific =~ /paired/) {
	return @{$self->{_hash}{'input'}{'pairedReadData'}};       
    } else {
	if (defined @{$self->{_hash}{'input'}{'pairedReadResults'}} ) { 
	    return (@{$self->{_hash}{'input'}{'readAlignmentResults'}} ,@{$self->{_hash}{'input'}{'pairedReadResults'}});     
	} else {
	    return @{$self->{_hash}{'input'}{'readAlignmentResults'}};
	}
    }
}


#============================================================================#
sub getAlignmentDepths {
    
    my $self = shift;
    my $specific = shift;
    my $blockName = "alignmentDepths";

 	
    if ($specific && $specific =~ /peakDepth/) {
	return ${$self->{_hash}{'metrics'}{$blockName}}[-2];
    } elsif ($specific && $specific =~ /estimatedGenomeSize/) {
	return ${$self->{_hash}{'metrics'}{$blockName}}[-1];       
    } elsif ($specific && $specific =~ /depths/) {
	my $numEl = $#{$self->{_hash}{'metrics'}{$blockName}};       	
	return @{$self->{_hash}{'metrics'}{$blockName}}[0..($numEl-2)];       
    } else {
	return @{$self->{_hash}{'metrics'}{$blockName}};       
    }
}


sub getConsensusResults {
    
    my $self = shift;
    my $specific = shift;  #standard, paired, or empty=all
    my $secondary = shift;
    my $blockName = "consensusResults";

    if ($specific && $specific =~ /standard/ ) {
	if ($secondary) {
	    return $self->{_hash}{'metrics'}{$blockName}{'readStatus'}{$secondary};
	} else {
	    return %{$self->{_hash}{'metrics'}{$blockName}{'readStatus'}};
	}
    } elsif ($specific && $specific =~ /paired/) {
	if (defined %{$self->{_hash}{'metrics'}{$blockName}{$specific}}) {
	    if ($secondary) {
		return $self->{_hash}{'metrics'}{$blockName}{'pairedReadStatus'}{$secondary};
	    } else {
		return %{$self->{_hash}{'metrics'}{$blockName}{'pairedReadStatus'}};
	    }
	} else { return ("notAvailable" => "n/a");}
    } elsif ($specific && $specific =~ /insertSize/) {

	if (defined @{$self->{_hash}{'input'}{$blockName}} ) {
	    return @{$self->{_hash}{'input'}{$blockName}};  	
	} else { return "notAvailable";}
	
    } elsif ($specific && $specific =~ /scaffoldMetrics/) {
	
	if (defined %{$self->{_hash}{'metrics'}{$blockName}{$specific}}) {
	    if ($secondary) {
		return $self->{_hash}{'metrics'}{$blockName}{$specific}{$secondary};
	    } else {
		return %{$self->{_hash}{'metrics'}{$blockName}{$specific}};
	    }
	} else { return ("notAvailable" => "n/a");}
    } elsif ($specific && $specific =~ /largeContigMetrics/) {
	if ($secondary) {
	    return $self->{_hash}{'metrics'}{$blockName}{$specific}{$secondary};
	} else {
	    return %{$self->{_hash}{'metrics'}{$blockName}{$specific}};
	}
    } elsif ($specific && $specific =~ /allContigMetrics/) {
	if ($secondary) {
	    return $self->{_hash}{'metrics'}{$blockName}{$specific}{$secondary};
	} else {
	    return %{$self->{_hash}{'metrics'}{$blockName}{$specific}};
	}
    } else {

    }
}





1;


