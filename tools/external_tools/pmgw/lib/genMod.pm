use strict;

=pod

This module contains various functions for misc/general purposes.  This includes
formatting data, opening files, deep copies of data, etc.  See functions
for documentation on each


Sam Rash
5-20-2002

=cut

############################
#BEGIN MODULE_NAME
{
#use strict;

#directive stuff
package genMod;
require Exporter;
#add any paths for use/require directives

#any other modules needed
#use attributes;
require loadMod;

#exporting not all that important with objects/classes (I *think*)
our @ISA = qw(Exporter);
#export by default
our @EXPORT = qw(genMod_dcopy genMod_openFile genMod_openDir genMod_readNextLine 
	genMod_printDebug genMod_printDebugLev genMod_checkLastUsageDisp genMod_logUsageCheck 
	genMod_calcDispTime genMod_getFullPath genMod_minVal genMod_maxVal
	genMod_setVal printDebug defVal openFile remComments);
our @EXPORT_OK = qw(DEBUG_LEVEL); #export by request

our $DEBUG_LEVEL = 0; #no debug output by default
our $usageCheckLog = ".rashScriptsUsageLog"; #file that logs when usage was last checked
					#by the user. Useful so a script can check if it's usage may have changed
					#since a user last examined the usage

#symbol list: list reelvant symbols here
our $__done;

sub BEGIN()
{
#METHOD PROTOTYPES - put method prototypes here in order to have
#them executed at the time the module is compiled
sub genMod_dcopy($); #($ref)
sub genMod_openFile(\*$;$); #(*FH, $openString, $failOK)
sub genMod_openDir(\*$;$); #(*FH, $openString, $failOK)
sub genMod_readNextLine(\*;$$\%); #(*FH, $filtComments=0, $func, %data)
sub genMod_printDebugLev($;@); #($level = 1, @list)
sub genMod_printDebug(@); #(@list)
sub printDebug(@); #(@list)
sub genMod_checkLastUsageDisp($$); #($script name, $lastChangeDate)
sub genMod_logUsageCheck($); #($scriptName)
sub genMod_calcDispTime($); #($timeSec)
sub genMod_getFullPath($); #($relPath) 
sub genMod_minVal(\@); #(@list)
sub genMod_maxVal(\@); #(@list)
sub genMod_setVal($$); #($val1, $val2)
sub rollLog($;$$); #($logFile, $maxLogs, $gzip=undef)
sub listFromStr($); #($listStr)
sub strFromList(@); #(@list)
sub macroFind($); #($str)
sub macroExp($\%;$$); #($str, %macros, $leaveUndef=0, $leave2perc=0)
sub colorStr($$\@\@;$); #($str, $color, @starts, @ends, $width=70)
sub widthFormatHtml($;$); #($str, $width=70)
sub jointSort(\@;$); #(@lists, $type=0)
sub arrayIntersection(\@); #(@arrayList)
sub genTempFileName($); #($base)
sub catFile($); #($file)
sub catFileBin($); #($file);
sub between($$$); #($num, $r1, $r2)
sub calcOv($$$$); #($s1, $e1, $s2, $e2);
sub genRandColor(;$); #($minColDist=0)
sub defVal($$); #($def, $check)
sub remComments($); #($str)
sub genMod_resolveLinks($); #($path)

sub loadMod::loadMod_loadHash($$\%);
}

########################

#functions:

#returns a full-blown deep copy of what ref points to;
#ie, returns another reference to deep copy of the object
# OR in the case of a scalar, it simply returns the value
sub genMod_dcopy($) #($ref)
{
my ($ref) = @_;
my $type = ref(\$ref);
#print "type1: [$type]\n";
$type = ref($ref)
	if $type eq "REF";
#print "type2: [$type]\n";
if($type eq "SCALAR")
	{
	return($ref);
	}
elsif($type eq "ARRAY")
	{
	my @retArr; # new array
	my $i;
	for($i=0; $i <= $#{@$ref}; $i++)
		{
		$retArr[$i] = genMod_dcopy(${@$ref}[$i]);
		}
	return(\@retArr);
	}
elsif($type eq "HASH")
	{
	my ($key, $val);
	my %retHash;
	while(($key, $val) = each %$ref)
		{
		$retHash{$key} = genMod_dcopy($val);
		}
	return(\%retHash);
	}
else
	{
	die "unknown ref type [$type]\n";
	}
}

sub openFile(\*$;$) #(*FH, $openStr, $failOk)
{
my ($fhRef, $oString, $failOK) = @_;
$failOK = 0
	unless defined($failOK);
if(!open($fhRef, $oString))
	{
	print STDERR "ERROR: open command [$oString] failed with error [$!]\n"
		unless $failOK == 1;
	return(0);
	}
return(1);
}
#opens a file; upon failure, returns 0.  if failOk is not set, prints an error message
sub genMod_openFile(\*$;$) #(\*FH, $openString, $failOK)
{
my ($fhRef, $oString, $failOK) = @_;
$failOK = 0
	unless defined($failOK);
if(!open($fhRef, $oString))
	{
	print STDERR "ERROR: open command [$oString] failed with error [$!]\n"
		unless $failOK == 1;
	return(0);
	}
return(1);
}

#open a dir; fail messages printed
sub genMod_openDir(\*$;$) #(*FH, $openString, $failOK)
{
my ($fhRef, $oString, $failOk) = @_;
$failOk = genMod::genMod_setVal(0, $failOk);
if(!opendir($fhRef, $oString))
	{
	print STDERR "ERROR: openDir command [$oString] failed with error [$!]\n"
		unless $failOk == 1;
	return(0);
	}
return(1);
}

#given an open file handle, reads the next line of a file or returns 0
#if EOF
sub genMod_readNextLine(\*;$$\%) #(*FH, $filtComments=0, $func, %data)
{
my ($fhRef, $filtComments, $func, $data) = @_;
$filtComments = 0
	unless defined($filtComments);
$data = {} #init to empty
	unless defined($data);
my $line;
while(defined($line = <$fhRef>))
	{
	chomp $line;
	$line =~ s/#.*$//
		if $filtComments;
	next if $line =~ /^\s*$/;
	#apply function to line if passed in
	$line = &{$func}($line, $data)
		if defined($func) && $func != 0;
	last;
	}
#redundant, yes
return(undef)
	unless defined($line);
return($line);
}

#print a debug statement at a given level.  useful for adding debugging statements
#to code and turning sets on/off by setting $genMod::DEBUG_LEVEL
sub genMod_printDebugLev($;@) #($level, @list)
{
my ($level, @list) = @_;
my $string = join("", @list);
$level = 1
	unless defined($level);
if(defined($DEBUG_LEVEL) && ($level <= $DEBUG_LEVEL))
	{
	print $string;
	}
}

#generic debug statement (level 1)
sub genMod_printDebug(@) #(@list)
{
my (@list) = @_;
genMod_printDebugLev(1, @list)
}


sub printDebug(@) #(@list)
{
my (@list) = @_;
genMod_printDebug(@list);
}


#This function checks the file $HOME/$usageCheckLog to see if $scriptName's usage
#has been checked since $lastChangeDate (seconds since epoch).  
#If not, it advises the user to check it.
sub genMod_checkLastUsageDisp($$) #($scriptName, $lastChangeDate)
{
my ($scriptName, $lastChangeDate) = @_;
my %usageCheck; #hash of script names and last usage checks
loadMod::loadMod_loadHash("$ENV{HOME}/$usageCheckLog", ",", %usageCheck)
	if -e "$ENV{HOME}/$usageCheckLog";
if(!defined($usageCheck{$scriptName}) || ($usageCheck{$scriptName} < $lastChangeDate) ||
	!(-e "$ENV{HOME}/$usageCheckLog"))
	{
	print "WARNING:  script and/or script's usage may have changed since you last viewed\n" .
		  "it.  Please run this script without any arguments and verify that you are\n" .
		  "aware of its correct usage.\n\n";
	print "PRESS RETURN/ENTER TO CONTINUE\n";
	my $garbage = <STDIN>;
	}
}

#logs that a user has checked usage of a script in $HOME/$usageCheckLog
sub genMod_logUsageCheck($) #($scriptName)
{
my ($scriptName) = @_;
my %usageCheck; #hash of script names and last usage checks
if(-e "$ENV{HOME}/$usageCheckLog")
	{
	loadMod::loadMod_loadHash("$ENV{HOME}/$usageCheckLog", ",", %usageCheck);
	}
local(*OUTF);
$usageCheck{$scriptName} = time();
genMod_openFile(*OUTF, "> $ENV{HOME}/$usageCheckLog") or die;
my ($key, $val);
while(($key, $val) = each(%usageCheck))
	{
	print OUTF "$key,$val\n";
	}
close(OUTF);
}

#given a number of seconds, calc the number of hrs/min/sec
sub genMod_calcDispTime($)
{
my ($timeSec) = @_;
my ($hours, $min, $sec) = (0,0,0);
$hours = int($timeSec / 3600);
$timeSec -= $hours * 3600;
$min = int($timeSec / 60);
$timeSec -= $min * 60;
$sec = $timeSec;
return($hours, $min, $sec);
}

#given a relative path to a file/directory, find the full path
sub genMod_getFullPath($) #($relPath) 
{
my($relPath) = @_;
return("")
	if !defined($relPath) || $relPath eq "";
#in case this is already an absolute path
return($relPath)
	if($relPath =~ /^\s*\//);
my $cwd = `pwd`;
chomp $cwd;
$cwd =~ s/^\s*\///;
$cwd =~ s/\/+/\//g;
$relPath =~ s/\/+/\//g;
my @dirArr = split("/", $cwd);
my @relArr = split("/", $relPath);
while(@relArr)
	{
	my $dir = shift(@relArr);
	next if $dir eq ".";
	if($dir eq "..")
		{
		pop(@dirArr);
		}
	else
		{
		push(@dirArr, $dir);
		}
	}
return("/" . join("/", @dirArr));
}

#return the min of a set of numbers
sub genMod_minVal(\@) #(@list)
{
my ($list) = @_;
return(undef)
	if @$list == 0;
my $min = $list->[0];
for(my $i = 1; $i < @$list; $i++)
	{
	$min = $list->[$i]
			if($list->[$i] < $min);
	}
return($min);
}

#return the max of a set of numbers
sub genMod_maxVal(\@) #(@list)
{
my ($list) = @_;
return(undef)
	if @$list == 0;
my $max = $list->[0];
for(my $i = 1; $i < @$list; $i++)
	{
	$max = $list->[$i]
			if($list->[$i] > $max);
	}
return($max);
}

#returns $val2 iff $val2 def'd.  else, return $val1
sub genMod_setVal($$) #($val1, $val2)
{
my ($val1, $val2) = @_;
if(defined($val2))
	{
	return($val2);
	}
else
	{
	return($val1);
	}
}

#rolls out existing log to make room for new one (and optionally enforces max #)
sub rollLog($;$$) #($logFile, $maxLogs, $gzip=undef)
{
my ($logFile, $maxLogs, $gzip) = @_;
$maxLogs = genMod_setVal(-1, $maxLogs);
if(-e $logFile)
	{
	my $next = 0;
	if($maxLogs != -1)
		{
		#we are doing true log rolling, so put current log in slot 0 and shift
		#everything else to the right
		for(my $i=$maxLogs-1; $i > 0; $i--)
			{
			my $oldLog = "$logFile." . ($i-1);
			#roll logs $i-1 -> $i
			#deal with case of compressed logs
			if(-e "$oldLog.gz")
				{
				$oldLog .= ".gz";
				}
			my $newLog = "$logFile.$i";
			if($oldLog =~ /\.gz$/)
				{
				$newLog .= ".gz";
				}
			if(-e $oldLog)
				{
				rename($oldLog, $newLog);
				if(defined($gzip) && !($newLog =~ /\.gz$/))
					{
					system("$gzip $newLog");
					}
				}
			}
		}
	#move current file to .0 and gzip
	system("mv $logFile $logFile.$next");
	system("$gzip $logFile.$next")
		if defined($gzip);
	}
}


#given a string of form x,y,z,a-c builds the list (x,y,z,a,b,c) and returns it
sub listFromStr($) #($listStr)
{
my ($listStr) = @_;
my @list = ();
foreach my $item (split(/\s*,\s*/, $listStr))
	{
	$item =~ s/\s*//g;
	if($item =~ /(\d+)-(\d+)/)
		{
		my ($lo, $hi) = ($1, $2);
		for(my $i=$lo; $i <= $hi; $i++)
			{
			push(@list, $i);
			}
		}
	else
		{
		push(@list, $item);
		}
	}
return(@list);
}

#given an array of numbers, creates a minimal string x-y,a,b,c, etc
sub strFromList(@) #(@list)
{
my $str = "";
my ($prev, $start);
my $num;
foreach $num (sort {$a <=> $b} @_)
	{
	if(!defined($start))
		{
		$prev = $start = $num;
		}
	elsif($num > $prev+1)
		{
		$str .= ","
			unless $str eq "";
		if($start != $prev)
			{
			$str .= "$start-$prev";
			}
		else
			{
			$str .= "$start";
			}
		$prev = $start = $num;
		}
	else
		{
		$prev = $num;
		}
	}
$str .= ","
unless $str eq "";
if($start != $prev)
	{
	$str .= "$start-$prev";
	}
else
	{
	$str .= "$start";
	}
return($str);
}

#returns a list of macros found
sub macroFind($) #($str)
{
my ($str) = @_;
my ($pref, $mac, $suf);
my @list = ();
my %names = ();
while(($pref, $mac, $suf) = ($str =~ /(^|(?:^.*?[^%]))%([^%]*?)%($|(?:.*$))/gs))
	{
	if(defined($mac) && ($mac ne ""))
		{
		$names{$mac} = 1;
		}
	$str = $suf;
	}
@list = keys(%names);
return(\@list);
}

#given a string, performs macro expansion. leaveUndef controls if %X% has no definition 
#whether or not it is left (if == 1, then %X% remains, if == 0, then %X% removed
#the last param, leave2perc controls if %% => % or stays as %%.  leave2perc = 1 <=> %%
sub macroExp($\%;$$) #($str, %macros, $leaveUndef, $leave2perc)
{
my ($str, $macros, $leaveUndef, $leave2perc) = @_;
$leaveUndef = genMod_setVal(0, $leaveUndef);
$leave2perc = genMod_setVal(0, $leave2perc);
#print("me: str: [$str], macros: [$macros]<br>\n");
my $procStr = "";
my ($pref, $mac, $suf);
while(($pref, $mac, $suf) = ($str =~ /(^|(?:^.*?[^%]))%([^%]*?)%($|(?:.*$))/gs))
	{
	if(defined($mac) && ($mac ne "") && defined($macros->{$mac}))
		{
		$procStr .= $pref . $macros->{$mac};
		}
	elsif($mac eq "" && !$leave2perc)
		{
		$procStr .= $pref . "%";
		}
	else
		{
		$procStr .= $pref;
		#behavior differs on whether undef'd macros are removed or not
		$procStr .= "\%$mac\%"
			if $leaveUndef;
		}
	$str = $suf;
	}
$procStr .= $str
	if defined($str);
return($procStr);
}

#given a string, color, and locations, color code it;  coordinates are assumed to be
#1-based AND that there are no overlaps (otherwise, it just doesn't make sense, does it?)
sub colorStr($$\@\@;$) #($str, $color, @starts, @ends, $width=70)
{
my ($str, $color, $starts, $ends, $width) = @_;
$width = genMod_setVal(70, $width);
if(@$starts != @$ends)
	{
	return(undef);
	}
if(@$starts == 0)
	{
	return($str);
	}
my @joint = ($starts, $ends);
my $sorted = genMod::jointSort(@joint);
my $startsSort = $sorted->[0];
my $endsSort = $sorted->[1];
my $colStr = "";
#first insert color tags
for(my $i=0; $i < @$startsSort; $i++)
	{
	my $nonColLen;
	my ($start, $end, $lastEnd);
	$start = $startsSort->[$i];
	$end = $endsSort->[$i];
	if($i == 0)
		{
		$lastEnd = 1;
		$nonColLen = $start - 1;
		}
	else
		{
		$lastEnd = $endsSort->[$i-1];
		$nonColLen = $start - $lastEnd - 1;
		}
	my $colLen = $end - $start + 1;
	my $x = substr($str, $start-1, $colLen);
	my $y = substr($str, $lastEnd-1, $nonColLen);
	$colStr .= $y . "<font color=$color>" . $x . "</font>";
	}
$colStr .= substr($str, $endsSort->[-1]);
#now format to width
my $retStr = genMod::widthFormatHtml($colStr, $width);
return($retStr);
}

#takes a string and formats to a fixed width, ignoring chars <...>
sub widthFormatHtml($;$) #($str, $width=70)
{
my ($str, $width) = @_;
$width = genMod_setVal(70, $width);
#now format to width
my $cnt = 0;
my $len = length($str);
my $retStr = "";
for(my $i=0; $i < $len; $i++)
	{
	my $c = substr($str, $i, 1);
	if($c eq "<")
		{
		while(($c = substr($str, $i++, 1)) ne ">")
			{
			$retStr .= $c;
			}
		$retStr .= $c;
		$i--;
		}
	else
		{
		$retStr .= $c;
		$cnt++;
		if($cnt == $width)
			{
			$cnt = 0;
			$retStr .= "\n";
			}
		}
	}
return($retStr);
}

#given a list of parallel arrays that are "linked", sorts on the first array (numeric)
sub jointSort(\@;$) #(@lists, $type=0)
{
my ($lists, $type) = @_;
$type = genMod_setVal(0, $type); #0 => numeric, 1 => lex
for(my $i=0; $i < @$lists; $i++)
	{
	if($i > 0 && @{$lists->[$i]} != @{$lists->[$i-1]})
		{
		return(undef);
		}
	}
my @tmp = ();
#build amalgamate array
for(my $i=0; $i < @{$lists->[0]}; $i++)
	{
	for(my $j=0; $j < @$lists; $j++)
		{
		push(@{$tmp[$i]}, $lists->[$j][$i]);
		}
	}
#sort
if($type == 0)
	{
	@tmp = sort {$a->[0] <=> $b->[0]} @tmp;
	}
else
	{
	@tmp = sort {$a->[0] cmp $b->[0]} @tmp;
	}
#build parallel return lists
my @retList = ();
for(my $i=0; $i < @tmp; $i++) #foreach element
	{
	for(my $j=0; $j < @{$tmp[$i]}; $j++) #foreach array
		{
		push(@{$retList[$j]}, $tmp[$i][$j]);
		}
	}
return(\@retList);
}


#given a set of arrays, takes the intersection of them and returns it
sub arrayIntersection(\@) #(@arrayList)
{
my ($arrayList) = @_;
my @inter = ();
my @hashes;
foreach my $arr (@$arrayList)
	{
	push(@hashes, {map {$_=>1} @$arr});
	}
#for 1st list
foreach my $item (@{$arrayList->[0]})
	{
	my $i;
	for($i=1; $i < @hashes; $i++)
		{
		if(!defined($hashes[$i]{$item}))
			{
			last;
			}
		}
	if($i == @hashes)
		{
		push(@inter, $item);
		}
	}
return(\@inter);
}


#generates a filename based on pid and time using the base portion
sub genTempFileName($) #($base)
{
my ($base) = @_;
my $fname = "$base." . $$ . "." . time();
return($fname);
}

#cats a file and returns the contents
sub catFile($) #($file)
{
my ($file) = @_;
local(*INF);
my $contents = "";
if(genMod_openFile(*INF, $file, 1))
	{
	$contents = join("", <INF>);
	close(INF);
	}
return($contents);
}


#read a binary file
sub catFileBin($) #($file)
{
my ($file) = @_;
local(*INF);
my $contents = "";
if(genMod_openFile(*INF, $file, 1))
	{
	binmode(INF);
	my $tmp;
	while(read(INF, $tmp, 1024))
		{
		$contents .= $tmp;
		}
	close(INF);
	}
return($contents);
}

#given a num, see if it falls between n1/n2 (will see if n1 > n2 or n2 > n1
sub between($$$) #($num, $r1, $r2)
{
my ($num, $r1, $r2) = @_;
if($r1 > $r2)
	{
	return($num >= $r2 && $num <= $r1);
	}
else
	{
	return($num >= $r1 && $num <= $r2);
	}
}

#given 2 regions, return the overlap between (ov must exist)
#returns (start,end)
sub calcOv($$$$) #($s1, $e1, $s2, $e2);
{
my ($s1, $e1, $s2, $e2) = @_;
my ($rSt, $rEd);
if($s1 < $s2) #s2 is rightmost start
	{
	$rSt = $s2;
	}
else #s1 is rightmost start
	{
	$rSt = $s1;
	}
if($e1 < $e2) #e1 is left-most end
	{
	$rEd = $e1;
	}
else #d2 is leftmost end
	{
	$rEd = $e2;
	}
return($rSt, $rEd);
}


#generates a random RGB color (optionally a min dist from white)
sub genRandColor(;$) #($minColDist=0)
{
my ($minColDist) = @_;
$minColDist = genMod::genMod_setVal(0, $minColDist);
my ($colR, $colG, $colB);
$colR = int(255*rand());
$colG = int(255*rand());
#impose requirement on 3rd component
my $maxB = $minColDist**2 - (255-$colR)**2 - (255-$colG)**2;
$maxB = 255 - sqrt($maxB)
	if $maxB >= 0;
$maxB = 255
	if $maxB > 255;
$colB = int($maxB*rand());
return($colR, $colG, $colB);
}

sub defVal($$) #($def, $check)
{
my ($def, $check) = @_;
return(genMod_setVal($def, $check));
}

sub remComments($) #($str)
{
my ($str) = @_;
$str =~ s/^(.*?)#.*$/$1/;
return($str);
}

# resolve a path that may contain symlinks into a symlink free path
sub genMod_resolveLinks($) #($path)
{
    my $path = shift;
    if (!($path =~ /^\//)) {
	return($path);  # do not process relative paths (must start with /)
    }
    my $resolve = "";
    while ($path ne "") {
	#print "Next: $resolve <==> $path\n";
	# extract first component (with no /'s) and rest of path (will start with /)
	my ($head, $tail) = ($path =~ /^\/([^\/]*)(.*)$/);
	# handle . and .. and // in path (remove them)
	if ($head eq '.' || $head eq '') {
	    $path = $tail;
	    next;
	}
	if ($head eq '..') {
	    $path = $tail;
	    next if ($resolve eq '');
	    $resolve =~ s/\/[^\/]*$//;  # strip the last component
	    #print "Parent: $resolve <==> $path\n";
	    next;
	}
	# create path with resolved plus new component
	$resolve = $resolve . "/" . $head;
	if (! -e $resolve) {
	    #print "No path: $resolve\n";
	    return '';
	}
	if (-l $resolve) {
	    # its a symlink - restart resolving using symlink path as head
	    my $link = readlink ($resolve) . $tail;
	    #print "Syml: $resolve :: $tail => $link\n";
	    if ($link =~ /^\//) {
		# absolute link - just use it
		$path = $link;
	    } else {
		# relative link - paste it into link ref
		$resolve =~ s/$head/$link/;
		$path = $resolve;
	    }
	    $resolve = "";
	    next;
        } else {
          #print "No syml: $resolve\n";
	}
	$path = $tail;
    }
    return($resolve); # absolute path with no symlinks
}

#must have a new function
sub new()
{
my $self = {};
bless $self;
return($self);
}

#must have a DESTROY function 
sub DESTROY()
{
}


} #END MODULE NAME
###########################

return 1;
