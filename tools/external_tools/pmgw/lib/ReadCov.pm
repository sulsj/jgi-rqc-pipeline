package ReadCov;

# FILE HISTORY:
# v0.1 2009-10 EK file created
# v0.2 2010-05-12 EK add methods to read wig files and return coverage stats for multiexon loci

=head1 NAME

ReadCov

=head1 PURPOSE

To store entire genome sequence and read coverage profile.

=head2 NOTE

genomic coordinates start at position 0 (not 1), consistent with UCSC bed and psl formats, but
inconsistent with Sanger GFF and wig formats (which numbers from 1)

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 VERSION

0.2 (5/12/10)

=head1 METHODS

=cut

# objects are blessed arrays, not hashes.
use constant {
	SEQ => 0,    # hash of chrom ID => nucleotide sequence
	LEN => 1,    # hash of chrom ID => chrom length (in bp)
	COV => 2,    # hash of chrom ID => read coverage per base
	RDS => 3     # hash of chrom ID => number of reads (each read contributes at a single point)
};

use PDL;
use PDL::IO::Misc;
use PDL::IO::FastRaw;
use PDL::NiceSlice;
use IO::File;

###############################################################################
## INIT

=head2 new

Create new object

=cut

sub new
{
	my ($class, $infile) = @_;
	my $this = [ {}, {}, {}, {} ];
	bless $this, $class;
	$this->read_fasta($infile) if $infile;
	return $this;
}

=head2 read_fasta

Load genome sequence Fasta file

=cut

sub read_fasta
{
	my ($this, $infile) = @_;
	my ($chr, $seq) = ('', '');
	open(FASTA, "<$infile") or die("ERROR: Cannot open $infile\n");
	while (<FASTA>)
	{
		chomp;
		if (/^>(\S+)/)
		{
			$this->_save_chr($chr, $seq);
			($chr, $seq) = ($1, '');
		} else
		{
			$seq .= $_;
		}
	}
	$this->_save_chr($chr, $seq);
	close FASTA;
	return

}

# private sub
sub _save_chr
{
	my ($this, $chr, $seq) = @_;
	return unless $chr and $seq;
	$this->[SEQ]->{$chr} = uc($seq);
	my $len = $this->[LEN]->{$chr} = length($seq);
	$this->[COV]->{$chr} = zeroes($len);
	$this->[RDS]->{$chr} = zeroes($len);
}

=head2 read_ace

Load genome sequence from Ace file

=cut

sub read_ace
{
	my ($this, $infile) = @_;
	my ($chr, $seq) = ('', '');
	open(ACE, "<$infile") or die("ERROR: Cannot open $infile\n");
	while (<ACE>)
	{
		chomp;
		if (/^CO (\S+)/)
		{
			($chr, $seq) = ($1, '');
			while (<ACE>)
			{
				chomp;
				last unless $_;
				$_ =~ s/\*//g;
				$seq .= $_;
			}
			$this->_save_chr($chr, $seq);
		}
	}
	close ACE;
}

###############################################################################
## ACCESSORS

=head2 seq

Given a chromosome ID, returns the complete sequence.

=cut

sub seq
{
	my ($this, $chr) = @_;
	die("$chr does not exist\n") unless exists($this->[SEQ]->{$chr});
	return $this->[SEQ]->{$chr};
}

=head2 chrs

Returns an alphabetically-sorted list of chromosome IDs

=cut

sub chrs
{
	my $this = shift;
	return sort keys %{$this->[LEN]};
}

=head2 size

Given a chromosome ID, returns the length in base pairs.

=cut

sub size
{
	my ($this, $chr) = @_;
	die("$chr does not exist\n") unless exists($this->[LEN]->{$chr});
	return $this->[LEN]->{$chr};
}

=head2 sizes

Returns a hash of chromosome ID and lengths.

=cut

sub sizes
{
	my $this = shift;
	# Note: Returns a copy of the data held in the object, not a pointer to the object's private data!
	my %sizes;
	foreach my $chr (keys %{$this->[LEN]})
	{
		$sizes{$chr} = $this->[LEN]->{$chr};
	}
	return \%sizes;
}

=head2 get_subseq

Return genome sequence at a prescribed locus. Note: 0-based coordinates are used.

=cut

sub get_subseq
{
	my ($this, $chr, $start, $size, $strand) = @_;

	# Validate input
	die("UNKNOWN CONTIG: $chr ($start,$size,$strand)\n") unless exists $this->[SEQ]->{$chr};
	die("BAD START: $start ($chr,$size,$strand)\n")      unless $start >= 0;
	die("BAD LEN: $size ($chr,$start,$strand)\n")        unless $size >= 1;
	$strand = '+' if !defined($strand);
	die("BAD STRAND: $strand ($chr,$start,$size)\n") unless $strand =~ m/^[+-]$/;
	my $end = $start + $size - 1;
	die("End coordinate exceeds end of contig: $chr, $start, $size, $strand\n") if $end > ($this->[LEN]->{$chr} - 1);

	# Get substring, reverse complement -ve strand
	my $subseq = substr($this->[SEQ]->{$chr}, $start, $size);
	return $strand eq '+' ? $subseq : rev_comp($subseq);

	# private sub
	sub rev_comp
	{
		my $seq  = shift;
		my $seq2 = reverse($seq);
		$seq2 =~ tr/ATCGRYatcgry/TAGCYRtagcyr/;
		return $seq2;
	}
}

###############################################################################
## INPUT

=head2 add_read

Save a read's alignment.  Note: 0-based coordinates are used.

=cut

sub add_read
{
	my ($this, $chr, $starts, $ends, $x) = @_;
	# VALIDATE INPUT
	die("Invalid chr, $chr\n")    unless exists($this->[COV]->{$chr});
	die("Invalid starts array\n") unless @$starts;
	die("Invalid ends array\n")   unless @$ends;
	$x = 1 unless $x;    # used for adding several identical reads at once (optional; default to 1)

	# INCREMENT COVERAGE VECTOR FOR EACH EXON LOCUS
	for (my $i = 0; $i <= $#$starts; $i++)
	{
		my $start = $starts->[$i];
		die "Invalid start, $start, for contig, $chr, of length " . $this->[LEN]->{$chr} . "\n" unless $start >= 0 and $start < $this->[LEN]->{$chr};
		my $end = $ends->[$i];
		die "Invalid end, $end, extends past end of contig, $chr, of length " . $this->[LEN]->{$chr} . "\n" unless $end < $this->[LEN]->{$chr};
		$this->[COV]->{$chr}->($start : $end) += $x;
	}

	# INCREMENT READ COUNTER
	# choose middle exon
	my $mid = int(@starts / 2);
	# pick midpoint in chosen exon (do it this way so points won't be within introns)
	my $pos = $starts->[$mid] + int(($ends->[$mid] - $starts->[$mid]) / 2);
	$this->[RDS]->{$chr}->($pos) += $x;
}

###############################################################################
## STATS

=head2 contig_stats

Returns the contig length, number of reads, plus median, mean, and stddev of coverage

=cut

sub contig_stats
{
	my ($this, $chr) = @_;
	# VALIDATE INPUT
	unless (exists($this->[COV]->{$chr}))
	{
		warn "Invalid chr, $chr\n";
		return;
	}
	my $len = $this->[LEN]->{$chr};
	# CALC STATS
	my $end    = $len - 1;
	my $rds    = sum($this->[RDS]->{$chr}->(0 : $end));
	my $median = oddmedian($this->[COV]->{$chr}->(0 : $end));
	my ($mean, $stddev) = stats($this->[COV]->{$chr}->(0 : $end));
	$mean   = int($mean * 100 + 0.5) / 100;
	$stddev = int($stddev * 100 + 0.5) / 100;
	return ($len, $rds, $median, $mean, $stddev);
}

=head2 locus_coverage_stats

Returns coverage stats for a locus (NCBI format).

=cut

sub locus_coverage_stats
{
	my ($this, $chr, $locus) = @_;
	die("Chromosome ($chr) does not exist!\n") unless exists($this->[LEN]->{$chr});
	# Parse locus string
	if ($locus =~ /^complement\((.+)\)$/) { $locus = $1 }
	if ($locus =~ /^join\(\<?(.+)\>?\)$/) { $locus = $1 }
	my @exons = split(/,/, $locus);
	my $len = 0;
	for (my $i = 0; $i <= $#exons; $i++)
	{
		if ($exons[$i] =~ /^(\d+)\.\.(\d+)$/)
		{
			$exons[$i] = [ $1, $2, $len = +($2 - $1 + 1) ];
		} elsif ($exons[$i] =~ /^(\d+)$/)
		{
			$exons[$i] = [ $1, $1, 1 ];
			++$len;
		} else
		{
			die("Unparseable exon: $exon\n");
		}
	}
	die("Locus extends beyond end of chromosome: $chr, $locus\n") if $exons[$#exons]->[1] > $this->[LEN]->{$chr};
	my $chr_cov  = $this->[COV]->{$chr};
	my $chr_rds  = $this->[RDS]->{$chr};
	my $gene_cov = zeroes($len);
	my $gene_rds = zeroes($len);
	my $start    = 0;
	my $end      = 0;

	foreach my $exon (@exons)
	{
		my ($ex_start, $ex_end, $ex_len) = @$exon;
		$end += ($ex_len - 1);
		$gene_cov ($start : $end) .= $chr_cov ($ex_start : $ex_end);
		$gene_rds ($start : $end) .= $chr_rds ($ex_start : $ex_end);
		$start = ++$end;
	}
	# CALC STATS
	my $reads  = sum($gene_rds       (0 : $len - 1));
	my $median = oddmedian($gene_cov (0 : $len - 1));
	my ($mean, $stddev) = stats($gene_cov (0 : $len - 1));
	$mean   = int($mean * 100 + 0.5) / 100;
	$stddev = int($stddev * 100 + 0.5) / 100;
	return ($len, $reads, $median, $mean, $stddev);
}

###############################################################################
# FILE I/O

=head2 write_wig

Output read-coverage and optionally read-count in UCSC genome browser's fixed-step .wig format

=cut

sub write_wig
{
	my ($this, $cov_file, $rds_file) = @_;
	my $out = new IO::File;
	open($out, ">$cov_file") or die($!);
	print $out "track type=wiggle_0 name=coverage_track description=read_coverage\n";
	$this->_write_wig(COV, $out);
	close $out;

	return unless $rds_file;
	open($out, ">$rds_file") or die($!);
	print $out "track type=wiggle_0 name=reads_track description=number_of_reads\n";
	$this->_write_wig(RDS, $out);
	close $out;

	sub _write_wig
	{
		my ($this, $i, $out) = @_;
		foreach my $chr (sort keys %{$this->[$i]})
		{
			my $pdl  = $this->[$i]->{$chr};
			my @dims = $pdl->dims;
			my $len  = $dims[0];
			print $out "fixedStep chrom=$chr start=1 step=1 span=$len\n";
			for (my $pos = 0; $pos < $len; $pos++)
			{
				my $x = $pdl ($pos);
				if ($x =~ /^\[(\d+)\]$/) { $x = $1 }
				print $out $x, "\n";
			}
		}
	}
}

=head2 read_wig

Read wiggle files.  Both read-coverage and read-count Wig files are required.

=cut

sub read_wig
{
	my ($this, $cov_file, $rds_file) = @_;
	die("Both read-coverage and read-count Wig files are required\n") unless $cov_file and $rds_file;
	# READ COV FILE
	my ($chr, $len, $pos) = ('', 0, 0);
	my $cov;
	open(IN, "<$cov_file") or die($!);
	my $header = <IN>;
	while (<IN>)
	{
		chomp;
		if (/^fixedStep chrom=(\S+) start=1 step=1 span=(\d+)/)
		{
			if ($chr and $len)
			{
				$this->[COV]->{$chr} = $cov;
				$this->[LEN]->{$chr} = $len;
			}
			($chr, $len) = ($1, $2);
			$cov = zeroes($len);
			$pos = 0;
		} else
		{
			$cov ($pos) .= $_;
			++$pos;
		}
	}
	close IN;
	if ($chr and $len)
	{
		$this->[COV]->{$chr} = $cov;
		$this->[LEN]->{$chr} = $len;
	}
	# READ RDS FILE
	($chr, $len, $pos) = ('', 0, 0);
	my $rds;
	open(IN, "<$rds_file") or die($!);
	$header = <IN>;
	while (<IN>)
	{
		chomp;
		if (/^fixedStep chrom=(\S+) start=1 step=1 span=(\d+)/)
		{
			if ($chr and $len)
			{
				die("Wig span in cov and rds files disagree for $chr!\n") unless $len == $this->[LEN]->{$chr};
				$this->[RDS]->{$chr} = $rds;
			}
			($chr, $len) = ($1, $2);
			$rds = zeroes($len);
			$pos = 0;
		} else
		{
			$rds ($pos) .= $_;
			++$pos;
		}
	}
	close IN;
	if ($chr and $len)
	{
		die("Wig span in cov and rds files disagree for $chr!\n") unless $len == $this->[LEN]->{$chr};
		$this->[RDS]->{$chr} = $rds;
	}
}

=head2 write_tsv

Write contig coverage statistics as tab-separated values

=cut

sub write_tsv
{
	my ($this, $filename) = @_;
	open(TSV, ">$filename") or die($!);
	print TSV "#chr,len,reads,median,mean,stddev\n";
	foreach my $chr (sort keys %{$this->[LEN]})
	{
		my @row = $this->contig_stats($chr);
		print TSV join("\t", $chr, @row), "\n";
	}
	close TSV;
}

=head2 write_gene_cov_tsv

Write gene coverage statistics as tab-separated values.  Genes must be in a table of gene_id, chrom_id, ncbi_locus

Example locus: complement(join(<2334..5325,5923..8472))

=cut

sub write_gene_cov_tsv
{
	my ($this, $infile, $outfile) = @_;
	open(IN,  "<$infile")  or die($!);
	open(OUT, ">$outfile") or die($!);
	print OUT "#chr,len,reads,median,mean,stddev\n";
	while (<IN>)
	{
		chomp;
		my ($chr, $gene, $locus) = split(/\t/);
		my ($len, $reads, $median, $mean, $stddev) = $this->locus_coverage_stats($chr, $locus);
		print OUT join("\t", $gene, $chr, $len, $reads, $median, $mean, $stddev), "\n";
	}
	close OUT;
}

1;
__END__
