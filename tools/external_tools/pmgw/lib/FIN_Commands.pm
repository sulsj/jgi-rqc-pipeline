#
# FIN_Commands.pm
#
# This module will contain all the exported constants that point to
# commonly used command functions
#
# Initial Version:  2010-05-13
#

#####################################################################
# Module Header                                                     #
#####################################################################

#
# Some boilerplate statements.
#
package FIN_Commands;
use Exporter;
@ISA = qw(Exporter);

%EXPORT_TAGS = (
		Constants => [qw(
				 ACE_COV
				 ADD_SYMLINKS
				 BLASTALL
				 CAT_CMD
				 COV_CTGS_LEN_VS_DEPTH_PLOT
				 COV_CTGS_LEN_VS_MEAN_COV_PLOT
				 CUT_CMD
				 EXTRACT_REPLICATES
				 FA_GET_LIST
				 FA_SIZE
				 FIGARO_TRIM_SEQS
				 GC
				 GC_LIB_PLOT
				 GC_COV_CONTIGS_PLOT_454
				 GET_METAGENOME_QC_STATS
				 GREP_CMD
                                 HTML_TO_PDF
				 LN_CMD
				 LUCY_TRIM
				 MEGAN
				 MEGAN_WRAPPER
				 METAGENOME_WORKFLOW
				 MV_CMD
				 NOHUP
				 NRGREP
				 PARSE_BLAST
				 PASTE_CMD
				 PREPROCESS
				 R_EXE
				 READ_COV
				 RUN_ASSEMBLY
				 RUN_ASSEMBLY_DIR
				 RUN_BLASTX_JOBS
				 SAMPLE_FASTA
				 SPLIT_ACE
				 SPLIT_FASTA
				 SUBSTRACT
                                 SFFFILE
				 SOURCE_SETTINGS
                                 ZIPEXE
				 )],
		Variables => [qw(
				 )],
		);

Exporter::export_tags('Constants', 'Variables');

use strict;
use warnings;

use FIN_Constants;

use constant CAT_CMD => 'cat';
use constant CUT_CMD => 'cut';
use constant GREP_CMD => 'grep';
use constant LN_CMD => 'ln -s';
use constant MV_CMD => 'mv';
use constant NOHUP => 'nohup';
use constant PASTE_CMD => 'paste';
use constant RUBY => 'ruby';

use constant ACE_COV => 'aceCov.pl';
use constant READ_COV => 'readcov.pl';
use constant ADD_SYMLINKS => 'add-symlinks.pl';
#use constant BLASTALL => '/jgi/tools/bin/blastall';
use constant BLASTALL => "module load blast;blastall";
use constant COV_CTGS_LEN_VS_DEPTH_PLOT => (RUBY) . " " . (BINDIR) . "covCtgsLenVsDepthPlot.rb";
use constant COV_CTGS_LEN_VS_MEAN_COV_PLOT => (RUBY) . " " . (BINDIR) . "covCtgsLenVsMeanCovPlot.rb";
use constant EXTRACT_REPLICATES => 'replicates/extract_replicates.py';
use constant FA_GET_LIST => 'faGetList';
use constant FA_SIZE => '/home/copeland/local/IX86/bin/faSize';
use constant FIGARO_TRIM_SEQS => '/home/copeland/scripts/figaro_trim_seqs.pl';
use constant GC => 'gc.pl';
use constant GC_COV_CONTIGS_PLOT_454 => (RUBY) . " " . (BINDIR) . "gcCovContigsPlot454.rb";
use constant GC_LIB_PLOT => (BINDIR) . "gcLibPlot.pl";
use constant LUCY_TRIM => "lucyTrim.sh";
use constant NRGREP => '/home/copeland/local/IX86/bin/nrgrep';
use constant PARSE_BLAST => "parse_blast.shea.pl";
#use constant RUN_ASSEMBLY_DIR => '/home/copeland/local/x86_64/newbler/2.5-internal-03-19-2010-gcc-3.4.6';
use constant RUN_ASSEMBLY_DIR => "echo 'no such path'";
#use constant RUN_ASSEMBLY_DIR => '/jgi/tools/454/rig-DataProcessing_2.4pre-20091204/bin';
use constant RUN_ASSEMBLY => (RUN_ASSEMBLY_DIR) . '/runAssembly';
use constant SFFFILE => (RUN_ASSEMBLY_DIR) . '/sfffile';
use constant SPLIT_ACE => 'split_ace.pl';
use constant SPLIT_FASTA => 'splitFasta.pl';
use constant SUBSTRACT => 'substract.pl';
use constant PREPROCESS => (BINDIR) . "preProcess2.pl";
use constant SOURCE_SETTINGS => "source " . (BINDIR) . "../config/megan_settings.sh";
use constant RUN_BLASTX_JOBS => (RQC_SW_BIN_DIR) . "support_scripts/run_blastx_jobs.pl";
use constant METAGENOME_WORKFLOW => (BINDIR) . "metaGenomeWorkFlow.pl";
use constant MEGAN_WRAPPER => (BINDIR) . "meganWrapper.pl";
use constant GET_METAGENOME_QC_STATS => (BINDIR) . "getMetagenomeQcStats.pl";
use constant SAMPLE_FASTA => (BINDIR) . "samplefasta.pl";
#use constant MEGAN => 'xvfb-run -a /jgi/tools/misc_bio/MEGAN/versions/3.9/MEGAN'; 
#use constant MEGAN => "xvfb-run -a /global/dna/projectdirs/PI/rqc/prod/tools/misc_bio/MEGAN/versions/3.9/MEGAN";
#use constant MEGAN => "/global/projectb/sandbox/rqc/prod/pipelines/external_tools/xvfb/xvfb-run -a /global/dna/projectdirs/PI/rqc/prod/tools/misc_bio/MEGAN/versions/3.9/MEGAN";
use constant MEGAN => "/global/projectb/scratch/sulsj/2016.09.01-megan-test/xvfb-run -a /global/dna/projectdirs/PI/rqc/prod/tools/misc_bio/MEGAN/versions/3.9/MEGAN";
###/usr/common/jgi/frameworks/MEGAN/5.0.80_beta/MEGAN";
#use constant R_EXE => '/jgi/tools/bin/R'; # Symlink to R
use constant R_EXE => "module load R;R";
use constant HTML_TO_PDF => (BINDIR) . "wkhtmltopdf-amd64";
use constant ZIPEXE => '/usr/bin/zip';

1;
