use strict;

=pod

This module contains various DB functions useful for interacting with a database.

Sam Rash
5-20-2002

=cut

############################
#BEGIN MODULE_NAME
{

#directive stuff
package dbMod;
require Exporter;
#add any paths for use/require directives

#any other modules needed
#use OTHER_MODULE;
use genMod;
use DBI;
use fastaUtil;

#exporting not all that important with objects/classes (I *think*)
our @ISA = qw(Exporter);
our @EXPORT = qw(); #export by default
#export by request
our @EXPORT_OK = 
	qw(tableExists dumpTable mysqlConnect mysqlDoQuery mysqlDoStmt getScaffoldSeq); 

#symbol list: list reelvant symbols here

sub BEGIN()
{
#METHOD PROTOTYPES - put method prototypes here in order to have
#them executed at the time the module is compiled

sub new();
sub DESTROY();
sub mysqlConnect($$$$;$); #($user, $pass, $db, $host, $errOk=0)
sub mysqlDoQuery($$;$\@); #($query, $dbh, $errOk = 0, @bp=undef)
sub mysqlDoStmt($$;$\@); #($query, $dbh, $errOk = 0, @bp=());
sub createTable($$;$$);#($dbh,$table,$name=undef,$sqlDir=/home/analysis/genome_browser/customDb
sub tableExists($$); #($table, $dbh)
sub dbExists($$); #($db, $dbh)
sub createTempTable($;$); #($dbh, $sql)
sub clearTable($$); #($table, $dbh);
sub dropTable($$); #($table, $dbh);
sub loadTable($$$;$$$$); #($file, $table, $dbh, $replace=0, $delim='\t', $lineDelim='\n', $noopt=1)
sub loadTableProc($$$\%\@;$$$);#$file,$table,$dbh,$whichCol,$colData,$replace=0,$delim='\t',
							  #$lineDelim='\n'
sub dumpTable($$$;\@$$$$$$); #($dbh, $table, $file, @cols=(*), $colSep="\t", $rowSep="\n", 
						  #$maxRowsPerQuery=1000000, $where="", $tables="", $alias="")

sub buildColMap($$\%); #($dbh, $table, %colMap)
sub buildColMapFromFile($\%;$);#($file, %colMap, $dir="/home/analysis/genome_browser/customDb")
sub loadFeatureTable($$\%\%\%;$$$$);#($dbh,$featureTable,%files,%work,%opts,$append=0,$mcd=100,
								  #$delim="\t", $lineDelim="\n")
sub createTrackDbEntry($$\%); #($dbh, $featureTable, %$opts)
sub loadTableAndCreate($$$;$$$$$); #($dbh, $table, $file, $append=0, $name=undef, 
								   #$sqlDir="....", delim="\t", $lineDelim="\n")
sub deleteTrack($$;\%); #($tableName, $dbh, %work)
sub getScaffoldSeq($$;$$); #($dbh, $scaff, $start, $end)

#more specific functions
sub logTrackInfo($$$\@$$$);#($dbh,$tableName,$user, @files, $desc, $confFile, $cmdStr)

sub loadFields($$$\@\@); #($dbh, $table, $db, @cols, @vals)
sub updateFields($$$\@\@$); #($dbh, $table, $db, @cols, @vals, $where)

#wrapper for compat with katherine's legacy code
sub mysqlKDoQuery($$); #($str, $dbh)

sub buildParams($\%\%); #($trackName, %opts, %params)
}


########################

#put actual functions here

#must have a new function
sub new()
{
my $self = {};
bless $self;
return($self);
}

#must have a DESTROY function 
sub DESTROY()
{
}

#connect to a msyql database;  if host undef, uses localhost.
#if db undef, does no db connect.  returns the db handle
sub mysqlConnect($$$$;$) #($user, $pass, $db, $host, $errOk=0)
{
my ($user, $pass, $db, $host, $errOk) = @_;
$db = genMod::genMod_setVal("", $db);
$host = genMod::genMod_setVal("localhost", $host);
$errOk = genMod::genMod_setVal(0, $errOk);
my $dbh = DBI->connect("DBI:mysql:$db:$host;mysql_local_infile=1", $user, $pass);
if(!$dbh && !$errOk)
	{
	die "mysqlConnect: $DBI::errstr [$user, $pass]\n";
	}
elsif(!$dbh)
	{
	return(undef);
	}
return($dbh);
}

#execute a mysql query and return ref to array of rows
sub mysqlDoQuery($$;$\@) #($query, $dbh, $errOk = 0, @bp = undef)
{
my ($query, $dbh, $errOk, $bp) = @_;
genMod::genMod_setVal(0, $errOk);
my $q = $dbh->prepare($query);
for(my $i=1; defined($bp) && $i < @$bp; $i++)
	{
	$q->bind_param($i, $bp->[$i-1]);
	}
$q->execute() 
	or ($errOk or die "ERROR: mysqlDoQuery(): [$DBI::errstr] query: [$query]\n");
my $retVal = $q->fetchall_arrayref();
return($retVal);
}

#execute a mysql query and do not return results
sub mysqlDoStmt($$;$\@) #($query, $dbh, $errOk = 0, @bp=());
{
my ($query, $dbh, $errOk, $bp) = @_;
genMod::genMod_setVal(0, $errOk);
my $q = $dbh->prepare($query);
for(my $i=0; defined($bp) && $i < @$bp; $i++)
	{
	$q->bind_param($i+1, $bp->[$i]);
	}
my $retCode;
$retCode = $q->execute();
if(!$retCode && !$errOk)
	{
	die "ERROR: mysqlDoStmt(): [$DBI::errstr] query: [$query]\n";
	}
return($retCode);
}

#given a table name, looks for "$table.sql" in sqldir and attemps to create.
#will replace any instance of #TABLENAME# will $name (if defined)
#RETURN:
#	-1 => table already exists
#	1 => success
#	0 => failure
sub createTable($$;$$) #($dbh, $table, $name, $sqlDir="/home/analysis/genome_browser/customDb)
{
my ($dbh, $table, $name, $sqlDir) = @_;
my $BASEDIR = ($0 =~ m{(.*?/)bin/})[0];
$sqlDir = genMod::genMod_setVal("${BASEDIR}genome_browser/customDb", $sqlDir);
if(dbMod::tableExists($table, $dbh))
	{
	return(-1);
	}
my $file = "$sqlDir/$table.sql";
my $sql = genMod::catFile($file);
if($sql =~ /^\s*$/)
	{
	die "ERROR: missing or empty sql file [$file]\n";
	}
if(defined($name))
	{
	$sql =~ s/#TABLENAME#/$name/g;
	}
#remove a trailing ; if it exists
$sql =~ s/;$//s;
#remove comments
$sql =~ s/#.*?\n//mg;
#create table
return(dbMod::mysqlDoStmt($sql, $dbh));
}



#given a database handle, returns 1 iff a table exists in that database
sub tableExists($$) #($table, $dbh)
{
my ($table, $dbh) = @_;
my $q = $dbh->prepare("show tables");
$q->execute();
my $row;
while(($row) = $q->fetchrow_array())
	{
	return(1)
		if($row eq $table);
	}
return(0);
}


#check if a given db exists
sub dbExists($$) #($db, $dbh)
{
my ($db, $dbh) = @_;
my $str= "SHOW databases LIKE '$db'";

my $results = dbMod::mysqlDoQuery("$str", $dbh);
return(scalar(@$results) > 0);
}

sub createTempTable($;$) #($dbh, $sql)
{
my ($dbh, $sql) = @_;
$sql = genMod::genMod_setVal("(id int(10) NOT NULL PRIMARY KEY)", $sql);

my $tempTable = "__TEMP_TABLE_" . time() . "_" . $$;
my $maxTries = 100;
my $numTries = 1;
while(tableExists($tempTable, $dbh) && ($numTries <= $maxTries))
	{
	sleep(1); #change time
	$tempTable = "__TEMP_TABLE_" . time() . "_" . $$;
	$numTries++;
	}
my $str = "CREATE TEMPORARY TABLE if not exists $tempTable $sql";
my $q = $dbh->prepare($str);
$q->execute();
return($tempTable);
}

#clear entries from a table
sub clearTable($$) #($table, $dbh);
{
my ($table, $dbh) = @_;
my $str = "delete from $table";
my $q = $dbh->prepare($str);
$q->execute();
}

#drop a table
sub dropTable($$) #($table, $dbh);
{
my ($table, $dbh) = @_;
my $str = "drop table if exists $table";
my $q = $dbh->prepare($str);
$q->execute();
}

#given a file, load into the db
sub loadTable($$$;$$$$) #($file, $table, $dbh, $replace=0, $delim='\t', $lineDelim='\n', $noopt=1)
{
my ($file, $table, $dbh, $replace, $delim, $lineDelim, $noopt) = @_;
$replace = genMod::genMod_setVal(0, $replace);
$file = genMod::genMod_getFullPath($file);
$delim = genMod::genMod_setVal('\t', $delim);
$lineDelim = genMod::genMod_setVal('\n', $lineDelim);
$noopt = genMod::genMod_setVal(0, $noopt);
my $insType = "IGNORE";
$insType = "REPLACE"
	if $replace == 1;
my $str = "load data LOCAL infile '$file' $insType into table $table FIELDS TERMINATED " 
	. "BY '$delim' LINES TERMINATED BY '$lineDelim'";
my $q = $dbh->prepare($str);
#print "str1: [$str]\n";
$q->execute()
	or return(0);
#build index
$str = "optimize table $table";
mysqlDoStmt($str, $dbh) if $noopt == 1;
return(1);
}


#loads a file into a table, but takes a hash to indicate what columns to fill in and
#an array for each col.  Procs each line and if hash{n} defined, col n will be taken from
#the array; alternatively, if the value of $whichCol{n} is not 1, it is a 3-elem aray
# where each elem (e1,e2,e3) combined with the value of colData col forms a s//op:
# s/e1/e2.col.e3/g;
sub loadTableProc($$$\%\@;$$$)#$file,$table,$dbh,%whichCol,@colData,$replace=0,$delim='\t',
							  #$lineDelim='\n'
{
my ($file, $table, $dbh, $whichCol, $colData, $replace, $delim, $lineDelim) = @_;
$replace = genMod::genMod_setVal(0, $replace);
$file = genMod::genMod_getFullPath($file);
$delim = genMod::genMod_setVal("\t", $delim);
$lineDelim = genMod::genMod_setVal("\n", $lineDelim);
my $saveLineDelim = $/;
$/ = $lineDelim;
my $insType = "IGNORE";
$insType = "REPLACE"
	if $replace == 1;
my $tmpFile = genMod::genTempFileName("/tmp/ltp.$table");
local(*INF, *OUTF);
if(!genMod::genMod_openFile(*INF, $file) || !genMod::genMod_openFile(*OUTF, "> $tmpFile"))
	{
	$/ = $saveLineDelim;
	return(0);
	}
my $i = 0;
while(my $line = genMod::genMod_readNextLine(*INF))
	{
	#printDebug "line: [$line]\n";
	my $j=0;
	my @a = split(/$delim/, $line);
	foreach my $col (sort {$a <=> $b} (keys(%$whichCol)))
		{
		if($whichCol->{$col} == 1)
			{
			#take ith element from jth column of data
			$a[$col] = $colData->[$j][$i];
			}
		elsif(ref($whichCol->{$col}))
			{
			my $elems = $whichCol->{$col};
			#the value is part 1 of a s// op and $colData is part 2
			$a[$col] =~ s/$elems->[0]/$elems->[1]$colData->[$j][$i]$elems->[2]/g;
			}
		$j++;
		}
	print OUTF join($delim, @a), $lineDelim;
	$i++;
	}
close(INF);
close(OUTF);
$/ = $saveLineDelim;
my $code = 1;
if(!dbMod::loadTable($tmpFile, $table, $dbh, $replace, $delim, $lineDelim))
	{
	$code = 0;
	}
#unlink($tmpFile);
#printDebug "tmpFile: [$tmpFile]\n";
return($code);
}

#dumps a table from an open db connection to a file with separators
# 1 => success
# 0 => failure (see $@)
# -1 => odd, vacuous success (see $@)
sub dumpTable($$$;\@$$$$$$) #($dbh, $table, $file, @cols=(*), $colSep="\t", $rowSep="\n", 
						 #$maxRowsPerQuery=1000000, $where="", $tables="", $alias= "")
{
my ($dbh, $table, $file, $cols, $colSep, $rowSep, $maxRowsPerQuery, $where, 
	$tables, $alias) = @_;
$where = genMod::defVal("", $where);
$tables = genMod::defVal("", $tables);
$alias = genMod::defVal("", $alias);
if($tables ne "")
	{
	$tables = ",$tables";
	}
if($where ne "")
	{
	$where = "where $where";
	}
my $colStr = "*";
$maxRowsPerQuery = genMod::genMod_setVal(1000000, $maxRowsPerQuery);
$colStr = join(",", @$cols)
	if defined($cols) && @$cols > 0;
$colSep = "\t"
	unless defined($colSep);
$rowSep = "\n"
	unless defined($rowSep);
my @row;
my $rowsDone = 0;
my $q = "SELECT count(*) FROM $table $alias$tables $where";
my $result = mysqlDoQuery("SELECT count(*) FROM $table $alias$tables $where", $dbh);
my $numRows = $result->[0][0];
my $code = 1;
local(*OUTF);
genMod::genMod_openFile(*OUTF, "> $file") or die;
while($rowsDone < $numRows)
	{
	my $cmd = "SELECT $colStr FROM $table $alias$tables $where LIMIT $rowsDone, $maxRowsPerQuery";
	print "cmd: [$cmd]\n";
	my $q = $dbh->prepare($cmd);
	if(!$q->execute())
		{
		$@ = "dumpTable: [$DBI::errstr]\n";
		$code = 0;
		last;
		}
	#have to fetch one row at a time as we don't know how big the table is
	while(@row = $q->fetchrow_array())
		{
		print OUTF join($colSep, @row), $rowSep;
		}
	$rowsDone += $maxRowsPerQuery;
	}
close(OUTF);
if($numRows == 0)
	{
	$@ = "ERROR: table empty.  Why are you dumping it?\n";
	$code = -1;
	}
return($code);
}

#Dump Multiple tables - added by jinal to allow dumping of multiple tables 03/14/2006
# 1 => success
# 0 => failure (see $@)
# -1 => odd, vacuous success (see $@)
sub dumpMultipleTables($$$;\@$$$$$$) #($dbh, $tablelist,$path @cols=(*), $colSep="\t", $rowSep="\n", 
						 #$maxRowsPerQuery=1000000, $where="", $tables="", $alias= "")
{
my ($dbh, $tablelist, $path, $cols, $colSep, $rowSep, $maxRowsPerQuery, $where, 
	$tables, $alias) = @_;

my $code = 1;
#List of tables to be dumped
my @tables_in_list = split(/,/,$tablelist);

#print "Dumping tables ".@tables_in_list."\n";

my $tabcount = $#tables_in_list;

if($tabcount < 0)
{
  $code = -1;
  $@ = "Please specify atleast one tablename\n";
  return ($code);
}
foreach my $table(@tables_in_list)
{

#print "printing table $table\n";
$path=genMod::defVal(".",$path);
$where = genMod::defVal("", $where);
$table = genMod::defVal("", $table);
$alias = genMod::defVal("", $alias);
if($where ne "")
	{
	$where = "where $where";
	}
my $colStr = "*";
$maxRowsPerQuery = genMod::genMod_setVal(1000000, $maxRowsPerQuery);
$colStr = join(",", @$cols)
	if defined($cols) && @$cols > 0;
$colSep = "\t"
	unless defined($colSep);
$rowSep = "\n"
	unless defined($rowSep);
my @row;
my $rowsDone = 0;
my $q = "SELECT count(*) FROM $table $alias$table $where";
my $result = mysqlDoQuery("SELECT count(*) FROM $table $alias$table $where", $dbh);
my $numRows = $result->[0][0];
local(*OUTF);
my $file = $table.".txt";
genMod::genMod_openFile(*OUTF, "> $path/$file") or die;
while($rowsDone < $numRows)
	{
	my $cmd = "SELECT $colStr FROM $table $alias$table $where LIMIT $rowsDone, $maxRowsPerQuery";
	#printDebug "cmd: [$cmd]\n";
	my $q = $dbh->prepare($cmd);
	if(!$q->execute())
		{
		$@ = "dumpTable: [$DBI::errstr]\n";
		$code = 0;
		last;
		}
	#have to fetch one row at a time as we don't know how big the table is
	while(@row = $q->fetchrow_array())
		{
		print OUTF join($colSep, @row), $rowSep;
		}
	$rowsDone += $maxRowsPerQuery;
	}
close(OUTF);
if($numRows == 0)
	{
	$@ = "ERROR: table $table empty.  Why are you dumping it?\n";
	$code = -1;
        return($code);
	}
}
        return($code);

}


#given a tablename, builds a map from column name to an index 
sub buildColMap($$\%) #($dbh, $table, %colMap)
{
my ($dbh, $table, $colMap) = @_;
my $results = dbMod::mysqlDoQuery("desc $table", $dbh);
my $pos = 0;
foreach my $row (@$results)
	{
	$colMap->{$row->[0]} = $pos;
	$pos++;
	}
}

#builds a map from column name to index from an sql file
sub buildColMapFromFile($\%;$)#($file, %colMap, $dir="/home/analysis/genome_browser/customDb")
{
my ($file, $colMap, $dir) = @_;
my $BASEDIR = ($0 =~ m{(.*?/)bin/})[0];
$dir = genMod::genMod_setVal("${BASEDIR}genome_browser/customDb", $dir);
$file = "$dir/$file";
local(*INF);
genMod::genMod_openFile(*INF, $file) 
	or return(0);
my $start = 0;
my $line;
my $pos = 0;
while($line = genMod::genMod_readNextLine(*INF))
	{
	if($line =~ /^\s*create/i)
		{
		$start = 1;
		}
	elsif($start && $line =~ /^\s+(\w+)\s+/ && $line !~ /^\s+(:?PRIMARY)?KEY/)
		{
		$colMap->{$1} = $pos++;
		}
	}
close(INF);
return(1);
}

#load/create a feature table. takes care of trackDb, hit, info, etc (as options dictate)
#opts will have trackDb entries in {trackDb}. Defaults apply to names, colors, etc
#default behavior is to wipe tables and overwrite; append can be set in which case
#data is loaded into existing tables
sub loadFeatureTable($$\%\%\%;$$$$)#($dbh,$featureTable,%files,%work,%opts,$append=0,$mcd=100,
								  #$delim="\t", $lineDelim="\n")
{
my ($dbh, $featureTable, $files, $work, $opts, $append, $mcd, $delim, $lineDelim) = @_;
$append = genMod::genMod_setVal(0, $append);
$mcd = genMod::genMod_setVal(100, $mcd);
$delim = genMod::genMod_setVal("\t", $delim);
$lineDelim = genMod::genMod_setVal("\n", $lineDelim);
my $mainTable = $featureTable;
my $hitTable = "${featureTable}Hit";
my $infoTable = "${featureTable}Info";
my $code = 1;
if(defined($work->{main}))
	{
	my $sqlType = "feature";
	$sqlType = 'feature'.ucfirst($opts->{type}) unless $opts->{type} eq 'generic';
	if(!loadTableAndCreate($dbh, $sqlType, $files->{main}, $append, $mainTable, undef, $delim,
						   $lineDelim))
		{
		print STDERR "WARNING: [$@]\n";
		$@ = "";
		$code = 0;
		}
	}
if(defined($work->{hit}))
	{
	if(!loadTableAndCreate($dbh, "featureHit1", $files->{hit}, $append, $hitTable, undef,
						   $delim, $lineDelim))
		{
		print STDERR "WARNING: [$@]\n";
		$@ = "";
		$code = 0;
		}
	}
if(defined($work->{info}))
	{
	if(!loadTableAndCreate($dbh, "featureInfo1", $files->{info}, $append, $infoTable, undef,
						   $delim, $lineDelim))
		{
		print STDERR "WARNING: [$@]\n";
		$@ = "";
		$code = 0;
		}
	}
if(defined($work->{trackDb}))
	{
	#generate color. default main color is random
	my ($r,$g,$b) = genMod::genRandColor(100);
	#create track db entry
	if(!createTrackDbEntry($dbh, $featureTable, %$opts))
		{
		$code = 0;
		}
	}
return($code);
}
	
#given params, creates a trackDb entry; removes if it already exists
sub createTrackDbEntry($$\%) #($dbh, $featureTable, %$opts)
{
my ($dbh, $featureTable, $opts) = @_;
#remove if there
dbMod::mysqlDoStmt("delete from trackDb where tableName='$featureTable'", $dbh);
my $type = genMod::genMod_setVal("generic", $opts->{type});
my $str = "insert into trackDb set tableName='$featureTable', type='$type'";
#each key in %opts should be a column/value pair for trackDb
my @bp;
foreach my $key (keys(%$opts))
	{
	next	
		if $key eq "type";
	if($key eq "html")
		{
		$str .= ", $key=?";
		push(@bp, $opts->{html});
		}
	else
		{
		$str .= ", $key='$opts->{$key}'";
		}
	}
if(!dbMod::mysqlDoStmt($str, $dbh, 0, @bp))
	{
	$@ .= "ERROR: cannot create trackDb entry\n";
	return(0);
	}
return(1);
}

#calls createTable and loadTable to create a table and populate; default behavior is
#to remove existing table
sub loadTableAndCreate($$$;$$$$$) #($dbh, $table, $file, $append=0, $name=undef, 
								  #$sqlDir="....", delim="\t", $lineDelim="\n")
{
my ($dbh, $table, $file, $append, $name, $sqlDir, $delim, $lineDelim) = @_;
$append = genMod::genMod_setVal(0, $append);
$delim = genMod::genMod_setVal("\t", $delim);
$lineDelim = genMod::genMod_setVal("\n", $lineDelim);
#tablename is $name if defined, otherwise $table
my $tableName = genMod::genMod_setVal($table, $name);
#if table is there and not appending, toast
if(dbMod::tableExists($tableName, $dbh))
	{
	if(!$append)
		{
		    print STDERR "INFO: drop table: $tableName\n";
		dbMod::dropTable($tableName, $dbh);
		    print STDERR "INFO: create table: $table\n";
		if(!createTable($dbh, $table, $name, $sqlDir))
			{
			$@ .= "ERROR: unable to create table [$tableName]\n";
			return(0);
			}
		}
	}
else
	{
	    print STDERR "INFO: create table: $table\n";
	if(!createTable($dbh, $table, $name, $sqlDir))
		{
		$@ .= "ERROR: unable to create table [$tableName]\n";
		return(0);
		}
	}
#load data (if $file defined--if not, user does not want data loaded)
print STDERR "INFO: load table: $tableName\n";
if(defined($file) && !loadTable($file, $tableName, $dbh, 1, $delim, $lineDelim))
	{
	$@ .= "ERROR: unable to load data for table [$tableName:$table] from file [$file]\n";
	return(0);
	}
return(1);
}


#delete a track from trackDb and tables (depending on %work)
sub deleteTrack($$;\%) #($tableName, $dbh, %work)
{
my ($tableName, $dbh, $work) = @_;
my $hitTable = "${tableName}Hit";
my $infoTable = "${tableName}Info";
if(defined($work->{trackDb}) || !defined($work))
	{
	dbMod::mysqlDoStmt("delete from trackDb where tableName='$tableName'", $dbh, 1);
	}
if(defined($work->{main}) || !defined($work))
	{
	dbMod::dropTable($tableName, $dbh);
	}
if(defined($work->{hit}) || !defined($work))
	{
	dbMod::dropTable($hitTable, $dbh);
	}
if(defined($work->{info}) || !defined($work))
	{
	dbMod::dropTable($infoTable, $dbh);
	}
}

#get sequence for a scaffold from the database
sub getScaffoldSeq($$;$$) #($dbh, $scaff, $start, $end)
{
my ($dbh, $scaff, $start, $end) = @_;
my ($str, $q);
my @row;

# Look to see if the scaffold is stored in a file database
$str = "SELECT size, fileName FROM chromInfo WHERE chrom = '$scaff' LIMIT 1";
$q = $dbh->prepare($str);
$q->execute;
if($q->rows < 1)
	{
	# Scaffold does not exist.  Let caller do error handling.
	$q->finish();
	return(undef);
	}
@row = $q->fetchrow_array();
$q->finish();
my $size = $row[0];
my $fileName = $row[1];
$start = 1	
	if ((!defined($start)) || ($start < 1));
$end = $size	
	if ((!defined($end)) || ($end > $size));

my $segmentLen = $end - $start + 1;

if((length($fileName) > 0) && ($fileName ne 'file'))
	{
	local(*PIPE);

	# In file
	$fileName =~ tr/-.\/A-Za-z0-9_//cd;
	$scaff =~ tr/-.\/A-Za-z0-9_//cd;
	$start =~ tr/-.\/A-Za-z0-9_//cd;
	$start--;
	my $seq = fastaUtil::getFastaSeq($scaff, $fileName, $start, $segmentLen);
	return( $seq );
	}
else
	{
	# In database
	$str = "SELECT SUBSTRING(seq, $start, $segmentLen) FROM scaffoldInfo WHERE " .
			"scaffold = '$scaff' LIMIT 1";
	$q = $dbh->prepare($str);
	$q->execute;
	@row = $q->fetchrow_array();
	$q->finish;

	# remove above once all databases has scaffoldSeq table
	if ( (length($row[0]) < 1) && tableExists( "scaffoldSeq", $dbh ) )
		{
		$str = "SELECT start, end FROM scaffoldSeq WHERE scaffold = '$scaff' AND start <= $end AND end >= $start ORDER BY start";
		$q = $dbh->prepare($str);
		$q->execute;
		my $rows = $q->fetchall_arrayref( {} );
		my $numRows = scalar( @{$rows} );

		if ( $numRows < 2 )
			{
			my $offStart = $rows->[0]->{start};
			my $offEnd = $rows->[0]->{end};
			if ( $offEnd < $end )
				{
				$end = $offEnd;
				$segmentLen = $end - $start + 1;
				}
			my $query = "SELECT SUBSTRING( seq, " . ( $start - $offStart + 1 ) . ", $segmentLen) FROM scaffoldSeq WHERE scaffold = '$scaff' AND start = '$offStart'";
			$q = $dbh->prepare($query);
			$q->execute;
			@row = $q->fetchrow_array();
			return $row[0];
			}
		else
			{
			my $firstRow = $rows->[0];
			my $lastRow = $rows->[$numRows - 1];
			my $firstOffStart = $firstRow->{start};
			my $firstOffEnd = $firstRow->{end};
			my $lastOffStart = $lastRow->{start};
			my $lastOffEnd = $lastRow->{end};
			my $firstSize = $firstOffEnd - $start + 1;
			my $lastSize = $end - $lastOffStart + 1;

			if ( $lastOffEnd < $end )
				{
				$end = $lastOffEnd;
				$segmentLen = $end - $start + 1;
				$lastSize = $end - $lastOffStart + 1;
				}

			my $seq = "";
			for (my $i = 0; $i < $numRows; $i++)
				{
				my $query;
				if ( $i == 0 )
					{
					$query = "SELECT SUBSTRING(seq, " . ($start - $firstOffStart + 1) . ", $firstSize) FROM scaffoldSeq WHERE scaffold = '$scaff' AND start = $firstOffStart";
					}
				elsif ( $i == $numRows - 1 )
					{
					$query = "SELECT SUBSTRING(seq, 1, $lastSize) FROM scaffoldSeq WHERE scaffold = '$scaff' AND start = $lastOffStart";
					}
				else
					{
					$query = "SELECT seq FROM scaffoldSeq WHERE scaffold = '$scaff' AND start = " . $rows->[$i]->{start};
					}
				$q = $dbh->prepare($query);
				$q->execute;
				@row = $q->fetchrow_array();
				$seq .= $row[0];
				$q->finish;
				}

			return $seq;
			}
		}
		else
		{
		return($row[0]);
		}
	}
}

#takes args and logs into the trackLog table (creates if not there)
sub logTrackInfo($$$\@$$$) #($dbh,$tableName, $user, @files, $desc, $confFile, $cmdStr)
{
my ($dbh, $tableName, $user, $files, $desc, $confFile, $cmdStr) = @_;
my $BASEDIR = ($0 =~ m{(.*?/)bin/})[0];
my $sqlFile = "${BASEDIR}genome_browser/customDb/trackLog.sql";
my $tmpFile = "/tmp/logTrackInfo.tmp.$$";
my $delim = "__||__";
my $lineDelim = "\n||\n||\n";
my $fileStr = join("\n", @$files) . "\n";
my $conf = "";
my ($now, $result);
local(*TMP);
#if table doesn't exist, create
if(!tableExists("trackLog", $dbh))
	{
	my $sql = `cat $sqlFile`;
	mysqlDoStmt("$sql", $dbh);
	}
#get conf file data
if(defined($confFile) && -e $confFile)
	{
	$conf = `cat $confFile`;
	}
#get insert time
$result = mysqlDoQuery("select NOW()", $dbh);
$now = $result->[0][0];
#create temp 
genMod::genMod_openFile(*TMP, "> $tmpFile") or die;
print TMP join($delim, ($tableName, $user, $now, $fileStr, $desc, $conf, $cmdStr)) . "\n";
close(TMP);
loadTable($tmpFile, "trackLog", $dbh, 1, $delim, $lineDelim);
unlink($tmpFile);
}

#populates a table's columns with values (loads a row) with
#a insert into db.table set col=val, ...
#returns 0/1 on succ/fail
sub loadFields($$$\@\@) #($dbh, $table, $db, @cols, @vals)
{
my ($dbh, $table, $db, $cols, $vals) = @_;
my $stmt = "insert into $db.$table set ";
if(@$cols != @$vals)
	{
	my $x = @$cols;
	my $y = @$vals;
	@$ = "dbMod::loadFields: #cols [$x] not equal #vals [$y]\n";
	return(0);
	}
my $first = 1;
foreach my $c (@$cols)
	{
	$stmt .= ", "
		if $first;
	$stmt .= "$c = ?";
	$first = 0;
	}
my $q = $dbh->prepare($stmt);
if(!$q->execute($stmt, @$vals))
	{
	$@ = $DBI::errstr;
	return(0)
	}
return(1);
}

#populates a table's columns with values (loads a row) with
#a insert into db.table set col=val, ...
#returns 0/1 on succ/fail
sub updateFields($$$\@\@$) #($dbh, $table, $db, @cols, @vals, $where)
{
my ($dbh, $table, $db, $cols, $vals, $where) = @_;
my $stmt = "update $db.$table set ";
if(@$cols != @$vals)
	{
	my $x = @$cols;
	my $y = @$vals;
	@$ = "dbMod::loadFields: #cols [$x] not equal #vals [$y]\n";
	return(0);
	}
my $first = 1;
foreach my $c (@$cols)
	{
	$stmt .= ", "
		if !$first;
	$stmt .= "$c = ?";
	$first = 0;
	}
$stmt .= " where $where";
my $q = $dbh->prepare($stmt);
if(!$q->execute(@$vals))
	{
	$@ = $DBI::errstr;
	return(0)
	}
return(1);
}

sub mysqlKDoQuery($$) #($str, $dbh)
{
my ($str, $dbh) = @_;
my $ref = mysqlDoQuery($str, $dbh);
my @ret;
foreach my $row (@$ref)
	{
	push(@ret, join("\t", @$row));
	}
if(@ret > 0)
	{
	return(\@ret);
	}
return(0);
}


#builds the %opts object to pass to loadFeatureTable()
sub buildParams($\%\%) #($trackName, %opts, %params)
{
my ($trackName, $opts, $params) = @_;
$params->{type} = "generic";
if (defined($opts->{mt})) {
  $params->{type} = "model";
} elsif (defined($opts->{pt})) {
  $params->{type} = "paired";
}

$params->{searchable} = genMod::defVal(0, $opts->{e});
$params->{shortLabel} = genMod::defVal($trackName, $opts->{s});
$params->{longLabel} = genMod::defVal($params->{shortLabel}, $opts->{l});
$params->{visibility} = genMod::defVal(1, $opts->{v}); #0,1,2 = hide,dense,full
$params->{priority} = genMod::defVal(1000, $opts->{p});	
$params->{private} = genMod::defVal(0, $opts->{r});
$params->{html} = "";
if(defined($opts->{h}))
	{
	$params->{html} = genMod::catFile($opts->{h});
	}
my ($colR, $colG, $colB, $altColR, $altColG, $altColB);
if(defined($opts->{c}))
	{
	($colR, $colG, $colB) = split(/,/, $opts->{c});
	}
else
	{
	($colR, $colG, $colB) = genMod::genRandColor();
	}
$params->{colorR} = $colR;
$params->{colorG} = $colG;
$params->{colorB} = $colB;
if(defined($opts->{o}))
	{
	($altColR, $altColG, $altColB) = split(/,/, $opts->{c});
	}
else
	{
	($altColR, $altColG, $altColB) = genMod::genRandColor();
	}
$params->{altColorR} = $altColR;
$params->{altColorG} = $altColG;
$params->{altColorB} = $altColB;
$params->{url} = genMod::defVal("", $opts->{u});
$params->{alt_draw} = genMod::defVal(0, $opts->{a});
}


} #END dbMod
###########################

return 1;
