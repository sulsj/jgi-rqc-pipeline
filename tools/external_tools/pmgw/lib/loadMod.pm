use strict;

=pod

******loadMod*******

This module contains various functions that deal with loading data into a problem.  Some
read the data from a file while others use the database.  If/when this grows too big,
the db functions may be moved to an independent module (dbMod), perhaps.
(helper functions are also present and may be internalized by removing the loadMod_
prefix and then not exporting).

Common params passed in via %data:
	$data{user} - username for db
	$data{pass} - password for db
	$data{dbName} - db to connect to
	$data{mainTable} - feature table
	$data{infoTable} - info table
	$data{mainTabList} - list of feature tables
	$data{infoTabList} - list of info tables
	$data{scaffList} - if defined before calling *Prep*() functions, contains
		a set of scaffolds to work on (w/o, is populated with all scaffolds).

=cut


############################
#BEGIN loadMod
{
#use strict;

#directive stuff
package loadMod;
require Exporter;
#add any paths for use/require directives

#any other modules needed
use DBI;
use genMod;
use parseMod;
use dbMod;
use Time::localtime;

#exporting not all that important with objects/classes (I *think*)
our @ISA = qw(Exporter);
our @EXPORT = 
	qw(loadMod_loadNextFeature loadMod_dbPrepLoadFeatures loadMod_dbLoadNextFeature 
	    loadMod_dbLoadFeatureRange
	   loadMod_dbPrepLoadFeaturesMerge loadMod_dbLoadNextFeatureMerge
	   loadMod_dbLoadNextFeatTabMerge loadMod_calcFeatScore loadMod_getHitFrame
	   loadMod_findStartEndHit loadMod_loadStringArr loadMod_loadHash
	   loadMod_loadFastaHash loadMod_loadFastaArr
	   loadMod_dbPrepLoadFeatScaff loadMod_dbLoadFeatNextScaff
	   loadMod_dbPrepLoadScaff loadMod_dbLoadNextScaff loadMod_dbLoadNextScaffTab
	   loadMod_getDbConfig loadMod_checkUserCap loadMod_fastaIterNext
	   loadMod_getWebHost loadMod_fastaIterOpen loadMod_fastaIterNext
	   loadMod_fastaIterClose);

our @EXPORT_OK = qw(); #export by request

#symbol list: list reelvant symbols here



sub BEGIN()
{
#METHOD PROTOTYPES - put method prototypes here in order to have
#them executed at the time the module is compiled

#function that loads feature froma  file
sub loadMod_loadNextFeature(\*\*\%;$); #(*track_in_fh, track_info_in_fh, %data, $noHspOk=0)
sub loadNextFeatureV2(\*\*\%;$); #(*track_in_fh, track_info_in_fh, %data, $noHspOk=0)
#functions that load data from database (1 feature and its HSPs)
sub loadMod_dbPrepLoadFeatures(\$\%); #($dbh, %data)
sub loadMod_dbLoadNextFeature($\%); #($dbh, %data)
#function to load a range of features
sub loadMod_dbLoadFeatureRange($$\%;$); #($startFid, $count, %data, $disconn=1)
#functions that load all the features/hsps hitting a scaffold (iterates)
sub loadMod_dbPrepLoadFeatScaff(\$\%); #($dbh, %data)
sub loadMod_dbLoadFeatNextScaff($\%;$); #($dbh, %data, $skipHsp)
#functions that load all the features hitting a set scaffold in a set of tables and
#makes those features HSPs
sub loadMod_dbPrepLoadFeaturesMerge(\$\%); #($dbh, %data)
sub loadMod_dbLoadNextFeatureMerge(\$\%); #($dbh, %data)
sub fillCache(\$$\%;$); #($dbh, $scaffNum, %data, $bufSize=100)
sub loadMod_dbLoadNextFeatTabMerge($$$\$$$$\@\%); #($dbh, $scaff, $hspNumRef, $metaFid,....
sub loadMod_calcFeatScore($$$); #($dbh, $fid, $table)
sub loadMod_getHitFrame($$$); #($dbh, $fid, $table)
sub loadMod_findStartEndHit($$$); #($dbh, $fid, $table)

#general data loading functions
sub loadMod_loadStringArr($\@;$\%); #($infile, @arr, $func, %data)
sub loadMod_loadHash($$\%); #($infile, $sep, %hash)
sub loadMod_loadFastaHash($\%;$); #($infile, %hash, $nameFunc)
sub loadMod_loadFastaArr($\@;$); #($infile, @arr, $nameFunc)

#features to loads all features from a set of tables hitting a scaffold/strand. 
sub loadMod_dbPrepLoadScaff(\$\%); #($dbh, %data)
sub loadMod_dbLoadNextScaff($\%); #($dbh, %data)
sub loadMod_dbLoadNextScaffTab($$\%); #($dbh, $tableInd, %data)
sub loadMod_getDbConfig($;$$); #($db, $webHost, $configFile)
sub loadMod_getWebHost($;$); #($db, $configFile)
sub loadMod_checkUserCap($$;$$); #($db, $printMsg, $host, $configFile)

#general functions
sub loadMod_buildScaffList(\$\%); #($dbh, %data)

#temp wrappers
sub loadMod_fastaIterOpen(\*$;$); #(FH, $fileStr, $failOk)
sub loadMod_fastaIterNext(\*;$$); #(*FH, $defFunc, $seqFunc)
sub loadMod_fastaIterClose(\*); #(FH)
}


########################

#given a pair of file handles, loads the next feature from the file
#(feature + hsps for that feature).  Assumes that all hsps for a feature are
#grouped together and these grouped HSPs appear in the same order as the features do
#(as in parseBlastXml.pl output)
#RETURNS VALUE in $data{features}, $data{hsps}
#NOTE: must init $data{id} = 1
sub loadMod_loadNextFeature(\*\*\%;$) #(*track_in_fh, track_info_in_fh, %data, $noHspOk=0)
{
my ($trackFh, $trackInfoFh, $data, $noHspOk) = @_;
$noHspOk = 0
	unless defined($noHspOk);
my $line;
$data->{id} = genMod::genMod_setVal(1, $data->{id});
#make sure feature id is at least 1 (no smaller)
$data->{id} = 1 
	if $data->{id} < 1;
#@{${%$data}{feature}} = ();
#@{${%$data}{hsps}} = ();
@{$data->{feature}} = ();
@{$data->{hsps}} = ();

#read next line of trackFh (main track file)
my $fileDone = 1;
while($line = <$trackFh>)
	{
	chomp $line;
	next if $line =~ /^\s*$/;
	$fileDone = 0;
	last;
	}
#return that the files are all parsed if main track file all read
return(0) if $fileDone;
#parse line
#@{${%$data}{feature}} = split(/\t/, $line);
@{$data->{feature}} =  split(/\t/, $line);
#note: id (element 0) may be BLANK. we set based on $data{id}
#my $id = ${%$data}{id}++;  #inc first so we start at 1
my $id = $data->{id}++;  #inc first so we start at 1
#assign id for now (auto-inc)
$data->{feature}[0] = genMod::genMod_setVal($id, $data->{feature}[0]);
$data->{feature}[0] = $id
	if $data->{feature}[0] eq "";
my $useStoredLine = 1;
#now read entries from INFO file that are linked to this feature
while(1)
	{
	if($useStoredLine && defined($data->{lastLine}) &&
		$data->{lastLine} ne "" )
		{
		$line = $data->{lastLine};
		}
	else
		{
		last
			unless defined($line = <$trackInfoFh>);
		}
	$useStoredLine = 0;
	chomp $line;
	next if $line =~ /^\s*$/;
	$data->{lastLine} = $line;
	#note: 0 - id, 1 - hsp num, 3 - score, 7,8 - start,end (in protein)
	my @a = split(/\t/, $line);
	#if id of HSP is not current feature id, we've got all the HSPs
	#for this feature
	last 
		if $a[0] != $data->{feature}[0];
	#push( @{${%$data}{hsps}}, \@a);
	push( @{$data->{hsps}}, \@a);
	}
#warn caller if no HSPs found UNLESS $noHspOk == 1
if(!$noHspOk && (@{$data->{hsps}} == 0))
	{
	print "WARNING: feature id [$data->{feature}[0]] has no HSPs\n";
	}
#we can assume we found at least one HSP if we reach here;  if not,
#it shouldn't be a big deal (not a fatal error--just a warning)
return(1);
}


#PRECONDITIONS: 
#	1) info file must have clustered hsps in same order they appear in feature file
#	2) sfCount assumed to be 6th column in feature file and is the # of HSPS for a feature
#
sub loadNextFeatureV2(\*\*\%;$) #(*track_in_fh, track_info_in_fh, %data, $noHspOk=0)
{
my ($trackFh, $trackInfoFh, $data, $noHspOk) = @_;
$noHspOk = 0
	unless defined($noHspOk);
my $line;
$data->{id} = genMod::genMod_setVal(1, $data->{id});
#make sure feature id is at least 1 (no smaller)
$data->{id} = 1 
	if $data->{id} < 1;
@{$data->{feature}} = ();
@{$data->{hsps}} = ();
#read next line of trackFh (main track file)
my $fileDone = 1;
while($line = <$trackFh>)
	{
	chomp $line;
	next if $line =~ /^\s*$/;
	$fileDone = 0;
	last;
	}
#return that the files are all parsed if main track file all read
return(0) if $fileDone;
#parse line
@{$data->{feature}} = split(/\t/, $line);
my $sfCount = $data->{feature}[6];
#note: id (element 0) may be BLANK. we set based on $data{id}
my $id = $data->{id}++;  #inc first so we start at 1
#assign id for now (auto-inc)
$data->{feature}[0] = genMod::genMod_setVal($id, $data->{feature}[0]);
$data->{feature}[0] = $id
	if $data->{feature}[0] eq "";
for(my $i=0; $i < $sfCount; $i++)
	{
	last
		unless defined($line = <$trackInfoFh>);
	chomp($line);
	next if $line =~ /^\s*$/;
	#note: 0 - id, 1 - hsp num, 3 - score, 7,8 - start,end (in protein)
	my @a = split(/\t/, $line);
	#if id of HSP is not current feature id, we've got all the HSPs
	#for this feature
	last 
		if $a[0] != $data->{feature}[0];
	push( @{$data->{hsps}}, \@a);
	}
if(scalar(@{$data->{hsps}}) < $sfCount - 1)
	{
	my $n =scalar(@{$data->{hsps}});
	$@ = "WARNING: only $n hsps found and $sfCount expected for feature [$id]\n";
	}
#warn caller if no HSPs found UNLESS $noHspOk == 1
if(!$noHspOk && (@{$data->{hsps}} == 0))
	{
	print "WARNING: feature id [$data->{feature}[0]] has no HSPs\n";
	}
#we can assume we found at least one HSP if we reach here;  if not,
#it shouldn't be a big deal (not a fatal error--just a warning)
return(1);
}

#connects to the db using params in %data, user, pass, dbName.  Also inits the iterator
#index $data{fid}=1 so loadMod_dbLoadNextFeature() knows what fid we are on in $data{table}
#also finds the max fid in the table so loadMod_dbLoadNextFeature() can detect when
#a table has been processed
sub loadMod_dbPrepLoadFeatures(\$\%) #($dbh, %data)
{
my ($dbhRef, $data) = @_;
print "connecting to DB [$data->{dbName}]...\n";
my $host = "localhost";
$host = $data->{host}
	if defined($data->{host});
#my $dbiAttr = { RaiseError => 1, LongTruncOk => 0, mysql_local_infile => 1 }; FRK FIXME
my $dbiAttr = { mysql_local_infile => 1 };

$$dbhRef = dbMod::mysqlConnect($data->{user}, $data->{pass}, $data->{dbName}, $host, $dbiAttr);
#	DBI->connect("DBI:mysql:${%$data}{dbName}:$host", ${%$data}{user}, ${%$data}{pass}, $dbiAttr)
#		or die "$DBI::errstr\n";

print "done\n";
#now find the max id in the table and store in $data{lastFid}
my $str = "select id from $data->{mainTable}";
#if user has specified a starting id, grab only feature ids above it
if(defined($data->{id}))
	{
	$str .= " where id >= $data->{id}";
	}
my $q = ($$dbhRef)->prepare($str);
$q->execute();
my @row;
while(@row = $q->fetchrow_array())
	{
	push(@{$data->{fidList}}, $row[0]);
	}
@{$data->{fidList}} = sort {$b <=> $a} @{$data->{fidList}};
}


#loads next feature with fid=$data{fid} from table $data{infoTable} and all HSPs in
#$data{infoTable}.  
sub loadMod_dbLoadNextFeature($\%) #($dbh, %data)
{
my ($dbh, $data) = @_;

my $fid = pop(@{$data->{fidList}});
#print "fid: $fid\n";

#first get the feature
my $str = "select * from $data->{mainTable} where id=$fid";
my $q = $dbh->prepare($str);
$q->execute();
@{$data->{feature}} = $q->fetchrow_array();
#simple error checks
die "ERROR: table [$data->{mainTable}] has no feature with id [$fid]\n"
	if @{$data->{feature}} <= 0;
die "ERROR: table [$data->{mainTable}] contains mutliple features with id [$fid]\n"
	if $q->fetchrow_array();
#now get the HPSs
$str = "select * from $data->{infoTable} where id=$fid";
$q = $dbh->prepare($str);
$q->execute();
my @row;
while(@row = $q->fetchrow_array())
	{
	my @hsp = @row;
	push(@{$data->{hsps}}, \@hsp);
	}
return(1);
}

#load a range of features from the db
sub loadMod_dbLoadFeatureRange($$\%;$) #($startFid, $count, %data, $disconn=1)
{
my ($startFid, $count, $data, $disconn) = @_;
my $endFid = $startFid+$count-1;
my $host = genMod::genMod_setVal("localhost", $data->{host});
$disconn = genMod::genMod_setVal(1, $disconn);
my $dbh;
if($disconn == 1)
	{
	$dbh = dbMod::mysqlConnect($data->{user}, $data->{pass}, $data->{dbName}, $host);
	}
else
	{
	$dbh = $data->{dbh};
	}
$data->{features} = dbMod::mysqlDoQuery(
	"select * from $data->{mainTable} where id >= $startFid and id <= $endFid order by id", 
		$dbh);
foreach my $feature (@{$data->{features}})
	{
	my $hsps = 
		dbMod::mysqlDoQuery("select * from $data->{infoTable} where id=$feature->[0]",
					 $dbh);
	push(@{$data->{hsps}}, $hsps);
	}
$dbh->disconnect()
	if $disconn == 1;
return(@{$data->{features}} > 0);
}

#inits a db connection for loading all of the features of a scaffold at once
#(does a load for each strand of each scaffold)
sub loadMod_dbPrepLoadFeatScaff(\$\%) #($dbh, %data)
{
my ($dbhRef, $data) = @_;
#same as loadMod_dbPrepLoadFeaturesMerge, so call it
loadMod_buildScaffList($$dbhRef, %$data);
}

#queries db and loads the next set of 
sub loadMod_dbLoadFeatNextScaff($\%;$) #($dbh, %data, $skipHsp)
{
my ($dbh, $data, $skipHsp) = @_;
$skipHsp = 0
	unless defined($skipHsp);
#wipe previous data
@{$data->{features}} = ();
@{$data->{hsps}} = ();
return (0)
	if $data->{scaffNum} >= @{$data->{scaffList}};
my $curScaff = $data->{scaffNum}++; #store current scaff index and update
my $cmd = "SELECT * FROM $data->{mainTable} 
		   WHERE chrom='$data->{scaffList}[$curScaff][0]' 
		   	and strand='$data->{scaffList}[$curScaff][1]'";
my $q = $dbh->prepare($cmd);
$q->execute();
#get all the features on a scaffold and store
$data->{features}= $q->fetchall_arrayref();
return(1)
	if $skipHsp == 1;
#now get the hsps for each feature
foreach my $feature (@{$data->{features}})
	{
	my $infoCmd = "SELECT * FROM $data->{infoTable}
				   WHERE id = $feature->[0]";
	my $infoQ = $dbh->prepare($infoCmd);
	$infoQ->execute();
	#store reference to array for this feature's hsps
	push(@{$data->{hsps}}, $infoQ->fetchall_arrayref());
	}
return(1);
}

#connects to the database using params in data: user, pass, dbname.  Also loads in
#all of the unique fields in "chrom" in each table from @$data{mainTabList} and stores in
#@$data{table}{scaffList};  assumes that each table in mainTabList is from the db
sub loadMod_dbPrepLoadFeaturesMerge(\$\%) #($dbh, %data)
{
my ($dbhRef, $data) = @_;
$data->{metaFid} = 1;
loadMod_buildScaffList($$dbhRef, %$data);
}

#loads the set of features on scaffold $data{scaffList}[$fid] and builds a feature
#with those features as HSPs and stores in $data{feature}, $data{hsps}.
#Loads from tables $data{mainTabList}, $data{infoTabList};  $data{scaffNum} is the 
#index into $data{scaffList} of which scaffold we are building.  $data{metaFid} is the feature
#id of the meta-feature we are creating
#NOTE:
sub loadMod_dbLoadNextFeatureMerge(\$\%) #($dbh, %data)
{
my ($dbhRef, $data) = @_;

#wipe out previous feature/hsps
@{$data->{feature}} = map {0} (0..14); #init to 15-element array of 0's
@{$data->{hsps}} = ();

return(0) 
	if $data->{scaffNum} >= @{$data->{scaffList}};

fillCache($$dbhRef, $data->{scaffNum}, %$data, $data->{scaffPerQuery});

my $hspNum = 1;
#store what feature/scaff this feature represents
$data->{feature}[1] = $data->{scaffList}[$data->{scaffNum}][0] .
	$data->{scaffList}[$data->{scaffNum}][1];
$data->{feature}[2] = $data->{scaffList}[$data->{scaffNum}][0];
$data->{feature}[3] = $data->{scaffList}[$data->{scaffNum}][1];
my $i;
#for each table in $data{mainTabList}
my $scaff = $data->{scaffList}[$data->{scaffNum}][0];
for($i=0; $i < @{$data->{mainTabList}}; $i++)
	{
	#get meta-HSPs (features) that hit current scaffold for this table
	loadMod_dbLoadNextFeatTabMerge($$dbhRef, $scaff, $data->{metaFid}, $hspNum, 
		$data->{mainTabList}[$i], $data->{infoTabList}[$i], 
		$data->{scaffList}[$data->{scaffNum}][1], @{$data->{hsps}}, %$data);
	}
$data->{feature}[0] = $data->{metaFid}; #only need to store the feature id
#update counter vars
$data->{metaFid}++;
$data->{scaffNum}++;
return(1);
}

sub fillCache(\$$\%;$) #($dbh, $scaffNum, %data, $bufSize=100)
{
my ($dbhRef, $scaffNum, $data, $bufSize) = @_;
$bufSize = genMod::genMod_setVal(100, $bufSize);
my $realBufSize = 2*$bufSize;
$data->{cache}{count}--
	if defined($data->{cache}{count});
if(defined($data->{cache}{count}) && $data->{cache}{count} > 0)
	{
	return;
	}
$data->{cache} = {};

for(my $j=0; $j < @{$data->{mainTabList}}; $j++)
	{
	my $mainTable = $data->{mainTabList}[$j];
	my $infoTable = $data->{infoTabList}[$j];
	my $hitTable = $data->{mainTabList}[$j] . "Hit";
	my $lastScaff = $scaffNum+$realBufSize;
	my $scaffStr = "";
	for(my $i=$scaffNum; $i < $lastScaff && $i < @{$data->{scaffList}}; $i+=2)
		{
		$scaffStr .= ","
			if $scaffStr ne "";
		$scaffStr .= "'$data->{scaffList}[$i][0]'";
		}

	#my $hasHit = dbMod::tableExists($hitTable, $$dbhRef);
	my $hasHit = 0;
	my $eq = ($$dbhRef)->prepare("show tables");
	my $eok;
	eval { $eok = $eq->execute() };
	if ($@ || !$eok) {
	    # connection has failed - reopen the database connection
	    print STDERR ts() . "INFO: database error: \$@ is \"" . (defined($@) ? $@ : "undef") . "\"\n";
	    print STDERR "INFO: database error: \$eok is \"" . (defined($eok) ? $eok : "undef") . "\"\n";
	    print STDERR "INFO: RE-connecting to DB [$data->{dbName}]...\n";
	    my $host = "localhost";
	    $host = $data->{host} if defined($data->{host});
	    #my $dbiAttr = { RaiseError => 1, LongTruncOk => 0, mysql_local_infile => 1 }; FRK FIXME
	    my $dbiAttr = { mysql_local_infile => 1 };

	    #$$dbhRef = DBI->connect("DBI:mysql:${%$data}{dbName}:$host", ${%$data}{user}, ${%$data}{pass}, $dbiAttr) or die "ERROR: RE-connect failed: $DBI::errstr\n";
        $$dbhRef = dbMod::mysqlConnect($data->{user}, $data->{pass}, $data->{dbName}, $host, $dbiAttr);
	    print STDERR ts() . "INFO: done\n";
	    $eq = ($$dbhRef)->prepare("show tables");
	    $eq->execute() or die ts() . "ERROR: $DBI::errstr";
	} else {
	    print STDERR ts() . "INFO: no error\n";
	}
	my $row;
	while(($row) = $eq->fetchrow_array())
	{
	    $hasHit = 1 if($row eq $hitTable);
	}
	
	my $hitStr1 = "";
	my $hitStr2 = "";
	my $lengthCol = "1";
	if($hasHit)
	{
	    $lengthCol = "length";
	    $hitStr1 = ", $hitTable h";
	    $hitStr2 = " AND f.name=h.name";
	}

	my $str = "SELECT f.id,  start,  end,  strand,  f.name, sum(HspScore),  chrom,  $lengthCol,  sum(HspAlignLen), sum(HspIdentity), 
sum(HspPositive),  sum(HspHitTo - HspHitFrom + 1), sum(HspQueryTo - HspQueryFrom + 1) 
FROM $mainTable f, $infoTable i $hitStr1 WHERE chrom in ($scaffStr) AND f.id=i.id $hitStr2 GROUP BY f.id";
	print ts() . "str: [$str]\n"; # FRK
	my $q = ($$dbhRef)->prepare($str);
	$q->execute() or die ts() . "ERROR: $DBI::errstr\nSQL: [$str]\n";
	my $results = $q->fetchall_arrayref();
	print ts() . "count: " . $#{$results} . "\n"; # FRK
	foreach my $item (@$results)
		{
		push(@{$data->{cache}{$mainTable}{$item->[6]}{$item->[3]}}, $item);
		}
	}
$data->{cache}{count} = $realBufSize;
}

#loads all features from $table with chrom = $scaff. pushes meta-hsps into @$hsps
sub loadMod_dbLoadNextFeatTabMerge($$$\$$$$\@\%) #($dbh, $scaff, $metaFid, $hspNumRef, 
											#$mainTable, $infoTable, $strand, @hsps, $data)
{
my ($dbh, $scaff, $metaFid, $hspNumRef, $mainTable, $infoTable, $strand, $hsps, $data) = @_;
#we only need id, start, end, strand for each feature to build a meta-HSP
my $str = "SELECT f.id,start,end,strand,name, sum(HspScore),chrom FROM $mainTable f, $infoTable i 
WHERE chrom = '$scaff' AND strand='$strand' AND f.id=i.id GROUP BY f.id";
#my $q = $dbh->prepare($str);
my $results;
#$q->execute();
#$results = $q->fetchall_arrayref();
$results = $data->{cache}{$mainTable}{$scaff}{$strand};
$results = []
	unless defined($results);
foreach my $item (@$results)
	{
	my @row = @$item;
	#don't need to do separate query to get score for each feature anymore--we get it
	#with the join above
	#my $score = loadMod_calcFeatScore($dbh, $row[0], $infoTable);
	my $score = $row[5];
	next #not score means this feature has no hsps
		unless defined($score);
	#build the HSP
	my @hsp;
	$hsp[0] = $metaFid; #map this meta-hsp to it's current feature
	$hsp[1] = ($$hspNumRef)++;
	$hsp[2] = 0; #don't score bitscore
	#retrieve score for this feature
	$hsp[3] = $score;
	$hsp[4] = 0; #no evalue to store
	#don't need start/stop in "query" (protein for now
	#store query pos in both "hit" and "query"
	$hsp[5] = $row[1];
	$hsp[6] = $row[2];
	$hsp[7] = $row[1]; #start of this feature/MHSP in scaffold is hit/subject start
	$hsp[8] = $row[2]; #end   "		"		"		"		"		"
	$hsp[9] = 0; #the strand of the "query" is the strand in the protein (don't need)
	#the strand of the "hit" for this MHSP is not needed either since the + and - strand
	#are done as 2 separate runs
	$hsp[10] = 1; #hit strand
	$hsp[11] = $row[9]; #sum of identities
	$hsp[12] = $row[10]; #sum of positivies
	$hsp[13] = 0;
	$hsp[14] = $row[8]; #sum of alignment length
	@hsp[15..18] = (0,0,0,0);
	#store the original table name and feature id for later recovery of this feature
	$hsp[19] = $mainTable;
	$hsp[20] = $row[0];
	$hsp[21] = $row[7]; #store length of the hit
	$hsp[22] = $row[11]; #sum of hit length
	$hsp[23] = $row[12]; #sum of query length
	push(@$hsps, \@hsp);
	}
}

#returns the sum of scores of HSPs in $table
sub loadMod_calcFeatScore($$$) #($dbh, $fid, $table)
{
my ($dbh, $fid, $table) = @_;
my $str = "select SUM(HspScore) from $table where id=$fid";
my $q = $dbh->prepare($str);
$q->execute();
my @row = $q->fetchrow_array();
return($row[0]);
}

#for a given feature id (fid), table, finds the frame of the HSPs with that id
#(assumes they all have the same 
sub loadMod_getHitFrame($$$) #($dbh, $fid, $table)
{
my ($dbh, $fid, $table) = @_;
my $str = "select HspHitFrame from $table where id=$fid";
my $q = $dbh->prepare($str);
$q->execute();
my @row = $q->fetchrow_array();
#only worried about first row since all rows (hsps) should have same strand (if not frame)
return($row[0]);
}

#for a given table, feature id finds the min start and max end in the hit for all HSPs
#from that fid
sub loadMod_findStartEndHit($$$) #($dbh, $fid, $table)
{
my ($dbh, $fid, $table) = @_;
my $str = "select min(HspHitFrom), max(HspHitTo) from $table where id=$fid";
my $q = $dbh->prepare($str);
$q->execute();
my @row = $q->fetchrow_array();
die "failure in loadMod_findStartEndHit()\n"
	if @row != 1;
return(@row);
}

#$infile must be a file with one string per line. Empty lines are ignored and @arr
#will contain each nonempty line in $infile in the order they occur
sub loadMod_loadStringArr($\@;$\%) #($infile, @arr, $func, %data)
{
my ($infile, $arr, $func, $data) = @_;
$data = {} #empty hash if not sent
	unless defined($data);
local(*INF);
genMod::genMod_openFile(*INF, $infile) or die;
my $line;
while(defined($line = <INF>))
	{
	chomp($line);
	#apply function to line if requested
	$line = &{$func}($line, $data)
		if defined($func);
	next 
		if !defined($line) || $line =~ /^\s*$/;
	$line =~ s/^\s*//;
	$line =~ s/\s*$//;
	push(@$arr, $line)
		if defined($line); #func() may have undef'd to indicate we don't want to store
	}
close(INF);
}

#loads a hash from a file.  The file should have one key value pair on each line separted by
#$sep. 
#NOTE: loadHash() will skip all lines that do not appear "keystr = valstr" and/or
# begin with 0 or more whitespace and then #
#for example:
#		#this is a comment
#		this is a comment
#		#this = comment 
sub loadMod_loadHash($$\%) #($infile, $sep, %hash)
{
my ($infile, $sep, $hash) = @_;
local(*INF);
genMod::genMod_openFile(*INF, $infile) or die;
my $line;
while(defined($line = <INF>))
	{
	chomp $line;
	next if $line =~ /^\s*$/;
	my ($key, $val);
	#must have $sep in it otherwise considered a comment line
	if(($key, $val) = ($line =~ /^(.*?)${sep}(.*)$/))
		{
		$hash->{$key} = $val;
		}
	}
close(INF);
}

#loads a fasta file into a hash with key=defline, value = seq
sub loadMod_loadFastaHash($\%;$) #($infile, %hash, $nameFunc)
{
my ($infile, $hash, $nameFunc) = @_;
local (*INF);
genMod::genMod_openFile(*INF, $infile) or die;
my $line;
my $defLine = "";
while(defined($line = <INF>))
	{
	chomp $line;
	next if $line =~ /^\s*$/;
	if($line =~ /^\s*>(.*)/)
		{
		$defLine = $1;
		$defLine = &{$nameFunc}($defLine)
			if defined($nameFunc);
		if(!defined($defLine) || $defLine eq "")
			{
			die "ERROR processing defline [$1]\n";
			}
                        $hash->{$defLine} = "";
		}
	else
		{
		die "empty defline" if $defLine eq "";
		$hash->{$defLine} .= $line;
		}
	}
close(INF);
}

#loads a fasta file into an array of hashes (defLine, seq) (preserves order read in)
sub loadMod_loadFastaArr($\@;$) #($infile, @arr, $nameFunc)
{
my ($infile, $arr, $nameFunc) = @_;
local (*INF);
genMod::genMod_openFile(*INF, $infile) or die;
my $line;
my $defLine = "";
while(defined($line = <INF>))
	{
	chomp $line;
	next if $line =~ /^\s*$/;
	if($line =~ /^\s*>(.*)/)
		{
		$defLine = $1;
		$defLine = &{$nameFunc}($defLine)
			if defined($nameFunc);
		my %hash = (defLine => $defLine, seq => "");
		push(@$arr, \%hash);
		}
	else
		{
		die "empty defline" if $defLine eq "";
		$arr->[-1]{seq} .= $line;
		}
	}
close(INF);
}
#connects to database using $data{user}, $data{pass}, $data{dbName}.  Also queries
#table chromInfo to get a scaffold list and stores in @{$data{scaffList}}.  Inits
#iterator vars$data{scaffNum} = 0 for use of loadMod_dbLoadNextScaff();
sub loadMod_dbPrepLoadScaff(\$\%) #($dbh, %data)
{
my ($dbhRef, $data) = @_;
loadMod_buildScaffList($$dbhRef, %$data);
return(0);
print "connecting to DB [$data->{dbName}] and retrieving scaffold list...\n";
my $host = "localhost";
$host = $data->{host}
	if defined($data->{host});
#my $dbiAttr = { RaiseError => 1, LongTruncOk => 0, mysql_local_infile => 1 }; FRK FIXME
my $dbiAttr = { mysql_local_infile => 1 };
$$dbhRef = dbMod::mysqlConnect($data->{user}, $data->{pass}, $data->{dbName}, $host, $dbiAttr);
#$$dbhRef = 
#	DBI->connect("DBI:mysql:${%$data}{dbName}:$host", ${%$data}{user}, ${%$data}{pass}, $dbiAttr)
#		or die "$DBI::errstr\n";
#now get the distinct scaffold names
my $str = "select distinct(chrom) from chromInfo";
my $q = ($$dbhRef)->prepare($str);
$q->execute();
my @row;
@{$data->{scaffList}} = ();
#store in $data{scaffList}
while(@row = $q->fetchrow_array())
	{
	die "incorrect number of columns returned in loadMod_dbPrepLoadFeatures()\n"	
		if @row != 1;
	#for each scaffold, we store a 2d array indicating scaffold name and strand
	push(@{$data->{scaffList}}, [$row[0], "+"]);
	push(@{$data->{scaffList}}, [$row[0], "-"]);
	}
print "done\n";
$data->{scaffNum} = 0;
}

#loads the next scaffold pointed to by $data{scaffList}[$data{scaffNum}].  
#creates: @{$data{featureList}} is an array of hashes with @{feature} and @{hsps} 
#containg the raw data from the database
#Loads from tables specified in @{$data{mainTabList}} and
#@{$data{infoTabList}}
sub loadMod_dbLoadNextScaff($\%) #($dbh, %data)
{
my ($dbh, $data) = @_;

#wipe out previously stored features/hsps
@{$data->{featureList}} = ();

#check if we are done
return(0) if $data->{scaffNum} >= @{$data->{scaffList}};

my $i;
#load the features/hsps for each table
for($i=0; $i < @{$data->{mainTabList}}; $i++)
	{
	loadMod_dbLoadNextScaffTab($dbh, $i, %$data);
	}
$data->{scaffNum}++;
return(1);
}


#loads the features in from a given scaffold ($data{scaffList}[$data{scaffNum}]) from
#a given table ($data{mainTabList}[$tableInd])
sub loadMod_dbLoadNextScaffTab($$\%) #($dbh, $tableInd, %data)'
{
my ($dbh, $tableInd, $data) = @_;

#do query of main table
my $str = "select * from $data->{mainTabList}[$tableInd] ";
$str .= "where chrom = '$data->{scaffList}[$data->{scaffNum}][0]' ";
$str .= "and strand = '$data->{scaffList}[$data->{scaffNum}][1]'";

my $q = $dbh->prepare($str);
$q->execute();
my @row;
my $fNum = scalar(@{$data->{featureList}}); #current index of feature we are storing
while(@row = $q->fetchrow_array())
	{
	#save the feature
	my @feature = @row;
	my @hsps;
	#store the original table name and feature id
	push(@feature, ($data->{mainTabList}[$tableInd], $feature[0]));
	#do query of Info table
	$str = "select * from $data->{infoTabList}[$tableInd] ";
	$str .= "where id=$feature[0]";
	my $infoQ = $dbh->prepare($str);
	$infoQ->execute();
	my @hspRow;
	while(@hspRow = $infoQ->fetchrow_array())
		{
		my @hsp = @hspRow;
		push(@hsps, \@hsp)
		}
	#store in @data{featureList} (ref to hash which is ref to two arrays
	push(@{$data->{featureList}}, {feature => \@feature, hsps => \@hsps});
	#inc feature count
	$fNum++;
	}
}

#parses a config file on a host to get the information about a given DB.  that info is
#the host the db is on as well as the username and password to access the db on that host.
#if the host is not found, and there is a default entry, that is returned.  Else,
# "" is returned for all 3 values
sub loadMod_getDbConfig($;$$) #($db, $webHost, $configFile)
{
my ($db, $webHost, $configFile) = @_;
local(*INF);
($webHost) = (`hostname` =~ /^(\w+)\.?/)
	unless defined($webHost);
my $BASEDIR = ($0 =~ m{(.*?/)bin/})[0];
$configFile = "${BASEDIR}web/cgi/conf/dbHosts.conf"
	unless defined($configFile);
genMod::genMod_openFile(*INF, $configFile) or die;
my $line;
my @defList; #list of default hosts to use (general)
my @dbDefList; #list of default hosts for a given database from "default" host
my @hostDefList; #list of default hosts to use (general)
my $curHost = "";
my @itemList; #array of triples (host, user, pass)
while(defined($line = <INF>))
	{
	chomp $line;
	next if $line =~ /^\s*$/; #empty line
	next if $line =~ /^\s*#/; #comment
	$line =~ s/^\s*//; #remove any leading whitespace
	$line =~ s/\s*$//; #remove any trailing whitespace
	if($line =~ /\[(\w+)\]/)
		{
		$curHost = $1;
		next;
		}
	#only examine this section if it is for $webHost or default
	next
		unless $curHost eq $webHost || lc($curHost) eq "default";
	my @arr = split(/\s*,\s*/, $line);
	my ($database, $host, $user, $pass, $userW, $passW);
	$database = "";
	if(@arr == 6)
		{
		($database, $host, $user, $pass, $userW, $passW) = @arr;
		}
	elsif(@arr == 4)
		{
		($database, $host, $user, $pass) = @arr;
		$userW = "";
		$passW = "";
		}
	else
		{
		($host, $user, $pass, $userW, $passW) = @arr;
		}
	if(@arr == 3 || @arr == 5) #default lines
		{
		#if only one item on a line, that is the default host
		#store as either a default for this webhost or as general defaul
		if($curHost eq "default") #general
			{
			#note that items are left-shifted one
			push(@defList, [$host, $user, $pass, $userW, $passW]);
			}
		else #specific
			{
			#note that items are left-shifted one
			push(@hostDefList, [$host, $user, $pass, $userW, $passW]);
			}
		}
	elsif(lc($curHost) eq "default" && $database eq $db) 
		{
		#this is for specific dbs from "default" host
		push(@dbDefList, [$host, $user, $pass, $userW, $passW]);
		}
	#if this is the entry for $webHost and $database, we're done
	if(($curHost eq $webHost) && ($db eq $database))
		{
		push(@itemList, [$host, $user, $pass, $userW, $passW]);
		}
	}
close(INF);
#if specific hosts exist for this database
if(@itemList > 0)
	{
	my $i = int(rand(scalar(@itemList)));
	#print "<!-- host: [$itemList[$i][0]] -->\n";
	return(@{$itemList[$i]});
	}
elsif(@hostDefList > 0) #use one of the default hosts for this webhost
	{
	my $i = int(rand(scalar(@hostDefList)));
	#print "<!-- host: [$hostDefList[$i][0]] -->\n";
	return(@{$hostDefList[$i]});
	}
elsif(@dbDefList > 0) #no entry for this host, but one for this db on default
	{
	my $i = int(rand(scalar(@dbDefList)));
	#print "<!-- host: [$dbDefList[$i][0]] -->\n";
	return(@{$dbDefList[$i]});
	}
else #use one of the general default hosts 
	{
	my $i = int(rand(scalar(@defList)));
	#print "<!-- host: [$defList[$i][0]] -->\n";
	return(@{$defList[$i]});
	}
}


#parses a config file on a db and returns information about the originating
#webHost.  Returns webHost or default ('*') if one exists, else returns ""
sub loadMod_getWebHost($;$) #($db, $configFile)  
{
my ($db, $configFile) = @_;
local(*INF);
my $BASEDIR = ($0 =~ m{(.*?/)bin/})[0];
$configFile = "${BASEDIR}web/cgi/conf/webHosts.conf"
    unless defined($configFile);
genMod::genMod_openFile(*INF, $configFile) or die;
my $line; 
while(defined($line = <INF>))
    {
    chomp $line;
    next if $line =~ /^\s*$/; #empty line
    next if $line =~ /^\s*#/; #comment
    $line =~ s/^\s*//; #remove any leading whitespace
    $line =~ s/\s*$//; #remove any trailing whitespace
    if($line =~ /^(\w+)\s*,\s*(\w+)/)
        {
        if ( ( $1 eq $db ) || ( $1 eq '*' ) )
            {
            return $2;
            }
		}
	}
close(INF);
return("");
}


#parses the user conf file to see what db's the htaccess auth'd user can access.
#returns 1 iff the user is allowed to access the specified db
sub loadMod_checkUserCap($$;$$) #($db, $printMsg, $host, $configFile)
{
my ($db, $printMsg, $host, $configFile) = @_;
($host) = (`hostname` =~ /^(\w+)\./)
	unless defined($host);
my $htUser = $ENV{REMOTE_USER};
#if not remote user, then this is on a non-htaccess protected machine and
#we cannot verify user cap list (hence, assume public server)
return(1) 
	unless defined($htUser);
local(*INF);
my $BASEDIR = ($0 =~ m{(.*?/)bin/})[0];
$configFile = "${BASEDIR}web/cgi/conf/userCL.conf"
	unless defined($configFile);
genMod::genMod_openFile(*INF, $configFile) or die;
my $line;
my $retVal = -1; #-1 means no specific accept/reject yet
my $curHost = "";
my %defCap = ();
my $defClStr = "";
my %macros; #store any macros defined
while(defined($line = <INF>))
	{
	chomp $line;
	next if $line =~ /^\s*$/; #empty line
	next if $line =~ /^\s*#/; #comment
	$line =~ s/^\s*//; #remove any leading whitespace
	$line =~ s/\s*$//; #remove any trailing whitespace
	#store any macros we find (ex: %x = 'val')
	if($line =~ /^\s*\%(.*?)\s*=\s*'(.*)'\s*$/)
		{
		my ($mac, $val) = ($1, $2);
		$macros{$mac} = $val;
		next;
		}
	#expand any macros
	#print "<!--gm::macroExp: $genMod::{macroExp}-->\n";
	$line = genMod::macroExp($line, %macros);
	#store what host block we are in
	if($line =~ /\[(\w+)\]/)
		{
		$curHost = $1;
		next;
		}
	#skip lines until we are in the host section for our host or default
	next 
		unless $curHost eq $host || lc($curHost) eq "default";
	my ($user, $clStr) = ($line =~ /(\w+)\s*:\s*(.*?)$/);
	my %cSet;
	%cSet= map {$_ => 1} split(/\s*,\s*/, $clStr)
		if $clStr ne "*";
	#if we find the capability list for this user, check
	if($user eq $htUser)
		{
		#if this is a host-specific cap, we can determine now
		if(lc($curHost) ne "default")
			{
			#else, check capabilities for this db
			$retVal = defined($cSet{$db});
			#if we have reason to let the user in already, do so
			return(1)
				if $clStr eq "*" || $retVal;
			#else, this user is not allowed 
			return(0);
			}
		else #store for checking after we're done parsing config file
			{
			$defClStr = $clStr;
			%defCap = %cSet;
			}
		}
	}
close(INF);
#if we make it here, it means no allow/deny has been found for the specific host
$retVal = defined($defCap{$db}) || $defClStr eq "*"
	if $retVal == -1;
#display message if asked
if($printMsg && ($retVal == 0))
	{
	$retVal = 0; #isn't really 0, so set
	print "ERROR: user [$htUser] is not allowed to access database [$db]\n";
	}
return($retVal); 
}

#connects to the database using params in data: user, pass, dbname.  Connects to
#localhost by default, or $data{host} if defined.  Also loads in
#all of the unique fields in "chrom" in in the chromInfo table and stores in
#@$data{table}{scaffList}; 
sub loadMod_buildScaffList(\$\%) #($dbh, %data)
{
my ($dbhRef, $data) = @_;
#first connect to DB
my $host = "localhost";
$host = $data->{host}
	if defined($data->{host});
print "connecting to DB [$data->{dbName}] on host [$host]...\n";
#my $dbiAttr = { RaiseError => 1, LongTruncOk => 0, mysql_local_infile => 1 }; FRK FIXME
my $dbiAttr = { mysql_local_infile => 1 };
$$dbhRef = dbMod::mysqlConnect($data->{user}, $data->{pass}, $data->{dbName}, $host, $dbiAttr);
	#DBI->connect("DBI:mysql:${%$data}{dbName}:$host", ${%$data}{user}, ${%$data}{pass}, $dbiAttr)
	#	or die "$DBI::errstr\n";
print "done\n";
if(defined($data->{scaffList}))
	{
	print "using user specified scaffold list\n";
	#caller has specified a list of scaffolds.  Convert to the +/- separation
	my @tmp;
	foreach my $scaff (@{$data->{scaffList}})
		{
		push(@tmp, [$scaff, "+"]);
		push(@tmp, [$scaff, "-"]);
		}
	$data->{scaffList} = \@tmp;
	}
else
	{
	#if the caller has not already specified a list of scaffolds, retrieve
	print "retrieving scaffold list...\n";
	#now get the distinct scaffold names
	my $str = "select distinct(chrom) from chromInfo";
	my $q = ($$dbhRef)->prepare($str);
	$q->execute();
	my @row;
	@{$data->{scaffList}} = ();
	#store in $data{scaffList}
	while(@row = $q->fetchrow_array())
		{
		die "incorrect number of columns returned in loadMod_dbPrepLoadFeatures()\n"	
			if @row != 1;
		push(@{$data->{scaffList}}, [$row[0], "+"]);
		push(@{$data->{scaffList}}, [$row[0], "-"]);
		}
	print "done got " . (scalar(@{$data->{scaffList}}) / 2) . " scaffolds\n";
	}
$data->{scaffNum} = 0;
}

#opens a file for iteration via fasta file iterator.  Actually opens file
#and then stores info about the iterator (typically last line read, if any)
sub loadMod_fastaIterOpen(\*$;$) #(FH, $fileStr, $failOk)
{
my ($fh, $fileStr, $failOk) = @_;
$failOk = 0
	unless defined($failOk);
genMod::genMod_openFile(*{$fh}, $fileStr, $failOk) or return(0);
#init to empty buf
$fastaUtil::iterInfo{fileno($fh)}{buf} = "";
return(1);
}

#fasta file iterator.  given an open file handle, reads the next fasta sequence
#and optionally applies functions
sub loadMod_fastaIterNext(\*;$$) #(*FH, $defFunc, $seqFunc)
{
my ($fh, $defFunc, $seqFunc) = @_;
my $fDesc = fileno($fh);
die "ERROR: loadMod_fastaIterNext: file handle not open\n" if (!defined($fDesc) || !defined($fastaUtil::iterInfo{$fDesc}) || !defined($fastaUtil::iterInfo{$fDesc}{buf}));
my $line;
my $defLine = "";
my $seq = "";
my $first = 1;
#read buf first
while((($line = $fastaUtil::iterInfo{$fDesc}{buf}) ne "") || defined($line = <$fh>))
	{
	#flush buffer
	$fastaUtil::iterInfo{$fDesc}{buf} = "";
	chomp $line;
	next if $line =~ /^\s*$/;
	if($line =~ /^\s*>(.*)/)
		{
		if(!$first)
			{
			#put line in buffer
			$fastaUtil::iterInfo{$fDesc}{buf} = $line;
			#now exit
			last;
			}
		else
			{
			$first = 0;
			}
		$defLine = $1;
		#apply defline function if pasesd in
		$defLine = &{$defFunc}($defLine)
			if defined($defFunc) && $defFunc != 0;
		}
	else
		{
		die "ERROR: empty defline" if $defLine eq "";
		$line =~ s/\s+//g;
		$seq .= $line;
		}
	}
if($seq ne "" || $defLine ne "")
	{
	#apply function if passed in
	$seq = &{$seqFunc}($seq)
		if defined($seqFunc) && $seqFunc != 0;
	return($defLine, $seq);
	}
return();
}

#closes a filehandle and also undefs the buffer
sub loadMod_fastaIterClose(\*) #(FH)
{
my ($fh) = @_;
die "ERROR: loadMod_fastaIterClose: file handle not open\n" if (!defined(fileno($fh)));
undef($fastaUtil::iterInfo{fileno($fh)});
close($fh);
}


###this set of functions indexes a $delim-delimited file by any set of colums (index key is
#col order concat'd by $delim). \n is assumed row-terminator.
#This allows efficient retrieval of rows by  index has as a value a list of pairs
#	(start, length)
#The number of items in each block may be determined by counting \n or splitting (not stored
#to save space in memory)
sub indexMainAndInfoFile(\@$\%) #(@cols, $file, %index)
{
my ($cols, $file, $index) = @_;
}

#loads all the features on one scaffold into + feature and a - feature
#assumes that the files are both in "scaffold order"
#and that a featureCount is passed in each time
#%fm,%hm are maps from column name to index position in the tab-delim file.  if empty
#hashes, the function files
sub loadByScaffFeatAsHSP(\@\@$;\%\%) #(@mainFHList, @infoFHList, $fCount,%fm,%hm)
{
my ($mainFHList, $infoFHList, $fCount, $fm, $hm) = @_;
if(keys(%$fm) == 0)
	{
	#buildColMapFromFile($\%;$);#($file, %colMap, $dir="/home/analysis/genome_browser/customDb")
	}
if(keys(%$hm) == 0)
	{
	#buildColMapFromFile($\%;$);#($file, %colMap, $dir="/home/analysis/genome_browser/customDb")
	}

}

#for a given open file, loads one feature and returns a meta-HSP for it
sub loadOneFileScaff($$$\%\%) #($scaff, $fCount, $fh, %fm, %hm)
{
my ($scaff, $fCount, $fh, $fm, $hm) = @_;


}

#loads the score for a feature from an info file (sums the hsp's scores)
sub loadByScaffFeatScore($$\%) #($num,$fh,%hm)
{
my ($num, $fh, $hm) = @_;
my $sum = 0;
for(my $i=0; $i <= $num; $i++)
	{
	my $line;
	if(!defined($line = <$fh>))
		{
		#premature end of file
		$@ = "ERROR: premature end of file in loadMod::loadByScaffFeatScore()\n";
		return(undef);
		}
	chomp($line);
	next
		if $line =~ /^\s*$/;
	my @hsp = split(/\t/, $line);
	$sum += $hsp[$hm->{HspScore}];
	}
return($sum);
}


sub ts {
    my $tm = localtime;
    my $date = sprintf "%04d.%02d.%02d.%02d.%02d.%02d ", $tm->year+1900, $tm->mon+1, $tm->mday, $tm->hour, $tm->min, $tm->sec;
    return $date;
}


######################################

#must have a new function
sub new()
{
my $self = {};
bless $self;
return($self);
}

#must have a DESTROY function 
sub DESTROY()
{
}


} #END loadMod
###########################

return 1;
