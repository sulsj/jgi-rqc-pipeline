use strict;

############################
#BEGIN MODULE_NAME
{

#directive stuff
package fastaUtil;
require Exporter;
#add any paths for use/require directives

#any other modules needed
use genMod;
use DB_File;
use parseMod;

#exporting not all that important with objects/classes (I *think*)
our @ISA = qw(Exporter);
our @EXPORT = qw(); #export by default
#export by request
our @EXPORT_OK = qw(fastaIterOpen fastaIterNext fastaIterClose filterSeqUnique
	indexFastaFile getFastaSeq); 

#symbol list: list reelvant symbols here
#my $BASEDIR = ($0 =~ m{(.*?/)bin/})[0];
#our $fastaIndexDir = "${BASEDIR}.fastaIndex";

sub BEGIN()
{
#METHOD PROTOTYPES - put method prototypes here in order to have
#them executed at the time the module is compiled

#fasta iterator
sub fastaIterOpen(\*$;$); #(FH, $fileStr, $failOk)
sub fastaIterNext(\*;$$); #(*FH, $defFunc, $seqFunc)
sub fastaIterClose(\*); #(FH)

#misc
sub filterSeqUnique($\@;$$$); #($outFile, @inFiles, $seqWidth=70, $defLineFunc)

#fasta index utils
sub indexFastaFile($;\%); #($file, %index = undef)
sub getFastaSeq($$;$$); #($defline, $file, $start, $length)
sub readSeqFromFile($$$;$$); #($fh, $offset, $seqLen, $start, $reqLen)

#used to filter fasta files
sub buildSeqInfo($\%;$$); #($file, %seqInfo, $maxLen, $minLen);
sub randSubSetByNum($$\%); #($num, $totNum, %seqInfo)
sub randSubSetBySize($$\%); #($size, $totSize, %seqInfo)
}


########################

#put actual functions here

#opens a file for iteration via fasta file iterator.  Actually opens file
#and then stores info about the iterator (typically last line read, if any)
sub fastaIterOpen(\*$;$) #(FH, $fileStr, $failOk)
{
my ($fh, $fileStr, $failOk) = @_;
$failOk = 0
	unless defined($failOk);
genMod::genMod_openFile(*{$fh}, $fileStr, $failOk) or return(0);
#init to empty buf
$fastaUtil::iterInfo{fileno($fh)}{buf} = "";
return(1);
}

#fasta file iterator.  given an open file handle, reads the next fasta sequence
#and optionally applies functions
sub fastaIterNext(\*;$$) #(*FH, $defFunc, $seqFunc)
{
my ($fh, $defFunc, $seqFunc) = @_;
my $fDesc = fileno($fh);
my $line;
my $defLine = "";
my $seq = "";
my $first = 1;
my $enteredLoop = 0;
#read buf first
while((($line = $fastaUtil::iterInfo{$fDesc}{buf}) ne "") || defined($line = <$fh>))
	{
	#flush buffer
	$fastaUtil::iterInfo{$fDesc}{buf} = "";
	chomp $line;
	next if $line =~ /^\s*$/;
	if($line =~ /^\s*>(.*)/)
		{
		$enteredLoop = 1; #found at least one seq
		if(!$first)
			{
			#put line in buffer
			$fastaUtil::iterInfo{$fDesc}{buf} = $line;
			#now exit
			last;
			}
		else
			{
			$first = 0;
			}
		$defLine = $1;
		#apply defline function if pasesd in
		$defLine = &{$defFunc}($defLine)
			if defined($defFunc) && $defFunc != 0;
		}
	else
		{
		die "ERROR: empty defline" if $defLine eq "";
		$line =~ s/\s+//g;
		$seq .= $line;
		}
	}
if($enteredLoop) #$seq ne "" || $defLine ne "")
	{
	#apply function if passed in
	$seq = &{$seqFunc}($seq)
		if defined($seqFunc) && $seqFunc != 0;
	return($defLine, $seq);
	}
return();
}

#closes a filehandle and also undefs the buffer
sub fastaIterClose(\*) #(FH)
{
my ($fh) = @_;
undef($fastaUtil::iterInfo{fileno($fh)});
close($fh);
}

#this function takes a list of input fasta files and uses DBM to filter out duplicate
#sequences (duplicates detected by identical deflines)
sub filterSeqUnique($\@;$$$) #($outFile, @inFiles, $seqWidth=70, $defLineFunc, $tmpFile)
{
my ($outFile, $inFiles, $seqWidth, $defLineFunc, $tmpFile) = @_;
$seqWidth = genMod::genMod_setVal(70, $seqWidth);
my %defLines;
$tmpFile = "/tmp/fastaUtil.pm.tmp.$$" if (!defined ($tmpFile));
$SIG{TERM} = sub {unlink($tmpFile);};
$SIG{INT} = sub {unlink($tmpFile);};
dbmopen(%defLines, $tmpFile, 0666)
	or die "ERROR: dbmopen() failed [$!]\n";
local(*INF, *OUTF);
my ($defLine, $seq);
genMod::genMod_openFile(*OUTF, "> $outFile") or die;
foreach my $file (@$inFiles)
	{
	fastaIterOpen(*INF, $file);
	while(($defLine, $seq) = fastaIterNext(*INF, $defLineFunc))
		{
		if(!defined($defLines{$defLine}))
			{
			$defLines{$defLine} = 1;
			$seq =~ s/(.{$seqWidth})/$1\n/g;
			print OUTF ">$defLine\n$seq\n";
			}
		}
	fastaIterClose(*INF);
	}
close(OUTF);
dbmclose(%defLines);
}


#index a fasta file for quick sequence retrieval.  index file is of format:
#
#<last modification time>
#<defline1>, <offset1>, <len1>
#<defline2>, <offset2>, <len2>
#<defline3>, <offset3>, <len3>
#
#
#also stores index in mem if a hash is passed in
sub indexFastaFile($;\%) #($file, %index=undef)
{
my ($file, $index) = @_;
my $size = -s $file;
my $mod = -M $file;
local(*INF, *OUTF);
genMod::genMod_openFile(*INF, $file) or die;
#my $fname = parseMod::parseMod_getFName($file);
#genMod::genMod_openFile(*OUTF, ">$fastaIndexDir/$fname.$size") or die;
genMod::genMod_openFile(*OUTF, ">$file.S$size") or die;
print OUTF "$mod\n";
my $len = -1;
my $pos = -1;
my $def;
while(defined(my $line = <INF>))
	{
	chomp($line);
	if($line =~ /\s*>(.*)$/)
		{
		if($len != -1)
			{
			#store the exact defline, position of sequence start, and the length
			#of the sequence
			print OUTF "$def,$pos,$len\n";
			if(defined($index))
				{
				$index->{$def} = [$pos, $len];
				}
			}
		$len = 0;
		$pos = -1;
		$def = $1;
		}
	else
		{
		#store the location of the start of the actual sequence
		$pos = tell(INF) - (length($line) + 1)
			if $pos == -1;
		$len += length($line) + 1;
		}
	
	}
print OUTF "$def, $pos, $len\n";
$index->{$def} = [$pos, $len];
close(INF);
close(OUTF);
}


#get sequence for a given defline from a fasta file. undef if error (not found, etc)
sub getFastaSeq($$;$$) #($defline, $file, $start, $reqLen)
{
my ($defline, $file, $start, $reqLen) = @_;
local(*INF);
#my $fname = parseMod::parseMod_getFName($file);
#my $indexFile = "$fastaIndexDir/$fname." . -s $file;
my $indexFile = "$file.S" . -s $file;
if(!(-e $indexFile))
	{
	indexFastaFile($file);
	}
genMod::genMod_openFile(*INF, $indexFile) or die;
my $modTime = <INF>;
chomp($modTime);
if(-M $file < $modTime)
	{
	close(INF);
	indexFastaFile($file);
	genMod::genMod_openFile(*INF, $indexFile) or die;
	$modTime = <INF>;
	chomp($modTime);
	}
my $offset;
my $seqLen;
while(defined(my $line = <INF>))
	{
	chomp($line);
	my ($def, $off, $len) = ($line =~ /^(.*)\s*,\s*(\d+)\s*,\s*(\d+)$/);
	if($def eq $defline)
		{
		$offset = $off;
		$seqLen = $len;
		last;
		}
	}
close(INF);
if(!defined($offset))
	{
	return(undef);
	}
genMod::genMod_openFile(*INF, $file) or die;
my $seq = readSeqFromFile(\*INF, $offset, $seqLen, $start, $reqLen);
close(INF);
return($seq);
}

#with a file handle + offset,len, returns the portion from the file (text)
sub readSeqFromFile($$$;$$) #($fh, $offset, $seqLen, $start, $reqLen)
{
my ($fh, $offset, $seqLen, $start, $reqLen) = @_;
seek($fh, $offset, 0);
my $seq = undef;
read($fh, $seq, $seqLen);
$seq =~ s/\s+//g;
if(defined($start))
	{
	my $retLen = $seqLen-$start + 1;
	if(defined($reqLen))
		{
		$retLen = $reqLen;
		if($retLen + $start - 1 > $seqLen)
			{
			$retLen = $seqLen - $start + 1;
			}
		}
	$seq = substr($seq, $start, $retLen);
	}
return($seq);
}

#scans a file and builds seqInfo with keys as deflines (unchanged except '>') removed
#and values the length of the sequences. optionally filters out sequences too long 
#or too short.  Check $@ for non-fatal errorss
sub buildSeqInfo($\%;$$) #($file, %seqInfo, $maxLen, $minLen);
{
my ($file, $seqInfo, $maxLen, $minLen) = @_;
#TODO: check that minLen is > 0, $maxLen > 0
$@ = "";
if($maxLen < 1)
	{
	$@ = "fastaUtil::buildSeqInfo(): max length < 1. Turning off max filter\n";
	$maxLen = undef;
	}
if($maxLen < 0)
	{
	$@ = "fastaUtil::buildSeqInfo(): min length < 0. Turning off max filter\n";
	$maxLen = undef;
	}
if(defined($maxLen) && defined($minLen) && $maxLen < $minLen)
	{
	$@ .=
	 "fastaUtil::buildSeqInfo(): max length [$maxLen] smaller than min length [$minLen]\n"
	 	. "NO LENGTH FILTERING DONE DUE TO INCONSISTEN PARAMETERS\n";
	$maxLen = $minLen = undef; #turn off length filtering
	}
local(*INF);
fastaIterOpen(*INF, $file);
my $numSeq = 0;
my $totSize = 0;
while(my ($def, $seq) = fastaIterNext(*INF))
	{
	my $len= length($seq);
	if(defined($maxLen) && $len > $maxLen)
		{
		next;
		}
	if(defined($minLen) && $len < $minLen)
		{
		next;
		}
	$seqInfo->{$def} = $len;
	$totSize += $len;
	$numSeq++;
	}
fastaIterClose(*INF);
return($numSeq, $totSize);
}

#deletes defline/size pairs until the right number of sequences are left in the hash
#NOTE: DESTRUCTIVE MOD TO %seqInfo
sub randSubSetByNum($$\%) #($num, $totNum, %seqInfo)
{
my ($num, $totNum, $seqInfo) = @_;
if($num > $totNum)
	{
	warn("WARNING: requesting [$num] random sequences where only [$totNum] exist\n" .
			"Hash is not being modified");
	return(1); #ok for no, may change codes later
	}
if($num == 0)
	{
	$@ = "ranSubSetByNum: Requesting 0 sequences.  Why?\n";
	return(0);
	}
if($totNum == 0)
	{
	$@ = "ranSubSetByNum: no sequences to choose from! Why?\n";
	return(0);
	}
my $taken = $totNum;
my $frac = $num/$totNum; #if we had perfect distribution, one iteration of the sequence
						#with weighted coin flips by this, we'd get the number we need
while($taken > $num)
	{
	foreach my $def (keys(%$seqInfo))
		{
		my $p = rand();
		if($p > $frac) #toss
			{
			delete($seqInfo->{$def});
			$taken--;
			}
		}
	}
return(1); #success
}

#selects a random subset of sequences with size near $size (takes first collection
#of sequences >= $size)
sub randSubSetBySize($$\%) #($size, $totSize, %seqInfo)
{
}

#must have a new function
sub new()
{
my $self = {};
bless $self;
return($self);
}

#must have a DESTROY function 
sub DESTROY()
{
}


} #END fastaUtil
###########################
return 1;
