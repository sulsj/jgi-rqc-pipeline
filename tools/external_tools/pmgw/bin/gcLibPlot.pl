#!/usr/bin/env perl

use strict;
use Carp;

use Cwd;
use Cwd 'abs_path';
use File::Basename;
use File::Path;
use Getopt::Long;
use FindBin qw($RealBin);
use lib "$RealBin/../lib";
use FIN_Commands;
use FIN_Constants;

if ( @ARGV != 2 ) {
    print STDERR "Usage: gcLibPlot.pl <inputFile> <description>\n";
    exit 1;
}

my ($infile, $description) = @ARGV;

if ( !-e $infile ) {
    print STDERR "Cannot find your file $infile.\n";
    exit 1;
}

# Set library path environment variable to get R working properly.
#
$ENV{LD_LIBRARY_PATH} = "/usr/lib:$ENV{LD_LIBRARY_PATH}";

my $R = "/jgi/tools/bin/R";
my $outfile = $infile . ".jpg";
my $tmpfile = $infile . ".r.temp";
my $rCmd = <<EOF;
gc<-read.table("$infile", header=TRUE);
rangeX = range(round(min(gc[,5]) - 5, -1), round(max(gc[,5])+5, -1))
jpeg("$outfile", quality=100, height=480, width=640)
xsub<-sprintf("avg=%.2f%%, std dev=%.2f%%, max=%.2f%%, min=%.2f%%", mean(gc[,5]), sd(gc[,5]), max(gc[,5]),  min(gc[,5]))
hist(gc[,5], breaks = 200, col=8, border=1, xlab="GC(%)", ylab="# of Reads", main="GC Histogram for $description", xlim=rangeX, sub=xsub)
axis(1, seq(rangeX[1], rangeX[2], by=5)) 
axis(2)
box()
dev.off()
EOF

open OFILE, ">$tmpfile" or die "ERROR: failed to create $tmpfile: $!\n";
print OFILE $rCmd;
close OFILE;

my $cmd = (R_EXE) . " --no-save < $tmpfile";
print "\nCMD:\n$cmd\n";
system($cmd);
unlink $tmpfile;
exit 0;

