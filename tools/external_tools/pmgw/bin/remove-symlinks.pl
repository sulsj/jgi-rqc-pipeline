#!/usr/bin/env perl

print "Deprecated\n";
exit(0);

use strict;

use lib "/home/prefin/lib";

use Cwd;
use Getopt::Long;

use FindBin;
use lib "$FindBin::Bin/../lib";

use FIN_Commands;
use FIN_Constants;

use vars qw($optAnalysisDir $optLibrary $optProject);

# input val
#
if( !GetOptions(
                "a=s" =>\$optAnalysisDir,
		"p=s" =>\$optProject,
		"l=s" =>\$optLibrary
		) ) {
    help();
}

unless ($optProject) {
    help();
}

unless ($optLibrary) {
    help();
}

unless ($optAnalysisDir) {
    help();
}

my $topDir = getcwd();

chdir $optAnalysisDir;

my $workingDir = getcwd();

my $assemblyDir = "${workingDir}/${optLibrary}_mi98ml80/assembly";
my $lucyDir = "${workingDir}/${optLibrary}_lucy";
my $trimDir = "${workingDir}/${optLibrary}_trim";
my $listsDir = "${workingDir}/${optLibrary}_lists";


my $cmd = '';

$cmd = "rm -f ${trimDir}/${optProject}.nr.lucy.99.3.fasta";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${trimDir}/${optProject}.454TrimmedReads.nr.fna";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/${optProject}.454AllContigs.lucy.fa";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/${optProject}.454SingletonReads.lucy.fa";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/${optProject}.454UnassembledReads.lucy.fa";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/contigs.Length_vs_MeanCov.jpg";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/contigs.GC_vs_Depth.jpg";
print "$cmd\n";
system($cmd);


$cmd = "rm -f ${assemblyDir}/${optProject}.454AllContigs.lucy.fa.v.SilvaLSU.blastn.e20.parsed";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/${optProject}.454AllContigs.lucy.fa.v.SilvaSSU.blastn.e20.parsed";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/${optProject}.454AllContigs.lucy.fa.v.nr.blastn.e20.parsed";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/${optProject}.454AllContigs.lucy.fa.qual";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/${optProject}.454SingletonReads.lucy.fa.qual";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/${optProject}.454UnassembledReads.lucy.fa.qual";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/${optLibrary}.lucy.nr.fa";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${assemblyDir}/${optLibrary}.lucy.nr.fa.qual";
print "$cmd\n";
system($cmd);

$cmd = "rm -f ${trimDir}/${optLibrary}.RedundantReads.list";
print "$cmd\n";
system($cmd);


chdir $topDir;

exit;


###############################################################################
sub help {

    print <<USE;

$0 [options] -l <libraryCode> -p <projectID> -a <dir>

   -p  project identifier
   -n  number of parallel jobs
   -a  analysis directory

USE
exit;
}
