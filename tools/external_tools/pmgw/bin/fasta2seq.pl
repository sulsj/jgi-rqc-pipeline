#!/usr/bin/env perl


$/ = "\n>";
while(<>){
    s/\n>$//;
    @temp = split("\n",$_);
    ($id = shift @temp)=~ s/^>//;
    print ">$id\t",join("",@temp),"\n";
}
