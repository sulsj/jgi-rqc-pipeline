#!/usr/bin/env perl

=head1 NAME

runMetagenomeQcPipeline.pl

=head1 SYNOPSIS

  runMetagenomeQcPipeline.pl [options] <projectId> <inputFile>

  Options:
  -cluster <clusteName> Specify the cluster name when running BlastX on the cluster
                        (optional; default is rhea).
  -config <configFile>  Specify config file (optional; default is metagenomeQc.config
                        in install path).
  -ad <dir>             Specify the analysis dir (optional).
  -ld <linkDir>         Create symbolic links of predefined files in <linkDir>
                        (usually a collaborator directory) (optional).
  -log <file>           Specify log file (optional; default is runMetagenomeQcPipeline.pl.log
                        in analysis dir).
  -h                    Detailed message (optional).

  The format of the input file is as follows, each line number representing a
  tab delimited entry:

  1. library
  2. U|P (U=unpaired library, P=paired library)
  3. sff file location and name

  Example:
  GPPU    U    /house/sfffiles/mysff1.sff
  GPPU    U    /house/sfffiles/mysff2.sff
  GXNT    P    /house/sfffiles/mysff3.sff
  GXNS    P    /house/sfffiles/mysff4.sff

=head1 DESCRIPTION

This script is used to generate QC statistics for metagenome projects.

The following is perform:

 1. Run metaGenomeWorkflow.pl to trim data using Newbler and Lucy, assemble
    data using Newbler.

 2. Run run_blastx_jobs.pl (RQC) to split fasta files and align against the nr
    database using BlastX running on the cluster.

 3. Concatenate blast files and run runMeganWrapper.pl to generate Megan image
    files.

 4. Run getMetagenomeQcStats.pl to generate the pdf file and create symbolic
    links in the specified link directory.

=head1 VERSION

$Revision: 1.4 $

$Date: 2011-05-17 17:24:43 $

=head1 AUTHOR(S)

Stephan Trong

=head1 HISTORY

=over

=item *

S.Trong 2010/10/09 creation

=back

=cut

use strict;
use warnings;
use Cwd;
use Cwd qw (realpath getcwd abs_path);
use Carp;
use File::Basename;
use File::Copy;
use File::Path;
use File::Path qw(rmtree);
use Pod::Usage;
use Getopt::Long;
use FindBin qw($RealBin);
use lib "$RealBin/../lib";
use FIN_Constants;
use FIN_Commands;
use lib (RQC_SW_BIN_DIR)."/lib";
use PGF::Parsers::FastaParse;
use PGF::Utilities::Logger;
use PGF::Utilities::Properties;
use PGF::Utilities::RunProcess qw(runProcess);
use vars qw( $optHelp $optLinkDir $optClusterName $optConfigFile
    $optAnalysisDir $optLogFile );

#============================================================================#
# INPUT PARAMETERS
#============================================================================#

my $programExecution = abs_path(dirname($0))."/".basename($0)." @ARGV";

if( !GetOptions(
                "cluster=s" =>\$optClusterName,
                "config=s" =>\$optConfigFile,
                "ld=s" =>\$optLinkDir,
                "log=s"=>\$optLogFile,
                "ad=s" =>\$optAnalysisDir,
                "h" =>\$optHelp,
               )
    ) {
    printhelp(1);
}

printhelp(2) if $optHelp;
printhelp(1) if @ARGV != 2;

#============================================================================#
# INITIALIZE VARIABLES
#============================================================================#

# Input parameters.
my ($projectId, $inputFile) = @ARGV;

# Config file.
my $configFile = $optConfigFile ?  abs_path($optConfigFile) :
    "$RealBin/../config/metagenomeQc.config";
    
# Property object for extracting key/values from config file.
my $OBJ_PROPS = PGF::Utilities::Properties->new(-configFile=>$configFile);
   $OBJ_PROPS->setExceptionIfEntryNotFound(1); # confess if entry in config file
                                               # is not found.
                                               
# Threshold to determine if a percent of the reads from the input fasta
# file were successfully aligned.
my $pctReadsSuccess = $OBJ_PROPS->getProperty("minPctReadsAlignedToBeSuccessful");

# number of reads to subsample for running BlastX
my $numSubsampleReads = $OBJ_PROPS->getProperty("numberOfReadsForSubsampling");
    
# Split subsample file into this many reads per file for blasting.
my $splitSampleToThisManyReadsPerFile = $OBJ_PROPS->getProperty(
    "splitSubsampleIntoThisManyReadsPerFile");
    
# Prefix name for the subsampled fasta files.
my $splitFastaPrefix = "blastall_results";

# Library list.
my ($unpairedLibraryDataHashRef, $pairedLibraryDataHashRef) = parseInputFile(
    $inputFile);
my $unpairedLibraryList = join ".", keys %$unpairedLibraryDataHashRef;
my $pairedLibraryList = %$pairedLibraryDataHashRef ?
    join ".", keys %$pairedLibraryDataHashRef : "";
my $libraryList = length $unpairedLibraryList ? "$unpairedLibraryList" : "";
   $libraryList .= "." if (length $unpairedLibraryList && length $pairedLibraryDataHashRef);
   $libraryList .= $pairedLibraryList if length $pairedLibraryList;
   
# Analysis directory.
$optAnalysisDir =~ s/\/+$// if $optAnalysisDir; # strip any trailing '/';
my $analysisDir = $optAnalysisDir ? abs_path($optAnalysisDir) :
    analysisDirName($projectId, $libraryList);
    
# Newbler assembly directory.
my $newblerDir = "$analysisDir/${libraryList}_mi98ml80";

# Assembly trimmed reads file name.
my $trimmedReadsFile = "$newblerDir/assembly/454TrimmedReads.fna";

# Subsample file name.
my $subsampledReadsFile = "$newblerDir/assembly/454TrimmedReads.fna.subsample";

# Blast directory.
my $blastDir = "$analysisDir/clusterblast_${libraryList}"; # FIXME

# Merged blast file name.
my $mergedBlastFile = "$blastDir/blastx.output";

# Logging.
my $logFile = $optLogFile ? $optLogFile : "$analysisDir/".basename($0).".log";
my $logDir = dirname($logFile);
my $OBJ_LOGGER = PGF::Utilities::Logger->new();

# Set cluster name for running BlastX on the cluster if not specified.
$optClusterName = $OBJ_PROPS->getProperty("defaultClusterForBlastX")
    if !$optClusterName;

# Expand link dir to full path.
$optLinkDir =~ s/\/+$// if $optLinkDir; # strip any trailing '/';
$optLinkDir = abs_path($optLinkDir) if $optLinkDir;

# Define Config files to be added to the analysis dir.
# Used for rerunning the pipeline if needed.
my $configFileInAnalysisDir = fileNameWithIteration(
    "$analysisDir/".basename($configFile));
    
#============================================================================#
# MAIN
#============================================================================#

print "Analysis Directory: $analysisDir\n\n";

# Create analysis dir if not exist.
#
mkpath( $analysisDir ) if !-e $analysisDir;

# Copy config file to analysis dir.
#
copy $configFile, $configFileInAnalysisDir;

# Create config file specifically for running the linking portion only.
#
my $configFileForLinking = createConfigFileForLinkingOnly($configFile,
    $analysisDir);

# Logging
#
setFileForLogging($logFile);
logExecution($programExecution, $analysisDir, $configFileInAnalysisDir,
    $configFileForLinking);
   
# Copy input file to analysis dir.
#
my $inputFileBasename = basename($inputFile);
copy $inputFile, "$analysisDir/$inputFileBasename";

# Trim reads using Newbler, lucy; Assemble.
#
if ( runThisComponent("metaGenomeWorkFlow") ) {
    runMetaGenomeWorkflow($logDir, $configFile, $projectId, $analysisDir,
        $newblerDir, $inputFile);
}
    
# Subsample trimmed assembled reads.
#
if ( runThisComponent("subsampleReadsFile") ) {
    subsampleReadsFile($trimmedReadsFile, $subsampledReadsFile,
        $numSubsampleReads);
}

# Run BlastX against nr using the trimmed assembled reads.
#
if ( runThisComponent("runBlastX") ) {
    runBlastJobs($blastDir, $splitSampleToThisManyReadsPerFile,
        $subsampledReadsFile, $optClusterName, $splitFastaPrefix);
}

# Merge blast files, run Megan wrapper and/or getMetagenomeQcStats.
#
if ( runThisComponent("megan") ) {
    createMeganFiles($projectId, $libraryList, $logDir, $blastDir,
        $subsampledReadsFile, $mergedBlastFile, $splitFastaPrefix);
}

# Gather QC metrics and create report in pdf.
#
if ( runThisComponent( "getMetagenomeQcStats") ) {
    createQcReport( $projectId, $libraryList, $unpairedLibraryList,
        $pairedLibraryList, $analysisDir, $blastDir, $logDir, $optLinkDir);
}

# Create a zip file containing a directory of csv files (for the collaborator).
#
if ( runThisComponent("createCsvFile") ) {
    createCsvFile($projectId, $libraryList, $newblerDir, $blastDir);
}
    
# Clean un-necessary files in the blastX directory.
#
if ( runThisComponent("cleanBlastDir") ) {
    cleanBlastDir($blastDir, $splitFastaPrefix);
}

logOutput("Done.\n"."-" x 70, 1);

exit 0;

#============================================================================#
# SUBROUTINES
#============================================================================#
sub runThisComponent {
    my $componentName = shift;
    
    # Check if component should be executed based on what's defined in the
    # config file with the prefix execute
    #
    
    my $runComponent = 0;
    my $findComponent = $OBJ_PROPS->getProperty("execute.$componentName");
       $findComponent =~ s/\s+$//;
    
    # If component is not defined in config file or if component is defined
    # and is set to 1, then return 1 (yes, run the component).
    #
    if ( !length $findComponent ||
        ( length $findComponent && $findComponent ) ) {
        $runComponent = 1;
    }
    return $runComponent;
}
    
#============================================================================#
sub subSample {
    my $numSubsampleReads = shift;
    my $trimmedReadsFile = shift;
    my $subsampledReadsFile = shift;

    my $cmd = (SAMPLE_FASTA) . " $trimmedReadsFile $numSubsampleReads ".
        $subsampledReadsFile;
    runCommand($cmd);

}

#============================================================================#
sub createConfigFileForLinkingOnly {
    my $configFile = shift;
    my $analysisDir = shift;
    
    my $configFileForLinkingExtension = "forLinkingOnly";
    
    # Look for config file ending in .forLinkingOnly
    #
    opendir(DIR, $analysisDir);
    my @configFileForLinking = grep /\.$configFileForLinkingExtension/,
        readdir(DIR);
    close DIR;
    
    # If config file containing .forLinkingOnly extension exists, don't need
    # to recreate one.
    #
    if ( @configFileForLinking ) {
        return $configFileForLinking[0];
        
    # Otherwise, create config file for linking files.
    #
    } else {
        my $configFileForLinking = $analysisDir."/".basename($configFile).
            ".$configFileForLinkingExtension";
        open IFILE, $configFile or
            die "ERROR: failed to open file $configFile.\n";
        open OFILE, ">$configFileForLinking" or
            die "ERROR: failed to create file $configFileForLinking.\n";
            
        while (my $line = <IFILE>) {
            if ( $line =~ /^execute\.([^=]+)/ ) {
                my $cmd = $1;
                $line = $cmd eq 'getMetagenomeQcStats'? "execute.$cmd=1\n" :
                    "execute.$cmd=0\n";
            }
            print OFILE $line;
        }
        
        close OFILE;
        close IFILE;
        return $configFileForLinking;
    }
}

#============================================================================#
sub parseInputFile {
    my $file = shift;
    
    my %unpairedLibraryData = ();
    my %pairedLibraryData = ();
    
    open IFILE, $file or die "Failed to open input file $file: $!\n";
    while (my $line = <IFILE>) {
        chomp $line;
        next if !length $line;
        next if $line =~ /^#/; # skip if comment.
        # Look for entries like 'library U|P sffFile'
        if ( $line =~ /^(\S+)\s+(\S+)\s+(\S+)/ ) {
            my ($library, $libraryType, $sffFile) = ($1, $2, $3);
            if ( $libraryType eq 'U' ) {
                push @{$unpairedLibraryData{$library}}, $sffFile;
            } elsif ( $libraryType eq 'P' ) {
                push @{$pairedLibraryData{$library}}, $sffFile;
            }
        }
    }
    
    return \%unpairedLibraryData, \%pairedLibraryData;
    
}
    
#============================================================================#
sub runMetaGenomeWorkflow {
    my $logDir = shift;
    my $configFile = shift;
    my $projectId = shift;
    my $workingDir = shift;
    my $newblerDir = shift;
    my $inputFile = shift;
    
    # Execute metagenome pipeline script
    #
    my $logFile = $logDir."/".basename(METAGENOME_WORKFLOW).".log";
    my $cmd = (METAGENOME_WORKFLOW).
        " -c $configFile ".
        " -od $workingDir".
        " -nd $newblerDir".
        " -log $logFile".
        " $projectId $inputFile";
    runCommand($cmd);
    
}

#============================================================================#
sub subsampleReadsFile {
    
    my $trimmedReadsFile = shift;
    my $subsampledReadsFile = shift;
    my $numSubsampleReads = shift;
    
    my $count = `grep -c ">" $trimmedReadsFile`;
    chomp $count;
    # If number of reads in trimmed reads file is greater than defined
    # subsample size, create subsample fasta file.
    #
    if ( $count > $numSubsampleReads ) {
        subSample($numSubsampleReads, $trimmedReadsFile, $subsampledReadsFile);
        
    # If number of reads in trimmed reads file is equal to or less than
    # the defined subsample size, create symbolic link of trimmed reads file
    # naming the symlink the subsample file name.
    #
    } else {
        symlink $trimmedReadsFile, $subsampledReadsFile;
    }
    
}

#============================================================================#
sub createMeganFiles {
    
    my $projectId = shift;
    my $libraryList = shift;
    my $logDir = shift;
    my $blastDir = shift;
    my $subsampledReadsFile = shift;
    my $mergedBlastFile = shift;
    my $splitFastaPrefix = shift;

    if ( !-e $blastDir ) {
        logError("Could not find blastx directory $blastDir.",1);
    }
    
    # Parse reads from input fasta file.
    #
    my ($fastaReadCount, $H_fastaReads) = getReadsFromFasta($subsampledReadsFile);
    
    # Merge bastx results into 1 file.
    #
    my $alignReadCount = mergeBlastFiles($mergedBlastFile, $blastDir,
        $H_fastaReads, $splitFastaPrefix);
    
    # If no reads were aligned, log error and exit.
    #
    logError("Could not find any aligned reads.\n", 1) if !$alignReadCount;
    
    # Compute percent reads aligned.
    #
    my $pctReadsAligned = getPctReadsAligned($fastaReadCount, $alignReadCount,
        $pctReadsSuccess);
    
    # Check to see if at least 90% (or whatever $pctReadsSuccess is set to) of
    # the reads in the input fasta file is accounted for in the blast files.
    # If yes, then run megan to generate image files.
    #
    if ($pctReadsAligned >= $pctReadsSuccess ) {
        my $rmaFile = "$blastDir/${projectId}_${libraryList}_reads.rma";
        runMegan($logDir, $libraryList, $rmaFile, $mergedBlastFile, $subsampledReadsFile);
        
    # If not, then log and create fasta file of missing blast hits for
    # re-blasting.
    #
    } else {
        logOutput("Insufficient number of reads aligned (requires at least ".
            "${pctReadsSuccess}%)." );
        
        # Create fasta containing reads that failed to blast (for re-running).
        #
#        createPartialFastaForReAligning($subsampledReadsFile, $H_fastaReads);
        
        # Create log file.
        #
        createLogFile($alignReadCount, $fastaReadCount, $subsampledReadsFile,
            $H_fastaReads);
    }
}

#============================================================================#
sub createQcReport {
    
    my $projectId = shift;
    my $libraryList = shift;
    my $unpairedLibraryList = shift;
    my $pairedLibraryList = shift;
    my $analysisDir = shift;
    my $blastDir = shift;
    my $logDir = shift;
    my $linkDir = shift || '';
    
    my $attribsFile = "$analysisDir/${projectId}_${libraryList}_qcMetrics.out";
    if ( $optLinkDir ) {
        my $analysisDirBasename = basename($analysisDir);
        $linkDir = "$optLinkDir/$analysisDirBasename";
    }
    runPdfGenerator($logDir, $configFile, $analysisDir, $projectId, $blastDir,
        $attribsFile, $unpairedLibraryList, $pairedLibraryList, $linkDir);
    
}
    
#============================================================================#
sub mergeBlastFiles {
    
    my $subsampledReadsFile = shift;
    my $blastDir = shift;
    my $H_fastaReads = shift;
    my $splitFastaPrefix = shift;
    
    my ($init,%buffer,$readid,$record);
    my $blastReadCount = 0;
    
    opendir(DIR, $blastDir);
    my @blastFiles = grep /^$splitFastaPrefix\./, readdir(DIR);
    close DIR;
    
    my $defaultRecordSeparator = $/;
    $/ = "\nBLAST";

    if ( !@blastFiles ) {
        logOutput("No $splitFastaPrefix.* files found.", 1);
        return $blastReadCount;
    }
    
    open OUT, ">$subsampledReadsFile" or
        die "ERROR: failed to open file $subsampledReadsFile: $!\n";
    
    foreach my $blastFile (@blastFiles){
        my $baseFileName = $blastFile;
        $blastFile = "$blastDir/$blastFile";
        open (IN, $blastFile) or die "can't open $blastFile\n";
        my $init = 1;
        $buffer{record}  = "";
        while($record = <IN>){
            next if ($record =~ s/\x0//g);
            $record =~ s/\nBLAST$//;
            if (! $init){
                $record = "BLAST" . $record;
                print OUT $buffer{record};
            } else {
                $init = 0;
            }
            $readid = ( $record=~/^.*Query=\s(\S+).*$/m)[0];
            if (defined $readid){
                $$H_fastaReads{$readid}++ if exists $$H_fastaReads{$readid};
                $buffer{record} = $record;
                $blastReadCount++;
            }
        }
        if ($buffer{record} =~ /Matrix:/){ #complete blast file
            print OUT $buffer{record};
            logOutput("processing $baseFileName ... complete", 1);
        } else {
            logOutput("processing $baseFileName ... incomplete", 1);
            $$H_fastaReads{$readid}-- if exists $$H_fastaReads{$readid};
            $blastReadCount--;
        }
        close IN;
    }
    close OUT;

    $/ = $defaultRecordSeparator;
    
    return $blastReadCount;    

}

#============================================================================#
sub getPctReadsAligned {
    
    my $fastaReadCount = shift;
    my $alignReadCount = shift;
    my $pctReadsSuccess = shift;
    
    my $pctReadsAligned = $fastaReadCount ?
        sprintf ("%.1f", $alignReadCount/$fastaReadCount*100) : 0;

    my $printmsg = "Reads in input fasta: $fastaReadCount\n".
                 "Reads in blast files: $alignReadCount\n".
                 "Percent reads blasted: $pctReadsAligned (>= $pctReadsSuccess required)\n";
    logOutput($printmsg, 1);
    
    return $pctReadsAligned;
    
}

#============================================================================#
sub runMegan{
    
    my $logDir = shift;
    my $library = shift;
    my $rmaFile = shift;
    my $blastFile = shift;
    my $fastaFile = shift;
    
    my $logFile = $logDir . "/" . basename(MEGAN_WRAPPER) . ".log";
    
    my $cmd = "";
       $cmd .= (SOURCE_SETTINGS)."; ";
       $cmd .= (MEGAN_WRAPPER) . " ";
       $cmd .= "$blastFile $fastaFile $rmaFile $library > $logFile";
    runCommand($cmd);
    
}
    
#============================================================================#
sub runPdfGenerator {
    
    my $logDir = shift;
    my $configFile = shift;
    my $qcDir = shift;
    my $projectId = shift;
    my $blastDir = shift;
    my $attribsFile = shift;
    my $unpairedLibraryList = shift;
    my $pairedLibraryList = shift;
    my $linkDir = shift || '';

    my $logFile = $logDir . "/" . basename(GET_METAGENOME_QC_STATS) . ".log";
    
    if ( $unpairedLibraryList ) {
        $unpairedLibraryList =~ s/\./ /;
        $unpairedLibraryList = "\'$unpairedLibraryList\'";
    }
    if ( $pairedLibraryList ) {
        $pairedLibraryList =~ s/\./ /;
        $pairedLibraryList = "\'$pairedLibraryList\'";
    }
    
    my $cmd = (GET_METAGENOME_QC_STATS);
       $cmd .= " -c $configFile";
       $cmd .= " -p $projectId";
       $cmd .= " -l $unpairedLibraryList" if $unpairedLibraryList;
       $cmd .= " -pel $pairedLibraryList" if $pairedLibraryList;
       $cmd .= " -a $qcDir";
       $cmd .= " -bd $blastDir";
       $cmd .= " -f $attribsFile";
       $cmd .= " -log $logFile";
       $cmd .= " $linkDir" if $linkDir;
    runCommand($cmd);
    
}

#============================================================================#
sub linkFiles {
    
    my $attribsFile = shift;
    my $linkDir = shift;
    
    my %attribs = getAttributes($attribsFile);
    my %linkFiles = ();
    my %renameLinkFiles = ();
    
    foreach my $name (keys %attribs) {
        $linkFiles{$name} = $attribs{$name} if $name =~ /^file\./;
        $renameLinkFiles{$name} = $attribs{$name} if $name =~ /^renameLink\./;
    }
        
    my $hrefLinkFiles = shift;
    my $hrefRenameLinkFiles = shift;
    
    foreach my $key (keys %linkFiles) {
        my $origFile = $linkFiles{$key};
        my $linkFile = $renameLinkFiles{"renameLink.$origFile"} ?
            $renameLinkFiles{"renameLink.$origFile"} : $origFile;
        $linkFile = $linkDir ."/". basename($linkFile);
        my $command = "ln -s $origFile $linkFile\n";
        print "$command\n";
	system($command);
    }
    
}

#============================================================================#
sub getAttributes {
    
    my $file = shift;
    my $getAttribute = shift || ''; # optionally specify attribute name to get.
    
    my %attribs = ();
    tie %attribs, "Tie::IxHash"; # this will allow me to print the keys in
                                 # insertion order.
    
    open IFILE, $file or die "ERROR: failed to open file $file: $!\n";
    while (<IFILE>) {
        chomp;
        next if !length;
        if ( /^([^=]+)=(.*)/ ) {
            my $name = $1;
            my $value = $2;
            $value = ' ' if !length $value;
            if ( $getAttribute ) {
                $attribs{$name} = $value if $getAttribute eq $name;
            } else {
                $attribs{$name} = $value;
            }
        }
    }
    
    close IFILE;
    
    return %attribs;
    
}

#============================================================================#
sub cleanBlastDir {
    
    my $blastDir = shift;
    my $splitFastaPrefix = shift;
    
    logOutput("Cleaning $blastDir ...", 1);
    
    # Remove directory containing partitioned fasta files.
    #
    rmtree( "$blastDir/query_partitions" ) if -e "$blastDir/query_partitions";
    
    opendir(DIR, $blastDir);
    while (my $file = readdir(DIR)) {
        if ( $file =~ /^${splitFastaPrefix}.+blastx\.out$/ ) {
            unlink "$blastDir/$file";
        }
    }
    
    close DIR;
    
}

#============================================================================#
sub createPartialFastaForReAligning {
    
    my $fastaFile = shift;
    my $H_fastaReads = shift;
    my $outputFile = shift;
    
    my $objFastaParse = PGF::Parsers::FastaParse->new($fastaFile);
    open OUT, ">$outputFile" or die "ERROR: failed to create file $outputFile: $!\n";
    
    while ($objFastaParse->MoreEntries) {
        $objFastaParse->ReadNextEntry(-rawFormat=>1);
        my $readName = $objFastaParse->Name;
        if ( exists $$H_fastaReads{$readName} &&
             $$H_fastaReads{$readName} == 1 ) {
            my $comment = $objFastaParse->Comment;
            my $seq = $objFastaParse->Seq;
            print OUT ">$readName";
            print OUT length $comment ? " $comment\n" : "\n";
            print OUT $seq;
        }
    }
    $objFastaParse->Close;
    
    close OUT;
    
    # remove output file if zero size.
    #
    if ( -z $outputFile ) {
        logOutput("Failed to create fasta of failed blastx reads: $outputFile.", 1);
        unlink $outputFile if -e $outputFile;
    } else {
        logOutput("Created fasta of failed blastx reads: $outputFile.", 1);
    }
    
}

#============================================================================#
sub createLogFile {
    
    my $blastReadCount = shift;
    my $fastaReadCount = shift;
    my $fastaFile = shift;
    my $H_fastaReads = shift;
    
    my $pctFailed = $fastaReadCount ?
        sprintf "%.1f", $blastReadCount/$fastaReadCount*100 : 0;
    my $logFile = "blastJobs.failed.log";
    
    open OUT, ">$logFile" or die "ERROR: failed to create $logFile: $!\n";
    print OUT "Num Reads in blast files: $blastReadCount\n";
    print OUT "Num Reads in $fastaFile: $fastaReadCount\n";
    print OUT "Percent Failure: $pctFailed\n\n";
    print OUT "List of reads that failed:\n\n";
    
    foreach my $readname (keys %$H_fastaReads) {
        print OUT "$readname\n" if $$H_fastaReads{$readname} == 1;
    }
    
    close OUT;
    
}

#============================================================================#
sub getReadsFromFasta {
    my $file = shift;
    
    my %reads = ();
    my $readCount = 0;
    
    open fhFasta, $file or die "ERROR: failed to open file $file: $!\n";
    while (my $entry = <fhFasta>) {
        chomp $entry;
        if ( $entry =~ /^>(\S+)/ ) {
            $reads{$1} = 1;
            $readCount++;
        }
    }
    close fhFasta;
    
    return $readCount, \%reads;
    
}

#============================================================================#
sub runBlastJobs {
    my $blastDir = shift;
    my $numSubsampleReadsPerFile = shift;
    my $trimmedReadsFile = shift;
    my $clusterName = shift;
    my $splitFastaPrefix = shift;
    
    my $nrDatabase = $OBJ_PROPS->getProperty("nrDatabase");
    my $maxJobSubmit = $OBJ_PROPS->getProperty("maxClusterJobsToSubmit");
    my $pctJobsToComplete = $OBJ_PROPS->getProperty("pctJobsRequiredToComplete");
    my $blastParams = $OBJ_PROPS->getProperty("blastXParameters");
    my $useScratchSpaceOnNode = $OBJ_PROPS->getProperty("useScratchSpaceOnNode");
    
    my $current_dir = getcwd;
    mkpath( $blastDir ) if !-e $blastDir;
    chdir $blastDir;
    
    logOutput("Output Blast Dir=$blastDir\n", 1);
    
    my $cmd = (RUN_BLASTX_JOBS); 
       $cmd .= " -sn $numSubsampleReadsPerFile ";
       $cmd .= " -sp $splitFastaPrefix";
       $cmd .= " -c $clusterName";
       $cmd .= " -nr $nrDatabase";
       $cmd .= " -pct $pctJobsToComplete";
       $cmd .= " -max $maxJobSubmit";
       $cmd .= " -bp \'$blastParams\'";
       $cmd .= " -scratch" if $useScratchSpaceOnNode;
       $cmd .= " $trimmedReadsFile $blastDir";
        
    runCommand($cmd, 0);
    
    chdir $current_dir;
    
    logOutput("Done blasting.\n", 1);
}

#============================================================================#
sub createCsvFile {
    
    my $proj = shift;
    my $lib = shift;
    my $assemblyDir = shift;
    my $blastDir = shift;
    
    $assemblyDir .= "/assembly";
    
    my $tmpdirBasename = $proj . '.' . $lib . '.' . "csv";
    my $tmpdir = $assemblyDir . '/' . $tmpdirBasename;
    my $zip = $analysisDir . '/' . $tmpdirBasename . '.zip';
    my $cmd = (ZIPEXE) . " $zip ";
    my $current_dir = getcwd;
    
    # Create tmp dir to store csv files to zip.
    #
    mkdir ($tmpdir) if !-e $tmpdir;
    
    logOutput("Creating csv file $zip");
    
    # Define files to zip. First element of array reference corresponds to
    # the path and name of the file to zip. The second element of array
    # reference corresponds to the renamed file to be zipped.
    #
    my @tab;
    $tab[0][0]="$assemblyDir/$lib.ctgs.lucy.99.3.fasta.v.SilvaLSU.blastn.e20.parsed";
    $tab[0][1]="AllCtgs.SilvLSU.blastn.csv";
    $tab[1][0]="$assemblyDir/$lib.ctgs.lucy.99.3.fasta.v.SilvaSSU.blastn.e20.parsed";
    $tab[1][1]="AllCtgs.SilvSSU.blastn.csv";
    $tab[2][0]="$assemblyDir/$lib.lucy.nr.fa.v.SilvaLSU.blastn.e20.parsed";
    $tab[2][1]="$lib.SilvLSU.blastn.csv";
    $tab[3][0]="$assemblyDir/$lib.lucy.nr.fa.v.SilvaSSU.blastn.e20.parsed";
    $tab[3][1]="$lib.SilvSSU.blastn.csv";
    $tab[4][0]="$assemblyDir/$lib.ctgs.lucy.99.3.fasta.v.nr.blastn.e20.parsed";
    $tab[4][1]="AllCtgs.nr.blastn.csv";
    $tab[5][0]="$blastDir/${proj}_${lib}_reads.megan.out";
    $tab[5][1]="${lib}_reads.megan.out.csv";
    $tab[6][0]="$assemblyDir/DepthCov/454Contigs.ace.new.contigs.cov.lucy.GC";
    $tab[6][1]="AllCtgs.cov.lucy.GC.csv";
    
    # Construct command to zip. Will copy csv files to tmpdir first, then
    # convert all tabs to commas in the file.
    #
    my $header = 0;
    for (my $i=0;$i<@tab;$i++){
        # If file doesn't exist, log warning and skip.
        #
        if (! -e $tab[$i][0]){
            logOutput("WARNING: $tab[$i][0] does not exist.", 1);
            next;
        }
        
        if ($tab[$i][0] =~ /\.csv$/){
            copy($tab[$i][0], "$tmpdir/$tab[$i][1]");
            $cmd .= "$tmpdir/$tab[$i][1] ";
        } else {
            open (IN,$tab[$i][0]) or die "Can't open $tab[$i][0]\n";
            $cmd .= "$tmpdirBasename/$tab[$i][1] ";
            open (OUT,">$tmpdir/$tab[$i][1]") or die "Can't open $tmpdir/$tab[$i][1]\n";
            while(<IN>){
                chomp;
                $_=~s/\,//g;
                $_=~s/\t/\,/g;
                print OUT "$_\n";
            }
            close(IN);
            close(OUT);
    
        }
    }
    
    # Zip directory containing csv files.
    #
    chdir $assemblyDir;
    runCommand($cmd);
    chdir $current_dir;
    
    # Remove tmp dir.
    #
    rmtree( $tmpdir);
    
}


#============================================================================#
sub analysisDirName {

    my $projectId = shift;
    my $libraryList = shift;
    
    # Get analysis dir from config file.
    #
    my $analysisDir = $OBJ_PROPS->getProperty("defaultAnalysisDir");
       $analysisDir =~ s/\/+$//; # remove trailing '/'.
       $analysisDir .= "/" . $projectId . '_' . $libraryList . '_' .
           getCurrentDate();
    
    # Add itereation number to name.
    #
    $analysisDir = fileNameWithIteration($analysisDir);
    
    return $analysisDir;
    
}

#============================================================================#
sub fileNameWithIteration {
    
    my $file = shift;
    
    if ( -e $file ) {
        my $extension = 1;
        while ( -e "$file.$extension" ) {
            $extension++;
        }
        $file = $file . ".$extension";
    }
    
    return $file;
    
}

    
#============================================================================#
sub getCurrentDate {

# Returns current date and time in format:
# 'MM-DD-YYYY HH24:MI:SS'

    my ($sec,$min,$hour,$day,$mon,$year) = localtime(time);

    $mon++;
    $year+=1900;

    my $time = sprintf( "%04d%02d%02d", $year, $mon, $day);

    return $time;

}

#============================================================================#
sub runCommand {
   
    my $cmd = shift;
    my $checkExitStatus = shift;
    
    # Default exit status checking to true if not defined.
    #
    $checkExitStatus = 1 if !defined $checkExitStatus;
    
    # Execute command, capture stderr, stdout, exitcode
    #
    my $errMessage = "CMD: $cmd\n";
    
    print "Running $cmd\n\n";
    logOutput($cmd);
    
    my %processInfo = runProcess($cmd,
        {-checkExecutable=>0,
        }
    );
    
    checkProcess(%processInfo) if $checkExitStatus;
    
}

#============================================================================#
sub checkProcess {
    
    my %processInfo = @_;
    
    if ( $processInfo{exitCode} ) {
        my $errMsg = '';
        if ( defined $processInfo{logStdoutMessage} &&
            $processInfo{logStdoutMessage} ) {
            $errMsg .= $processInfo{stdoutMessage};
        }
        $errMsg .= $processInfo{stderrMessage};
        logError($errMsg,1);
    }
    
}

#============================================================================#
sub logError {
    my $message = shift;
    my $confess = shift || 0;
    
    $OBJ_LOGGER->logError($message);
    
    confess if $confess;
}
    
#============================================================================#
sub logOutput {
    my $message = shift;
    my $printMsg = shift || 0;
    
    $OBJ_LOGGER->logOut($message);
    print "$message\n" if $printMsg;
}
    
#============================================================================#
sub logExecution {
    
    my $programExecution = shift;
    my $analysisDir = shift;
    my $configFile = shift;
    my $configFileForLinking = shift;

    # Construct command to rerun the pipeline using the config and input files
    # within the analysis directory.
    #
    my ($rerunCmd, $projectId, $inputFile) =
        ($programExecution =~ /(.+)\s+(\S+)\s+(\S+)\s*$/);
    my $configFileBasename = basename($configFile);
    my $configFileForLinkingBasename = basename($configFileForLinking);
    my $inputFileBasename = basename($inputFile);
    $rerunCmd =~ s/\s+\-config \S+//; # remove the -config option.
    $rerunCmd =~ s/\s+\-ad \S+//; # remove the -ad option.
    $rerunCmd .= " -ad $analysisDir";
    $rerunCmd .= " -config $analysisDir/$configFileBasename";
    
    my $cmdForLinking = $rerunCmd;
    $cmdForLinking =~ s/$configFileBasename/$configFileForLinkingBasename/;
    $cmdForLinking .= " -ld LINKDIR" if $cmdForLinking !~ / \-ld /;
    
    $rerunCmd .= " $projectId $analysisDir/$inputFileBasename";
    $cmdForLinking .= " $projectId $analysisDir/$inputFileBasename";
    
    my $msg = "Command:\n";
       $msg .= "$programExecution\n\n";
       $msg .= "For re-running:\n";
       $msg .= "$rerunCmd\n\n";
       $msg .= "For linking files only:\n";
       $msg .= "$cmdForLinking\n\n";
       $msg .= "Current directory: ".getcwd."\n";
       $msg .= "Analysis Dir: $analysisDir\n";

    logOutput($msg);
}

#============================================================================#
sub setFileForLogging {
    
    my $logFile = shift;
    
    $OBJ_LOGGER->setLogOutFileAppend($logFile);
    $OBJ_LOGGER->setLogErrorFileAppend($logFile);
    
}

#============================================================================#
sub printhelp {
    my $verbose = shift || 1;
    pod2usage(-verbose=>$verbose);
    exit 1;
}

#============================================================================#
    
    




