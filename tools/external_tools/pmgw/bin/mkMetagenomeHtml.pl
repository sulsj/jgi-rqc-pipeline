#!/usr/bin/env perl

=head1 NAME

=head1 SYNOPSIS

  mkMetagenomeHtml.pl [options] <attributeFile> <outputHtmlFile> [linkDir]

  Options:
  -h            Detailed message (optional)

=head1 DESCRIPTION

=head1 VERSION

$Revision: 1.1.1.4 $

$Date: 2011-01-19 18:42:52 $

=head1 AUTHOR(S)

Stephan Trong

=head1 HISTORY

=over

=item *

S.Trong 2010/05/28 creation

=back

=cut

use strict;

use Cwd;
use Cwd qw(abs_path);
use Pod::Usage;
use File::Basename;
use CGI qw/:standard/;
use Getopt::Long;
use Tie::IxHash;
use FindBin qw($RealBin);
use lib "$RealBin/../lib";
use vars qw($optHelp);

#============================================================================#
# INPUT VALIDATION
#============================================================================#

if( !GetOptions(
                "h"   =>\$optHelp,
                )
    ) {
    printhelp(1);
}
 
printhelp(2) if $optHelp;

if ( @ARGV < 2 ) {
    my $errMsg = "Required input parameters are missing in command line.";
    print STDERR "$errMsg\n";
    printhelp(1);
}

my ($attribsFile, $outputHtmlFile, $linkDir) = @ARGV;

$linkDir = '' if !defined $linkDir;
$linkDir =~ s/\/$//;

if ( !-s $attribsFile ) {
    print STDERR "Can't find your file $attribsFile.\n";
    exit 1;
}

#============================================================================#
# INITIALIZE VARIABLES
#============================================================================#


#============================================================================#
# MAIN
#============================================================================#

my %attribs = getAttributes($attribsFile);
my $title = "Metagenome QC For Project $attribs{sampleId}";

open OFILE, ">$outputHtmlFile" or die "ERROR: failed to create file $outputHtmlFile: $!\n";

print OFILE start_html(
    -title=>$title,
    -head=>style({-type=>'text/css'},
                 join('', <DATA>), # slurp __DATA__ )
                )
);

printTitle(*OFILE, %attribs);

printProjectInfo(*OFILE, %attribs);

printAssemblyInfo(*OFILE, %attribs);

print OFILE "<DIV style=\"page-break-after:always\"></DIV>\n";
printLucyInfo(*OFILE, %attribs);

print OFILE "<DIV style=\"page-break-after:always\"></DIV>\n";
printAlignDepthInfo(*OFILE, %attribs);

printPlots(*OFILE, %attribs);

print OFILE "<DIV style=\"page-break-after:always\"></DIV>\n";
printDownloads(*OFILE, $linkDir, $attribsFile, %attribs) if $linkDir;

print OFILE end_html;

exit 0;

#============================================================================#
# SUBROUTINES
#============================================================================#
sub getAttributes {
    
    my $file = shift;
    my $getAttribute = shift || ''; # optionally specify attribute name to get.
    
    my %attribs = ();
    tie %attribs, "Tie::IxHash"; # this will allow me to print the keys in insertion order.
    
    open IFILE, $file or die "ERROR: failed to open file $file: $!\n";
    while (<IFILE>) {
        chomp;
        next if !length;
        if ( /^([^=]+)=(.*)/ ) {
            my $name = $1;
            my $value = $2;
            $value = ' ' if !length $value;
            if ( $getAttribute ) {
                $attribs{$name} = $value if $getAttribute eq $name;
            } else {
                $attribs{$name} = $value;
            }
        }
    }
    
    close IFILE;
    
    return %attribs;
    
}

#============================================================================#
sub printTitle {
    
    my $fh = shift;
    my %attribs = @_;
    
    print $fh
    h3,
    "<table><tr>",
    "<td>", img( {src=>$attribs{'jgiLogo'}} ), "</td>",
    "<td>",h2,"Metagenome QC Summary","</td>",
    "</tr></table>",
    br,
    "$attribs{sampleId} ($attribs{library})",
    br,
    $attribs{date};
    
}

#============================================================================#
sub printProjectInfo {
    
    my $fh = shift;
    my %attribs = @_;
    
    print $fh
        h3, "Project Information:", p,
        table({-border=>1},
        Tr({-align=>'LEFT', -valign=>'TOP'},
        [
            td([ 'Program:', "$attribs{program} $attribs{programYear}" ]),
            td([ 'PMO Project:', $attribs{pmoProject} ]),
            td([ 'PMO Project ID', $attribs{pmoProjectId} ]),
            td([ 'Sample ID', $attribs{sampleId} ]),
            td([ 'Library(s)', $attribs{libraryEntry} ]),
            td([ 'Run ID(s)', $attribs{runId} ]),
        ]
        )
    );
    
}

#============================================================================#
sub printAssemblyInfo {
    
    my $fh = shift;
    my %attribs = @_;
    
    my $color = "lightgray";
    
    print $fh
    h3,
    "Assembly Statistics:",
    p,
    $attribs{newblerAssembly},
    p;
    
    print $fh
        table({-border=>1},
        Tr({-align=>'LEFT', -valign=>'TOP'},
        [
            th({-bgcolor=>$color, -colspan=>2},[ 'Run Metrics' ]),
            td([ 'totalNumberOfReads:', $attribs{totalNumberOfReads} ]),
            td([ 'totalNumberOfBases:', $attribs{totalNumberOfBases} ]),
            th({-bgcolor=>$color, -colspan=>2},[ 'Read Status' ]),
            td([ 'numAlignedReads:', $attribs{'readStatus.numAlignedReads'} ]),
            td([ 'numAlignedBases:', $attribs{'readStatus.numAlignedBases'} ]),
            td([ 'inferredReadError:', $attribs{'readStatus.inferredReadError'} ]),
            td([ 'numberAssembled:', $attribs{'readStatus.numberAssembled'} ]),
            td([ 'numberPartial:', $attribs{'readStatus.numberPartial'} ]),
            td([ 'numberSingleton:', $attribs{'readStatus.numberSingleton'} ]),
            td([ 'numberRepeat:', $attribs{'readStatus.numberRepeat'} ]),
            td([ 'numberOutlier:', $attribs{'readStatus.numberOutlier'} ]),
            td([ 'numberTooShort', $attribs{'readStatus.numberTooShort'} ]),
            th({-bgcolor=>$color, -colspan=>2},[ 'Large Contig Metrics' ]),
            td([ 'numberOfContigs', $attribs{'largeContigs.numberOfContigs'} ]),
            td([ 'numberOfBases', $attribs{'largeContigs.numberOfBases'} ]),
            td([ 'avgContigSize', $attribs{'largeContigs.avgContigSize'} ]),
            td([ 'N50ContigSize', $attribs{'largeContigs.N50ContigSize'} ]),
            td([ 'largeContigSize', $attribs{'largeContigs.largestContigSize'} ]),
            td([ 'Q40PlusBases', $attribs{'largeContigs.Q40PlusBases'} ]),
            td([ 'Q39MinusBases', $attribs{'largeContigs.Q39MinusBases'} ]),
            th({-bgcolor=>$color, -colspan=>2},[ 'All Contig Metrics' ]),
            td([ 'numberOfContigs', $attribs{'allContigs.numberOfContigs'} ]),
            td([ 'numberOfBases', $attribs{'allContigs.numberOfBases'} ])
        ]
        )
    );
        
}

#============================================================================#
sub printAlignDepthInfo {
    
    my $fh = shift;
    my %attribs = @_;
    
    my @alignDepths = ();
    
    foreach my $key (keys %attribs) {
        if ( $key =~ /^alignDepth.([^=]+)/ ) {
            my $depth = $1;
            my $value = $attribs{$key};
            push @alignDepths, [$depth, $value];
        }
    }
    
    @alignDepths = sort byDepth @alignDepths;
    
    print $fh
    p,
    "Alignment Depths:",
    p, "<table border=1>",
    "<tr bgcolor=\"lightgray\">",
    "<th>Depth</th>",
    "<th>Observed</th>",
    "</tr>";
    
    foreach my $depth (@alignDepths) {
        print $fh "<tr><td>$$depth[0]</td><td>$$depth[1]</td></tr>";
    }
    print $fh "</table>";
    
    print $fh
    p,
    "Peak Depth: ", $attribs{peakDepth},
    br,
    "Estimated Genome Size: ", $attribs{estSize};
    
}
    
#============================================================================#
sub printLucyInfo {
    
    my $fh = shift;
    my %attribs = @_;
    
    # has non-paired only
    if (exists $attribs{nonpelibrary} && !exists $attribs{pelibrary}){
        print $fh
        p,
        h3, "Redundancy and Lucy Trimming Statistics:",
        p,
        "<table border=1>",
        "<th bgcolor=\"lightgray\"></th>",
        "<th bgcolor=\"lightgray\">Reads</th>",
        "<th bgcolor=\"lightgray\">Contigs</th>",
        "<th bgcolor=\"lightgray\">Singlets</th>",
        "<th bgcolor=\"lightgray\">Unassembed (Singletons, Repeats, Outliers)</th>",
        "<tr>",
        "<td bgcolor=\"lightgray\">Raw</td>",
        "<td>$attribs{rawReads}</td>",
        "<td>$attribs{rawContigs}</td>",
        "<td>$attribs{rawSinglets}</td>",
        "<td>$attribs{rawUnassembled}</td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">Newbler/contam filtered</td>",
        "<td>$attribs{newblerContamReads}</td>",
        "<td></td>",
        "<td></td>",
        "<td></td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">Non-Redundant</td>",
        "<td>$attribs{nonRedundant}</td>",
        "<td></td>",
        "<td></td>",
        "<td></td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">Lucy Trimmed 99.3%</td>",
        "<td>$attribs{lucyTrimReads}</td>",
        "<td>$attribs{lucyTrimContigs}</td>",
        "<td>$attribs{lucyTrimSinglets}</td>",
        "<td>$attribs{lucyTrimUnassem}</td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">% Redundant Reads</td>",
        "<td>$attribs{pctRedundantReads}</td>",
        "<td></td>",
        "<td></td>",
        "<td></td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">% Lucy Trim Non-Redundant</td>",
        "<td>$attribs{pctLucyTrimNonRedundant}</td>",
        "<td></td>",
        "<td></td>",
        "<td></td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">% Raw Remaining</td>",
        "<td>$attribs{pctRawRemainReads}</td>",
        "<td>$attribs{pctRawRemainContigs}</td>",
        "<td>$attribs{pctRawRemainSinglets}</td>",
        "<td>$attribs{pctRawRemainUnassembled}</td>",
        "</tr>",
        "</table>";
        
    # has pe lib
    } elsif (exists $attribs{nonpelibrary} && exists $attribs{pelibrary}) {
        print $fh
        p,
        h3, "Redundancy and Lucy Trimming Statistics:",
        p,
        "<table border=1>",
        "<th bgcolor=\"lightgray\"></th>",
        "<th bgcolor=\"lightgray\">Reads</th>",
        "<th bgcolor=\"lightgray\">PE Reads</th>",
        "<th bgcolor=\"lightgray\">Contigs</th>",
        "<th bgcolor=\"lightgray\">Singlets</th>",
        "<th bgcolor=\"lightgray\">Unassembed (Singletons, Repeats, Outliers)</th>",
        "<tr>",
        "<td bgcolor=\"lightgray\">Raw (Reads are Pre-split)</td>",
        "<td colspan=\"2\" align=\"center\">$attribs{rawReads}</td>",
    #    "<td></td>",
        "<td>$attribs{rawContigs}</td>",
        "<td>$attribs{rawSinglets}</td>",
        "<td>$attribs{rawUnassembled}</td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">Newbler/contam filtered</td>",
        "<td>$attribs{newblerContamReads}</td>",
        "<td>$attribs{pe_newblerContamReads}</td>",
        "<td></td>",
        "<td></td>",
        "<td></td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">Non-Redundant</td>",
        "<td>$attribs{nonRedundant}</td>",
        "<td>$attribs{pe_nonRedundant}</td>",
        "<td></td>",
        "<td></td>",
        "<td></td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">Lucy Trimmed 99.3%</td>",
        "<td>$attribs{lucyTrimReads}</td>",
        "<td>$attribs{pe_lucyTrimReads}</td>",
        "<td>$attribs{lucyTrimContigs}</td>",
        "<td>$attribs{lucyTrimSinglets}</td>",
        "<td>$attribs{lucyTrimUnassem}</td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">% Redundant Reads</td>",
        "<td>$attribs{pctRedundantReads}</td>",
        "<td>$attribs{pepctRedundantReads}</td>",
        "<td></td>",
        "<td></td>",
        "<td></td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">% Lucy Trim Non-Redundant</td>",
        "<td>$attribs{pctLucyTrimNonRedundant}</td>",
        "<td>$attribs{pepctLucyTrimNonRedundant}</td>",
        "<td></td>",
        "<td></td>",
        "<td></td>",
        "</tr>",
        "<tr>",
        "<td bgcolor=\"lightgray\">% Raw Remaining</td>",
        "<td>$attribs{pctRawRemainReads}</td>",
        "<td>$attribs{pepctRawRemainReads}</td>",
        "<td>$attribs{pctRawRemainContigs}</td>",
        "<td>$attribs{pctRawRemainSinglets}</td>",
        "<td>$attribs{pctRawRemainUnassembled}</td>",
        "</tr>",
        "</table>";
    }
}

#============================================================================#
sub printPlots {
    
    my $fh = shift;
    my %attribs = @_;
    
    my $analysisDir = $attribs{analysisDir};
    
    print $fh
    p,
    imageText( "GC Histogram: $attribs{library} Lucy Trimmed Reads:",
        "$analysisDir/$attribs{'image.gc'}" ),
    imageText( "GC Histogram: $attribs{library} Lucy Trimmed Contigs Reads:",
        "$analysisDir/$attribs{'image.contigs.lucy.GC'}" ),
    imageText( "GC Histogram: $attribs{library} Lucy Trimmed Singlet Reads:",
        "$analysisDir/$attribs{'image.singlets.lucy.GC'}" ),
    imageText( "GC Histogram: $attribs{library} Lucy Trimmed Unassembled Reads:",
        "$analysisDir/$attribs{'image.unassembled.lucy.GC'}" ),
    imageText( "GC Histogram: $attribs{library} Lucy Trimmed Outliers:",
        "$analysisDir/$attribs{'image.outliers.lucy.GC'}" ),
    imageText( "GC Histogram: $attribs{library} Contig GC% vs. Contig Depth:",
        "$analysisDir/$attribs{'image.contigs.gcVsDepth'}"),
    imageText( "GC Histogram: $attribs{library} Contig Length vs. Contig Mean Coverage:",
        "$analysisDir/$attribs{'image.contigs.lengthVsMeanCov'}" ),
    imageText( "MEGAN: Taxa Distribution of $attribs{library}.lucy.fa Reads vs. NR DB",
        "$analysisDir/$attribs{'image.reads.taxa'}" ),
    imageText( "MEGAN: Phylogeny of $attribs{library}.lucy.fa Reads vs. NR DB",
        "$analysisDir/$attribs{'image.reads.phylo'}" ),
    imageText( "MEGAN: Phylogeny of $attribs{library}.lucy.fa Reads vs. NR DB Filtered By Class",
        "$analysisDir/$attribs{'image.reads.phylo2'}" ),
    imageText( "MEGAN: GC Histogram Plot of $attribs{library}.lucy.fa Reads vs. NR DB",
        "$analysisDir/$attribs{'image.megan'}" );
    
}

#============================================================================#
sub imageText {
    
    my $title = shift;
    my $image = shift;
    
    my $txt = "<div CLASS=\"nobreak\">".
              b($title).
              p;
              
    if ( defined $image && -e $image ) {
        $txt .= img( {src=>$image} );
    } else {
        $txt .= "&#60&#60no image&#62&#62";
    }
    $txt .= "</div>";

    return $txt;
    
}

#============================================================================#
sub printDownloads {
    
    my $fh = shift;
    my $linkDir = shift;
    my $attribsFile = shift;
    my %attribs = @_;
    
    my $lib = $attribs{library};
    my $pe_lib =  (exists $attribs{pelibrary}) ? $attribs{pelibrary} : "";
    my $non_pe_lib = (exists $attribs{nonpelibrary}) ? $attribs{nonpelibrary} : "" ;
    my $proj = $attribs{sampleId};
    
    
    my %files = ();
    tie %files, "Tie::IxHash"; # this will allow me to print the keys in insertion order.
    
    # Get file name.
    #
    foreach my $attrib (keys %attribs) {
        if ( $attrib =~ /^file\.(.+)/ ) {
            my $name = $1;
            $files{$name} = '';
        }
    }
    # Add file description to name.
    #
    foreach my $attrib (keys %attribs) {
        if ( $attrib =~ /^fileDesc\.(.+)/ && defined $files{$1} ) {
            my $name = $1;
            $files{$name} = $attribs{$attrib};
        }
    }
    
    print $fh
    p,
    h3,
    b("The following files may be downloaded from our secure http site:"),
    br,
    $attribs{url},
    p,
    "<table border=1>",
    "<tr bgcolor=\"lightgray\"><th>File</th><th>Description</th></tr>";
    
    while ( my ($file, $desc) = each %files ) {
        $file .= " <font color='red'>(missing)</font>" if !-l "$linkDir/$file";
        
        print $fh
        "<tr><td>",
        $file,
        "</td><td>",
        $desc,
        "</td></tr>";
    }
    
    print $fh "</table>";
    
}

#============================================================================#
sub byDepth {
    
    my $depthA = $a->[0];
    my $depthB = $b->[0];
    
    $depthA =~ s/^(\d+)\-*.*/$1/;
    $depthB =~ s/^(\d+)\-*.*/$1/;
    
    return $depthA <=> $depthB;
    
}

#============================================================================#
sub printhelp {
    my $verbose = shift || 1;
    pod2usage(-verbose=>$verbose);
    exit 1;
}

#============================================================================#

__DATA__

.nobreak{
  display: block;
  page-break-inside: avoid;
}
 
