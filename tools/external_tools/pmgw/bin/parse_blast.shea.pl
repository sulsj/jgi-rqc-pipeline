#!/usr/bin/env perl

# Example: /home/shea/Bin/parse_blast_for_Nisha.pl -b <blast_out> -o <blast_hits_table> 

# Turns normal default blast output into blastall  -m 8 tab format  , input file is blast_out containing blast output using standard blast settings, output file created is blast_hits_table

$| = 1; select STDERR;
$| = 1; select STDOUT;

#use strict;
use Bio::Perl;
use Bio::DB::GenBank;
use Bio::SeqIO::genbank;
use Bio::SearchIO;  # only one used
use lib qw/./;
use Getopt::Std;



my %opt;
my $status = getopts('b:o:',\%opt); 
my ($probes_for_blast_fasta,$blast_report,$outfile,$match_length_to_report,$out_for_stats,$outfile_keep);

if ($status == 0) {
    print "Usage: parse_blast_for_Nisha.pl \n-b <blast_output_to_be_parsed>\n-o <output_parsed_table>\n";
    die "Error processing command line";
}
if (defined($opt{b})) {
  $blast_report = $opt{b};
} else {
   die "No blast file given to be parsed, -b <blast_output_to_be_parsed>\n";
}


if (defined($opt{o})) {
  $out_for_stats = $opt{o};
} else {
  $out_for_stats ="blast_hits_table";
}


open OUT2, ">$out_for_stats" or die "Cannot open $out_for_stats for writing: $!.\n";

print OUT2 "query\tsubject\tsubject.desc\tpct.identity\talign.len\tmm\tgaps\tq.start\tq.end\ts.start\ts.end\te.value\tbits\tlongest.match\n"	;

my ($gb,$seq1,$seq2,$seqio,$searchio,$result,$hit,$hit_name,
    $hsp,$hsp_start,$acc,$searchio_parsing,$query_seq);


my $in = new Bio::SearchIO(-format => 'blast',
			   -file   => "$blast_report");

while( my $result = $in->next_result ) {
    my $name = $result->query_name;
    my $description = $result->query_description;
    $name .= " ".$description;
    $name =~ /sequence=([ATGC]+)/;
    my $full_probe_seq=$1;
    #print "full_probe_seq=$full_probe_seq\n";
    
    
    while( my $hit = $result->next_hit ) {
	my $this_hit_name=$hit->name();
	#print "\$this_hit_name1: $this_hit_name\n";
	$this_hit_name =~   s/^([a-z]*)\|([^\|]+)\|.*/$1|$2/ ;   #s/(gi.*)\|$/$1/;
	chomp($this_hit_name);	
	
        # print "this hit name2 (should be accno): $this_hit_name\n";
	
	my $hit_desc_no_commas =$hit->description();
	$hit_desc_no_commas =~ s/,/ /g;
	my  $match_len_max=0;
	
	while( my $hsp = $hit->next_hsp ) {
	    my $mm= sprintf( "%1.0f",(1-$hsp->frac_identical) * $hsp->length('total'));
	    my  $pid=sprintf("%.2f", $hsp->percent_identity);
	    my $homology_string= $hsp->homology_string;
	    #print "homology string: $homology_string\n";
	    my $longest_match=0;
	    while ($homology_string =~ /(\|+)/g) {
		#print "match: $1\n";
		my $match=length($1);
		if ($match > $longest_match) {
		    $longest_match=$match;
		    #print "$match\n";
		}
	    }

	    print OUT2 "$name\t", $hit->name(), "\t", $hit->description , "\t", $pid, "\t", $hsp->length('total'), "\t", $mm, "\t", $hsp->gaps, "\t", $hsp->start('query'), "\t", $hsp->end('query'), "\t", $hsp->start('hit'), "\t", $hsp->end('hit'), "\t", $hsp->evalue, "\t", $hsp->bits, "\t$longest_match\n" ; 
	 
	} # while( my $hsp = $hit->next_hsp )
	
    } # while( my $hit = $result->next_hit ) {
    
    
    

} # while( my $result = $in->next_result )

close OUT2;


sub read_fasta_input {
    my $fasta=shift;
    open INDATA,"$fasta" or die "Can't open $fasta: $!\n";
    #get all sequences and make a hash called strain with id and sequence info
    my $sequence = "";
    my $id;
    my $num_genomes=0;
    my %strain=();
    while (my $line = <INDATA>) {
        #chomp $line;  
        if ($line =~ /^>(.*)/) {
            if ($sequence ne "") {
                $strain{$id}=uc($sequence); #make hash with $id as keys and $sequence as values
                $num_genomes++;
            }
            $id = $1;
            chomp($id);
            $sequence = "";
        } else {
            chomp($line);
            $line =~ s/[-|\*]//g; #Get rid of "-" and "*" and "|" if it's from an alignment
            $line =~ s/[\n\t\s]//g; # Get rid of spaces, tabs, and new lines
            $sequence .= $line;
        }
    }

    # Add last one
    if (defined $id) {
      $strain{$id}=uc($sequence);
      $num_genomes++;
    }
    close INDATA or warn  "Can't close $fasta: $!\n";
    return %strain;

} # end sub read_fasta_input

