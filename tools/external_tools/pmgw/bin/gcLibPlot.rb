require 'yaml'

# This script generates a jpeg of a gc histogram for library reads using R
$VERBOSE = nil

if ARGV.size != 2
  STDERR.puts "Usage: $0 <inFile> <description of source of reads, e.g. 'Library FUAY'>"
  Process.exit  
end

inFile = ARGV[0]
if !File.exists? inFile or File.zero? inFile
  STDERR.puts "File #{inFile} does not exist or empty"
  Process.exit
end

lib = ARGV[1]

outFile = inFile + ".jpg"
tempFile = inFile + ".temp"
cmd = <<EOF 
gc<-read.table("#{inFile}", header=TRUE);
rangeX = range(round(min(gc[,5]) - 5, -1), round(max(gc[,5])+5, -1))
jpeg("#{outFile}", quality=100, height=480, width=640)
xsub<-sprintf("avg=%.2f%%, std dev=%.2f%%, max=%.2f%%, min=%.2f%%", mean(gc[,5]), sd(gc[,5]), max(gc[,5]),  min(gc[,5]))
hist(gc[,5], breaks = 200, col=8, border=1, xlab="GC(%)", ylab="# of Reads", main="GC Histogram for #{lib}", xlim=rangeX, sub=xsub)
axis(1, seq(rangeX[1], rangeX[2], by=5)) 
axis(2)
box()
dev.off()
EOF

config = YAML.load_file(File.dirname(__FILE__) + '/../config/plot.yml')

File.open(tempFile, "w") { |f| f.puts cmd }
system("#{config['R']} --no-save < #{tempFile}")

