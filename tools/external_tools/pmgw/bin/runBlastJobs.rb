#!/usr/bin/env ruby

require 'yaml'
require "getoptlong"


$VERBOSE = nil

require File.dirname(__FILE__) + '/ioUtils'
require File.dirname(__FILE__) + '/clusterBlast'

File.umask(0)


def usage
  puts "Usage:   #{$0} -l <library> -c <cluster_name> --od <output_directory> fasta_file"
  Process.exit
end

opts = GetoptLong.new(
  [ '--cluster', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '-c', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--library', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '-l', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--outdir', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--od', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--jn', GetoptLong::OPTIONAL_ARGUMENT ]
)

# TODO: need to be in project dir - probably prompt first!
usage if ARGV.size < 1

config = YAML.load_file(File.dirname(__FILE__) + '/../config/blast.yml')

libId = nil
cluster = nil
outdir = nil
jobName = nil

opts.each do |opt, arg|
  case opt
    when '--cluster', '-c'
      cluster = arg

      if cluster == 'crius'
        config['sge_dir'] = "/opt/uge/#{cluster}/uge/#{cluster}/common"
        config['settings_sh'] = "/opt/uge/#{cluster}/uge/#{cluster}/common/settings.sh"
        config['settings_csh'] = "/opt/uge/#{cluster}/uge/#{cluster}/common/settings.csh"
      else
        config['sge_dir'] = "/opt/sge/#{cluster}/common"
        config['settings_sh'] = "/opt/sge/#{cluster}/common/settings.sh"
        config['settings_csh'] = "/opt/sge/#{cluster}/common/settings.csh"
      end
    when '--library', '-l'
      libId = arg
    when '--outdir', '--od'
      outdir = arg
    when '--jn'
      jobName = arg
  end
end

contigFa = ARGV[0]
basename = File.basename(contigFa)

if !File.exists?(config['sge_dir'])
  STDERR.puts "You are not logged into a machine that can access the SGE cluster"
  Process.exit
elsif !File.exists?(contigFa)
  STDERR.puts contigFa + " is missing"
  Process.exit
end

if libId.nil?
  pid = Process.pid.to_s
else
  pid = "#{libId}_" + Process.pid.to_s 
end

if outdir.nil?
  Dir.mkdir(config['cluster_blast_data']) if !File.exists? config['cluster_blast_data']
  outdir = "#{config['cluster_blast_data']}/clusterblast_" + pid
else
  outdir = File.expand_path(outdir)

  #if !File.exists?(outdir)
  #  STDERR.puts "#{outdir} is missing"
  #  Dir.mkdir(config['cluster_blast_data']) if !File.exists? config['cluster_blast_data']
  #  outdir = "#{config['cluster_blast_data']}/clusterblast_" + pid
  #
  #  STDERR.puts "Setting output directory to #{outdir}"
  #else
  #  outdir += "/clusterblast_" + pid
  #end
end

if cluster.nil? || cluster.empty?
  cluster = config['cluster']
end

puts "\nSending job to #{cluster}\n"
cBlast = ClusterBlast.new(outdir, config)

# run blastn against silva for each library
clusterStarted = false

# run blastx against nr on sge
if jobName.nil?
  cBlast.runJob(pid, contigFa, libId)
else
  cBlast.runJob(pid, contigFa, jobName)
end

puts "Blastx job running for #{pid}"
clusterStarted = true  

Email.send(config['status_email_address'], "Metagenome QC Blastx Status", "Cluster jobs started") if clusterStarted

pid = fork do
  cBlast.monitor
end
Process.detach(pid)

