#!/usr/bin/env perl

use strict;
use FindBin qw($RealBin);
use lib "$RealBin/../lib";
#use lib '/house/homedirs/g/galaxy/scripts';
use ReadCov;

my $usage = <<'ENDHERE';
NAME
    aceCov.pl
PURPOSE
    To extract coordinates of reads on contigs from an Ace file, generate table of coverage stats plus genome track.
SYNOPSIS
    aceCov.pl assembly.ace assembly.tsv assembly.wig
INPUT
    $1 : Ace file
OUTPUT
    $2 : Table of contig read coverage statistics
    $3 : Read coverage genome browser track in Wig format
    $4 :Number of reads genome browser track in Wig format (optional)
AUTHOR
    Edward Kirton (ESKirton@LBL.gov)
VERSION
    0.2 - 4/13/2010
ENDHERE

die($usage) unless @ARGV == 3 or @ARGV == 4;
my ($ace_infile,$tsv_outfile, $cov_outfile, $rds_outfile) = @ARGV;

# DEFINE VARIABLES
my ($numb_contigs, $tot_numb_reads);                           # GENOME VARS
my ($contig_id,    $contig_len, $contig_numb_reads);           # CONTIG VARS
my ($read_id,      $strand, $start, $stop, $deletions_str);    # READ VARS
my $seq;                                                       # USED WHILE PARSING BOTH CONTIGS AND READS
my @pads               = ();                                   # LOCATION OF PAD CHARACTERS (*) IN CONTIG CONSENSUS SEQUENCE
my %read_padded_starts = ();                                   # READ ID => PADDED LOC. OF START IN ALIGNMENT
my %read_strands       = ();                                   # READ I => STRAND (+ OR -)
my %read_deletions     = ();                                   # READ ID => LOCATION OF DELETIONS
my $start_time         = time;
my $str                = '';
my $read_cov = new ReadCov();
$read_cov->read_ace($ace_infile);

# PARSE ACE FILE
open(ACE, "<$ace_infile") or die("ERROR: Unable to open infile, $ace_infile: $!\n");
while (<ACE>) {
	chomp;
	if (/^AS (\d+) (\d+)$/) { # HEADER
		($numb_contigs, $tot_numb_reads) = ($1, $2);

	} elsif (/^CO (\S+) \d+ (\d+) \d+ \w$/) { # CONTIG
		($contig_id, $contig_numb_reads) = ($1,$2);
		$contig_len         = 0;
		@pads               = ();
		%read_padded_starts = ();
		%read_strands       = ();
		%read_deletions     = ();
		$seq                = '';
        while(<ACE>) {
            chomp;
            last unless $_;
            $seq .= $_;
        }
        @pads       = get_positions('*', $seq);
        $contig_len = length($seq) - scalar(@pads);
        $seq        = '';

	} elsif (/^AF (\S+) (\w) (-?\d+)$/) {    # READ ID, STRAND, START
		$read_padded_starts{$1} = $3;
		$read_strands{$1} = $2 eq 'U' ? '+' : '-';

	} elsif (/^RD (\S+) \d+ \d+ \d+/) { # READ SEQUENCE
		($read_id, $seq) = ($1, '');
        while(<ACE>) {
            chomp;
            last unless $_;
            $seq .= $_;
        }

        # TRIM GAPS FROM ENDS OF READS
        while (substr($seq, 1) eq '*') {
            $seq = substr($seq, 1, length($seq) - 1);
            ++$read_padded_starts{$read_id};
        }
        while (substr($seq, -1) eq '*') {
            $seq = substr($seq, 0, length($seq) - 1);
        }

        # DETERMINE READ START AND STOP POSITIONS
        $start = $read_padded_starts{$read_id};
        $stop  = $start + length($seq) - 1;       # PADDED LOC.

        # DISCOUNT PAD CHARACTERS
        my @pads_in_read = get_positions('*', $seq);
        my $o = $read_padded_starts{$read_id} - 1;
        for (my $i = 0 ; $i <= $#pads_in_read ; ++$i) { $pads_in_read[$i] += $o }
        ($start, $stop, $deletions_str) = discount_pad_characters(\@pads, $start, $stop, \@pads_in_read);
        $read_deletions{$read_id} = $deletions_str;

        # ADJUST START AND STOP COORDINATES TO BE WITHIN VALID RANGE
        $start = 1           if $start < 1;             # CANNOT HAVE START POSITION BEFORE BEGINING OF CONTIG CONSENSUS SEQ
        $stop  = $contig_len if $stop > $contig_len;    # CANNOT HAVE STOP POSITION AFTER END OF CONTIG CONSENSUS SEQ

        # SAVE ADJUSTED READ COORDINATES
        $read_cov->add_read($contig_id,[$start-1],[$stop-1]);
    }
}
close ACE;

# WRITE OUTFILES
$read_cov->write_tsv($tsv_outfile);
$read_cov->write_wig($cov_outfile, $rds_outfile);
exit;

###############################################################################
# SUBROUTINES

sub round {
	my $x = shift;
	return int($x + .5);
}

sub discount_pad_characters {
	my ($padsAr, $start, $stop, $pads_in_read_Ar) = @_;
	my ($numb_pads_before_start, $numb_pads_before_stop);
	my @deletions = ();

	# IF THERE ARE NO PADS, THERE IS NOTHING TO DO
	return ($start, $stop, '') unless scalar(@$padsAr);

	# IF ALL PADS BEFORE START THEN NO NEED TO SEARCH THROUGH LIST
	if ($start > $padsAr->[$#$padsAr]) {
		# ALL PADS ARE BEFORE THE START (AND STOP)
		$numb_pads_before_start = $numb_pads_before_stop = scalar(@$padsAr);
		$start -= $numb_pads_before_start;
		$stop  -= $numb_pads_before_stop;
		return ($start, $stop, '');
	}

	# FIND INDEX OF FIRST ELEMENT GREATER THAN READ START POSITION AND CALCULATE UNPADDED STOP POSITION
	if ($start < $padsAr->[0]) {
		$numb_pads_before_start = 0;    # THERE ARE NO PADS BEFORE START

	} else
	{
		# INTERPOLATION SEARCH
		my ($i0, $i, $i1, $last_i) = (0, int($#$padsAr / 2), $#$padsAr, -1);
		while (abs($i - $last_i) > 10) {
			$last_i = $i;

			# EXTRAPOLATE INDEX
			my $scale = ($padsAr->[$i1] - $padsAr->[$i0]) / ($i1 - $i0);
			$i += round(($start - $padsAr->[$i]) / $scale);

			$i = 0         if $i < 0;
			$i = $#$padsAr if $i > $#$padsAr;

			$i0 = $i if $i > $i0 and $padsAr->[$i] < $start;
			$i1 = $i if $i < $i1 and $padsAr->[$i] > $start;
		}

		# VERY CLOSE SO JUST SCAN ARRAY SEQUENTIALLY
		if ($padsAr->[$i] > $start) {
			do { --$i } while ($padsAr->[$i] > $start and $i > 0);
		}
		if ($padsAr->[$i] < $start) {
			do { ++$i } while ($padsAr->[$i] <= $start and $i < $#$padsAr);
		}
		$numb_pads_before_start = $i;
	}
	$start -= $numb_pads_before_start;

	# FIND INDEX OF FIRST ELEMENT GREATER THAN READ STOP POSITION AND CALCULATE UNPADDED STOP POSITION
	my %pads_in_contig = ();
	if ($stop < $padsAr->[0])
	{
		$numb_pads_before_stop = 0;    # THERE ARE NO PADS BEFORE STOP

	} else
	{
		# VERY CLOSE SO JUST SCAN ARRAY SEQUENTIALLY
		my $i = $numb_pads_before_start;
		while ($padsAr->[$i] <= $stop and $i < $#$padsAr) {
			$pads_in_contig{$padsAr->[$i]} = 1;
			++$i;
		}
		$numb_pads_before_stop = $i;
	}
	$stop -= $numb_pads_before_stop;

	# CALCULATE POSITION OF DELETIONS IN READ
	foreach my $p (@$pads_in_read_Ar) {
		if (!exists($pads_in_contig{$p})) {
			my $i = $numb_pads_before_start;
			while ($padsAr->[$i] <= $p and $i < $#$padsAr) { ++$i }
			push @deletions, $p - $i;
		}
	}

	my $deletions_str = scalar(@deletions) ? join(',', @deletions) : '';
	return ($start, $stop, $deletions_str);
}

sub get_positions {
	my ($char, $string) = @_;
	my @positions = ();
	my $offset    = 0;
	my $result    = index($string, $char, $offset);
	while ($result != -1) {
		push @positions, $result + 1;
		$offset = $result + 1;
		$result = index($string, $char, $offset);
	}
	return @positions;
}
__END__
