#!/usr/bin/env perl

# HISTORY:
# 0.1 2010-04-13 EK file created
# 0.2 2010-05-12 EK add methods to read wig files and return coverage stats for multiexon loci
# 0.3 2010-05-25 EK improved input options; merged .pl and .pm files since pm file not used by any other scripts
# 0.4 2010-11-10 EK added blast and sam support
# 0.5 2010-11-23 EK added ace support

###############################################################################
# DRIVER

use strict;
use Getopt::Long;

my $usage = <<'ENDHERE';
NAME
    readCov.pl
PURPOSE
    To analyze read coverage over contig sequences, generate a stats table and Wig file.
    When optional gene coordinates table is provided, also generates table of coverage per gene.
INPUT
    --ref <*.fna> : reference genome contigs (required unless using --ace input)
    --gff <*.gff3> : gene coordinates infile in GFF3 format (optional)
    At least one alignment file required; these formats are supported:
    --blast <*.tsv> : tab-delimited BLAST output (i.e. using -m 8 option); uses first alignment for any query
    --map <*.tsv> : alignments in Galaxy interval format; assumes reads are uniquely mapped
        col 1 : contig id
        col 2 : left coords list (e.g. 2342,2544)
        col 3 : right coords list (e.g. 2444,2988)
    --sam <*.sam> : alignments in Sam format; uses first alignment for any query; requires only one alignment per row
    --bam <*.bam> : alignments in Bam format
    --ace <*.ace> : assembly in Ace format
OUTPUT
    --con <*.tsv> : Table of contig coverage statistics with the following fields:
        contig identifier
        contig length
        number of reads
        median read coverage
        mean read coverage
        standard deviation of read coverage
    --exp <*.tsv> : Table of gene coverage statistics with the following fields:
        contig identifier
        contig length
        gene identifier
        gene length
        number of reads
        median read coverage
        mean read coverage
        standard deviation of read coverage
    --cov <*.wig> : Read coverage UCSC track in Wig format (optional)
    --rds <*.wig> : Number of reads UCSC track in Wig format. Note: Wig uses 0-based coordinate system (optional)
AUTHOR/SUPPORT
    Edward Kirton (ESKirton@LBL.gov)
ENDHERE

# INIT VARS
my ($help, $ref_infile, $interval_infile, $sam_infile, $bam_infile, $blast_infile, $contigs_tsv_outfile, $cov_wig_outfile, 
$rds_wig_outfile, $genes_infile, $genes_outfile, $ace_infile);

# GET OPTIONS
GetOptions(
    'ref=s'    => \$ref_infile,
    'gff=s'    => \$genes_infile,
    'map=s'    => \$interval_infile,
    'blast=s'  => \$blast_infile,
    'sam=s'    => \$sam_infile,
    'bam=s'    => \$bam_infile,
    'con=s'   => \$contigs_tsv_outfile,
    'exp=s'   => \$genes_outfile,
    'cov=s'   => \$cov_wig_outfile,
    'rds=s'   => \$rds_wig_outfile,
    'ace=s'   => \$ace_infile,
    'h|help'  => \$help
);
if ($help) { print $usage; exit; }
$genes_infile='' if defined($genes_infile) and $genes_infile eq 'None';
$genes_outfile='' if defined($genes_outfile) and $genes_outfile eq 'None';
die("Missing genes outfile\n") if $genes_infile and !$genes_outfile;
die("Infile required\n") unless $interval_infile or $sam_infile or $blast_infile or $ace_infile;

# INIT OBJECT
my $read_cov=new ReadCov();    # object uses 0-based coordinates
$read_cov->read_fasta($ref_infile) unless $ace_infile;

# LOAD ALIGNMENTS

if ($interval_infile) {
    $read_cov->read_interval_file($interval_infile)
} elsif ($sam_infile) {
    $read_cov->read_sam_file($sam_infile);
} elsif ($bam_infile) {
    $read_cov->read_sam_file($bam_infile,1);
} elsif ($blast_infile) {
    $read_cov->read_blast_file($blast_infile);
} elsif ($ace_infile) {
    $read_cov->read_ace($ace_infile);
} else {
    die("Alignments infile required\n");
}

# CONTIG OUTPUT
$read_cov->write_cov_wig($cov_wig_outfile) if defined($cov_wig_outfile);
$read_cov->write_rds_wig($rds_wig_outfile) if defined($rds_wig_outfile);
$read_cov->write_tsv($contigs_tsv_outfile);
exit unless $genes_infile and $genes_outfile;

# GENE OUTPUT
$read_cov->write_gene_cov_tsv($genes_infile, $genes_outfile);
exit;

###############################################################################
# OBJECT

package ReadCov;

=head1 NAME

ReadCov

=head1 PURPOSE

To store entire genome sequence and read coverage profile.

=head2 NOTE

genomic coordinates start at position 0 (not 1), consistent with UCSC bed and psl formats, but
inconsistent with Sanger GFF and wig formats (which numbers from 1)

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 VERSION

0.3 (5/27/10)

=head1 METHODS

=cut

# objects are blessed arrays, not hashes.
use constant {
    SEQ => 0,    # hash of chrom ID => nucleotide sequence
    LEN => 1,    # hash of chrom ID => chrom length (in bp)
    COV => 2,    # hash of chrom ID => read coverage per base
    RDS => 3     # hash of chrom ID => number of reads (each read contributes at a single point)
};

use PDL;
use PDL::IO::Misc;
use PDL::IO::FastRaw;
use PDL::NiceSlice;
use IO::File;

###############################################################################
## INIT

=head2 new

Create new object

=cut

sub new
{
    my ($class, $infile) = @_;
    my $this = [ {}, {}, {}, {} ];
    bless $this, $class;
    $this->read_fasta($infile) if $infile;
    return $this;
}

###############################################################################
# LOAD SEQUENCE DATA

=head2 read_fasta

Load genome sequence Fasta file

=cut

sub read_fasta
{
    my ($this, $infile) = @_;
    my ($chr, $seq) = ('', '');
    open(FASTA, "<$infile") or die("ERROR: Cannot open $infile\n");
    while (<FASTA>) {
        chomp;
        if (/^>(\S+)/) {
            $this->_save_chr($chr, $seq);
            ($chr, $seq) = ($1, '');
        } else {
            $seq .= $_;
        }
    }
    $this->_save_chr($chr, $seq);
    close FASTA;
    return

}

# private sub
sub _save_chr
{
    my ($this, $chr, $seq) = @_;
    return unless $chr and $seq;
    $this->[SEQ]->{$chr} = uc($seq);
    my $len = $this->[LEN]->{$chr} = length($seq);
    $this->[COV]->{$chr} = zeroes($len);
    $this->[RDS]->{$chr} = zeroes($len);
}

=head2 read_ace

Load genome sequence from Ace file

=cut

sub read_ace
{
    my ($this, $ace_infile) = @_;

    # DEFINE VARIABLES
    my ($numb_contigs, $tot_numb_reads);                           # GENOME VARS
    my ($contig_id,    $contig_len, $contig_numb_reads);           # CONTIG VARS
    my ($read_id,      $strand, $start, $stop, $deletions_str);    # READ VARS
    my $seq;                                                       # USED WHILE PARSING BOTH CONTIGS AND READS
    my @pads               = ();                                   # LOCATION OF PAD CHARACTERS (*) IN CONTIG CONSENSUS SEQUENCE
    my %read_padded_starts = ();                                   # READ ID => PADDED LOC. OF START IN ALIGNMENT
    my %read_strands       = ();                                   # READ I => STRAND (+ OR -)
    my %read_deletions     = ();                                   # READ ID => LOCATION OF DELETIONS
    my $start_time         = time;
    my $str                = '';

    # PARSE ACE FILE
    open(ACE, "<$ace_infile") or die("ERROR: Unable to open infile, $ace_infile: $!\n");
    while (<ACE>) {
        chomp;
        if (/^AS (\d+) (\d+)$/) { # HEADER
            ($numb_contigs, $tot_numb_reads) = ($1, $2);

        } elsif (/^CO (\S+) \d+ (\d+) \d+ \w$/) { # CONTIG
            ($contig_id, $contig_numb_reads) = ($1,$2);
            $contig_len         = 0;
            @pads               = ();
            %read_padded_starts = ();
            %read_strands       = ();
            %read_deletions     = ();
            $seq                = '';
            while(<ACE>) {
                chomp;
                last unless $_;
                $seq .= $_;
            }
            @pads       = get_positions('*', $seq);
            $contig_len = length($seq) - scalar(@pads);
            my $unpadded_seq = $seq;
            $unpadded_seq =~ s/\*//g;
            $this->_save_chr($contig_id,$unpadded_seq);
            $seq        = '';

        } elsif (/^AF (\S+) (\w) (-?\d+)$/) {    # READ ID, STRAND, START
            $read_padded_starts{$1} = $3;
            $read_strands{$1} = $2 eq 'U' ? '+' : '-';

        } elsif (/^RD (\S+) \d+ \d+ \d+/) { # READ SEQUENCE
            ($read_id, $seq) = ($1, '');
            while(<ACE>) {
                chomp;
                last unless $_;
                $seq .= $_;
            }

            # TRIM GAPS FROM ENDS OF READS
            while (substr($seq, 1) eq '*') {
                $seq = substr($seq, 1, length($seq) - 1);
                ++$read_padded_starts{$read_id};
            }
            while (substr($seq, -1) eq '*') {
                $seq = substr($seq, 0, length($seq) - 1);
            }

            # DETERMINE READ START AND STOP POSITIONS
            $start = $read_padded_starts{$read_id};
            $stop  = $start + length($seq) - 1;       # PADDED LOC.

            # DISCOUNT PAD CHARACTERS
            my @pads_in_read = get_positions('*', $seq);
            my $o = $read_padded_starts{$read_id} - 1;
            for (my $i = 0 ; $i <= $#pads_in_read ; ++$i) { $pads_in_read[$i] += $o }
            ($start, $stop, $deletions_str) = discount_pad_characters(\@pads, $start, $stop, \@pads_in_read);
            $read_deletions{$read_id} = $deletions_str;

            # ADJUST START AND STOP COORDINATES TO BE WITHIN VALID RANGE
            $start = 1           if $start < 1;             # CANNOT HAVE START POSITION BEFORE BEGINING OF CONTIG CONSENSUS SEQ
            $stop  = $contig_len if $stop > $contig_len;    # CANNOT HAVE STOP POSITION AFTER END OF CONTIG CONSENSUS SEQ

            # SAVE ADJUSTED READ COORDINATES
            $read_cov->add_read($contig_id,[$start-1],[$stop-1]);
        }
    }
    close ACE;

    sub discount_pad_characters {
        my ($padsAr, $start, $stop, $pads_in_read_Ar) = @_;
        my ($numb_pads_before_start, $numb_pads_before_stop);
        my @deletions = ();

        # IF THERE ARE NO PADS, THERE IS NOTHING TO DO
        return ($start, $stop, '') unless scalar(@$padsAr);

        # IF ALL PADS BEFORE START THEN NO NEED TO SEARCH THROUGH LIST
        if ($start > $padsAr->[$#$padsAr]) {
            # ALL PADS ARE BEFORE THE START (AND STOP)
            $numb_pads_before_start = $numb_pads_before_stop = scalar(@$padsAr);
            $start -= $numb_pads_before_start;
            $stop  -= $numb_pads_before_stop;
            return ($start, $stop, '');
        }

        # FIND INDEX OF FIRST ELEMENT GREATER THAN READ START POSITION AND CALCULATE UNPADDED STOP POSITION
        if ($start < $padsAr->[0]) {
            $numb_pads_before_start = 0;    # THERE ARE NO PADS BEFORE START

        } else
        {
            # INTERPOLATION SEARCH
            my ($i0, $i, $i1, $last_i) = (0, int($#$padsAr / 2), $#$padsAr, -1);
            while (abs($i - $last_i) > 10) {
                $last_i = $i;

                # EXTRAPOLATE INDEX
                my $scale = ($padsAr->[$i1] - $padsAr->[$i0]) / ($i1 - $i0);
                $i += round(($start - $padsAr->[$i]) / $scale);

                $i = 0         if $i < 0;
                $i = $#$padsAr if $i > $#$padsAr;

                $i0 = $i if $i > $i0 and $padsAr->[$i] < $start;
                $i1 = $i if $i < $i1 and $padsAr->[$i] > $start;
            }

            # VERY CLOSE SO JUST SCAN ARRAY SEQUENTIALLY
            if ($padsAr->[$i] > $start) {
                do { --$i } while ($padsAr->[$i] > $start and $i > 0);
            }
            if ($padsAr->[$i] < $start) {
                do { ++$i } while ($padsAr->[$i] <= $start and $i < $#$padsAr);
            }
            $numb_pads_before_start = $i;
        }
        $start -= $numb_pads_before_start;

        # FIND INDEX OF FIRST ELEMENT GREATER THAN READ STOP POSITION AND CALCULATE UNPADDED STOP POSITION
        my %pads_in_contig = ();
        if ($stop < $padsAr->[0])
        {
            $numb_pads_before_stop = 0;    # THERE ARE NO PADS BEFORE STOP

        } else
        {
            # VERY CLOSE SO JUST SCAN ARRAY SEQUENTIALLY
            my $i = $numb_pads_before_start;
            while ($padsAr->[$i] <= $stop and $i < $#$padsAr) {
                $pads_in_contig{$padsAr->[$i]} = 1;
                ++$i;
            }
            $numb_pads_before_stop = $i;
        }
        $stop -= $numb_pads_before_stop;

        # CALCULATE POSITION OF DELETIONS IN READ
        foreach my $p (@$pads_in_read_Ar) {
            if (!exists($pads_in_contig{$p})) {
                my $i = $numb_pads_before_start;
                while ($padsAr->[$i] <= $p and $i < $#$padsAr) { ++$i }
                push @deletions, $p - $i;
            }
        }

        my $deletions_str = scalar(@deletions) ? join(',', @deletions) : '';
        return ($start, $stop, $deletions_str);

        sub get_positions {
            my ($char, $string) = @_;
            my @positions = ();
            my $offset    = 0;
            my $result  = CORE::index($string, $char, $offset);
            while ($result != -1) {
                push @positions, $result + 1;
                $offset = $result + 1;
                $result = CORE::index($string, $char, $offset);
            }
            return @positions;
        }

        sub round {
            my $x = shift;
            return int($x + .5);
        }
    }
}


###############################################################################
# LOAD ALIGNMENT DATA

=head2 read_interval_file

Loads single-line records from a tabular file.  Multiple alignment blocks are indicated by comma-separate lists of start/end coords.

Interval format has a 0-based start coord and 1-based end coord!

=cut

sub read_interval_file
{
    my ($this, $infile) = @_;

    # VARS
    my @row;
    my $ref_id;
    my @starts;
    my @ends;

    # READ
    open(ALIGN, "<$infile") or die($!);
    while (<ALIGN>) {
        next if /^#/;    # skip comment lines
        next if /^\*/;    # unaligned read
        chomp;
        @row      = split(/\t/);
        $ref_id   = $row[0];
        @starts   = split(/,/, $row[1]); # in infile, start is 0-based!
        @ends     = map { --$_ } split(/,/, $row[2]); # in infile, end is 1-based! convert to 0-based.
        if ($ends[0] < $starts[0]) {
            # Coords are always with respect to top strand (we don't current distinguish between +/-).
            # Interval files normally are correctly ordered but we check just in case.
            for (my $i = 0 ; $i <= $#starts ; $i++) {
                ($starts[$i], $ends[$i]) = ($ends[$i], $starts[$i]);    # swap
            }
        }
        $this->add_read($ref_id, \@starts, \@ends);
    }
    close ALIGN;
}

sub read_blast_file
{
    my ($this, $infile) = @_;

    # VARS
    my @row;
    my $ref_id;
    my $start;
    my $end;
    my $prev_query_id='';

    # READ
    open(IN, "<$infile") or die($!);
    while (<IN>) {
        chomp;
        @row      = split(/\t/);
        next unless @row=12;
        next if $row[0] eq $prev_query_id;
        $prev_query_id=$row[0];
        $ref_id   = $row[1];
        $start=$row[8]-1; # convert to 0-based coordinates
        $end=$row[9]-1; # convert to 0-based coordinates
        $this->add_read($ref_id, [$start], [$end]);
    }
    close IN;
}

sub read_sam_file {
    my ($this, $infile, $is_bam)=@_;

    my $prev_read_id='';
    if ($is_bam) {
        open(IN, "samtools view $infile|") or die($!);
    } else {
        open(IN, "<$infile") or die($!);
    }
    while (<IN>) {
        chomp;
        next if /^@/; # header section
        my @row=split(/\t/);
        my $read_id=$row[0];
        my $flag=$row[1];
        my $strand= $flag & (1 << 4) ? '-':'+';
        my $ref_id=$row[2];
        next if $ref_id eq '*'; # unaligned
        my $cigar=$row[5];
        my $len=ref_length($cigar);
        my $left = $row[3]-1; # convert to 0-based coord system
        my $right=$left + $len - 1; # convert to 0-based coord system
        $this->add_read($ref_id,[$left],[$right]);
        $prev_read_id=$read_id;
    }
    close IN;
    return;

    sub ref_length {
        my $fullcigar=shift;
        my $len=0;
        my $cigar=$fullcigar;
        while($cigar) {
            if ($cigar =~ /^(\d+)([MIDNSHP])(.*)$/) {
                $cigar=$3;
                unless ($2 eq 'I' or $2 eq 'S' or $2 eq 'H') { $len += $1 ? $1:1 }
            } else {
                die("Unable to parse cigar string, \"$fullcigar\"\n");
            }
        }
        return $len;
    }
}

###############################################################################
## ACCESSORS

=head2 seq

Given a chromosome ID, returns the complete sequence.

=cut

sub seq
{
    my ($this, $chr) = @_;
    die("$chr does not exist\n") unless exists($this->[SEQ]->{$chr});
    return $this->[SEQ]->{$chr};
}

=head2 chrs

Returns an alphabetically-sorted list of chromosome IDs

=cut

sub chrs
{
    my $this = shift;
    return sort keys %{$this->[LEN]};
}

=head2 size

Given a chromosome ID, returns the length in base pairs.

=cut

sub size
{
    my ($this, $chr) = @_;
    die("$chr does not exist\n") unless exists($this->[LEN]->{$chr});
    return $this->[LEN]->{$chr};
}

=head2 sizes

Returns a hash of chromosome ID and lengths.

=cut

sub sizes
{
    my $this = shift;
    # Note: Returns a copy of the data held in the object, not a pointer to the object's private data!
    my %sizes;
    foreach my $chr (keys %{$this->[LEN]}) {
        $sizes{$chr} = $this->[LEN]->{$chr};
    }
    return \%sizes;
}

=head2 get_subseq

Return genome sequence at a prescribed locus. Note: 0-based coordinates are used.

=cut

sub get_subseq
{
    my ($this, $chr, $start, $size, $strand) = @_;

    # Validate input
    die("UNKNOWN CONTIG: $chr ($start,$size,$strand)\n") unless exists $this->[SEQ]->{$chr};
    die("BAD START: $start ($chr,$size,$strand)\n")      unless $start >= 0;
    die("BAD LEN: $size ($chr,$start,$strand)\n")        unless $size >= 1;
    $strand = '+' if !defined($strand);
    die("BAD STRAND: $strand ($chr,$start,$size)\n") unless $strand =~ m/^[+-]$/;
    my $end = $start + $size - 1;
    die("End coordinate exceeds end of contig: $chr, $start, $size, $strand\n") if $end > ($this->[LEN]->{$chr} - 1);

    # Get substring, reverse complement -ve strand
    my $subseq = substr($this->[SEQ]->{$chr}, $start, $size);
    return $strand eq '+' ? $subseq : rev_comp($subseq);

    # private sub
    sub rev_comp
    {
        my $seq  = shift;
        my $seq2 = reverse($seq);
        $seq2 =~ tr/ATCGRYatcgry/TAGCYRtagcyr/;
        return $seq2;
    }
}

###############################################################################
## INPUT

=head2 add_read

Save a read's alignment.  Note: 0-based coordinates are used.

=cut

sub add_read
{
    my ($this, $chr, $starts, $ends, $x) = @_;
    # VALIDATE INPUT
    die("Cannot add read to unknown chr, $chr\n")    unless exists($this->[COV]->{$chr});
    die("Invalid starts array\n") unless @$starts;
    die("Invalid ends array\n")   unless @$ends;
    $x = 1 unless $x;    # used for adding several identical reads at once (optional; default to 1)

    # INCREMENT COVERAGE VECTOR FOR EACH EXON LOCUS
    my $len=0;
    for (my $i = 0 ; $i <= $#$starts ; $i++) {
        my $start = $starts->[$i];
        unless ($start >= 0) {
            print "Warning: Invalid start coordinate, $start, for contig, $chr (" . $this->[LEN]->{$chr} . " bp)\n";
            $start=0;
        }
        unless ($start < $this->[LEN]->{$chr}) {
            die "Invalid start coordinate, $start, for contig, $chr (" . $this->[LEN]->{$chr} . " bp)\n" 
        }
        my $end = $ends->[$i];

        if ( $end >= $this->[LEN]->{$chr} ) {
            print "Warning: Invalid end coordinate, $end, for contig, $chr (" . $this->[LEN]->{$chr} . " bp)\n";
            $end = $this->[LEN]->{$chr} - 1;
        }
        $this->[COV]->{$chr}->($start : $end) += $x;
        $len += ($end-$start+1);
    }

    # INCREMENT READ COUNTER
    # choose midpoint
    my $half = int($len/2);
    my $pos;
    for (my $i=0; $i<=$#$starts; $i++) {
        my $start = $starts->[$i];
        my $end = $ends->[$i];
        my $len = $end-$start+1;
        if ($half < $len) { # midpoint is in this block
            $pos = $start + $half;
        } else {
            $half -= $len;
        }
    }
    $this->[RDS]->{$chr}->($pos) += $x;
}

###############################################################################
## STATS

=head2 parse_genbank_locus

Returns start and end coordinate lists.

=cut

sub parse_genbank_locus
{
    my ($this, $chr, $locus) = @_;
    die("Chromosome ($chr) does not exist!\n") unless exists($this->[LEN]->{$chr});
    # Parse locus string
    if ($locus =~ /^complement\((.+)\)$/) { $locus = $1 }
    if ($locus =~ /^join\(\<?(.+)\>?\)$/) { $locus = $1 }
    my @exons = split(/,/, $locus);
    my @starts=();
    my @ends=();
    for (my $i = 0 ; $i <= $#exons ; $i++) {
        if ($exons[$i] =~ /^(\d+)\.\.(\d+)$/) {
            # -1 to convert to 0-based coordinate system
            push @starts, $1-1;
            push @ends, $2-1;
        } elsif ($exons[$i] =~ /^(\d+)$/) {
            # silly 1-base blocks are allowed
            push @starts, $1-1;
            push @ends, $1-1;
        } else {
            die("Unparseable exon: $exons[$i]");
        }
    }
    die("Locus extends beyond end of chromosome: $chr, $locus\n") if $ends[$#ends] > $this->[LEN]->{$chr};
    return (\@starts,\@ends);
}

=head2 locus_coverage_stats

Returns coverage stats for a locus/gene.  Multiple block/exon loci are supported.

=cut

sub locus_coverage_stats
{
    my ($this, $chr, $starts, $ends) = @_;
    die("Chromosome ($chr) does not exist!\n") unless exists($this->[LEN]->{$chr});
    if ($ends->[$#$ends] >= $this->[LEN]->{$chr}) {
        print "Warning: Locus ends as position ".$ends->[$#$ends].", beyond end of $chr (".$this->[LEN]->{$chr}." bp)\n";
        $ends->[$#$ends] = $this->[LEN]->{$chr} - 1;
    }
    my $tot_len = 0;
    for (my $i = 0 ; $i <= $#$starts ; $i++) {
        $tot_len += ($ends->[$i] - $starts->[$i] + 1);
    }
    my $chr_cov  = $this->[COV]->{$chr};
    my $chr_rds  = $this->[RDS]->{$chr};
    my $gene_cov = zeroes($tot_len);
    my $gene_rds = zeroes($tot_len);
    my $start    = 0;
    my $end      = 0;
    for (my $i = 0 ; $i <= $#$starts ; $i++) {
        my $ex_start = $starts->[$i];
        my $ex_end = $ends->[$i];
        my $ex_len = $ex_end - $ex_start + 1;
        $end += ($ex_len - 1);
        $gene_cov ($start : $end) .= $chr_cov ($ex_start : $ex_end);
        $gene_rds ($start : $end) .= $chr_rds ($ex_start : $ex_end);
        $start = ++$end;
    }
    my $reads  = sum($gene_rds       (0 : $tot_len - 1));
    my $median = oddmedian($gene_cov (0 : $tot_len - 1));
    my ($mean, $stddev) = stats($gene_cov (0 : $tot_len - 1));
    $mean   = int($mean * 100 + 0.5) / 100;
    $stddev = int($stddev * 100 + 0.5) / 100;
    return ($tot_len, $reads, $median, $mean, $stddev);
}

###############################################################################
# FILE I/O

=head2 write_cov_wig

Output read-coverage in UCSC genome browser's fixed-step .wig format

=cut

sub write_cov_wig
{
    my ($this, $cov_wig_outfile)=@_;
    return unless $cov_wig_outfile;
    my $out = new IO::File;
    open($out, ">$cov_wig_outfile") or die($!);
    print $out "track type=wiggle_0 name=coverage_track description=read_coverage\n";
    $this->_write_wig(COV, $out);
    close $out;
}


=head2 write_cov_wig

Output read-count in UCSC genome browser's fixed-step .wig format

=cut

sub write_rds_wig
{
    my ($this, $rds_wig_outfile)=@_;
    return unless $rds_wig_outfile;
    my $out = new IO::File;
    open($out, ">$rds_wig_outfile") or die($!);
    print $out "track type=wiggle_0 name=reads_track description=number_of_reads\n";
    $this->_write_wig(RDS, $out);
    close $out;
}

# private method
sub _write_wig
{
    my ($this, $i, $out) = @_;
    foreach my $chr (sort keys %{$this->[$i]}) {
        my $pdl  = $this->[$i]->{$chr};
        my @dims = $pdl->dims;
        my $len  = $dims[0];
        print $out "fixedStep chrom=$chr start=1 step=1 span=$len\n";
        for (my $pos = 0 ; $pos < $len ; $pos++) {
            my $x = $pdl ($pos);
            if ($x =~ /^\[(\d+)\]$/) { $x = $1 }
            print $out $x, "\n";
        }
    }
}

=head2 read_wig

Read wiggle files.  Both read-coverage and read-count Wig files are required.

=cut

sub read_wig
{
    my ($this, $cov_wig_outfile, $rds_wig_outfile) = @_;
    die("Both read-coverage and read-count Wig files are required\n") unless $cov_wig_outfile and $rds_wig_outfile;
    # READ COV FILE
    my ($chr, $len, $pos) = ('', 0, 0);
    my $cov;
    open(IN, "<$cov_wig_outfile") or die($!);
    my $header = <IN>;
    while (<IN>) {
        chomp;
        if (/^fixedStep chrom=(\S+) start=1 step=1 span=(\d+)/) {
            if ($chr and $len) {
                $this->[COV]->{$chr} = $cov;
                $this->[LEN]->{$chr} = $len;
            }
            ($chr, $len) = ($1, $2);
            $cov = zeroes($len);
            $pos = 0;
        } else {
            $cov ($pos) .= $_;
            ++$pos;
        }
    }
    close IN;
    if ($chr and $len) {
        $this->[COV]->{$chr} = $cov;
        $this->[LEN]->{$chr} = $len;
    }
    # READ RDS FILE
    ($chr, $len, $pos) = ('', 0, 0);
    my $rds;
    open(IN, "<$rds_wig_outfile") or die($!);
    $header = <IN>;
    while (<IN>) {
        chomp;
        if (/^fixedStep chrom=(\S+) start=1 step=1 span=(\d+)/) {
            if ($chr and $len) {
                die("Wig span in cov and rds files disagree for $chr!\n") unless $len == $this->[LEN]->{$chr};
                $this->[RDS]->{$chr} = $rds;
            }
            ($chr, $len) = ($1, $2);
            $rds = zeroes($len);
            $pos = 0;
        } else {
            $rds ($pos) .= $_;
            ++$pos;
        }
    }
    close IN;
    if ($chr and $len) {
        die("Wig span in cov and rds files disagree for $chr!\n") unless $len == $this->[LEN]->{$chr};
        $this->[RDS]->{$chr} = $rds;
    }
}

=head2 write_tsv

Write contig coverage statistics as tab-separated values

=cut

sub write_tsv
{
    my ($this, $filename) = @_;
    open(TSV, ">$filename") or die($!);
    print TSV "#chr\tlen\treads\tmedian\tmean\tstddev\n";
    foreach my $chr (sort keys %{$this->[LEN]}) {
        # VALIDATE INPUT
        if ($chr eq '*') {
            print "Warning; Invalid chr, '*'\n";
            next;
        }
        my $len = $this->[LEN]->{$chr};
        # CALC STATS
        my $end    = $len - 1;
        my $rds    = sum($this->[RDS]->{$chr}->(0 : $end));
        my $median = oddmedian($this->[COV]->{$chr}->(0 : $end));
        my ($mean, $stddev) = stats($this->[COV]->{$chr}->(0 : $end));
        $mean   = int($mean * 100 + 0.5) / 100;
        $stddev = int($stddev * 100 + 0.5) / 100;
        print TSV join("\t", $chr, $len, $rds, $median, $mean, $stddev), "\n";
    }
    close TSV;
}

=head2 write_gene_cov_tsv

Write gene coverage statistics as tab-separated values.  Genes must be in Gff3 format. Gff uses a 1-based coordinate system.

=cut

sub write_gene_cov_tsv
{
    my ($this, $infile, $outfile) = @_;
    open(IN,  "<$infile")  or die($!);
    open(OUT, ">$outfile") or die($!);
    print OUT "#chr\tgene\tlen\treads\tmedian\tmean\tstddev\n";

    my $prev_chr;
    my $prev_gene='';
    my @prev_starts;
    my @prev_ends;
    while (<IN>) {
        chomp;
        next if /^#/;
        my @row=split(/\t/);
        next unless @row == 9;
        my ($chr,$src,$type,$start,$end,$score,$strand,$phase,$annot)=@row;
        if ($type eq 'CDS') {
            my %annot=();
            foreach my $a (split(/;/, $annot)) { 
                my ($key,$value)=split(/=/, $a);
                $annot{$key}=$value;
            }
            my $gene = exists($annot{ID}) ? $annot{ID} : "$chr.$start"; # create ID if not defined
            if ($gene eq $prev_gene) {
                push @prev_starts, $start;
                push @prev_ends, $end;
            } else {
                if ($prev_gene) {
                    my ($len, $reads, $median, $mean, $stddev) = $this->locus_coverage_stats($prev_chr, \@prev_starts, \@prev_ends);
                    print OUT join("\t", $prev_chr, $prev_gene, $len, $reads, $median, $mean, $stddev), "\n";
                }
                $prev_chr=$chr;
                $prev_gene=$gene;
                @prev_starts=($start);
                @prev_ends=($end);
            }
        }
    }
    if ($prev_chr and $prev_gene and @prev_starts and @prev_ends) {
        my ($len, $reads, $median, $mean, $stddev) = $this->locus_coverage_stats($prev_chr, \@prev_starts, \@prev_ends);
        print OUT join("\t", $prev_chr, $prev_gene, $len, $reads, $median, $mean, $stddev), "\n";
    }
    close OUT;
}

__END__
