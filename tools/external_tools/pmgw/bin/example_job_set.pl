#!/usr/bin/env perl


use strict;


use Getopt::Long;
use Cwd qw(realpath);
use File::Basename;
use IO::File;
use Time::HiRes;

use FindBin '$RealBin';
use lib "$RealBin/../lib";

use JGI_Job_Set;
use JGI_Commands;
use JGI_Constants;
use JGI_Job_Set;
use JGI_Log;
use JGI_QC_Utility;
use JGI_Utility;


###################################################################
# Constant Definitions                                            #
###################################################################

my $SCRIPT_ID = "ill-job-ctrl.pl";
my $VERSION_ID = "Version 9/24/2010";
my $AUTHOR_ID = "Jimmy Huang";

my $ASSEMBLY = 'assembly';
my $READ = 'read';

###################################################################
# Job Control Constants                                           #
###################################################################

my $JGI_JOB_SET_DEFAULT_CLUSTER_NAME = "hyperion";
my $JGI_JOB_SET_DEFAULT_CLUSTER_QUEUE = "long.c";
my $JGI_JOB_SET_DEFAULT_MAX_SUBMITS = 5;
my $JGI_JOB_SET_DEFAULT_MAX_RETRIES = 3;
my $JGI_JOB_SET_DEFAULT_MEM_REQ = "32G";
my $JGI_JOB_SET_SGE_QMASTER_PORT_DEFAULT = 536;
my $JGI_JOB_SET_SGE_ROOT_DEFAULT = "/opt/sge";

my $previous_umask = umask(QC_UMASK);

my $job_set = JGI_Job_Set->new();
my %run_mode, my %job_parameters;

my $log = Log::Log4perl->get_logger();

my ($assembly_only, $read_only, 
    $run_type, $max_jobs, $max_retries, $cluster_name, $sge_root,
    $sge_qmaster_port, $cluster_queue, $memory_req);

unless (&GetOptions("assembly", \$assembly_only,
		    "read", \$read_only,
		    "max-jobs=i", \$max_jobs,
		    "mem=s", \$memory_req,
		    "max-retry=i", \$max_retries,
		    "name=s", \$cluster_name,
		    "port=i", \$sge_qmaster_port,
		    "queue=s", \$cluster_queue,
		    "root=s", \$sge_root,
		    "run-type=s", \$run_type)) {
    $log->error("Unable to process the command line arguments!");
    exit_failure();
}

initialize_run_mode(\%run_mode, $log);
initialize_job_parameters(\%job_parameters, $log);

my $start_time = [Time::HiRes::gettimeofday()];

my @seq_units;

retrieve_illumina_files(\@seq_units, $ARGV[0], $log);

my (@library_names, @cmds);

my ($lib_name, $run_id, $lane_id, $basecall_id, $symlink);
my $count = 0, my $read_length = 0;

# Create Directories
my $cwd;

if(safe_cwd(\$cwd, $log) != JGI_SUCCESS) {
    $log->error("Cannot identify current working directory (safe_cwd).");
    exit_failure();
}

my $outdir = $ARGV[1];

if (illumina_create_outdir($cwd, \$outdir, $log) != JGI_SUCCESS) {
    exit_failure();
}

my $cmd, my $return_value;

# validate files
foreach my $seq_unit (@seq_units) {
    chomp($seq_unit);

    $seq_unit =~ /(^\d{3,4})\.(\d{1,2})\.(\d+)/;

    $run_id = $1;
    $lane_id = $2;
    $basecall_id = $3;

    @library_names = undef;

    if (lookup_illumina_libraries($run_id, $lane_id, \@library_names, $log) != JGI_SUCCESS) {
	$log->error("Unable to retrieve the Illumina library names for $seq_unit");
	exit_failure();
    }

    $lib_name = join("_", @library_names);
    
    $read_length = read_length_from_file((ILLUMINA_FASTQ_DIR) . "/${seq_unit}.fastq.bz2");

    if ($read_length < 10) {
	$log->info("Read length of ${seq_unit} is only $read_length");
        next;
    }

    $count++;

    # get info from fetch library
    if (write_illumina_library_info($lib_name, $log) != JGI_SUCCESS) {
	$log->warn("Library info for $lib_name was not retrieved");
    } else {
	my $lib_info = "${lib_name}.fetch_library.info";

	if (run_local_cmd("mv $lib_info ${outdir}/$lib_info", \$return_value, $log) 
	    != JGI_SUCCESS) { 
	    $log->error("Cannot change the permission of $outdir");
	    exit_failure();
	}

	$log->info("Library info for $lib_name was retrieved");
    }

    # get run info from seq_file_find3.pl
    if (write_illumina_run_info($lib_name, $log) != JGI_SUCCESS) {
	$log->warn("Library run info for $lib_name was not retrieved");
    } else {
	my $run_info = "${lib_name}.library_run.info";

	if (run_local_cmd("mv $run_info ${outdir}/$run_info", \$return_value, $log) 
	    != JGI_SUCCESS) { 
	    $log->error("Cannot change the permission of $outdir");
	    exit_failure();
	}
    }

    $cmd = (ILL_SGE_QC_JOB_PL) . " -seq-unit ${seq_unit}.srf -outdir $outdir -length $read_length";

    if ($run_mode{$ASSEMBLY} == CONFIG_TRUE) {
	$cmd .= " -assembly";
    }

    if ($run_mode{$READ} == CONFIG_TRUE) {
	$cmd .= " -read";
    }

    push(@cmds, $cmd);
}

$job_parameters{ (JGI_JOB_SET_COMMANDS) } = \@cmds;

if ($job_set->set_parameter(\%job_parameters) != JGI_SUCCESS) {
    $log->error("Unable to store the job set parameters!");
    exit_failure();
}

if ($job_set->run_job_set() != JGI_SUCCESS) {
    $log->error("Unable to run Illumina QC");
    exit_failure();
}

exit_success();


###################################################################
# Function Definitions                                            #
###################################################################

sub initialize_run_mode {
    my ($ref_run_mode, $log) = @_;

    if (defined($assembly_only)) {
	$$ref_run_mode{$ASSEMBLY} = CONFIG_TRUE;
    } else {
	$$ref_run_mode{$ASSEMBLY} = CONFIG_FALSE;
    }

    if (defined($read_only)) {
	$$ref_run_mode{$READ} = CONFIG_TRUE;
    } else {
	$$ref_run_mode{$READ} = CONFIG_FALSE;
    }

    if ($$ref_run_mode{$ASSEMBLY} == CONFIG_FALSE && 
	$$ref_run_mode{$READ} == CONFIG_FALSE) {
	$$ref_run_mode{$ASSEMBLY} = CONFIG_TRUE;
	$$ref_run_mode{$READ} = CONFIG_TRUE;
    }
}


sub initialize_job_parameters {
    my ($ref_job_parameters, $log) = @_;
    
    if (!defined($run_type)) {
	$run_type = JGI_JOB_SET_RUN_TYPE_SGE;
    }

    if (!defined($cluster_name)) {
	$cluster_name = $JGI_JOB_SET_DEFAULT_CLUSTER_NAME;
    }

    if (!defined($sge_root)) {
	$sge_root = $JGI_JOB_SET_SGE_ROOT_DEFAULT;
    }

    if (!defined($sge_qmaster_port)) {
	$sge_qmaster_port = $JGI_JOB_SET_SGE_QMASTER_PORT_DEFAULT;
    }

    if (!defined($cluster_queue)) {
	$cluster_queue = $JGI_JOB_SET_DEFAULT_CLUSTER_QUEUE;
    }

    if (!defined($max_jobs)) {
	$max_jobs = $JGI_JOB_SET_DEFAULT_MAX_SUBMITS;
    }

    if (!defined($max_retries)) {
	$max_retries = $JGI_JOB_SET_DEFAULT_MAX_RETRIES;
    }

    # If h_vmem is exceeded by a job running in the queue, it is topped by a SIGKILL signal.
    if (!defined($memory_req)) {
	$memory_req = $JGI_JOB_SET_DEFAULT_MEM_REQ;
    }

    $$ref_job_parameters{ (JGI_JOB_SET_RUN_TYPE) } = $run_type;
    $$ref_job_parameters{ (JGI_JOB_SET_CLUSTER_NAME) } = $cluster_name;
    $$ref_job_parameters{ (JGI_JOB_SET_SGE_ROOT) } = $sge_root;
    $$ref_job_parameters{ (JGI_JOB_SET_SGE_QMASTER_PORT) } = $sge_qmaster_port;
    $$ref_job_parameters{ (JGI_JOB_SET_CLUSTER_QUEUE) } = $cluster_queue;
    $$ref_job_parameters{ (JGI_JOB_SET_MAX_SUBMIT) } = $max_jobs;
    $$ref_job_parameters{ (JGI_JOB_SET_MAX_RETRIES) } = $max_retries;
    $$ref_job_parameters{ (JGI_JOB_SET_MEM_LIMIT) } = $memory_req;
}


sub retrieve_illumina_files {
    my ($ref_seq_units, $seq_units_file, $log) = @_;

    if (!defined($seq_units_file)) {
	$log->error("No input file was provided");
	exit_failure();
    }

    if ( !-s $seq_units_file ) {
	$log->error("$seq_units_file does not exist");
    }

    @$ref_seq_units = `cat $seq_units_file`;
}


#
# Title    : exit_failure
# Function : This function provides a standard way of failing out of
#            the script.
# Usage    : exit_failure ( )
# Args     : None; relies on all variables defined in the main body of
#            the script being accessible.
# Returns  : Nothing; exits with JGI_FAILURE.
# Comments : None.
#
sub exit_failure
{
    #
    # Restore the old umask value.
    #
    umask($previous_umask);

    #
    # Exit with failure.
    #
    exit(JGI_FAILURE);
} # End of the exit_failure function.


#
# Title    : exit_success
# Function : This function provides a standard way of exiting the
#            script with success.
# Usage    : exit_success ( )
# Args     : None; relies on all variables defined in the main body of
#            the script being accessible.
# Returns  : Nothing; exits with JGI_SUCCESS.
# Comments : None.
#
sub exit_success
{
    #
    # Restore the old umask value.
    #
    umask($previous_umask);

    #
    # Record some useful timing information.
    #
    my $total_time = Time::HiRes::tv_interval($start_time);
    $log->info("Success. ($total_time seconds)");

    #
    # Exit with success.
    #
    exit(JGI_SUCCESS);
} # End of the exit_success function.
