#!/usr/bin/env perl

use strict;
if (@ARGV !=3) {
    print STDERR "Usage: $0 <fasta> <number to sample> <output file>\n";
    exit;
}

my $fasta = $ARGV[0];
my $num = $ARGV[1];
my $out = $ARGV[2];

my $count = `grep -c ">" $fasta`;
chomp $count;

my $factor = $num /$count;
if ($factor > 1){
    die "Can't sample $num from $count select different number\n";
}

my (@temp,$id,$found);
my ($one,$two);
open (IN,"/home/bfoster/scripts/fasta2seq.pl $fasta|") or die "can't open $fasta\n";
open (OUT,"|/home/bfoster/scripts/seq2fasta.pl > $out") or die "can't open $out\n";
while(<IN>){
    print OUT $_ if (rand()< $factor);
}
close(IN);
close(OUT);
