#!/usr/bin/env perl


use strict;

use File::Basename;
use File::Path;
use Cwd;
use Cwd 'abs_path';
use FindBin qw($RealBin);
use lib "$RealBin/../lib";
use FIN_Commands;
use FIN_Constants;
use PGF::Utilities::Properties;

# Set library path environment variable to get MEGAN and R working properly.
#
setEnvs();

my $programExecution = abs_path(dirname($0))."/".basename($0)." @ARGV";
print "DATE: ",getCurrentDateAndTime(),"\n";
print "$programExecution\n\n";

# 17aug2010 klabutti modified system calls to source settings file so user doesnt have to prior to running this script
#

unless (@ARGV==4){
    die "\nUsage: $0 <blastx results file> <read input fasta> <output rma file name> <lib>\n\n" . 
	"Example: $0 4088596_GXTG_reads.v.nr.blastx.e3 GXTG.lucy.nr.fa 4088596_GXTG_reads.rma  GXTG\n\n\n";
}

my $blastx = $ARGV[0];
my $readsfa = $ARGV[1];
my $rma = $ARGV[2];
my $lib = $ARGV[3];

if (! -e $blastx || ! -e $readsfa){
    die "$blastx or $readsfa do not exist\n";
}

my $meganexe = (SOURCE_SETTINGS) . ";" . MEGAN;
my $rexe = R_EXE;
my $gcexe = "$RealBin/gc.pl";

my $DEBUG = 0;
my $cmd;
# make rma file

&make_rma($blastx,$readsfa,$rma,$meganexe,1);

my $meganout = $rma;
$meganout=~s/\.rma//;
$meganout = $meganout . '.megan.out';

&make_meganout($rma,$meganout,$meganexe,1);
my %class = &get_classhash($meganout);

#get top categories
#get totalcount
my $totalclasshits;
foreach my $key (keys %class){
    $totalclasshits += $class{$key}{count};
}
my %topCategories;
my $runningcount = 0;  ###$class{'Nohits'}{count} + $class{'Notassigned'}{count};
my @categoriesByCount = sort {$class{$b}{count}<=>$class{$a}{count}} keys %class;
foreach my $key (@categoriesByCount){
    next if ($key eq 'Nohits' || $key eq 'Notassigned');
    $topCategories{$key}= $class{$key};
    $runningcount +=$class{$key}{count};
    last if (($runningcount / $totalclasshits) > 0.99);
}

$topCategories{'Nohits'} = $class{'Nohits'};
$topCategories{'Notassigned'} = $class{'Notassigned'};
&dump_topCategories($rma,\%topCategories,$meganexe,1);

my $gcgraphic = dirname($rma) . "/$lib.megan.jpg";
&createGChistogram(dirname($rma),$gcgraphic,$readsfa,$lib,$rexe,$gcexe,1);

#my $histgraphic = dirname($rma) . "/$rma";
my $histgraphic = $rma;
$histgraphic =~ s/\.rma//;
$histgraphic .= ".taxa.jpg";
&createTaxhistogram(dirname($rma),$histgraphic,\%class,$lib,$rexe,1);

#my $classTree =  dirname($rma) . "/$rma";
my $classTree =  $rma;
$classTree =~ s/\.rma//;
$classTree .= ".phylo.jpg";
&createTaxtree(dirname($rma),$rma,$classTree,"Class",3,$meganexe,1);

#my $kingdomTree =  dirname($rma) . "/$rma";
my $kingdomTree =  $rma;
$kingdomTree =~ s/\.rma//;
$kingdomTree .= ".phylo2.jpg";

&createTaxtree(dirname($rma),$rma,$kingdomTree,"Kingdom",5,$meganexe,1);

print "\nDATE: ",getCurrentDateAndTime(),"\n";

exit;

sub setEnvs {
    $ENV{LD_LIBRARY_PATH} = '' if !defined $ENV{LD_LIBRARY_PATH};
    $ENV{LD_LIBRARY_PATH} =
        "/global/dna/projectdirs/PI/rqc/prod/tools/misc_libraries/pixman/0.21.4/lib:".
	"/global/dna/projectdirs/PI/rqc/prod/tools/misc_libraries/cairo/1.8.2/lib:".
	"/global/dna/projectdirs/PI/rqc/prod/tools/misc_libraries/pango/1.14.9/lib:".
	"/usr/common/usg/languages/gcc/4.6.3_1/lib64:".
	"/usr/lib:".
	$ENV{LD_LIBRARY_PATH};
}

sub createTaxtree{
    my $_dir = $_[0];
    my $_rma = $_[1];
    my $_jpg = $_[2];
    my $_taxa = $_[3];
    my $_verticleScale = $_[4];
    my $_megan = $_[5];
    my $_run = $_[6];

    my $execute = "\"open meganfile=$_rma;collapse level=\'$_taxa\';update;";
    foreach (1..$_verticleScale){
	$execute .= "expand direction=vertical;update;";
    }
    $execute .= "nodelabels names=true ids=false assigned=true summarized=false;update;exportgraphics format=JPG replace=true  title=\'Taxonomy tree by $_taxa\' file=$_jpg;quit;\"";
    my $cmd = "$_megan \+g false -x $execute";
    print "$cmd\n\n";
    system ( (SOURCE_SETTINGS) . "; $cmd") if $_run;
}


sub createTaxhistogram{
    my $_dir = $_[0];
    my $_jpg = $_[1];
    my $_classhash = $_[2];
    my $_lib = $_[3];
    my $_rexe = $_[4];
    my $_run = $_[5];

    my $count = scalar(keys %{$_classhash});
    my @keys;
    if ($count > 50){
	@keys =  (sort {$class{$b}{count}<=>$class{$a}{count}} keys %class)[0..49];
    }
    else{
	@keys =  (sort {$class{$b}{count}<=>$class{$a}{count}} keys %class);
    }
    
    my $rtempin = $_dir . "/" . time() . "rtempin";
    my $rtemp = $_dir . "/" . time() . "rtemp";

    open (OUT, ">$rtempin") or die "Can't open $rtempin\n";
    print OUT "tax\tcount\n";
    foreach my $key (@keys){
	print OUT "\"$key\"\t$class{$key}{count}\n";
    }
    close(OUT);

    open(OUT, ">$rtemp") or die "can't open $rtemp\n";
    print OUT "jpeg(\"${_jpg}\", quality=100, height=480, width=640)\n";
    print OUT "taxa = read.table(\"$rtempin\",header=TRUE)\n";
    print OUT "par(mar=c((max(nchar(as.character(taxa[,1])))/2)+1,4,4,2))\n";       
    print OUT "barplot(taxa[,2],names.arg=taxa[,1],col=2,las=2,main=\"50 Taxa Most Frequently Identified for $_lib\")\n";
    print OUT "dev.off()\n";
    close(OUT);

    $cmd = "$rexe --no-save < $rtemp";
    print "$cmd\n";
    system( (SOURCE_SETTINGS) . "; $cmd") if $_run;
    
    unlink($rtempin)unless $DEBUG;
    unlink($rtemp) unless $DEBUG;
    
}

sub createGChistogram{
    my $_dir = $_[0];
    my $_jpg = $_[1];
    my $_readsfa = $_[2];
    my $_lib = $_[3];
    my $_rexe = $_[4];
    my $_gcexe = $_[5];
    my $_run = $_[6];


    # get gc files and readcount
    my %files;
    my $cmd;
    my ($file,$readsfile);
    opendir (DIR , $_dir) or die "Can't open $_dir\n";
    while($file=readdir(DIR)){
	next unless ($file=~/^reads\-.*.fasta$/ || $file=~/^$_readsfa$/);
	if ($file=~/^reads\-(.*).fasta$/){
	    $readsfile = $1;
	}
	else{
	    $readsfile = "AllReads";
	}
	$cmd = "cat $_dir/$file | $_gcexe -v > $_dir/$file.GC;";
	$cmd .= "grep -c \">\" $_dir/$file > $_dir/$file.count"; 
	print "$cmd\n";
	system ( (SOURCE_SETTINGS) . "; $cmd") if $_run;
	$files{$readsfile}{count} = `cat $_dir/$file.count`;
	chomp $files{$readsfile}{count};
	$files{$readsfile}{filename} = $_dir . "/$file";
	$files{$readsfile}{gcfilename} = $_dir . "/$file.GC";
    }	
    close(DIR);
    # Build R file
    my $rtemp = $_dir . "/" . time() . "rtemp";
    open(OUT, ">$rtemp") or die "can't open $rtemp\n";
    print OUT "jpeg(\"$_dir/${lib}.megan.jpg\", quality=100, height=480, width=640)\n";

    #reading in gcfiles
    foreach my $readsfile (sort {$files{$b}{count}<=>$files{$a}{count}} keys %files){
	print OUT "$readsfile<-read.table(\"$files{$readsfile}{gcfilename}\", header=TRUE);\n";	
    }
    
    #outputting ranges
    my $rangeString = "";
    foreach my $readsfile (sort {$files{$b}{count}<=>$files{$a}{count}} keys %files){
	print OUT "range${readsfile}X = range(round(min(${readsfile}[,5]) - 5, -1), round(max(${readsfile}[,5])+5, -1))\n";
	$rangeString .= "range${readsfile}X,";
    }
    $rangeString =~ s/\,$//;
    print OUT "minX = min($rangeString)\n";
    print OUT "maxX = max($rangeString)\n";
    print OUT "rangeX = range(minX,maxX)\n";
    print OUT "breakpoints = seq(minX, maxX, by=0.5)\n";
    
    #building histograms
    my $histString = "";
    foreach my $readsfile (sort {$files{$b}{count}<=>$files{$a}{count}} keys %files){
	print OUT "h${readsfile} = hist(${readsfile}[,5], breaks = breakpoints, plot=FALSE)\$counts\n";
	$histString .= "h${readsfile},";
    }
    $histString =~ s/\,$//;
    
    print OUT  "maxY = max(${histString})\n";
    print OUT  "rangeY = range(0, maxY)\n";
    print OUT  "par(mar=c(5,4,4,4))\n";
    
    #Adding color
    my $colorString = "";
    my $color = 0;
    foreach my $readsfile (sort {$files{$b}{count}<=>$files{$a}{count}} keys %files){
	$color++;
	if ($color == 1){
	    print OUT  "hist(${readsfile}[,5], breaks = breakpoints, col=8, border=1, xlab=\"\", ylab=\"\", main=\"GC Histogram for ${lib}\", xlim=rangeX, ylim=rangeY)\n";
	}
	else{
	    print OUT "par(new=T)\n";
	    print OUT "hist(${readsfile}[,5], breaks = breakpoints, col=${color}, border=1, xlab=\"\", ylab=\"\", main=\"\", xlim=rangeX, ylim=rangeY)\n";
	}
	$colorString .= "$color,";
    }
    $colorString =~ s/\,$//;
    
    print OUT  "axis(1, seq(minX, maxX, by=5))\n";
    print OUT  "mtext(\"GC(%)\",side=1,line=2, col=1)\n";
    print OUT  "axis(2)\n";
    print OUT  "mtext(\"# of Reads\",side=2,line=2, col=1)\n";
    print OUT  "box()\n";
    
    my $keyString = "\"" . join("\",\"",sort {$files{$b}{count}<=>$files{$a}{count}} keys %files) . "\"";
    print OUT  "legend(minX, maxY ,c(${keyString}), pch=20, col=c(${colorString}), cex=.85)\n";
    print OUT  "par(new=F)\n";
    print OUT  "dev.off()\n";
    close(OUT);

    $cmd = "$rexe --no-save < $rtemp";
    print "$cmd\n";
    system( (SOURCE_SETTINGS) . "; $cmd") if $_run;
    unlink($rtemp) unless $DEBUG; # FIXME
}



#system("/jgi/tools/bin/R --no-save < rTemp")


sub dump_topCategories{
    my $_rma = $_[0];
    my $_categories = $_[1];
    my $_megan = $_[2];
    my $_run = $_[3];

    
    my $_rmadir = dirname($_rma);
    my $execute;
    my $taxa;
    $execute = "\"open meganfile=$_rma;";
    foreach  my $cat (keys %{$_categories}){
	$taxa = $_categories->{$cat}{taxa};
	$execute  .= "extract outdir=$_rmadir outfile=\'reads-$cat.fasta\' summarized=true taxa=\'$taxa\';";
    }
    $execute .= "quit;\"";
    my $cmd = "$_megan \+g false -x $execute";
    print "$cmd\n\n";
    system ( (SOURCE_SETTINGS) . "; $cmd") if $_run;

}


sub make_rma{
    my $_blastx = $_[0];
    my $_readsfa = $_[1];
    my $_rma = $_[2];
    my $_megan = $_[3];
    my $_run = $_[4];


    my $execute = "\"import blastfile=$_blastx readfile=$_readsfa meganfile=$_rma maxmatches=100 minscore=35.0 toppercent=10.0 winscore=0.0 minsupport=5 summaryonly=false usecompression=true usecogs=true usegos=true useseed=false;quit;\"";
    my $cmd = "$_megan \+g false -x $execute";
    print "$cmd\n\n";
    system ( (SOURCE_SETTINGS) . "; $cmd") if $_run;
    
}


sub make_meganout{
    my $_rma = $_[0];
    my $_meganout = $_[1];
    my $_megan = $_[2];
    my $_run = $_[3];

    my $execute = "\"open meganfile=$_rma;collapse level='Class';update;select leaves;update;list summary=selected;quit;\"";
    my $cmd = "$_megan \+g false -x $execute > $_meganout";
    print "$cmd\n\n";
    system ( (SOURCE_SETTINGS) . "; $cmd") if $_run;
}

sub get_classhash{
    my $_meganout = $_[0];

    my %tax;
    open (IN, $_meganout) or die "can't open $_meganout\n";
    my $parse = 0;
    my $tmp;
    my ($classname,$taxa,$count);
    while(<IN>){
	if (($_=~/^\+\+\+\+\+\+\+\+/) && ($parse == 0)){
	    $parse = 1;
	    while(($tmp = <IN>)!~/^Taxonomy/){};
	    $_ = <IN>;
	}
	elsif (($_=~/^\+\+\+\+\+\+\+\+/) && ($parse == 1)){
	    $parse = 0;
	}
	
	if($parse){
	    $_=~m/^\s*(.*)\:\s(\d+).*$/;
	    $taxa=$1;
	    $count=$2;
	    $classname=$taxa;
	    $classname=~s/\(//g;
	    $classname=~s/\)//g;
	    $classname=~s/\s//g;
	    $tax{$classname}{count}=$count;
	    $tax{$classname}{taxa} = $taxa;
	}
    }
    close(IN);
    return %tax;
}

sub getCurrentDateAndTime {

# Returns current date and time in format:
# 'MM-DD-YYYY HH24:MI:SS'

    my ($sec,$min,$hour,$day,$mon,$year) = localtime(time);

    $mon++;
    $year+=1900;

    my $time = sprintf( "%02d/%02d/%04d %02d:%02d:%02d", 
        $mon,$day,$year,$hour,$min,$sec );

    return $time;

}
    
