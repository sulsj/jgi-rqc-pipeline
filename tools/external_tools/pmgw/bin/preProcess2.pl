#!/usr/bin/env perl
use strict;

use Getopt::Long;
use Pod::Usage;
use Carp;
use Cwd;
use Cwd 'abs_path';
use File::Basename;
use File::Path;
use FindBin qw($RealBin);
use lib "$RealBin/../lib";
use PGF::Utilities::Logger;
use PGF::Utilities::Properties;
use FIN_Commands;
use FIN_Constants;
use vars qw( $optHelp $optConfigFile $optLogFile);

my $programExecution = abs_path(dirname($0))."/".basename($0)." @ARGV";

if( !GetOptions(
                "h"   =>\$optHelp,
                "c=s" =>\$optConfigFile,
                "log=s"=>\$optLogFile,
                )
    ) {
    printhelp(1);
}
 
printhelp(2) if $optHelp;

if (@ARGV < 4) {
    print "USAGE $0 newblerBin libraryCode dataType(std|pe) inputdata....\n";
    exit;
}

#NEWBLER DEFINITIONS
#
my $newblerBin  = shift @ARGV;
my $libraryCode = shift @ARGV;
my $dataType    = shift @ARGV;
my @inputData   = @ARGV;
my $runAssembly = "$newblerBin/runAssembly"; 
my $sfffile     = "$newblerBin/sfffile"; 

my $configFile = $optConfigFile ?  $optConfigFile :
    "$RealBin/../config/metagenomeQc.config";
my $OBJ_PROPS = PGF::Utilities::Properties->new(-configFile=>$configFile);
   $OBJ_PROPS->setExceptionIfEntryNotFound(1); # confess if entry in config file
                                               # is not found.
                                               
my $peSwitch = ($dataType) =~ /pe/ ? 1 : undef;
my $data = join "\n", @inputData;print "\n";

my $OBJ_LOGGER = PGF::Utilities::Logger->new();
my $logfile = $optLogFile ? $optLogFile : basename($0).".log.".$$;
setFileForLogging($logfile);
logExecution($programExecution);

logOutput("library: $libraryCode", 1);
logOutput("dataType: $dataType", 1);
logOutput("data: $data", 1);

preprocess($libraryCode, $peSwitch, @inputData);

logOutput("DONE", 1);

exit;

sub preprocess {
    my ($libraryCode, $peSwitch, @input) = @_;

    my $newblerOutputTrimDir  = "${libraryCode}_trim";
    my $outputFnaLink         = "$newblerOutputTrimDir/454TrimmedReads.fna";
    my $outputQualLink        = "$newblerOutputTrimDir/454TrimmedReads.qual";
    my $extractedOutputDir        = "$newblerOutputTrimDir/${libraryCode}.nr.97.0.20.fa";
    my $extractedOutput           = "$extractedOutputDir/extracted_clusters_unique.fa";
    my $outputUniqueReadsList     = "$newblerOutputTrimDir/${libraryCode}.nr.fna.list";    
    my $outputTrimmedReadsList    = "$newblerOutputTrimDir/${libraryCode}.TrimmedReads.list";
    my $outputRedundantReadsList  = "$newblerOutputTrimDir/${libraryCode}.RedundantReads.list";
    my $outputTrimUniqueReadsFna  = "$newblerOutputTrimDir/${libraryCode}.454TrimmedReads.nr.fna";
    my $outputTrimUniqueReadsQual = "$newblerOutputTrimDir/${libraryCode}.454TrimmedReads.nr.fna.qual";
    my $lucyInputName  = "$newblerOutputTrimDir/${libraryCode}.nr.lucy";
    
    logOutput("PREPROCESSING...", 1);

    # run newbler to trim reads
    my $contamFile = $peSwitch ? "$RealBin/../data/pe454Contam.fa" :
        "$RealBin/../data/454Contam.fa";
    my $params = $OBJ_PROPS->getProperty("newblerTrimParams");
    my $cmd = (RUN_ASSEMBLY) . " $params -o $newblerOutputTrimDir " .
        "-vt $contamFile -vs $contamFile ";
    $cmd = $cmd . join " ", @input;
    $cmd = $cmd . " > newbler.out.$libraryCode";

    doThis($cmd,"${newblerOutputTrimDir}/454TrimmedReads.fna");
    logOutputFileCreation("${newblerOutputTrimDir}/454TrimmedReads.qual");

    # extract redundant data - Per Stephanie's request, changed the % identity to
    # 97% and the sequence length to 20.
    #
    $cmd = (BINDIR) . (EXTRACT_REPLICATES) . " $newblerOutputTrimDir/454TrimmedReads.fna 0.97 0 20 $extractedOutputDir";
    doThis($cmd,"$extractedOutput");

    # create redundant list
    $cmd = (GREP_CMD) . " '>' $extractedOutput | ".
        "sed 's/>//g' | ".
        "sed 's/_left//g' | ".
        "sed 's/_right//g'| ".
        "awk '{print \$1}' > $outputUniqueReadsList";
    doThis($cmd, $outputUniqueReadsList) ;

    if ( $peSwitch ) {
        $cmd = (GREP_CMD) . " '>' $outputFnaLink | ".
            "sed 's/>//g' | ".
            "sed 's/_left//g' | ".
            "sed 's/_right//g'| ".
            "awk '{print \$1}'  > $outputTrimmedReadsList";
    } else {
        $cmd = (GREP_CMD) . " '>' $outputFnaLink | ".
            "sed 's/>//g' | ".
            "awk '{print \$1}' > $outputTrimmedReadsList";
    }
    doThis($cmd, $outputTrimmedReadsList) ;

    $cmd = (BINDIR) . (SUBSTRACT) . " $outputTrimmedReadsList ".
        "$outputUniqueReadsList >  $outputRedundantReadsList";
    doThis($cmd, $outputRedundantReadsList);

    # lucy trim
    if ( $peSwitch ) {
	my $lucyOutput = "$newblerOutputTrimDir/${libraryCode}.clean.sff";
	$cmd = (SFFFILE) . " -nmft -o $lucyOutput -i $outputUniqueReadsList  ";
	$cmd = $cmd . join " ", @input;
	doThis($cmd,$lucyOutput) ;
        my $lucyOutputBasename = basename($lucyOutput);
	$cmd = (LN_CMD) . " $lucyOutput ".
            "$newblerOutputTrimDir/sff/$lucyOutputBasename";
      	doThis($cmd);
    } else {
        my $lucyOutput     = "${lucyInputName}.99.3.fasta";
	$cmd = (BINDIR) . (FA_GET_LIST) . " $outputFnaLink ".
            "$outputUniqueReadsList $outputTrimUniqueReadsFna";
	doThis($cmd, $outputTrimUniqueReadsFna) ;
	
	$cmd = (BINDIR) . (FA_GET_LIST) . " $outputQualLink ".
            "$outputUniqueReadsList $outputTrimUniqueReadsQual";
	doThis($cmd, $outputTrimUniqueReadsFna) ;
		
	$cmd = (BINDIR) . (LUCY_TRIM) . " 99.3 $lucyInputName $outputTrimUniqueReadsFna";
	doThis($cmd,"$lucyOutput") ;
    }
    
}

###############################################################################
sub printMsg {
    my ($msg) = @_;
    my $date = setDate();
    print "#$date\n$msg\n\n";
}


###############################################################################
sub doThis {
    my ($cmd, $expectedOutput) = @_;

    logOutput($cmd, 1);
    system($cmd);
    logOutputFileCreation($expectedOutput) if defined $expectedOutput;

}

###############################################################################
sub checkOutput {
    my ($file) = @_;

    unless (-s $file) {
	print "#ERROR: file ($file) does not exist of is empty.\n";
	exit;
    }
}

###############################################################################
sub setDate {
    my $date = `date`;
    chomp $date;
    return $date;
    
}

###############################################################################
sub logOutputFileCreation {
    
    my $outputFile = shift;
    
    $outputFile = abs_path($outputFile);
    
    my $fileExists = -s $outputFile ? 1:0;
    my $msg = $fileExists ? "Created $outputFile" :
        "ERROR: failed to create $outputFile";
    logOutput($msg,1);
    
    return $fileExists;
    
}

################################################################################
sub setFileForLogging {
    
    my $logFile = shift;
    
    $OBJ_LOGGER->setLogOutFileAppend($logFile);
    $OBJ_LOGGER->setLogErrorFileAppend($logFile);
    
}

###############################################################################
sub logExecution {
    
    my $programExecution = shift;
    
    my $msg = "Command: ".$programExecution."\n".
              "Current directory: ".getcwd;
    logOutput($msg);
}

###############################################################################
sub logError {
    my $message = shift;
    my $confess = shift || 0;
    
    $OBJ_LOGGER->logError($message);
    
    confess if $confess;
}
    
###############################################################################
sub logOutput {
    my $message = shift;
    my $printMsg = shift || 0;
    
    $OBJ_LOGGER->logOut($message);
    print "$message\n" if $printMsg;
}
    
###############################################################################
sub printhelp {
    my $verbose = shift || 1;
    pod2usage(-verbose=>$verbose);
    exit 1;
}

###############################################################################
