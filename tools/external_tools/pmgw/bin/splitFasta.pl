#!/usr/bin/env perl

use strict;

use FindBin '$RealBin';
use lib "$RealBin/../lib";

use genMod;
use parseMod;
	
my $usage = "
usage: splitFasta.pl <infile> <maxSize/maxSeq> <outPrefix> [-s] [-f]
	[-C <outDir>] [-fp] [-m <size>]

without -s, the second argument will be interpreted as a maximum size
per file(in bp). Using -s indicates that the second argument will be the 
maximum number of sequences per file

-f => create file flist.<outPrefix> that is a list of the files generated
-C <outDir>: put output files in <outDir>
-fp => create file flist.<outPrefix>.fullPath that is a full-path list
	of all files create
-m <size> => skip sequences less than this size, too small for this ride
-di => generate seq filename -> defline index
";

my ($opts, $nonOpt) = parseMod_getArgs("s|f|C:|fp|m:|di", @ARGV, 3, $usage);

my ($infile, $maxSizeSeq, $outprefix) = @$nonOpt;
my $outDir = genMod_setVal(".", $opts->{C});
my $i = 0;
my $fi = sprintf("%06d", $i);
# remove any old stuff
system("rm -f $outDir/$outprefix.*");
#open input file
genMod_openFile(*INF, $infile) or die;
#open first output file
genMod_openFile(*OUTF, "> $outDir/$outprefix.$fi") or die;
#if we are creating an flist, open the file and output first filename
if(defined($opts->{f}))
	{
	genMod_openFile(*OUTF_FLIST, "> $outDir/flist.$outprefix") or die;
	print OUTF_FLIST "$outprefix.$fi\n";
	}
if(defined($opts->{fp}))
	{
	genMod_openFile(*OUTF_FLIST_FP, "> $outDir/flist.$outprefix.fullPath") or die;
	print OUTF_FLIST_FP genMod_getFullPath("$outDir/$outprefix.$fi") . "\n";
	}
if(defined($opts->{di}))
	{
	genMod_openFile(*OUTF_FLIST_DI, "> $outDir/defindex.$outprefix") or die;
	}

my $outSizeSeq = 0;
my $inseqcnt = 0;
my $outseqcnt = 0;
my $seqName;
my $seq;

while (<INF>)
{
    chomp;
    next if /^\s*$/;
    if (/\s*^>/o)
    {
	++$inseqcnt;
	processSeq() if (defined $seq);
	$seqName = $_;
	$seq = "";
    }
    else
    {
	$seq .= $_;
    }
}
processSeq() if (defined ($seqName));
close(OUTF_FLIST_DI) if(defined($opts->{di}));

print "INFO: processed $inseqcnt input sequences and generated $outseqcnt output sequences.\n";
exit(0);

sub processSeq
{
    return if (defined ($opts->{m}) && length ($seq) < $opts->{m});
    if($outSizeSeq >= $maxSizeSeq)
    {
	$outSizeSeq = 0;
	close(OUTF);
	$i++;
	$fi = sprintf("%06d", $i);
	genMod_openFile(*OUTF, "> $outDir/$outprefix.$fi") or die;
	print OUTF_FLIST "$outprefix.$fi\n" if (defined ($opts->{f}));
	print OUTF_FLIST_FP genMod_getFullPath("$outDir/$outprefix.$fi") . "\n" if (defined ($opts->{fp}));
    }
    if (defined($opts->{s})) {
	$outSizeSeq++;
    } else {
	$outSizeSeq += length ($seq);
    }
    ++$outseqcnt;
    if(defined($opts->{di})) {
	print OUTF_FLIST_DI "$outprefix.$fi " . length($seq) . " $seqName\n";
    }
    pr ($seqName);
    $seq =~ s/(.{100})/$1\n/g;
    chop $seq if ($seq =~ /\n$/);
    pr ($seq);
}

sub pr
{
    my $line = shift;
    print OUTF "$line\n"
	or die "FATAL ERROR: unable to write output to file $outDir/$outprefix.$fi\n";
}

# FRK - this is the old code that has been updated to skip seqs of a min size
#my $line;
#my $outSizeSeq = 0;
#while(defined($line = <INF>))
#	{
#	chomp $line;
#	next if $line =~ /^\s*$/;
#	if($line =~ /^\s*>/o)
#		{
#		if($outSizeSeq >= $maxSizeSeq)
#			{
#			$outSizeSeq = 0;
#			close(OUTF)	if($i > 0);
#			$i++;
#			$fi = sprintf("%06d", $i);
#			genMod_openFile(*OUTF, "> $outDir/$outprefix.$fi") or die;
#			print OUTF_FLIST "$outprefix.$fi\n"
#				if defined ${%$opts}{f};
#			print OUTF_FLIST_FP genMod_getFullPath("$outDir/$outprefix.$fi") . "\n"
#				if defined ${%$opts}{fp};
#			}
#		$outSizeSeq++ if defined(${%$opts}{s});
#		}
#	else
#		{
#		#if we are splitting on size, update our count
#		$outSizeSeq += length($line)
#			unless defined(${%$opts}{s});
#		}
#	print OUTF "$line\n"
#		or die "FATAL ERROR: unable to write output to file $outDir/$outprefix.$fi\n";
#	}
##close last file
#close(OUTF);
#close(INF);
#close(OUTF_FLIST) if defined ${%$opts}{f};
#close(OUTF_FLIST_FP) if defined ${%$opts}{fp};
#
#exit(0);
