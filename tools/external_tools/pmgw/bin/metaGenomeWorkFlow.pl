#!/usr/bin/env perl

# workflow for processing metagenomic data

use strict;
use Carp;

use Cwd;
use Cwd 'abs_path';
use File::Basename;
use File::Path;
use Getopt::Long;
use FindBin qw($RealBin);
use lib "$RealBin/../lib";
use PGF::Utilities::JobManager;
use PGF::Utilities::Properties;
use FIN_Commands;
use FIN_Constants;
use PGF::Utilities::Logger;

use vars qw($optHelp $optDebug $optNumParallel $optWorkingDir $optNewblerDir
    $optConfigFile $optLogFile);

# input val
#
if( !GetOptions(
                "h"     =>\$optHelp,
                "debug" =>\$optDebug,
                "od=s"  =>\$optWorkingDir,
                "nd=s"  => \$optNewblerDir, 
                "n=i"   =>\$optNumParallel,
                "c=s"   =>\$optConfigFile,
                "log=s" =>\$optLogFile,
                ) ) { 
    help();
} 

unless (@ARGV == 2) {
    help();
}
unless ($optNumParallel) { 
    $optNumParallel = 5;
}

my ($projectId, $inputFile) = @ARGV;

if ( !-e $inputFile ) {
    print "Cannot find your input file $inputFile.\n";
    exit 1;
}

$optWorkingDir =~ s/\/$// if $optWorkingDir;

my $configFile = $optConfigFile ?  $optConfigFile :
    "$RealBin/../config/metagenomeQc.config";
my $OBJ_PROPS = PGF::Utilities::Properties->new(-configFile=>$configFile);
   $OBJ_PROPS->setExceptionIfEntryNotFound(1); # confess if entry in config file
                                               # is not found.

my ($unpairedLibraryDataHashRef, $pairedLibraryDataHashRef) = parseInputFile(
    $inputFile);

my $unpairedLibraryList = join ".", keys %$unpairedLibraryDataHashRef;
my $libraryList = length $unpairedLibraryList ? "$unpairedLibraryList" : "";
if ( %$pairedLibraryDataHashRef ) {
   $libraryList .= "." if length $unpairedLibraryList;
   $libraryList .= join ".", keys %$pairedLibraryDataHashRef
}

my $workingDir = $optWorkingDir ? $optWorkingDir :
    getcwd . "/" . $projectId . '_' . $libraryList . '_' . $$;

mkdir $workingDir if !-e $workingDir;
chdir $workingDir;

my $logfile = $optLogFile ? $optLogFile : "$workingDir/".basename($0).".log";
my $logDir = dirname($logfile);
my $programExecution = abs_path(dirname($0))."/".basename($0)." @ARGV";
my $OBJ_LOGGER = PGF::Utilities::Logger->new();

setFileForLogging($logfile);
logExecution($programExecution);

logOutput("Analysis Dir: $workingDir ...", 1);

# Perform Newbler and lucy trimming of the reads.
#
preProcess($logDir, $configFile, $optNumParallel, $unpairedLibraryDataHashRef,
    $pairedLibraryDataHashRef) if runThisComponent("metaGenomeWorkFlow.execute.readTrimming");

my $newblerOutputDir = $optNewblerDir ? $optNewblerDir :
    "$workingDir/${libraryList}_mi98ml80";
my $lucyTrimFasta = "${unpairedLibraryList}_trim/".
    "${unpairedLibraryList}.nr.lucy.99.3.fasta";
    
# Assemble trimmed reads.
#
assemble($newblerOutputDir, $lucyTrimFasta, $libraryList,
    $pairedLibraryDataHashRef) if runThisComponent("metaGenomeWorkFlow.execute.assembly");

# Generate analysis data.
#
postProcess($newblerOutputDir, $lucyTrimFasta, $libraryList, $projectId,
    $optNumParallel) if runThisComponent("metaGenomeWorkFlow.execute.analysis");

# Blast assembled reads against db.
#
runBlasts($newblerOutputDir,$libraryList, $projectId, $lucyTrimFasta,
    $optNumParallel) if runThisComponent("metaGenomeWorkFlow.execute.blastn");

logOutput("DONE", 1);

exit 0;

################################## subroutines ################################
sub parseInputFile {
    my $file = shift;
    
    my %unpairedLibraryData = ();
    my %pairedLibraryData = ();
    
    open IFILE, $file or die "Failed to open input file $file: $!\n";
    while (my $line = <IFILE>) {
        chomp $line;
        next if !length $line;
        next if $line =~ /^#/; # skip if comment.
        # Look for entries like 'library U|P sffFile'
        if ( $line =~ /^(\S+)\s+(U|P)\s+(\S+)/ ) {
            my ($library, $libraryType, $sffFile) = ($1, $2, $3);
            if ( $libraryType eq 'U' ) {
                push @{$unpairedLibraryData{$library}}, $sffFile;
            } elsif ( $libraryType eq 'P' ) {
                push @{$pairedLibraryData{$library}}, $sffFile;
            }
        }
    }
    close IFILE;
    
    # Check if values are parsed from input file. If no values present,
    # log and exit with error.
    #
    if ( !%unpairedLibraryData && !%pairedLibraryData ) {
        print STDERR "The input file $file is incorrectly formatted.\n";
        exit 1;
    }
    
    return \%unpairedLibraryData, \%pairedLibraryData;
    
}
    
###############################################################################
sub assemble {
    my $newblerOutputDir = shift;
    my $lucyTrimFasta = shift;
    my $libraryList = shift;
    my $pairedLibraryDataHashRef = shift;

    my $runAssembly = RUN_ASSEMBLY;

    logOutput("ASSEMBLING...($newblerOutputDir)", 1);    
    
    #run mi98ml80 assembly
    my $params = $OBJ_PROPS->getProperty("newblerAssemParams");
    my $cmd = "$runAssembly $params -o $newblerOutputDir ";
       $cmd .= "-force " if -e $newblerOutputDir;
    
    # Add paired library (trimmed) sffs if found.
    #
    foreach my $library (keys %$pairedLibraryDataHashRef) {
        my $pairedSff = "${library}_trim/${library}.clean.sff";
        $cmd .= "-p $pairedSff " if -e $pairedSff;
    }

    $cmd .= "$lucyTrimFasta " if -e $lucyTrimFasta;
    $cmd .= "> newbler.out.$libraryList";
    
    runCommand($cmd,"$newblerOutputDir/assembly/454Scaffolds.txt");
    
}

###############################################################################
sub runCommand {
    my ($cmd, $expectedOutput) = @_;

    logOutput($cmd, 1);
    system($cmd);
    logOutputFileCreation($expectedOutput) if defined $expectedOutput;

}

###############################################################################
sub runJobsInParallel {
    
    my $numParallelProcess = shift;
    my $checkExitStatus = shift;
    my @jobs = @_;
  
    foreach my $job (@jobs) {
	logOutput( "parallelJob: $job", 1 );
    }

    # Run jobs in parallel.
    #
    my $objJobManager = PGF::Utilities::JobManager->new();
   
    $objJobManager->setExitStatusChecking($checkExitStatus);
    $objJobManager->setMaxNumberOfParallelProcesses($numParallelProcess);
    $objJobManager->turnOnDebugging() if $optDebug;
    $objJobManager->parallel(@jobs);
    
}

###############################################################################
sub preProcess {
    my $logDir = shift;
    my $configFile = shift;
    my $numParallelProcess = shift;
    my $unpairedLibraryDataHashRef = shift;
    my $pairedLibraryDataHashRef = shift;
    
    my @jobs = ();
    my $unpairedLibraryList = "";
    my @unpairedLibrarySffs = ();
    my $logFile = $logDir . "/" . basename(PREPROCESS) . ".log";
    
    while ( my ($library, $sffArrayRef) = each %$unpairedLibraryDataHashRef ) {
        push @unpairedLibrarySffs, @$sffArrayRef;
        $unpairedLibraryList .= "$library.";
    }
    $unpairedLibraryList =~ s/\.$//;

    # Group all unpaired libraries together for trimming.
    #
    if ( @unpairedLibrarySffs ) {
        push @jobs, (PREPROCESS) .
            " -c $configFile".
            " -log $logFile".
            " " . (RUN_ASSEMBLY_DIR) .
            " $unpairedLibraryList std @unpairedLibrarySffs > ".
            "$unpairedLibraryList.trim.log";
    }
    
    # For each paired library, trim separately.
    #
    foreach my $library (keys %$pairedLibraryDataHashRef) {
        my @sffs = @{$$pairedLibraryDataHashRef{$library}};
        if ( @sffs ) {
            push @jobs, (PREPROCESS) .
                " -c $configFile" .
                " -log $logFile" .
                " " . (RUN_ASSEMBLY_DIR) .
                " $library pe @sffs > $library.trim.log";
        }
    }
    
    runJobsInParallel($numParallelProcess, 1, @jobs);
    
}

###############################################################################
sub postProcess {
    my $newblerOutputDir = shift;
    my $unpairedLucyTrimFasta = shift;
    my $libraryCode = shift;
    my $projectId = shift;
    my $numParallelProcess = shift;
    
    my @jobs;
    my $cmd;
    my $currentDir = getcwd;

    my $assemblyDir = "${newblerOutputDir}/assembly";
    my $depthCovDir = "${assemblyDir}/DepthCov";

    print "\n";
    logOutput("POSTPROCESSING...", 1);
    
    $cmd = (BINDIR) . (READ_COV) .
        " --ace $assemblyDir/454Contigs.ace".
        " --con $assemblyDir/454Contigs.contig.cov".
        " --cov $assemblyDir/454Contigs.contigcov.wig".
        " --rds $assemblyDir/454Contigs.readcov.wig";
    runCommand($cmd, "$assemblyDir/454Contigs.contig.cov");


    for ("Singleton", "Outlier", "Repeat") {
	$cmd = (GREP_CMD) . " '$_' $assemblyDir/454ReadStatus.txt | ".
            "awk '{print \$1}' | ".
            "sed 's/_left//g' | sed 's/_right//g' ".
            "> $assemblyDir/454_${_}Reads.list";
	runCommand($cmd, "$assemblyDir/454_${_}Reads.list");
    }

    $cmd = (CAT_CMD) .
        " $assemblyDir/454_SingletonReads.list".
        " $assemblyDir/454_OutlierReads.list".
        " $assemblyDir/454_RepeatReads.list >".
        " $assemblyDir/454_UnassembledReads.list";
    runCommand($cmd, "$assemblyDir/454_UnassembledReads.list");

  
    for ("Singleton", "Outlier", "Repeat", "Unassembled") {
	$cmd = (BINDIR) . (FA_GET_LIST) . 
	    " $assemblyDir/454TrimmedReads.fna".
            " $assemblyDir/454_${_}Reads.list".
            " $assemblyDir/${projectId}_${libraryCode}_454${_}Reads.fna";
	runCommand($cmd, "$assemblyDir/${projectId}_${libraryCode}_454${_}Reads.fna");
	$cmd = (BINDIR) . (FA_GET_LIST) . 
	    " $assemblyDir/454TrimmedReads.qual".
            " $assemblyDir/454_${_}Reads.list".
            " $assemblyDir/${projectId}_${libraryCode}_454${_}Reads.fna.qual";
	runCommand($cmd, "$assemblyDir/${projectId}_${libraryCode}_454${_}Reads.fna.qual");  
    }
    
    $cmd = (LN_CMD) . " 454AllContigs.qual $assemblyDir/454AllContigs.fna.qual";
    runCommand($cmd, "$assemblyDir/454AllContigs.fna.qual");
    
    $cmd = (BINDIR) . (LUCY_TRIM) .
        " 99.3".
        " $assemblyDir/${libraryCode}.ctgs.lucy".
        " $assemblyDir/454AllContigs.fna";
    runCommand($cmd,"$assemblyDir/${libraryCode}.ctgs.lucy.99.3.fasta");

    $cmd = (BINDIR) . (LUCY_TRIM) .
        " 99.3".
        " $assemblyDir/${libraryCode}.singlets.lucy".
        " $assemblyDir/${projectId}_${libraryCode}_454SingletonReads.fna";
    runCommand($cmd,"$assemblyDir/${libraryCode}.singlets.lucy.99.3.fasta");

    $cmd = (BINDIR) . (LUCY_TRIM) .
        " 99.3".
        " $assemblyDir/${libraryCode}.unassem.lucy".
        " $assemblyDir/${projectId}_${libraryCode}_454UnassembledReads.fna";
    runCommand($cmd,"$assemblyDir/${libraryCode}.unassem.lucy.99.3.fasta");

    $cmd = (BINDIR) . (LUCY_TRIM) .
        " 99.3".
        " $assemblyDir/${libraryCode}.repeat.lucy".
        " $assemblyDir/${projectId}_${libraryCode}_454RepeatReads.fna";
    runCommand($cmd,"$assemblyDir/${libraryCode}.repeat.lucy.99.3.fasta");

    $cmd = (BINDIR) . (LUCY_TRIM) .
        " 99.3".
        " $assemblyDir/${libraryCode}.outlier.lucy".
        " $assemblyDir/${projectId}_${libraryCode}_454OutlierReads.fna";
    runCommand($cmd,"$assemblyDir/${libraryCode}.outlier.lucy.99.3.fasta");

    ###### This part is not working, but doesn't seem like it is needed. 
    #$cmd = (LN_CMD) . " $assemblyDir/454TrimmedReads.qual ".
    #    "$assemblyDir/454TrimmedReads.fna.qual";
    #runCommand($cmd);
    #    
    #$cmd = (BINDIR) . (LUCY_TRIM) . " 99.3 ".
    #    "$assemblyDir/${libraryCode}.nr.lucy ".
    #    "$assemblyDir/454TrimmedReads.fna";
    #runCommand($cmd,"$assemblyDir/${libraryCode}.nr.lucy.99.3.fasta");
    
    $cmd = (CAT_CMD) . " $assemblyDir/${libraryCode}.ctgs.lucy.99.3.fasta | ".
        (BINDIR) . (GC) . " -v > $assemblyDir/contigs.lucy.GC";
    runCommand($cmd,"$assemblyDir/contigs.lucy.GC");

    $cmd = (CAT_CMD) . " $unpairedLucyTrimFasta | " . (BINDIR) . (GC) .
        " -v > $assemblyDir/${libraryCode}.GC";
    runCommand($cmd,"$assemblyDir/${libraryCode}.GC");

    $cmd = (CAT_CMD) . " $assemblyDir/${libraryCode}.singlets.lucy.99.3.fasta | " .
        (BINDIR) . (GC) . " -v > $assemblyDir/singlets.lucy.GC";
    runCommand($cmd,"$assemblyDir/singlets.lucy.GC");

    $cmd = (CAT_CMD) . " $assemblyDir/${libraryCode}.unassem.lucy.99.3.fasta | " .
        (BINDIR) . (GC) . " -v > $assemblyDir/unassem.lucy.GC";
    runCommand($cmd,"$assemblyDir/unassem.lucy.GC");

    $cmd = (CAT_CMD) . " $assemblyDir/${libraryCode}.repeat.lucy.99.3.fasta | " .
        (BINDIR) . (GC) . " -v > $assemblyDir/repeats.lucy.GC";
    runCommand($cmd,"$assemblyDir/repeats.lucy.GC");

    $cmd = (CAT_CMD) . " $assemblyDir/${libraryCode}.outlier.lucy.99.3.fasta | " .
        (BINDIR) . (GC) . " -v > $assemblyDir/outliers.lucy.GC";
    runCommand($cmd,"$assemblyDir/outliers.lucy.GC");


    push (@jobs, (GC_LIB_PLOT) . " $assemblyDir/contigs.lucy.GC contigs.lucy");
    push (@jobs, (GC_LIB_PLOT) . " $assemblyDir/${libraryCode}.GC $libraryCode");
    push (@jobs, (GC_LIB_PLOT) . " $assemblyDir/singlets.lucy.GC singlets.lucy");
    push (@jobs, (GC_LIB_PLOT) . " $assemblyDir/unassem.lucy.GC unassem.lucy");
    push (@jobs, (GC_LIB_PLOT) . " $assemblyDir/repeats.lucy.GC repeats.lucy");
    push (@jobs, (GC_LIB_PLOT) . " $assemblyDir/outliers.lucy.GC outliers.lucy");
    
    runJobsInParallel($numParallelProcess, 1, @jobs);
    
    logOutputFileCreation("$assemblyDir/contigs.lucy.GC.jpg");    
    logOutputFileCreation("$assemblyDir/${libraryCode}.GC.jpg");    
    logOutputFileCreation("$assemblyDir/singlets.lucy.GC.jpg"); 
    logOutputFileCreation("$assemblyDir/unassem.lucy.GC.jpg");        
    logOutputFileCreation("$assemblyDir/repeats.lucy.GC.jpg");    
    logOutputFileCreation("$assemblyDir/outliers.lucy.GC.jpg");           

    $cmd = (GREP_CMD) . " '>' $assemblyDir/${libraryCode}.ctgs.lucy.99.3.fasta |".
        " sed 's/>//g' | awk '{print \$1}'".
        " > $assemblyDir/${projectId}.454AllContigs.lucy.list";
    runCommand($cmd,"$assemblyDir/${projectId}.454AllContigs.lucy.list");

    $cmd = (BINDIR) . (SPLIT_ACE) .
        " $assemblyDir/454Contigs.ace".
        " $assemblyDir/${projectId}.454AllContigs.lucy.list";
    runCommand($cmd,"$assemblyDir/454Contigs.ace.new");

    logOutput("mkdir ${depthCovDir}", 1);
    mkdir $depthCovDir;

    $cmd = (MV_CMD) . " $assemblyDir/454Contigs.ace.new $depthCovDir/";
    runCommand($cmd,"$depthCovDir/454Contigs.ace.new");

    $cmd = (BINDIR) . (READ_COV) .
        " --ace $depthCovDir/454Contigs.ace.new".
        " --con $depthCovDir/454Contigs.ace.new.contigs.cov".
        " --cov $depthCovDir/454Contigs.ace.new.contigcov.wig". 
        " --rds $depthCovDir/454Contigs.ace.new.readcov.wig";
    runCommand($cmd, "$depthCovDir/454Contigs.ace.new.contigs.cov");

    $cmd = "cd $assemblyDir; export LD_LIBRARY_PATH=/usr/lib:\$LD_LIBRARY_PATH; " .
        (GC_COV_CONTIGS_PLOT_454) .
        " $assemblyDir/contigs.lucy.GC".
        " $depthCovDir/454Contigs.ace.new.contigs.cov".
        " contigs.lucy";
    runCommand($cmd,"$assemblyDir/contigs.GC_vs_Depth.jpg");
    
    $cmd = "cd $assemblyDir; export LD_LIBRARY_PATH=/usr/lib:\$LD_LIBRARY_PATH; " .
        (COV_CTGS_LEN_VS_DEPTH_PLOT) .
        " $assemblyDir/contigs.lucy.GC".
        " $depthCovDir/454Contigs.ace.new.contigs.cov".
        " contigs.lucy";
    runCommand($cmd,"$assemblyDir/contigs.Length_vs_Depth.jpg");
    
    $cmd = "cd $assemblyDir; export LD_LIBRARY_PATH=/usr/lib:\$LD_LIBRARY_PATH; " .
        (COV_CTGS_LEN_VS_MEAN_COV_PLOT) .
        " $assemblyDir/contigs.lucy.GC".
        " $depthCovDir/454Contigs.ace.new.contigs.cov".
        " contigs.lucy";
    runCommand($cmd,"$assemblyDir/contigs.Length_vs_MeanCov.jpg");
    
    $cmd = (CUT_CMD) . " -f 5 $assemblyDir/contigs.lucy.GC > $assemblyDir/tmp";
    runCommand($cmd,"$assemblyDir/tmp");

    $cmd = (PASTE_CMD) .
        " $depthCovDir/454Contigs.ace.new.contigs.cov".
        " $assemblyDir/tmp".
        " > $depthCovDir/454Contigs.ace.new.contigs.cov.lucy.GC";
    runCommand($cmd,"$depthCovDir/454Contigs.ace.new.contigs.cov.lucy.GC");
}

################################################################################
sub runBlasts {
    my ($newblerOutputDir, $libraryCode, $projectId, $lucyOutput,
        $numParallelProcess) = @_;
    
    my @jobs = ();
    my $assemblyDir = "${newblerOutputDir}/assembly";
    my $blastDbDir = $OBJ_PROPS->getProperty("blastDbDir");
    my $largeSubunitrRNADb = $OBJ_PROPS->getProperty("largeSubunitrRNADb");
    my $smallSubunitrRNADb = $OBJ_PROPS->getProperty("smallSubunitrRNADb");
    my $blastNT = $OBJ_PROPS->getProperty("blastNTDatabase");
    
    logOutput("BLAST STAGE!!", 1);    
    
    # blast contigs vs. Large Subunit rRNA db
    push(@jobs, (BLASTALL) .
        " -b 5 -v 5 -p blastn -a 2 -i".
        " ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta".
        " -d $largeSubunitrRNADb".
        " -e 1e-20".
        " -o ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.SilvaLSU.blastn.e20");
    
    #blast contigs vs. Small Subunit rRNA db
    push(@jobs, (BLASTALL) .
        " -b 5 -v 5 -p blastn -a 2 -i".
        " ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta".
        " -d $smallSubunitrRNADb".
        " -e 1e-20".
        " -o ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.SilvaSSU.blastn.e20");

    # add blastn for contigs vs. NR db
    push(@jobs, (BLASTALL) .
        " -b 5 -v 5 -p blastn -a 4".
        " -i ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta".
        " -d $blastNT -e 1e-20".
        " -o ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.nr.blastn.e20");

    #blast reads vs. Large Subunit rRNA db
    push(@jobs, (BLASTALL) .
        " -b 5 -v 5 -p blastn -a 2".
        " -i ${lucyOutput}".
        " -d $largeSubunitrRNADb".
        " -e 1e-20".
        " -o ${assemblyDir}/${libraryCode}.lucy.nr.fa.v.SilvaLSU.blastn.e20");

    #blast reads vs. Small Subunit rRNA db
    push(@jobs, (BLASTALL) .
        " -b 5 -v 5 -p blastn -a 2".
        " -i ${lucyOutput}".
        " -d $smallSubunitrRNADb".
        " -e 1e-20 -o ${assemblyDir}/${libraryCode}.lucy.nr.fa.v.SilvaSSU.blastn.e20");

    runJobsInParallel ($numParallelProcess, 0, @jobs);    
    @jobs = ();
    
    logOutputFileCreation("${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.SilvaLSU.blastn.e20");    
    logOutputFileCreation("${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.SilvaSSU.blastn.e20");
    logOutputFileCreation("${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.nr.blastn.e20");
    logOutputFileCreation("${assemblyDir}/${libraryCode}.lucy.nr.fa.v.SilvaLSU.blastn.e20");
    logOutputFileCreation("${assemblyDir}/${libraryCode}.lucy.nr.fa.v.SilvaSSU.blastn.e20");  

    # Parse the Silva db blast hits
    push(@jobs, (BINDIR) . (PARSE_BLAST) .
        " -b ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.SilvaLSU.blastn.e20".
        " -o ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.SilvaLSU.blastn.e20.parsed");
    push(@jobs, (BINDIR) . (PARSE_BLAST) .
        " -b ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.SilvaSSU.blastn.e20".
        " -o ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.SilvaSSU.blastn.e20.parsed");
    push(@jobs, (BINDIR) . (PARSE_BLAST) .
        " -b ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.nr.blastn.e20".
        " -o ${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.nr.blastn.e20.parsed");
    push(@jobs, (BINDIR) . (PARSE_BLAST) .
        " -b ${assemblyDir}/${libraryCode}.lucy.nr.fa.v.SilvaLSU.blastn.e20".
        " -o ${assemblyDir}/${libraryCode}.lucy.nr.fa.v.SilvaLSU.blastn.e20.parsed");
    push(@jobs, (BINDIR) . (PARSE_BLAST) .
        " -b ${assemblyDir}/${libraryCode}.lucy.nr.fa.v.SilvaSSU.blastn.e20".
        " -o ${assemblyDir}/${libraryCode}.lucy.nr.fa.v.SilvaSSU.blastn.e20.parsed");
    runJobsInParallel ($numParallelProcess, 1, @jobs);    
    @jobs = ();
    
    logOutputFileCreation("${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.SilvaLSU.blastn.e20.parsed");    
    logOutputFileCreation("${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.SilvaSSU.blastn.e20.parsed");
    logOutputFileCreation("${assemblyDir}/${libraryCode}.ctgs.lucy.99.3.fasta.v.nr.blastn.e20.parsed");
    logOutputFileCreation("${assemblyDir}/${libraryCode}.lucy.nr.fa.v.SilvaLSU.blastn.e20.parsed"); 
    logOutputFileCreation("${assemblyDir}/${libraryCode}.lucy.nr.fa.v.SilvaSSU.blastn.e20.parsed"); 
}

###############################################################################
sub runThisComponent {
    my $componentName = shift;
    
    #
    # Check if component should be executed based on what's defined in the
    # config file with the prefix execute
    #
    
   $OBJ_PROPS->setExceptionIfEntryNotFound(0); # turn off confess if entry in
                                               # config file is not found.
                                               
    my $runComponent = 0;
    my $findComponent = $OBJ_PROPS->getProperty("execute.$componentName");
       $findComponent =~ s/\s+$//;
    
   $OBJ_PROPS->setExceptionIfEntryNotFound(1); # confess if entry in config file
                                               # is not found.
                                               
    # If component is not defined in config file or if component is defined
    # and is set to 1, then return 1 (yes, run the component).
    #
    if ( !length $findComponent ||
        ( length $findComponent && $findComponent ) ) {
        $runComponent = 1;
    }
    return $runComponent;
}
    
###############################################################################
sub logOutputFileCreation {
    
    my $outputFile = shift;
    
    $outputFile = abs_path($outputFile);
    
    my $fileExists = -s $outputFile ? 1:0;
    my $msg = $fileExists ? "Created $outputFile" :
        "ERROR: failed to create $outputFile";
    logOutput($msg,1);
    
    return $fileExists;
    
}

################################################################################
sub setFileForLogging {
    
    my $logFile = shift;
    
    $OBJ_LOGGER->setLogOutFileAppend($logFile);
    $OBJ_LOGGER->setLogErrorFileAppend($logFile);
    
}

###############################################################################
sub logExecution {
    
    my $programExecution = shift;
    
    my $msg = "Command: ".$programExecution."\n".
              "Current directory: ".getcwd;
    logOutput($msg);
}

###############################################################################
sub logError {
    my $message = shift;
    my $confess = shift || 0;
    
    $OBJ_LOGGER->logError($message);
    
    confess if $confess;
}
    
###############################################################################
sub logOutput {
    my $message = shift;
    my $printMsg = shift || 0;
    
    $OBJ_LOGGER->logOut($message);
    print "$message\n" if $printMsg;
}
    
###############################################################################
sub help {


print <<USE;

$0 [options] <projectID> <inputFile>

Options:
    -od <dir>    Analysis dir (optional; default=<proj>.<libs>.pid).
    -nd <dir>    Newbler dir (optional).
    -n <number>  Number of paralell processes (optional; default=5).
    -c <configFile> Config file (optional).
    -h           Help message (optional).
USE
exit 1;
}

###############################################################################
