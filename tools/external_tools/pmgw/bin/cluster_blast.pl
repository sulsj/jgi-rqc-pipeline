#!/usr/bin/env perl -w

######################################################
# Name:  cluster_blast.pl
# Author: Shane Brubaker
# Created: 3/14/05
# Modified: Oct 2005
#	Mar 2006 - fixed bug with a single file produced
#
#
# Purpose: splits a large fasta file into many chunks
#  and then submits multiple blast jobs to the
# cluster (SGE) using the Array job facility
#
# Notes:
# You must set up SGE by going to the mother node
# (currently magnemite) and typing:
# source /sge/jgi/common/settings.csh 
#  (or .sh for bash shell)
#
# Your output and logs will be placed in your
# current working directory
#
# This script uses the following scripts:
# splitFasta.pl
# blastall_arrayjob.sh
#
#
######################################################

# Declare Modules #
use strict;
use Getopt::Long;

use FindBin '$RealBin';
use lib "$RealBin/../lib";

use FIN_Commands;
use FIN_Constants;


=head1 NAME cluster_blast.pl
=head1 AUTHOR Shane Brubaker

=head1 OPTIONS
=over 16

=item -f (FastaFile) The fasta file to use
=item -dir (Directory) Directory to use for input and output
=item -s (Size) Size to split files into
   blast target database
=item -d blast database path
=item -e evalue to use
=item -l library name
=item -m output type to create (default: 7 for xml output)
=item -U masked data (default: TRUE)
=item -g gapped (default: TRUE)
=item -p blast program name (default: blastn)
  valid values are: blastp, blastn, blastx,psitblastn,tblastn,tblastx
=item -a number of cpus to use per machine (default: 1)
=item -v number of one line descriptions to show 
=item -b number of database sequences to show for alignments
=item -ps ParamString - you can pass through more arbitrary parameters to blast here
=item -q cluster queue to use
=item -c cluster source file
=item --verbose verbose output

=cut


my $usage_str = "
cluster_blast.pl <options>
-c The cluster source settings to use [OPTIONAL]
-f (FastaFile) The fasta file to use [REQUIRED]
-dir (Directory) Directory to use for input and output [REQUIRED]
-s (Size) Size to split files into [REQUIRED]
   blast target database
-d blast database path [REQUIRED]
-e evalue to use (default: 0.1)
-l library name
-m output type to create (default: 7 for xml output)
-U masked data (default: TRUE)
-g gapped (default: TRUE)
-p blast program name (default: blastn)
 valid values are: blastp, blastn, blastx,psitblastn,tblastn,tblastx
-a number of cpus to use per machine (default: 1)
-v number of one line descriptions to show [REQUIRED]
-b number of database sequences to show for alignments [REQUIRED]
-ps ParamString - you can pass through more arbitrary parameters to blast here
-q cluster queue to use [REQUIRED]
-x interpret -s as number of sequences rather than size
--ArrayJob - uses SGE array jobs (default: on)
--verbose verbose output
";

if ($#ARGV < 1) { die $usage_str; }
my $cmdl = join(' ',@ARGV);

my %opt = (
 FastaFile => undef,
 Directory => undef,
 FileSize => undef,
 BlastDatabase => undef,
 EValue  => "0.1",
 OutputType => 7,
 Masked  => 1,
 Gapped  => 1,
 BlastProgramName => "blastn",
 NumCpus => 1,
 NumDesc => undef,
 NumSeqs => undef,
 ParamString => undef,
 Queue  => undef,
 ArrayJob => 1,
 verbose  => 0,
 Seq => undef,
 Cluster => undef,
 Library => undef,
 );

&GetOptions(
 "c|Cluster=s" => \$opt{Cluster},
 "f|FastaFile=s" => \$opt{FastaFile},
 "dir|Directory=s"  => \$opt{Directory},
 "s|FileSize=i"  => \$opt{FileSize},
 "d|BlastDatabase=s"  => \$opt{BlastDatabase},
 "e|EValue=s"    => \$opt{EValue},
 "l|Library=s"   => \$opt{Library},
 "m|OutputType=i"  => \$opt{OutputType},
 "U|Masked!"    => \$opt{Masked},
 "g|Gapped!"    => \$opt{Gapped},
 "p|BlastProgramName=s" => \$opt{BlastProgramName},
 "a|NumCpus=i"   => \$opt{NumCpus},
 "v|NumDesc=i"   => \$opt{NumDesc},
 "b|NumSeqs=i"  => \$opt{NumSeqs},
 "ps|ParamString=s"  => \$opt{ParamString},
 "q|Queue=s"  => \$opt{Queue},
 "x|Seq" => \$opt{Seq},
 "ArrayJob!"  => \$opt{ArrayJob},
 "verbose!"     => \$opt{verbose},
 ) || die $usage_str;


## Declarations ##
my $cmd;   # system command
my $file;   # split fasta files
my $verbose;
my $num_array_jobs;  # number of array jobs (fasta files) to submit
my $seqo = "";
$seqo = "-s" if (defined ($opt{Seq}) && $opt{Seq});

## Begin Code ##
print "Starting\n" if ($opt{verbose});
$ verbose = $opt{verbose};
# Change into the working directory
chdir "$opt{Directory}" || die "cannot change to directory $opt{Directory} in cluster_blast.pl\n";

# 1. First run splitFasta.pl on the input file
# use the file list option to create a list of the output files
# 
# todo - replace this with a module

$cmd = (BINDIR) . (SPLIT_FASTA) . " $opt{FastaFile} $opt{FileSize} ${seqo} fasta_split -f";
system($cmd);


if ($opt{ArrayJob}) {

 # 2.  Write the cluster script that will be used for the array job
 open OUTF, ">blastall_arrayjob.sh"  or die "ERROR: Cant open file: blastall_arrayjob.sh :: $!";

 print OUTF "#!/bin/bash\n";
 print OUTF "#\$ -S /bin/bash\n";
 print OUTF "# cmd: $0 $cmdl\n";
 print OUTF "export JOBNUM=\$((\$SGE_TASK_ID-1))\n";
 print OUTF "export i=\$JOBNUM\n";
 print OUTF "export j=`printf %06d \$i`\n";
 print OUTF "export JOBNUM=\$j\n"; 

 # prepare sge
 my $shell = `ps -p $$ | tail -1 | awk '{print \$NF}'`;
 my $source_cmd = '';

 if (!defined($opt{Cluster}) || $opt{Cluster} eq "") {
     if ($shell =~ /csh/) {
	 $source_cmd = "source $ENV{SGE_SETTINGS}.csh";
     } else {
	 $source_cmd = "source $ENV{SGE_SETTINGS}.sh";
     }
 } else {
     $source_cmd = "source $opt{Cluster}";
 }

 print OUTF "$source_cmd\n";

 if (defined($opt{ParamString})) {
	 print OUTF (BLASTALL) . " -i $opt{Directory}/fasta_split.\$JOBNUM -o $opt{Directory}/blastall_results.\$JOBNUM -d $opt{BlastDatabase} -e $opt{EValue} -m $opt{OutputType} -p $opt{BlastProgramName} -a $opt{NumCpus} -v $opt{NumDesc} -b $opt{NumSeqs} $opt{ParamString} ";
 } else {
	 print OUTF (BLASTALL) . " -i $opt{Directory}/fasta_split.\$JOBNUM -o $opt{Directory}/blastall_results.\$JOBNUM -d $opt{BlastDatabase} -e $opt{EValue} -m $opt{OutputType} -p $opt{BlastProgramName} -a $opt{NumCpus} -v $opt{NumDesc} -b $opt{NumSeqs} ";
 }
 print OUTF " -U " if (defined($opt{Masked}) && $opt{Masked});
 print OUTF " -g " if (defined($opt{Gapped}) && $opt{Gapped});
 print OUTF "\n";

 close OUTF;
 `chmod +x blastall_arrayjob.sh`;

 # 3. Now call blastall_arrayjob.sh on the cluster with the parameters supplied for each file
 # determine number of fasta files created to determine number of array jobs submitted

 $num_array_jobs = `wc -l flist.fasta_split`;  # we used splitFasta above with the -f option to 
 # create a file flist.fasta_split with the list of files
 # so the number of lines is the number of files/jobs
 $num_array_jobs = substr($num_array_jobs,0,rindex($num_array_jobs," "));
 $num_array_jobs =~ s/ //g; 
 $num_array_jobs =~ s/\t//g; 	# these things are done to make sure it works on sun

 if (!defined($num_array_jobs)) { warn "could not get number of array jobs properly\n"; $num_array_jobs = 1; }
 print "Found number of jobs/files: $num_array_jobs\n" if ($verbose);

 $opt{Library} =~ s|\/|_|go;

 if (defined($opt{Library})) {
  $cmd = "qsub -N $opt{Library} -t 1-$num_array_jobs -cwd -o $opt{Directory}/log.blastall.\\\$TASK_ID -b y -l $opt{Queue}.c ./blastall_arrayjob.sh -e $opt{Directory}/error.log";
 } else {
  $cmd = "qsub -t 1-$num_array_jobs -cwd -o $opt{Directory}/log.blastall.\\\$TASK_ID -b y -l $opt{Queue}.c ./blastall_arrayjob.sh -e $opt{Directory}/error.log";
 }

 print "system command will be: $cmd\n" if ($verbose);
 system("$source_cmd; $cmd");

} # end if Array Job mode
else {   # use standard mode of individual qsub
 die "not implemented yet\n";
}

print "Your jobs have been submitted to queue $opt{Queue}\n" if ($opt{verbose});

print "Ending\n" if ($opt{verbose});

## End of Code ##

