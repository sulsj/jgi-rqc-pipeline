#!/usr/bin/env perl
print "Deprecated\n";
exit(1);
use strict;

use lib "/home/prefin/lib";

use Cwd;
use Getopt::Long;

use FindBin;
use lib "$FindBin::Bin/../lib";

use FIN_Commands;
use FIN_Constants;

use vars qw($optAnalysisDir $optLibrary $optProject);

# input value
#
if( !GetOptions(
                "a=s" =>\$optAnalysisDir,
		"p=s" =>\$optProject,
		"l=s" =>\$optLibrary
		) ) {
    help();
}

unless ($optProject) {
    help();
}

unless ($optLibrary) {
    help();
}

unless ($optAnalysisDir) {
    help();
}

my $topDir = getcwd();

chdir $optAnalysisDir;

my $workingDir = getcwd();

my $assemblyDir = "${workingDir}/${optLibrary}_mi98ml80/assembly";
my $lucyDir = "${workingDir}/${optLibrary}_lucy";
my $trimDir = "${workingDir}/${optLibrary}_trim";
my $listsDir = "${workingDir}/${optLibrary}_lists";


my $cmd = '';

$cmd = (LN_CMD) . " ${lucyDir}/${optLibrary}.nr.lucy.99.3.fasta ${trimDir}/${optProject}.nr.lucy.99.3.fasta";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${lucyDir}/${optLibrary}.454TrimmedReads.nr.fna ${trimDir}/${optProject}.454TrimmedReads.nr.fna";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${assemblyDir}/${optLibrary}.ctgs.lucy.99.3.fasta ${assemblyDir}/${optProject}.454AllContigs.lucy.fa";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${assemblyDir}/${optLibrary}.singlets.lucy.99.3.fasta ${assemblyDir}/${optProject}.454SingletonReads.lucy.fa";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${assemblyDir}/${optLibrary}.unassem.lucy.99.3.fasta ${assemblyDir}/${optProject}.454UnassembledReads.lucy.fa";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${workingDir}/contigs.Length_vs_MeanCov.jpg ${assemblyDir}/contigs.Length_vs_MeanCov.jpg";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${workingDir}/contigs.GC_vs_Depth.jpg ${assemblyDir}/contigs.GC_vs_Depth.jpg";
system($cmd);
print "$cmd\n";


$cmd = (LN_CMD) . " ${assemblyDir}/${optLibrary}.ctgs.lucy.99.3.fasta.v.SilvaLSU.blastn.e20.parsed ${assemblyDir}/${optProject}.454AllContigs.lucy.fa.v.SilvaLSU.blastn.e20.parsed";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${assemblyDir}/${optLibrary}.ctgs.lucy.99.3.fasta.v.SilvaSSU.blastn.e20.parsed ${assemblyDir}/${optProject}.454AllContigs.lucy.fa.v.SilvaSSU.blastn.e20.parsed";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${assemblyDir}/${optLibrary}.ctgs.lucy.99.3.fasta.v.nr.blastn.e20.parsed ${assemblyDir}/${optProject}.454AllContigs.lucy.fa.v.nr.blastn.e20.parsed";
system($cmd);
print "$cmd\n";


$cmd = (LN_CMD) . " ${assemblyDir}/${optLibrary}.ctgs.lucy.99.3.qual ${assemblyDir}/${optProject}.454AllContigs.lucy.fa.qual";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${assemblyDir}/${optLibrary}.singlets.lucy.99.3.qual ${assemblyDir}/${optProject}.454SingletonReads.lucy.fa.qual";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${assemblyDir}/${optLibrary}.unassem.lucy.99.3.qual ${assemblyDir}/${optProject}.454UnassembledReads.lucy.fa.qual";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${lucyDir}/${optLibrary}.nr.lucy.99.3.fasta ${assemblyDir}/${optLibrary}.lucy.nr.fa";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${lucyDir}/${optLibrary}.nr.lucy.99.3.qual ${assemblyDir}/${optLibrary}.lucy.nr.fa.qual";
system($cmd);
print "$cmd\n";

$cmd = (LN_CMD) . " ${listsDir}/${optLibrary}.RedundantReads.list ${trimDir}/${optLibrary}.RedundantReads.list";
system($cmd);
print "$cmd\n";


chdir $topDir;

exit;


###############################################################################
sub help {

    print <<USE;

$0 [options] -l <libraryCode> -p <projectID> -a <dir>

   -p  project identifier
   -n  number of parallel jobs
   -a  analysis directory

USE
exit;
}
