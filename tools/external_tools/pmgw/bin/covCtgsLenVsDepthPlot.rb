require 'yaml'
$VERBOSE = nil

if ARGV.size != 3
  STDERR.puts "Usage: $0 <inFile> <description of source of reads, e.g. 'Library FUAY'>"
  Process.exit  
end

contigsGC = ARGV[0]
if !File.exists? contigsGC or File.zero? contigsGC
  STDERR.puts "File #{contigsGC} does not exist or empty"
  Process.exit
end


contigsCov = ARGV[1]
if !File.exists? contigsCov or File.zero? contigsCov
  STDERR.puts "File #{contigsCov} does not exist or empty"
  Process.exit
end

proj = ARGV[2]

# get name, gc, and length
#CONTIG  G+C     A+T     tot     %G+C
#contig00001     416     332     748      55.615
contigInfo = Hash.new
File.open(contigsGC, "r").each_line do |line|
#  next if line !~ /\.C\d+/
   next if line !~ /contig\d+/
  line.chomp!
  name, blah, blah, length, gc = line.split(/\s+/)
#  puts "contig name is #{name}"
  contigInfo[name] = Hash.new
  contigInfo[name]['length'] = length
  contigInfo[name]['gc'] = gc
end



# get depth
##contig len     reads   mean_cov        stddev_cov
#contig00001     748     2       1.9     0.34
#contig00002     740     2       1.9     0.28
File.open(contigsCov, "r").each_line do |line|
#  next if line !~ /.C\d+/
   next if line !~ /contig\d+/
  line.chomp!
  name, blah, depth, blah, blah = line.split(/\s+/)
  if !contigInfo.has_key?(name)
    puts "Warning: #{name} not in *contigs.GC"
  else
    contigInfo[name]['depth'] = depth
  end
end

#sort by contig #
names = contigInfo.keys.sort do |a,b|
  x = y = 0
#  x = $1.to_i if a =~ /\.C(\d+)/
 x = $1.to_i if a =~ /contig(\d+)/
#  y = $1.to_i if b =~ /\.C(\d+)/
 y = $1.to_i if b =~ /contig(\d+)/ 
  x <=> y
end

# output
File.open("contigs.stats", "w") do |f|
  f.puts "Name\tLength\tGC\tDepth"  
  names.each do |name|
    hash = contigInfo[name]
    f.puts "#{name}\t#{hash["length"]}\t#{hash["gc"]}\t#{hash["depth"]}"
  end  
end

  


outFile = "contigs.Length_vs_Depth.jpg"
tempFile = "contigs.temp"
cmd = <<EOF 
gcd<-read.table("contigs.stats", header=TRUE);
jpeg("#{outFile}", quality=100, height=480, width=640)
plot(gcd[,2], gcd[,4], pch="x", xlab="Contig Length",ylab="Coverage (# of reads in contig)",main="#{proj} Coverage vs. Contig Length")
dev.off()
EOF

config = YAML.load_file(File.dirname(__FILE__) + '/../config/plot.yml')

File.open(tempFile, "w") { |f| f.puts cmd }
system("#{config['R']} --no-save < #{tempFile}")

