#!/usr/bin/env perl

while(<>){
    chomp;
    @temp = split("\t",$_);
    print "$temp[0]\n",&seq2fasta($temp[1]);
}


#====================================================================================
sub seq2fasta{
    my $seq = $_[0];
    $seq =~ s/(.{50})/$1\n/g;
    ($seq .= "\n") =~ s/\n+$/\n/;
    return $seq;
}

#====================================================================================
sub qual2fasta{
    my $qual = $_[0];
    $qual =~ s/((?:\s\d+){50})/$1\n/g;
    ($qual .= "\n") =~ s/\n+$/\n/;
    return $qual;
}
