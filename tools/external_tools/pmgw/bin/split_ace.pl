#!/usr/bin/env perl

use strict;

use Data::Dumper;
use Time::localtime;


if (@ARGV < 2) {
    print STDERR "Usage: $0 <acefile> <file of contig names> \n";
    exit;
}

my %contigs;
open(IN, $ARGV[1]) or die "Can't open $ARGV[1]\n";
while(<IN>){
    chomp;
    $contigs{$_}++;
}
close(IN);


my %contig_reads;
my $temp_file_new = $ARGV[0] . '.new';
my $temp_file_old = $ARGV[0] . '.old';

open (IN, $ARGV[0]) or die "can't open $ARGV[0]\n";
open (NEW, ">$temp_file_new") or die "can't open $temp_file_new\n";
open (OLD, ">$temp_file_old") or die "can't open $temp_file_old\n";
my $co = 0;
my ($tmp, $contig);
while (<IN>){
    if ($_ =~/^AS\s+(\d+)\s+(\d+)/){
	$contig_reads{$temp_file_old}{'contigs'} = $1;
	$contig_reads{$temp_file_old}{'reads'} = $2;
	$tmp = <IN>;
	next;
    }
    if ($_ =~ /^CO\s+(\S+)/ && exists($contigs{$1})){
	$co = 1;
	$contig_reads{$temp_file_new}{'contigs'}++;
	print NEW $_;
	next;
    }
    elsif ($_ =~ /^CO\s+(\S+)/ && ! exists($contigs{$1})){
	$co = 0;
    }
    elsif ($_ =~ /^CT\{/){
	$co = 0 if $co;
	$tmp = <IN>;
	($contig = $tmp) =~ s/^(\S+)\s.*$/$1/;
	chomp $contig;
	if (exists($contigs{$contig})){
	    print NEW "CT\{\n$tmp";
	    while (($tmp = <IN>) !~ /^\}/){
		print NEW $tmp;
	    }
	    if ($tmp =~ /^\}/){
		print NEW $tmp;
		$tmp = <IN>;
		print NEW $tmp;
	    }
	}
	else{
	    print OLD "CT\{\n$tmp";
	    while (($tmp = <IN>) !~ /^\}/){
		print OLD $tmp;
	    }
	    if ($tmp =~ /^\}/){
		print OLD $tmp;
		$tmp = <IN>;
		print OLD $tmp;
	    }
	}
	next;
    }
    elsif($_=~ /^RD/ && $co){
	$contig_reads{$temp_file_new}{'reads'}++;
    }

    print NEW $_ if ($co);
    print OLD $_ if !($co);
}
close(IN);
close(NEW);
close(OLD);


$contig_reads{$temp_file_old}{'reads'} = $contig_reads{$temp_file_old}{'reads'} - $contig_reads{$temp_file_new}{'reads'};
$contig_reads{$temp_file_old}{'contigs'} = $contig_reads{$temp_file_old}{'contigs'} - $contig_reads{$temp_file_new}{'contigs'};

my $temp_file = time() . 'temp';
my $cmd;
foreach my $file (keys %contig_reads){
    open (IN, $file) or die "Can't open $file\n";
    open (OUT, ">$temp_file") or die "Can't open $temp_file\n";
    while(<IN>){
	if ($. == 1) {
	    print OUT "AS $contig_reads{$file}{'contigs'} $contig_reads{$file}{'reads'}\n\n$_";
	}
	else{
	    print OUT $_;
	}
    }
    close(IN);
    close(OUT);
    $cmd = "mv $temp_file $file\n";
    system("$cmd");
}

