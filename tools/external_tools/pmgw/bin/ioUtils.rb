class Dir
  # append contents of each file (based on pattern) to another file
  # similar to linux cat command
  def self.fileCopy(pattern, to, errorIfMissing = true)
    sources = Dir[pattern]
    if sources.size == 0
      if errorIfMissing
        STDERR.puts "No files found using pattern #{pattern}" 
      else          
        puts "[WARNING] No files found using pattern #{pattern}"
      end      
    else
      File.open(to, "w") do |dest|
        sources.each do |src|          
          File.open(src, "r").each_line { |line| dest.print line}
        end      
      end
    end
  end
end

module Email
  def Email.send(to, subject, msg, lastStage = nil, command = nil)
    return if !to or !msg
    
    pid = fork do
      subject = "[Pipeline] " + subject
      fileName = "tmpPipelineEmail"
      file = File.open(fileName, "w")
      file.write("This is an automated message from the pipeline manager tool.  Do not respond to this email.")
      file.write("\n\n--- Message ---\n#{msg}")
      file.write("\n\nLast stage completed: #{lastStage}") if lastStage
      file.write("\nAttempting to run command: #{command}") if command
      
      user = `whoami`.chomp
      host = `hostname`.chomp
      os = `uname -a`.chomp
      pwd = `pwd`.chomp
  
      file.write("\n\n\n\n--- Pipeline Environment ---")
      file.write("\nUsername: #{user}")
      file.write("\nHostname: #{host}")
      file.write("\nWorking Directory: #{pwd}")
      file.write("\nOperating System: #{os}")    
      file.close
          
      mailCmd = "mail -a \"From: PipelineManager <no-reply@jgi-psf.org>\" -s \"#{subject}\" #{to} < #{fileName}"
      `#{mailCmd}` 
      
      puts "Notification sent to #{to}"
      `rm -f #{fileName}`
      #File.delete(fileName) # can't always find file
    end
    Process.detach(pid)
  end
end
