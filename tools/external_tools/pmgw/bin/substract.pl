#!/usr/bin/env perl

$big_file = $ARGV[0];
$small_file = $ARGV[1];
$field = $ARGV[2];
$field =0 unless ($field);

open (SMALL,$small_file) || die "cannot open smaller list $small_file";
while (<SMALL>){
	chomp;
	$data{$_} = 1;
}
close SMALL;

open (BIG,$big_file) || die "cannot open larger list $big_file";
while (<BIG>){
	chomp;	
	@line = split;
	print "$_\n" unless (exists $data{$line[$field]});
}
close BIG;
