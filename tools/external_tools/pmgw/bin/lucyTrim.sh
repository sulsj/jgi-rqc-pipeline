#!/bin/bash

echo "Deprecated"
exit 1

LUCY="/jgi/tools/bin/lucy"
FA_SIZE="/home/copeland/local/IX86/bin/faSize"
FIGARO_TRIM_SEQS="/home/copeland/scripts/figaro_trim_seqs.pl"
NRGREP='/home/copeland/local/IX86/bin/nrgrep'

#-pass_header Don't modify fasta or qual header (RQC)]
#-trim_seq trim output instead of writing trim coords (RQC)]
#LUCY="/jgi/tools/misc_bio/lucy/versions/rqc/lucy_rqc -pass_header -trim_seq"

PERR=${1:?"Specify desired output percent accuracy e.g. 95"}
#FA="$PERR.fa" ;
FA="$PERR.fna" ;
FQ="$FA.qual" ;
PRE=${2:?"Specify trimmed output filename prefix e.g. GBAN" } 
FO=$PRE.$PERR
FA_IN=${3:?"Specify fasta file to trim"}
FQ_IN="$FA_IN.qual"

shift;shift;shift;
XTRAARGS="$*" ;# e.g. -vector <vec.fa> <vec.splice.fa>

if [ ! -s $FA_IN ] ; then echo "(lucyTrim.sh) Error: can't find fasta file: $FA_IN" ; exit ; fi
if [ ! -s $FQ_IN ] ; then echo "(lucyTrim.sh) Error: can't find fasta file: $FQ_IN" ; exit ; fi

ERR=$(perl -e "print 1.0-$PERR/100") ;
echo "#!/bin/bash" > $FO.log

#run lucy
CMD="$LUCY -error $ERR $ERR -bracket 20 $ERR -debug $FO.info -xtra 6 $XTRAARGS -output $FA $FQ $FA_IN $FQ_IN" ;
echo $CMD >>$FO.log

# create trim.points
CMD="$NRGREP '>' $FA | sed 's/>//' | cut -d' ' -f 1,5,6 > $FO.trim.points"
echo $CMD >>$FO.log

# do trimming
CMD="$FIGARO_TRIM_SEQS -o $FO -c $FO.trim.points  -m f -f $FA -q $FQ "
echo $CMD >>$FO.log

# stats
CMD="$FA_SIZE $FO.fasta > $FO.stats"
echo $CMD >>$FO.log
. $FO.log

# cat $FO.stats

#cleanup
rm $FA $FQ 
#rm $FO.qual
