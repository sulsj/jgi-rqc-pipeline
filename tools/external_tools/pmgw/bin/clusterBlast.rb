# this class monitors clusterblast jobs on the SGE cluster 
# assumes this is run from above clusterblast output dirs
require File.dirname(__FILE__) + '/ioUtils'
require 'time'
require 'pp'
require 'yaml'

class ClusterBlast
  attr_reader :successful
  attr_accessor :RESTART_FREQ, :PROGRESS_FREQ, :MONITOR_SLEEP
  
  def initialize(outdir, config)
    @outdir = outdir
    @config = config

    # prepare sge
    shell = `ps -p $$ | tail -1 | awk '{print $NF}'`

    if shell =~ /csh/
        @cluster_source = @config['settings_csh']
    else
        @cluster_source = @config['settings_sh']
    end

    @dirs = Hash.new
    @pid = Hash.new
    @libFa = Hash.new
    @lib = Hash.new
    @cmd = Hash.new
    @errors = Hash.new  # by job and task
    @timeUpdate = Hash.new
    @lastNumCompleted = Hash.new
    @lastNumFailed = Hash.new 
    @startedRunning = Hash.new
    @restarted = Hash.new
    @restart = Hash.new
    @taskErrors = Hash.new
    @tasks = Hash.new
    @autoJobId = -1
    
    @RESTART_FREQ = 43200  # restart job if no progress in previous 12 hours (after job actually started)
                           # includes running time and queueing time 
    @PROGRESS_FREQ = 7200  # if more than 90% done, and no progress since previous 2 hours, that's good enough
    @MONITOR_SLEEP = 20   # essentially controls how often qacct running (still needed?)
  end
  
  def getNextId
    @autoJobId -= 1
    return @autoJobId 
  end
  
  def errorUnstartedJob(dir = nil)    
    id = getNextId
    @dirs[id] = dir
    @errors[id] = true
  end
  
     
  def nonEmptyResultFiles(job)
    files = Dir[@dirs[job] + "/blastall_results.*"]
    nonEmpty = Array.new
    files.each { |f| nonEmpty.push f if File.size(f)> 0 }
    return nonEmpty
  end

  # only concatenate jobs without errors, and that are not empty
  def concatenateJobResults(job, dest)
    #resultFiles = nonEmptyResultFiles(job)
    resultFiles = Array.new
    @tasks[job].each do |task, bool|
      task = task.to_i
      resultFiles.push(@dirs[job] + "/" + sprintf("blastall_results.%06d", task-1))
    end
  
    pp @tasks
    
    @taskErrors[job].each do |num,v|
      num = num.to_i
      result = @dirs[job] + "/" + sprintf("blastall_results.%06d", num-1)      
      resultFiles.delete result
    end
    
    pp @taskErrors
    
    dest.sync = true
    resultFiles.each do |f| 
      if File.exists? f
        puts "including #{f}\n"
        dest << IO.read(f)
      else
        puts "file not exists: #{f}\n"
      end
    end          
    dest
  end

  def destFile(job)
    @dest = File.open(@dirs[job] + "/" + @pid[job] + "_reads.v.nr.blastx.e3", "w")
  end

  def stat(job)
    out = `source #{@cluster_source}; qstat -j #{job} 2>&1`
  end
  
  def del(job)
    system("#{@cmd}; qdel #{job}")
  end

  def done?(job)
    return true if (stat(job) =~ /Following jobs do not exist/m )
  end
  
  def inError?(job)
    return true if (stat(job) =~ /error reason/m)        
  end
  
  def jobRunning?(job)
    user = `whoami`.chomp
    return true if (`source #{@cluster_source}; qstat -u #{user}` =~/#{job}[^\n]*\sr\s/)
  end

  # assumes getTasksStats called previously
  def getFailedTasks(job, outFile = "#{job}.out")
    failedTasks = 0
    failedTasks = `grep -B 10 "failed       [^0]" #{outFile} | grep taskid`
    failedTasks += `grep -B 10 "exit_status  [^0]" #{outFile} | grep taskid`
    failedTasks.each do |line|
      if line =~ /taskid\s+(\d+)/
        @taskErrors[job] = Hash.new if !@taskErrors.has_key? job 
        @taskErrors[job][$1] = true
      end
    end
      
    if @taskErrors.has_key?(job)
      return @taskErrors[job].size
    else
      return 0
    end
  end

  def numberOfTasks(job)
    out = stat(job)
    if out =~ /tasks:\s+1-(\d+)/
      return $1.to_i 
    else      
      return 0
    end    
  end
  
  def getTasksStats(job, outFile = nil)
    listedTasks = rescheduledTasks = completedTasks = failedTasks = numberTasks = 0    
    numberTasks = numberOfTasks(job)
    puts "number of tasks = #{numberTasks}\n"
    if (numberTasks > 0)      
      if (!outFile)
        outFile = "#{job}.out"
        puts "before qacct #{Time.now.to_i}"
        `qacct -j #{job} -t 1-#{numberTasks} > #{outFile} 2>/dev/null`   # this will take 4 - 6 minutes
        puts "just ran qacct\n"
        puts `ls -l #{outFile}`
        puts
      end
      
      listedTasks = `grep -c failed #{outFile}`.to_i
      rescheduledTasks = `grep -c reschedul #{outFile}`.to_i
      completedTasks = listedTasks - rescheduledTasks     # completed successfully or completed unsuccessfully
      failedTasks = getFailedTasks(job, outFile) - rescheduledTasks
    
      tasks = `grep taskid #{outFile}`
      tasks.each do |line|
        if line =~ /taskid\s+(\d+)/
          @tasks[job]= Hash.new if !@tasks.has_key? job
          @tasks[job][$1] = true 
          puts "adding #{job} to tasks"
        end  
      end
      
      puts "taskids"
      puts tasks
      puts
      pp @tasks
      puts "done getTasksStats\n\n"
      
    end  
      
    return [failedTasks, completedTasks, numberTasks]
  end
  
  
  def runJob(pid, libFa, lib, command = nil, restart = nil)
    libDir = @outdir

    cleanDir(libDir) if File.exists? libDir
    Dir.mkdir(libDir) if !File.exists? libDir

    basename = File.basename(libFa)
    fullname = File.expand_path(libFa)

    system("ln -s #{fullname} #{libDir}/#{basename} 2>/dev/null") if !File.exists? "#{libDir}/#{basename}"

    if command.nil?
      cmd = "perl " + File.dirname(__FILE__) + "/#{@config['cluster_blast_pl']} -f #{libDir + "/" + basename}" +
        " -l #{lib} -dir #{libDir} -s 50000 -d #{@config['nr_blast_db']} -e 1e-3 -v 20 -b 20 -q long" +
        " --verbose -p blastx -m 0 -noU -c #{@cluster_source}"
    else
      cmd = command
    end        
    
    puts "source #{@cluster_source}; #{cmd}"
    out = `source #{@cluster_source}; #{cmd}`

    jobId = addJob(libDir, out, pid, libFa, lib)
    @cmd[jobId] = cmd
    @timeUpdate[jobId] = Time.now
    @lastNumCompleted[jobId] = 0
    @lastNumFailed[jobId] = 0    
    @restarted[jobId] = true if restart
    @tasks[jobId] = Hash.new
    @taskErrors[jobId] = Hash.new
    return jobId
  end


  def cleanDir(dir)
    if dir =~ /clusterblast/
      cmd = "find #{dir} -type f -name \"*\" -print | xargs rm -rf"
      system(cmd)
    end    
  end
  
  # output is captured output when submitting job to cluster
  # dir is directory where cluster output is stored
  def addJob(dir, output, pid, libFa, lib)
    jobId = nil
    if output =~ /Your job-array (\d+)/m
      jobId = $1
    elsif output =~ /Your job (\d+)/m
      jobId = $1
    else
      puts "Warning: job could not start"  
      errorUnstartedJob(dir)
      return
    end
    
    if File.exists? dir
      @dirs[jobId] = dir
      @pid[jobId] = lib
      @libFa[jobId] = libFa
      @lib[jobId] = lib
    else
      puts "Warning: Directory #{dir} missing"
      @errors[jobId] = true
    end    
    return jobId
  end
  
  def restartClusterJob(job, msg = "")
    if (!@restarted.has_key?(job))
      msg = "Restarting #{job}. " + msg
      puts msg
      Email::send(@config['status_email_address'], "Metagenome QC Blastx Status", msg)    
      del(job)                
      @restart[job] = true
      runJob(@pid[job], @libFa[job], @cmd[job], restart = true)     
    else
      Email.send(@config['status_email_address'], "Metagenome QC Blastx Status", 
                  "Error: Already restarted job.  Use qdel to delete job #{job}.  " + msg)    
      @errors[job] = true      
    end
  end

  def emailReport    
    user = `whoami`.chomp    
    if (@errors.size > 0)
      msg = "ERROR: One or more SGE runs failed (#{@errors.keys.join(',')})."
      STDERR.puts msg
      @successful = false
    else
      msg = "All SGE run(s) successful"
      puts msg
      @successful = true
    end    
    Email.send(@config['status_email_address'], "Metagenome QC Blastx Status", msg) 
    if (user != @config['maintainer'])
      cmd = "ldapsearch -v -x -b 'dc=jgi-psf,dc=org' -s sub uid=" + user + " 2>/dev/null | grep mail:"
      if (`#{cmd}` =~ /^mail:\s+(\S+)$/)
        Email.send($1, "Metagenome QC Blastx Status", msg)      
      end
    end    
  end

 # def moveResultsToProjDir
 #   @dirs.each do |jobId, dir|
 #     system("rm -f #{dir}/core")
 #     system("rsync --remove-source-files -a #{dir} #{Dir.pwd} && rm -rf #{dir}")
 #   end    
 # end

  # periodically checks state of different jobs - decides whether to restart, wait, or kill job
  def monitor
    done = Hash.new 
    
    while 1      
      if @dirs.size > 0  
        break if (done.size + @errors.size + @restart.size) >= @dirs.size       # mutually exclusive states

        @dirs.keys.each do |job|            
          next if @errors.has_key?(job)   # job enters error state if attempted to be restarted twice
                                          # or too many task errors reported by qacct
          next if @restart.has_key?(job)  # track the old restarted jobs since old job ids never disappear, 
                                          # restarted job gives new job id
          next if done.has_key?(job)

          # get task status                     
          failed, completed, allotted  = getTasksStats(job)
          percentTaskErrors = percentDone = 0.0
          if (allotted > 0)
              percentDone = 100.0 * (completed - failed).to_f / allotted.to_f
              percentTaskErrors = 100.0 * failed.to_f / allotted.to_f
          end

          # qstat reports finished status
          if done?(job)
#            concatenateJobResults(job, destFile(job))
            done[job] = true

          # restart if too many errors
          elsif (percentTaskErrors >= 10.0)
            restartClusterJob(job,  "Too many errors.")

          # no progress?
          elsif (completed == @lastNumCompleted[job] and failed == @lastNumFailed[job])

            # job is almost done
            if ((Time.now - @timeUpdate[job]) > @PROGRESS_FREQ and percentDone > 90.0)                  
              puts "> 90% done and no more progress, deleting the job and summarizing results"
#              concatenateJobResults(job, destFile(job)) 
              done[job] = true
              del(job)
                            
            ## job is not almost done
            #elsif ((Time.now - @timeUpdate[job]) > @RESTART_FREQ)
            #  # allows for restarts if job queued too long 
            #  restartClusterJob(job, "No progress for #{@RESTART_FREQ.to_f/3600.0} hours")              

            # job in Eqw state but not almost done - restart immediately
            elsif (!jobRunning?(job) and inError?(job))
              restartClusterJob(job, "Likely in Eqw state.") if inError?(job)              
            end
            
          # progress made - wait it out
          else 
            @lastNumCompleted[job] = completed
            @lastNumFailed[job] = failed     
            @timeUpdate[job] = Time.now                      
          end
                                            
          sleep @MONITOR_SLEEP
        end

      else
        STDERR.puts "No jobs started."
        Process.exit
      end
    end
    
#    moveResultsToProjDir()
    emailReport()
  end
end
