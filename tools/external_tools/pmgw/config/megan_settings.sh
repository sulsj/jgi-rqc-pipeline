#!/bin/bash
# commented out in "megan.pl" - these paths do not exist.  Bryce - 2013-10-29
#source /jgi/tools/Tilden/bin/tilden_env.sh PRODUCTION;

source  /global/homes/s/sulsj/work/bitbucket-repo/jgi-rqc-pipeline/tools/external_tools/pmgw/config/tilden_env.sh PRODUCTION;
## sulsj 20131209, bryce changed from projectb to dna 20140314
#source /global/dna/projectdirs/PI/rqc/prod/versions/jgi-rqc-pipeline-20131125/tools/external_tools/pmgw/config/tilden_env.sh PRODUCTION;

#source /opt/sge/hyperion/common/settings.sh;
#export GEM_PATH=/home/analytics/ruby/gems:/jgi/tools/ruby/gems/1.8:/var/lib/gems/1.8;
#export PATH=$PATH:/home/copeland/scripts:/home/analytics/metagenomeQC:/home/analytics/ruby/gems/bin:/jgi/tools/ruby/gems/1.8/bin:/var/lib/gems/1.8/bin;
#export RUBYLIB=/jgi/tools/ruby/lib:/home/analytics/metagenomeQC;     #:/home/analytics/pipeline;



mode=$1
if [ -z "$mode" ]
then
        mode=PROD
fi

if [ "$mode" == "DEV" ]
then
      #export MQC_PATH="/home/pwinward/workspace/metagenomeQC"
      echo ""
elif [ "$mode" == "PROD" ]
then
      #export MQC_PATH="/home/analytics/metagenomeQC"
      echo ""
else
      echo "INVALID mode '$mode'" 1>&2
      return -1
fi
