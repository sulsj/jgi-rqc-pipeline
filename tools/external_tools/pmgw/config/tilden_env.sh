#
# $Header: /repository/PI/Tilden/bin/tilden_env.sh,v 1.17 2010-01-07 05:42:15 regan Exp $
#

# This script defines the environment for Tilden
# Be sure to source this file to get everything to work properly
#

_tilden_mode=$1
if [ -z "$_tilden_mode" ]
then
    _tilden_mode=PRODUCTION
fi

alt_path=$2

if [ "${TILDEN_ENV}" != "$_tilden_mode" ]
then
  if [ "$_tilden_mode" == "PRODUCTION" -o "$_tilden_mode" == 'PROD' ]
  then
    export TILDEN_ENV=PRODUCTION
    export TILDEN_PATH=/global/dna/projectdirs/PI/rqc/prod/tools/Tilden
    export CHROMAT_HOST=tilden.jgi-psf.org
    export CHROMAT_HOST_PORT=80
    export CHROMAT_HOST_LISTEN=8080

    export TILDEN_COMMON_SERVER=venonat.jgi-psf.org
    export TILDEN_SERVER=clay.jgi-psf.org
    
    # variables specific to production
    # Comma separated list of all the hosts for the load-balanced environment
    export TILDEN_DEPLOYMENT_HOSTS=${TILDEN_DEPLOYMENT_HOSTS:="pi-edge1.jgi-psf.org,pi-edge2.jgi-psf.org"}
    export TILDEN_DEPLOYMENT_PATH=${TILDEN_DEPLOYMENT_PATH:=/opt/apache-tilden}
    
  elif [ "$_tilden_mode" == "DEV_TEST" ]
  then
    export TILDEN_ENV=$_tilden_mode
    export TILDEN_PATH=/global/dna/projectdirs/PI/rqc/prod/tools/Tilden/dev_test
    export CHROMAT_HOST=diablo.jgi-psf.org
    export CHROMAT_HOST_PORT=8080
    export CHROMAT_HOST_LISTEN=8080

    export TILDEN_COMMON_SERVER=goku.jgi-psf.org
    export TILDEN_SERVER=loam.jgi-psf.org
    
    # Comma separated list of all the hosts for the load-balanced environment
    export TILDEN_DEPLOYMENT_HOSTS=${TILDEN_DEPLOYMENT_HOSTS:="diablo.jgi-psf.org"}
    export TILDEN_DEPLOYMENT_PATH=${TILDEN_DEPLOYMENT_PATH:=/opt/tilden/apache-tilden-8080}
    
  elif [ "$_tilden_mode" == "HEAD" ]
  then
    export TILDEN_ENV=$_tilden_mode
    export TILDEN_PATH=/global/dna/projectdirs/PI/rqc/prod/tools/Tilden/dev_head
    export CHROMAT_HOST=diablo.jgi-psf.org
    export CHROMAT_HOST_PORT=8081
    export CHROMAT_HOST_LISTEN=8081
    
    export TILDEN_COMMON_SERVER=goku.jgi-psf.org
    export TILDEN_SERVER=loam.jgi-psf.org

    # Comma separated list of all the hosts for the load-balanced environment
    export TILDEN_DEPLOYMENT_HOSTS=${TILDEN_DEPLOYMENT_HOSTS:="diablo.jgi-psf.org"}
    export TILDEN_DEPLOYMENT_PATH=${TILDEN_DEPLOYMENT_PATH:=/opt/tilden/apache-tilden-8081}
    
  elif [ -d "$_tilden_mode" ]
  then
    export TILDEN_ENV=DEVELOPMENT-$_tilden_mode
    tag=${USER}-DEVELOPMENT
    install_path=${SCRATCH:=/scratch}
    mkdir -p "$install_path"
    
    export TILDEN_PATH=$install_path/Tilden-${tag}
    export CHROMAT_HOST=${CHROMAT_HOST:=diablo.jgi-psf.org}
    export CHROMAT_HOST_PORT=${CHROMAT_HOST_PORT:=8081}
    export CHROMAT_HOST_LISTEN=${CHROMAT_HOST_LISTEN:=8081}

    export TILDEN_COMMON_SERVER=goku.jgi-psf.org
    export TILDEN_SERVER=loam.jgi-psf.org

    alias redeploy_tilden="${TILDEN_PATH}/bin/deploy_Tilden.sh ${_tilden_mode}"

    # host to deploy to (if not diablo)
    if [ "${CHROMAT_HOST}" != 'diablo.jgi-psf.org' ]
    then
        export TILDEN_DEPLOYMENT_HOSTS=${TILDEN_DEPLOYMENT_HOSTS:=${CHROMAT_HOST}}
        export TILDEN_DEPLOYMENT_PATH=${TILDEN_DEPLOYMENT_PATH:=/scratch/apache-tilden-${CHROMAT_HOST_LISTEN}}
    fi

  else
    echo "INVALID _tilden_mode '$_tilden_mode'" 1>&2
    return -1
  fi

  if [ "${TILDEN_ENV}" != 'PRODUCTION' ]
  then
      export PS1="\nTilden ${TILDEN_ENV} ${PS1}"
  fi
  
  export CHROMAT_SERVER=${CHROMAT_HOST}
  [ "${CHROMAT_HOST_PORT}" == '80' ] || export CHROMAT_SERVER=${CHROMAT_SERVER}:${CHROMAT_HOST_PORT}
  
  [ -n "${alt_path}" ] || alt_path=${TILDEN_PATH}
  
  #export PATH=${alt_path}/bin:/jgi/tools/bin:${PATH}:/jgi/tools/454/rig/bin
  #export LD_LIBRARY_PATH=${alt_path}/lib:/jgi/tools/lib:${LD_LIBRARY_PATH}
  #export PERL5LIB=${alt_path}/lib/perl5/site_perl/5.10.1:${PERL5LIB}
  #export PYTHONPATH=${alt_path}/lib/python2.5/site-packages:${PYTHONPATH}
  export PHRED_PARAMETER_FILE="/global/dna/projectdirs/PI/rqc/prod/tools/Tilden/phredpar.dat"

  echo "Successfully established a ${TILDEN_ENV} mode for TILDEN" 1>&2

fi

#
# $Log: tilden_env.sh,v $
# Revision 1.17  2010-01-07 05:42:15  regan
# moved to newer version of perl
#
# Revision 1.16  2009-05-19 18:16:03  regan
# eliminated namespace conflicts on internal an variable (mode -> _tilden_mode)
#
# Revision 1.15  2009-02-25 18:12:46  regan
# added pi-edge2
#
# Revision 1.14  2009-02-11 19:44:16  regan
# bugfix
#
# Revision 1.13  2009-02-11 00:28:41  regan
# first pass at including apache configuration with deployment
#
# Revision 1.12  2008-11-14 22:38:38  regan
# added PHRED_PARAMETER_FILE env variable
#
# Revision 1.11  2008-07-18 22:02:53  regan
# cleaned code - replaced tabs with spaces
#
# Revision 1.10  2008-05-20 23:54:00  regan
# enclosed vars in curly braces
#
# Revision 1.9  2008-05-20 23:09:43  regan
# added /jgi/tools to the PATH & LD_LIBRARY_PATH
#
# Revision 1.8  2008-05-15 18:38:26  regan
# added 454 sff tools to path for Tilden environment
#
# Revision 1.7  2008-05-07 22:19:41  regan
# changed production web server
#
# Revision 1.6  2008-04-24 21:28:58  regan
# changed prompt for all but production
#
# Revision 1.5  2008-04-24 21:23:34  regan
# attempting to fix environment problems
#
# Revision 1.4  2008-04-23 00:02:55  regan
# faster development deployment
#
# Revision 1.3  2008-04-22 19:13:16  regan
# added better development support
#
# Revision 1.2  2008-04-18 22:21:09  regan
# bugfix
#
# Revision 1.1  2008-04-18 22:11:54  regan
# first pass at establishing an environment
#
#
