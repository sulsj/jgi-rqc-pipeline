#!/usr/bin/env python
# -*- coding: utf-8 -*-


'''
RQC Metrics reporting
look up raw read, asm, filtering and alignment, clrs metrics
export to xls, json, web

To do:
- spike-in expected field (yes/no)
- problem with timeouts



limitations:
- illumina data only
- its data for library type
- only *.fastq.gz data
- only libraries, not parent or unknown seq units


Created: Jan 11 2016
sulsj (ssul@lbl.gov)


Revisions
    
    1-11-2016: Creating a separate tool to search RQC metrics
 

'''

import os
import sys
import MySQLdb
import time
import json
import re
import argparse

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from db_access import jgi_connect_db
import rqc_tools as rqctools
from rqc_system_constants import RQCPipelineTypeConstants



class RQCMetricsSearch():
    ## use get params instead
    ## https://rqc-1.jgi-psf.org/api/metrics/results/?date1=04-22-2014&date2=05-22-2014
    def get_results(self, args, kwargs):
        return self.post_results(args, kwargs)
    
    def post_results(self, args, kwargs):
        myJson = {}
        
        defaultPageSize = 100 # only for html output
        page = 1 # 1..
        
        startDate = None
        endDate = None
        pipelineList = []
        libTypeList = []
        projTypeList = []
        outputFormat = 2 ## csv only
        libFilter = None
        
        if args:
            if args.date1:
                startDate = time.strftime("%Y-%m-%d", time.strptime(args.date1, "%m-%d-%Y"))
            
            if args.date2:
                endDate = time.strftime("%Y-%m-%d", time.strptime(args.date2, "%m-%d-%Y"))
            
            if args.libraryTypeList:
                libTypeList = args.libraryTypeList.split(",")
            
            if args.projectTypeList:
                projTypeList = args.projectTypeList.split(",")
    
        if kwargs:
            if 'date1' in kwargs:
                if kwargs['date1']:
                    startDate = time.strftime("%Y-%m-%d", time.strptime(kwargs['date1'], "%m-%d-%Y"))
            
            if 'date2' in kwargs:
                if kwargs['date2']:
                    endDate = time.strftime("%Y-%m-%d", time.strptime(kwargs['date2'], "%m-%d-%Y"))
            
            if 'pipelineList' in kwargs:
                pipelineList = kwargs['pipelineList'].split(",")
            
            if 'libTypeList' in kwargs:
                libTypeList = kwargs['libTypeList'].split(",")
            
            if 'projTypeList' in kwargs:
                projTypeList = kwargs['projTypeList'].split(",")
            
            if 'outputFormat' in kwargs:
                outputFormat = int(kwargs['outputFormat'])
            
            if 'libFilter' in kwargs:
                libFilter = kwargs['libFilter']
            
            if 'page' in kwargs:
                page = int(kwargs['page'])
                if page < 1: page = 1

        
        ## mapping of libTypeList to lib_type, yes it should be an attribute of the class passed into the metric_search page ...
        ## should come from lib_info.lib_protocol field
        libTypeDict = {
            1 : "CLRS",
            2 : "iTag",
            3 : "Low Input (DNA)",
            4 : "Low Input (RNA)",
            5 : "Regular (DNA)",
            6 : "Regular (RNA)",
            7 : "smRNA",
            8 : "Tight Insert",
            9 : "Ultra-Low Input (DNA)",
            10: "Ultra-Low Input (RNA)",
        }
        
        projTypeDict = {
            1 : "Fungal",
            2 : "Genomic Tech",
            3 : "JGI Science",
            4 : "Metagenome",
            5 : "Microbial",
            6 : "Plant",
            7 : "Synthetic Biology",
        }
        
        ## whether to look up this pipeline or not
        readFlag = False
        asmFlag = False
        filterFlag = False
        alignFlag = False
        clrsFlag = False
        
        for i in pipelineList:
            if i.isdigit():
                i = int(i)
                if i == 0:
                    readFlag = True
                    asmFlag = True
                    filterFlag = True
                    alignFlag = True
                    clrsFlag = True
                elif i == 3:
                    readFlag = True
                elif i == 4:
                    asmFlag = True
                elif i == 5:
                    filterFlag = True
                elif i == 6:
                    alignFlag = True
                elif i == 15:
                    clrsFlag = True
        
        ## overwrite settings using args
        if args.pipelineList:
            for p in args.pipelineList.split(','):
                if p == "read": readFlag = True
                elif p == "asm": asmFlag = True
                elif p == "filter": filterFlag = True
                elif p == "align": alignFlag = True
                elif p == "clrs": clrsFlag = True
                else:
                    print "Unknown pipeline type!"
                    sys.exit(1)
        
        else:
            readFlag = True
            asmFlag = True
            filterFlag = True
            alignFlag = True
            clrsFlag = True


        db = self.get_db()
        sth = db.cursor(MySQLdb.cursors.DictCursor)
        sth2 = db.cursor(MySQLdb.cursors.DictCursor)
    
        ## fetch seq_units, library_info data for seq_units that match the criteria
        ## - not pooled or unknown, Illumina (9) only
        sqlParams = []
        orderBy = "run_date"
        
        sql = """
select
    s.seq_unit_id,
    s.library_name,
    s.seq_unit_name,
    s.run_date,
    s.segment_id,
    s.raw_reads_count,
    s.filter_reads_count,
    s.sdm_raw_reads_count,
    s.sdm_raw_base_count,
    s.run_configuration,
    s.run_type,
    s.instrument_type,
    (select s2.library_name from seq_units as s2 where s.run_id = s2.run_id and s.run_section = s2.run_section and s2.is_multiplex = 1 limit 0,1) as pool_name,
    l.index_name,
    l.index_sequence,
    l.ncbi_organism_name,
    l.ncbi_tax_id,
    l.sample_number,
    l.size_estimated_kb,
    l.account_jgi_sci_prog,
    l.account_jgi_user_prog,
    l.account_purpose,
    l.account_year,
    l.seq_proj_id,
    l.seq_proj_name,
    l.library_protocol,
    l.sow_item_id,
    l.sow_item_type,
    l.tight_insert_flag,
    l.overlapping_reads_flag,
    l.target_fragment_size,
    l.rrna_depletion,
    l.poly_a_selection,
    l.degree_of_pooling,
    l.seq_prod_name,
    l.logical_amount_units,
    l.total_sow_item_log_amt_targeted,
    l.total_sow_item_log_amt_completed,
    l.actual_insert_size,
    l.actual_fragment_size,
    l.material_type,
    l.sample_name,
    l.plate_id,
    l.plate_name,
    l.well_id,
    l.kingdom,
    l.genus,
    l.species,
    l.strain,
    l.isolate
from seq_units s
left join library_info l on s.rqc_library_id = l.library_id
where
    s.is_multiplex = 0 and s.model_id = 9 and s.library_name not in ('unknown', 'phix')
        """
    
    #sql += " and s.seq_unit_name in ('7893.7.87008.GTGGCC.fastq.gz', '7893.2.87009.CGGAAT.fastq.gz', '7893.2.87009.CCAACA.fastq.gz')"
        
        if startDate:
            sql += " and s.run_date >= %s"
            sqlParams.append(startDate)
        
        if endDate:
            sql += " and s.run_date <= %s"
            sqlParams.append(endDate)
        
        if projTypeList:
            sqlProjType = ""
            for ptype in projTypeList:
                if ptype.isdigit():
                    ptype = int(ptype)
                    if ptype > 0:
                        sqlProjType += "%s,"
                        sqlParams.append(projTypeDict[ptype])
            
            if sqlProjType:
                sql += " and l.account_jgi_sci_prog in ("+sqlProjType[:-1]+")"
        
        if libTypeList:
            sqlLibType = ""
            
            for ltype in libTypeList:
                if ltype.isdigit():
                    ltype = int(ltype)
                    if ltype > 0:
                        sqlLibType += "%s,"
                        sqlParams.append(libTypeDict[ltype])
            
            if sqlLibType:
                sql += " and l.library_protocol in ("+sqlLibType[:-1]+")"
        
        if libFilter:
            libFilter = re.sub("[^A-Za-z0-9\.\%\*]+", ",", libFilter)
            sqlArr = []
            
            arr = libFilter.split(",")
            orderBy = "seq_unit"
            
            for l in arr:
                ## improve matching  ....
                l = l.replace("*", "%")
                if str(l).isdigit():
                    if int(l) < 99999:
                        
                        sqlArr.append("s.run_id = %s")
                        sqlParams.append(l)
                    else:
                        ## spid
                        sqlArr.append("l.seq_proj_id = %s")
                        sqlParams.append(l)
                
                elif str(l).find(".") > -1:
                    sqlArr.append("s.seq_unit_name like %s")
                    l = l + "%"
                    sqlParams.append(l)
            
                else:
                    sqlArr.append("s.library_name like %s")
                    sqlParams.append(l)
                    orderBy = "lib_name"
            
            sql += " and (" + " or ".join(sqlArr)+")"

        
        if orderBy == "seq_unit":
            sql += " order by s.seq_unit_id"
        elif orderBy == "lib_name":
            sql += " order by s.library_name"
        else:
            sql += " order by s.run_date"

        
        if outputFormat == 0 or outputFormat == 1:
            pageLimit = (page - 1) * defaultPageSize
            sql += " limit %s, %s" % (pageLimit, defaultPageSize)
            #sql += " limit 0,1"
        else:
            #sql += " limit 0,100" # temp
            #sql += " limit 0,1"
            pass
        
        
        jsonList = []
        
        ## These should match the sql query fields and this is the order to show them on the csv, web report
        seq_unit_key_list = ['seq_unit_id', 'library_name', 'pool_name', 'seq_unit_name', 'run_date', 'segment_id', 'raw_reads_count', 'filter_reads_count', 'sdm_raw_reads_count',
                 'sdm_raw_base_count', 'run_configuration', 'run_type', 'instrument_type', 'index_name', 'index_sequence', 'ncbi_organism_name', 'ncbi_tax_id',
                 'sample_number', 'size_estimated_kb', 'account_jgi_sci_prog', 'account_jgi_user_prog', 'account_purpose', 'account_year', 'seq_proj_id', 'seq_proj_name',
                 'library_protocol', 'sow_item_id', 'sow_item_type', 'tight_insert_flag', 'overlapping_reads_flag', 'target_fragment_size', 'rrna_depletion', 'poly_a_selection',
                 'degree_of_pooling', 'seq_prod_name', 'logical_amount_units', 'total_sow_item_log_amt_targeted',
                 'total_sow_item_log_amt_completed', 'actual_insert_size', 'actual_fragment_size', 'material_type', 'sample_name', 'plate_id', 'plate_name',
                 'well_id', 'kingdom', 'genus', 'species', 'strain', 'isolate'
                ]
        
        ## all other key lists generated on the fly
        readKeyDict = {}
        asmKeyDict = {}
        filterKeyDict = {}
        alignKeyDict = {}
        clrsKeyDict = {}
        
        sth.execute(sql, (sqlParams))
        rowCount = int(sth.rowcount)
                
        rows = sth.fetchall()
        
        for rs in rows:
            myJson = {"seq_unit_name" : rs['seq_unit_name']}
            
            ## need to convert to string for JSON library
            rs['run_date'] = str(rs['run_date'])
            
            myJson['seq_unit'] = rs
            
            if readFlag == True:
                queue_id, seq_unit_name, library_name, run_folder = rqctools.get_queue_id(sth2, rs['seq_unit_name'], RQCPipelineTypeConstants.READ_QC)
                
                if queue_id != 0:
                    readJson = {}
                    readJson = rqctools.get_pipeline_stats(sth2, queue_id = queue_id)
                    ## remove keys not needed ...
                    
                    if "read_n_pattern" in readJson:
                        readJson.pop("read_n_pattern")
                    
                    myJson['read_qc'] = readJson
                    
                    for key in readJson:
                        if len(key) > 3:
                            
                            if key in readKeyDict:
                                readKeyDict[key] += 1
                            else:
                                readKeyDict[key] = 1

            
            if asmFlag == True:
                asmJson = {}
                queue_id, seq_unit_name, library_name, run_folder = rqctools.get_queue_id(sth2, rs['seq_unit_name'], RQCPipelineTypeConstants.ASSEMBLY_QC)
                
                if queue_id != 0:
                    asmJson = rqctools.get_pipeline_stats(sth2, queue_id = queue_id)
                
                ## remove keys not needed ...
                delList = []
                
                for key in asmJson:
                    if key.startswith("assembly_top_hits_of"):
                        delList.append(key)
                    if key.startswith("assembly_taxlist_file_of"):
                        delList.append(key)
                    if key.startswith("assembly_tophit_file_of"):
                        delList.append(key)
                    if key.startswith("assembly_parsed_file_of"):
                        delList.append(key)
                    if key.startswith("assembly_depth_hist_plot"):
                        delList.append(key)
                    if key.startswith("assembly_depth_text_hist"):
                        delList.append(key)
                    if key.startswith("contig_level"):
                        delList.append(key)
                    if key in ("assembly_gc_plot", "assembly_gc_text_hist", "assembly_top_5_contig_file"):
                        delList.append(key)
                
                for key in delList:
                    asmJson.pop(key)
                
                myJson['asm_qc'] = asmJson
                
                for key in asmJson:
                    if key in asmKeyDict:
                        asmKeyDict[key] += 1
                    else:
                        asmKeyDict[key] = 1
            
            if filterFlag == True:
                queue_id, seq_unit_name, library_name, run_folder = rqctools.get_queue_id(sth2, rs['seq_unit_name'], RQCPipelineTypeConstants.FILTERING)
                if queue_id != 0:
                    filterJson = {}
                    filterJson = rqctools.get_pipeline_stats(sth2, queue_id = queue_id)
                    
                    myJson['filter'] = filterJson
                    
                    for key in filterJson:
                        if key in filterKeyDict:
                            filterKeyDict[key] += 1
                        else:
                            filterKeyDict[key] = 1

            
            if alignFlag == True:
                queue_id, seq_unit_name, library_name, run_folder = rqctools.get_queue_id(sth2, rs['seq_unit_name'], RQCPipelineTypeConstants.ALIGNMENT)
                if queue_id != 0:
                    alignJson = {}
                    alignJson = rqctools.get_pipeline_stats(sth2, queue_id = queue_id)
                    
                    if "genome_reference_file" in alignJson:
                        alignJson['genome_reference_file'] = rqctools.clean_rqc_path(alignJson['genome_reference_file'])
                    if "transcriptome_reference_file" in alignJson:
                        alignJson['transcriptome_reference_file'] = rqctools.clean_rqc_path(alignJson['transcriptome_reference_file'])
                    
                    myJson['align'] = alignJson
                    
                    for key in alignJson:
                        if key in alignKeyDict:
                            alignKeyDict[key] += 1
                        else:
                            alignKeyDict[key] = 1

            
            if clrsFlag == True:
                queue_id, seq_unit_name, library_name, run_folder = rqctools.get_queue_id(sth2, rs['seq_unit_name'], RQCPipelineTypeConstants.CLRS_REDUNDANCY)
                if queue_id != 0:
                    clrsJson = {}
                    clrsJson = rqctools.get_pipeline_stats(sth2, queue_id = queue_id)
                    
                    myJson['clrs'] = clrsJson
                    
                    for key in clrsJson:
                        if key in clrsKeyDict:
                            clrsKeyDict[key] += 1
                        else:
                            clrsKeyDict[key] = 1

            
            jsonList.append(myJson)
        
        db.close()
        
        ## order found keys for each section
        readKeyList = []
        asmKeyList = []
        filterKeyList = []
        alignKeyList = []
        clrsKeyList = []
        
        for key in sorted(readKeyDict):
            readKeyList.append(key)
        
        for key in sorted(asmKeyDict):
            asmKeyList.append(key)
        
        for key in sorted(filterKeyDict):
            filterKeyList.append(key)
        
        for key in sorted(alignKeyDict):
            alignKeyList.append(key)
        
        for key in sorted(clrsKeyDict):
            clrsKeyList.append(key)

        
        seqUnitDict = {
            "seq_unit" : seq_unit_key_list,
            "read_qc" : readKeyList,
            "asm_qc" : asmKeyList,
            "filter" : filterKeyList,
            "align" : alignKeyList,
            "clrs" : clrsKeyList
        }

        
        myLinkArr = []
        if kwargs:
            for key in kwargs:
                if key != "page":
                    if kwargs[key]:
                        myLinkArr.append("%s=%s" % (key, kwargs[key]))
        
        myLink = "&".join(myLinkArr)
        myJson = jsonList
        
        if outputFormat == 2:
            ## Excel/csv
            myBuffer = self.__make_metrics_csv(myJson, seqUnitDict)
            
            # write to disk ...
            #csv_file = "dump.csv"
            #fh = open(csv_file, "w")
            #fh.write(myBuffer)
            #fh.close()
            
            return myBuffer
        
    
    ## generates csv for XLS download
    def __make_metrics_csv(self, myJson, seqUnitDict):
        myCsvBuffer = "Spread your wings" ## Queen forever!
        csvList = []
        headerList = []
        
        sectionList = ['seq_unit', 'read_qc', 'asm_qc', 'filter', 'align', 'clrs']
        
        for section in sectionList:
            myList = seqUnitDict[section]
            for key in myList:
                # headerList.append(str(key).title().replace("_"," "))
                headerList.append(str(key).title())
        
        header = ",".join(headerList)
        
        for myData in myJson:
            
            rowList = []
            
            for section in sectionList:
                myList = seqUnitDict[section]
                
                if section in myData:                    
                    for key in myList:
                        val = ""
                        
                        if key in myData[section]:
                            key = str(key)
                            val = myData[section][key]

                            if val:
                                try:
                                    val = re.sub("[^A-Za-z0-9\-\_\.\+\ \:\/\$]+", "", str(val))
                                except UnicodeEncodeError:
                                    #print "code error: ", val
                                    ##UnicodeEncodeError: 'ascii' codec can't encode character u'\xae' in position 36: ordinal not in range(128)
                                    val = ""
                                    pass
                            else:
                                val = ""
                        
                        rowList.append(val)
                else:
                    for key in myList:
                        val = ""
                        rowList.append(val)
            
            csvList.append(",".join(rowList))
        
        
        myCsvBuffer = "%s\n%s" % (header, "\n".join(csvList))
        
        return myCsvBuffer
    
    
    def get_db(self):
        return jgi_connect_db("rqc")
        

if __name__ == '__main__':
    desc = "RQC Metrics Search"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-d1", "--date1", dest="date1", help="start date (format: MM-DD-YYYY), ex) -d1 12-1-2015", required=True)
    parser.add_argument("-d2", "--date2", dest="date2", help="end date (format: MM-DD-YYYY), ex) -d2 12-31-2015", required=True)
    parser.add_argument("-p", "--pipeline", dest="pipelineList", help="comma separated pipeline list: (read, asm, align, filter, clrs), ex) -p read,asm,filter", required=False)
    parser.add_argument("-l", "--library", dest="libraryTypeList", help="library type list: (CLRS=1, iTag=2, Low Input (DNA)=3, Low Input (RNA)=4, Regular (DNA)=5, Regular (RNA)=6, smRNA-7, Tight Insert=8, Ultra-Low Input (DNA)=9, Ultra-Low Input(RNA)=10), ex) -l 5,6", required=False)
    parser.add_argument("-t", "--project", dest="projectTypeList", help="project type list: (Fungal=1, Genomic Tech=2, JGI Science=3, Metagenome=4, Microbial=5, Plant=6), ex) -t 1,2,3,4", required=False)
    
    options = parser.parse_args()
    
    ms = RQCMetricsSearch()
    print ms.get_results(options, None)
    
    ## Header info
    ## Seq Unit Id,Library Name,Pool Name,Seq Unit Name,Run Date,Segment Id,Raw Reads Count,Filter Reads Count,Sdm Raw Reads Count,Sdm Raw Base Count,Run Configuration,Run Type,Instrument Type,Index Name,Index Sequence,Ncbi Organism Name,Ncbi Tax Id,Sample Number,Size Estimated Kb,Account Jgi Sci Prog,Account Jgi User Prog,Account Purpose,Account Year,Seq Proj Id,Seq Proj Name,Library Protocol,Sow Item Id,Sow Item Type,Tight Insert Flag,Overlapping Reads Flag,Target Fragment Size,Rrna Depletion,Poly A Selection,Degree Of Pooling,Seq Prod Name,Logical Amount Units,Total Sow Item Log Amt Targeted,Total Sow Item Log Amt Completed,Actual Insert Size,Actual Fragment Size,Material Type,Sample Name,Plate Id,Plate Name,Well Id,Kingdom,Genus,Species,Strain,Isolate,Base C10,Base C15,Base C20,Base C25,Base C30,Base C5,Base Q10,Base Q15,Base Q20,Base Q25,Base Q30,Base Q5,Gc Divergence Coeff R1 At,Gc Divergence Coeff R1 Atcg,Gc Divergence Coeff R1 Cg,Gc Divergence Coeff R2 At,Gc Divergence Coeff R2 Atcg,Gc Divergence Coeff R2 Cg,Illumina Read Insert Size 10Th Perc,Illumina Read Insert Size 25Th Perc,Illumina Read Insert Size 50Th Perc,Illumina Read Insert Size 75Th Perc,Illumina Read Insert Size 90Th Perc,Illumina Read Insert Size Ambiguous Num,Illumina Read Insert Size Ambiguous Perc,Illumina Read Insert Size Avg Insert,Illumina Read Insert Size Insert Range End,Illumina Read Insert Size Insert Range Start,Illumina Read Insert Size Joined Num,Illumina Read Insert Size Joined Perc,Illumina Read Insert Size Mode Insert,Illumina Read Insert Size No Solution Num,Illumina Read Insert Size No Solution Perc,Illumina Read Insert Size Num Reads,Illumina Read Insert Size Std Insert,Illumina Read Insert Size Too Short Num,Illumina Read Insert Size Too Short Perc,Illumina Read Insert Size Total Time,Illumina Read Percent Contamination Artifact,Illumina Read Percent Contamination Artifact 50Bp,Illumina Read Percent Contamination Artifact 50Bp Seal,Illumina Read Percent Contamination Artifact Seal,Illumina Read Percent Contamination Contaminants,Illumina Read Percent Contamination Contaminants Seal,Illumina Read Percent Contamination Dna Spikein,Illumina Read Percent Contamination Dna Spikein Seal,Illumina Read Percent Contamination Ecoli Combined,Illumina Read Percent Contamination Ecoli Combined Seal,Illumina Read Percent Contamination Fosmid,Illumina Read Percent Contamination Fosmid Seal,Illumina Read Percent Contamination Mitochondrion,Illumina Read Percent Contamination Mitochondrion Seal,Illumina Read Percent Contamination Phix,Illumina Read Percent Contamination Phix Seal,Illumina Read Percent Contamination Plastid,Illumina Read Percent Contamination Plastid Seal,Illumina Read Percent Contamination Rna Spikein,Illumina Read Percent Contamination Rna Spikein Seal,Illumina Read Percent Contamination Rrna,Illumina Read Percent Contamination Rrna Seal,Illumina Read Sciclone Dna Count Matched,Illumina Read Sciclone Dna Count Matched Perc,Illumina Read Sciclone Dna Count Total,Illumina Too Small Num Reads,Motif Freq Cnt 1,Motif Freq Num,Motif Freq Patt 1,Motif Freq Perc 1,Overall Bases Q Score Mean,Overall Bases Q Score Std,Q30 Bases Q Score Mean,Q30 Bases Q Score Std,Read 20Mer Percentage Random Mers,Read 20Mer Percentage Starting Mers,Read 20Mer Sample Size,Read Base Count,Read Bwa Aligned,Read Bwa Aligned Duplicate,Read Bwa Aligned Duplicate Percent,Read Count,Read Gc Mean,Read Gc Median,Read Gc Mode,Read Gc Std,Read Length 1,Read Length 2,Read Matching Hits Of Nt,Read Matching Hits Of Refseq Archaea,Read Matching Hits Of Refseq Bacteria,Read N Frequence,Read Q10,Read Q15,Read Q20,Read Q20 Read1,Read Q20 Read2,Read Q25,Read Q30,Read Q5,Read Tax Species Of Nt,Read Tax Species Of Refseq Archaea,Read Tax Species Of Refseq Bacteria,Read Top Hits Of Nt,Read Top Hits Of Refseq Archaea,Read Top Hits Of Refseq Bacteria,Reads Number

## EOF