#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Update loading concentration using new table
- prints the sql statements, you still need to import them into the db
- part of the rqc_system/sh/daily_update.sh

July 12 2017



bug - some runs think the loading concentration is different although it is the same, e.g. run 11273.1
- some runs missing because offline runs (M####, N####)
"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
import MySQLdb

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from db_access import jgi_connect_db


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program



if __name__ == "__main__":


    its_db = jgi_connect_db("dw")
    sth_its = its_db.cursor()

    db = jgi_connect_db("rqc")

    sth = db.cursor(MySQLdb.cursors.DictCursor)
    sth2 = db.cursor(MySQLdb.cursors.DictCursor)


    # using seq_prod_name instead of actual_seq_prod_name because if missing from genealogy it won't have the actual_seq_prod_name record
    sql = """
select s.seq_unit_name, s.library_name, s.gls_physical_run_unit_id, s.instrument_type, s.run_id, s.gls_physical_run_unit_id,
ils.lane_stat_id, ils.run_id, ils.lane_number ln, ils.loading_concentration lc, ils.material_type mt
from seq_units s
inner join illumina_lane_stats ils on s.run_id = ils.run_id and s.run_section = ils.lane_number
where
    s.is_multiplex = 1
    and s.dt_created > date_sub(now(), interval 6 month)
    """
    # using 2018-01-01 because its July and we have everything from 2017
    
    #sql += " and s.run_id = 11273"
    #sql += " and s.run_id = 11043"
    #sql += " limit 0,50"

    #select run_id, lane_number, lane_stat_id from illumina_lane_stats where loading_concentration is null;

    sth.execute(sql)
    row_cnt = int(sth.rowcount)
    sys.stderr.write("-- checking %s rows from illumina_lane_stats\n" % row_cnt)
    for _ in range(row_cnt):
        rs = sth.fetchone()

        lc = 0.0
        lc2 = 0.0
        lib2 = ""

        # artifact_name vs pru_id returns different results
        # 11721.5.216469.fastq.gz: 15.0 != 11.5
        # trust second query over the first one
        #print rs['library_name'], rs['gls_physical_run_unit_id']
        sql = "select artifact_name, library_conversion_factor from uss.dt_physical_run_unit where artifact_name = :lib"
        sth_its.execute(sql, lib = rs['library_name'])
        for rs_its in sth_its:
            lc2 = rs_its[1]
            lib2 = rs_its[0]

        sql = "select artifact_name, library_conversion_factor from uss.dt_physical_run_unit where clarity_artifact_limsid = :pru_id"
        sth_its.execute(sql, pru_id = rs['gls_physical_run_unit_id'])
        for rs_its in sth_its:
            lc = rs_its[1]
            lib2 = rs_its[0]


        #if lc2 != lc:
        #    print "%s: %s != %s" % (rs['seq_unit_name'], lc, lc2)
        #if not lc:
        #    lc = 0.0

        if lc == 0.0 and lc2 > 0.0:
            lc = lc2


        my_lc = rs['lc']
        if not my_lc:
            my_lc = 0.0


        # even though lc looks like my_lc, python might still think they are different

        d = 0.0

        try:
            d = max(lc, my_lc) - min(lc, my_lc)
        except:
            sys.stderr.write("-- BAD %s: %s %s\n" % (rs['seq_unit_name'], my_lc, lc))

        if d > 0.001 and lc > 0:
            sys.stderr.write("-- %s: lc = %s, new lc = %s\n" % (rs['seq_unit_name'], my_lc, lc))
            #print type(lc)
            #print type(my_lc)

            if rs['instrument_type'].lower() == "nextseq":
                sql = "update illumina_lane_stats set loading_concentration = %s, material_type = '%s' where run_id = %s;" % (lc, rs['mt'], rs['run_id'])
                print sql
            else:
                sql = "update illumina_lane_stats set loading_concentration = %s where lane_stat_id = %s;" % (lc, rs['lane_stat_id'])
                print sql
        #else:
        #    print "lc: %s, my_lc: %s" % (lc, my_lc)


    # how many lc's are null?  98 as of July 12, 2017
    sql = """
select ils.lane_stat_id, ils.run_id, ils.lane_number, s.seq_unit_name, s.library_name, s.instrument_type
from illumina_lane_stats ils
inner join seq_units s on s.run_id = ils.run_id and s.is_multiplex = 1
where
    ils.loading_concentration is null
    and s.dt_created > date_sub(now(), interval 6 month)
    """

    sth.execute(sql)
    row_cnt = int(sth.rowcount)
    sys.stderr.write("-- checking %s rows from illumina_lane_stats where loading_concentration is null\n" % row_cnt)
    for _ in range(row_cnt):
        rs = sth.fetchone()

        if rs['instrument_type'].lower() in ("nextseq", "hiseq-2500 rapid v2"):
            if rs['lane_number'] > 1:
                sql = "select loading_concentration, material_type from illumina_lane_stats where run_id = %s and lane_number = 1"
                sth2.execute(sql, (rs['run_id']))
                rs2 = sth2.fetchone()
                if rs2:
                    if rs2['loading_concentration']:
                        sql = "update illumina_lane_stats set loading_concentration = %s, material_type = '%s' where lane_stat_id = %s;" % (rs2['loading_concentration'], rs2['material_type'], rs['lane_stat_id'])
                        print sql


    db.close()
    #its_db.close()
    sys.exit(0)

'''


'''
