#!/usr/bin/env python
# -*- coding: utf-8 -*-


'''
run_blastplus_taxserver: a wrapper for NCBI Blast+ blastn

Created: Feb 3 2016
sulsj (ssul@lbl.gov)


Description
= Purpose: to replace Jigsaw's run_blast.pl
= Localize Blast database files
  - check /scratch/blastdb/global/dna/shared/rqc/ref_databases/ncbi/CURRENT first
= Default blast options
  - can be overridden by user-specified options


Dependencies
- rqc_utility.py
- os_utility.py
- NCBI tax db files (nodes.dmp, names.dmp, and gi_taxid_nucl.dmp.gz) for creating lineage info


Revisions
    02032016 0.0.9: Created
    02102016 0.1.0: Changed gi2taxid from dict pickled to sqlite for possible memory issue

    02172016 1.0.0: Tested
    02182016 1.1.0: PyCogent tested

    02242016 1.2.0: Reverted to sqlite from pycogent b/c pycogent is not supported on some Genepool compute nodes
    02252016 1.2.1: Updated Blast output sorting criteria
    04142016 1.2.2: Fixed tophit sort params

    04222016 1 3.0: Added outfmt 6,7,10 support
    04282016 1.3.1: Added organism name to subject field (-s option)
    05142016 1.3.2: Fixed organism name adding part (lineage checking)
    06032016 1.3.3: Removed creating symlink from localize_dir; Skip localization if the target dir exists already.
    06032016 1.3.4: Added header line to blast output (.parsed)
    07062016 1.3.5: Removed get_organism_name; Added full lineage infoto the subject field in parsed, tophit, and top100hit instead of species name.

    07082016 1.4.0: Changed blast options: num_alignments=100, num_threads=32
    07112016 1.5.0: Added salltitles for LSSURef, LSURef, SSURef, Collab16s, JGIContaminants which do not have gi
    07192016 1.5.1: Set the default num_threads back to 8

    07212016 1.6.0: Added non-gi subject title support; Updated taxlist generation to handle non-gi subject title; Added nr, refseq.fungi, refseq.mitochondrion, refseq.plant, refseq.plasmid, refseq.plastid, refseq.viral
    07262016 1.6.1: Set num_alignments=5 for blastn

    08102016 1.6.2: Minor bug fixes
    08232016 1.6.3: Fixed taxlist file creation for custom reference db

    08312016 1.7.0: Rewrote localize_dir2()
    09152016 1.7.1: Updated to cope with the case when gi2taxid fails (RQCSUPPORT-813)
    09162016 1.7.2: Removed redundant gi info from salltitles before adding lineage info to subject field
    09232016 1.7.3: Replaced ':' and ';' with '_' in the subject string
    10212016 1.7.4: Added user specified output file name
    11212016 1.7.5: Changed blastn version to 2.5.0
    12122016 1.7.6: Changed default taxonomy dump location

    12192016 2.0.0: Updated to use tax server
    01182017 2.0.1: Set to taxonomy server at taxonomy.jgi-psf.org

    05152017 2.1.0: Used both gi and accession number
    05242017 2.1.1: Updated to try to search header string from the taxonomy server if there is no gi or accession

    08022017 2.2.0: Enabled burst buffer on cori
    08032017 2.2.1: Added "-f" for fungal pipeline (Append salltitles to subject field and skip collecting lineage info)

    08072017 2.3.0: Updated to use persistent burst buffer on Cori
    08072017 2.3.1: Updated to compute and add pctQuery in taxlist output
    08152017 2.3.2: Fixed a bug in addSalltitles
    08232017 2.3.3: Updated to try to search taxonomy server with fasta header if there is no gi or accession
    08232017 2.3.4: Updated to cope with header string starting with number in get_taxa_by_gi_or_acc()
    10132017 2.3.5: Upgraded blast+ to 2.7.0 on denovo and cori
    10232017 2.3.6: Changed the delimiter from space to tab in tophit
    10262017 2.3.7: Added green-genes, collab16s, ssu, lsu and others to db options
    11152017 2.3.8: Removed skipping db localization on Denovo


'''

import os
import sys
import argparse
import platform
import json
from tempfile import mkstemp
import re ## for pctQuery
import pprint

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

## jgirqc module for qc_user's conda env
sys.path.insert(0, '/global/homes/q/qc_user/miniconda/miniconda2/lib/python2.7/site-packages/jgirqc/lib')

from common import get_logger
from rqc_utility import safe_basename, safe_dirname, localize_dir2
from rqc_constants import RQCExitCodes, RQCReferenceDatabases
from os_utility import get_tool_path, run_sh_command, make_dir_p, change_mod, back_ticks, remove_file, move_file

## jgirqc module for qc_user's conda env
# from jgirqc import common
# from jgirqc import rqc_utility
# from jgirqc import rqc_constants
# from jgirqc import os_utility

## Globals
VERSION = "2.3.8"
LOG_LEVEL = "DEBUG"
SCRIPT_NAME = __file__
# NCBI_TAXONOMY_DB_PATH = "/global/projectb/sandbox/rqc/qcdb/taxonomy/tax/DEFAULT/"
# NCBI_TAXONOMY_DB_PATH = "/global/dna/shared/rqc/ref_databases/ncbi/CURRENT/tax"
NERSC_HOST = os.environ.get('NERSC_HOST', 'unknown')

## Outfmt
##
## ex)
## MISEQ04:278:000000000-ALA09:1:1101:5246:13068    gi|851289456|ref|NZ_AXDV01000017.1| 279 9e-74   151 100.00  1   151 151 3509    3659    5516    plus    N/A
## MISEQ04:278:000000000-ALA09:1:1101:5638:14198    gi|851289456|ref|NZ_AXDV01000017.1| 279 9e-74   151 100.00  1   151 151 2881    3031    5516    plus    N/A
##
## ex2)
## MISEQ04:278:000000000-ALA09:1:1101:15574:1382    gi|320006635|gb|CP002475.1| 274 3e-70   151 99.34   1   151 151 4576476 4576626 7337497 591167
## MISEQ04:278:000000000-ALA09:1:1101:15574:1382    gi|478743931|gb|CP003990.1| 268 1e-68   151 98.68   1   151 151 2895762 2895612 7526197 1265601
##
## ex3) with salltitles
## LSSURef, LSURef, SSURef, Collab16s, JGIContaminants do not have gis. Instead, salltitles will contain the lineage info.
##
## vs. LSSURef
## NODE_1_length_1097_cov_5.158615  AHJR01000001.1122280.1125317    2108    0.0 1159    99.482  11159   1159    1716    558 3038    N/A AHJR01000001.1122280.1125317_Archaea;Crenarchaeota;Thermoprotei;Sulfolobales;Sulfolobaceae;Sulfolobus;Sulfolobus_islandicus_M.16.43
##
## vs. collab16s
## NODE_10_length_278_cov_2.492806  38238_Sulfolobus_solfataricus_98/2_W2_resequencing_LIBS_OYPP_POOLS_OPXT 440 2.26e-124   238 100.000 103 340 340 1499    1262    1499    N/A 38238_Sulfolobus_solfataricus_98/2_W2_resequencing_LIBS_OYPP_POOLS_OPXT
##
## New blast+ options for taxonomy info
## Ref) http://blastedbio.blogspot.com/2012/05/blast-tabular-missing-descriptions.html
## staxids sscinames scomnames sblastnames sskingdom
## - staxids means unique Subject Taxonomy ID(s), separated by a ';' (in numerical order)
## - sskingdoms means unique Subject Super Kingdom(s), separated by a ';' (in alphabetical order)
##
## Ref) https://www.biostars.org/p/76551/
## taxdb (ftp://ftp.ncbi.nlm.nih.gov/blast/db/taxdb.tar.gz) should be found from BLASTDB
##
## Ref) http://www.verdantforce.com/2014/12/building-blast-databases-with-taxonomy.html
##
## This works only for nt
## $ export BLAST DB=$BLASTDB:/global/projectb/scratch/sulsj/2016.02.03-run_blastn_py-test/
## $ blastn -query 10184.1.150307.ATGTC.fasta -db /global/dna/shared/rqc/ref_databases/ncbi/CURRENT/nt/nt -num_threads 8 -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids'
##
## Added stitle
## Ref) http://blastedbio.blogspot.com/2012/05/blast-tabular-missing-descriptions.html
## Removed qstrand and sstrandq
##
DEFAULT_OUTFMT = " -outfmt '6 qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles' "
DEFAULT_BLAST_OPTIONS = " -evalue 1e-30 -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -num_alignments 5 %s " % (DEFAULT_OUTFMT)


"""
Run blastn and return blast output in tabular format
"""
def run_blastn(queryFile, dbFile, blastOptions, outDir, customOutputName, log):
    fastaFileBasename, _ = safe_basename(queryFile, log)
    dbFileBasename, _ = safe_basename(dbFile, log)
    blastOutDir = outDir
    make_dir_p(blastOutDir)

    if customOutputName:
        blastOutFile = os.path.join(blastOutDir, customOutputName + ".parsed")
    else:
        blastOutFile = os.path.join(blastOutDir, "megablast." + fastaFileBasename + ".vs." + dbFileBasename + ".parsed")

    # blastnCmd = "module unload gcc; module load gcc/4.9.2; module unload blast+; module load blast+/2.5.0; blastn "
    blastnCmd = get_tool_path("blastn", "blast+/2.6.0") ## This is called on Cori/Denovo, Blast+ 2.7.0 will be used

    if queryFile.endswith(".gz"):
        cmd = "zcat %s | %s -db %s %s > %s 2> %s.log " % (queryFile, blastnCmd, dbFile, blastOptions, blastOutFile, blastOutFile)
    else:
        cmd = "%s -query %s -db %s %s > %s 2> %s.log " % (blastnCmd, queryFile, dbFile, blastOptions, blastOutFile, blastOutFile)

    _, _, exitCode = run_sh_command(cmd, True, log)

    if exitCode == 0:
        log.info("run_blastn complete.")
    else:
        log.error("Failed to run_blastn. Unable to run the run_blastn command! Exit code != 0")
        return RQCExitCodes.JGI_FAILURE, None


    return RQCExitCodes.JGI_SUCCESS, blastOutFile


"""
Get organism name by gi or accession

@param gi_or_acc: gi or accession number (string type)
"""
def get_taxa_by_gi_or_acc(gi_or_acc, log):
    curlCmd = ""

    ## Just check if gi_or_gcc is "int". If not, try searching by accession
    try:
        int(gi_or_acc)
        curlCmd = "curl https://taxonomy.jgi-psf.org/tax/gi/%s" % gi_or_acc
    except ValueError as e:
        curlCmd = "curl https://taxonomy.jgi-psf.org/tax/accession/%s" % (gi_or_acc)

    stdOut, _, exitCode = run_sh_command(curlCmd, True, log, False, False) ## not to print stdOut to log

    taxaJason = None
    oldKey = None
    newKey = None

    if exitCode == 0:
        ## Try to search taxonomy server with header string if failed to find a lineage info by gi or accession
        if stdOut.find("Not found") != -1:
            curlCmd = "curl https://taxonomy.jgi-psf.org/tax/name/%s" % (gi_or_acc)
            stdOut2, stdErr2, exitCode2 = run_sh_command(curlCmd, True, log, False, False)
            assert exitCode2 == 0, "Failed to call taxonomy server: %s, %s, %s" % (curlCmd, stdOut2, stdErr2)

            ## ex) curl https://taxonomy.jgi-psf.org/tax/name/1108099_Myxococcus_fulvus_DSM_16525
            ## 1108099_Myxococcus_fulvus_DSM_16525 ==> Myxococcus fulvus DSM
            ## And search the name from taxonomy server
            ##
            ## Orig. gi_or_acc = 1108099_Myxococcus_fulvus_DSM_16525
            ## Searched lineage by "Myxococcus fulvus DSM" ==> {u'Myxococcus fulvus DSM': {u'superkingdom': {u'name': u'Bacteria', u'tax_id': u'2'}, u'name': u'Myxococcus fulvus', u'family': {u'name': u'Myxococcaceae', u'tax_id': u'31'}, u'level': u'species', u'class': {u'name': u'Deltaproteobacteria', u'tax_id': u'28221'}, u'order': {u'name': u'Myxococcales', u'tax_id': u'29'}, u'phylum': {u'name': u'Proteobacteria', u'tax_id': u'1224'}, u'suborder': {u'name': u'Cystobacterineae', u'tax_id': u'80811'}, u'subphylum': {u'name': u'delta/epsilon subdivisions', u'tax_id': u'68525'}, u'genus': {u'name': u'Myxococcus', u'tax_id': u'32'}, u'species': {u'name': u'Myxococcus fulvus', u'tax_id': u'33'}, u'tax_id': u'33'}}
            ##
            ## Note: Need to change the key name "Myxococcus fulvus DSM" to "1108099_Myxococcus_fulvus_DSM_16525" using newKey and oldKey
            ##
            if stdOut2.find("Not found") != -1:
                newKey = gi_or_acc
                oldKey = ' '.join([i for i in gi_or_acc.split('_') if not i.isdigit()])
                searchKey = '%20'.join([i for i in gi_or_acc.split('_') if not i.isdigit()])

                curlCmd = "curl https://taxonomy.jgi-psf.org/tax/name/%s" % (searchKey)
                stdOut3, stdErr3, exitCode3 = run_sh_command(curlCmd, True, log, False, False)
                assert exitCode3 == 0, "Failed to call taxonomy server: %s, %s, %s" % (curlCmd, stdOut3, stdErr3)

                if stdOut3.find("Not found") != -1:
                    return RQCExitCodes.JGI_FAILURE, None
                else:
                    stdOut = stdOut3
            else:
                stdOut = stdOut2

        jstr = stdOut.strip()

        if jstr:
            try:
                taxaJason = json.loads(jstr)
            except ValueError as e:
                log.error("Failed to convert return string from tax server to json - [%s][%s] %s" % (gi_or_acc, jstr, e))
                return RQCExitCodes.JGI_FAILURE, None
    else:
        log.error("Failed to call curl. Exit code != 0")
        return RQCExitCodes.JGI_FAILURE, None

    try:
        if newKey is not None:
            taxaJason[newKey] = taxaJason.pop(oldKey)
    except KeyError as e:
        log.error("Invalid key name: %s %s %s", newKey, oldKey, e)


    return RQCExitCodes.JGI_SUCCESS, taxaJason


"""
Get full lineage from gi
"""
def get_lineage(gi, taxaDict):
    fl = "n.a."
    if gi in taxaDict and 'lineage' in taxaDict[gi] and taxaDict[gi]['lineage'] != "-1" and  taxaDict[gi]['lineage'] != "":
        fl = taxaDict[gi]['lineage'].replace(" ", "_")

    return fl


##==============================
if __name__ == '__main__':
    desc = "RQC Blast+ wrapper"
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-c", "--output-name", dest="outputName", help="Custom output file name")
    parser.add_argument("-d", "--db", dest="dbFile", help="Reference Blast db, Full path to BLAST db or [refseq.archaea | refseq.bacteria | nt | nr | refseq.fungi | refseq.mitochondrion | refseq.plant | refseq.plasmid | refseq.plastid | refseq.viral | green_genes | ssu | lsu | lssu | collab16s | sagtam | jgicontam]", required=True)
    parser.add_argument("-f", "--add-salltitles", action="store_true", help="Append salltitles to subject field and skip collecting lineage info", dest="addSalltitles", default=False, required=False)
    parser.add_argument("-k", "--keep-orig-parsed", action="store_true", help="Keep original parsed output", dest="keepOrigParsed", default=False, required=False)
    parser.add_argument("-l", "--skip-localization", action="store_true", help="Skip database localization", dest="skipLocalization", default=False, required=False)
    parser.add_argument("-n", "--add-pct-query", action="store_true", help="Add pctQuery value in taxlist output", dest="addPctQuery", default=False, required=False)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Target path to write output files to")
    parser.add_argument("-p", "--blast-options", dest="blastOptions", help="User-specified Blast+ options. Default: -num_threads 8 -evalue 1e-30 \
                        -perc_identity 90 -word_size 45 -task megablast -show_gis -dust yes -soft_masking true -outfmt '6 qseqid sseqid bitscore evalue \
                        length pident qstart qend qlen sstart send slen staxids salltitles'. Note: top hits, top 100 hits, and tax list files will be generated \
                        only with outfmt = 6 (non-custom), 7, 10. Ex1) run_blastplus_taxserver.py -p=\"-perc_identity 70\", Ex2) run_blastplus_taxserver.py -p=\"-evalue \
                        1e-30 -perc_identity 90 -word_size 45 -outfmt 5\", Ex3) run_blastplus_taxserver.py -p=\"-evalue 1e-20 -perc_identity 80 \
                        -outfmt '6 qseqid sseqid bitscore evalue pident'\"", required=False)
    parser.add_argument("-q", "--query", dest="queryFile", help="Input query file (full path to fasta or gzipped fasta)", required=True)
    parser.add_argument("-s", "--add-species", action="store_true", help="Add taxonomy lineage/organism name to subject field", dest="addLineage", default=False, required=False)
    parser.add_argument("-t", "--num-threads", dest="numThreads", help="Number of threads to run (default: 16)", default=16, required=False)
    parser.add_argument("-v", "--version", action="version", version=VERSION)

    options = parser.parse_args()

    ## Mandatory options
    queryFile = options.queryFile
    dbFile = options.dbFile
    # outputPath = os.getcwd()
    # outputPath = options.outputPath if options.outputPath else outputPath = os.getcwd()

    # consider other aliases, e.g. refseq.archaea or archaea
    # if dbFile == "refseq.microbial":
    #     dbFile = RQCReferenceDatabases.REFSEQ_MICROBIAL
    if dbFile == "refseq.archaea":
        dbFile = RQCReferenceDatabases.REFSEQ_ARCHAEA
    elif dbFile == "refseq.bacteria":
        dbFile = RQCReferenceDatabases.REFSEQ_BACTERIA
    elif dbFile == "nr":
        dbFile = RQCReferenceDatabases.NR
    elif dbFile == "nt":
        dbFile = RQCReferenceDatabases.NT_maskedYindexedN_BB
    elif dbFile == "refseq.fungi":
        dbFile = RQCReferenceDatabases.REFSEQ_FUNGI
    elif dbFile == "refseq.mitochondrion":
        dbFile = RQCReferenceDatabases.REFSEQ_MITOCHONDRION
    elif dbFile == "refseq.plant":
        dbFile = RQCReferenceDatabases.REFSEQ_PLANT
    elif dbFile == "refseq.plasmid":
        dbFile = RQCReferenceDatabases.REFSEQ_PLASMID
    elif dbFile == "refseq.plastid":
        dbFile = RQCReferenceDatabases.REFSEQ_PLASTID
    elif dbFile == "refseq.viral":
        dbFile = RQCReferenceDatabases.REFSEQ_VIRAL
    elif dbFile == "green_genes":
        dbFile = RQCReferenceDatabases.GREEN_GENES
    elif dbFile == "lsu":
        dbFile = RQCReferenceDatabases.LSU_REF
    elif dbFile == "ssu":
        dbFile = RQCReferenceDatabases.SSU_REF
    elif dbFile == "lssu":
        dbFile = RQCReferenceDatabases.LSSU_REF
    elif dbFile == "collab16s":
        dbFile = RQCReferenceDatabases.COLLAB16S
    elif dbFile.startswith("sagtam"):
        dbFile = RQCReferenceDatabases.SAGTAMINANTS
    elif dbFile.startswith("jgicontam"):
        dbFile = RQCReferenceDatabases.CONTAMINANTS
    #elif dbFile == "bacteria_98":
    #    dbFile = "/global/dna/shared/rqc/ref_databases/ncbi/refseq.bacteria-98/bacteria_98.fasta"

    skipLocalization = False
    blastOutfmtOption = None
    addLineage = False ## append lineage info to subject
    customOutputName = None
    addSalltitles = False ## append salltitles to subject
    keepOrigParsed = False
    addPctQuery = False

    ## Burst Buffer on Cori
    dw_job_bbuffer = None
    dw_persistent_striped_ncbi_db = None

    if options.outputPath:
        outputPath = options.outputPath
        make_dir_p(outputPath)
    else:
        outputPath = os.getcwd()

    if options.blastOptions:
        if options.blastOptions.find("-outfmt") == -1: ## force to generate tabular output
            options.blastOptions += DEFAULT_OUTFMT
            blastOutfmtOption = "default"

        DEFAULT_BLAST_OPTIONS = options.blastOptions

    if options.numThreads:
        numThreads = options.numThreads

    if DEFAULT_BLAST_OPTIONS.find("num_threads") == -1:
        DEFAULT_BLAST_OPTIONS += " -num_threads %s" % (numThreads)

    # if NERSC_HOST == "denovo": ## skip localization on denovo
    #     skipLocalization = True

    if options.skipLocalization:
        skipLocalization = True

    if not skipLocalization and NERSC_HOST == "cori": ## check burst buffer on cori
        if "DW_JOB_STRIPED" in os.environ and os.environ['DW_JOB_STRIPED'] is not None:
            dw_job_bbuffer = os.environ['DW_JOB_STRIPED']
        elif "DW_JOB_PRIVATE" in os.environ and os.environ['DW_JOB_PRIVATE'] is not None:
            dw_job_bbuffer = os.environ['DW_JOB_PRIVATE']

        if "DW_PERSISTENT_STRIPED_NCBI_DB" in os.environ and os.environ['DW_PERSISTENT_STRIPED_NCBI_DB'] is not None: ## if persisten bbf found
            skipLocalization = False
            dw_job_bbuffer = None
            dw_persistent_striped_ncbi_db = os.environ['DW_PERSISTENT_STRIPED_NCBI_DB']
        elif dw_job_bbuffer is not None: ## final check for burst buffer
            skipLocalization = False
        else:
            skipLocalization = True

    if options.addLineage:
        addLineage = True

    if options.addSalltitles:
        addSalltitles = True

    if options.keepOrigParsed:
        keepOrigParsed = True

    if options.addPctQuery:
        addPctQuery = True

    if options.outputName:
        customOutputName = options.outputName


    logFileName = "run_blastplus.log"
    log = get_logger("run_blast", os.path.join(outputPath, logFileName), LOG_LEVEL, False, True)


    log.info("=================================================================")
    log.info("   RQC Blast wrapper (version %s)", VERSION)
    log.info("=================================================================")

    log.info("Starting %s with %s query file...", SCRIPT_NAME, queryFile)


    ## Get the number of query to compte pctQuery value and set it in taxlist
    totalQuery = 0 ## total number of query

    if addPctQuery:
        reformatshCmd = get_tool_path("reformat.sh", "bbtools")
        cmd = "%s %s" % (reformatshCmd, queryFile)
        _, stdErr, exitCode = run_sh_command(cmd, True, None, True)
        assert exitCode == 0

        for l in stdErr.split('\n'):
            if l.startswith("Input:"):
                m = re.findall("(\d+.\d*)", l)
                log.info("Input query read and base cnt = %s, %s", m[0], m[1])
                totalQuery = int(m[0])
                break


    ############################################################################
    ## Blast db localization
    ############################################################################
    if not skipLocalization:
        log.info("DB localization started for %s", dbFile)

        if dw_job_bbuffer:
            log.info("Burst Buffer initialized: %s", dw_job_bbuffer)
        elif dw_persistent_striped_ncbi_db:
            log.info("Persistent Burst Buffer found: %s", dw_persistent_striped_ncbi_db)

        ## lib/rqc_utility.py
        localizedDbPath, exitCode = localize_dir2(dbFile, log, bbuffer=dw_job_bbuffer)

        if exitCode == RQCExitCodes.JGI_SUCCESS: ## Replace dbFile with the localized location
            dbFile = os.path.join(localizedDbPath, safe_basename(dbFile, log)[0])
            log.info("Blast DB localization completed for %s on %s", dbFile, platform.node())
        else:
            log.info("Blast DB localization failed on %s. Use %s instead.", platform.node(), dbFile)



    ############################################################################
    ## Run Blastn
    ############################################################################
    log.info("Blast search options: %s", DEFAULT_BLAST_OPTIONS)

    retCode, blastOutputFile = run_blastn(queryFile, dbFile, DEFAULT_BLAST_OPTIONS, outputPath, customOutputName, log)

    if retCode == 0:
        status = "run_blastn complete"
    else:
        status = "run_blastn failed"
        log.error("Failed to run_blastplus_taxserver. Unable to run the run_blastn command! Exit code != 0")
        exit(-1)

    assert os.path.isfile(blastOutputFile), "ERROR: blast output file not found"



    ############################################################################
    ## Post processing
    ############################################################################

    ##
    ## Check blast output format options before running postprocessing
    ## ex) ~/work/bitbucket-repo/jgi-rqc-pipeline/tools/run_blastplus.py -q scaffolds.trim.fasta -o ./out-w-lineage -l
    ## -d BBPAP_16s.fasta -k -s -n -p="-outfmt '6 qseqid sseqid bitscore evalue pident'"
    ##
    ## print i, optList, optList[i + 1] ==> 0 ['-outfmt', "'6", 'qseqid', 'sseqid', 'bitscore', 'evalue', "pident'"] '6
    ##
    if options.blastOptions:
        optList = options.blastOptions.strip().split()
        i = optList.index("-outfmt")
        if blastOutfmtOption != "default":
            if optList[i + 1][0] in ('"', "'"):
                log.error("Blast+ custom output format is not supported: %s", DEFAULT_BLAST_OPTIONS)
                ## can not produce the other output files.
                exit(-1)
            else:
                blastOutfmtOption = int(optList[i + 1].replace("'", "").replace('"', ""))


    ##
    ## Add header line to the parsed output for default and -outfmt 10

    assert os.path.isfile(blastOutputFile), "ERROR: blast ouput not found."

    if os.path.isfile(blastOutputFile):
        if not options.blastOptions or blastOutfmtOption == "default":
            headerStr = "#qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids"

            ## if "-s" is not set, the salltitles filed will be remained in the last column of blast output file, *.parsed
            ## if set, the salltitles will be appended to the subject field string.
            if not addLineage:
                headerStr += " salltitles"

        if blastOutfmtOption == 10:
            headerStr = "#query id, subject id, perc identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score"

        headerAddCmd = """echo "%s" | cat - %s | sed 's/:/_/g; s/;/_/g' > %s && mv %s %s"""\
                       % (headerStr, blastOutputFile, os.path.join(outputPath, "temp.parsed"), os.path.join(outputPath, "temp.parsed"), blastOutputFile)

        _, _, exitCode = run_sh_command(headerAddCmd, True, log)

        if exitCode != 0:
            log.error("Failed to add header to blast output. Exit code != 0")
            exit(-1)

    assert os.path.isfile(blastOutputFile), "ERROR: adding header to blast output failed."

    log.info("Blast+ outfmt option = %s", blastOutfmtOption)
    log.info("Blast+ option = %s", DEFAULT_BLAST_OPTIONS)


    ##
    ## Collect unique gi from parsed output
    ## taxonomyInfoDict will be used to store lineage info per each gi
    ##
    log.info("Creating gi list...")
    taxonomyInfoDict = {} ## {unique gi: {lineage, salltitles, cnt, perc}} (here, gi can be real gi or a part of subject header string)

    with open(blastOutputFile, 'r') as PARSEDFH:
        for l in PARSEDFH:
            if l.startswith('#'):
                continue

            try:
                (qseqid, sseqid, bitscore, evalue, length, pident, qstart, qend, qlen, sstart, send, slen, staxid, salltitles) = l.rstrip().split('\t')
            except ValueError:
                log.warning("No BLAST hits found.")
                break

            ## gi|851289456|ref|NZ_AXDV01000017.1|
            ## or
            ## gi|472256744| gi|461490773| gi|71143482| gi|461490773|
            ## or
            ## 5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB
            ## or
            ## ADDN01001501.390762.393334
            ##
            ## or (since Apr 2017)
            ## ref|accession#...
            ## or
            ## accession|accession# or accession accession#
            ## or
            ## gbk|accession#
            ##
            if sseqid.startswith("gi|"):
                gi = sseqid.split('|')[1] ## string gi
                if gi not in taxonomyInfoDict:
                    taxonomyInfoDict[gi] = {} ## new entry

            elif sseqid.startswith("ref|") or sseqid.startswith("accession|") or sseqid.startswith("gbk|"):
                acc = sseqid.split('|')[1]
                if acc not in taxonomyInfoDict:
                    taxonomyInfoDict[acc] = {} ## new entry

            elif sseqid.startswith("accession "):
                ## ex) accession NC_001422.1 abcabc
                ## ex) accession NC_001422.1|abcabc
                # acc = sseqid.split(' ')[1]
                acc = sseqid.split('|')[0].split(' ')[1]
                if acc not in taxonomyInfoDict:
                    taxonomyInfoDict[acc] = {} ## new entry

            else:
                ## HE582776.1.1512 Bacteria;Proteobacteria;Gammaproteobacteria;Pseudomonadales;Moraxellaceae;uncultured;Agitococcus lubricus
                ## To lookup the taxonomy server with header
                h = sseqid.split()[0]
                if h not in taxonomyInfoDict:
                    taxonomyInfoDict[h] = {} ## new entry


    log.info("Number of gi(s) and accession(s) collected from .parsed: %s", len(taxonomyInfoDict))


    ##
    ## Create the lineage
    ##
    log.info("Finding lineage...")

    # ranks = ['domain', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    ranks = ['superkingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species']
    ranksLookup = dict([(r, idx) for idx, r in enumerate(ranks)])
    speciesNameDict = {}

    ## For collected gis from parsed output, create lineage info using gi2taxid nad NCBI tax tree
    for gi_or_acc, v in taxonomyInfoDict.iteritems():
        ret, taxJson = get_taxa_by_gi_or_acc(gi_or_acc, log)

        # print gi_or_acc
        # pprint.pprint(taxJson[gi_or_acc])

        if ret == RQCExitCodes.JGI_SUCCESS:
            if taxJson is None:
                log.warning("tax id not found for gi_or_acc = %s", str(gi_or_acc))
                continue

            if gi_or_acc in taxJson:
                lineage = [""] * len(ranks)
                for k2, v2 in taxJson[gi_or_acc].iteritems():
                    if k2 != "tax_id" and "name" in v2 and k2 in ranksLookup: ## ignore subspecies, kingdom
                        try:
                            lineage[ranksLookup[k2]] = v2["name"]
                        except (TypeError, ValueError, KeyError) as err:
                            log.warning("Exception: %s %s", err, err.args)

                if len(lineage[-1]) > 0: ## lineage[6]
                    speciesName = lineage[-1]

                    ## if species name exists in speciesNameDict, update the cnt and perc instead of creating new cnt and perc
                    ## 05242017 Now that we are using accession number, there are cases that the same species have different accession numbers
                    ## so disable this species name comparison.
                    if speciesName in speciesNameDict and False:
                        v['lineage'] = "-1" ## not to save this to taxlist file
                    else:
                        speciesNameDict[speciesName] = gi_or_acc
                        v['lineage'] = ";".join(lineage)

                else: ## if there is no species name found
                    v['lineage'] = ";".join(lineage)


    ##
    ## Processing parsed file to add taxonomy
    ##
    if addLineage:
        log.info("Adding lineage info to parsed file...")

        fh, tempFile = mkstemp()

        with open(tempFile, 'w') as NEW_PFH:
            with open(blastOutputFile, 'r') as PFH:
                for l in PFH:
                    if l.startswith('#'):
                        NEW_PFH.write(l)
                        continue

                    try:
                        (qseqid, sseqid, bitscore, evalue, length, pident, qstart, qend, qlen, sstart, send, slen, staxid, salltitles) = l.rstrip().split('\t')
                    except ValueError as e:
                        log.error("blast parsed file format error: %s, %s", blastOutputFile, e)
                        break

                    ## gi|851289456|ref|NZ_AXDV01000017.1|
                    ## or
                    ## gi|472256744| gi|461490773| gi|71143482| gi|461490773|
                    ## or
                    ## 5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB
                    ## or
                    ## ADDN01001501.390762.393334
                    ##
                    ## or (since Apr 2017)
                    ## ref|accession#...
                    ## or
                    ## accession|accession# or accession accession#
                    ## or
                    ## gbk|accession#
                    ##
                    ## Silva
                    ## FJ009592.1.2044 Eukaryota_Opisthokonta_Nucletmycea_Fungi_Kickxellomycotina_Glomeromycota_Glomeromycetes_Glomerales_Rhizophagus_Rhizophagus intraradices

                    ## Request from Jasmyn
                    if addSalltitles:
                        ## ex) sseqid salltitles = ref|NC_015711.1| ref|NC_015711.1| Myxococcus fulvus HW-1, complete genome
                        if salltitles.startswith(sseqid):
                            salltitles2 = salltitles.replace(sseqid, '').strip()
                        else:
                            salltitles2 = salltitles.strip()

                        sseqid = sseqid + ' ' + salltitles2


                    if sseqid.startswith("gi|") or sseqid.startswith("ref|") or sseqid.startswith("accession|") or sseqid.startswith("accession ") or sseqid.startswith("gbk|"):
                        gi = sseqid.split('|')[1]
                        fullLineage = get_lineage(gi, taxonomyInfoDict)
                        lineageNameToPrint = "n.a."

                        if fullLineage != "n.a.":
                            lineageNameToPrint = fullLineage
                        elif salltitles != "":
                            # if salltitles.split()[0].startswith("gi|"): ## if salltitles contains subject gi field like "gi|910084296|ref|NZ_BBMP01000017.1| Halococcus sediminico ..."
                            if sseqid.startswith("gi|") or sseqid.startswith("ref|") or sseqid.startswith("accession|") or sseqid.startswith("accession ") or sseqid.startswith("gbk|"):
                                salltitles = " ".join(salltitles.split()[1:])
                            lineageNameToPrint = salltitles.replace(" ", '_').replace(':', '_') ## salltitles may have ':'

                        NEW_PFH.write("\t".join([qseqid, sseqid + ':' + lineageNameToPrint, bitscore, evalue, length, pident, qstart, qend, qlen, sstart, send, slen, staxid]) + '\n')

                    else:
                        ## TODO: is there any ';' left in salltitles?
                        if salltitles.find(';') != -1: ## if header contains lineage info
                            toks = salltitles.split(';')
                            toks[0] = toks[0].split(' ')[-1] ## remove subject name from the lineage ex) FJ231214.1.2349 Eukaryota;...;;;; -> Eukaryota;...;;;;
                            salltitles = ';'.join(toks)
                        else:
                            ## There is no lineage info but species name
                            ## Just append the species name after the subject title
                            ## ex) 5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB:5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB

                            ## 05242017
                            key = salltitles.split()[0]
                            fullLineage = get_lineage(key, taxonomyInfoDict)

                            # if fullLineage not in ("n.a.", ";;;;;;"):
                            if fullLineage != "n.a.":
                                lineageNameToPrint = fullLineage
                            else:
                                lineageNameToPrint = salltitles.replace(" ", '_').replace(':', '_')

                        ## This is the place to add lineage to subject (with ':' as a divider)
                        NEW_PFH.write("\t".join([qseqid, sseqid + ':' + lineageNameToPrint, bitscore, evalue, length, pident, qstart, qend, qlen, sstart, send, slen, staxid]) + '\n')


        os.close(fh)

        if not keepOrigParsed:
            remove_file(blastOutputFile)
        else:
            move_file(blastOutputFile, blastOutputFile + ".orig")

        move_file(tempFile, blastOutputFile)
        change_mod(blastOutputFile, "0666")
        # change_grp(blastOutputFile, "genome")
        back_ticks(["chgrp", "genome", blastOutputFile])



    ############################################################################
    ## Create .tophit and .top100hit files
    ############################################################################

    ## Input format (blast output)
    ## OLD               3      4      5      6                 9                        13
    ## qseqid sseqid bitscore evalue length pident qstart qend qlen qstrand sstart send slen sstrand staxids
    ##
    ## NOTE: failed to get qstrand 02052016
    ##
    ## NEW      2       3       4      5      6                 9                12     13
    ## qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids
    ##

    ## NEW      2        3       4      5      6                 9                12     13     14
    ## qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids salltitles

    ##
    ## Output tophit format
    ##   1       2       3      4       5
    ## query  subject  expect  length  %id  q_length  s_length
    ##
    ## ex)
    ## #query subject expect length perc_id q_length s_length
    ## MISEQ04:278:000000000-ALA09:1:2114:18141:5140 gi|851289456|ref|NZ_AXDV01000017.1| 5e-37 85 100.00 151 5516
    ## MISEQ04:278:000000000-ALA09:1:1104:14980:2927 gi|850494210|ref|NZ_JJQO01000102.1| 2e-31 91 94.51 151 2246
    ## MISEQ04:278:000000000-ALA09:1:1107:16838:26088 gi|850494210|ref|NZ_JJQO01000102.1| 2e-31 91 94.51 151 2246

    ## Sorting
    ## gensub(regexp, replacement, how [, target])
    ## 1. sort blast output by query (asc) & by bitscore (asc) & by perc identity (asc)
    ## 2. replace " " with "  " in subject
    ## 3. replace " " with "_" in subject
    ## 4. collect best hits per query (by a[$1] dict)
    ## 4. sort by evalue (asc) & by percentage identity (desc)
    ##
    ## The original BLAST sorting criteria
    ## 1. Expect Value: default
    ## 2. Max Score: By the bit score of HSPs, similar to Expect Value
    ## 3. Total Score: By the sum of scores from all HSPs from the same database sequence
    ## 4. Query Coverage: By the percent of length coverge for the query
    ## 5. Max Identity: By the maximal percent ID of the HSPs
    ## Here sort by 1, 2, and 5 only

    ##
    ## 04.14.2016: Changed evalue sort param from "sort -k 4,4n -k 6,6n" to "sort -g -k3,3 -k5,5n"
    ## 04.20.2016: Changed evalue sort param: "sort -k3,3g -k5,5rn"
    ##

    ## if -outfmt=6
    ## ex)
    ## MISEQ04:278:000000000-ALA09:1:1101:10764:6462    gi|851289456|ref|NZ_AXDV01000017.1| 100.00  151 0   01151   2908    3058    9e-74   279
    ##
    ## if -outfmt=7
    ## ex)
    ## # BLASTN 2.2.31+
    ## # Query: MISEQ04:278:000000000-ALA09:1:1101:27028:10929 1:N:0:ATGTC
    ## # Database: /scratch/rqc/refseq.archaea/refseq.archaea
    ## # Fields: query id, subject id, % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score
    ## # 1 hits found
    ## MISEQ04:278:000000000-ALA09:1:1101:27028:10929   gi|851289456|ref|NZ_AXDV01000017.1| 100.00  151 0   01151   2765    2615    9e-74   279
    ##
    ## if -outfmt=10
    ## ex)
    ## MISEQ04:278:000000000-ALA09:1:1101:26780:18252,gi|851289456|ref|NZ_AXDV01000017.1|,100.00,151,0,0,1,151,248,98,9e-74,279



    if not options.blastOptions or blastOutfmtOption == "default":
        ## 10232017 Changed the delimiter from space to tab
        ## a[$1]=t" "$4" "$5" "$6" "$9" "$12" "$13} to a[$1]=t"\t"$4"\t"$5"\t"$6"\t"$9"\t"$12"\t"$13}
        tophitCmd = """echo "#query subject expect length perc_id q_length s_length staxids" > %s ;
grep -v '^#' %s 2>/dev/null |
sort -t$'\t' -k1,1 -k3,3n -k6,6n |
gawk -F'[\t]' '!/^#/{
s=gensub(" ", "  ", 1, $2); # creates new column:=3
t=gensub(" ", "_", "g", s)
a[$1]=t"\t"$4"\t"$5"\t"$6"\t"$9"\t"$12"\t"$13}
END{ for(e in a) print e,a[e] }' |
sort -k3,3g -k5,5rn >> %s""" % (blastOutputFile + ".tophit", blastOutputFile, blastOutputFile + ".tophit")

        _, _, exitCode = run_sh_command(tophitCmd, True, log)

        if exitCode == 0:
            log.info("Blast tophit file created: %s", blastOutputFile + ".tophit")
        else:
            log.error("Failed to run_blastplus_taxserver. Failed to create tophit file. Exit code != 0")
            exit(-1)


        ## Create top100 hit file from *.parsed
        ## 1. ordered by bit score (desc) and evalue (asc) and %id (desc) and query_id
        ## 2. get unique query
        ##
        top100Cmd = """echo "#qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids" > %s;
grep -v '^#' %s 2>/dev/null |
sort -t$'\t' -k3,3rn -k4,4g -k6,6rn -uk1,1 |
head -n 100 >> %s""" % (blastOutputFile + ".top100hit", blastOutputFile, blastOutputFile + ".top100hit")

        ## if "-s" is not set
        ## "salltitles" will exist at the last column of blast output (*.parsed).
        ## Thus remove the last column
        if not addLineage:
            top100Cmd = """echo "#qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids" > %s;
awk '{$NF=""; print $0}' %s |
grep -v '^#' 2>/dev/null |
sort -t$'\t' -k3,3rn -k4,4g -k6,6rn -uk1,1 |
head -n 100 >> %s""" % (blastOutputFile + ".top100hit", blastOutputFile, blastOutputFile + ".top100hit")

        _, _, exitCode = run_sh_command(top100Cmd, True, log)

        if exitCode == 0:
            log.info("Blast top100 hit file created: %s", blastOutputFile + ".top100hit")
        else:
            log.error("Failed to run_blastplus_taxserver. Failed to create top100 hit file. Exit code != 0")
            exit(-1)


    elif blastOutfmtOption == 6 or blastOutfmtOption == 7 or blastOutfmtOption == 10:
        ##    1           2           3             4              5           6        7         8       9         10      11       12
        ## query id, subject id, % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score
        tophitCmd = """echo "#query subject expect length perc_id q_length s_length" > %s ;
grep -v '^#' %s 2>/dev/null |
sort -t$%s -k1,1 -k12,12n -k3,3n |
gawk -F'[%s]' '!/^#/{
s=gensub(" ", "  ", 1, $2); # creates new column:=3
t=gensub(" ", "_" ,"g" ,s)
a[$1]=t" "$11" "$4" "$3" "$8-$7+1" "sqrt(($10-$9)^2)+1" "$5}
END{ for(e in a) print e,a[e] }' |
sort -k3,3g -k5,5rn >> %s"""

        if blastOutfmtOption == 10: ## comma separated
            tophitCmd = tophitCmd % (blastOutputFile + ".tophit", blastOutputFile, "','", ",", blastOutputFile + ".tophit")
        else:
            tophitCmd = tophitCmd % (blastOutputFile + ".tophit", blastOutputFile, "'\\t'", "\\t", blastOutputFile + ".tophit")

        _, _, exitCode = run_sh_command(tophitCmd, True, log)

        if exitCode == 0:
            log.info("Blast tophit file created: %s", blastOutputFile + ".tophit")
        else:
            log.error("failed to run_blastplus_taxserver. Failed to create tophit file. Exit code != 0")
            exit(-1)


        ## Create top100 hit file from *.parsed
        ## 1. ordered by bit score (desc) and evalue (asc) and %id (desc)
        ## 2. get unique query
        ##
        top100Cmd = """echo "#qseqid sseqid bitscore evalue length pident qstart qend qlen sstart send slen staxids" > %s;
grep -v '^#' %s 2>/dev/null |
gawk -F'[%s]' '{print $1"\t"$2"\t"$12"\t"$11"\t"$4"\t"$3"\t"$7"\t"$8"\t"$8-$7+1"\t"$9"\t"$10"\t"sqrt(($10-$9)^2)+1"\t"$5}' |
sort -t$'\t' -k3,3rn -k4,4g -k6,6rn -uk1,1 |
head -n 100 >> %s"""

        if blastOutfmtOption == 10: ## comma separated
            top100Cmd = top100Cmd % (blastOutputFile + ".top100hit", blastOutputFile, ",", blastOutputFile + ".top100hit")
        else:
            top100Cmd = top100Cmd % (blastOutputFile + ".top100hit", blastOutputFile, "\\t", blastOutputFile + ".top100hit")

        _, _, exitCode = run_sh_command(top100Cmd, True, log)

        if exitCode == 0:
            log.info("Blast top100 hit file created: %s", blastOutputFile + ".top100hit")
        else:
            log.error("failed to run_blastplus_taxserver. Failed to create top100 hit file. Exit code != 0")
            exit(-1)

    else:
        log.warning("The Blast option is not supported yet: %s", DEFAULT_BLAST_OPTIONS)
        log.warning("The top hit, top 100 hit, and taxlist output files will not be created.")
        exit(-1)




    ############################################################################
    ## Create .taxlist file
    ############################################################################

    totalTophits = 0
    taxonomyInfoDict.clear()
    speciesNameDict.clear()

    ## Collect the gis from tophit file
    with open(blastOutputFile + ".tophit", 'r') as TOPFH:
        for l in TOPFH:
            if l.startswith('#'):
                continue

            try:
                (query, subject, expect, length, percId, qLength, sLength, staxid) = l.rstrip().split() ## staxid is not used for now.
            except ValueError:
                log.warning("No BLAST hits found.")
                break

            ## gi|9626372|ref|NC_001422.1|:Viruses;;;;Microviridae;Microvirus;Enterobacteria_phage_phiX174_sensu_lato
            ## or
            ## gi|472256744| gi|461490773| gi|71143482| gi|461490773|:Viruses;;;;Microviridae;Microvirus;Enterobacteria_phage_phiX174_sensu_lato
            ## or
            ## 5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB:5843_unknown_prasinophyte_CCMP1205_Gene_Expression_LIBS_HBXW_POOLS_HCOB
            ## or
            ## ADDN01001501.390762.393334:Eukaryota;Viridiplantae;Streptophyta;Embryophyta;Tracheophyta;Spermatophyta;Magnoliophyta;Liliopsida;Poales;Poaceae;BEP_clade;Pooideae;Brachypodieae;Brachypodium;;Brachypodium_distachyon
            ## X86563.95161.96651:Bacteria;Cyanobacteria;Chloroplast;Zea_mays
            ## or
            ## ref|NC_001422.1|:;;;;;;

            ## NEW 07212016 using the subject title as key instead of using gi (for subject title w/o gi)
            ## Count the hit numbers

            ##
            ## or (since Apr 2017)
            ## ref|accession#...
            ## or
            ## accession|accession# or accession accession#
            ## or
            ## gbk|accession#
            ##

            ## Silva
            ## HQ191393.1.2397:HQ191393.1.2397_Eukaryota_Opisthokonta_Nucletmycea_Fungi_Chytridiomycota_Chytridiomycetes_uncultured_uncultured_Chytridiomycota
            ## EWC01043727.1704.3050_Eukaryota_Archaeplastida_Chloroplastida_Charophyta_Phragmoplastophyta_Streptophyta_Embryophyta_Tracheophyta_Spermatophyta_Magnoliophyta_Solanales_Solanum_tuberosum_(potato)

            gi_or_acc = subject.split(':')[0]
            if gi_or_acc not in taxonomyInfoDict: ## load new taxonomyInfoDict
                taxonomyInfoDict[gi_or_acc] = {} ## new entry
                taxonomyInfoDict[gi_or_acc]['cnt'] = 1
                if addLineage:
                    if subject.find(':') != -1:
                        taxonomyInfoDict[gi_or_acc]['salltitles'] = subject.split(':')[-1]

            else:
                taxonomyInfoDict[gi_or_acc]['cnt'] += 1

            totalTophits += 1

    log.info("Number of tophits processed: %s", totalTophits)
    log.debug("Number of unique subject: %s", len(taxonomyInfoDict))
    # log.debug("taxonomy Info Dict: %s", taxonomyInfoDict)


    ## Collect the lineage
    for gi_or_acc, v in taxonomyInfoDict.iteritems():
        if gi_or_acc.startswith("gi|") or gi_or_acc.startswith("ref|") or gi_or_acc.startswith("accession|") or gi_or_acc.startswith("accession ") or gi_or_acc.startswith("gbk|"):
            gi_or_acc2 = ""
            if gi_or_acc.startswith("gi|") or gi_or_acc.startswith("ref|") or gi_or_acc.startswith("accession|") or gi_or_acc.startswith("gbk|"):
                gi_or_acc2 = gi_or_acc.split('|')[1]
            elif gi_or_acc.startswith("accession "):
                ## ex) accession NC_001422.1 abcabc
                ## ex) accession NC_001422.1|abcabc
                gi_or_acc2 = gi_or_acc.split('|')[0].split(' ')[1]

            try:
                ## TODO: remove this and use appended lineage info from header line
                ret, taxJson = get_taxa_by_gi_or_acc(gi_or_acc2, log)

                if taxJson is None:
                    log.warning("tax id not found for gi from %s", str(gi_or_acc))

                    ## RQCSUPPORT-813
                    ## gi cannot be converted to taxid b/c it's new.
                    ## ex) gi|1042821167|gb|CP016182.1|:Escherichia_coli_strain_EC590,_complete_genome
                    ## --> use "Escherichia_coli_strain_EC590,_complete_genome"
                    if "salltitles" in v and v['salltitles']:
                        speciesName = v['salltitles'].split(';')[-1]

                        if speciesName in speciesNameDict:
                            taxonomyInfoDict[speciesNameDict[speciesName]]['cnt'] += int(v['cnt'])
                            taxonomyInfoDict[speciesNameDict[speciesName]]['perc'] += (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                            taxonomyInfoDict[speciesNameDict[speciesName]]['pct_query'] += (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0
                            # v['lineage'] = "-1" ## not to save this to taxlist file
                        else:
                            speciesNameDict[speciesName] = gi_or_acc
                            v['species'] = speciesName
                            v['lineage'] = ";;;;;;" + v['salltitles']
                            v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0 ## compute percentage of tophits
                            v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0 ## compute percentage of query

                else:
                    lineage = [""] * len(ranks)
                    for k2, v2 in taxJson[gi_or_acc.split('|')[1]].iteritems():
                        if k2 != "tax_id" and "name" in v2 and k2 in ranksLookup: ## ignore subspecies, kingdom
                            try:
                                lineage[ranksLookup[k2]] = v2["name"]
                            except (TypeError, ValueError, KeyError) as err:
                                log.warning("Exception: %s %s", err, err.args)

                    if len(lineage[-1]) > 0: ## lineage[6]
                        speciesName = lineage[-1]

                        # if species name exists in speciesNameDict, update the cnt and perc instead of creating new cnt and perc
                        if speciesName in speciesNameDict:
                            taxonomyInfoDict[speciesNameDict[speciesName]]['cnt'] += int(v['cnt'])
                            taxonomyInfoDict[speciesNameDict[speciesName]]['perc'] += (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                            taxonomyInfoDict[speciesNameDict[speciesName]]['pct_query'] += (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0
                            v['lineage'] = "-1" ## not to save this to taxlist file

                        else:
                            ## need to assign part of header string not the gi only for non-standard header format
                            speciesNameDict[speciesName] = gi_or_acc
                            v['species'] = speciesName
                            v['lineage'] = ";".join(lineage)
                            v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                            v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0

                    else: ## if there is no species name found
                        v['lineage'] = ";".join(lineage)
                        v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                        v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0

            except KeyError:
                v['lineage'] = ""
                v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0

        else: ## no gi found
            if 'salltitles' in v and v['salltitles']:
                speciesName = v['salltitles'].split(';')[-1]

                if speciesName in speciesNameDict:
                    taxonomyInfoDict[speciesNameDict[speciesName]]['cnt'] += int(v['cnt'])
                    taxonomyInfoDict[speciesNameDict[speciesName]]['perc'] += (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                    v['lineage'] = "-1" ## not to save this to taxlist file

                else:
                    speciesNameDict[speciesName] = gi_or_acc
                    v['species'] = speciesName
                    v['lineage'] = v['salltitles']
                    v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                    v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0

            else:
                v['lineage'] = ""
                v['perc'] = (100.0 * v['cnt'] / totalTophits) if totalTophits > 0 else 0.0
                v['pct_query'] = (100.0 * v['cnt'] / totalQuery) if totalQuery > 0 else 0.0



    log.info("Creating taxlist file...")

    with open(blastOutputFile + ".taxlist", 'w') as TLFH:
        TLFH.write("#name\tnumHits\tpctHit\tpctQuery\ttaxonomy\n")
        for gi_or_acc in sorted(taxonomyInfoDict.keys(), key=lambda y: (taxonomyInfoDict[y]['cnt']), reverse=True): ## sort dict by cnt
            if 'lineage' in taxonomyInfoDict[gi_or_acc] and taxonomyInfoDict[gi_or_acc]['lineage'] != "-1" and  taxonomyInfoDict[gi_or_acc]['lineage'] != "":
                ## For backward compatibility, add the unused field, pctQuery
                ## 08072017 compute pctQuery = numHits / numQuery * 100.0

                ## Output format
                ## speciesName  numHits   pctHit  pctQuery  taxonomyLineage

                ## For {'ref|NC_001422.1|': {'lineage': ';;;;;;', 'perc': 100.0, 'cnt': 5, 'salltitles': ';;;;;;'}}
                speciesName = taxonomyInfoDict[gi_or_acc]['lineage'].split(';')[-1].replace(" ", "_")
                if speciesName == "":
                    speciesName = gi_or_acc

                # TLFH.write("%s\t%s\t%.2f\t0.00\t%s\n" % (speciesName,
                #                                          taxonomyInfoDict[gi_or_acc]['cnt'],
                #                                          taxonomyInfoDict[gi_or_acc]['perc'],
                #                                          taxonomyInfoDict[gi_or_acc]['lineage'].replace(" ", "_")))

                TLFH.write("%s\t%s\t%.2f\t%.2f\t%s\n" % (speciesName,
                                                         taxonomyInfoDict[gi_or_acc]['cnt'],
                                                         taxonomyInfoDict[gi_or_acc]['perc'],
                                                         taxonomyInfoDict[gi_or_acc]['pct_query'],
                                                         taxonomyInfoDict[gi_or_acc]['lineage'].replace(" ", "_")))

    if os.path.isfile(blastOutputFile + ".taxlist"):
        log.info("Taxlist file created: %s", blastOutputFile + ".taxlist")
    else:
        log.error("Failed to run_blastplus_taxserver. Failed to create taxlist file. Exit code != 0")
        exit(-1)


    log.info("run_blastplus_taxserver.py completed.")

    exit(0)


## EOF