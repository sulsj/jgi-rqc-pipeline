#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time
from subprocess import Popen, call, PIPE

p = Popen("qstat", stdout=PIPE, stderr=PIPE, shell=True)
stdOut, stdErr = p.communicate()
exitCode = p.returncode

if exitCode == 0:
    lines = stdOut.split('\n')
    if lines:
        for l in lines:
            t = l.strip().split()
            if len(t) == 8 and t[3] == "qc_user" and t[4].lower() == "eqw":
                p = Popen("qdel %s" % t[0], stdout=PIPE, stderr=PIPE, shell=True)
                stdOut, stdErr = p.communicate()
                exitCode = p.returncode
                if exitCode == 0:
                    print "%s - %s: DELETED!" % (time.strftime("%Y-%m-%d %H:%M"), l.strip())
                else:
                    print >> sys.stderr, "Failed to execute qdel: %s" % l.strip()

