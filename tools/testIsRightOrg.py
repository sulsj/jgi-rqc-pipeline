#!/usr/bin/env python

import os
import sys
import pprint
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
import orgutils

'''
files = [
    '/global/dna/shared/rqc/pipelines/microbe_iso/archive/02/93/98/33/iso/megablast/megablast.scaffolds.trim.fasta.vs.nt_bbdedupe_bbmasked_formatted.parsed',
    '/global/dna/shared/rqc/pipelines/microbe_iso/archive/02/93/98/33/iso/megablast/megablast.scaffolds.trim.fasta.vs.refseq.bacteria.parsed',
    '/global/dna/shared/rqc/pipelines/microbe_iso/archive/02/93/98/33/iso/megablast/megablast.scaffolds.trim.fasta.vs.refseq.archaea.parsed',
    ]
organismName = 'Fibrobacter sp.'
taxId = 35828
debug = False
isRightOrganism = orgutils.Utils(debug=debug).isRightOrganism(files, organismName, taxId=taxId, maxExpectScore=1, minAlignLength=500, minPctId=90)
print "isRightOrganism=%s"%isRightOrganism
'''

files = ['/global/projectb/scratch/brycef/iso/BPGHW/iso/megablast/megablast.scaffolds.trim.fasta.vs.nt_bbdedupe_bbmasked_formatted.parsed', '/global/projectb/scratch/brycef/iso/BPGHW/iso/megablast/megablast.scaffolds.trim.fasta.vs.refseq.archaea.parsed', '/global/projectb/scratch/brycef/iso/BPGHW/iso/megablast/megablast.scaffolds.trim.fasta.vs.refseq.bacteria.parsed']
organismName = 'Taibaiella chishuiensis'
taxId = 1434707
debug = False
isRightOrganism = orgutils.Utils(debug=debug).isRightOrganism(files, organismName, taxId=taxId, maxExpectScore=1, minAlignLength=500, minPctId=90)
print "isRightOrganism=%s"%isRightOrganism

