#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
Run Folder

Show run paths for all pipelines for a seq unit id, library or sequencing project

e.g.
$ ./run_folder.py --seq-units 7274.7.65872.TAGCTT.fastq.gz,7275.3.65879.ACTGAT.fastq.gz,7275.7.65880.TAGCTT.fastq.gz
$ ./run_folder.py --lib-names OZWG,OSZC
$ ./run_folder.py --spid 1014865 --quiet
$ ./run_folder.py -spid 1060131 -ap
$ ./run_folder.py -spid 1060131
$ ./run_folder.py --lib apszn

* to do: return contig file for assembly, jigsaw runs
jigsaw: ./vpallpaths/assembly/DATA/std_shredpairs/ASSEMBLIES/run/submission.assembly.fasta or ./final/submission.assembly.fasta
asm: 7620.7.78922.TGAATG.subsampled5000000.fastq.velvet.k63/contigs.fa

1.3 - 2015-09-14
- works with data units (Hacksaw, heisenberg, itags, falcon)
- additional help/usage text, pylint cleanup

1.3.1 - 2016-03-17
- ap_tool not called correctly, fixed (Kurt found it)

1.3.2 - 2017-04-03
- message to get run folder from JAMO

1.3.3 - 2017-11-27
- changed to jgi_connect_db

1.3.4 - 2017-12-01
- bug for library BSSZS - denovo python did not recognize response, forced to be json
- changed to use curl2 lib

'''

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


from argparse import ArgumentParser
import sys
import os
import re
import MySQLdb
import json

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from db_access import jgi_connect_db
from common import human_size, run_command, post_mortem_cmd
from curl2 import Curl, CurlHttpException

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions




'''
Return the path for the pipeline for a seq unit
- will return run folders "in progress"

@param seqUnitName: seq_units.seqUnitName
@param pipelineTypeId: id of the pipeline to change status

'''
def run_folder_path(seq_unit_name, complete_status, mode = None):

    cnt = 0

    # Illumina (.fastq.gz) or Pacbio (.fastq)
    if seq_unit_name:

        # legacy pipeline
        pipeline_dict = {
            1: "Read QC",
            2: "Assembly QC",
            3: "Alignment",
            4: "JIGSAW Single Cell",  # SAG = Single Amplified Genome
            5: "JIGSAW Isolate",
            10: "Resequence",
            100: "Assembly Release" # skip
        }


        if str(seq_unit_name).endswith(".fastq.gz") or str(seq_unit_name).endswith(".fastq"):

            sql = """
select
    s.seq_unit_id,
    s.seq_unit_name,
    s.data_file_size,
    s.segment_id,
    s.raw_reads_count,
    rpq.rqc_pipeline_queue_id,
    rpq.fs_location as run_path,
    rpq.rqc_pipeline_status_id,
    rpq.metadata_id,
    rpt.pipeline_name,
    s.library_name as su_lib_name,
    l.library_name,
    l.seq_proj_name,
    l.seq_prod_name,
    l.seq_proj_id,
    'new' as mode
from seq_units s
left join library_info l on s.rqc_library_id = l.library_id
left join rqc_pipeline_queue rpq on s.seq_unit_id = rpq.seq_unit_id
inner join rqc_pipeline_type_cv rpt on rpq.rqc_pipeline_type_id = rpt.rqc_pipeline_type_id
where
    s.seq_unit_name = %s
            """


            #if status == "complete":
            #    sql += " and rpq.rqc_pipeline_status_id = 16"

        elif str(seq_unit_name).endswith(".srf"):

            sql = """
select
    su.seq_unit_id,
    a.analysis_id,
    a.analysis_type_id,
    a.analysis_dir,
    a.analysis_status_id,
    su.seq_unit_name,
    su.segment_id,
    su.data_file_size,
    su.raw_reads_count,
    su.library_name as su_lib_name,
    lib.library_name,
    lib.seq_proj_name,
    lib.seq_prod_name,
    lib.seq_proj_id,
    '' as metadata_id,
    'legacy' as mode
from m2m_analysis_seq_unit m
inner join rqc_analysis a on m.analysis_id = a.analysis_id
inner join seq_units su on m.seq_unit_name = su.seq_unit_name
inner join library_info lib on su.rqc_library_id = lib.library_id
where
    su.seq_unit_name = %s
            """

            #if status == "complete":
            #    sql += " and a.analysis_status_id in (3,11)"



        #print sql
        #print seq_unit_name

        elif str(seq_unit_name).isdigit(): # data unit id

            sql = """
select
    d.data_unit_id as seq_unit_id,
    d.data_name,
    0 as data_file_size,
    0 as segment_id,
    0 as raw_reads_count,
    rpq.rqc_pipeline_queue_id,
    rpq.fs_location as run_path,
    rpq.rqc_pipeline_status_id,
    rpq.metadata_id,
    rpt.pipeline_name,
    d.str_param2 as su_lib_name,
    l.library_name,
    l.seq_proj_name,
    l.seq_prod_name,
    l.seq_proj_id,
    'new' as mode
from data_units d
left join library_info l on d.str_param2 = l.library_name
left join rqc_pipeline_queue rpq on d.data_unit_id = rpq.data_unit_id
inner join rqc_pipeline_type_cv rpt on rpq.rqc_pipeline_type_id = rpt.rqc_pipeline_type_id
where
    d.data_unit_id = %s
            """

        sth.execute(sql, (seq_unit_name,))

        seq_unit_id = 0 # last seq_unit_id seen

        row_cnt = int(sth.rowcount)



        for _ in range(row_cnt):

            rs = sth.fetchone()



            if seq_unit_id != rs['seq_unit_id']:

                cnt += 1
                read_count = "UNKNOWN"
                lib_name = None

                # bad chars, e.g. UYBU
                if rs['seq_proj_name']:
                    #rs['seq_proj_name'] = re.sub('[^A-Za-z0-9\.\ \(\)\:\,\/\-]', ' ', rs['seq_proj_name'])
                    rs['seq_proj_name'] = rs['seq_proj_name'].encode('ascii', 'ignore').decode('ascii')

                if rs['raw_reads_count']:
                    read_count = "{:,}".format(rs['raw_reads_count'])

                if mode == "quiet":
                    pass
                else:
                    print
                    if 'seq_unit_name' in rs:
                        print "  Seq Unit Name: %s (%s, %s reads), segment_id: %s" % (rs['seq_unit_name'], human_size(rs['data_file_size']), read_count, rs['segment_id'])
                    else:
                        print "      Data Name: %s" % (rs['data_name'])
                    if rs['library_name']:
                        print "   Library name: %s, Product: %s, Project: %s, Seq proj id: %s" % (rs['library_name'], rs['seq_prod_name'], rs['seq_proj_name'], rs['seq_proj_id'])

                    elif rs['su_lib_name']:
                        print "   Library name: %s" % (rs['su_lib_name'])
                        lib_name = rs['su_lib_name']

                seq_unit_id = rs['seq_unit_id']

            run_path = None
            pipeline_name = None

            if rs['mode'] == "legacy":

                pipeline_name = rs['analysis_type_id']
                run_path = rs['analysis_dir']

                if rs['analysis_type_id'] in pipeline_dict:
                    pipeline_name = pipeline_dict[rs['analysis_type_id']]

            else:
                pipeline_name = rs['pipeline_name']
                run_path = rs['run_path']


            if rs['library_name']:
                lib_name = rs['library_name']
            elif rs['su_lib_name']:
                lib_name = rs['su_lib_name']

            # flag to only show if completed
            complete_status_flag = True

            if complete_status == "complete":
                complete_status_flag = False
                if seq_unit_name.endswith(".srf"):
                    if rs['analysis_status_id'] == 3 or rs['analysis_status_id'] == 11:
                        complete_status_flag = True
                else:

                    if rs['rqc_pipeline_status_id'] == 14:
                        complete_status_flag = True




            status = ""
            if run_path:
                if not os.path.isdir(run_path):
                    status = "[NOT ONLINE]"
                    # JAMO?

                    if rs['metadata_id']:
                        status += " - "+jamo_fetch_cmd(rs['metadata_id'])
                        # get jamo fetch command

                # status check
                if complete_status_flag == True:
                    if mode == "quiet":
                        if status != "":
                            run_path = status
                        if 'seq_unit_name' in rs:
                            print "%s,%s,%s,%s,%s" % (rs['seq_unit_name'], lib_name, rs['seq_proj_id'], pipeline_name, run_path)
                        else:
                            print "%s,%s,%s,%s,%s" % (rs['data_name'], lib_name, rs['seq_proj_id'], pipeline_name, run_path)
                    else:
                        print "%15s: %s  %s" % (pipeline_name, run_path, status)



        if row_cnt == 0:
            if mode != "quiet":
                # not always correct: if itags it looked for pooled library which doesn't have library info
                print
                print "*** SEQ UNIT NAME %s: NOT FOUND" % (seq_unit_name)

        if mode != "quiet":
            print


    return cnt


'''
Create the jamo restore command for a run folder

'''
def jamo_fetch_cmd(metadata_id):

    fetch_cmd = ""
    file_id = ""
    file_name = ""

    if metadata_id:

        sdm_url = "https://sdm2.jgi-psf.org"
        sdm_api = "/api/metadata/file/[metadata_id]"

        sdm_api = sdm_api.replace("[metadata_id]", metadata_id)

        #print sdm_api
        curl = Curl(sdm_url)

        response = None
        try:
            response = curl.get(sdm_api)

        except CurlHttpException as e:
            #pass
            print e.response

            #transLog.error("- Failed SDM API Call: %s/%s, %s  %s", sdmURL, sdmRawFileAPI, e, RQCGlobals.msgFail)
        except Exception as e:
            #pass
            print e.args

        if response:
            #print response
            if type(response) == str:
                response = json.loads(response)
            #sys.exit(44)
            if 'file_id' in response:
                #print "****** %s" % type(response['file_id']) # weird response in denovo
                fetch_cmd = "jamo fetch custom file_id=%s" % (str(response['file_id']))
                file_id = response['file_id']
                
            if 'current_location' in response:
                file_name = response['current_location']

    fetch_cmd += " # restore to: %s" % file_name
    
    # special Bryce output - used for Bob requesting a bunch of run folders from tape and restoring to scratch
    #fetch_cmd = "*** %s|%s|%s" % (metadata_id, file_id, file_name)
    
    return fetch_cmd


'''
Returns list of seq unit names from a list of seq_unit_ids

@param seqUnitIdList: list of seq unit ids from seq_units.seq_unit_id
@return seqUnitNames = list of seq unit names found in the database
'''
def get_seq_unit_names_by_id(seq_unit_id_list):

    seq_unit_names_list = []
    if seq_unit_id_list:


        sql = "select seq_unit_id, seq_unit_name from seq_units where model_id in (9,11) and seq_unit_id in "

        sql_list = []
        sql_val = []

        for seq_unit_id in seq_unit_id_list:
            sql_list.append("%s") # add placeholder
            sql_val.append(seq_unit_id) # add value

        sql += "(" + ",".join(sql_list) + ")"


        sth.execute(sql, (sql_val))

        row_cnt = int(sth.rowcount)

        found_dict = {} # dictionary of seq unit ids that we found out of the requested list

        for _ in range(row_cnt):

            rs = sth.fetchone()
            seq_unit_names_list.append(rs['seq_unit_name'])

            if found_dict.has_key(rs['seq_unit_id']):
                found_dict[str(rs['seq_unit_id'])] += 1
            else:
                found_dict[str(rs['seq_unit_id'])] = 1



        # report any ids not found
        for seq_unit_id in seq_unit_id_list:

            if found_dict.has_key(str(seq_unit_id)):
                pass
            else:
                print "*** SEQ UNIT ID %s NOT FOUND" % (seq_unit_id)

    return seq_unit_names_list


'''
Return list of seq unit names from
'''
def get_seq_unit_names_by_lib(lib_list):

    seq_unit_names = []
    if lib_list:


        sql = "select seq_unit_name, library_name from seq_units where model_id in (9,11) and library_name in "

        sql_list = []
        sql_val = []
        for lib in lib_list:
            sql_list.append("%s") # add placeholder
            sql_val.append(lib) # add value

        sql += "(" + ",".join(sql_list) + ")"


        sth.execute(sql, (sql_val))
        row_cnt = int(sth.rowcount)

        found_dict = {} # dictionary of libs that we found out of the requested list

        for _ in range(row_cnt):

            rs = sth.fetchone()
            seq_unit_names.append(rs['seq_unit_name'])

            if found_dict.has_key(rs['library_name']):
                found_dict[str(rs['library_name']).lower()] += 1
            else:
                found_dict[str(rs['library_name']).lower()] = 1


        # data units, str_param2 normally library_name
        sql = "select data_unit_id, str_param2 as library_name from data_units where owner = 'rqc' and str_param2 in "
        sql_list = []
        sql_val = []
        for lib in lib_list:
            sql_list.append("%s") # add placeholder
            sql_val.append(lib) # add value

        sql += "(" + ",".join(sql_list) + ")"

        sth.execute(sql, (sql_val))
        row_cnt = int(sth.rowcount)


        for _ in range(row_cnt):

            rs = sth.fetchone()
            seq_unit_names.append(rs['data_unit_id'])

            if found_dict.has_key(rs['library_name']):
                found_dict[str(rs['library_name']).lower()] += 1
            else:
                found_dict[str(rs['library_name']).lower()] = 1



        # report any ids not found
        for lib in lib_list:

            if found_dict.has_key(str(lib).lower()):
                pass
            else:
                print "*** LIBRARY %s NOT FOUND" % (lib)



    return seq_unit_names

'''
Get list of seq units based on sequencing project id

'''
def get_seq_unit_names_by_spid(spid_list):

    seq_unit_names = []
    if spid_list:


        sql = """
select
    s.seq_unit_name,
    l.seq_proj_id
from seq_units s
inner join library_info l on s.rqc_library_id = l.library_id
where
    l.seq_proj_id in
        """

        sql_list = []
        sql_val = []
        for spid in spid_list:
            sql_list.append("%s") # add placeholder
            sql_val.append(spid) # add value

        sql += "(" + ",".join(sql_list) + ")"


        sth.execute(sql, (sql_val))
        row_cnt = int(sth.rowcount)

        found_dict = {} # dictionary of libs that we found out of the requested list

        for _ in range(row_cnt):

            rs = sth.fetchone()
            seq_unit_names.append(rs['seq_unit_name'])

            if found_dict.has_key(rs['seq_proj_id']):
                found_dict[str(rs['seq_proj_id']).lower()] += 1
            else:
                found_dict[str(rs['seq_proj_id']).lower()] = 1



        # normally int_param1 = seq proj id
        sql = "select d.data_unit_id, d.int_param1 as seq_proj_id from data_units d where d.owner = 'rqc' and d.int_param1 in "

        sql_list = []
        sql_val = []
        for spid in spid_list:
            sql_list.append("%s") # add placeholder
            sql_val.append(spid) # add value

        sql += "(" + ",".join(sql_list) + ")"

        sth.execute(sql, (sql_val))
        row_cnt = int(sth.rowcount)

        found_dict = {} # dictionary of libs that we found out of the requested list

        for _ in range(row_cnt):

            rs = sth.fetchone()
            seq_unit_names.append(rs['data_unit_id'])

            if found_dict.has_key(rs['seq_proj_id']):
                found_dict[str(rs['seq_proj_id']).lower()] += 1
            else:
                found_dict[str(rs['seq_proj_id']).lower()] = 1



        # report any ids not found
        for spid in spid_list:

            if found_dict.has_key(str(spid).lower()):
                pass
            else:
                print "*** SEQ PROJECT ID %s NOT FOUND" % (spid)



    return seq_unit_names




# curl http://proposals.jgi-psf.org/pmo_webservices/analysis_projects?sequencing_project_id=1044852
def show_analysis_project(seq_proj_id):


    cmd = "%s --spid %s" % (os.path.join(dir, "ap_tool.py"), seq_proj_id)

    std_out, std_err, exit_code = run_command(cmd, True)
    post_mortem_cmd(cmd, exit_code, std_out, std_err, None)

    print std_out

    return



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## main program



scriptName = __file__

if __name__ == "__main__":

    version = "1.3.4"

    # Parse options
    usage = "run_folder.py [options]\n"
    usage += """
run_folder looks up the pipeline run folders for a seq unit, library or sequencing project id.

Look up run folders for a list of seq units:
$ ./run_folder.py --seq-units 7274.7.65872.TAGCTT.fastq.gz,7275.3.65879.ACTGAT.fastq.gz,7275.7.65880.TAGCTT.fastq.gz

Look up run folders for a list of libraries:
$ ./run_folder.py --lib-names OZWG,OSZC

Look up run folders for one sequencing project id (spid), output in quiet format
$ ./run_folder.py --spid 1014865 --quiet

Look up run folders for sequencing project id and show the analysis project & tasks
$ ./run_folder.py -spid 1060131 -ap
   
    
    """


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-s", "-su", "--seq_units", "--seq-units", "--seq_unit", "--seq-unit", dest="seq_unit", help = "Comma separated list of seq unit names (.fastq.gz)")
    parser.add_argument("-suid", "--seq_unit_ids", "--seq-unit-ids", dest="seq_unit_ids", help = "Comma separated list of seq unit ids (from RQC database)")
    parser.add_argument("-lib", "--lib-list", "--lib-names", "--lib", dest="lib_names", help = "Comma separated list of library names")
    parser.add_argument("-spid", "--spid", "--seq-proj-id", dest="spid_list", help = "Comma separated list of sequencing project ids")
    parser.add_argument("-c", "--complete", default = False, action = "store_true", dest="status_flag", help = "Show only complete runs")
    #parser.add_argument("-contig", "--contigs", dest="contig", help = "Find assembly contig files")
    parser.add_argument("-ap", "--analysis-project", dest="ap_flag", default = False, action = "store_true", help = "Look up analysis project info for a sequencing project id")
    parser.add_argument("-q", "--quiet", dest="quiet", default=False, action="store_true", help = "minimal output")
    parser.add_argument('-v', '--version', action='version', version=version)


    args = parser.parse_args()


    seq_unit_name_list = []
    seq_unit_id_list = []
    library_list = []
    seq_proj_id_list = []
    complete_status = None
    ap_flag = False

    mode = None # "quiet" = special output for Kecia

    if args.seq_unit:
        seq_unit_name_list = re.split(r'[,;\s]+', args.seq_unit)

    if args.lib_names:
        library_list = re.split(r'[,;\s]+', args.lib_names)

    if args.seq_unit_ids:
        seq_unit_id_list = re.split(r'[,;\s]+', args.seq_unit_ids)

    if args.spid_list:
        seq_proj_id_list = re.split(r'[,;\s]+', args.spid_list)

    if args.ap_flag:
        ap_flag = args.ap_flag

    if args.status_flag == True:
        complete_status = "complete"


    if args.quiet == True:
        mode = "quiet"


    if mode != "quiet":
        print "~" * 100
        print "RQC Run Folder Tool: %s" % version
        print

    # database connection (read only)
    db = jgi_connect_db("rqc")



    if db == None:
        print "Error: Cannot open database connection!"
        sys.exit(4)

    sth = db.cursor(MySQLdb.cursors.DictCursor)



    # convert seq_unit_ids into their names (if in seq_units table)
    if len(seq_unit_id_list) > 0:
        seq_unit_name_list_id = get_seq_unit_names_by_id(seq_unit_id_list)

        # append seq units to the existing list
        new_list = seq_unit_name_list + seq_unit_name_list_id
        seq_unit_name_list = new_list

    if len(library_list) > 0:
        seq_unit_name_list_lib = get_seq_unit_names_by_lib(library_list)

        new_list = seq_unit_name_list + seq_unit_name_list_lib
        seq_unit_name_list = new_list

    if len(seq_proj_id_list) > 0:

        seq_unit_name_list_spid = get_seq_unit_names_by_spid(seq_proj_id_list)

        new_list = seq_unit_name_list + seq_unit_name_list_spid
        seq_unit_name_list = new_list


    # pacbio
    #print jamo_fetch_cmd("54af1c1a0d8785565d470e4a")
    #run_folder_path(1242, complete_status, mode)
    #sys.exit(1)

    if ap_flag == True:

        for seq_proj_id in seq_proj_id_list:
            show_analysis_project(seq_proj_id)

    cnt = 0

    if mode == "quiet":
        print "Seq Unit Name,Library Name, Seq Proj Id,Pipeline Name,Run Folder"

    for seq_unit_name in seq_unit_name_list:
        cnt += run_folder_path(seq_unit_name, complete_status, mode)


    if mode != "quiet":
        print
        print "Found %s seq units" % (cnt)
        print

    db.close()


    sys.exit(0)
