#!/usr/bin/env python
'''
    PacBio triangle read finder.
    Ref: GAA-1957
    A triangle read is a read that joins the 2 complimentary strands togather.
    eg :
            ACTGACTGGCATT
            TGACTGACCGTAA     => TGACTGACCGTAA-ACTGACTGGCATT

    module load blat;
    blat -noHead -minMatch=3 -tileSize=8 -repMatch=10000 -minIdentity=75 -extendThroughN TARGET QUERY stdout |
    awk '{if ( ($9=="-") && ($10==$14) && ( ($13-$12) > 400 ) && ($12==$16) && ($13==$17) && ( ( ($2+$6)/($13-$12) ) < 0.25 ) ) print}'
    (1) ($9=="-") ensures that we have a negative alignment
    (2) ($10==$14) target and query ID are the same (self alignment)
    (3) ($13-$12) > 400 at least 400bp section of the query participates in the triangle
    (4) ($12==$16) This is to ensure that the target and query start are the same position
    (5) ( ($2+$6)/($13-$12) ) < 0.25 This is a quality measure that ensures that the number of (mismatches + query gaps) is not too large.

    TARGET and QUERY are the same fasta file.

    Take input of a fasta file, and an optional batch size (-i FASTA -b BSIZE), run blat with input of BSIZE records.
    Results go to stdout

    ----------
    Shijie Yao
    12/09/2015 : initial implementation
    10/25/2016 : multithreading -
                 test input /global/seqfs/sdm/prod/pacbio/jobs/030/030456/data/filtered_subreads.fasta
                 on exclusive.q :
                 serial version ran for > 5
                 MT with 8 thread : 25m (35G)
                 MT with 16 thread : 35m (40G)

'''
import sys
import os
import re
import time
import collections
import re
import gzip

from argparse import ArgumentParser
import pprint
from threading import Thread
import threading

ROOT_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), os.path.pardir)
LIB_DIR = os.path.join(ROOT_DIR, 'lib')
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from common import run_command

class bcolors:
    RED = '\033[1;31;40m'
    GREEN = '\033[1;32;40m'
    YELLOW = '\033[1;33;40m'
    BLUE = '\033[1;34;40m'
    PURPLE = '\033[1;35;40m'
    CYAN = '\033[1;36;40m'
    WHITE = '\033[1;37;40m'
    BLACK = '\033[2;30;47m'
    ENDC = '\033[0m'

LOG_FILE = None
def fasta_record_generator(fname):
    if fname.endswith('.gz'):
        fh = gzip.open(fname)
    else:
        fh = open(fname, 'r')

    if fh:
        lcnt = 0
        read = ''
        header = ''
        for line in fh: #PE vs SE? fastq vs fasta?
            line = line.strip()
            lcnt += 1

            if line.startswith('>'):
                if read:
                    yield '%s\n%s' % (header, read)
                read = ''
                header = line
            else:
                read += line

        #- last read for fasta format
        if read:
            yield '%s\n%s' % (header, read)
    fh.close()

'''
fh : output file handle
'''
def outstream(msg, stdout, fh=None):
    if stdout:
        print(msg)
    if fh:
        fh.write('%s\n' % msg)


'''

'''
BLOCK_CNT = 0
def do_blat(reads, stats, outs, blockcnt, clean):
    st = time.time()
    rootdir = os.environ['SCRATCH'] # use user's scratch space
    fname = 'pb_%d_%s_%d' % (os.getpid(), time.time(), blockcnt)
    ftmp = os.path.join(rootdir, fname)
    # while os.path.isfile(ftmp + '.fasta'): # make this filename unique
    #     ftmp += '0'

    ftmp += '.fasta'
    wlist = []  # white list
    blist = []  # black list
    for read in reads:
        header = read.split('\n')[0].strip()
        wlist.append(header.split()[0][1:]) #read id

    with open(ftmp, 'w') as fh:
        fh.write('\n'.join(reads))
        #cmd blat

    ## TODO: test and replace blat with pblat
    ## ref) https://github.com/icebert/pblat
    blat = 'module load blat;blat -noHead -minMatch=3 -tileSize=8 -repMatch=10000 -minIdentity=75 -extendThroughN %s %s stdout' % (ftmp, ftmp)

    # blat = "/global/homes/s/sulsj/work/bitbucket-repo/jgi-rqc-pipeline/tools/pblat/pblat -noHead -minMatch=3 -tileSize=8 -repMatch=10000 -minIdentity=75 -extendThroughN -threads=16 %s %s stdout" % (ftmp, ftmp))

    blat += '| awk \'{ if ( ($9=="-") && ($10==$14) && ( ($13-$12) > 400 ) && ($12==$16) && ($13==$17) && ( ( ($2+$6)/($13-$12) ) < 0.25 ) ) print }\''
    #blat += '| awk \'{ if ($9=="-") print }\''

    global BLOCK_CNT
    # BLOCK_CNT += 1
    # print('%d) CMD : %s\n' % (BLOCK_CNT, blat))
    stdOut, stdErr, exitCode = run_command(blat, True)
    if exitCode == 0:
        stdOut = stdOut.strip()
        if stdOut:
            rects = stdOut.strip().split('\n')
            for hit in rects:
                hit = hit.strip()
                if hit:
                    tok = hit.split()
                    read_header = tok[9]
                    if read_header in stats['black_list'] or read_header in blist:  # already counted, must be >1 match in self mapping
                        continue

                    # tok[9] is T seq header, in format of m140619_200815_42175_c000583142559900001500000112311637_s1_p0/72270/5280_11491
                    if read_header in wlist:
                        wlist.remove(read_header)
                    blist.append(read_header)

                    tok2 = read_header.split('/')
                    cellid = tok2[0]
                    zmwid = tok2[1] # the ZMW id (e.g. 72270)

                    stats['triangle_reads'] += 1
                    # print('tri read count = %d' % stats['triangle_reads'])
                    if cellid in stats['triangle_reads_in_zmw'] and zmwid in stats['triangle_reads_in_zmw'][cellid]:
                        stats['triangle_reads_in_zmw'][cellid][zmwid] += 1
                    elif cellid in stats['triangle_reads_in_zmw']:
                        stats['triangle_reads_in_zmw'][cellid][zmwid] = 1
                    else:
                        stats['triangle_reads_in_zmw'][cellid] = {zmwid: 1}

                    #outstream(hit, None, outs['out_file_fh'])
    if clean:
        os.remove(ftmp)

    if blist:
        stats['black_list'] += blist;

    if wlist:
        stats['white_list'] += wlist;

    run_command('echo \"  - block time : %d > %s' % ((time.time() - st), LOG_FILE), True)

def run_thread(reads, stats, outs, blockcnt, clean):
    t = Thread(target=do_blat, args=(reads, stats, outs, blockcnt, clean))
    t.start()
    return t

def do_driver(flist, bsize, outs, thread, clean):
    rootdir = os.environ['SCRATCH']
    block = []
    blockcnt = 0
    rcnt = 0

    ## prep ouput file handles:
    # if outs['out_file']:
    #     outs['out_file_fh'] = open (outs['out_file'], 'w')

    if outs['out_stats_file']:
        if outs['out_stats_file'] == outs['out_file'] and outs.get('out_file_fh'):
            outs['out_stats_file_fh'] = outs['out_file_fh']
        else:
            outs['out_stats_file_fh'] = open (outs['out_stats_file'], 'w')

    stats = {   'triangle_reads_in_zmw':{},
                'zmw_cnt':{},
                'triangle_reads': 0,
                'white_list' : [],  # element in form of m140619_200815_42175_c000583142559900001500000112311637_s1_p0/72270 - identify unique hole
                'black_list' : [], # element in form of m140619_200815_42175_c000583142559900001500000112311637_s1_p0/72270/5280_11491 - identify unique read (Alex's clean script can use this)
            }

    waitt = 0.0001 # waiting time (sec)
    wtotal = 0  # total waiting time (sec)
    for f in flist:
        for cnt, rec in enumerate(fasta_record_generator(f)):
            header = rec.split('\n')[0].strip()

            tok = header.split()[0].split('/')
            #print(tok)
            if len(tok) < 2:
                print('\n%s' % bcolors.RED)
                print('ERROR : the header does not in std pacbio format.')
                print('FOUND : %s' % header)
                print('EXPECT: >SMRT_CELL_ID/ZMW_ID/X_Y')
                print('Cannot continue, exit')
                print('%s' % bcolors.ENDC)
                sys.exit(1)

            cellid = tok[0][1:]
            zmwid = tok[1]

            if cellid in stats['zmw_cnt'] and zmwid in stats['zmw_cnt'][cellid]:
                stats['zmw_cnt'][cellid][zmwid] += 1
            elif cellid in stats['zmw_cnt']:
                stats['zmw_cnt'][cellid][zmwid] = 1     # if cellid is not new, but zmwid is new - create a new dict of zmwid with init value of 1
            else:
                stats['zmw_cnt'][cellid] = {zmwid: 1}   # if cellid is new - add new key to stats['zmw_cnt'] and init a new dict

            rcnt += 1
            block.append(rec)
            if bsize == 1 or (cnt > 0 and (cnt+1) % bsize == 0):
                #fire blat job on the block
                while threading.active_count() > thread:
                    time.sleep(waitt)
                    wtotal += waitt


                run_thread(block, stats, outs, blockcnt, clean)
                blockcnt += 1
                block = []
                continue
    # last truncated block
    if block:
        blockcnt += 1
        run_thread(block, stats, outs, blockcnt, clean)

    while threading.active_count() > 1:     # this program is also a thread
        time.sleep(waitt)
        wtotal += waitt


    stats['total_read_count'] = rcnt
    stats['tmp_file_count'] = blockcnt

    #pprint.pprint(stats)
    msg = ['\nsummary:']
    msg.append('========')
    msg.append('files processed:')
    for ifile in fasta_list:
        msg.append(' - %s' % ifile)

    #pprint.pprint(stats)
    for cellid in stats['zmw_cnt'].keys():
        msg.append('~' * 65)
        msg.append('cellid: %s' % cellid)
        msg.append('\ntotal_reads\ttriangle_reads\ttriangle_read_pct')
        msg.append('-------------------------------------------------')
        msg.append('%-11d\t%-14d\t%-14.5f' % (stats['total_read_count'], stats['triangle_reads'], (stats['triangle_reads']*100.0/(stats['total_read_count']))))
        msg.append('-------------------------------------------------')
        total_zmw_cnt = len(stats['zmw_cnt'][cellid])
        total_zmw_with_triangle_reads = 0
        if cellid in stats['triangle_reads_in_zmw']:
            total_zmw_with_triangle_reads = len(stats['triangle_reads_in_zmw'][cellid])

        msg.append('\ntotal_zmw\tzmw_with_triangle_reads\tpct')
        msg.append('-----------------------------------------------------------------')
        msg.append('%-9d\t%-23d\t%-.4f' % (total_zmw_cnt, total_zmw_with_triangle_reads, (total_zmw_with_triangle_reads*100.0)/total_zmw_cnt))
        msg.append('-----------------------------------------------------------------')

        msg.append('~' * 65)
        msg.append('\n')


    blistfh = None
    blistfh2 = None
    if outs['out_file']:
        dname = os.path.dirname(outs['out_file'])
        fname = os.path.basename(outs['out_file'])
        fnbase = fname.rsplit('.', 1)[0]

        blistfn = os.path.join(dname, '%s.blacklist' % fnbase)
        blistfn2 = os.path.join(dname, '%s.blacklist2' % fnbase)
        whitefn = os.path.join(dname, '%s.whitelist' % fnbase)
        # print(blistfn)
        # print(blistfn2)
        # print(whitefn)

        wlistfh = open(whitefn, 'w')
        for aid in stats['white_list']:
            wlistfh.write('%s\n' % aid)

        blistfh = open(blistfn, 'w')
        blistfh2 = open(blistfn2, 'w')

    for aid in stats['black_list']:
        #msg.append(aid)
        if blistfh2:
            blistfh2.write('%s\n' % aid)
        if blistfh:
            blistfh.write('%s\n' % '/'.join(aid.split('/')[:-1]))

    outstream('\n'.join(msg), None, outs['out_stats_file_fh'])


    #pprint.pprint(stats)
    print('total waiting time : %.3f (sec)' % wtotal)
    return True

def err_exit(msg):
    print('\n%s\n' % msg)
    sys.exit(1)

def do_log(msg):
    if LOG_FILE:
        run_command('echo \"%s\" >> %s' % (msg, LOG_FILE), True)
    print(msg)

if __name__ == '__main__':
    NAME = 'PB triangle read finder'
    VERSION = '1.0.0'

    USAGE = 'prog [options]\n'
    USAGE += '* %s, version %s\n' % (NAME, VERSION)

    JOB_CMD = ' '.join(sys.argv)
    PARSER = ArgumentParser(usage=USAGE)
    PARSER.add_argument('-i', '--in', dest='input', help='fasta or a directory containing fastq files, fastq file can be in gz format', required=True)
    PARSER.add_argument('-d', '--dir', dest='odir', help='output dir')
    PARSER.add_argument('-o', '--out', dest='output', help='output file name. will be in -d or cwd', required=True)
    PARSER.add_argument('-s', '--stats', dest='stats', help='stats file. will be in 0d or cwd, will be -o.summary if not provided.')
    PARSER.add_argument('-b', '--batch-size', dest='bsize', default=50, help='batch size, default to 20 (optimum)')
    PARSER.add_argument('-t', '--thread', dest='thread', default=30, help='number of thread to use, default to 30')
    PARSER.add_argument('-nc', '--no-clean', dest='clean', default=True, action='store_false', help='Cleanup batched fasta files.')
    PARSER.add_argument('-v', '--version', action='version', version=VERSION)

    ARGS = PARSER.parse_args()

    if not ARGS.input or not (os.path.isdir(ARGS.input) or os.path.isfile(ARGS.input)):
        err_exit('Wrong input %s' % ARGS.input)

    outs = {}   #output options

    wdir = os.getcwd()
    if ARGS.odir:
        wdir = os.path.abspath(ARGS.odir)
        if not os.path.isdir(wdir):
            os.makedirs(wdir)
            if not os.path.isdir(wdir):
                print('Failed to create the out put dir: %s' % wdir)
                exit(1)

    ofile = ARGS.output
    if not ofile.startswith('/') and not ofile.startswith('~/'):
        ofile = os.path.join(wdir, ofile)
    outs['out_file'] = ofile

    sfile = ARGS.stats
    if sfile:
        if not sfile.startswith('/') and not sfile.startswith('~/'):
            sfile = os.path.join(wdir, sfile)
    else:
        fname = os.path.basename(ofile)
        fnbase = fname.rsplit('.', 1)[0]
        sfile = os.path.join(wdir, '%s.summary' % fnbase)

    outs['out_stats_file'] = sfile

    LOG_FILE = os.path.join(wdir, 'pb_triangle_read_finder.log')

    VALID_FILE_EXT = ('fa', 'fasta', 'fa.gz', 'fasta.gz')
    fasta_list = []
    if os.path.isfile(ARGS.input):
        if ARGS.input and re.search(r'\.(%s)$' % '|'.join(VALID_FILE_EXT), ARGS.input):
            fasta_list.append(ARGS.input)
        else:
            err_exit('Input file must be a fasta file (can be compressed), recorgnized by file extension %s.' % '|'.join(VALID_FILE_EXT))
    else:
        flist = os.listdir(ARGS.input)
        for fname in flist:
            fname = os.path.realpath(os.path.join(ARGS.input, fname))
            if re.search(r'\.(%s)$' % '|'.join(VALID_FILE_EXT), fname):
                fasta_list.append(fname)

    if not fasta_list:
        err_exit('No fasta fils found in the given input [%s].' % ARGS.input)

    try:
        bsize = int(ARGS.bsize)
        if bsize < 1:
            err_exit('batch size must be >=1 [%s]' % ARGS.bsize)
    except:
        err_exit('batch size must be int [%s]' % ARGS.bsize)

    try:
        thread = int(ARGS.thread)
        if thread < 1:
            err_exit('thread must be >=1 [%s]' % ARGS.thread)
    except:
        err_exit('thread must be int [%s]' % ARGS.thread)

    st = time.time()

    do_driver(fasta_list, bsize, outs, thread, ARGS.clean)

    do_log('Total time : %d' % ((time.time() - st)))


