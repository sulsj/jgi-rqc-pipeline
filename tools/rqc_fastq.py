#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import re
import gzip
import MySQLdb
import argparse

pipe_root = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/' + os.pardir)
sys.path.append(pipe_root + '/lib')   # common

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from common import run_command
from db_access import jgi_connect_db
from os_utility import get_tool_path, run_sh_command


"""
These are functions for fastq file analysis


"""

def _err_return(msg, log=None):
    if log:
        log.error(msg)
    print msg
    return None


'''

For a given fastq file (zipped or unzipped), check the format by comparing the string lengths of seq and quality

'''
def check_fastq_format(fastq, log=None):
    if not fastq:
        return _err_return('Function read_length_from_file() requires a fastq file.', log)

    if not os.path.isfile(fastq):
        return _err_return('The given fastq file [%s] does not exist' % fastq, log)

    if fastq.endswith(".gz"):
        fh = gzip.open(fastq, 'r')
    else:
        fh = open(fastq, 'r')

    lCnt = 0    # line counter
    seq = None
    lstr = None
    missing = False

    while 1:
        lstr = fh.readline()
        if not lstr:
            break

        lCnt += 1
        seqLen = 0
        qualLen = 0

        idx = lCnt % 4  # line index in the 4-line group

        if idx == 1:
            continue
        elif idx == 2:
            seq = lstr.strip()
        elif idx == 3:
            continue
        elif idx == 0:
            seqLen = len(seq)
            qualLen = len(lstr.strip())

            if seqLen != qualLen:
                missing = True
                break

    fh.close()

    if missing:
        log.error("Incorrect fastq file: missing quality score character or base character.\n seq=%s\n qual=%s" % (seq, lstr))
        return -1

    if lCnt % 4 != 0:
        log.error("Incorrect fastq file: missing fastq record item. Number of lines in the output fastq file = %d" % (lCnt))
        return -2


    return lCnt



'''
    Ref : JGI_Utility::read_length_from_file
    The first read count (read 1 and read 2) meets the sszie will stop the record reading.
'''
def read_length_from_file(fastq, log=None, ssize=10000):
    ''' For a given fastq file (zipped or unzipped), reads the first *ssize* reads to compute average length
        and return (avgLen1, avgLen2, isPE)
        The file scanning will stop when the ssize is met by either read (1 or 2).
    '''

    is_pe = False   # def to none paired end
    readi = 0   # counter for pe read 1
    readj = 0   # counter for pe read 2
    leni = 0    # total length of read 1
    lenj = 0    # total length of read 2

    if not fastq:
        return _err_return('Function read_length_from_file() requires a fastq file.', log)

    if not os.path.isfile(fastq):
        return _err_return('The given fastq file [%s] does not exist' % fastq, log)

    if fastq.endswith(".gz"):
        fh = gzip.open(fastq, 'r')
    else:
        fh = open(fastq, 'r')

    lCnt = 0    # line counter
    done = False

    while not done:
        lstr = fh.readline()
        lstr = lstr.rstrip()

        lCnt += 1
        idx = lCnt % 4  # line index in the 4-line group
        if idx == 1:
            header = lstr
        elif idx == 2:
            seq = lstr
        elif idx == 3:
            plus = lstr
        elif idx == 4:
            quality = lstr
        else:
            if not header or not seq:   # end of file
                done = True

            if header.find('enter') != -1 and header.find('exit') != -1:    # copied from perl's logic
                continue

            match = re.match(r'^@([^/]+)([ |/]1)', header)  # for pe read 1
            aLen = len(seq.strip())
            if match:
                # to let read2 match up
                readi += 1
                if readi > ssize:   #read 1 meet the max count; done regardless situations of read2 count.
                    readi -= 1
                    done = True
                else:
                    leni += aLen
            else:
                match = re.match(r'^@([^/]+)([ |/]2)', header)  # for pe read 2
                if match:
                    readj += 1
                    if readj > ssize:   #read2 meet the max count; done regardless situation of read1 count
                        readj -= 1
                        done = True
                    else:
                        lenj += aLen
                    if leni > 0:  ### Only set is_pe to true if leni > 0, which means the 1st read in the pair was found
                        is_pe = True
    fh.close()

    # debug to be sure the max are met properly for both reads
    #print('read1len=%d; read1cnt=%d; read2len=%d; read2cnt=%d' %(leni, readi, lenj, readj))
    if leni > 0 and readi > 0:
        leni = leni / readi
    if lenj > 0 and readj > 0:
        lenj = lenj / readj

    return (leni, lenj, is_pe)


def get_working_read_length(fastq, log):
    read_length = 0
    read_length_1 = 0
    read_length_2 = 0

    (read_length_1, read_length_2, is_pe) = read_length_from_file(fastq, log)

    if not is_pe:
        log.info("It is NOT pair-ended. read_length_1 %s" % (read_length_1))
        ## for hudson data
        read_length = read_length_1 if read_length_1 > 0 else read_length_2
            
    else:
        log.info("It is pair-ended. read_length_1 %s read_length_2 %s" % (read_length_1, read_length_2))
        if read_length_1 != read_length_2:
            log.warning("The lengths of read 1 (" + str(read_length_1) + ") and read 2 (" + str(read_length_2) + ") are not equal")

        if read_length_1 < read_length_2:
            read_length = read_length_1
        else:
            read_length = read_length_2

        #if read_length < 10:
        #    log.error("File name: " + fastq + ". Read length is less than 10 bps. Is this paired end? " + str(is_pe) + ". Read one length: " + str(read_length_1) + "; Read two length: " + str(read_length_2) )
        #    return (0, read_length_1, read_length_2, is_pe)

    return (read_length, read_length_1, read_length_2, is_pe)



def read_count(fpath):
    'return the raw read count in a fastq file, assuming each record occupy 4 lines in file.'
    if not os.path.isfile(fpath):
        return None

    if fpath.endswith(".gz"):
        cmd = 'zcat %s | wc -l' % fpath
    else:
        cmd = 'cat %s | wc -l' % fpath

    stdOut, _, _ = run_command(cmd, True)

    lcnt = int(stdOut.strip())

    #print('read_count for %s; cmd=[%s]line cnt=%d' % (fpath, cmd, lcnt))

    ## 09.23.2015 sulsj
    ## The assumption is weak. Should return error code if lcnt % 4 != 0
    # if lcnt % 4 != 0:
    #     ## incorrect fastq file format
    #     return -1
    
    if lcnt > 4:
        return lcnt / 4
    elif lcnt == 0:
        return 0
    else:
        return None


"""
Count the number of reads from input fastq using reformat.sh


"""
def read_count2(fpath, log):
    reformatTool = get_tool_path("reformat.sh", "bbtools")
    cmd = "%s in=%s out=null" % (reformatTool, fpath)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)
    assert exitCode == 0
    numReads = -1

    for l in stdErr.split('\n'):
        ## ex) Input:                  	87120204 reads          	13155150804 bases
        if l.startswith("Input:"):
            numReads = int(l.split()[1])
            break
        
    return numReads
    
    
def is_clip_pe(seqUnit, db=None, log=None):
    'Return True/False for a given seq unit. Return None on error.'

    # logic copied from JGI_QC_Utility::is_clip_pe_library

    if not seqUnit:
        return _err_return('Require a valid seqUnit in calling is_clip_pe(seqUnit, db=None, log=None)', log)

    if not db:
        db = jgi_connect_db("rqc")

        if db is None:
            return _err_return('Error!  Cannot open database connection!', log)

    sth = db.cursor(MySQLdb.cursors.DictCursor)

    sql = '''
SELECT
    L.library_protocol,
    L.library_name,
    L.library_type,
    S.is_multiplex
FROM library_info as L
inner join seq_units as S on L.library_id = S.rqc_library_id
WHERE
    S.seq_unit_name LIKE %s
        '''

    seqUnit = seqUnit.replace('.srf', '')
    sth.execute(sql, (seqUnit + '%', ))
    rs = sth.fetchone()

    if not rs:
        return _err_return('No record for the given seq unit[%s] in RQC database' % seqUnit, log)

    multiplex = rs['is_multiplex']
    lib_name = rs['library_name']
    lib_type = rs['library_type']
    lib_protocol = rs['library_protocol']

    if not lib_name:    # has to have a library name
        return _err_return('No library name for [seqUnit]', log)

    if multiplex:       # clipPE cant be mulitiplex
        return False

    if lib_type != 'Illumina CLIP PE' and lib_protocol != 'CLIP-PE':
        return False

    return True



def is_rna(seqUnit, db=None, log=None):
    'Return True/False for a given seq unit. Return None on error.'

    # logic copied from JGI_QC_Utility::illumina_is_library_rna

    if not seqUnit:
        return _err_return('Require a valid seqUnit in calling is_clip_pe(seqUnit, db=None, log=None)', log)

    if not db:
        db = jgi_connect_db("rqc")

        if db is None:
            return _err_return('Error!  Cannot open database connection!', log)

    sth = db.cursor(MySQLdb.cursors.DictCursor)

    sql = '''
SELECT
    L.sow_item_type,
    L.library_name
FROM library_info L
INNER JOIN seq_units S on L.library_id = S.rqc_library_id AND S.seq_unit_name LIKE %s
        '''

    seqUnit = seqUnit.replace('.srf', '')
    sth.execute(sql, (seqUnit + '%', ))
    rs = sth.fetchone()

    if not rs:
        return _err_return('No record for the given seq unit[%s] in RQC database' % seqUnit, log)

    #print('LIBNAME=' + rs['library_name'])
    if rs['sow_item_type']:
        #print('HAS SOW_ITEM_TYPE')
        if rs['sow_item_type'] == 'RNA':
            return True
        else:
            return False
    elif len(rs['library_name']) == 4 and rs['library_name'][:1] == 'C':
        return True

    return False





## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## main program

if __name__ == "__main__":

    # test
    #fastq = "/global/dna/dm_archive/rqc/filtered_seq_unit//00/01/05/82/10582.7.167444.GATCAG.anqrpht.fastq.gz"
    #print read_length_from_file(fastq)
    #sys.exit(13)

    my_name = "rqc_fastq utility"
    version = "1.0.0"

    # Parse options
    usage = "* rqc_fastq unitily, version %s\n" % (version)
    CMD_LIST = ['read_count', 'read_length_from_file', 'get_working_read_length', 'is_clip_pe', 'is_rna']
    oriCmd = ' '.join(sys.argv)

    # command line options
    parser = argparse.ArgumentParser(description=usage, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-f", "--fastq", dest="fastq", help="Fastq file", required=True)
    parser.add_argument("-m", "--method", dest="method", choices=CMD_LIST, help="LMP type : %s" % str(CMD_LIST), required=True)
    parser.add_argument("-v", "--version", action="version", version=version)

    options = parser.parse_args()

    if options.fastq and not os.path.isfile(options.fastq):
        print 'Cannot access the fastq file [%s]' % (options.fastq)


    fastq = options.fastq = os.path.realpath(options.fastq)

    print 'Test function  : %s()' % (options.method)
    print 'Test file      : %s' % (fastq)

    result = 'To be implemented.'
    if options.method == 'read_count':
        rcnt = read_count(fastq)
        if rcnt:
            result = 'Resluts       : %d' % rcnt
    else:
        pass # implement the other function testing cases here

    print(result)

