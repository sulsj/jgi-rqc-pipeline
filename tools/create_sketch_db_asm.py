#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
create_sketch_db.py

Created: Sep 26 2017
sulsj (ssul@lbl.gov)


Description
  Create sketch db for each assembly
  Create combined sketch db per program (Plant, Microbial-iso, ce, sps, Microbial-sag, Fungal, Metagenome)

Steps
  1. Collect target assemblies through JAMO

    ex) $ jamo report select file_path, file_name, metadata.library_name, metadata.sequencing_project_id, metadata.jat_key
        where file_status = BACKUP_COMPLETE and metadata.jat_label=assembly and metadata.template_name=microbial_isolate_minimal

    Fungal Min: metadata.template_name=fungal_min_release and file_type=scaffolds and metadata.jat_label=fungal_main_scaffolds
    PBMID: metadata.jat_label=assembly and metadata.assembler=HGAP and metadata.template_name=microbial_isolate_improved_pacbio
    Falcon: metadata.template_name in (fungal_min_release, fungal_std_release, fungal_minimal_pacbio_release, fungal_imp_releaes)
            and file_type=scaffolds and metadata.jat_label=fungal_main_scaffolds
    Metagenome: metadata.jat_label=assembly and metadata.template_name=metagenome

    * Still need cell enrichments, Single Cells, single particle sorts

    * JAMO queries from the RQC wiki: https://rqc-wiki.jgi-psf.org/cgi-bin/moin.cgi/JAT_JAMO

    * All template names
        $ jamo report select metadata.template_name where metadata.jat_label=assembly
        $ jamo report select metadata.template_name where metadata.jat_label=fungal_main_scaffolds

        microbial_isolate_minimal
        microbial_isolate_improved_pacbio
        microbial_isolate_improved
        microbial_isolate_improved_illumina
        microbial_single_minimal
        research_and_development
        genome_from_metagenome
        microbial_amplified_targeted_cell_enrichment
        amplified_random_cell_enrichment
        cell_enrichment_coassembly
        microbial_single_particle_sort

    * Number of target assemblies (10132017 10
        microbial_isolate_improved_pacbio.list: 237
        microbial_isolate_minimal.list: 602
        fungal_all.list: 233


  2. Create sketch db per each assembly and save it to /global/dna/shared/rqc/sketch/

    ex) Spid 1105419 = 01 10 54 19 = 01/10/54/1105419.sketch
    ex) /global/dna/shared/rqc/sketch/illumina/microbial/(spid path)/
        /global/dna/shared/rqc/sketch/pacbio/microbial/(spid path)/

  3. Concatenate the created sketch db to the combined db

    find ./out/fungal -type f -name "*.sketch" -print -exec cat {} \; > ./out/collections/jgi.rqc.fungal.sketch
    find ./out/microbial -type f -name "*.sketch" -print -exec cat {} \; > ./out/collections/jgi.rqc.microbial.sketch
    find ./out/metagenome -type f -name "*.sketch" -print -exec cat {} \; > ./out/collections/jgi.rqc.metagenome.sketch
    find ./out/genomic_tech -type f -name "*.sketch" -print -exec cat {} \; > ./out/collections/jgi.rqc.genomic_tech.sketch

    ex) /global/dna/shared/rqc/sketch/asm/collections/jgi.rqc.plant.sketch
        /global/dna/shared/rqc/sketch/asm/collections/jgi.rqc.microbial.iso.sketch


    4. Copy sketch files to
    /global/dna/shared/rqc/sketch/asm
    /global/dna/shared/rqc/sketch/asm/collecitons


Other features
    - Sketch JGI published sequences into a single sketch file
    - Sketch Illumina filtered fastq reads and PB subread files


Performance
    Sketching ~600 assemblies (microbial_minimal_draft_isolate) took ~14min with 16 cores
    Combined DB size ~50MB

    Sketching total 4074 assemblies with 7~10 workers ==> 10 min


Revisions:
    09.26.2017 0.0.9: init
    10.16.2017 1.0.0: Production mode

"""

import os
import sys
import argparse
import MySQLdb
import datetime
# import re

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from common import get_logger
from rqc_utility import safe_basename, safe_dirname, pad_string_path
from os_utility import run_sh_command
from db_access import jgi_connect_db

## Globals
VERSION = "0.0.9"
LOG_LEVEL = "DEBUG"
# LOG_LEVEL = "INFO"
SCRIPT_NAME = __file__

## production
# TARGET_SKETCH_DIR = "/global/dna/shared/rqc/sketch"
# TARGET_COLLECTION_DIR = "/global/dna/shared/rqc/sketch/collections"
## test
TARGET_SKETCH_DIR = "/global/projectb/scratch/sulsj/2017.09.26-create-sketch-db-test/asm/out"
TARGET_COLLECTION_DIR = "/global/projectb/scratch/sulsj/2017.09.26-create-sketch-db-test/asm/out/collections"


##==============================================================================
if __name__ == '__main__':
    desc = "Create sketch DB"
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-i", "--input", dest="inputFile", help="Input file generated by JAMO", required=True)
    parser.add_argument("-f", "--output-file", dest="outputFile", help="Output TFMQ file", required=True)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Target path to write output files to", required=False)
    parser.add_argument("-ow", "--over-write", action="store_true", help="Recreate all outputs", dest="overWrite", default=False, required=False)
    parser.add_argument("-v", "--version", action="version", version=VERSION)

    options = parser.parse_args()
    inputFile = options.inputFile
    outputFile = options.outputFile

    outputPath = None
    overWrite = False
    collectionCleared = False ## to remove collection file and recreate it

    if options.outputPath:
        outputPath = options.outputPath
    else:
        outputPath = os.getcwd()

    if options.overWrite:
        overWrite = True

    if not os.path.isdir(outputPath):
        os.makedirs(outputPath)

    logFile = os.path.join(outputPath, "create_sketch_db.log")
    log = get_logger("create_sketch_db", logFile, LOG_LEVEL, True, True)

    ## get program name
    db = jgi_connect_db("rqc")

    if db is None:
        log.error("Cannot open database connection!")
        sys.exit(4)

    sth = db.cursor(MySQLdb.cursors.DictCursor)

    sqlStmt = """
SELECT
    s.instrument_type,
    l.account_jgi_sci_prog,
    l.tax_species,
    l.ncbi_tax_id,
    l.actual_seq_prod_name,
    l.library_created_dt,
    l.gold_tax_id
FROM seq_units s
LEFT JOIN library_info l on s.rqc_library_id = l.library_id
INNER JOIN seq_unit_file sf on s.seq_unit_id = sf.seq_unit_id
WHERE s.library_name = %s"""


    with open(inputFile, 'r') as lf:
        cnt = 0
        with open(outputFile, 'w') as of:
            for l in lf:
                t = l.rstrip().split('\t')
                contigFile = os.path.join(t[0], t[1])

                ## libName can be [u'AACVV'] or [u'AXWUT', u'AUBZB', u'BNCXO'] or ZJSH
                # libName = re.findall(r"\[u'(.+?)'\]", t[2])[0]
                libName = t[2].strip().replace("[u'", "").replace("']", "").replace("', u'", ",")
                lt = libName.split(',')
                libNameToSearch = lt[0]

                ## seqProjId can be [1231312] or 13231241 or [1073291, 1073292]
                seqProjId = t[3].strip().replace("[", "").replace("]", "").replace(" ", "")
                seqProjIdToPrint = seqProjId
                if seqProjId.find(',') != -1:
                    s = seqProjId.split(',')
                    seqProjId = s[-1] ## if multiple seqProjIds, choose the last one
                    seqProjIdToPrint = "_".join(s)

                jatKey = t[4]

                # sth.execute(sqlStmt, [libName])
                sth.execute(sqlStmt, [libNameToSearch])
                rs = sth.fetchone()

                # log.info("sql: %s", sqlStmt % (libName))

                if rs:
                    ################################################################
                    ## make header
                    ##
                    # sdmSeqUnitId = str(rs['sdm_id']) if rs['sdm_id'] else "n.a."
                    sequencerName = str(rs['instrument_type']).replace(" ", "_") if rs['instrument_type'] else "n.a."
                    progName = str(rs['account_jgi_sci_prog']).replace(" ", "_") if rs['account_jgi_sci_prog'] else "n.a."
                    orgaName = str(rs['tax_species']).replace(" ", "_") if (rs['tax_species'] and  rs['tax_species'] != '-') else "n.a."

                    if rs['gold_tax_id'] > 0:
                        taxonId = str(rs['gold_tax_id'])
                    elif rs['ncbi_tax_id'] > 0:
                        taxonId = str(rs['ncbi_tax_id'])
                    else:
                        taxonId = '0'

                    prodName = str(rs['actual_seq_prod_name']).replace(" ", "_") if rs['actual_seq_prod_name'] else "etc"
                    # libCreateDate = str(rs['library_created_dt']).replace(" ", "_") if rs['library_created_dt'] else "n.a."
                    libCreateDate = str(rs['library_created_dt']).split()[0] if rs['library_created_dt'] else "n.a."


                    ################################################################
                    ## Run sketch
                    ##
                    ## dest. dir name: Spid 1105419 = 01 10 54 19 = 01/10/54/1105419.sketch
                    # indSketchFile = os.path.join(TARGET_SKETCH_DIR, progName.lower(), pad_string_path(seqProjId, depth=-1), seqProjId + ".sketch")
                    indSketchFile = os.path.join(TARGET_SKETCH_DIR, progName.lower(), pad_string_path(seqProjId, depth=-1), seqProjIdToPrint + ".sketch")

                    ## if sketch file has already been created, skip!
                    if not overWrite and os.path.isfile(indSketchFile):
                        continue

                    sketchCmd = "module load bbtools; sketch.sh in=%s out=%s ow=t taxid=%s spid=%s name=%s meta_sequencername=%s meta_progname=%s meta_libname=%s meta_jatkey=%s meta_libcreatedate=%s meta_sketchdate=%s" % \
                                (contigFile, indSketchFile, taxonId, seqProjId, orgaName,
                                 sequencerName, progName, libName, jatKey, libCreateDate, str(datetime.datetime.now().strftime("%Y-%m-%d")).replace(" ", "_"))

                    # log.info("sketchCmd = %s", sketchCmd)

                    # stdOut, stdErr, exitCode = run_sh_command(sketchCmd, True, log)
                    #
                    # if exitCode != 0:
                    #     log.error("sketch.sh failed")
                    #     sys.exit(-1)


                    ################################################################
                    ## Create collection sketch
                    ##

                    ## Decided to run it later
                    ## with find ./out -type f -name '*.sketch' -exec cat {} \; > output

                    # prodName = '_'.join(prodName.replace(',', '').split())
                    # comSketchFile = os.path.join(TARGET_COLLECTION_DIR, "jgi.rqc." + prodName.lower() + ".sketch")
                    #
                    # if overWrite and not collectionCleared and os.path.isfile(comSketchFile):
                    #     collectionCleared = True
                    #     os.remove(comSketchFile)
                    #
                    # if not os.path.isfile(comSketchFile):
                    #     touchCmd = "mkdir -p %s; touch %s" % (TARGET_COLLECTION_DIR, os.path.join(TARGET_COLLECTION_DIR, comSketchFile))
                    #     # log.info("touchCmd = %s", touchCmd)
                    #
                    #     _, _, exitCode = run_sh_command(touchCmd, True, log)
                    #     assert exitCode == 0
                    #
                    # concatCmd = "cat %s >> %s" % (indSketchFile, comSketchFile)
                    # # log.info("concatCmd = %s", concatCmd)
                    # stdOut, stdErr, exitCode = run_sh_command(concatCmd, True, log)
                    #
                    # if exitCode != 0:
                    #     log.error("concatCmd failed")
                    #     sys.exit(-1)

                    of.write("%s::0\n" % (sketchCmd))

                    cnt += 1


    log.info("%s sketch tasks completed!!", cnt)

    db.close()

## EOF
