Feature: Plotting and modelling the signal data

  Scenario Outline: Plotting composition data
    Given I create the file "data.csv" with the contents:
      """
      "position","read","variable","value"
      1,"Read 1","CG",3
      2,"Read 1","CG",2
      3,"Read 1","CG",1
      1,"Read 2","CG",1
      2,"Read 2","CG",2
      3,"Read 2","CG",3
      1,"Read 1","AT",1
      2,"Read 1","AT",2
      3,"Read 1","AT",3
      1,"Read 2","AT",3
      2,"Read 2","AT",2
      3,"Read 2","AT",1
      """
    When I run the command "../bin/plot_read_signal --input data.csv --output figure.png --type <data>"
    Then the standard out should be empty
    And the standard error should be empty
    And the exit code should be 0
    And the file "figure.png" should exist

    Examples:
      | data        |
      | composition |
      | quality     |

  Scenario: Generating composition coeffiencts
    Given I create the file "data.csv" with the contents:
      """
      "position","read","variable","value"
      1,"Read 1","CG",3
      2,"Read 1","CG",2
      3,"Read 1","CG",1
      1,"Read 2","CG",1
      2,"Read 2","CG",2
      3,"Read 2","CG",3
      1,"Read 1","AT",1
      2,"Read 1","AT",2
      3,"Read 1","AT",3
      1,"Read 2","AT",3
      2,"Read 2","AT",2
      3,"Read 2","AT",1
      """
    When I run the command "../bin/model_read_signal --input data.csv --output output.csv"
    Then the standard out should be empty
    And the standard error should be empty
    And the exit code should be 0
    And the file "output.csv" should exist
