Feature: Formatting input signal data from different sources

  Scenario: Passing incorrect an read parameter
    When I run the command "../bin/format_signal_data --input rqc_input.txt --output output.csv --type composition --read error"
    Then the standard out should be empty
    And the standard error should contain:
    """
    Unknown input to --read option: error
    """
    And the exit code should be 1


  Scenario: Passing incorrect an type parameter
    When I run the command "../bin/format_signal_data --input rqc_input.txt --output output.csv --type error --read 1"
    Then the standard out should be empty
    And the standard error should contain:
    """
    Unknown input to --type option: error
    """
    And the exit code should be 1

  Scenario: Trying to format incorrectly paired composition data for plotting
    Given I create the file "rqc_input.txt" with the contents:
      """
      #Pos	A	C	G	T	N
      0	0.19632	0.33981	0.27827	0.18445	0.00115
      1	0.22946	0.26864	0.32135	0.18034	0.00021
      2	0.18669	0.34094	0.28834	0.18404	0.00000

      """
    When I run the command "../bin/format_signal_data --input rqc_input.txt --output output.csv --type composition --read both"
    Then the standard out should be empty
    And the standard error should contain:
      """
      Error: R1 and R2 do not have the same number of bases. This data likely contains errors.
      """
    And the exit code should be 1


  Scenario: Formatting paired composition data for plotting
    Given I create the file "rqc_input.txt" with the contents:
      """
      #Pos	A	C	G	T	N
      0	0.19632	0.33981	0.27827	0.18445	0.00115
      1	0.22946	0.26864	0.32135	0.18034	0.00021
      2	0.18669	0.34094	0.28834	0.18404	0.00000
      3	0.22626	0.25868	0.33920	0.17508	0.00077

      """
      When I run the command "../bin/format_signal_data --input rqc_input.txt --output output.csv --type composition --read both"
    Then the standard out should be empty
    And the standard error should be empty
    And the exit code should be 0
    And the file "output.csv" should exist with the contents:
      """
      "position","read","variable","value"
      1,"Read 1","CG",6.15
      2,"Read 1","CG",5.27
      1,"Read 2","CG",5.26
      2,"Read 2","CG",8.05
      1,"Read 1","AT",1.19
      2,"Read 1","AT",4.91
      1,"Read 2","AT",0.26
      2,"Read 2","AT",5.12
      1,"Read 1","AT+CG",7.34
      2,"Read 1","AT+CG",10.18
      1,"Read 2","AT+CG",5.52
      2,"Read 2","AT+CG",13.17

      """


  Scenario: Formatting unpaired composition data for plotting
    Given I create the file "rqc_input.txt" with the contents:
      """
      #Pos	A	C	G	T	N
      0	0.19632	0.33981	0.27827	0.18445	0.00115
      1	0.22946	0.26864	0.32135	0.18034	0.00021
      2	0.18669	0.34094	0.28834	0.18404	0.00000
      3	0.22626	0.25868	0.33920	0.17508	0.00077

      """
      When I run the command "../bin/format_signal_data --input rqc_input.txt --output output.csv --type composition --read 1"
    Then the standard out should be empty
    And the standard error should be empty
    And the exit code should be 0
    And the file "output.csv" should exist with the contents:
      """
      "position","read","variable","value"
      1,"Read 1","CG",6.15
      2,"Read 1","CG",5.27
      3,"Read 1","CG",5.26
      4,"Read 1","CG",8.05
      1,"Read 1","AT",1.19
      2,"Read 1","AT",4.91
      3,"Read 1","AT",0.26
      4,"Read 1","AT",5.12
      1,"Read 1","AT+CG",7.34
      2,"Read 1","AT+CG",10.18
      3,"Read 1","AT+CG",5.52
      4,"Read 1","AT+CG",13.17

      """


  Scenario: Formatting paired quality data for plotting
    Given I create the file "rqc_input.txt" with the contents:
      """
      #BaseNum	Read1_linear	Read1_log	Read2_linear	Read2_log
      1	33.064	26.803	32.094	19.321
      2	33.233	28.906	32.283	19.539
      3	33.295	29.747	32.325	19.526
      4	36.548	31.222	35.622	19.729

      """
      When I run the command "../bin/format_signal_data --input rqc_input.txt --output output.csv --type quality --read both"
    Then the standard out should be empty
    And the standard error should be empty
    And the exit code should be 0
    And the file "output.csv" should exist with the contents:
      """
      "position","read","variable","value"
      1,"Read 1","Quality Score",33.06
      2,"Read 1","Quality Score",33.23
      3,"Read 1","Quality Score",33.3
      4,"Read 1","Quality Score",36.55
      1,"Read 2","Quality Score",32.09
      2,"Read 2","Quality Score",32.28
      3,"Read 2","Quality Score",32.33
      4,"Read 2","Quality Score",35.62
      1,"Difference","Quality Score",0.97
      2,"Difference","Quality Score",0.95
      3,"Difference","Quality Score",0.97
      4,"Difference","Quality Score",0.93

      """


  Scenario: Formatting unpaired quality data for plotting
    Given I create the file "rqc_input.txt" with the contents:
      """
      #BaseNum	Read1_linear	Read1_log
      1	33.064	26.803
      2	33.233	28.906
      3	33.295	29.747
      4	36.548	31.222

      """
      When I run the command "../bin/format_signal_data --input rqc_input.txt --output output.csv --type quality --read 1"
    Then the standard out should be empty
    And the standard error should be empty
    And the exit code should be 0
    And the file "output.csv" should exist with the contents:
      """
      "position","read","variable","value"
      1,"Read 1","Quality Score",33.06
      2,"Read 1","Quality Score",33.23
      3,"Read 1","Quality Score",33.3
      4,"Read 1","Quality Score",36.55

      """
