set_package_environment <- function(dir){
  libs <- file.path(dir, "..", "vendor", "r", "packrat", "lib", "*", "*")
  .libPaths(c(.libPaths(), libs))
}

load_packages <- function(packages){
  sink("/dev/null");
  lapply(packages, FUN = function(X) {
    do.call("library", list(X, warn.conflicts=FALSE))
  })
  sink();
}

load_packrat_packages <- function(dir, packages){
  set_package_environment(dir)
  load_packages(packages)
}
