parse_command_args <- function(){
  'usage:
    model_read_signal --input <file> --output <file>

  options:
    --help         Show this screen
    --input FILE   Formatted signal input file
    --output FILE  Destination to write model coefficients' -> doc
  docopt(doc)
}

calculate_coef <- function(value, position){
  scaled = position / max(position)
  model <- suppressWarnings(loess(value ~ scaled, span = 0.9))
  round(predict(model, data.frame(scaled = 1)), digits = 1)
}

model_signal <- function(data){
  data %>%
    group_by(read, variable) %>%
    summarise(coefficient =  calculate_coef(value, position))
}
