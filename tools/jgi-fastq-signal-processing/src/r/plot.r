parse_command_args <- function(){
  'usage:
    plot_read_signal --input <file> --output <file> --type <type>

  options:
    --help, -h         Show this screen
    --type, -t TYPE    The type of input data
    --input, -i FILE   Formatted signal input file
    --output, -o FILE  Destination to write plot' -> doc
  docopt(doc)
}

check_args <- function(args){
  if (! is.element(args$type, c("composition", "quality"))) {
    write(paste("Unknown input to --type option:", args$type), stderr())
    quit(status = 1)
  }
}

limits <- function(type){
  switch(type,
         quality     = c(0, 40),
         composition = c(0, 5))
}


plot_signal <- function(data, type, output){
  # sulsj 04302017
  # options(bitmapType='cairo')
  plt <- ggplot(aes(x = position, y = value, col = variable, pch = variable, group = variable), data = data) +
  geom_point(alpha = 0.35, size = 1.3) +
  geom_smooth(aes(col = variable), method = loess,  lty = 2, span = 0.7, alpha = 0.2) +
  facet_wrap(~ read) +
  scale_x_continuous("Position in read") +
  scale_y_continuous("Variable", limits = limits(type)) +
  theme_bw()

  if (type == "composition"){
    plt <- plt + geom_hline(aes(yintercept = 2), col = "red", lty = 3)
  }

  suppressWarnings(ggsave(output, width = 10, height = 5))
}
