#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Summarize qs per user for Genepool, Denovo and Cori
r, qw
- writes to /tmp/(uname)_qs.txt to not clash with user permissions

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import getpass
import os
import sys

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from common import run_cmd




## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions



def print_report(q):

    x = "%-14s %5s %5s %5s %7s %s"

    #print qs_dict
    ttl = {'qw' : 0, 'r' : 0, 'c' : 0, 'ram' : 0.0}
    print x % ("NAME", "QW", "R", "TOTAL", "PCT RUN", "")

    max_ttl = 0
    #print q
    for k in q:
        if q[k]['r'] > max_ttl:
            #max_ttl = q[k]['cnt']
            max_ttl = q[k]['r']

    for k in sorted(q.iterkeys()):
        pct = 0.0
        if q[k]['cnt'] > 0:
            pct = q[k]['r'] / float(q[k]['cnt'])
        #print pct, type(pct)

        #s = int(35 * q[k]['cnt'] / float(max_ttl)) # bar chart *'s
        s = int(35 * q[k]['r'] / float(max_ttl)) # bar chart *'s

        print x % (k, q[k]['qw'], q[k]['r'], q[k]['cnt'], "{:.1%}".format(pct), "*" * s)
        ttl['qw'] += q[k]['qw']
        ttl['r'] += q[k]['r']
        ttl['c'] += q[k]['cnt']
        ttl['ram'] += q[k]['ram']

    pct = 0.0
    if ttl['c'] > 0:
        pct = ttl['r'] / float(ttl['c'])
    print x % ("TOTAL", ttl['qw'], ttl['r'], ttl['c'], "{:.1%}".format(pct), "")


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

if __name__ == "__main__":

    cluster = os.environ.get('NERSC_HOST', 'unknown')

    # f = temp file
    f = "/tmp/%s_qs-%s.txt" % (getpass.getuser(), cluster)

    if cluster == "genepool":
        cmd = "qs > %s" % (f)
        _, _, _ = run_cmd(cmd)

    elif cluster == "denovo":
        cmd = "squeue -o '%%i %%u %%a %%t' > %s" % (f)
        _, _, _ = run_cmd(cmd)

    elif cluster == "cori":
        #squeue -o "%i|%j|%T|%V|%S|%a|%M|%N|%P|%u"

        cmd = "squeue --qos genepool -o '%%i %%u %%a %%t' > %s" % (f)
        _, _, _ = run_cmd(cmd)
        
        cmd = "squeue --qos genepool_shared -o '%%i %%u %%a %%t' >> %s" % (f)
        _, _, _ = run_cmd(cmd)
        
    else:
        print "Unsuppored cluster: %s" % cluster
        sys.exit(1)

    qsu = {} # per user
    qsp = {} # project

    cnt = 0

    # genepool column ids
    ui = 3 # user
    pi = 4 # project
    ri = 8 # rss mem
    ni = 7 # nodes
    si = 1 # state

    if cluster == "denovo":
        ui = 1
        pi = 2
        si = 3
        
        ri = 1
        ni = 1
        

    if cluster == "cori":
        ui = 1
        si = 3
        pi = 2
        
        ri = 1
        ni = 1

    fh = open(f, "r")
    for line in fh:

        arr = line.strip().split()
        if line.startswith("-----"):
            continue
        
        if arr[0] == "JOBID":
            continue

        if len(arr) > 3:

            u = arr[ui] # user
            p = arr[pi] # project
            r = arr[ri] # ?
            n = arr[ni] # ?
            s = arr[si] # state



            ttl_ram = 0.0
            #ttl_ram = float(str(r).replace("G", ""))
            #if str(n).isdigit():
            #    ttl_ram = int(n) * float(str(r).replace("G",""))

            if cluster != "genepool":
                if s == "R":
                    s = "r"
                elif s == "PD":
                    s = "qw"
                else:
                    s = s.lower()

            #print "u: %s, r: %s, n: %s, ram: %s" % (u,r,n, ttl_ram)
            if u in qsu:
                qsu[u]['cnt'] += 1
            else:
                qsu[u] = { "cnt" : 1, "r" : 0, "qw" : 0, "ram" : 0.0 }

            if s in qsu[u]:
                qsu[u][s] += 1
            else:
                qsu[u][s] = 1

            if s == "r":
                qsu[u]['ram'] += ttl_ram

            if p in qsp:
                qsp[p]['cnt'] += 1
            else:
                qsp[p] = { "cnt" : 1, "r" : 0, "qw" : 0, "ram" : 0.0 }

            if s in qsp[p]:
                qsp[p][s] += 1
            else:
                qsp[p][s] = 1


        cnt += 1


    fh.close()


    title = "[ USERS ]"
    x = (90 - len(title)) / 2
    print "-" * x + title + "-" * x


    print_report(qsu)
    print

    title = "[ PROJECTS ]"
    x = (90 - len(title)) / 2
    print "-" * x + title + "-" * x

    print_report(qsp)

    print

    sys.exit(0)

'''


'''
