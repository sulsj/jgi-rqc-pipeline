#!/usr/bin/env python
'''
    Evaluate microbial genome agains AntiFam DB. The JOB_DIR/hmmer/results.txt contains the tabular results of hits.

    1) run prodigal on the microbial genome to predict genes:
    prodigal -a PROTEIN_TRANSLATIONS.faa -d GENE_NUCLEOTIDE_SEQ.fasta -i MICROBIAL_CONTIGS.fasta -o LOG
    2) run hmmsearch on the gene protein sequences:
    hmmsearch ANTIFAM_DB PROTEIN_TRANSLATIONS.faa > OUT.antifam.hmmsearch

    use module load on genepool and docker image on cori for prodigal and hmmsearch

    Run example:
        antifam.py -f /global/dna/shared/data/functests/assembly/Microbe/Actinoalloteichus_cyanogriseus_DSM_43889/mnt/secondary/Smrtanalysis/current/common/jobs/081/081569/data/draft_assembly.fasta
        -o /global/projectb/scratch/syao/antifam

    Shijie Yao
    Last Modified:
        08/11/2017 - initial implementation

    To do:
    -
'''
import os
import sys
from argparse import ArgumentParser

# custom libs in '../lib/'
CUR_DIR = os.path.dirname(os.path.realpath(__file__))   # so will work even file is linked
sys.path.append(os.path.join(CUR_DIR, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from common import get_logger, run_command #, checkpoint_step

ANTIFAM_DB = '/global/dna/shared/rqc/ref_databases/antifam/AntiFam.hmm'

GENE_PRO = 'gene_p.fa'  # prodigal predicted genes file - protein sequence
GENE_NUC = 'gene_n.fa'  # prodigal predicted genes file - nucleotide sequence

def do_log(msg, err=False):
    'log the msg to the log file, and also print to std out. if log is an error msg, exit.'

    if not err:
        LOG.info(msg)
    else:
        msg = 'Error: %s' % msg
        LOG.error(msg)

    print(msg)
    if err:
        sys.exit(1)

def add_to_log(msg, fname):
    run_command('echo \"%s\" >> %s' % (msg, fname), True)
    print(msg)

def run_step(cmd, label):
    do_log(cmd)
    sout, serr, ecode = run_command(cmd, True)
    if ecode != 0:
        do_log('%s run failed sout[%s] serr[%s]' % (label, sout, serr), True)


def run_driver(contig, wdir):
    host = os.environ.get('NERSC_HOST', 'unknown')
    if host not in ['genepool', 'cori', 'denovo']:
        do_log('unsupported host - %s' % host, True)
    else:
        cwd = os.getcwd()

        ## run prodigal
        prodigaldir = os.path.join(wdir, 'prodigal')
        if not os.path.exists(prodigaldir):
            os.makedirs(prodigaldir)

        gene_p = os.path.join(prodigaldir, GENE_PRO)
        gene_n = os.path.join(prodigaldir, GENE_NUC)
        log = os.path.join(prodigaldir, 'prodigal.log')

        if host == 'genepool':
            cmd = 'module load prodigal; prodigal -i %s -a %s -d %s -o %s' % (contig, gene_p, gene_n, log)
        else:
            imgname = 'registry.services.nersc.gov/jgi/prodigal'
            cmd = 'shifter --image=%s prodigal -i %s -a %s -d %s -o %s' % (imgname, contig, gene_p, gene_n, log)

        os.chdir(prodigaldir)
        run_step(cmd, 'prodigal')

        ## run hmmsearch
        hmmdir = os.path.join(wdir, 'hmmer')
        if not os.path.exists(hmmdir):
            os.makedirs(hmmdir)

        hmmlog = os.path.join(hmmdir, 'hmm.log')
        results = os.path.join(hmmdir, 'results.txt')
        if host == 'genepool':
            cmd = 'module load hmmer; hmmsearch --tblout %s %s %s > %s' % (results, ANTIFAM_DB, gene_p, hmmlog)
        else:
            imgname = 'registry.services.nersc.gov/jgi/hmmer'
            cmd = 'shifter --image=%s hmmsearch --tblout %s %s %s > %s' % (imgname, results, ANTIFAM_DB, gene_p, hmmlog)

        os.chdir(hmmdir)
        run_step(cmd, 'hmmsearch')

        print('The hit list is in %s' % results)
        ## go back to ori dir
        os.chdir(cwd)

if __name__ == '__main__':
    NAME = 'Contig evaluation against AntiFam'
    VERSION = 'version 1.0.0'

    # Parse options
    USAGE = '\n%s [options]\n' % sys.argv[0]

    # JOB_CMD += '/global/homes/b/brycef/git/jgi-rqc/framework/test/pipeline_test.py --fastq %s --output-dir %s'
    JOB_CMD = ' '.join(sys.argv)
    PARSER = ArgumentParser(usage=USAGE)

    RUN_TYPE = ['lwf', 'd-bac', 'd-arc', 'user'] # lineage-wf, domain bacteria, domain archaea,
    PARSER.add_argument('-f', '--fasta', dest='fasta', type=str, help='The assembly to be evaluated (full path to fasta)', required=True)
    PARSER.add_argument('-o', '--output-dir', dest='outdir', help='Output dir name', required=True)
    PARSER.add_argument('-l', '--log-file', dest='logfile', default='antifam.log', help='Log file name (will be in the -o DIR if not an abs path).')
    PARSER.add_argument('-v', '--version', action='version', version=VERSION)

    ARGS = PARSER.parse_args()

    ## create the output dir and move into it
    wdir = os.path.abspath(ARGS.outdir)
    if not os.path.exists(wdir):
        os.makedirs(wdir)

    # initialize my logger
    log_level = 'INFO'
    log_file = ARGS.logfile

    if not log_file.startswith('/'):
        log_file = os.path.abspath(os.path.join(wdir, log_file))

    LOG = get_logger("antifam.eval", log_file, log_level)

    ## move back to work dir
    do_log(JOB_CMD)
    add_to_log('---------------------------------------------', log_file)
    add_to_log('%15s : %s' % ('INPUT', ARGS.fasta), log_file)
    add_to_log('%15s : %s' % ('OUT DIR', wdir), log_file)
    add_to_log('%15s : %s' % ('RUN LOG', log_file), log_file)
    add_to_log('---------------------------------------------\n', log_file)

    if not os.path.isfile(ARGS.fasta):
        do_log('contig file not exists - %s' % ARGS.fasta, True)

    run_driver(ARGS.fasta, wdir)
