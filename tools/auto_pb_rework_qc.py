#!/usr/bin/env python
'''
    A Python script to perform bulk rework qc submittion.
    A input file with the multiple submission entries are needed.

    Author: Shijie Yao
    Update Date  : Oct 16, 2015 - initial implementation
    Update Date  : Sep 18, 2017 - use MF cv from api, with the update cv(RQCSUPPORT-1512)

    test run:
    ~/src/jgi-rqc-pipeline/tools/auto_pb_rework_qc.py -b PPZB,PPYZ,PPZA,PHTB
    ~/src/jgi-rqc-pipeline/tools/auto_pb_rework_qc.py -i pacbio.in



    - to produce input template:
        auto_pb_rework_qc.py -b ASPZT > test.in
        auto_pb_rework_qc.py -b ASPZT,XBWO,APYWC > test.in
        auto_pb_rework_qc.py -b ASPZT,XBWO,APYWC > test.in
        auto_pb_rework_qc.py -b libs.txt > test.in  (libs.txt is a file containing the list of lib names)

    - to run rework qc on a input file:
        auto_pb_rework_qc.py -i test.in (dry-run; action_type default to ITS validation)
        * use -p to pring json data; use -t submit to submit to ITS; use -l to a live-run
        * production run:
            auto_pb_rework_qc.py -i myqc.in -t submit -l
'''

import sys
import os
import datetime
import time
import argparse
import re
import json

import urllib

import MySQLdb
import pprint
pp = pprint.PrettyPrinter(indent=4)

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR

LOG_FILE = None

if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
import os_utility as util
from curl import Curl, CurlHttpException

if 'run_sh_command' in dir(util):
    run_command = util.run_sh_command
elif 'run_command' in dir(util):
    run_command = util.run_command

from db_access import jgi_connect_db

## !! set DEBUG = False for production !!
DEBUG = True
LINE_CNT = 0

##
## - global constants
##

#- param to ws mapping
TO_ITS_WS_MAP = {
    'targeted_amount' : 'targeted-logical-amount',
    'logical_amount' : 'total-logical-amount-completed',

    'abandon_sample' : 'abandon-sample',
    'abandon_library' : 'abandon-library',

    'estimated_genome_size' : 'revised-genome-size-mb',
    'genome_size_method' : 'genome-size-estimation-method',
    'complete_sow' : 'sow-item-complete',
}

#- required param
PARAM_REQUIRED = {
    'analyst' : '',         # STR : "FIRST_NAME LAST_NAME" in rqc_analyst table; needed to find analyst_id

    'seq_unit_name' : '',   # STR

    'targeted_amount' : '', # FLOAT
    'logical_amount' : '',  # FLOAT

    'lab_action' : 'No Lab Action',             # for PASS

    'abandon_library' : 'N',    # has default value, config file can omit this param for using default
    'abandon_sample' : 'N',     # has default value, config file can omit this param for using default

    'platform': 'Illumina',     # default to
    'library_name': '',
    'useable': '',
}

# optional cgi param
PARAM_OPTIONAL = {
    'measured_insert_size' : '',
    'measured_insert_size_std' : '',
    'insert_size_method': '',

    'estimated_genome_size' : '',
    'genome_size_method' : '',

    'fm_where' : '',
    'fm_what' : '',

    'sow-item-complete' : '',

    'comments' : '',

    'email_name' : '',
}

GENOME_SIZE_EST_METHOD_MAP = {
    '0' : 'Collaborator Est.',
    '1' : 'Library QC',
    '2' : 'Draft Assembly',
    '3' : 'Finished Genome',
}

LAB_ACTION_MAP = {
    '0' : 'No Lab Action',
    '1' : 'Make New Library',
    '2' : 'qPCR Existing Library',
    '3' : 'Sequence Existing Library'
}

SOW_STATUS_MAP = {
    '0' : 'Needs Lab Work',
    '1' : 'Complete',
    '2' : 'Needs PMO Attention'
}

BLOCK_HEADER_LIB_NAME = 'Lib_Name'
BLOCK_HEADER_LIB_TLA = 'TLA'
BLOCK_HEADER_LIB_GENE_SIZE = 'Genome_Size(mb)'
BLOCK_HEADER_LIB_GENE_SIZE_MET = 'Genome_Size_Method'
BLOCK_HEADER_LIB_LAB_ACTION = 'Lab_Action'
BLOCK_HEADER_LIB_AB_LIB = 'Abandon_Library'
BLOCK_HEADER_LIB_AB_SAM = 'Abandon_Sample'
BLOCK_HEADER_LIB_SOW_STATUS = 'SOW_Status'
BLOCK_HEADER_LIB_COMMENTS = 'Comments'


# all param
ALL_PARAM = dict(PARAM_REQUIRED.items() + PARAM_OPTIONAL.items())

QUOTE = '\'\"'

WEB_HOST = 'https://rqc.jgi-psf.org'
WEB_API = 'api/sow_qc/submit'  # default to production
ACTION_FAIL = 0
ACTION_OK = 1


'''
retrive FM definitions from the centralized code of seqqc.py
1) failure_major_code
2) failure_mode_illumina
'''
def failure_mode_data(api):
    cmd = 'curl %s/api/seqqc/%s' % (WEB_HOST, api)
    err = ''
    # if DEVELOP:
    #     write_log("RUN : %s" % cmd)
    std_out, std_err, exit_code = run_command(cmd, True)

    if exit_code == 0:
        jd = json.loads(std_out)
        if 'errors' in jd:
            err = 'failed : %s [%s]' % (cmd, ';'.join(jd['errors']))
        else:
            if api == 'failure_major_code':
                fmdat = {}  # change keys into int
                for k in jd:
                    fmdat[int(k)] = jd[k]
                jd = fmdat
            return jd
    else:
        err = 'failed : %s [%s][%s]' % (cmd, std_out.strip(), std_err.strip())

    if err:
        print_error(err)


def print_template(header=False):
    'Print out the input file template for a given action type.'

    FF_CODE_TXT = ''
    for fm in sorted(FM_MAJOR_CODE): # sorted keys
        FF_CODE_TXT += '# %d : %s\n' % (fm, FM_MAJOR_CODE[fm])
        for idx, ffm in enumerate(FM_DATA[FM_MAJOR_CODE[fm]]):
            FF_CODE_TXT += '#                   %d : %s\n' % (idx, FM_DATA[FM_MAJOR_CODE[fm]][idx])
        FF_CODE_TXT += '#\n'

    #  SEQ_UNIT_NAME\tACTION\tTLA\tCLA\tCOMMENTS
    #  ----------------------------    -------   ------   ------   ------    ------    -------------------------------------------------------
    #  7341.7.68152.GCCAAT.fastq.gz    UPDATE  4.0     1.17
    FORMAT =            '#SU_NAME                          CLA_GB         CLA                USEABLE        FM_M           FM_m'
    SAMPLE = '\n'.join(['pbio-000.0001.fastq               0.5525         0.5525             -1             -1             -1',
                        'pbio-001.0002.fastq               0.5525         0.5525             -1             -1             -1'])


    text = '''
#######################################################################
##
## ===================================================================
## This is the script produced template rework QC tamplate.
## create time : %s
''' % (time_now_str())

    text +='''
## ===================================================================
#  Comments:
#  - WRITE YOUR COMMENTS OF THIS FILE HERE -
#

## ===================================================================
#  meta data (MANDATORY): - Change the name to your name as recorded in
#  RQC in FIRST_NAME LAST_NAME format
Analyst=Barack Obama

## ===================================================================
# Your email name (without @lbl.gov) to receive failure notification email
email=

## ===================================================================
#  QC data (MANDATORY):
#  Use \\n in comments for new line
#  seq_unit_name must be in *.fastq.gz format

# Genome Size Est. Method Code Map:
# 0 : Collaborator Est.
# 1 : Library QC
# 2 : Draft Assembly
# 3 : Finished Genome

# Lab Action Code Map:
# 0 : No Lab Action
# 1 : Make New Library
# 2 : qPCR Existing Library
# 3 : Sequence Existing Library

# Data Uesable Code Map:
# 0 : Not Useable        - inform jamo to NOT publish this seq unit through portal
# 1 : Uesable            - inform jamo to publish this seq unit through portal

# SOW Status Code Map:
# 0 : None               - leave sow item open (sends NEEDS WORK notification email)
# 1 : Complete           - close sow item (sends COMPLETED notification email)
# 2 : Needs Attention    - leave sow item open and sends NEEDS ATTENTION notification email

# Failure Mode Code Map
# (where)           (what)
# FM_M              FM_m
# ----              ----
''' + FF_CODE_TXT + '''
# To Reschedule Pool, you can use this tool to save (using -t save) ALL pool members to RQC.
# Your input must have CLA, TLA and USEABLE set for each member.
# After the save, you can run the web app (https://rqc.jgi-psf.org/sow_qc/page/LIBNAME) on one of the member, and
#   choose Reschedule Pool for your QC submission.

#
# When USEABLE=1, FM_M and FM_m can be ignored with values of -1;
# But USEABLE=0 requires valid FM_M and FM_m code
#
# ABANDON_LIB=Y MUST be paired with LAB_ACTION=0 or LAB_ACTION=1
# ABANDON_SAMPLE=Y MUST be paired with LAB_ACTION=0, and SOW=2
#
# In-line comments (single line) needs to follow the "Comments:" leading tag;
# Multi-line comment lines need to follow the "Comments:" line and terminated with #END_COM line
#################################################################
'''

    if not header:
        gparam = '\n'.join([
            'Lib_Name:                      BZGSB',
            'TLA:                           -1',
            'Genome_Size(mb):               -1',
            'Genome_Size_Method:            -1',
            'Lab_Action:                    -1',
            'Abandon_Library:               N',
            'Abandon_Sample:                N',
            'SOW_Status:                    -1',
            'Comments:                      YOUR COMMNETS'
        ])
        text = "%s%s\n%s\n%s\n" % (text, gparam, FORMAT, SAMPLE)

    print(text)


#
# sanity check helper function
# param is a dict of current submission params
# required is a dict of required params
#
def missing_required_param(param, required):
    'Return key that missing corrresponding value.'
    for key in required.keys():
        if not required[key] and key not in param:
            return key


# helper DB function to check if a given sun has been QCed (Completed) befor
def check_qc_status(sth, sulist):
    'Return boolean value to indictate if the given segment id is in segment_qc table.'

    # check rework QC illumina table:
    sql = 'SELECT S.seq_unit_name FROM sow_qc Q INNER JOIN seq_units S ON Q.seq_unit_id=S.seq_unit_id WHERE Q.status_id=3 AND S.seq_unit_name IN ('

    suns = []
    for su in sulist:
        suns.append(su['sun'])

    tmp = '%s,' * len(suns)
    sql += tmp[:-1] + ')'

    sth.execute(sql, suns)

    rows = sth.fetchall()

    if rows:
        rtn = {
                'sunits': rows,
                'errors': 'contains already submitted seq units'
            }
    else:
        rtn = {}

    return rtn

# helper DB function to check if all the seq units exists for the libname in RQC
def check_seq_units_of_lib(sth, libname, sulist):
    'Return boolean value to indictate if the given segment id is in segment_qc table.'

    # check rework QC illumina table:
    sql = 'SELECT seq_unit_name FROM seq_units WHERE library_name=%s AND seq_unit_name IN ('

    suns = []
    for su in sulist:
        suns.append(su['sun'])

    tmp = '%s,' * len(suns)
    sql += tmp[:-1] + ')'

    sth.execute(sql, [libname] + suns)

    rows = sth.fetchall()

    if rows:
        indb = []
        for row in rows:
            indb.append(row['seq_unit_name'])
        for su in suns:
            if su not in indb:
                write_log('Fatal Error - seq unit [%s] not in lib [%s]' % (su, libname), mtype=ACTION_FAIL, logfile=None, eCode=1)
    else:
        write_log('Fatal Error - seq unit(s) %s not in lib [%s]' % (str(suns), libname), mtype=ACTION_FAIL, logfile=None, eCode=1)


def is_pacbio_platform(sth, libname):
    sql = 'SELECT platform_name FROM seq_units WHERE library_name=%s and platform_name=%s'
    sth.execute(sql, (libname, 'PacBio'))
    data = sth.fetchone()
    if data:
        return True
    else:
        return False

def get_lib_name(sth, sun):
    sql = 'select library_name from seq_units where seq_unit_name=%s'
    sth.execute(sql, (sun, ))
    data = sth.fetchone()
    if data:
        return data.get('library_name')
    else:
        return None

# return the analyst name, if found in the give line
def get_analyst(line):
    'Return the analyst name string encoded in the input file Analyst line. Return None if not found in line.'
    import re
    match = re.match('analyst=(.*)', line, re.IGNORECASE)
    if match:
        return match.group(1).strip()
    else:
        return None

# helper DB function to check if a given data has been QCed (Completed) befor
def get_analyst_id(sth, name):
    'Return the RQC analyst_id in rqc_analyst table for the given analyst name in FIRSTNAME LASTNAME format'
    if not name:
        return False

    names = name.split()
    if len(names) < 2:
        return False

    first_name = names[0]                   # first name is the 1st word: rqc_analyst.first_name all are single word
    last_name = ' '.join(names[1:len(names)])  # last name is the rest word(s): rqc_analyst.last_name can be multiple words

    sql = 'SELECT analyst_id FROM rqc_analyst WHERE first_name=\'%s\' AND last_name = \'%s\'' % (first_name, last_name)
    sth.execute(sql)
    data = sth.fetchone()

    if data:
        return int(data['analyst_id'])
    else:
        return None

def analyst_info(sth, name):
    'Return the RQC analyst_id in rqc_analyst table for the given analyst name in FIRSTNAME LASTNAME format'
    if not name:
        return False

    names = name.split()
    if len(names) < 2:
        return False

    first_name = names[0]                   # first name is the 1st word: rqc_analyst.first_name all are single word
    last_name = ' '.join(names[1:len(names)])  # last name is the rest word(s): rqc_analyst.last_name can be multiple words

    sql = 'SELECT * FROM rqc_analyst WHERE first_name=\'%s\' AND last_name = \'%s\'' % (first_name, last_name)
    sth.execute(sql)
    data = sth.fetchone()

    if data:
        return data
    else:
        return None

#helper for logging
def write_log(msg, mtype=None, logfile=None, eCode=None):
    'print and log message'
    global LOG_FILE

    if logfile:
        LOG_FILE = logfile
    elif not LOG_FILE:
        LOG_FILE = os.path.basename(__file__)
        LOG_FILE = LOG_FILE[:len(LOG_FILE) - 3] + '.log'
    'Print a message to log file as well as stdout.'
    fout = None
    if LOG_FILE:
        fout = open(LOG_FILE, 'a') # append to

    if mtype == ACTION_FAIL:
        msg = '   ??? --> %s' % msg
    elif mtype == ACTION_OK:
        msg = '       ==> %s' % msg
    elif msg.startswith('Fatal'):
        msg = '\n%s\n' % msg

    print(msg)
    if fout:
        fout.write(msg + "\n")

    if eCode != None:
        sys.exit(eCode)


def clean_ws_param(param):
    del param['abandon_library']
    del param['abandon_sample']
    del param['library_name']
    del param['logical_amount']
    del param['targeted_amount']
    del param['seq_unit_name']
    del param['useable']
    if 'fm_what' in param:
        del param['fm_what']
    if 'fm_where' in param:
        del param['fm_where']
    del param['comments']
    if 'sow-item-complete' in param:
        del param['sow-item-complete']

def error_in_ws_call(data):
    try:
        rtn = json.loads(data)
        if 'errors' in rtn:
            return rtn.get('errors')
        else:
            return None
    except:
        return data

def load_config_to_param(config, param):
    for key in config.keys():   # check each param of the input line
        if key in param.keys() and config[key] != None and config[key] != '':
            param[key] = config[key]

def rm_null_values(param):
    # remove empty value records; can use param = dict((k, v) for k, v in param.iteritems() if v)
    for key in param.keys():
        #print('\nDEBUG [%s][%s]' % (key, param[key]))
        if param[key] == None or param[key] == '':
            del param[key]

#dataUseable [0, 1]
def make_sunits_list(param):
    # construct ws json data structure
    pp.pprint(param)
    sunits = {
        'sun'                               :   param.get('seq_unit_name'),
        'targeted-logical-amount'           :   param.get('targeted_amount'),
        'total-logical-amount-completed'    :   param.get('logical_amount'),
        'abandon-library'                   :   param.get('abandon_library'),
        'abandon-sample'                    :   param.get('abandon_sample'),
        'data_useable'                      :   param.get('useable'),
        'fm_what'                           :   param.get('fm_what', ''),
        'fm_where'                          :   param.get('fm_where', ''),
        'comments'                          :   param.get('comments')
    }

    if not sunits['fm_what']:
        del sunits['fm_what']
    if not sunits['fm_where']:
        del sunits['fm_where']

    if param.get('sow-item-complete'):
        sunits['sow-item-complete'] = param.get('sow-item-complete')

    return sunits;

def encoded_param(param):
    param = urllib.urlencode(param)
    param = param.replace('%5Cn', '%0A')    # replace the encoded '\n' with NEWLINE code
    return param

def curl_cmd(param):
    #opt = encoded_param(param)
    url = '%s/%s' % (WEB_HOST, WEB_API)
    #
    # CURL post with json format:
    # curl -H "Content-Type: application/json" -d '{"username":"xyz","password":"xyz"}' http://localhost:3000/api/login
    #

    # replace ' with unicode code in json string (when it appears in comments). double quotes are handled by dumps()
    cmd = 'curl -H "Content-Type: application/json" -d \'%s\' %s' % (str(param).replace('\'', '"'), url)

    return cmd

'''
    for reschedule pool, pool members only need to have:
    {
        'data_useable': USEABLE,                    //backend will del this after reading the value out.
        'targeted-logical-amount': TLA,
        'total-logical-amount-completed': CLA,
        'abandon-sample': 'N',                      // fixed
        'abandon-library': 'N'                      // fixed
    }

'''
#
# to submit ONE rework QC
# @live: boolean
#


def get_purid_by_su(sth, sun):
    sql = 'SELECT gls_physical_run_unit_id FROM seq_units WHERE seq_unit_name=%s'
    sth.execute(sql, (lib, ))
    row = sth.fetchone()
    if row:
        return row['gls_physical_run_unit_id']
    else:
        return None

'''
TODO : 8/28/2015 - tobe completed!!!
'''
def do_one(sth, item, gmeta, toprint, atype, live):
    'Perform segmentQC submission for PASS on a single record.'
    ## cml keys in config file to cgi params, and rm cgi params that not in config file :

    #print('DEBUG In do_one')
    #pp.pprint(item)
    #pp.pprint(gmeta)
    #param = dict(gmeta)       # a local copy of ALL params

    gdata = item['global']
    jsdata = {
                'action_type': atype,
                'platform' : 'PacBio',
                'analyst' : gmeta.get('analyst'),
                'lab_action' : gdata.get('lab_action'),

                'sequnits' : []
            }

    if gmeta.get('email_name'):
        jsdata['email_name'] = gmeta.get('email_name')

    for idx, su in enumerate(item['sus']):
        sudata = {
                    'sun': su['sun'],
                    'data_useable': su['data_useable'],
                    'total-logical-amount-completed': su['total-logical-amount-completed'],
                    'targeted-logical-amount': gdata.get('targeted_amount'),
                    'comments': gdata.get('comments'),
                    'abandon-library': gdata.get('abandon_library'),
                    'abandon-sample': gdata.get('abandon_sample'),
                }

        if 'revised-genome-size-mb' in gdata:
            sudata['revised-genome-size-mb'] = gdata['revised-genome-size-mb']
            sudata['genome-size-estimation-method'] = gdata['genome-size-estimation-method']

        if idx == 0:
            sudata['sow-item-complete'] = gdata['sow-item-complete']
            #pprint.pprint(gdata)
            if 'revised-genome-size-mb' in gdata and 'genome-size-estimation-method' in gdata:
                sudata['revised-genome-size-mb'] = gdata['revised-genome-size-mb']
                sudata['genome-size-estimation-method'] = gdata['genome-size-estimation-method']

        jsdata['sequnits'].append(sudata)

    if toprint:
        print('json data for submission:')
        pp.pprint(jsdata)
    else:
        cmd = curl_cmd(jsdata)
        if live:
            write_log('Command: %s\n' % cmd)
            std_out, std_err, exit_code = run_command(cmd, True)    # exit_code of 0 is success
            if exit_code == 0:
                error =  error_in_ws_call(std_out)
                if error:
                    write_log('Error (RQC WS returned error):')
                    write_log('---------------------------------------\n%s' % error)
                    write_log('---------------------------------------')
                    return {'errors': 'RQC WS returned error'}
                else:
                    write_log('WS call OK')
                    write_log(std_out)
            else:
                write_log('Error (Run curl command failed):')
                write_log('---------------------------------------\n%s' % std_err)
                write_log('---------------------------------------')
                return {'errors': 'Run curl command failed'}
        else:
            write_log('----------------------')
            write_log(cmd)
            write_log('')

    return {'status': 'OK'}



def clean_comment(cmt):
    'Remove leading and trailing quotes in the given string, and return the cleaned string.'
    cmt = cmt.strip()
    if cmt[0] in QUOTE and cmt[-1] in QUOTE and cmt[0] == cmt[-1]:
        cmt = cmt[1:-1]
    return cmt

def time_now_str():
    'Return the formated time string for NOW.'
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

def dbinfo(sth):
    'Return a string with currently connected databsae info.'
    sql = 'SELECT @@hostname AS hostname, database() AS dbname, user() AS uname'
    sth.execute(sql)
    rows = sth.fetchone()
    #return 'DB host=%s, name=%s, user=%s' % (rows['hostname'], rows['dbname'], rows['uname'])
    return 'dbhost=%s, dbname=%s' % (rows['hostname'], rows['dbname'])


def validate_value(aVal, aList, aTag, aLine):
    if aVal not in aList:
        write_log('Fatal Error - %s must have values of %s' % (aTag, str(list(aList))))
        write_log('L:%d   %s' % (LINE_CNT, aLine), None, None, 1)


def validate_bad_relation(badRelation, aTag, aLine):
    if badRelation:
        write_log('Fatal Error - %s' % aTag)
        write_log('L:%d   %s' % (LINE_CNT, aLine), None, None, 1)


def YN2INT(aVal):
    if aVal == 'Y':
        return 1
    elif aVal == 'N':
        return 0
    else:
        write_log('Fatal Error - wrong value YN %s' % aVal, None, None, 1)


'''
    Generator to open a file, and return data packet
    Usage:
        gen = record_generator(fname)
        #1) global meat data
        meta = gen.next()
        while True:
            #2) record header param
            gparam = record_gen.next()
            while True:
                #3) seq unit records
                sunit = record_gen.next()   #None at EOF, False at end-of-record
                if not sunit:
                    break;
            if sunit == None:
                break

'''
def record_generator(fname):

    VALID_FILE_SUFFIX = ('.fastq.gz', '.fastq')
    VALID_LAB_ACTION_CODE = LAB_ACTION_MAP.keys()
    VALID_LAB_ACTION_CODE.sort()

    VALID_GEN_SIZE_METHOD_CODE = GENOME_SIZE_EST_METHOD_MAP.keys()
    VALID_GEN_SIZE_METHOD_CODE.sort()

    analyst = None
    analyst_id = None
    email = None

    def next_line(fh, noempty=True, nocom=True):
        'skip empty line (noempty) and lines start with # (nocom). return line string, or None at EOF'
        global LINE_CNT

        while True:
            line = fh.readline()
            LINE_CNT += 1

            if not line:
                return None

            if (noempty and not line.strip()) or (nocom and line.startswith('#')):   # ignore empty line, comment lines, or line without seq unit name
                continue
            else:
                break

        return line

    def get_global_param(line, tag):
        if not line:
            write_log('Fatal Error - expect param for [%s] in line @%d' % (tag, LINE_CNT), mtype=ACTION_FAIL, logfile=None, eCode=1)

        tok = re.split('[: ]+', line)
        if len(tok) < 2:
            write_log('Fatal Error - expect param for [%s] in line @%d' % (tag, LINE_CNT), mtype=ACTION_FAIL, logfile=None, eCode=1)
        else:
            return tok[1].strip()

    #pbio-743.7044.fastq                     0.0119              1.488                1                  -1                  -1
    def validate_seq_unit_line(line):
        'must start with a seq unit name, followed with 5 float numbers'
        tok = line.split()
        if len(tok) != 6 or not tok[0].endswith(('.fastq', '.fastq.gz')):
            return False
        for itm in tok[1:]:
            try:
                float(itm)
            except:
                return False
        return True

    def get_seq_unit_param(line):
        if not line:
            write_log('Fatal Error - expect seq unit line but get NULL at @%d' % LINE_CNT, mtype=ACTION_FAIL, logfile=None, eCode=1)
        tok = line.split()
        if len(tok) != 6:
            write_log('Fatal Error - expect seq unit line with 6 params but get [%d] in line @%d' % (len(tok), LINE_CNT), mtype=ACTION_FAIL, logfile=None, eCode=1)


        try:
            cla = float(tok[2])
            if cla < 0:
                #print('DEBUG line=[%s]' % line)
                write_log('Fatal Error - seq unit line : CLA must be a positive value L:%d [%s]; 2nd element in \"%s\"' % (LINE_CNT, tok[2], "\t".join(tok)), mtype=ACTION_FAIL, logfile=None, eCode=1)
        except ValueError as e:
            write_log('Fatal Error - seq unit line: CLA float number parsing error L:%d   value=[%s]   error=[%s]' % (LINE_CNT, tok[2], str(e)), mtype=ACTION_FAIL, logfile=None, eCode=1)

        validate_value(tok[3], '01', 'Useable', line)

        sunit = {
            'sun' : tok[0],
            'total-logical-amount-completed' : cla,
            'data_useable': int(tok[3]),
            'fm_where': tok[4],
            'fm_what': tok[5]
        }

        # failure mode
        # it is optional if USEABLE=1 (where, -1 meaning )
        try:
            sunit['fm_where'] = int(sunit['fm_where'])
        except:
            write_log('Fatal Error: wrong format in FM_M @%d\n%s' % (LINE_CNT, line), None, None, 1)

        if sunit['data_useable'] == '0' or sunit['fm_where'] != -1:
            validate_value(sunit['fm_where'], sorted(FM_MAJOR_CODE.keys()), 'Failure Mode Where (FM_M)', line)

        try:
            sunit['fm_what'] = int(sunit['fm_what'])
        except:
            write_log('Fatal Error: wrong format in FM_m: \n%s' % line, None, None, 1)

        if sunit['fm_where'] != -1:
            validate_value(sunit['fm_what'], xrange(len(FM_DATA[FM_MAJOR_CODE[sunit['fm_where']]])), 'Failure Mode What (FM_m)', line)

        # FM Code to CV
        if sunit['fm_what'] > -1 and sunit['fm_where'] > -1:
            sunit['fm_what'] = FM_DATA[FM_MAJOR_CODE[sunit['fm_where']]][sunit['fm_what']]
            sunit['fm_where'] = FM_MAJOR_CODE[sunit['fm_where']]
        else:
            del sunit['fm_where']
            del sunit['fm_what']

        return sunit

    def check_header(line, header):
        if not line.startswith(header):
            write_log('Fatal Error - expect leading tag of [%s] in L:%d line=[%s]' % (header, LINE_CNT, line), mtype=ACTION_FAIL, logfile=None, eCode=1)
            return False
        else:
            return True



    ##- start process the input file
    with open(fname, 'r') as FH:

        ##-- the global meta data
        while True:
            line = next_line(FH)
            if line == None:
                break;

            if not analyst: # have to have the analyst set before data lines
                analyst = get_analyst(line)
                aid = None
                if analyst:
                    ainfo = analyst_info(sth, analyst)
                    if not ainfo:
                        write_log('Fatal Error: Analyst not found in RQC for [%s]. Please update your input file with \"Analyst=YOUR NAME\" as recorded in RQC.' % analyst, None, None, 1)
                else:
                    write_log('Failed to find the Required Analyst entry in input', None, None, 1)

                continue

            if line.find('email') == 0 and line.find('=') > 0:
                token = line.split('=')
                if len(token) == 2:
                    email = token[1].strip()
                continue



            if line.startswith('Lib_Name'):
                if not email and ainfo:
                    email = ainfo.get('email')
                    if email and email.find('@') > -1:
                        email = str(email.split('@')[0])
                global_meta = { 'platform'      : 'pacbio',
                                'analyst'       : analyst,
                                'analyst_id'    : ainfo.get('analyst_id'),
                                'email_name'    : email,
                            }

                yield global_meta
                break;

        ##-- main loop for the rest of the input file
        while True:

            ##-- global param data
            gparam = {}

            #expecting the library name line, e.g. : "Lib_Name:                 PPZB"
            check_header(line, BLOCK_HEADER_LIB_NAME)
            gparam['library_name'] = get_global_param(line, 'library name')
            line = next_line(FH)

            #expecting the TLA line, e.g. : "TLA:                      50.0"
            check_header(line, BLOCK_HEADER_LIB_TLA)
            gparam['targeted_amount'] = float(get_global_param(line, 'tla value'))
            line = next_line(FH)

            #expecting the Genome_Size(mb) line, e.g. : "Genome_Size(mb):             -1"
            check_header(line, BLOCK_HEADER_LIB_GENE_SIZE)
            gparam['revised-genome-size-mb'] = float( get_global_param(line, 'genome size (mb) value') )
            line = next_line(FH)

            #expecting the Genome_Size_Method line, e.g. : "Genome_Size_Method:                     1"
            check_header(line, BLOCK_HEADER_LIB_GENE_SIZE_MET)
            gparam['genome-size-estimation-method'] = get_global_param(line, 'genome size est method value')
            if gparam['revised-genome-size-mb'] > 0:
                validate_value(gparam['genome-size-estimation-method'], VALID_GEN_SIZE_METHOD_CODE, 'Genome_Size_Method', line)
                gparam['genome-size-estimation-method'] = GENOME_SIZE_EST_METHOD_MAP[gparam['genome-size-estimation-method']]
            else:
                del gparam['genome-size-estimation-method']
                del gparam['revised-genome-size-mb']
            line = next_line(FH)

            #expecting the lab action line, e.g. : "Lab_Action:               0"
            check_header(line, BLOCK_HEADER_LIB_LAB_ACTION)
            gparam['lab_action'] = get_global_param(line, 'library action')
            validate_value(gparam['lab_action'], VALID_LAB_ACTION_CODE, 'Lab Action', line)
            gparam['lab_action'] = LAB_ACTION_MAP[gparam['lab_action']]
            line = next_line(FH)

            #expecting the abandon library line, e.g. : "Abandon_Library:           N"
            check_header(line, BLOCK_HEADER_LIB_AB_LIB)
            gparam['abandon_library'] = get_global_param(line, 'abandon library')
            validate_value(gparam['abandon_library'], 'YN', 'Abandon Library', line)
            line = next_line(FH)

            #expecting the abandon sample line, e.g. :  "Abandon_Sample:           N"
            check_header(line, BLOCK_HEADER_LIB_AB_SAM)
            gparam['abandon_sample'] = get_global_param(line, 'abandon sample')
            validate_value(gparam['abandon_sample'], 'YN', 'Abandon Sample', line)
            line = next_line(FH)

            #expecting the sow status line, e.g. :  "SOW_Status:               0"
            check_header(line, BLOCK_HEADER_LIB_SOW_STATUS)
            gparam['sow-item-complete'] = get_global_param(line, 'sow status')
            validate_value(gparam['sow-item-complete'], '012', 'SOW Stats', line)
            gparam['sow-item-complete'] =  SOW_STATUS_MAP[gparam['sow-item-complete']]
            line = next_line(FH)

            #expecting the comments line, e.g. :  "Comments:               this is a comments line"
            check_header(line, BLOCK_HEADER_LIB_COMMENTS)
            tok = line.split(':')
            comments = ''
            if len(tok) == 2 and tok[1].strip():    # inline (single-line) comments following "Comments:"
                comments = tok[1].strip()
            else:                                   #multi-line comments under "Comments:", terminated with "#END_COM"
                cline = []
                while True:
                    line = next_line(FH, False, False)
                    if not line:
                        write_log('Fatal Error - read multi-line comments to EOF. May forgot \"#END_COM\" mark?', mtype=ACTION_FAIL, logfile=None, eCode=1)
                    if line.startswith('#END_COM'):
                        if cline:
                            comments = '\n'.join(cline)
                        break
                    else:
                        cline.append(line.rstrip())

            gparam['comments'] = comments

            validate_bad_relation(gparam['abandon_library'] == 'Y' and gparam['lab_action'] > '1', 'Abandon Library cannot pair with LAB_ACTION > 1', line)
            validate_bad_relation(gparam['abandon_sample'] == 'Y' and gparam['lab_action'] != '0', 'Abandon Sample must be paired with LAC_ACTION=0', line)
            validate_bad_relation(gparam['abandon_sample'] == 'Y' and gparam['sow-item-complete'] != SOW_STATUS_MAP['2'], 'Abandon Sample requires SOW=2', line)

            if gparam['abandon_sample'] == 'Y':
                gparam['sow-item-complete'] = 'Needs Attention'

            gparam['abandon_library'] = YN2INT(gparam['abandon_library'])
            gparam['abandon_sample'] = YN2INT(gparam['abandon_sample'])

            if gparam['targeted_amount'] < 0:
                write_log('Fatal Error: Negative values in TLA field found for %s.' % gparam['library_name'], None, None, 1)

            yield gparam


            ##-- seq units
            while True:
                line = next_line(FH)
                if line == None:
                    yield None
                    break
                elif line.startswith('Lib_Name'):
                    yield False
                    break;
                else:
                    sunit = get_seq_unit_param(line)
                    yield sunit


            if line == None:
                yield None
                break


def process_input(sth, ifile, toprint, atype, live):
    'Carry out the segment qc process on a given input file.'
    DASH_LEN = 100

    write_log('')
    write_log('=' * DASH_LEN)
    write_log('input file        : %s' % ifile)
    if not live:
        write_log(' --> this is a dry run. use -l | --live option to really perform the action')
    write_log('-' * DASH_LEN)
    write_log('Start time: ' + time_now_str() + '\n')

    write_log('Processing input file %s    ...' % os.path.abspath(ifile))
    rec_list = []    # hold seq unit name as key for duplication and segment id check

    #collect submission records from the input file into rec_list
    record_gen = record_generator(ifile)

    global_meta = record_gen.next() # global meta data
    #print('DEBUG : global_meta=%s' % str(global_meta))

    while True:
        rec = {'global': record_gen.next(), 'sus': []}
        rec_list.append(rec)
        while True:
            sunit = record_gen.next()   # get a new seq unit from input for the
            if not sunit:
                break;
            else:
                rec['sus'].append(sunit)
        if sunit == None:
            break



    def summar_line(msg, iVal):
        write_log('%-40s : %d' % (msg, iVal) )

    if rec_list:
        action_cnt = 0
        action_succeeded = 0
        action_failed = 0

        can_try_cnt = 0                     # tolal record in input that can be used for submission
        bad_record = 0                      # malformed record count in input file
        qc_have_been_done_before_cnt = 0    # have been QCed count

        if toprint:
            write_log("\nWS URL=%s/%s" % (WEB_HOST, WEB_API))
            write_log('Content Type: application/json')

        #- TODO : debug
        #pp.pprint(rec_list)

        for itm in rec_list:
            action_cnt += 1
            libname = itm['global']['library_name']
            write_log("\n%d) %s - %s" % (action_cnt, atype.title(), libname))

            msg = ''
            mtype = ACTION_OK

            su_qc_status = check_qc_status(sth, itm['sus'])
            if 'errors' in su_qc_status:
                write_log('error: %s' % su_qc_status['errors'])
                for su in su_qc_status['sunits']:
                    write_log('   seq unit %s has been QCed before and thus can not be QCed again.' % su['seq_unit_name'])
                write_log('try to remove the already QCed seq unit from your input file and submit again.')
                qc_have_been_done_before_cnt += 1
                continue
            else:
                can_try_cnt += 1

            check_seq_units_of_lib(sth, libname, itm['sus'])
            rtn = do_one(sth, itm, global_meta, toprint, atype, live)
            if 'errors' in rtn:
                if live:
                    msg = 'action failed'
                    mtype = ACTION_FAIL
                    action_failed += 1
            else:
                if live:
                    msg = 'action completed'
                action_succeeded += 1

            if msg and not toprint:
                write_log(msg, mtype)

            write_log('')


            # pause for 5 seconds, to give the PPS WS server a break when the input file has many entries
            if live:
                time.sleep(5)


        write_log('')
        write_log('-' * DASH_LEN)
        summar_line('total records in file', len(rec_list))
        if live:
            summar_line('total records attempted for action', can_try_cnt)
        else:
            summar_line('total records can be used for action', can_try_cnt)

        if live:
            summar_line('total succeeded action', action_succeeded)
            summar_line('total failed action', action_failed)

        summar_line('total records have been QCed before', qc_have_been_done_before_cnt)
        summar_line('total records of bad data', bad_record)
        write_log('=' * DASH_LEN)
        write_log('End time : ' + time_now_str() + "\n")
    elif not analyst:
        write_log('No analyst (mandantory) is defined in input file.')
    else:
        write_log('No data in input file for segment qc.')

'''
Construct the auto qc record for the given lib
SU_NAME  CLA   USEABLE   FM_M   FM_M
-------  ---   -------   ----   ----
'''
def print_pb_record(libname, data, ofile=None):
    FH = None
    if ofile:
        FH = open(ofile, 'w')

    line = '#Submittion block for %s\n#SU_NAME                 CLA      USEABLE      FM_M    FM_m' % libname
    def write_line(msg):
        if FH:
            FH.write('%s\n' % msg)
        else:
            print('%s' % msg)

    write_line(line)
    for row in data:
        baseGB = -1
        if row['sdm_raw_base_count']:
            baseGB = float(row['sdm_raw_base_count']) * 0.000000001
        line = '%s\t%f\t%d\t%d\t%d' % (row['seq_unit_name'], baseGB, 1, -1, -1)
        write_line(line)
    line = '#LAB_ACTION     SOW_STATUS'
    write_line(line)

    line = '%d\t%d' % (-1, -1)
    write_line(line)

def read_libs(sth, ifile, ofile=None):
    libs = []
    with open(ifile, 'r') as FH:
        for line in FH:
            tok = line.strip().split()
            libs.extend(tok)


    for lib in libs:
        platform = platform_name(sth, lib)

        if not platform:
            write_log('\nError : No platform found in RQC [%s]. Maybe a wrong library name?\n' % lib, None, None, 1)
        elif platform.lower() != 'pacbio':
            write_log('\nError : No pacbio library [%s] platform=%s \n' % (lib, platform), None, None, 1)


        sql = '''
            SELECT S.seq_unit_name, S.sdm_raw_base_count, Q.status_id
            FROM seq_units S LEFT JOIN sow_qc Q on S.seq_unit_id=Q.seq_unit_id
            WHERE library_name = %s;
        '''
        sth.execute(sql, (lib, ))
        rows = sth.fetchall()
        if not rows:
            write_log('\nError : library not found in RQC [%s]\n' % lib)
        else:
            print_pb_record(lib, rows, ofile)

def conn_db(dev=True):
    global WEB_HOST

    # developer dev env, if recorgnized
    if dev:
        WEB_HOST = 'https://rqc-4.jgi-psf.org'  # Shijie's dev web server
    elif 'RQC_SW_TOP_LEVEL_DIR' in os.environ:
        print('\nError : You are a developer and need to run this script with the --dev option.\n')
        sys.exit(1)

    conn = None
    if not dev:
        conn = jgi_connect_db('rqc-prod')
    else:
        conn = jgi_connect_db('rqc-dev')

    if conn == None:
        print('Error : failed to connect to RQC database')
        sys.exit(1)

    sth = conn.cursor(MySQLdb.cursors.DictCursor)
    dbparam = dbinfo(sth)
    print('#%s' % dbparam)



    return sth


def get_seq_units(sth, lib):
    'for a given pb lib name, return all seq units, and sdm_raw base count in GB, ignore su that already QC submitted.'
    sql = '''
        SELECT seq_unit_name, sdm_raw_base_count/1000000000 AS base_count_gb
        FROM seq_units S LEFT JOIN sow_qc Q ON S.seq_unit_id=Q.seq_unit_id
        WHERE library_name=%s AND (status_id is NULL OR status_id <> 3)
    '''

    sth.execute(sql, (lib, ))
    rows = sth.fetchall()
    if rows:
        for r in rows:
            r['base_count_gb'] = str(r['base_count_gb'])
    return rows

def get_its_meta(sun):
    url = 'https://rqc.jgi-psf.org/api/sow_qc/its_logical_amounts'
    curl = Curl(url)
    resp = {}

    try:
        resp = curl.get('', data={'seq_unit_name' : sun})
    except CurlHttpException as e:
        resp['errors'] = 'HTTP: ' + str(e)
    except Exception as e:
        resp['errors'] = str(e)

    return resp

def print_input(sth, lib_list):
    print_template(header=True)
    #debug('work on libraris [%s]' % ' '.join(lib_list))

    gparam_label = [
                        BLOCK_HEADER_LIB_NAME,
                        BLOCK_HEADER_LIB_TLA,
                        BLOCK_HEADER_LIB_GENE_SIZE,
                        BLOCK_HEADER_LIB_GENE_SIZE_MET,
                        BLOCK_HEADER_LIB_LAB_ACTION,
                        BLOCK_HEADER_LIB_AB_LIB,
                        BLOCK_HEADER_LIB_AB_SAM,
                        BLOCK_HEADER_LIB_SOW_STATUS,
                        BLOCK_HEADER_LIB_COMMENTS,
                    ]

    gparam_value = ['-1', '-1', '-1', ' N', ' N', '-1', ' YOUR COMMNETS']
    gparam_value_comments = []
    header = ['#SU_NAME', 'CLA_GB', 'CLA',   'USEABLE',     'FM_M',     'FM_m']

    lblmax = max([len(x) for x in gparam_label])
    space = 10

    for lib in lib_list:

        if not is_pacbio_platform(sth, lib):
            write_log('!!! ERROR : Not a PacBio library [%s] !!!' % lib, None, None, 1)
        else:
            sus = get_seq_units(sth, lib)
            if sus:
                itsmeta = get_its_meta(sus[0]['seq_unit_name'])
                tla = -1
                cla = -1
                tla_unit = 'gb'
                if itsmeta and 'errors' not in itsmeta:
                    tla = float(itsmeta.get('its_tla'))
                    tla_unit = itsmeta.get('tla_unit')
                    print('## -  ITS_TLA=%f(%s)     ITS_CLA=%s(%s)' % (tla, tla_unit, itsmeta.get('its_cla'), tla_unit))

                    tla_unit=tla_unit.lower()
                    if tla_unit != 'gb':
                        print('## ?? ITS TLA Unit not in Gb !! Please make CLA values in ITS unit before submit !!')

                    if tla_unit == 'gb':
                        cla = 0.0
                        for su in sus:
                            cla += float(su['base_count_gb'])
                else:
                    if not itsmeta:
                        write_log('!!! ERROR : failed to retrieve TLA unit for [%s] !!!' % lib, None, None, 1)
                    else:
                        write_log('!!! ERROR : failed to retrieve TLA unit for [%s] [%s]!!!' % (lib, itsmeta['errors']), None, None, 1)

                gval = [lib, tla] + gparam_value
                for idx, lbl in enumerate(gparam_label):
                    print('%s:%s%s' % (lbl, ' '*(lblmax-len(lbl) + space), gval[idx]))

                sumax = max([len(x['seq_unit_name']) for x in sus])
                bcmax = max([len(x['base_count_gb']) for x in sus])
                print('%s%s%s%s%s%s%s%s%s%s%s' % (  header[0],
                                        ' '*(40-len(header[0])),
                                        header[1],
                                        ' '*(20-len(header[1])),
                                        header[2],
                                        ' '*(20-len(header[2])),
                                        header[3],
                                        ' '*(20-len(header[3])),
                                        header[4],
                                        ' '*(20-len(header[4])),
                                        header[5],
                                    ) )
                for su in sus:
                    cla = su['base_count_gb']
                    if tla_unit != 'gb':
                        cla = '-1'
                    print('%-40s%-20s%-20s%-20d%-20d%-20d' % (su['seq_unit_name'], su['base_count_gb'], cla, -1, -1, -1))
                print('')
            else:
                print('# !!! ERROR : No seq units are ready for QC (either has been QCed before, or no seq units in RQC) for library=%s !!!' % lib)



def get_list_from_file(fname):
    rtn = []
    with open(fname, 'r') as FH:
        for line in FH:
            line = line.strip();
            rtn += [x for x in re.split('[,;\'"\t ]+', line) if x]
    return rtn

def print_msg(msg, err=False):
    if err:
        print('\nERROR : %s' % msg)
        exit(1)
    else:
        print('%s' % msg)

def debug(msg):
    if DEBUG:
        print('# debug: %s' % msg)

if __name__ == '__main__':
    VERSION = "1.0.0 (8/19/2015)"
    NOTE = '\n'.join([
                        'Bulk PacBio rework QC command line tool.',
                        'Author : Shijie Yao',
                    ])

    USAGE = "* Auto PacBio rework QC, version %s\n" % (VERSION)

    TYPES = ['validate', 'submit', 'save']
    # command line options
    parser = argparse.ArgumentParser(description=USAGE, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input", dest="infile", help="A file containing the rework qc entries.")
    parser.add_argument("-b", "--libs", dest="libs", help="Print out input sample for libs listed in the given files, or inline (csv)")
    parser.add_argument("-l", "--live", dest="live", action='store_true', default=False, help="To actually execute the QC. Else do dry run.")
    parser.add_argument("-p", "--print", dest="toprint", action='store_true', help="Print out the template for input file format.")
    parser.add_argument("-d", "--dev", dest="dev", action='store_true', help="Execute script in development mode.")
    parser.add_argument("-t", "--type", dest="atype", choices=TYPES, default=TYPES[0], help="The run type.")
    parser.add_argument("-", "--hist", dest="history", action='store_true', help="Print coding history")
    parser.add_argument('-v', '--version', action='version', version=VERSION)

    OPTIONS = parser.parse_args()
    sth = conn_db(OPTIONS.dev)

    if OPTIONS.history:
        print('')
        print('Version : %s' % VERSION)
        print(NOTE)
        print('')
        exit(0)

    FM_MAJOR_CODE = failure_mode_data('failure_major_code')
    FM_DATA = failure_mode_data('failure_mode_illumina')

    if OPTIONS.toprint and not OPTIONS.infile:
        print_template()
        exit(0)

    if not OPTIONS.infile and not OPTIONS.libs:
        print_msg('require -i INPUT_DATA_FILE or -b LIBS|LIB_LIST_FILE', err=True)
        sys.exit(1)
    elif OPTIONS.infile and  OPTIONS.libs:
        print_msg('either -i INPUT_DATA_FILE or -b LIBS|LIB_LIST_FILE but not both.', err=True)
        sys.exit(1)


    if OPTIONS.libs:
        lib_list = []
        if os.path.isfile(OPTIONS.libs):
            lib_list = get_list_from_file(OPTIONS.libs)
        else:
            lib_list = OPTIONS.libs.strip().split(',')

        print_input(sth, lib_list)
        sys.exit(0)


    if not os.path.isfile(OPTIONS.infile):
        print('Invalide input file %s!' % OPTIONS.infile)
        sys.exit(1)
    else:
        LOG_FILE = '%s.log' % OPTIONS.infile
        process_input(sth, OPTIONS.infile, OPTIONS.toprint, OPTIONS.atype, OPTIONS.live)
