#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
$ ./ap_tool.py -d -spid 1085148 -tatid 47 --cmd rework

Tool to update PMOS web service
- kind of stupid to write sometime to update a web service when it should have a command line from PMOS
- used by rqc_system code (rqc_monitor), hacksaw, heisenberg, falcon, run_folder

info
- get the ap,at info for the list of spids
-- if add aptype, attype then filter by these id types

Dependencies
its dw connection
rqc db connection
PMOS web service

1.0 - 2015-09-14
- initial production version

1.1 - 2015-11-03
- look up task_type_name from data warehouse since PMOS no longer sending it

1.2 - 2015-11-23
- include current_task_status_id in output

1.3 - 2015-12-14
- be able to use status id or status name (Kurt request)

1.3.1 - 2015-12-16
- status needs to be correct capitalization

1.3.2 - 2015-12-18
- added debug flag, debugged problem for Kurt (jgi.doe.gov vs jgi-psf.org)

1.3.3 - 2016-03-24
- only print the project if the -tatid matches (Kurt & Alicia: 2016-03-23 request)

1.3.4 - 2016-03-31
- do not allow changing of analysis task status if status is in terminal state (completed, deleted, abandoned)

1.4 - 2016-04-18
- rework web service
- added spid lookup given a analysis task id
- includes PM notes also
- use "pmos_url" in .pmos.cfg instead of default

1.4.1 - 2016-04-25
- bug picking up wrong analysis project id (./ap_tool.py -spid 1094205 -tatid 47)

1.4.2 - 2016-04-26
- bug showing analysis task even though its abandoned

1.5 - 2016-05-06
- Vasanth's requirement - update multiple at_id's across multiple ap_ids for a spid

1.5.1 - 2016-05-16
- bug when looking up a task id that doesn't exist
./ap_tool.py -spid 1094613 -tapid 2 -tatid 47 (no tapid 2)

1.6 - 2016-07-25
- exports seq units for an AT
- use lists for tapid and tatids, each maps together
- hide complete, abandoned, deleted ATs by default (Kurt - RQC-834)

1.6.1 - 2016-08-01
- show analysis task status (RQC-844)

1.6.2 - 2016-08-04
- allow analysis task status updates for projects in a terminal state (Alicia couldn't complete a task because the analysis project status was "Complete")

1.6.3 - 2016-08-10
- update status by analysis task id (./ap_tool.py -at 139421 -s 9 --cmd update - Brian - RQC-846)
- show all analysis tasks in a certain state (./ap_tool.py -tatid 32 --cmd list -s complete)

1.6.4 - 2016-08-18
- prevent changing from terminal state to non-terminal state
- prevent changing from state to same state
- --cmd list show tasks status fields

1.6.5 - 2016-08-29
- bug with status id for updating (./ap_tool.py -l AUTPU -tatid 32 --cmd update --status 9)

1.6.6 - 2016-09-07
- changed to use target-state-id for analysis task service instead of target-state
- Rene: IMPORTANT: Notice of parameter deprecation for "Update Analysis Task Status" web service ... 2016-09-06 11:15am

1.6.7 - 2016-09-22
- query by AP: RQCSUPPORT-820

1.6.8 - 2016-09-28
- fix for detecting if in same status

1.6.9 - 2016-10-07
- removed \n from pm_notes field (-spid 1120285)
- include new PMOS web service to return all projects in a particular status
- consolidated web service calls: get_page, post_page
- sets the status to "in progress" then "complete" if status is "created" (Created (1) -> Complete (9) not legal state transition)

1.7.0 - 2016-10-13
- return json format option - doesn't work well with multiple spids

1.7.1 - 2016-10-18
- clean out non-std characters from ap_name
- cmd --list -s created works (used to require status id)

1.7.2 - 2016-10-31
- fix json format option to work with python json module

1.7.3 - 2016-11-16
- set intermediate status to "in progress" then "complete" ("Data Available" (5) -> Complete (9) not legal state transition)

1.8 - 2017-01-25
- correctly pull filtered fastqs tasks with itags
- added sow_item_id to output
~/git/jgi-rqc-pipeline/tools$ ./ap_tool.py -l botwb -m psv -sa
- at_type_id: its = 58, 18s-v4 = 60, 16s-v4 = 37, 16s-v4-pna = 59, 16s-v4-v5 = 70, 16s-v4-v5-pna = 71, custom = 68

1.8.1 - 2017-03-07
- ap_tool.py -at 158222 -- show the analysis task type and analysis task type id, reordered output
- use standard colors library

1.8.2 - 2017-03-14
- show genome & transcriptome reference for analysis project if available

1.8.3 - 2017-03-29
- create unique spid list in get_seq_proj_id (RQCSUPPORT-1172)

1.8.4 - 2017-04-06
- bug where library name not returned if using -spid (./ap_tool.py -spid 1097702 -m json -sa | j)

1.8.5 - 2017-04-28
- show AP status date, need to ask Rene about AT status date (PMOS-45539)
- RQC-951 - not updating genome extraction  (./ap_tool.py -at 160205 --cmd update --status 6) -- ./ap_tool.py -at 110847 --cmd update --status 6 = works
- RQCSUPPORT-1229 - show sample_id in output

1.8.6 - 2017-05-10
- show status date in results for Analysis Project and Analysis Task: PMOS-45539
- fix for Kurt: spid not showing up in tsv: ./ap_tool.py -apid 1119166 -m tsv
- fix for change in PMOS service (no more seq units fields, using pru_ids?)

1.8.7 - 2017-05-12
- fix for Alex Spunde (no spid for apid 1142069) RQC-983
- "$ ./ap_tool.py -at 181898" now shows the project info

1.8.8 - 2017-05-15
- removed task_type_lookup_table - not used and Oracle connection breaks on denovo

1.8.9 - 2017-06-08
- handle pru_id correctly in show_ap_at_info

1.8.10 - 2017-06-19
- fix for lookup_analysis_tasks_per_status

1.8.11: 2017-06-26
- show sow items if using -at option (ap_tool.py -at 201281)

1.8.12: 2017-08-07
- turned off itag sow condition

1.8.13: 2017-10-03
- included new "done" status

1.8.14: 2017-11-20
- PMOS added new state for ATs: "cancelled" (set as a terminal state)
./ap_tool.py -l axpxz -tatid 47 -sa

1.8.15: 2017-11-21
- only allow updates if using -at parameter (Alicia request)

1.8.16: 2017-11-27
- use jgi_connect_db
- show ap/at info for data in air table (not seq_units + lib table)

1.8.17: 2018-03-29
- fail right away if we get an error code and return the exit code (APAT meeting 2018-03-29)


To get dw working on denovo:
1) Change to SLES12
2) module load oracle_client
3) export PATH=/global/homes/b/brycef/miniconda2/bin:$PATH
4) source activate qaqc

Note from 2016-12-15 AP/AT Meeting:
Once AP/AT is in 'Data available' no new SOW can be added
- PM adding SOW to AP in 'data available' is not allowed

New SOW item must be attached to a 'created' AT
- new SOW item when there is no target AT will be  an error
- create JIRA ticket

Now new SOW item can be a SP if AP/AT is 'in progress'
--

Test data:
./ap_tool.py -d -spid 1085148 -tatid 47
./ap_tool.py -d -spid 1084725 -tatid 47

SP id - AP id - AT id
1085148 - 1085147 - 118946
1084725 - 1084705 - 118551


./ap_tool.py -spid 1085148 -tatid 47 --cmd rework


update
- update the ap,ats to "status"
-- apat=12312,1231 --status "complete"
-- not a list because want a success or fail for each possible combo



- Statuses: Created, Data Available, In Progress, Output Created and Awaiting Review, Complete, Abandoned, On Hold, Deleted
- other statuses available but not live yet

Metagenome Metatranscriptome = 10, 38
Eukaryote Community Metatranscriptome = 61, 62

ap_tool -spid 1068225 -tapid 10 -tatid 38
ap_tool -spid 1068227 -tapid 61,62


Rene's documentation:
http://sia-sandbox.jgi-psf.org:8888/mdwiki.html#!index.md

https://docs.google.com/a/lbl.gov/document/d/19HWRo-jkfRW7l-79jWb0_3yR1Tm9QsKoV9yhbPFwekg/edit
$ curl -v -k -H "Authorization: Token token=cc4d9f0ff9d14045e49cabd069e8dbed" -X POST -d "{'updated-by-cid': '11440', 'target-state': 'Data Available'}"

$ curl -v -k https://proposals.jgi.doe.gov/pmo_webservices/analysis_task/160205

Note - curl (whatever) | j --> j is an alias in bryce's env for "python -m json.tool" - formats json output nicely for the screen

Staging:
http://proposals-stage.jgi.doe.gov
- note: staging was failing to pass the token because curl3 library was looking for "proposals.jgi-psf.org"
- Rene will setup test data as needed

AP/AT States
https://docs.google.com/document/d/1q1W4gUBGTsPvnitd2giQjHX66dTBl8E2yobKHOCjSDI/edit#heading=h.me8mrbf5tnfb

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use


import os
import re
import sys
from argparse import ArgumentParser
import MySQLdb
import json

# custom libs in "../lib/"
this_dir = os.path.dirname(__file__)
sys.path.append(os.path.join(this_dir, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from db_access import jgi_connect_db
from curl3 import Curl, CurlHttpException # curl3 has apptoken for pmos web services
from common import get_colors

## Globals
HEADER_CNT = 0 # number of times header printed if csv,tsv...
LIB_DICT = {}

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Show info for all analysis tasks types for a given spid
(not in use yet)
- only good thing is the pm_notes field right now
$ curl https://proposals.jgi-psf.org/pmo_webservices/analysis_tasks?sequencing_project_id=1085207\&analysis_task_type_id=47 | j
'''
def show_at_info(seq_proj_id, target_at_type_id):

    exit_code = 0

    ap_api = "pmo_webservices/analysis_tasks?sequencing_project_id=%s" % (seq_proj_id)
    if target_at_type_id > 0:
        ap_api += "&analysis_task_type_id=%s" % (target_at_type_id)

    response, exit_code = get_page(pmos_url, ap_api)

    return exit_code


'''
atttz = spid 1085207
$ curl https://proposals.jgi-psf.org/pmo_webservices/analysis_projects?sequencing_project_id=1085207 | j
- using ap id 1085201 (spid 1085207)
$ curl https://proposals.jgi.doe.gov/pmo_webservices/analysis_project/1085201 | j

'''
def get_pmo_analysis_projects(pmos_url, seq_proj_id, analysis_project_id):

    response = None
    exit_code = 0

    ap_api = "pmo_webservices/analysis_projects?sequencing_project_id=%s" % seq_proj_id
    if int(analysis_project_id) > 0:
        ap_api = "pmo_webservices/analysis_project/%s" % analysis_project_id

    response, exit_code = get_page(pmos_url, ap_api)

    if type(response) == dict:
        response = [response] # create a single element list for show_ap_at_info to iterate through
        #print type(response)

    return response, exit_code



'''
Show the Analysis Project, Task info

mode = csv = simple output csvnh or cnh = csv w/o title line
- needs seq_proj_id because web service uses spid (pmo_webservices/analysis_projects?sequencing_project_id=%s)

'''
def show_ap_at_info(seq_proj_id, analysis_proj_id, target_ap_type_id, target_at_type_id, pmos_url, mode):

    ap_id = 0
    at_id = 0
    at_list = []

    at_status = ""
    exit_code = 0
    response = None


    # the last at found, even if status is in a terminal state
    at_id_last = 0
    at_status_last = ""

    json_header = ""
    json_buffer = ""

    target_sow_item_id = 0 # look up in LIB_DICT

    terminal_state = ("abandoned", "deleted", "complete", "done", "cancelled")

    if show_all_flag == True:
        terminal_state = ()

    global HEADER_CNT
    global LIB_DICT

    library_name = None

    if str(seq_proj_id) in LIB_DICT:
        library_name = LIB_DICT[str(seq_proj_id)]['lib']
        if target_at_type_id > 0:
            target_sow_item_id = LIB_DICT[str(seq_proj_id)]['sow_item_id']

    sep = get_sep(mode)


    if seq_proj_id or analysis_proj_id:

        if mode != "silent" and sep == "":
            print "-" * 80

            if seq_proj_id > 0:
                if library_name:
                    print "Sequencing Project Id: %s, Library: %s" % (seq_proj_id, LIB_DICT[str(seq_proj_id)]['lib'])
                else:
                    print "Sequencing Project Id: %s" % (seq_proj_id)


        # get projects & tasks
        response, exit_code = get_pmo_analysis_projects(pmos_url, seq_proj_id, analysis_proj_id)

        my_buffer = "Seq Proj Id,Analysis Project Name,Analysis Product Name,Analysis Product Type Id,Analysis Project Id,Analysis Project Status,Analysis Task Name,Analysis Task Type Id,Analysis Task Id,Analysis Task Status,Analysis Task Status Id,PM Notes,PRU IDs,Library Name,SOW Item Id,Sample Id"
        if mode == "json":
            json_header = my_buffer.replace(",", sep)


        if sep and HEADER_CNT == 0:
            if mode != "json":
                print my_buffer.replace(",", sep)

            HEADER_CNT += 1 # global var, don't print the header more than once - if multiple seq_project_ids


        if response:

            for thing in response:

                ap_header = ""
                my_proj_list = []
                at_cnt = 0
                uss_key = "uss_analysis_project"
                if uss_key in thing:

                    prod_name = thing[uss_key].get('analysis_product_type_name')

                    analysis_project_id = thing[uss_key].get('analysis_project_id')
                    analysis_status = thing[uss_key].get('status_name')
                    status_date = thing[uss_key].get('status_date')
                    task_list = thing[uss_key].get('analysis_tasks')
                    proj_name = thing[uss_key].get('analysis_project_name')
                    prod_id = thing[uss_key].get('analysis_product_id')

                    genome_ref = thing[uss_key].get("ref_genome")
                    genome_ref_type = thing[uss_key].get("ref_genome_type")
                    trans_ref = thing[uss_key].get("ref_transcriptome")
                    trans_ref_type = thing[uss_key].get("ref_transcriptome_type")


                    proj_name = clean_str(proj_name)


                    # if using ap_id instead of spid, we can look up the spid in the json data
                    if seq_proj_id == 0:
                        if 'sequencing_projects' in thing[uss_key]:
                            seq_proj_list = thing[uss_key]['sequencing_projects']
                            spid_list = []
                            for sp in seq_proj_list:
                                spid_list.append(str(sp['sequencing_project_id']))
                            #print "Sequencing Project Id: %s" % ",".join(spid_list)

                            #seq_proj_id = 0
                            if len(spid_list) > 0:
                                seq_proj_id = spid_list[0]

                    print_analysis_project_flag = 0
                    if target_ap_type_id > 0:
                        if prod_id == target_ap_type_id:
                            print_analysis_project_flag = 1
                            ap_id = analysis_project_id
                    else:
                        print_analysis_project_flag = 1

                    if mode == "silent":
                        print_analysis_project_flag = 0

                    if print_analysis_project_flag == 1:

                        if sep:
                            my_proj_list = [seq_proj_id, proj_name, prod_name, prod_id, analysis_project_id, analysis_status]

                        else:

                            ap_header += "\n"
                            ap_header += "Analysis Project Name: %s\n" % (proj_name)
                            ap_header += "Analysis Product Name: %s\n" % (prod_name)
                            ap_header += "Analysis Product Type Id: %s\n" % (prod_id)
                            ap_header += "Analysis Project Id: %s\n" % (analysis_project_id)
                            ap_header += "Analysis Project Status: %s (%s)\n" % (analysis_status, status_date)
                            if genome_ref:
                                ap_header += "Genome Reference: %s (%s)\n" % (genome_ref, genome_ref_type)
                            if trans_ref:
                                ap_header += "Transcriptome Reference: %s (%s)\n" % (trans_ref, trans_ref_type)
                            #ap_header += "\n"

                    at_cnt = 0
                    at_buffer = ""
                    for my_task in task_list:
                        my_list = []
                        #print my_task

                        task_name = my_task.get("analysis_task_type_name")
                        analysis_task_id = my_task.get("analysis_task_id")
                        analysis_task_type_id = my_task.get("analysis_task_type_id")

                        status = my_task.get("status_name")
                        status_id = my_task.get("current_status_id")
                        status_date = my_task.get("status_date")
                        status_date = status_date[0:10]

                        pm_notes = str(my_task.get("pm_notes"))
                        #seq_units_list = my_task.get("seq_unit_filenames")
                        pru_list = my_task.get("pru_ids") # new - 2017-05-10
                        sow_item_list = my_task.get("sow_item_ids")
                        task_sample_id = my_task.get("sample_id")

                        # spid 1120285 - has new line
                        pm_notes = convert_pm_notes(pm_notes)

                        prus = None
                        if len(pru_list) > 0:
                            prus = ";".join(str(x) for x in pru_list)

                            #seq_units = seq_units.replace(",", ";") # prevents [abc.123.fastq,def.234.fastq] from becoming [abc.123.fastq|def.234.fastq] (-m psv)

                        print_analysis_task_flag = 0

                        if target_at_type_id > 0:

                            # turn off this condition when filtering is ready for everything
                            # shows the AP even though it won't have the target at type id
                            #itag_skip = 1
                            #if prod_id != 30 and target_at_type_id == 72:
                            #    itag_skip = 1


                            if analysis_task_type_id == target_at_type_id: # or itag_skip == 1:

                                print_analysis_task_flag = 1

                                # store last task id found, even if in terminal state
                                at_id_last = analysis_task_id
                                at_status_last = status

                                if status.lower() in terminal_state:
                                    pass

                                else:
                                    at_id = analysis_task_id

                                    at_status = status
                                    # if multiple at's, grab the one not complete

                                    at_list.append("%s/%s/%s" % (analysis_project_id, analysis_task_id, status_id))

                                    if ap_id == 0:
                                        if analysis_status.lower() in terminal_state:
                                            pass
                                        else:
                                            ap_id = analysis_project_id


                        else:
                            print_analysis_task_flag = 1
                            if print_analysis_project_flag == 0:
                                print_analysis_task_flag = 0

                        if mode == "silent":
                            print_analysis_task_flag = 0

                        # 2017-01-24, prod_id == 30 = itags
                        # remove the if prod_id == 30 when filtering is on for everything from PMOS

                        if target_ap_type_id > 0:
                            if prod_id != target_ap_type_id:
                                print_analysis_task_flag = 0

                        if target_sow_item_id > 0:
                            if prod_id == 30 and 1==2: # itag analysis, turned off condition 2017-08-07
                            #if 1==1:
                                if type(sow_item_list) == list:
                                    if target_sow_item_id not in sow_item_list:
                                        print_analysis_task_flag = 0



                        if print_analysis_task_flag == 1:

                            #at_list.append("%s/%s/%s" % (analysis_project_id, analysis_task_id, status_id))

                            if sep:
                                my_list.append(task_name)
                                my_list.append(analysis_task_type_id)
                                my_list.append(analysis_task_id)
                                my_list.append(status)
                                my_list.append(status_id)
                                my_list.append(pm_notes)
                                my_list.append(prus)
                                # add these after library
                                #my_list.append(sow_item_list)
                                #my_list.append(task_sample_id)

                                # join doesn't like ints!
                                #print sep.join(my_list)
                                my_buffer = ""
                                for i in my_proj_list:
                                    my_buffer += "%s%s" % (str(i).replace(sep, " "), sep)

                                for i in my_list:
                                    my_buffer += "%s%s" % (str(i).replace(sep, " "), sep)

                                if library_name:
                                    my_buffer += "%s%s" % (library_name, sep)
                                else:
                                    # spids can have multiple library names, so don't show it if we didn't receive it
                                    my_buffer += "None%s" % sep

                                my_buffer += "%s%s" % (sow_item_list, sep)
                                my_buffer += "%s%s" % (task_sample_id, sep)

                                # don't show terminal states in csv
                                if status.lower() in terminal_state:
                                    pass
                                else:

                                    if mode == "json":
                                        json_buffer += my_buffer[:-1] + "\n"
                                    else:
                                        print my_buffer[:-1]

                            else:

                                # only print if AP not in terminal state
                                if status.lower() in terminal_state:
                                    if debug_flag:
                                        print "%s- AT %s in terminal state: %s%s" % (color['red'], analysis_task_id, status, color[''])
                                else:
                                    at_cnt += 1
                                    #at_buffer += "\n"
                                    at_buffer += "- analysis task name: %s\n" % (task_name)
                                    at_buffer += "- analysis task type id: %s\n" % (analysis_task_type_id)
                                    at_buffer += "- analysis task id: %s\n" % (analysis_task_id)
                                    at_buffer += "- analysis task status: %s (%s) (%s)\n" % (status, status_id, status_date)
                                    at_buffer += "- pm notes: %s\n" % (pm_notes)
                                    at_buffer += "- pru ids: %s\n" % (prus) # 2017-05-10
                                    at_buffer += "- sow items: %s\n" % (sow_item_list)
                                    at_buffer += "- sample id: %s\n" % (task_sample_id)
                                    at_buffer += "\n"

                # if we found at least one analysis task to show then print the header & buffer
                # - Kurt bug 2016-03-23, but he should really not be parsing it this way
                if at_cnt > 0:

                    print ap_header
                    print at_buffer,

                    # add "," to the end to not print "\n" - needed in this case

    else:
        print "Cannot find data for sequencing project id: %s" % (seq_proj_id)
        exit_code = 11

    if mode == "json":
        print make_json(json_header, json_buffer)


    if at_id == 0 and at_id_last > 0:
        at_id = at_id_last
        at_status = at_status_last
        if debug_flag:
            print "# using last found analysis task, no analysis tasks in non-terminal state"

    #print at_list

    #return ap_id, at_id, at_status, exit_code
    return at_list, exit_code



'''
Call pmos to update the analysis task
- dw: select * from analysis_task_status_cv;

http://sia-sandbox.jgi-psf.org:8765/pmo_webservices/analysis_task/:id/state_transition

'''
def update_ap_at_status(ap_id, at_id, at_status, new_status, pmos_url, pmos_cid, pmos_token):

    # list of valid statuses
    #valid_status_list = ['created', 'data available', 'in progress', 'output created and awaiting review', 'complete', 'abandoned', 'on hold', 'deleted']


    # terminal states
    terminal_list = ["complete", "abandoned", "deleted", "done", "cancelled"]

    my_new_status_id, my_new_status_str = convert_status(new_status)
    at_status_id, at_status_str = convert_status(at_status)

    #print ap_id, at_id

    exit_code = 0

    # if at_status in terminal state then return error - cannot change state
    if str(at_status_str).lower() in terminal_list:
        ap_id = 0
        at_id = 0
        print "* Error: cannot change status to %s because analysis task status in terminal state: %s" % (new_status, at_status)
        exit_code = 12
        return exit_code


    if not str(my_new_status_str).lower() in valid_status_dict:
        ap_id = 0
        at_id = 0
        print "* Error: incorrect status!  Cannot set status '%s'" % (new_status)
        exit_code = 17
        return exit_code

    if at_status_id == 1 and my_new_status_id == 9:
        #print "* Error: cannot change status from Created to Complete"
        #exit_code = 18

        #return exit_code
        # change to intermediate "in progress" status (6) - 1.6.9 change
        update_ap_at_status(ap_id, at_id, at_status, 6, pmos_url, pmos_cid, pmos_token)

    elif at_status_id == 5 and my_new_status_id == 9:
        # change to intermediate "in progress" status (6) - 1.7.3 change
        update_ap_at_status(ap_id, at_id, at_status, 6, pmos_url, pmos_cid, pmos_token)

    if ap_id > 0 and at_id > 0 and my_new_status_id > 0:

        exit_code = 0
        response = None

        if at_status_id != my_new_status_id:

            if str(new_status).isdigit():
                # Updating analysis project/task id 1090686/123833 to '9' (Complete)
                print "Updating analysis project/task id %s/%s to '%s' (%s)" % (ap_id, at_id, new_status, my_new_status_str)
            else:
                # Updating analysis project/task id 1090686/123833 to 'Complete' (9)
                print "Updating analysis project/task id %s/%s to '%s' (%s)" % (ap_id, at_id, new_status, valid_status_dict[my_new_status_str.lower()])


            #pmos_url2 = "http://sia-sandbox.jgi-psf.org:8765"
            pmos_state_transition_api = "pmo_webservices/analysis_task/%s/state_transition" % str(at_id)
            #http://sia-sandbox.jgi-psf.org:8765/pmo_webservices/analysis_task/:id/state_transition

            post_body = { "target-state-id" : my_new_status_id, "updated-by-cid" : pmos_cid }
            #print post_body

            response, exit_code = post_page(pmos_url, pmos_state_transition_api, pmos_token, post_body)
            if exit_code > 0:
                print "Error: failed web service call"
                
            if response:

                for r in response:
                    if r in ("See Ya!", "SSL Required"):
                        print "- * need SSL?"
        else:
            print "* Error: new status (%s) == old status (%s)" % (new_status, at_status)
            exit_code = 13


    else:
        if not ap_id > 0:
            print "*Error: missing analysis project id (%s)" % (ap_id)
            exit_code = 14
        if not at_id > 0:
            print "*Error: missing analysis task id (%s)" % (at_id)
            exit_code = 15
        if not new_status:
            print "* Error: missing new status (%s)" % (new_status)
            exit_code = 16


    return exit_code


'''
Rework - sets current AT to "Complete" and creates a new AT for rework

http://sia-sandbox.jgi-psf.org:8889/mdwiki.html#!analysis_task_rework.md
https://proposals.jgi.doe.gov/pmo_webservices/analysis_task/:id/rework

'''
def set_rework(ap_id, at_id, pmos_url, pmos_cid, pmos_token):

    exit_code = 0
    response = None

    if at_id > 0 and ap_id > 0:

        print "Updating analysis project/task id %s/%s to 'rework'" % (ap_id, at_id)

        pmos_rework_api = "/pmo_webservices/analysis_task/%s/rework" % str(at_id)
        post_body = { "updated-by-cid" : pmos_cid }

        response, exit_code = post_page(pmos_url, pmos_rework_api, pmos_token, post_body)

    else:
        if not ap_id > 0:
            print "*Error: missing analysis project id (%s)" % (ap_id)
            exit_code = 14
        if not at_id > 0:
            print "*Error: missing analysis task id (%s)" % (at_id)
            exit_code = 15


    return exit_code




'''
Look up the seq_proj_ids from the library list
-- but it doesn't link the spid to the library name
'''
def get_seq_proj_id(lib_csv):

    global LIB_DICT
    seq_proj_id_list = []

    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)

    library_list = re.split(r'[,;\s:]+', lib_csv)



    if len(library_list) > 0:
        #sql = "select library_name, seq_proj_id from library_info where library_name in ("
        sql = """
select l.library_name, l.seq_proj_id, s.sow_item_id
from library_info l
inner join seq_units s on l.library_id = s.rqc_library_id
where
    l.library_name in (
        """

        sql = "select lib_name as library_name, sp_project_id as seq_proj_id, sow_item_id from air where lib_name in ("

        sql += "%s," * len(library_list)
        sql = sql[:-1]
        sql += ")"



        sth.execute(sql, (library_list))

        row_cnt = int(sth.rowcount)

        for _ in range(row_cnt):

            rs = sth.fetchone()
            seq_proj_id_list.append(rs['seq_proj_id'])
            #LIB_DICT[str(rs['seq_proj_id'])] = rs['library_name']
            LIB_DICT[str(rs['seq_proj_id'])] = { "lib" : rs['library_name'], "sow_item_id" : rs['sow_item_id'] }

            #print "%s = %s" % (rs['library_name'], rs['seq_proj_id'])

    db.close()

    seq_proj_id_list = list(set(seq_proj_id_list)) # make unique list - BOWAA 2 sow items, 1 spid and would return [1129136, 1129136]
    return seq_proj_id_list

'''
Get library list for SPIDs
- create same structure if pass in the library name
'''
def get_lib_list(spid_list):

    global LIB_DICT
    seq_proj_id_list = []

    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)



    if len(spid_list) > 0:
        #sql = "select library_name, seq_proj_id from library_info where library_name in ("
        sql = """
select l.library_name, l.seq_proj_id, s.sow_item_id
from library_info l
inner join seq_units s on l.library_id = s.rqc_library_id
where
    l.seq_proj_id in (
        """

        sql = "select lib_name as library_name, sp_project_id as seq_proj_id, sow_item_id from air where sp_project_id in ("

        sql += "%s," * len(spid_list)
        sql = sql[:-1]
        sql += ")"

        sth.execute(sql, (spid_list))

        row_cnt = int(sth.rowcount)

        for _ in range(row_cnt):

            rs = sth.fetchone()
            seq_proj_id_list.append(rs['seq_proj_id'])

            LIB_DICT[str(rs['seq_proj_id'])] = { "lib" : rs['library_name'], "sow_item_id" : rs['sow_item_id'] }

    db.close()

    seq_proj_id_list = list(set(seq_proj_id_list)) # make unique list - BOWAA 2 sow items, 1 spid and would return [1129136, 1129136]
    return seq_proj_id_list



'''
Look up the sequencing project id for an analysis task id
'''
def find_spid(at_id):

    # return only the string, called by looking up the at id one at a time
    at_list = "" # ap/at/at_status_id
    #sp_id = 0


    if at_id > 0:

        print "Analysis Task Id: %s" % at_id

        sql = """
select
    at.current_status_id, at.analysis_task_type_id, atscv.status,
    ap.final_deliv_project_id, sp.sequencing_project_id, sp.sequencing_project_name,
    ap.analysis_project_id, ap.analysis_product_id, at.analysis_task_type
from analysis_task at
inner join analysis_project ap on at.analysis_project_id = ap.analysis_project_id
inner join sequencing_project sp on ap.final_deliv_project_id = sp.final_deliv_project_id
inner join analysis_task_status_cv atscv on at.current_status_id = atscv.status_id
where
    at.analysis_task_id = :atid
            """



        if debug_flag:
            print "%s%s%s" % (color['green'], sql.replace(":atid", str(at_id)), color[''])
        #print sql

        db_its = jgi_connect_db("its")

        if db_its:
            sth = db_its.cursor()

            sth.execute(sql, atid = at_id)


            for rs in sth:
                #fd_id = rs[0]
                sp_id = rs[4]
                sp_name = rs[5]
                at_status = rs[2]
                at_status_id = rs[0]
                at_type_id = rs[1]
                ap_id = rs[6]
                at_type = rs[8]

                # missing analysis product type,
                print "Sequencing Project Id: %s" % sp_id
                print "Sequencing Project Name: %s" % clean_str(sp_name)
                print "Analysis Project Id: %s" % ap_id
                print "Analysis Task Type (task type id): %s (%s)" % (at_type, at_type_id)
                print "Analysis Task Status (status id): %s (%s)" % (at_status, at_status_id)
                print

                at_list = "%s/%s/%s" % (ap_id, at_id, at_status_id)

            db_its.close()

        else:
            print "* Error: Cannot open data warehouse connection."

    return at_list


'''
 ./ap_tool.py -at 160205,110847 --cmd find-ap
 returns ap_id & current at status
'''
def get_pmos_analysis_task(analysis_task_id, print_mode, mode = "silent"):

    # needed to do an update
    ap_id = 0
    at_status_id = 0
    global HEADER_CNT

    if analysis_task_id > 0:

        exit_code = 0
        response = None


        ap_api = "pmo_webservices/analysis_task/%s" % analysis_task_id
        response, exit_code = get_page(pmos_url, ap_api)
        at_id = 0

        if "uss_rw_analysis_task" in response:
            if "analysis_project" in response['uss_rw_analysis_task']:
                ap_info = response['uss_rw_analysis_task']['analysis_project']
            at_status = response['uss_rw_analysis_task'].get("status_name")
            at_status_id = response['uss_rw_analysis_task'].get("current_status_id")
            at_status_date = response['uss_rw_analysis_task'].get("status_date")
            at_type_id = response['uss_rw_analysis_task'].get("analysis_task_type_id")
            pm_notes = response['uss_rw_analysis_task'].get("pm_notes")
            at_type_name = response['uss_rw_analysis_task'].get("analysis_task_type_name")
            at_id = response['uss_rw_analysis_task'].get("analysis_task_id")
            pru_ids = response['uss_rw_analysis_task'].get("pru_ids") # was seq_unit_filenames: 2017-05-10
            sow_items = response['uss_rw_analysis_task'].get("sow_item_ids")
            sample_id = response['uss_rw_analysis_task'].get("sample_id")

            ap_id = ap_info['analysis_project_id']


            pm_notes = clean_str(pm_notes)

        if mode == "silent":
            at_id = 0

        if at_id > 0:

            sp_list = []
            for s in ap_info['sequencing_projects']:
                sp_list.append(str(s['sequencing_project_id']))

            # print modes - psv, csv, json
            if print_mode.endswith("sv") or print_mode == "json":

                sep = get_sep(print_mode)

                # does not print seq units or library name
                my_header = "Seq Proj Id,Analysis Project Name,Analysis Product Name,Analysis Product Type Id,Analysis Project Id,Analysis Project Status,Analysis Task Name,Analysis Task Type Id,Analysis Task Id,Analysis Task Status,Analysis Task Status Id,PM Notes,SOW Item Id,Sample Id,PRU Ids"
                my_buffer = my_header
                if sep and HEADER_CNT == 0:
                    if print_mode != "json":
                        print my_buffer.replace(",", sep)
                        HEADER_CNT += 1


                sp = " ".join(sp_list)
                my_list = [sp, ap_info['analysis_project_name'], ap_info['analysis_product_type_name'], ap_info['analysis_product_id'], ap_info['analysis_project_id'],
                           ap_info['status_name'], at_type_name, at_type_id, at_id, at_status, at_status_id, pm_notes, sow_items, sample_id, pru_ids]

                my_list = map(str, my_list)
                if print_mode == "json":
                    # does not join them together well ...
                    print make_json(my_header.replace(",","|"), "|".join(my_list))
                    #HEADER_CNT += 1
                    #print HEADER_CNT
                else:
                    print sep.join(my_list)


            else:

                print "-" * 80

                if len(sp_list) > 1:
                    print "Sequencing Project Ids: %s" % (", ".join(sp_list))
                elif len(sp_list) == 1:
                    print "Sequencing Project Id: %s" % (", ".join(sp_list))
                else:
                    print "Sequencing Project Id: None"

                print
                print "Analysis Project Name: %s" % (ap_info['analysis_project_name'])
                print "Analysis Product Name: %s" % (ap_info['analysis_product_type_name'])
                print "Analysis Product Type Id: %s" % (ap_info['analysis_product_id'])
                print "Analysis Project Id: %s" % (ap_info['analysis_project_id'])
                print "Analysis Project Status: %s" % (ap_info['status_name'])

                print
                print "- analysis task name: %s" % at_type_name
                print "- analysis task type id: %s" % at_type_id
                print "- analysis task id: %s" % at_id
                print "- analysis task status: %s (%s) (%s)" % (at_status, at_status_id, at_status_date)
                print "- pm notes: %s" % clean_str(pm_notes)
                print "- pru ids: %s" % pru_ids
                print "- sow items: %s" % sow_items
                print "- sample id: %s" % sample_id
                print



    return ap_id, at_status_id

'''
Use pmos web service to return all projects in a particular state
- has a limit of 10,000 records
'''
def lookup_analysis_tasks_per_status(tat_id, task_status, mode):

    task_status_id, _ = convert_status(task_status)

    my_header = ""
    my_buffer = ""
    sep = get_sep(mode)
    if not sep:
        sep = ","

    if tat_id > 0 and task_status_id > 0:

        my_header = "SPID|AP Name|AP Type Name|AP Type Id|AP Id|AP Status|AT Name|AT Type Id|AT Id|AT Status|AT Status Id|PM Notes"
        my_header = my_header.replace("|", sep)

        pmos_waiting_api = "pmo_webservices/analysis_tasks/awaiting?analysis_task_type_id=[att_id]&analysis_task_state_id=[ats_id]"
        pmos_waiting_api = pmos_waiting_api.replace("[att_id]", str(tat_id)).replace("[ats_id]", str(task_status_id))
        response, exit_code = get_page(pmos_url, pmos_waiting_api)



        my_buffer = ""
        if 'analysis_tasks' in response:
            for my_tasks in response['analysis_tasks']:

                ap_dict = my_tasks['analysis_project']
                ap_type_id = ap_dict.get('analysis_product_id')

                ap_type_name = ap_dict.get('analysis_product_type_name')
                ap_id = ap_dict.get('analysis_project_id')
                ap_name = ap_dict.get('analysis_project_name')
                ap_status = ap_dict.get('status_name')

                spid = 0
                if 'sequencing_projects' in ap_dict:
                    for my_spid in ap_dict['sequencing_projects']:
                        spid = my_spid['sequencing_project_id']

                at_id = my_tasks.get('analysis_task_id')
                at_type_id = my_tasks.get('analysis_task_type_id')
                at_type_name = my_tasks.get('analysis_task_type_name')
                pm_notes = str(my_tasks.get('pm_notes'))



                at_status = my_tasks.get('status_name')
                at_status_id = my_tasks.get('current_status_id')

                # clean up
                pm_notes = convert_pm_notes(pm_notes)
                ap_name = clean_str(ap_name)
                pm_notes = clean_str(pm_notes)


                my_list = [str(spid), ap_name, ap_type_name, str(ap_type_id), str(ap_id), ap_status, at_type_name, str(at_type_id), str(at_id), at_status, str(at_status_id), pm_notes]
                my_buffer += sep.join(my_list) + "\n"


            if mode == "json":
                print make_json(my_header, my_buffer)
            else:
                print my_header
                print my_buffer

        else:
            print "- Error: bad data returned from %s" % pmos_waiting_api


    else:
        print "- Error: missing tatid (%s) or task status_id (%s)" % (tat_id, task_status_id)



'''
Return a list of all projects for a target analysis task type id and status (id)
- replace with pmos web service & filter by tat_id
pmos_analysis_tasks_awaiting_api = pmo_webservices/analysis_tasks/awaiting?analysis_task_type_id=[att_id]&analysis_task_state_id=[ats_id]
'''
def lookup_analysis_tasks_per_status_dw(tat_id, task_status, mode):

    sep = get_sep(mode)
    if not sep:
        sep = ","

    my_buffer = ""
    status_id = 0

    if not str(task_status).isdigit():
        # convert to digit
        task_status = task_status.strip().lower()

        if task_status in valid_status_dict:
            status_id = valid_status_dict[task_status]
    else:
        status_id = task_status

    if tat_id > 0 and status_id > 0:

        sql = """
select
    ap.analysis_project_name,
    ap.analysis_strategy_name,
    ap.analysis_strategy_id,
    ap.current_status_id,
    aps.status as ap_status,
    at.analysis_project_id,
    at.analysis_task_id,
    at.analysis_task_type_id,
    att.analysis_task_type,
    at.current_status_id,
    ats.status,
    at.pm_notes,
    sp.sequencing_project_id,
    sp.sequencing_project_name
from analysis_task at
inner join analysis_project ap on at.analysis_project_id = ap.analysis_project_id
inner join analysis_task_status_cv ats on at.current_status_id = ats.status_id
inner join analysis_task_type_cv att on at.analysis_task_type_id = att.analysis_task_type_id
inner join analysis_project_status_cv aps on ap.current_status_id = aps.status_id
inner join sequencing_project sp on ap.final_deliv_project_id = sp.final_deliv_project_id
where
    at.current_status_id = :tstatusid
    and at.analysis_task_type_id = :tatid
order by sp.sequencing_project_id, ap.analysis_project_id, at.analysis_task_id
        """

        if debug_flag:
            print "%s%s%s" % (color['green'], sql.replace(":tatid", str(tat_id)).replace(":tstatusid", str(status_id)), color[''])
        #print sql

        db_its = jgi_connect_db("its")

        my_header = "SPID|AP Name|AP Type Name|AP Type Id|AP Id|AP Status|AT Name|AT Type Id|AT Id|AT Status|AT Status Id|PM Notes"
        my_header = my_header.replace("|", sep)
        #  my_header = my_header.replace("AP ", "Analysis Product ").replace("AT ", "Analysis Task ")


        if db_its:
            sth = db_its.cursor()

            sth.execute(sql, tatid = tat_id, tstatusid = status_id)

            for rs in sth:

                # field order from query: 0 1 2 5 3 8 7 6 10 9 11

                pm_notes = convert_pm_notes(rs[11])
                pm_notes = clean_str(pm_notes)
                ap_name = clean_str(str(rs[0]))

                b = [str(rs[12]), ap_name, str(rs[1]), str(rs[2]), str(rs[5]), str(rs[3]), str(rs[8]), str(rs[7]), str(rs[6]), str(rs[10]), str(rs[9]), pm_notes]

                my_buffer += sep.join(b) + "\n"

            db_its.close()

            if mode == "json":
                print make_json(my_header, my_buffer)
            else:
                print my_header
                print my_buffer

        else:
            print "* Error: Cannot open data warehouse connection."
    else:
        print "* Error: Cannot find status '%s' or target analysis type id: %s" % (task_status, tat_id)



'''
Look up the analysis task info from data warehouse using the sequencing project id

'''
def lookup_analysis_task(spid):

    if spid > 0:

        db = jgi_connect_db("rqc")
        sth = db.cursor(MySQLdb.cursors.DictCursor)

        sql = "select final_deliverable_id from library_info where seq_proj_id = %s"
        sql = "select fd_project_id as final_deliverable_id from air where sp_project_id = %s"
        sth.execute(sql, (spid))

        rs = sth.fetchone()
        if rs:
            fdid = rs['final_deliverable_id']


        db.close()

        if fdid > 0:


            print "* Analysis Project & Tasks for Sequencing/Final Project Id: %s/%s" % (spid, fdid)
            print

            sql = """
select
    ap.analysis_project_name,
    ap.analysis_strategy_name,
    ap.analysis_strategy_id,
    ap.current_status_id,
    aps.status as ap_status,
    at.analysis_project_id,
    at.analysis_task_id,
    at.analysis_task_type_id,
    att.analysis_task_type,
    at.current_status_id,
    ats.status,
    at.pm_notes
from analysis_task at
inner join analysis_project ap on at.analysis_project_id = ap.analysis_project_id
inner join analysis_task_status_cv ats on at.current_status_id = ats.status_id
inner join analysis_task_type_cv att on at.analysis_task_type_id = att.analysis_task_type_id
inner join analysis_project_status_cv aps on ap.current_status_id = aps.status_id
where
    ap.final_deliv_project_id = :fdid
            """

        #print sql

        db_its = jgi_connect_db("its")

        if db_its:
            sth = db_its.cursor()

            sth.execute(sql, fdid = fdid)

            ap_old = "Anywhere but here" # FFDP
            for rs in sth:

                ap = rs[0]
                ap_strategy = rs[1] # prodect name
                ap_type_id = rs[2] # product id
                ap_status_id = rs[3]
                ap_status = rs[4]
                ap_id = rs[5]

                at_id = rs[6]
                at_type_id = rs[7]
                at = rs[8]
                at_status_id = rs[9]
                at_status = rs[10]
                pm_notes = rs[11]

                ap = clean_str(ap)
                pm_notes = convert_pm_notes(pm_notes)
                pm_notes = clean_str(pm_notes)

                if ap_old != ap:
                    print "Analysis Project Name: %s" % ap
                    print "Analysis Product Name: %s" % ap_strategy
                    print "Analysis Product Type Id: %s" % ap_type_id
                    print "Analysis Project Id: %s" % ap_id
                    print "Analysis Project Status: %s (%s)" % (ap_status, ap_status_id)
                    print

                print "- analysis task name: %s" % at
                print "- analysis task type id: %s" % at_type_id
                print "- analysis task id: %s" % at_id
                print "- analysis task status: %s (%s)" % (at_status, at_status_id)
                if pm_notes:
                    print "- pm notes: %s" % pm_notes
                print

                ap_old = ap

            db_its.close()

        else:
            print "* Error: cannot find final deliverable id for sequencing project id: %s" % spid


## -- helper functions

'''
Get separator type
'''
def get_sep(mode):
    sep = "" # separator
    mode = mode.lower()
    if mode.endswith("sv"):
        if mode.startswith("c"):
            sep = ","
        elif mode.startswith("t"):
            sep = "\t"
        elif mode.startswith("p"):
            sep = "|" # pipe!

    if mode == "json":
        sep = "|" # convert to json later

    return sep


'''
Put all curl get calls into one function
'''
def get_page(my_url, my_api):

    response = None
    exit_code = 0

    if debug_flag:
        print "%s%s/%s%s" % (color['cyan'], my_url, my_api, color[''])

    if my_url and my_api:

        curl = Curl(my_url)

        try:
            response = curl.get(my_api)
        except CurlHttpException as e:

            print "- Failed HTTPS PMOS API Call: %s/%s, %s: %s" % (my_url, my_api, e.code, e.response)
            #code = e.code
            response = e.response
            exit_code = 10

        except Exception as e:
            print "- Failed PMOS API Call: %s/%s, %s" % (my_url, my_api, e.args)
            exit_code = 10


    if response:
        if debug_flag:
            print color['green']
            print json.dumps(response, indent=4, sort_keys=True)
            print color['']
            print

    return response, exit_code

'''
Wrapper function to call post
'''
def post_page(my_url, my_api, my_token, my_body):

    exit_code = 0
    response = None

    if debug_flag:
        print color['cyan']
        print "URL: '%s'" % my_url
        print "API: '%s'" % my_api
        print "Body: '%s'" % my_body
        print "Token: '%s'" % my_token
        print color['']

    if my_url and my_api and my_token and my_body:

        curl = Curl(my_url, appToken = my_token)

        try:
            response = curl.post(my_api, my_body, "raw")

        except CurlHttpException as e:

            print "- Failed HTTPS PMOS API Call: %s/%s, %s: %s" % (my_url, my_api, e.code, e.response)
            #code = e.code
            response = e.response
            exit_code = 10

        except Exception as e:
            print "- Failed PMOS API Call: %s/%s, %s" % (my_url, my_api, e.args)
            exit_code = 10


    if response:
        if debug_flag:
            print color['green']
            print json.dumps(response, indent=4, sort_keys=True)
            print color['']
            print

    return response, exit_code

'''
Given a list separated by |, turn into a json object
bug: ./ap_tool.py -spid 1085148,1120084 -m json
- returns 2 different json objects (not an easy fix)

'''
def make_json(my_header, my_str):

    sep = "|"
    json_list = []
    my_header_list = my_header.split(sep)


    for line in my_str.split("\n"):
        my_list = line.split("|")
        if len(my_list) > 2:
            #print my_list
            c = 0
            lil_json = {}
            for i in my_list:

                my_key = str(my_header_list[c]).lower().replace(" ", "_")
                if str(i).isdigit():
                    lil_json[my_key] = int(i)
                else:
                    lil_json[my_key] = str(i).replace('"', "")
                c += 1


            json_list.append(lil_json)
    json_str = str(json_list)
    json_str = json_str.replace("'", '"') # json.loads doesn't like fields 'enclosed' by single quote
    return json_str


'''
Convert PM notes to a single string - usually has \r or \n in it
'''
def convert_pm_notes(my_str):
    new_str = str(my_str).replace("\n", ";").replace("\r", ";")
    return new_str


'''
get pmos config options
'''
def get_pmos_config():
    pmos_config = os.path.join(os.getenv("HOME"), ".pmos.cfg")
    pmos_url = "https://proposals.jgi.doe.gov" # default for reading...
    pmos_cid = 0
    pmos_token = ""

    if os.path.isfile(pmos_config):
        fh = open(pmos_config, "r")
        for line in fh:
            if line.startswith("#"):
                continue
            else:
                arr = line.split("=")
                if len(arr) > 1:
                    key = str(arr[0]).strip().lower()
                    val = str(arr[1]).strip()

                    if key == "pmos_url":
                        pmos_url = val
                    elif key == "cid":
                        pmos_cid = val
                    elif key == "token":
                        pmos_token = val

        fh.close()

    else:
        if cmd == "update":
            print "Please create %s with:" % pmos_config
            print " cid = (pmos cid*)"
            print " token = (pmos token*)"
            print " pmos_url = %s" % pmos_url
            print "* get from PMOS/Rene"
            sys.exit(2)

    return pmos_url, pmos_cid, pmos_token


'''
Return the status string and int for a status
'''
def convert_status(my_status):
    my_status_id = 0
    my_status_str = ""

    if str(my_status).isdigit():
        my_status_id = int(my_status)
    else:
        my_status_id = valid_status_dict.get(my_status.lower())

    if str(my_status).isdigit():
        for k in valid_status_dict:
            if int(valid_status_dict[k]) == my_status_id:
                my_status_str = k.title() # needs to be title case!
    else:
        my_status_str = str(my_status)

    return my_status_id, my_status_str

'''
Stupid unicode stuff
'''
def clean_str(my_str):
    my_new_str = my_str
    if my_str:
        if type(my_str) is str or type(my_str) is unicode:
            #my_new_str = my_str.decode('utf8', 'ignore') # doesn't work
            #my_new_str = my_str.encode('string-escape')
            #my_new_str = re.sub(r'[^\x00-\x7F]+', '', my_str) # works
            my_new_str = my_str.encode("ascii", "ignore").decode("ascii") # also works

    return my_new_str

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__


if __name__ == "__main__":


    my_name = "AP Tool"
    version = "1.8.17"

    # Parse options

    usage = "%s, version %s\n" % (my_name, version)

    usage += """
AP Tool can look up or update the status for analysis projects using the PMOS api.
Valid statuses ("status name", status id):
- "created", 1
- "library creation in progress", 2
- "data available", 5
- "in progress", 6
- "automatic analysis complete", 7
- "output created and awaiting review", 8
- "complete", 9 (for analysis projects)
- "done", 9 (for analysis tasks)
- "abandoned", 10 (for analysis projects)
- "cancelled", 10 (for analysis tasks)
- "on hold", 12
- "deleted", 13
- "cancelled", 14
- "rework", 99
* the AP tool will not show projects in the "completed", "done", "abandoned", "cancelled" or "deleted" state unless the -sa (show all) parameter is used.

To look up all of the analysis projects and tasks for a sequencing project id that are not completed, deleted or abandoned:
$ ap_tool.py -spid 1040153

To look up all of the analysis projects and tasks for a sequencing project id: (show all option)
$ ap_tool.py -spid 1040153 --show-all

To look up the analysis project and task only for analysis type task id = 32 (methylation) for spid 1040153:
$ ap_tool.py -spid 1040153 -tatid 32

To look up all the analysis project and task info for spids 1040153 and 1072312 in csv format:
$ ap_tool.py -spid 1040153,1072312 --mode csv

To look up the analysis project for an analysis project id:
$ ap_tool.py -apid 1068186

To look up the sequencing project id for an analysis task id (133754)
* this uses the data warehouse and does not reflect the most recent task status updates
$ ap_tool.py -at 133754 --cmd find-spid

To find the analysis project information for an analysis task id (160205 and 110847)
$ ap_tool.py -at 160205,110847 --cmd find-ap

To show all analysis project and task ids for target analysis task id = 32 (methylation) and status = complete
$ ap_tool.py -tatid 32 --cmd list -s complete

To lookup all analysis projects & tasks for analysis task type id 47 (assembly) in status 1 (Created)
$ ap_tool.py --cmd list -tatid 47 --status 1

To lookup all analysis projects & tasks for analysis task id 70 (ITAG 16s-V4-V5) in status 1 (Created) - using data warehouse tables
$ ap_tool.py --cmd listdw -tatid 70 --status 1

-----[ Change Analysis Task Status ]-------------

To update an analysis task the known analysis task id (139421) to status id (9 = done)
$ ap_tool.py -at 139421 --cmd update --status 9
* if previous status is "created" then ap_tool will set to "in progess" (6) then to "done" (9)

To update an analysis task using the known analysis task id (110847) to "Output Created" (8):
$ ap_tool.py -c u -at 110847 -s 8

To set rework for an analysis task for an analysis task id (167104):
$ ap_tool.py -at 167104 --cmd rework
* rework closes the open analysis task id and requests a new analysis task to be created


    """

    # disallowed
    #To update the status for analysis type task id = 32 (methylation) for spid 1040153:
    #$ ap_tool.py -spid 1040153 --status "In Progress" --cmd update -tatid 32

    #To update a library using the status id (6 = in progress)
    #$ ap_tool.py -l atttz --cmd update --status 6 -tapid 2 -tatid 47



    color = get_colors()
    



    # 2, 3, 4, 7, 11 are not active: 2015-12-14 - need to be "Complete" vs "complete"
    valid_status_dict = {
        "created" : 1,
        #"library creation in progress" : 2,
        #"sequencing in progress" : 3,
        #"sdm in progress" : 4,
        "data available" : 5,
        "in progress" : 6,
        #"automatic analysis complete" : 7,
        "output created and awaiting review" : 8,
        "complete" : 9,
        "done" : 9, # new 2017-10-03
        "abandoned" : 10,
        "needs attention" : 11,
        #"on hold" : 12, removed 2016-08-18
        "deleted" : 13
    }



    parser = ArgumentParser(usage = usage)

    parser.add_argument("-spid", "--spid", dest="spid_list", type=str, help = "csv list of sequencing project ids")
    parser.add_argument("-apid", "--analysis-project-id", dest="ap_id", type=str, help = "csv list of analysis project ids")
    parser.add_argument("-l", "--lib", dest="lib_list", type=str, help = "csv list of library names")
    parser.add_argument("-c", "--cmd", dest="cmd", type=str, help = "command to run (default = info)")
    parser.add_argument("-tapid", "--target-analysis-project-type-id", dest="target_ap_type_id", type=str, help = "target analysis project type id to find for the sequence project id")
    parser.add_argument("-tatid", "--target-analysis-task-type-id", dest="target_at_type_id", type=str, help = "target analysis task type id to find for the sequence project id")
    parser.add_argument("-at", "--analysis-task-id", dest="at_id", type=str, help = "analysis task id to use to look up sequencing project id")

    parser.add_argument("-s", "--status", dest="status", type=str, help = "change the analysis task status to this value")
    parser.add_argument("-sa", "--show-all", dest="show_all", default=False, action="store_true", help = "Show completed, done, deleted and abandoned analysis tasks")
    parser.add_argument("-d", "--debug", dest="debug", default=False, action="store_true", help = "Additional output for debugging")
    parser.add_argument("-m", "--mode", dest="mode", type=str, help = "output mode (csv, tsv, psv) default = none = normal output")
    parser.add_argument("-v", "--version", action="version", version=version)


    seq_proj_id_list = [] # list of spids to look up
    ap_id_list = [] # list of analysis project ids to look up

    cmd = "info"
    target_ap_type_id = 0
    target_at_type_id = 0
    tap_list = [] # target ap list
    tat_list = [] # target at list

    at_lookup_list = []

    #id = 0 # only used for looking up the spid in data warehouse
    new_status = ""
    debug_flag = False
    show_all_flag = False # show tasks that are in Abandoned, Deleted, Complete
    mode = ""

    args = parser.parse_args()

    if args.lib_list:
        seq_proj_id_list = get_seq_proj_id(args.lib_list)

    if args.spid_list:
        seq_proj_id_list = re.split(r'[,;\s]+', args.spid_list)
        seq_proj_id_list = get_lib_list(seq_proj_id_list)

    if args.ap_id:
        ap_id_list = args.ap_id.split(",")

    if args.cmd:
        cmd = str(args.cmd).lower()
        if cmd.startswith("u"):
            cmd = "update"
        elif cmd.startswith("r"):
            cmd = "rework"
        elif cmd.startswith("c"):
            cmd = "close"

    if args.status:
        status = args.status

    if args.debug:
        debug_flag = True

    if args.show_all:
        show_all_flag = True

    if args.at_id:
        at_lookup_list = args.at_id.split(",")



    if args.target_ap_type_id:
        #target_ap_type_id = args.target_ap_type_id
        tap_list = args.target_ap_type_id.split(",")

    if args.target_at_type_id:
        #target_at_type_id = args.target_at_type_id
        tat_list = args.target_at_type_id.split(",")

    if args.status:
        new_status = args.status

    if args.mode:
        mode = args.mode


    task_name_dict = {} # task_type_id = name of the task



    pmos_url = "https://proposals.jgi.doe.gov"
    pmos_cid = 0
    pmos_token = "" # need to token to update service

    pmos_url, pmos_cid, pmos_token = get_pmos_config()


    at_list2 = []

    for at_lookup_id in at_lookup_list:

        #133754 == 1101190/1101187 (at == spid, fdid)
        if cmd == "find-spid":
            # 160205 = no spid so returns nothing (RQC-951)
            at_list_tmp = find_spid(at_lookup_id)
            if at_list_tmp:
                at_list2.append(at_list_tmp)

        if cmd in ("find-ap", "rework", "update", "info"):

            m = "silent"
            if cmd in ("find-ap", "info"):
                m = "show"

            ap_id, at_status = get_pmos_analysis_task(at_lookup_id, mode, m)

            # update per AT: RQC-951
            if cmd == "rework":
                exit_code = set_rework(ap_id, at_lookup_id, pmos_url, pmos_cid, pmos_token)
            elif cmd == "update":
                exit_code = update_ap_at_status(ap_id, at_lookup_id, at_status, new_status, pmos_url, pmos_cid, pmos_token)
                #print "exit code = %s" % exit_code
                if exit_code > 0:
                    sys.exit(exit_code)
                    
        if debug_flag:
            for s in at_list2:
                ap_id, at_id, at_status = s.split("/")
                print "%sap_id: %s, at_id: %s, at_status: %s%s" % (color['yellow'], ap_id, at_id, at_status, color[''])

        if cmd != "info":
            for s in at_list2:
                ap_id, at_id, at_status = s.split("/")

                if new_status.lower() == "rework" or new_status == "99" or cmd == "rework":
                    exit_code = set_rework(ap_id, at_id, pmos_url, pmos_cid, pmos_token)
                else:
                    exit_code = update_ap_at_status(ap_id, at_id, at_status, new_status, pmos_url, pmos_cid, pmos_token)



    # dictionary of spids and apids for looking up
    lookup_dict = {}

    cnt = 0

    for spid in seq_proj_id_list:
        cnt += 1
        sp_key = "sp_%s" % cnt

        lookup_dict[sp_key] = { "type" : "spid", "id" : spid }
        if cmd == "at":
            show_at_info(spid, target_at_type_id)

    for apid in ap_id_list:
        cnt += 1
        ap_key = "ap_%s" % cnt
        lookup_dict[ap_key] = { "type" : "apid", "id" : apid }
        # look up spid


    # take first element from the list
    target_ap_type_id = 0
    target_at_type_id = 0
    done = False # still elements in one of the 2 lists

    if len(tap_list) == 0:
        tap_list.append(0)

    if len(tat_list) == 0:
        tat_list.append(0)


    while done == False:
        if len(tap_list) > 0:
            target_ap_type_id = tap_list.pop(0)
        if len(tat_list) > 0:
            target_at_type_id = tat_list.pop(0)

        try:
            target_ap_type_id = int(target_ap_type_id)
        except:
            target_ap_type_id = 0

        try:
            target_at_type_id = int(target_at_type_id)
        except:
            target_at_type_id = 0

        if len(tap_list) + len(tat_list) > 0:
            done = False
        else:
            done = True

        if cmd == "list":
            lookup_analysis_tasks_per_status(target_at_type_id, new_status, mode)

        if cmd == "listdw":
            lookup_analysis_tasks_per_status_dw(target_at_type_id, new_status, mode)


        #for spid in seq_proj_id_list:
        for k in lookup_dict:

            spid = 0
            apid = 0
            if lookup_dict[k]['type'] == "spid":
                spid = lookup_dict[k]['id']

            if lookup_dict[k]['type'] == "apid":
                apid = lookup_dict[k]['id']
                # look up spid

            ap_id = 0
            at_id = 0
            at_status = None
            exit_code = 0

            if cmd in ("info", "update", "rework", "close"):

                if cmd == "update":
                    mode = "silent"

                # at_list = ap_id/at_id/task_status_id

                #print "show_ap_at_info(%s, %s, %s, %s, %s)" % (spid, target_ap_type_id, target_at_type_id, pmos_url, mode)
                at_list, exit_code = show_ap_at_info(spid, apid, target_ap_type_id, target_at_type_id, pmos_url, mode)


                if debug_flag:
                    for s in at_list:
                        ap_id, at_id, at_status = s.split("/")
                        print "%sap_id: %s, at_id: %s, at_status: %s%s" % (color['yellow'], ap_id, at_id, at_status, color[''])
                #    print "# ap_id: %s, at_id: %s, at_status: %s" % (ap_id, at_id, at_status)


            # Required to use analysis task id (1.8.15)
            if cmd in ("update", "rework") and exit_code == 0:
                for s in at_list:
                    print "* Error: can only update an analysis task using the analysis task id"
                    exit_code = 1

            # --cmd "close" kept for methylation releases (not documented)
            if cmd in ("close") and exit_code == 0:

                for s in at_list:
                    ap_id, at_id, at_status = s.split("/")

                    if new_status.lower() == "rework" or new_status == "99" or cmd == "rework":
                        exit_code = set_rework(ap_id, at_id, pmos_url, pmos_cid, pmos_token)
                    else:
                        exit_code = update_ap_at_status(ap_id, at_id, at_status, new_status, pmos_url, pmos_cid, pmos_token)

                    if exit_code > 0:
                        sys.exit(exit_code)


    sys.exit(0)



"""
Exit Codes
10 = failed PMOS call
11 = no ap/at for spid
12 = invalid status
13 = status the same
14 = missing analysis project id
15 = missing analysis task id
16 = missing status
17 = analysis task in terminal state already

                      .-.
         .-._    _.../   `,    _.-.
         |   `'-'    \     \_'`   |
         \            '.__,/ `\_.--,      Not dead which eternal lie
          /                '._/     |     Stranger eons death may die
         /                    '.    /
        ;   _                  _'--;      Drain you of your sanity
     '--|- (_)       __       (_) -|--'   Face the thing that should not be
     .--|-          (__)          -|--.
      .-\-                        -/-.    Metallica - The Thing That Should Not Be
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`

2016-05-05: These work
Vasanth's test - multiple RNA expressions across multiple project ids
./ap_tool.py -spid 1074278 -tatid 12

./ap_tool.py -l atttz --cmd update --status 6 -tapid 2 -tatid 47
./ap_tool.py -d -spid 1085148 -tatid 47 --cmd update --status 6

update multiple at once
./ap_tool.py --lib baghs -tatid 39,47 -tapid 37,2  --cmd update --status 6
Updating analysis project/task id 1069592/110849 to 'In Progress' (6)
Updating analysis project/task id 1069591/110847 to 'In Progress' (6)
./ap_tool.py --lib baghs -tatid 39,47 -tapid 37,2  --cmd update --status 8


2017-01-24: getting filtered task id (should only work on itags)
./ap_tool.py -l BNONN -sa
filtering pipeline example - expect both to return the same output
./ap_tool.py -l BGOXP -m psv
./ap_tool.py -l BGOXP -m psv -tatid 72

itag filtering:
./ap_tool.py -l botwc -sa -tatid 72
- itag 3 tasks for same id (PMOS error)
./ap_tool.py -l BOHUH -sa -tatid 72
"""