#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
	Some JIRA contains a large number of SPIDs that uses the same previously uploaded reference,
	e.g. https://issues.jgi-psf.org/browse/SEQQC-7604. This script is to simplify the process.

	To use, privide the genome and/or transcriptome ids in seq_locations table, and list of SPIDs.

	2/10/2016
	Version 1.0.0
	Shijie Yao

	Usage : (can be run by any user, as the action is to insert new entries into database)
	1) work on test db server (draw):
		a) test run: jgi-rqc-web/utils/bulk_assign_reference.py -i spid.list -g 2156 -t 2157 -test
		b) real run: jgi-rqc-web/utils/bulk_assign_reference.py -i spid.list -g 2156 -t 2157
	2) use production
		a) test run: jgi-rqc-web/utils/bulk_assign_reference.py -i spid.list -g 2156 -t 2157 -l -test
		b) real run: jgi-rqc-web/utils/bulk_assign_reference.py -i spid.list -g 2156 -t 2157 -l
"""

import sys
import os
import re
import shutil
from argparse import ArgumentParser
import MySQLdb

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
	sys.path.append(LIB_DIR)

from db_access import db_connect

#========================================
# utility functions
def get_db(prod=False, dbname='seq_location_repos'):
	dbserver ='draw.jgi-psf.org'
	dbuser = 'rqc_dev'
	dbpwd = 'b1Jpb24uVGhyeWxsLnJhdDo5OQ=='

	if prod:
		dbserver ='scidb1.nersc.gov'
		dbuser = 'rqc_prod'
		dbpwd = 'dHJvd3QudGFYZXMub0Jsb25nLjIyMzs='

	print('\n Connecting to %s@%s ...' % (dbname, dbserver))
	db = db_connect(db_server=dbserver, db_user=dbuser, db_name=dbname, db_pwd=dbpwd)

	sth = None
	if db:
		sth = db.cursor(MySQLdb.cursors.DictCursor)
	else:
		print('  -- data base connection failed!')
	return sth

def error_exit(msg):
	print('\n *** ERROR: %s\n' % msg)
	exit(1)
#=====================================
def load_spid_from_file(fname):
	alist = []
	with open(fname, 'r') as FH:
		for line in FH:
			alist += line.strip().split()

	return alist

def validate_spids(alist, live):
	sql = '''
		SELECT seq_proj_id FROM library_info WHERE seq_proj_id IN (%s)
	'''
	sql = sql % ','.join(alist)

	print(sql)
	if live:
		sth = get_db(live, 'rqc')
	else:
		sth = get_db(live, 'rqc_dump')

	sth.execute(sql)
	rows = sth.fetchall()

	dlist = []
	for row in rows:
		print(row)
		dlist.append(row['seq_proj_id'])

	ilist = [int(x) for x in alist]

	print(dlist)
	return True

def get_ref_paths(sth, aid, tid):
	if not aid:
		return None

	sql = '''
		SELECT id, bio_name, ori_location, current_location, type_id FROM seq_locations WHERE id = %s and type_id = %d
	''' % (aid, tid)
	#print('sql=%s'%sql)
	sth.execute(sql)
	row = sth.fetchone()
	return row

def insert_to_db(sth, spid, ref, bioname, live=False):
	sql = 'INSERT INTO seq_locations (bio_name,ori_location,current_location,type_id,comments,seq_proj_id) VALUES (%s, %s, %s, %s, %s, %s)'
	if not bioname:
		bioname = ref['bio_name']
	param = (bioname, ref['ori_location'], ref['current_location'], ref['type_id'], 'Copied from id=%s by qc_user' % ref['id'], spid)
	if not live:
		print('DRY RUN: %s\n' % (sql % param) )
	else:
		print('%s\n' % (sql % param) )
		sth.execute(sql, param)


def add_records(idlist, sth, gref, tref, bioname, live=False):
	gcnt = 0
	tcnt = 0
	for aid in idlist:
		if gref:
			insert_to_db(sth, aid, gref, bioname, live)
			gcnt += 1
		if tref:
			insert_to_db(sth, aid, tref, bioname, live)
			tcnt += 1

	print('')
	if live:
		print('Records inserted into the selected database : ')
	else:
		print('Records can be inserted into the selected database : ')
	print('            genome : %d' % gcnt)
	print(' transcriptome ref : %s' % tcnt)

	if not live:
		print('')
		print('YOU CAN RERUN THE COMMAND, WITH THE ADDITIONAL -l OPTION, TO MAKE A LIVE RUN.')
		print('')



#=====================================
# the main
if __name__ == '__main__':
	NAME = 'Bulk Reference Assign'
	VERSION = '1.0.0'

	USAGE = "prog [options]\n"
	USAGE += "* %s, version %s\n" % (NAME, VERSION)

	JOB_CMD = ' '.join(sys.argv)
	DBTYPE = ['prod', 'dev']
	PARSER = ArgumentParser(usage=USAGE)
	PARSER.add_argument("-i", "--spid", dest="spid", help="The sequencing project id the references will apply to. Can be a file of SPIDs, or a cvs value", required=True)
	PARSER.add_argument("-n", "--bioname", dest="bioname", help="The bioname to use. If not provided, the bioname from genome/trans will be used.")
	PARSER.add_argument("-g", "--genome", dest="genome_id", help="The genome id in seq_locations table")
	PARSER.add_argument("-d", "--db", dest="dbtype", choices=DBTYPE, help="database type", required=True)
	PARSER.add_argument("-t", "--trans", dest="trans_id", help="The transcriptome id in seq_locations table")
	PARSER.add_argument("-l", "--live", dest="live", default=False, action="store_true", help="Run against production server")
	PARSER.add_argument("-v", "--version", action="version", version=VERSION)

	ARGS = PARSER.parse_args()

	STH = get_db(ARGS.dbtype.strip() == 'prod')
	if not STH:
		error_exit('failed to connect to db')

	if not ARGS.genome_id and not ARGS.trans_id:
		error_exit('should have either genome id [-g] or transcriptome id [-t] or both')

	spids = []
	if os.path.isfile(ARGS.spid):
		spids = load_spid_from_file(ARGS.spid)
	else:
		spids = ARGS.spid.split(',')

	if not spids:
		error_exit('no SPIDs given in command line')
	#else:
	#	spids = [int(x) for x in spids]

	#ok = validate_spids(spids, ARGS.live)
	TYPE_ID_G = 16	# genome type id
	TYPE_ID_T = 18	# transcriptome type id

	print('')

	print('%20s : %s' % ("command", JOB_CMD))
	print('%20s : %s' % ('current dir', os.path.abspath('.')))
	print('%20s : %s' % ("db type", str(ARGS.dbtype)))
	print('%20s : %s' % ("dry run", str(not ARGS.live)))
	print('%20s : %s' % ("spid list file", ARGS.spid))
	if ARGS.bioname:
		bninfo = ARGS.bioname
	else:
		bninfo = ' >>>> Not provided by user. Will use the value associated with the given reference record in database.'
	print('%20s : %s' % ("bioname", bninfo))
	print('%20s : %s' % ("genome id", ARGS.genome_id))
	print('%20s : %s' % ("transcriptome id", ARGS.trans_id))
	print('')

	gref = None
	tref = None
	if not ARGS.genome_id and not ARGS.trans_id:
		error_exit('require at least of reference ID (either genome [-g] and/or transcriptome [-t])')

	if ARGS.genome_id:
		gref = get_ref_paths(STH, ARGS.genome_id, TYPE_ID_G)
	if ARGS.trans_id:
		tref = get_ref_paths(STH, ARGS.trans_id, TYPE_ID_T)

	if not gref and not tref:
		if not gref:
			error_exit('no genome record for id=%s' % ARGS.genome_id)
		else:
			error_exit('no transcriptome record for id=%s' % ARGS.trans_id)

	justdoit = raw_input('Do you want to continue [y|n] ? ')
	justdoit = justdoit.strip().lower()
	if justdoit not in ('yes', 'no', 'y', 'n'):
		error_exit('Don not understand your awnser: [%s]' % justdoit)

	if justdoit.startswith('y'):
		add_records(spids, STH, gref, tref, ARGS.bioname, live=ARGS.live)
	else:
		print('Thanks for trying. Bye!\n')
