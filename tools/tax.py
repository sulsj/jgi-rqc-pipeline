#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
TAX
- update tax_* fields in library_info based on gold_tax_id (or ncbi_tax_id)

- use gold_tax_id over ncbi_tax_id if it is available
- outputs as sql statements
- part of the rqc_system/sh/daily_update.sh

1.2 - 2018-07-12
- added ncbi_organism != null to query

1.2.1  - 2018-07-20
- standardized with lc07.py & gold.py

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
import MySQLdb

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

## for conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)
    
from taxonomy import get_taxonomy
from db_access import jgi_connect_db

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "TAX"
    version = "1.2.1"

    tax_fields = {
        "tax_kingdom" : "superkingdom",
        "tax_phylum" : "phylum",
        "tax_class" : "class",
        "tax_order" : "order",
        "tax_family" : "family",
        "tax_genus" : "genus",
        "tax_species" : "species"
    }

    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)

    # look for missing taxonomies
    sql = """
    select l.ncbi_organism_name, l.ncbi_tax_id, l.library_id, l.library_name, l.gold_tax_id, l.gold_organism_name
from library_info l
where
    (l.tax_kingdom is null or l.tax_kingdom = '-')
    and (l.ncbi_tax_id > 0 or l.gold_tax_id > 0)
    and (l.ncbi_organism_name != 'synthetic construct' or l.ncbi_organism_name is null)
    and l.ncbi_tax_id != 131567
    and l.dt_created > date_sub(now(), interval 6 month)
    """
    
        
    # 131567 = no template control (nothing)

    # to sync gold taxonomies
    # update library_info set tax_kingdom = null where gold_tax_id > 0 and gold_tax_id != ncbi_tax_id
    sql2 = """
select l.ncbi_organism_name, l.ncbi_tax_id, l.library_id, l.library_name, l.gold_tax_id, l.gold_organism_name
from library_info l
where
    gold_tax_id > 0
    and gold_tax_id != ncbi_tax_id
    and l.tax_kingdom is null
    """



    #sql += " limit 0,100"


    sth.execute(sql)
    row_cnt = int(sth.rowcount)
    ucnt = 0
    for _ in range(row_cnt):
        rs = sth.fetchone()
        tax_id = rs['ncbi_tax_id']
        if rs['gold_tax_id'] > 0:
            tax_id = rs['gold_tax_id']

        sys.stderr.write("-- %s: ncbi = %s (%s), gold = %s (%s)\n" % (rs['library_name'], rs['ncbi_tax_id'], rs['ncbi_organism_name'], rs['gold_tax_id'], rs['gold_organism_name']))

        td = get_taxonomy(tax_id, "id")
        if td.get("error", "") == "Not found.":
            td = get_taxonomy(rs['ncbi_tax_id'], "id")

        if td.get("error", "") == "Not found.":
            td = get_taxonomy(rs['ncbi_organism_name'], "name")

        if td.get("error", "") == "Not found.":
            sys.stderr.write("-- nothing found on tax server\n")
            continue

        #print td

        sql_list = []
        tax_full = ""
        for t in tax_fields:

            tax_key = tax_fields[t]
            if tax_key in td:
                if td[tax_key] == "unidentified":
                    td[tax_key] = "-"
                tax_full += td[tax_key]
                td[tax_key] = str(td[tax_key]).replace("'", "\\'")
                sql_list.append("%s = '%s'" % (t, td[tax_key]))


        #print sql_list
        if len(sql_list) > 0:
            if tax_full == "-------":
                sys.stderr.write("-- taxonomy is empty\n")
            else:
                sql = "update library_info set "
                sql += ", ".join(sql_list)
                sql += " where library_id = %s;" % rs['library_id']
                print sql
                ucnt += 1
        else:
            sys.stderr.write("-- nothing to update\n")

    sys.stderr.write("-- %s records to update / %s total\n" % (ucnt, row_cnt))
    db.close()

    sys.exit(0)

