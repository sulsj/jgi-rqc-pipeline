#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
create_sketch_db_filtered.py

Created: Sep 26 2017
sulsj (ssul@lbl.gov)


Description
  Create sketch db for each filtered fastq
  Create combined sketch db per program (Plant, Microbial-iso, ce, sps, Microbial-sag, Fungal, Metagenome)

Steps
  1. Collect target filtered fastq

  2. Create sketch db per each filtered fastq and save it to /global/dna/shared/rqc/sketch/filter

    ex)  RunID=12345 ==> ROOT_DIR/01/23/45/SEQ_UNIT_NAME_PREFIX.sketch
    ex) runid = 11198, file name = 11198.2.195851.TCGGTTA-GTAACCG.filter-RNA.fastq.gz
        /global/dna/shared/rqc/sketch/filter/plant/01/11/98/11198.2.195851.TCGGTTA-GTAACCG.filter-RNA.sketch

  3. Concatenate the created sketch db to the combined db

    ex) /global/dna/shared/rqc/sketch/filter/jgi.rqc.plant.sketch


Parallel processing by TFMQ

    1. Create tfmq task list

       ex) module load bbtools; sketch.sh in=/global/dna/dm_archive/rqc/analyses/AUTO-39039/11198.2.195851.CACCTTA-GTAAGGT.filt
        er-RNA.fastq.gz out=/global/projectb/scratch/sulsj/2017.09.26-create-sketch-db-test/filter/out/plant/01/11/98/11198.
        2.195851.CACCTTA-GTAAGGT.filter-RNA.sketch ow=t taxid=4558 spid=11198 name=Sorghum_bicolor meta_sequencername=HiSeq-
        2500_1TB meta_progname=Plant meta_libname=BOCNO meta_jatkey=AUTO-39039 meta_libcreatedate=2016-12-14 meta_sketchdate
        =2017-10-05; mkdir -p /global/projectb/scratch/sulsj/2017.09.26-create-sketch-db-test/filter/out/collections; touch
        /global/projectb/scratch/sulsj/2017.09.26-create-sketch-db-test/filter/out/collections/jgi.rqc.plant_transcriptome_c
        ounting-only.sketch.$SGE_TASK_ID; cat /global/projectb/scratch/sulsj/2017.09.26-create-sketch-db-test/filter/out/pla
        nt/01/11/98/11198.2.195851.CACCTTA-GTAAGGT.filter-RNA.sketch >> /global/projectb/scratch/sulsj/2017.09.26-create-ske
        tch-db-test/filter/out/collections/jgi.rqc.plant_transcriptome_counting-only.sketch.$SGE_TASK_ID::0

    2. Run tfmq client

       ex) tfmq-client -i <tfmq_task_list_file> -q filter_sketch

    3. Run tfmq workers

       ex) worker.q
       #!/bin/sh

        #$ -S /bin/bash
        #$ -V
        #$ -cwd
        #$ -notify
        ##$ -P prok-IMG.p
        #$ -j y -o worker.$TASK_ID.log
        #$ -l normal.c
        #$ -N tfmq_sketch
        #$ -l h_rt=03:00:00
        #$ -l ram.c=5.25G
        #$ -pe pe_slots 8

        module load python
        module load taskfarmermq
        tfmq-worker -q filter_sketch

        * qsub array job (Note: must request nodes with array job b/c SGE_TASK_ID is used to create collection file)

        ex) qsub -t 1-60 worker.q

    4. Concatenate the created sketch db to the combined db

    find ./out/fungal -type f -name "*.sketch" -print -exec cat {} \; > ./out/collections/jgi.rqc.fungal.sketch
    find ./out/microbial -type f -name "*.sketch" -print -exec cat {} \; > ./out/collections/jgi.rqc.microbial.sketch
    find ./out/metagenome -type f -name "*.sketch" -print -exec cat {} \; > ./out/collections/jgi.rqc.metagenome.sketch
    find ./out/genomic_tech -type f -name "*.sketch" -print -exec cat {} \; > ./out/collections/jgi.rqc.genomic_tech.sketch

    ex) /global/dna/shared/rqc/sketch/filter/collections/jgi.rqc.plant.sketch
        /global/dna/shared/rqc/sketch/filter/collections/jgi.rqc.microbial.iso.sketch


    5. Copy sketch files to
    /global/dna/shared/rqc/sketch/filter
    /global/dna/shared/rqc/sketch/filter/collections


Performance
    * Assembly
    Sketching ~600 assemblies (microbial_minimal_draft_isolate) took ~14min with 16 cores
    Combined DB size ~50MB


    * Filtering
    10078 filtered fastqs (07.15.2017 ~ now)
    Sketching with 20~40 workers ==> ~3hr

    500 fastqs with 3 workers using 1 core each => 35min
    500 fastqs with 3 workers using 8 cores each => 29min
    500 fastqs with 3 workers using 16 cores each => 29min
    500 fastqs with 3 workers using 32 cores each => 29min

    Est. run time for 26746 fastqs with 3 workers using 8 cores ==> 26hr
    Est. run time for 26746 fastqs with 30 workers using 8 cores ==> 2.6hr
    Est. run time for 26746 fastqs with 60 workers using 8 cores ==> 1.3hr


Revisions:
    10.04.2017 0.0.9: init
    10.16.2017 1.0.0: Production mode

"""

import os
import sys
import argparse
import datetime

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from common import get_logger
from rqc_utility import safe_basename, safe_dirname, pad_string_path
from os_utility import run_sh_command
from db_access import jgi_connect_db

## Globals
VERSION = "0.0.9"
LOG_LEVEL = "DEBUG"
# LOG_LEVEL = "INFO"
SCRIPT_NAME = __file__

## production
# TARGET_SKETCH_DIR = "/global/dna/shared/rqc/sketch/filter"
# TARGET_COLLECTION_DIR = "/global/dna/shared/rqc/sketch//filter/collections"
## test
TARGET_SKETCH_DIR = "/global/projectb/scratch/sulsj/2017.09.26-create-sketch-db-test/filter/out"
TARGET_COLLECTION_DIR = "/global/projectb/scratch/sulsj/2017.09.26-create-sketch-db-test/filter/out/collections"


##==============================================================================
if __name__ == '__main__':
    desc = "Create sketch DB"
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-i", "--input", dest="inputFile", help="Input file", required=True)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Target path to write output files to", required=False)
    parser.add_argument("-ow", "--over-write", action="store_true", help="Recreate all outputs", dest="overWrite", default=False, required=False)
    parser.add_argument("-f", "--output-file", dest="outputFile", help="Output TFMQ file", required=True)
    parser.add_argument("-v", "--version", action="version", version=VERSION)

    options = parser.parse_args()

    inputFile = options.inputFile
    outputFile = options.outputFile
    overWrite = False
    collectionCleared = False ## to remove collection file and recreate it

    if options.outputPath:
        outputPath = options.outputPath
    else:
        outputPath = os.getcwd()

    if options.overWrite:
        overWrite = True

    if not os.path.isdir(outputPath):
        os.makedirs(outputPath)

    logFile = os.path.join(outputPath, "create_sketch_db.log")
    log = get_logger("create_sketch_db", logFile, LOG_LEVEL, True, True)

    ## get program name
    db = jgi_connect_db("rqc")

    if db is None:
        log.error("Cannot open database connection!")
        sys.exit(4)


    with open(inputFile, 'r') as lf:
        cnt = 0
        with open(outputFile, 'w') as of:
            for l in lf:
                t = l.rstrip().split('\t')
                filteredFile = os.path.join(t[0], t[1])
                libName = t[2]
                runId = t[3]
                jatKey = t[4]
                sequencerName = t[5].replace(" ", "_")
                progName = t[6].replace(" ", "_")
                orgaName = t[7].replace(" ", "_") if t[7] != '-' else "NA"

                try:
                    if t[9] and t[9] != '-' and int(t[9]) > 0:
                        taxonId = t[9]
                    elif t[8] and t[8] != '-' and int(t[8]) > 0:
                        taxonId = t[8]
                    else:
                        taxonId = 0
                except ValueError:
                    print "Invalid data: %s" % l
                    print "Will ignore the data."

                prodName = t[10].replace(" ", "_") if t[10] else "etc"
                # libCreateDate = t[11].replace(" ", "_") if t[11] else "NA"
                libCreateDate = t[11].split()[0] if (t[11] is not None and t[11] != "NULL") else "NA"
                seqProjId = t[12] if t[12] != "NULL" else "0"

                if os.path.isfile(filteredFile):
                    ################################################################
                    ## Run sketch
                    ##
                    ## dest. dir name: run id  11419 = 00 01 14 19 = 01/10/54/1105419.sketch
                    indSketchFile = os.path.join(TARGET_SKETCH_DIR, progName.lower(), pad_string_path(runId, padLength=6), t[1].replace(".fastq", "").replace(".gz", "") + ".sketch")

                    ## if sketch file has already been created, skip!
                    if not overWrite and os.path.isfile(indSketchFile):
                        continue

                    sketchCmd = "module load bbtools; sketch.sh in=%s out=%s ow=t taxid=%s spid=%s name=%s meta_sequencername=%s meta_progname=%s meta_libname=%s meta_jatkey=%s meta_libcreatedate=%s meta_sketchdate=%s meta_runid=%s" % \
                                (filteredFile, indSketchFile, taxonId, seqProjId, orgaName,
                                 sequencerName, progName, libName, jatKey, libCreateDate, str(datetime.datetime.now().strftime("%Y-%m-%d")), runId)

                    # log.info("sketchCmd = %s", sketchCmd)
                    # print sketchCmd


                    ################################################################
                    ## Create collection sketch
                    ##
                    # prodName = '_'.join(prodName.replace(',', '').split())
                    # comSketchFile = os.path.join(TARGET_COLLECTION_DIR, "jgi.rqc." + prodName.lower() + ".sketch.$SGE_TASK_ID")
                    #
                    # # if overWrite and not collectionCleared and os.path.isfile(comSketchFile):
                    # #     collectionCleared = True
                    # #     os.remove(comSketchFile)
                    #
                    # # touchCmd = None
                    # # if not os.path.isfile(os.path.join(TARGET_COLLECTION_DIR, comSketchFile)):
                    # touchCmd = "mkdir -p %s; touch %s" % (TARGET_COLLECTION_DIR, os.path.join(TARGET_COLLECTION_DIR, comSketchFile))
                    # # log.info("touchCmd = %s", touchCmd)
                    # # _, _, exitCode = run_sh_command(touchCmd, True, log)
                    # # assert exitCode == 0
                    #
                    # concatCmd = "%s; cat %s >> %s" % (touchCmd, indSketchFile, comSketchFile)
                    # # log.info("concatCmd = %s", concatCmd)
                    # # print concatCmd
                    #
                    # # stdOut, stdErr, exitCode = run_sh_command(concatCmd, True, log)
                    # # if exitCode != 0:
                    # #     log.error("concatCmd failed")
                    # #     sys.exit(-1)

                    # of.write("%s; %s::0\n" % (sketchCmd, concatCmd))
                    of.write("%s::0\n" % (sketchCmd))

                    cnt += 1

    log.info("%s sketch tasks created!", cnt)

    db.close()

## EOF
