#!/usr/bin/env python
'''
    A Python script to perform bulk rework qc submittion.
    A input file with the multiple submission entries are needed.

    Author: Shijie Yao
    Update Date  : Jan 01, 2015 - initial implementation
    Update Date  : Jan 26, 2015 - add save function
    Update Date  : Mar 02, 2015 - match the backend change that unified the pacbio and illumna sow qc process
    Update Date  : Jun 26, 2015 - sow status const updated to ['Needs Lab Work', 'Complete', 'Needs PMO Attention']
    Update Date  : Arg 06, 2015 - make it be able to use jgi-rqc-pipeilne/lib or jgi-rqc/rqc_system/lib
    Update Date  : Jan 29, 2016 - enforce TLA/CLA limits
    Update Date  : Arg 25, 2016 - replace ' with unicode code in json string (when it appears in comments)
    Update Date  : Apr 03, 2017 - add insert_size, insert_size_std and insert_size_method to reworkqc submission
    Update Date  : Apr 28, 2017 - get MF cv from WS call to seqqc.py; support multiple failure mode
    Update Date  : Sep 18, 2017 - update MF cv (RQCSUPPORT-1512)
    Update Date  : Nov 09, 2017 - comment cleanup : remove leading AND trailing quotes ONLY when they both exist
    Update Date : 2017-11-30 - fixed database connection to use jgi_connect_db
'''

import sys
import os
import datetime
import time
import argparse
import re
import json

import urllib

import MySQLdb
import pprint
pp = pprint.PrettyPrinter(indent=4)

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR

## conda
ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

LOG_FILE = None

if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from color import color_str

import os_utility as util
if 'run_sh_command' in dir(util):
    run_command = util.run_sh_command
elif 'run_command' in dir(util):
    run_command = util.run_command


from db_access import jgi_connect_db


DEVELOP = False

# https://docs.google.com/document/d/1O4XNSSLAOH2k9eyKKzZ-Vwg5XeVDZTT-wW7Oi6pj-eQ/edit
# x of 300 may be questionable
# set reads per barcode value per RQCSUPPORT-666 20160302
# x and reads per increated vir Bryce's email (4/4/2016)
LOGICAL_AMOUNT_LIMITS =  {
    'x': 150000,
    'm mappable reads': 500,
    'm reads' : 500,
    'reads per barcode': 5000000,
    'gb': 1000
}

##
## - global constants
##

#- param to ws mapping
TO_ITS_WS_MAP = {
    'targeted_amount' : 'targeted-logical-amount',
    'logical_amount' : 'total-logical-amount-completed',

    'abandon_sample' : 'abandon-sample',
    'abandon_library' : 'abandon-library',

    'estimated_genome_size' : 'revised-genome-size-mb',
    'genome_size_method' : 'genome-size-estimation-method',
    'complete_sow' : 'sow-item-complete',
}

#- required param
PARAM_REQUIRED = {
    'analyst' : '',         # STR : "FIRST_NAME LAST_NAME" in rqc_analyst table; needed to find analyst_id

    'seq_unit_name' : '',   # STR

    'targeted_amount' : '', # FLOAT
    'logical_amount' : '',  # FLOAT

    'lab_action' : 'No Lab Action',             # for PASS

    'abandon_library' : 'N',    # has default value, config file can omit this param for using default
    'abandon_sample' : 'N',     # has default value, config file can omit this param for using default

    'platform': 'Illumina',     # default to
    'library_name': '',
    'useable': '',
}

# optional cgi param
PARAM_OPTIONAL = {
    'measured_insert_size' : '',
    'measured_insert_size_std' : '',
    'insert_size_method': '',

    'estimated_genome_size' : '',
    'genome_size_method' : '',

    'fm_where' : '',
    'fm_what' : '',

    'sow-item-complete' : '',

    'comments' : '',

    'email_name' : '',
}

LAB_ACTION_MAP = {
    '0' : 'No Lab Action',
    '1' : 'Make New Library',
    '2' : 'qPCR Existing Library',
    '3' : 'Sequence Existing Library',
    '4' : 'Use Existing Library in New Pool'
}

SOW_STATUS_MAP = {
    '0' : 'Needs Lab Work',
    '1' : 'Complete',
    '2' : 'Needs PMO Attention'
}

# all param
ALL_PARAM = dict(PARAM_REQUIRED.items() + PARAM_OPTIONAL.items())

QUOTE = '\'\"'

WEB_HOST = 'https://rqc.jgi-psf.org'
WEB_HOST_DEV = 'https://rqc-4.jgi-psf.org'
WEB_API = 'api/sow_qc/submit'  # default to production
ACTION_FAIL = 0
ACTION_OK = 1
ACTION_EXIT = 2

'''
retrive FM definitions from the centralized code of seqqc.py
1) failure_major_code
2) failure_mode_illumina
'''
def failure_mode_data(api):
    cmd = 'curl %s/api/seqqc/%s' % (WEB_HOST, api)
    err = ''
    if DEVELOP:
        write_log("RUN : %s" % cmd)
    std_out, std_err, exit_code = run_command(cmd, True)

    if exit_code == 0:
        jd = json.loads(std_out)
        if 'errors' in jd:
            err = 'failed : %s [%s]' % (cmd, ';'.join(jd['errors']))
        else:
            if api == 'failure_major_code':
                fmdat = {}  # change keys into int
                for k in jd:
                    fmdat[int(k)] = jd[k]
                jd = fmdat
            return jd
    else:
        err = 'failed : %s [%s][%s]' % (cmd, std_out.strip(), std_err.strip())

    if err:
        print_error(err)


def print_template():
    'Print out the input file template for a given action type.'

    ff_code_txt = ''
    for fm in sorted(FM_MAJOR_CODE): # sorted keys
        ff_code_txt += '# %d : %s\n' % (fm, FM_MAJOR_CODE[fm])
        for idx, ffm in enumerate(FM_DATA[FM_MAJOR_CODE[fm]]):
            ff_code_txt += '#                   %d : %s\n' % (idx, FM_DATA[FM_MAJOR_CODE[fm]][idx])

    lab_action_txt = ''
    for la in sorted(LAB_ACTION_MAP): # sorted keys
        lab_action_txt += '# %s : %s\n' % (str(la), LAB_ACTION_MAP[la])

    sow_txt = ''
    for sowid in sorted(SOW_STATUS_MAP): # sorted keys
        sow_txt += '# %s : %s\n' % (str(sowid), SOW_STATUS_MAP[sowid])

    #  SEQ_UNIT_NAME\tACTION\tTLA\tCLA\tCOMMENTS
    #  ----------------------------    -------   ------   ------   ------    ------    -------------------------------------------------------
    #  7341.7.68152.GCCAAT.fastq.gz    UPDATE  4.0     1.17
    FORMAT = 'SU_NAME      TLA    CLA  ABAN_LIB  ABAN_SAM   LAB_ACT  USEABLE   SOW  FM_M  FM_m   COMMENTS'

    SAMPLE = '   '.join(['XYZ.fastq.gz',
                          '-4.0',
                          '-3.5',
                          '      N',
                          '      N',
                          '     -1',
                          '    -1',
                          ' -1',
                          ' -1',
                          ' -1',
                          'Note: YOUR QC COMMENTS'
                          ])

    text = '''
## ===================================================================
## This is the script produced template rework QC tamplate.
## create time : %s
''' % (time_now_str())

    text +='''
## ===================================================================
#  Comments:
#  - WRITE YOUR COMMENTS OF THIS FILE HERE -
#

## ===================================================================
#  meta data (MANDATORY): - Change the name to your name as recorded in
#  RQC in FIRST_NAME LAST_NAME format
Analyst=Barack Obama

## ===================================================================
# Your email name (without @lbl.gov) to receive failure notification email
email=

## ===================================================================
#  QC data (MANDATORY):
#  Use \\n in comments for new line
#  seq_unit_name must be in *.fastq.gz format

# Lab Action Code Map:
''' + lab_action_txt + '''
#
# Data Uesable Code Map:
# 0 : Not Useable        - inform jamo to NOT publish this seq unit through portal
# 1 : Uesable            - inform jamo to publish this seq unit through portal

# SOW Status Code Map:
''' + sow_txt + '''
#
# Failure Mode Code Map
# (where)           (what)
# FM_M              FM_m
# ----              ----
''' + ff_code_txt + '''
#
# Use FM_M=100 for multiple failure modes, and then
# FM_m=M:m,M:m where M is FM_M and m is FM_m ex : 0:2,3:5 (Library Group:low read quality AND Sequencing:mix up)

# To Reschedule Pool, you can use this tool to save (using -t save) ALL pool members to RQC.
# Your input must have CLA, TLA and USEABLE set for each member.
# After the save, you can run the web app (https://rqc.jgi-psf.org/sow_qc/page/LIBNAME) on one of the member, and
#   choose Reschedule Pool for your QC submission.

#
# When USEABLE=1, FM_M and FM_m can be ignored with values of -1;
# But USEABLE=0 requires valid FM_M and FM_m code
#
# ABANDON_LIB=Y MUST be paired with LAB_ACTION=0 or LAB_ACTION=1
# ABANDON_SAMPLE=Y MUST be paired with LAB_ACTION=0, and SOW=2

'''

    text = "%s#\n#  %s\n%s" % (text, FORMAT, SAMPLE)

    print(text)


#
# sanity check helper function
# param is a dict of current submission params
# required is a dict of required params
#
def missing_required_param(param, required):
    'Return key that missing corrresponding value.'
    for key in required.keys():
        if not required[key] and key not in param:
            return key


# helper DB function to check if a given sun has been QCed (Completed) befor
def check_qc_status(sth, sun):
    'Return boolean value to indictate if the given segment id is in segment_qc table.'

    # check rework QC illumina table:
    sql = 'select Q.status_id from sow_qc Q inner join seq_units S on Q.seq_unit_id=S.seq_unit_id where S.seq_unit_name=%s'
    sth.execute(sql, (sun, ))
    data = sth.fetchone()

    if not data:
        # check segment QC table:
        sql = 'select Q.status_id from segment_qc Q inner join seq_units S on Q.segment_id=S.segment_id where S.seq_unit_name=%s'
        sth.execute(sql,  (sun, ))
        data = sth.fetchone()

    if data:
        if int(data['status_id']) == 3:
            return True
        else:
            return False
    else:
        return False

def platform_name(sth, sun):
    sql = 'select platform_name from seq_units where seq_unit_name=%s'
    sth.execute(sql, (sun, ))
    data = sth.fetchone()
    if data:
        return data.get('platform_name')
    else:
        return None

def library_name(sth, sun):
    sql = 'select library_name from seq_units where seq_unit_name=%s'
    sth.execute(sql, (sun, ))
    data = sth.fetchone()
    if data:
        return data.get('library_name')
    else:
        return None

# return the analyst name, if found in the give line
def get_analyst(line):
    'Return the analyst name string encoded in the input file Analyst line. Return None if not found in line.'
    import re
    match = re.match('analyst=(.*)', line, re.IGNORECASE)
    if match:
        return match.group(1).strip()
    else:
        return None

# helper DB function to check if a given data has been QCed (Completed) befor
def get_analyst_id(sth, name):
    'Return the RQC analyst_id in rqc_analyst table for the given analyst name in FIRSTNAME LASTNAME format'
    if not name:
        return False

    names = name.split()
    if len(names) < 2:
        return False

    first_name = names[0]                   # first name is the 1st word: rqc_analyst.first_name all are single word
    last_name = ' '.join(names[1:len(names)])  # last name is the rest word(s): rqc_analyst.last_name can be multiple words

    sql = 'SELECT analyst_id FROM rqc_analyst WHERE first_name=\'%s\' AND last_name = \'%s\'' % (first_name, last_name)
    sth.execute(sql)
    data = sth.fetchone()

    if data:
        return int(data['analyst_id'])
    else:
        return None

#helper for logging
def write_log(msg, mtype=None, logfile=None):
    'print and log message'
    global LOG_FILE

    if logfile:
        LOG_FILE = logfile
    elif not LOG_FILE:
        LOG_FILE = os.path.basename(__file__)
        LOG_FILE = LOG_FILE[:len(LOG_FILE) - 3] + '.log'
    'Print a message to log file as well as stdout.'
    fout = None
    if LOG_FILE:
        fout = open(LOG_FILE, 'a') # append to

    if mtype == ACTION_FAIL:
        msg = '   ??? --> %s' % msg
    elif mtype == ACTION_OK:
        msg = '       ==> %s' % msg
    elif msg.startswith('Fatal'):
        msg = '\n%s\n' % msg

    print(msg)
    if fout:
        fout.write(msg + "\n")
        fout.close()

    if mtype == ACTION_EXIT:
        exit(1)

def clean_ws_param(param):
    del param['abandon_library']
    del param['abandon_sample']
    del param['library_name']
    del param['logical_amount']
    del param['targeted_amount']
    del param['seq_unit_name']
    del param['useable']
    if 'fm_what' in param:
        del param['fm_what']
    if 'fm_where' in param:
        del param['fm_where']
    del param['comments']
    if 'sow-item-complete' in param:
        del param['sow-item-complete']

    del param['insert_size_method']
    del param['measured_insert_size']
    del param['measured_insert_size_std']

def error_in_ws_call(data):
    try:
        rtn = json.loads(data)
        if 'errors' in rtn:
            return rtn.get('errors')
        else:
            return None
    except:
        return data

def load_config_to_param(config, param):
    for key in config.keys():   # check each param of the input line
        if key in param.keys() and config[key] != None and config[key] != '':
            param[key] = config[key]

def rm_null_values(param):
    # remove empty value records; can use param = dict((k, v) for k, v in param.iteritems() if v)
    for key in param.keys():
        #print('\nDEBUG [%s][%s]' % (key, param[key]))
        if param[key] == None or param[key] == '':
            del param[key]

#dataUseable [0, 1]
def make_sunits_list(param):
    # construct ws json data structure
    #pp.pprint(param)
    sunits = {
        'sun'                               :   param.get('seq_unit_name'),
        'targeted-logical-amount'           :   param.get('targeted_amount'),
        'total-logical-amount-completed'    :   param.get('logical_amount'),
        'abandon-library'                   :   param.get('abandon_library'),
        'abandon-sample'                    :   param.get('abandon_sample'),
        'data_useable'                      :   param.get('useable'),
        'fm_what'                           :   param.get('fm_what', ''),
        'fm_where'                          :   param.get('fm_where', ''),
        'comments'                          :   param.get('comments'),

        'insert_size_method'                :   param.get('insert_size_method'),
        'measured_insert_size'              :   param.get('measured_insert_size'),
        'measured_insert_size_std'          :   param.get('measured_insert_size_std')
    }

    if not sunits['fm_what']:
        del sunits['fm_what']
    if not sunits['fm_where']:
        del sunits['fm_where']

    if param.get('sow-item-complete'):
        sunits['sow-item-complete'] = param.get('sow-item-complete')

    return sunits;

def encoded_param(param):
    param = urllib.urlencode(param)
    param = param.replace('%5Cn', '%0A')    # replace the encoded '\n' with NEWLINE code
    return param

def curl_cmd(param):
    url = '%s/%s' % (WEB_HOST, WEB_API)

    #
    # CURL post with json format:
    # curl -H "Content-Type: application/json" -d '{"username":"xyz","password":"xyz"}' http://localhost:3000/api/login
    #

    # replace ' with unicode code in json string (when it appears in comments). double quotes are handled by dumps()
    cmd = 'curl -H "Content-Type: application/json" -d \'%s\' %s' % (json.dumps(param).replace('\'', '\u0027'), url)

    return cmd

def tla_unit_from_its(sun):
    cmd = 'curl -H "Content-Type: application/json" %s/api/sow_qc/its_logical_amounts?seq_unit_name=%s' % (WEB_HOST, sun)
    if DEVELOP:
        write_log('RUN : %s\n' % cmd)
    std_out, std_err, exit_code = run_command(cmd, True)    # exit_code of 0 is success
    if exit_code == 0:
        try:
            return json.loads(std_out)
        except:
            return None
    return None

'''
    for reschedule pool, pool members only need to have:
    {
        'data_useable': USEABLE,                    //backend will del this after reading the value out.
        'targeted-logical-amount': TLA,
        'total-logical-amount-completed': CLA,
        'abandon-sample': 'N',                      // fixed
        'abandon-library': 'N'                      // fixed
    }
'''
#
# to submit ONE rework QC
# config is a dict build from the single-line record in input file,
# param is a dict of all possible params for cgi
# required is a dict of required params
# live: boolean
#

def do_one(sth, config, param, required, toprint, atype, live):
    'Perform segmentQC submission for PASS on a single record.'
    ## cml keys in config file to cgi params, and rm cgi params that not in config file :
    # print('config : %s' % config)
    # print('param : %s' % param)

    param = dict(param)       # a local copy of ALL params

    load_config_to_param(config, param)

    ## perform sanity check on required cgi params from config file:
    key = missing_required_param(config, required)
    if key:
        write_log("Error : Missing Required value for param of %s" % key)
        return {'errors': 'bad record'}

    param['action_type'] = atype

    sunits = make_sunits_list(param)

    clean_ws_param(param)

    param['sequnits'] = [sunits,]

    if toprint:
        print('\nJSON data for RQC WS call:')
        pp.pprint(param)
        return {'status': 'OK'}

    cmd = curl_cmd(param)
    if live:
        write_log('Command: %s\n' % cmd)
        std_out, std_err, exit_code = run_command(cmd, True)    # exit_code of 0 is success
        if exit_code == 0:
            error =  error_in_ws_call(std_out)
            if error:
                write_log('Error (RQC WS returned error):')
                write_log('---------------------------------------\n%s' % error)
                write_log('---------------------------------------')
                return {'errors': 'RQC WS returned error'}
            else:
                write_log('WS call OK')
        else:
            write_log('Error (Run curl command failed):')
            write_log('---------------------------------------\n%s' % std_err)
            write_log('---------------------------------------')
            return {'errors': 'Run curl command failed'}
    else:
        write_log('----------------------')
        write_log(cmd)
        write_log('')

    return {'status': 'OK'}



def clean_comment(cmt):
    'Remove leading and trailing quotes in the given string, and return the cleaned string.'
    cmt = cmt.strip()
    if cmt[0] in QUOTE and cmt[-1] in QUOTE and cmt[0] == cmt[-1]:
        cmt = cmt[1:-1]
    return cmt

def time_now_str():
    'Return the formated time string for NOW.'
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

def dbinfo(sth):
    'Return a string with currently connected databsae info.'
    sql = 'SELECT @@hostname AS hostname, database() AS dbname, user() AS uname'
    sth.execute(sql)
    rows = sth.fetchone()
    return 'DB host=%s, name=%s, user=%s' % (rows['hostname'], rows['dbname'], rows['uname'])





def validate_value(aVal, aList, aTag, aLine):
    if aVal not in aList:
        write_log('Fatal Error - %s must have values of %s' % (aTag, str(sorted(list(aList)))))
        write_log('%s' % aLine, mtype=ACTION_EXIT)
        #exit(1)

def validate_value_ex(aVal, aObj, aTag, aLine, literal=False):
    if  (literal and aVal not in aObj) or ( type(aObj) == dict and aVal not in aObj ) or( type(aObj) == list and not literal and aVal not in xrange(len(aObj)) ): # not a key value
        write_log('Fatal Error - %s = ( %s ) : must be one of the following values:' % (aTag, color_str(str(aVal), 'red')))
        if type(aObj) == dict:
            for k in sorted(aObj.keys()):
                write_log('    %s - %s' % (str(k), aObj[k]))
        elif type(aObj) == list or type(aObj) == str:
            if not literal:
                for idx, val in enumerate(aObj):
                    write_log('    %d - %s' % (idx, val))
            else:
                tmp = ''
                for v in sorted(aObj):
                    if tmp:
                        tmp += ', '
                    tmp += v
                write_log('    [%s]' % tmp)
        else:
            write_log(' unknown data range object %s' % str(aObj))
        write_log('\n%s' % aLine, mtype=ACTION_EXIT)

def validate_bad_relation(badRelation, aTag, aLine):
    if badRelation:
        write_log('Fatal Error - %s' % aTag)
        write_log('   %s' % aLine)
        exit(1)

def YN2INT(aVal):
    if aVal == 'Y':
        return 1
    elif aVal == 'N':
        return 0
    else:
        write_log('Fatal Error - wrong value YN %s' % aVal)
        exit(1)

def parse_int(aVal, msg):
    try:
        return int(aVal)
    except:
        write_log(msg, mtype = ACTION_EXIT)

def insert_size_obj(sun):
    cmd = 'curl %s/api/sow_qc/insert_size/%s' % (WEB_HOST, sun)

    if DEVELOP:
        print('RUN : %s' % cmd)
    std_out, std_err, exit_code = run_command(cmd, True)

    if exit_code == 0:
        try:
            return json.loads(std_out.strip())
        except:
            print('Error : %s' % std_out.strip())
            return {'errors' : std_out.strip()}
    else:
        return {'errors' : std_err.strip()}

def do_auto(sth, ifile, toprint, atype, live):
    'Carry out the segment qc process on a given input file.'
    DASH_LEN = 50

    write_log('')
    write_log('==' * DASH_LEN)
    write_log('input file        : %s' % ifile)
    if not live:
        write_log(' --> this is a dry run. use -l | --live option to really perform the action')
    write_log('-' * DASH_LEN)
    write_log('Start time: ' + time_now_str() + '\n')

    VALID_FILE_SUFFIX = ('.fastq.gz', '.fastq')
    VALID_LAB_ACTION_CODE = LAB_ACTION_MAP.keys()

    def validate_positive_number(num, errmsg):
        if num < 0:
            write_log(errmsg, mtype = ACTION_EXIT)

    def validate_logical_amount_range(val, unit, sun, atype):
        valmax = LOGICAL_AMOUNT_LIMITS[unit.lower()]
        if val > valmax:
            write_log('Fatal Error: [%s] %s (%.1f) is greater than the max value of %d(%s)' % (sun, atype, val, valmax, unit), mtype = ACTION_EXIT)

    write_log('Processing input file %s    ...' % os.path.abspath(ifile))
    sun_map = {}    # hold seq unit name as key for duplication and segment id check
    analyst = None
    analyst_id = None
    email = None
    fin = open(ifile, 'r')

    lcnt = 0
    #PATTEN = r'(\S+)\s+(\S+)\s+(\S+)\s+(\w)\s+(\w)\s+([-]?\d)\s+([-]?\d)\s+([-]?\d)\s+([-]?\d+)\s+([-]?\d+)\s+(.*)$'    # rework QC format of ""
    PATTEN = r'(\S+)\s+(\S+)\s+(\S+)\s+(\w)\s+(\w)\s+([-]?\d)\s+([-]?\d)\s+([-]?\d)\s+([-]?\d+)\s+(\S+)\s+(.*)$'    # rework QC format of ""
    for line in iter(fin):
        lcnt += 1
        line = line.strip()
        if not line or line.startswith('#') or line.find('fastq') == 0:   # ignore empty line, comment lines, or line without seq unit name
            continue

        if not analyst: # have to have the analyst set before data lines
            analyst = get_analyst(line)
            if analyst:
                analyst_id = get_analyst_id(sth, analyst)
                if not analyst_id:
                    write_log('Fatal Error: Analyst not found in RQC: %s' % analyst, mtype = ACTION_EXIT)

        elif analyst and analyst_id:   # now go through the data after the analyst and analyst_id have valid values:
            if line.find('email') == 0 and line.find('=') > 0:
                token = line.split('=')
                if len(token) == 2:
                    email = token[1].strip()
                continue

            sun = None
            match = re.match(PATTEN, line)
            if match:
                sun = match.group(1)

            line = 'L%d : %s' % (lcnt, line)
            #print('\debug match=%s' % sun)
            if not sun or not sun.endswith(VALID_FILE_SUFFIX):
                write_log('Fatal Error - wrong line format :')
                write_log('   %s\n' % line)
                write_log('Check your input file format.\n', mtype = ACTION_EXIT)

            else:
                config = {  'seq_unit_name' : sun,
                            'platform'      : str(platform_name(sth, sun)).lower(),
                            'library_name'  : str(library_name(sth, sun)),
                            'analyst'       : analyst,
                            'analyst_id'    : analyst_id,
                            'email_name'    : email,
                            'tla_unit'      : 'None',
                            'its_tla'       : 'None',
                            'its_cla'       : 'None',
                }

                insertObj =insert_size_obj(sun)

                if 'errors' not in insertObj:
                    config['measured_insert_size'] = insertObj.get('measured_insert_size_filtered')
                    config['measured_insert_size_std'] = insertObj.get('measured_insert_size_std_filtered')
                    config['insert_size_method'] = insertObj.get('insert_size_method')

                try:
                    config['targeted_amount'] = float(match.group(2))
                    config['logical_amount'] = float(match.group(3))

                except ValueError as verror:
                    write_log('Fatal Error: Non-numeric values in TLA/CLA field found: \n%s' % line, mtype = ACTION_EXIT)

                validate_positive_number(config['targeted_amount'], 'Fatal Error: [%s] TLA cannot be negative' % sun)
                validate_positive_number(config['logical_amount'], 'Fatal Error: [%s] CLA cannot be negative' % sun)

                itsvals = tla_unit_from_its(sun)
                if not itsvals:
                    write_log('Fatal Error: Failed to retrieve TLA unit from ITS for %s' % sun, mtype = ACTION_EXIT)
                elif 'errors' in itsvals:
                    write_log('Error: Failed to retrieve TLA unit from ITS for %s - %s' % (sun, itsvals['errors']))
                else:
                    aUnit = itsvals['tla_unit']
                    validate_logical_amount_range(config['targeted_amount'], aUnit, sun, 'TLA')
                    validate_logical_amount_range(config['logical_amount'], aUnit, sun, 'CLA')

                    config['its_tla'] = itsvals['its_tla']
                    config['its_cla'] = itsvals['its_cla']
                    config['tla_unit'] = aUnit

                # the rework QC format
                config['abandon_library'] = match.group(4)
                config['abandon_sample'] = match.group(5)

                validate_value_ex(config['abandon_library'], 'YN', 'Abandon Library', line, literal=True)
                validate_value_ex(config['abandon_sample'], 'YN', 'Abandon Sample', line, literal=True)
                config['abandon_library'] = YN2INT(config['abandon_library'])
                config['abandon_sample'] = YN2INT(config['abandon_sample'])


                config['lab_action'] = match.group(6)
                validate_value_ex(config['lab_action'], LAB_ACTION_MAP, 'Lab Action', line)
                #validate_value(config['lab_action'], VALID_LAB_ACTION_CODE, 'Lab Action', line)

                config['useable'] = match.group(7)
                validate_value_ex(config['useable'], '01', 'Useable', line, literal=True)

                config['useable'] = int(config['useable'])  # must convert to int

                config['sow-item-complete'] = match.group(8)
                validate_value_ex(config['sow-item-complete'], SOW_STATUS_MAP, 'SOW Status', line)
                config['sow-item-complete'] = SOW_STATUS_MAP[config['sow-item-complete']]

                # failure mode
                # it is optional if USEABLE=1 ( where, -1 meaning igore )
                config['fm_where'] = match.group(9)
                config['fm_where'] = parse_int(config['fm_where'], 'Fatal Error: wrong format in FM_M: \n%s' % line)

                # Failuer Mode Major: valid value is required for FM_M if data is not useable, OR FM_M !=-1 / 100
                if config['useable'] == '0' or (config['fm_where'] != -1 and config['fm_where'] != 100) :
                    validate_value_ex(config['fm_where'], FM_MAJOR_CODE, 'Failure Mode major (FM_M)', line)

                #Failure Mode minor
                if config['fm_where'] != -1 and config['fm_where'] != 100:
                    config['fm_what'] = match.group(10)
                    if config['fm_where'] != -1 and config['fm_where'] != 100:
                        config['fm_what'] = parse_int(config['fm_what'], 'Fatal Error: wrong format in FM_m: \n%s' % line)
                    validate_value_ex(config['fm_what'], FM_DATA[FM_MAJOR_CODE[config['fm_where']]], 'Failure Mode minor (FM_m)', line)
                elif config['fm_where'] == 100:
                    config['fm_what'] = match.group(10)

                # FM Code to CV
                if (config['fm_where'] > -1 or config['fm_where'] == 100) and config['fm_what'] > -1:
                    if config['fm_where'] != 100:
                        config['fm_what'] = FM_DATA[FM_MAJOR_CODE[config['fm_where']]][config['fm_what']]
                        config['fm_where'] = FM_MAJOR_CODE[config['fm_where']]  # now the code of FM_M is no longer needed, convert back to str
                    else:
                        # multi FM :
                        toks = config['fm_what'].split(',')
                        fmdict = {}
                        for tk in toks:
                            tmp = tk.split(':')
                            if len(tmp) != 2:
                                write_log('Fatal Error - Multiple Failure mode has wrong FM_m format [ %s ]' % color_str(tk, 'red'))
                                write_log('%s' % line, mtype=ACTION_EXIT)
                            else:
                                tmp_fM = parse_int(tmp[0], 'Fatal Error: Need int in Multi Failure Mode major [%s] : %s'%(tk, line))
                                tmp_fm = parse_int(tmp[1], 'Fatal Error: Need int in Multi Failure Mode minor [%s] : %s'%(tk, line))
                                validate_value_ex(tmp_fM, FM_MAJOR_CODE, 'Multi Failure Mode major in [%s]'%tk, line)
                                validate_value_ex(tmp_fm, FM_DATA[FM_MAJOR_CODE[tmp_fM]], 'Multi Failure Mode minor in [%s]'%tk, line)
                                tlist = fmdict.get(tmp_fM)
                                if not tlist:
                                    tlist = []
                                    fmdict[tmp_fM] = tlist
                                # FM minor value for multip failure mode need to be in format of FMM_FMm
                                tlist.append('%s_%s' % (FM_MAJOR_CODE[tmp_fM], FM_DATA[FM_MAJOR_CODE[tmp_fM]][tmp_fm]))
                        config['fm_where'] = 'multiple_failure_modes'   # FM Major key for multip failure mode is 'multiple_failure_modes'
                        fmlist = []
                        for fmM in sorted(fmdict):
                            fmlist += fmdict[fmM]
                        config['fm_what'] = ':'.join(fmlist)
                else:
                    if 'fm_where' in config:
                        del config['fm_where']
                    if 'fm_what' in config:
                        del config['fm_what']


                config['comments'] = clean_comment(match.group(11))

                validate_bad_relation(config['abandon_library'] == 'Y' and config['lab_action'] > '1', 'Abandon Library cannot pair with LAB_ACTION > 1', line)
                validate_bad_relation(config['abandon_sample'] == 'Y' and config['lab_action'] != '0', 'Abandon Sample must be paired with LAC_ACTION=0', line)
                validate_bad_relation(config['abandon_sample'] == 'Y' and config['sow-item-complete'] != SOW_STATUS_MAP['2'], 'Abandon Sample requires SOW=2', line)


                config['lab_action'] = LAB_ACTION_MAP[config['lab_action']]

                if config['abandon_sample'] == 'Y':
                    config['sow-item-complete'] = 'Needs Attention'


                if config['targeted_amount'] < 0 or config['logical_amount'] < 0:
                    write_log('Fatal Error: Negative values in TLA/CLA field found: %s' % line, mtype = ACTION_EXIT)

                if sun in sun_map:
                    write_log('Seq unit has been used already : \n%s\n' % line, mtype = ACTION_EXIT)
                sun_map[sun] = config

    def summar_line(msg, iVal):
        write_log('%-40s : %d' % (msg, iVal) )

    if sun_map:
        action_cnt = 0
        action_succeeded = 0
        action_failed = 0

        can_try_cnt = 0                     # tolal record in input that can be used for submission
        bad_record = 0                      # malformed record count in input file
        qc_have_been_done_before_cnt = 0    # have been QCed count

        if toprint:
            write_log("\nWS URL=%s/%s" % (WEB_HOST, WEB_API))
            write_log('Content Type: application/json')


        for sun in sun_map:
            conf = sun_map[sun]
            action_cnt += 1
            write_log("\n%d) %s - %s\tlib=%s\tITS_TLA=%s(%s)\tITS_CLA=%s" % (action_cnt, atype.title(), sun, conf['library_name'], conf['its_tla'], conf['tla_unit'], conf['its_cla']) )

            msg = ''
            mtype = ACTION_OK

            # TODO: check if sun in RQC db first!!
            if conf['library_name'] == 'None':
                write_log('bad data - no db record for seq unit name.', ACTION_FAIL)
                bad_record += 1
                continue

            if conf['platform'].lower() != 'illumina':
                write_log('bad data - unsupported platform [%s].' % conf['platform'], ACTION_FAIL)
                bad_record += 1
                continue

            if check_qc_status(sth, conf['seq_unit_name']):
                write_log('seq unit %s has been QCed before and thus can not be QCed again.' % config['seq_unit_name'], ACTION_FAIL)
                qc_have_been_done_before_cnt += 1
                continue
            else:
                can_try_cnt += 1

            if not conf['tla_unit'] or conf['tla_unit'] == 'None':
                write_log('bad data - no tla unit.')
                bad_record += 1
                continue

            rtn = do_one(sth, conf, ALL_PARAM, PARAM_REQUIRED, toprint, atype, live)
            if 'errors' in rtn:
                if live:
                    msg = 'action failed'
                    mtype = ACTION_FAIL
                    action_failed += 1
            else:
                if live:
                    msg = 'action completed'
                action_succeeded += 1


            if msg and not toprint:
                write_log(msg, mtype)

            write_log('')


            # pause for 5 seconds, to give the PPS WS server a break when the input file has many entries
            if live:
                time.sleep(5)


        write_log('')
        write_log('-' * DASH_LEN)
        summar_line('total records in file', len(sun_map))
        if live:
            summar_line('total records attempted for action', can_try_cnt)
        else:
            summar_line('total records can be used for action', can_try_cnt)

        if live:
            summar_line('total succeeded action', action_succeeded)
            summar_line('total failed action', action_failed)

        summar_line('total records have been QCed before', qc_have_been_done_before_cnt)
        summar_line('total records of bad data', bad_record)
        write_log('==' * DASH_LEN)
        write_log('End time : ' + time_now_str() + "\n")
    elif not analyst:
        write_log('No analyst (mandantory) is defined in input file.')
    else:
        write_log('No data in input file for segment qc.')


def conn_db():

    conn = None
    db_id = "rqc-prod"
    if DEVELOP:
        db_id = "rqc-dev"
        

    #print('Connecting to dev DB : %s ... ' % dbn)
    conn = jgi_connect_db(db_id)

    if conn == None:
        print('Error : failed to connect to RQC database')
        sys.exit(1)

    sth = conn.cursor(MySQLdb.cursors.DictCursor)
    dbparam = dbinfo(sth)
    print(dbparam)



    return sth

def print_error(msg):
    print('\nERROR : %s' % msg)
    exit(1)

if __name__ == '__main__':
    VERSION = "2.1.0 (4/28/2017)"
    NOTE = '\n'.join([
                        'Bulk rework QC command line tool.',
                        'Author : Shijie Yao',
                        ' 4/28/2017 - get MF cv from WS call to seqqc.py; handle multiple failure mode',
                        ' 4/03/2017 - add insert_size, insert_size_std and insert_size_method to reworkqc submission',
                        ' 1/29/2016 - enforce TLA/CLA limits',
                        ' 8/06/2015 - make it be able to use jgi-rqc-pipeilne/lib or jgi-rqc/rqc_system/lib',
                        ' 6/26/2015 - sow status const updated to ["Needs Lab Work", "Complete", "Needs PMO Attention"]',
                        ' 2/26/2015 - match with sow_qc backend Version 1.2.0',
                        ' 3/02/2015 - enforce TLA/CLA limitsh the backend change that unified the pacbio and illumna sow qc process',
                        ' 1/16/2015 - support pass|fail of 2 different input template formats (the legacy segmentQC format, and the new rework QC format)',
                        ' 1/21/2015 - remove pass|fail ACTION and only use the new verbose data format',
                        ' 1/26/2015 - add save action'
                    ])

    USAGE = "* Auto rework QC, version %s\n" % (VERSION)

    TYPES = ['submit', 'save', 'validate']
    # command line options
    parser = argparse.ArgumentParser(description=USAGE, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input", dest="infile", help="A file containing the rework qc entries.")
    parser.add_argument("-l", "--live", dest="live", action='store_true', default=False, help="To actually execute the QC. Else do dry run.")
    parser.add_argument("-p", "--print", dest="toprint", action='store_true', help="Print out the template for input file format. Use -f [1|2] to specify format.")
    parser.add_argument("-d", "--dev", dest="dev", action='store_true', help="Execute script in development mode.")
    parser.add_argument("-t", "--type", dest="atype", choices=TYPES, default=TYPES[0], help="The run type.")
    parser.add_argument("-", "--hist", dest="history", action='store_true', help="Print coding history")
    parser.add_argument('-v', '--version', action='version', version=VERSION)

    OPTIONS = parser.parse_args()

    DEVELOP = OPTIONS.dev

    if OPTIONS.history:
        print('')
        print('Version : %s' % VERSION)
        print(NOTE)
        print('')
        exit(0)

    if DEVELOP:
        WEB_HOST = WEB_HOST_DEV  # Shijie's dev web server
    elif 'RQC_SW_TOP_LEVEL_DIR' in os.environ:
        print('\nError : You are a developer and need to run this script with the --dev option.\n')
        sys.exit(1)

    FM_MAJOR_CODE = failure_mode_data('failure_major_code')
    FM_DATA = failure_mode_data('failure_mode_illumina')

    if OPTIONS.toprint and not OPTIONS.infile:
        print_template()
        exit(0)

    if DEVELOP:
        print('\n Run in dev mode ...\n')
    else:
        print('\n Run in production mode ...\n')

    sth = conn_db()

    if not OPTIONS.infile:
        print('require -f INPUT_DATA_FILE run this program')
        sys.exit(1)
    elif not os.path.isfile(OPTIONS.infile):
        print('Invalide input file %s!' % OPTIONS.infile)
        sys.exit(1)
    else:
        LOG_FILE = '%s.log' % OPTIONS.infile
        do_auto(sth, OPTIONS.infile, OPTIONS.toprint, OPTIONS.atype, OPTIONS.live)
