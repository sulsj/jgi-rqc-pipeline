#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Pipeline Tarballer
- used to create a tarball of the pipeline run and create a metadata.json file for submissions
- RQC-1124

v 1.0: 2018-07-10
- initial version

Consider looking at log files to get the ap/at and library and inputs.


./pipe_fail.py -o /global/projectb/scratch/brycef/iso/gch3 -pl -l cnboy


./jamo_save.py -f /global/projectb/scratch/brycef/filtering_references-20160921.tar --file-type tarball -j /global/projectb/scratch/brycef/filter_ref.metadata.json

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import getpass
import json

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))
sys.path.append(os.path.join(my_path, '../sag'))

from common import get_logger, run_cmd, set_colors, get_msg_settings, human_size
from micro_lib import get_web_page

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Create a tarball in the output_path named (analysis_project_id).(analysis_task_id).tar.gz
- might have issues with a output_path that has millions of files in it (trinity run?)

'''
def make_tarball(analysis_project_id, analysis_task_id):
    log.info("make_tarball:  ap = %s, at = %s", analysis_project_id, analysis_task_id)

    tarball = os.path.join(output_path, "%s.fail.tar.gz" % (analysis_task_id))
    tarball_contents = tarball.replace(".tar.gz", ".tar.txt") # contents of tarball
    tarball_toc = tarball.replace(".tar.gz", ".tar.json") # table of contents for metadata.json

    #if os.path.isfile(tarball):
    #    cmd = "rm %s" % tarball
    #    std_out, std_err, exit_code = run_cmd(cmd, log)
    #if os.path.isfile(tarball_contents):
    #    cmd = "rm %s" % tarball_contents
    #    std_out, std_err, exit_code = run_cmd(cmd, log)
    #if os.path.isfile(tarball_toc):
    #    cmd = "rm %s" % tarball_toc
    #    std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(tarball_toc):
        log.info("- found %s, skipping tarring folder", tarball_toc)
    else:

        cmd = "cd %s;tar -czvf %s *" % (output_path, tarball)
        # c = create, z = gzip, v = verbose, f = create file
        std_out, std_err, exit_code = run_cmd(cmd, log)

        if os.path.isfile(tarball):
            cmd = "tar -tvf %s > %s" % (tarball, tarball_contents)
            # get the contents of the tarball (-t = test)
            std_out, std_err, exit_code = run_cmd(cmd, log)

            toc = [] # list of dicts
            fh = open(tarball_contents, "r")
            for line in fh:
                line = line.strip()
                arr = line.split()
                # -rw-r--r-- brycef/genome  8648 2018-06-28 14:28 contigs_Class.png
                if len(arr) > 3:
                    if int(arr[2]) > 0:
                        tocd = { "file_name" : os.path.basename(arr[-1]), "file_size" : int(arr[2]), "file_path" : os.path.dirname(arr[-1]) }
                        toc.append(tocd)
            fh.close()

            log.info("- created: %s (%s)", tarball, human_size(os.path.getsize(tarball)))
            log.info("- file count: %s", len(toc))

            fh = open(tarball_toc, "w")
            json.dump(toc, fh)
            fh.close()


'''
Get the spid, library name and input seq units
https://rqc-1.jgi-psf.org/api/rqcws/lib2su/cnboy
- create (analysis_task_id).fail.metadata.json for importing into jamo
'''
def create_fail_metadata(library_name, analysis_project_id, analysis_task_id):
    log.info("create_fail_metadata: %s, %s, %s", library_name, analysis_project_id, analysis_task_id)


    # this is the initial metadata version, might need to change to a JAT template ...
    fail_metadata = {
    "metadata" : {
       "library_name" : [],
       "sequencing_project_id": [],
       "analysis_project_id" : [analysis_project_id],
       "analysis_task_id" : [analysis_task_id],
       "seq_unit_name" : []
    },
    "outputs" : [
      {
        "label" : "tarball",
        "file" : "",
        "folder_index" : []
      }
    ],
    "publish_flag" : False
    }


    rqc_api = "api/rqcws/lib2su/%s" % library_name
    response = get_web_page(RQC_URL, rqc_api, log)


    for r in response:
        if r['read_count'] > 0:
            if r['library_name'] not in fail_metadata['metadata']['library_name']:
                fail_metadata['metadata']['library_name'].append(r['library_name'])
            if r['spid'] not in fail_metadata['metadata']['sequencing_project_id']:
                fail_metadata['metadata']['sequencing_project_id'].append(r['spid'])
            if r['seq_unit_name'] not in fail_metadata['metadata']['seq_unit_name']:
                fail_metadata['metadata']['seq_unit_name'].append(r['seq_unit_name'])


    # get tarball_toc
    tarball_json = os.path.join(output_path, "%s.fail.tar.json" % (analysis_task_id))
    fh = open(tarball_json)
    tarball_toc = json.load(fh)
    fh.close()


    tarball =  "%s.fail.tar.gz" % (analysis_task_id) # want relative path
    fail_metadata['outputs'][0]['file'] = tarball
    fail_metadata['outputs'][0]['folder_index'] = tarball_toc


    fail_metadata_json = os.path.join(output_path, "%s.fail.metadata.json" % (analysis_task_id))
    fh = open(fail_metadata_json, "w")
    json.dump(fail_metadata, fh)
    fh.close()

    log.info("- ready to import %s", fail_metadata_json)


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "Pipeline Tarballer"
    version = "1.0"

    RQC_URL = "https://rqc-1.jgi-psf.org"

    uname = getpass.getuser() # get the user, brycef can't use scell.p, but qc_user can!

    color = {}
    color = set_colors(color, True)
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)


    # Parse options
    usage = "pipe_fail.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """

    """

    parser = ArgumentParser(usage = usage)

    parser.add_argument("-ap", "--analysis-project-id", dest="analysis_project_id", type = int, help = "analysis project id")
    parser.add_argument("-at", "--analysis-task-id", dest="analysis_task_id", type = int, help = "analysis task id")
    parser.add_argument("-l", "--library", dest="library_name", type = str, help = "library name")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-o", "--output-path", dest="output_path", type = str, help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-v", "--version", action="version", version=version)

    output_path = None
    print_log = False
    analysis_project_id = 0
    analysis_task_id = 0
    library_name = None

    # parse args
    args = parser.parse_args()
    print_log = args.print_log

    if args.output_path:
        output_path = args.output_path

    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    # analysis project id
    if args.analysis_project_id:
        analysis_project_id = args.analysis_project_id
    if args.analysis_task_id:
        analysis_task_id = args.analysis_task_id
    if args.library_name:
        library_name = args.library_name


    # initialize my logger
    log_file = os.path.join(output_path, "pipeline_tarballer.log")



    # log = logging object
    log_level = "INFO"
    log = None
    log = get_logger("tarball", log_file, log_level, print_log)


    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "analysis_project_id", analysis_project_id)
    log.info("%25s      %s", "analysis_task_id", analysis_task_id)
    log.info("%25s      %s", "library_name", library_name)
    log.info("")

    if not library_name:
        log.error("- missing library name!  %s", msg_fail)
        sys.exit(88)


    # create tarball, with index of files
    make_tarball(analysis_project_id, analysis_task_id)

    # grab metadata based on library name and ap,at ids and create *.fail.metadata.json
    create_fail_metadata(library_name, analysis_project_id, analysis_task_id)


    log.info("Completed %s", script_name)

    sys.exit(0)

"""

              --      --
            .:"  | .:'" |
          --  ___   ___  -           ... mmmm beer
        /:.  /  .\ /.  \ .\
       |:|. ;\___/O\___/  :|
       |:|. |  `__|__'  | .|
       |:|.  \_,     ,_/  /
        \______       |__/
         |:.           \
        /.:,|  |        \
       /.:,.|  |         \
       |::.. \_;_\-;       |
 _____|::..    .::|       |
/   ----,     .::/__,    /__,
\_______|,...____;_;_|../_;_|


metadata.json
{
  "metadata" : {
     "library_name" : ["..."],
     "sequencing_project_id": [...],
     "analysis_project_id" : [...],
     "analysis_task_id" : [....],
     "seq_unit_name" : ["..."]
    },
    "outputs":[
     {
       "label" : "tarball",
       "file" : "tarball.tar"
       "folder_index" : [...]
      }
    ],
    "publish_flag" : false
}


-----------------------------[ JAT yaml template ]
name: project_failure
description: tarball of failed run

tags:
- failed

required_metadata_keys:
- macro: production_release


outputs:
- description: Tarball containing all the files in the pipeline run
  label: tarball
  required_metadata_keys:
    - macro: file_info
  default_metadata_values:
    file_format: tar
    compression: gz
-------------------------------

"""