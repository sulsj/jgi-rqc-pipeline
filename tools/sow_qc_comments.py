#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    RQCSUPPORT-871
    looks at filtered seq units (and pdf reports) created in the last 6 months and checks if
    the seq_qc is pass: portal.display_location is set or if seq_qc is fail: portal.display_location is not set

    RUN module load lapinpy first !
    Shijie Yao
    Nov 17, 2016
"""
import os
import sys
import MySQLdb
import datetime
import re
import time
from argparse import ArgumentParser
import unicodedata

from pprint import pprint

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir)
LIB_DIR = '%s/lib' % ROOT_DIR
if LIB_DIR not in sys.path:
    sys.path.append(LIB_DIR)

from db_access import db_connect
from os_utility import run_sh_command


def get_db_connection(dbname='rqc'):

    if dbname == 'rqc_dump':
        dbserver ='draw.jgi-psf.org'
        dbuser = 'rqc_dev'
        dbpwd = 'b1Jpb24uVGhyeWxsLnJhdDo5OQ=='
    else:
        dbserver ='scidb1.nersc.gov'
        dbuser = 'rqc_prod'
        dbpwd = 'dHJvd3QudGFYZXMub0Jsb25nLjIyMzs='

    print('\nConnecting to %s@%s ... \n' % (dbname, dbserver))
    db = db_connect(db_server=dbserver, db_user=dbuser, db_name=dbname, db_pwd=dbpwd)

    sth = None
    if db:
        sth = db.cursor(MySQLdb.cursors.DictCursor)
    else:
        print('  -- data base connection failed!')
    return sth

def print_comments(com, smark='>', emark='<'):
    print(smark*60)
    print(com)
    print(emark*60)

def do_one(sth, sun, comments, rtype, live):
    sql = 'SELECT comments FROM seq_unit_qc WHERE seq_unit_name = %s'
    sth.execute(sql, (sun,))
    row = sth.fetchone()

    # print('%15s : %s' % ('Seq Unit Name', sun))
    if row:
        if not live:
            print('%15s : ' % 'Old Comments')
            print_comments(row['comments'])

        if rtype == 'append':
            comments = '%s\n%s' % (row['comments'], comments)
        elif rtype == 'prepend':
            comments = '%s\n%s' % (comments, row['comments'])


        if not live:
            print('%15s :' % 'New Comments')
            print_comments(comments, smark='~', emark='^')

        if live:
            sql = "Update seq_unit_qc SET comments = %s WHERE seq_unit_name = %s"
            sth.execute(sql, (comments, sun))
            print('comments updated.')
    else:
        print('data not QCed. SKIP')
    print('\n\n')

def do_update(infile, rtype, database, live):
    if database=='prod':
        dbname = 'rqc'
        # print('Connecting to RQC production db : %s' % dbname)
    else:
        dbname = 'rqc_dump'
        # print('Connecting to RQC development db : %s' % dbname)

    sth = get_db_connection(dbname)

    print('%15s : %s' % ('Input file', infile))
    print('%15s : %s' % ('DB type', database))
    print('%15s : %s' % ('Run type', rtype))
    print('%15s : %s' % ('Live run', live))
    print('\n')

    count = 0
    with open(infile, 'r') as FH:
        for line in FH:
            count += 1
            toks = re.split('[ \t,;]+', line.strip(), 1)
            print('%3d) %s' % (count, toks[0]))
            print('-' * 60)
            do_one(sth, toks[0], toks[1], rtype, live)


if __name__ == '__main__':
    NAME = 'Update SOW QC comments for already QCed records.'
    VERSION = '1.0.0'

    USAGE = "prog [options]\n"
    USAGE += "* %s, version %s\n" % (NAME, VERSION)

    JOB_CMD = ' '.join(sys.argv)
    UPDATE_TYPE = ['append', 'prepend', 'replace']
    DB_TYPE = ['prod', 'dev']
    PARSER = ArgumentParser(usage=USAGE)
    PARSER.add_argument("-i", "--input", dest="infile", help="Input file [SEQ_UNIT_NAME COMMENTS]", required=True)
    PARSER.add_argument("-t", "--type", dest="type", choices=UPDATE_TYPE, help="Update type", required=True)
    PARSER.add_argument("-d", "--db", dest="database", choices=DB_TYPE, default=DB_TYPE[0], help="database to be used")
    PARSER.add_argument("-l", "--live", dest="live", action='store_true', default=False, help="Live run")
    PARSER.add_argument("-v", "--version", action="version", version=VERSION)

    ARGS = PARSER.parse_args()

    print('\n\n')
    print('START PROCESSING ...\n')
    do_update(ARGS.infile, ARGS.type, ARGS.database, ARGS.live)

