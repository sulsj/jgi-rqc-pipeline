#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
PacBioqc reads-of-insert

Command
    $ reads_of_insert.py --params settings.xml --output output_path --temp tmp_path --xml input.xml


Created: May 23 2016

sulsj (ssul@lbl.gov)


Rvisions:




"""

import os
import sys
# import time
import argparse
import glob

## custom libs in "../lib/"
ROOT = os.path.dirname(__file__)
sys.path.append(os.path.join(ROOT, '../lib'))    ## rqc-pipeline/lib
sys.path.append(os.path.join(ROOT, '../tools'))  ## rqc-pipeline/tools

from common import get_run_path, get_logger, get_status, append_rqc_stats, append_rqc_file
from rqc_utility import *
from rqc_constants import *
from db_access import *
from os_utility import make_dir_p, run_sh_command


SCRIPTNAME = __file__
VERSION = "0.0.9"
CFG = {}
#LOGLEVEL = "INFO"
LOGLEVEL = "DEBUG"



"""
STEP 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def roi(settingXml, inputXml, outputPath, tempPath, log):
    log.info("\n\nSTEP1 - reads of insert <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n")
    status = "1_roi in progress"
    checkpoint_step_wrapper(status)

    ## Raw command:
    ## smrtpipe.py --params=settings.xml --output=(output path) -D TMP=(output path/tmp) xml:(xml for input files)
    cmd = "module load smrtanalysis/2.3.0_p5; smrtpipe.py --params=%s --output=%s -D TMP=%s xml:%s" % (settingXml, outputPath, tempPath, inputXml)
    run_sh_command(cmd, True, log, True)

    retCode = RQCExitCodes.JGI_SUCCESS


    if retCode != RQCExitCodes.JGI_SUCCESS:
        log.info("1_roi failed.")
        status = "1_roi failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("1_roi complete.")
        status = "1_roi complete"
        checkpoint_step_wrapper(status)


    return status


"""
STEP 2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def step_2(log):
    log.info("\n\nSTEP2 - step 2 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n")
    status = "2_step_2 in progress"
    checkpoint_step_wrapper(status)


    retCode = RQCExitCodes.JGI_SUCCESS


    if retCode != RQCExitCodes.JGI_SUCCESS:
        log.info("2_step_2 failed.")
        status = "2_step_2 failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("2_step_2 complete.")
        status = "2_step_2 complete"
        checkpoint_step_wrapper(status)


    return status



'''===========================================================================
    checkpoint_step_wrapper

'''
def checkpoint_step_wrapper(status):
    assert(CFG["status_file"])
    checkpoint_step(CFG["status_file"], status)


'''===========================================================================
Create the input.xml for smrtpipe based on the library name
- look up sample ids (run_barcode) in seq_units table
- look up h5 in sdm_pacbio database
- create a input.xml with the path-to-*.h5
runs should be stored here: /global/seqfs/sdm/prod/pacbio/runs/
'''
def create_input_xml(libraryName, outputPath, log):
    log.info("create_input_xml: %s", libraryName)

    input_xml = None

    if libraryName:
        input_xml = os.path.join(outputPath, "input-%s.xml" % (libraryName))
        fh = open(input_xml, "w")

        fh.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n")
        fh.write("<pacbioAnalysisInputs>\n")

        # skipping <job>..</job> section

        fh.write("  <dataReferences>\n")

        sample_list = [] # list of sample_ids to pull from sdm_pacbio


        if libraryName:
            # model_id 11 = Pacbio
            sql = "select seq_unit_id, run_barcode from seq_units where library_name = %s and model_id = 11"

            db = jgi_connect_db("rqc")
            sth = db.cursor(MySQLdb.cursors.DictCursor)

            sth.execute(sql, (libraryName))
            row_cnt = int(sth.rowcount)

            log.info("- found %s seq_units", row_cnt)

            for _ in range(row_cnt):
                rs = sth.fetchone()
                sample_list.append(rs['run_barcode'])

            db.close()

        file_cnt = 0

        # use sc_location to pull out bax.h5 files instead of sdm_pacbio database which is missing files
        # glob: rs['sc_location']/Analysis_Results/*.bax.h5
        # or like 'sample_name%' ... ?
        if len(sample_list) > 0:
            sql = "select sc.smrt_cell_id, sc.sample_name, sc.fs_location from smrt_cells sc where sc.sample_name in ("

            for _ in sample_list:
                sql += "%s,"
            sql = sql[:-1]
            sql = sql + ")"

            db = jgi_connect_db("sdm-pb")
            sth = db.cursor(MySQLdb.cursors.DictCursor)

            sth.execute(sql, (sample_list))
            row_cnt = int(sth.rowcount)
            log.info("- found %s smrt_cells", row_cnt)

            for _ in range(row_cnt):
                rs = sth.fetchone()

                fh.write("  <data ref=\"%s\">\n" % (rs['fs_location']))
                log.info("- ref: %s", rs['fs_location'])

                h5_arr = glob.glob(os.path.join(rs['fs_location'], "Analysis_Results", "*.bax.h5"))

                for h5_file in h5_arr:
                    fh.write("      <location>%s</location>\n" % h5_file)
                    log.info("-   h5: %s", os.path.basename(h5_file))
                    file_cnt += 1

                fh.write("  </data>\n")

        #<tag type="primaryAnalysisProtocol">Analysis_Results</tag>
        fh.write("  </dataReferences>\n")
        fh.write("</pacbioAnalysisInputs>\n")
        fh.close()

        if file_cnt == 0:
            log.error("- No h5 files found for library: %s", libraryName)
            sys.exit(2)
        else:
            log.info("- wrote %s, %s h5 files", input_xml, file_cnt)

        db.close()


    return input_xml



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

if __name__ == "__main__":
    desc = "RQC Pacbio Reads-of-insert Pipeline"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-i", "--input-xml", dest="inputXmlFile", help="Input XML file to use", required=False)
    parser.add_argument("-l", "--lib-name", dest="libName", help="Library name", required=True)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Output path to write to", required=False)
    parser.add_argument("-s", "--settings", dest="settingsXmlFile", help="Custom smrtportal settings.xml", required=False)
    parser.add_argument("-t", "--temp-path", dest="tempPath", help="Temp path to write to", required=False)
    parser.add_argument('-v', '--version', action='version', version=VERSION)

    options = parser.parse_args()

    outputPath = None
    tempPath = None


    libName = options.libName
    if options.outputPath:
        outputPath = options.outputPath
    else: outputPath = os.getcwd()
    if options.tempPath:
        tempPath = options.tempPath
    else:
        tempPath = os.path.join(outputPath, "tmp")

    make_dir_p(outputPath)
    make_dir_p(tempPath)

    print "Output path: ", outputPath
    print "Temp path: ", tempPath

    CFG["status_file"] = os.path.join(outputPath, "reads_of_insert_status.log")
    CFG["files_file"] = os.path.join(outputPath, "reads_of_insert_files.tmp")
    CFG["stats_file"] = os.path.join(outputPath, "reads_of_insert_stats.tmp")
    CFG["log_file"] = os.path.join(outputPath, "reads_of_insert.log")

    log = get_logger("reads-of-insert", CFG["log_file"], LOGLEVEL, False, True)

    if options.inputXmlFile:
        inputXmlFile = options.inputXmlFile
    else:
        ## create input_xml based on library name
        inputXmlFile = create_input_xml(libName, outputPath, log)
    if options.settingsXmlFile:
        settingsXmlFile = options.settingsXmlFile



    print "Started reads-of-insert pipeline, writing log to: %s" % (CFG["log_file"])



    log.info("=================================================================")
    log.info("   Pacbio reads-of-insert analysis (version %s)" % (VERSION))
    log.info("=================================================================")

    log.info("Starting %s...", SCRIPTNAME)


    status = None
    nextStep = 0


    if os.path.isfile(inputXmlFile) and os.path.isfile(settingsXmlFile):
        log.info("Found config files, %s and %s, starting processing.", inputXmlFile, settingsXmlFile)
        if os.path.isfile(CFG["status_file"]):
            status = get_status(CFG["status_file"], log)
            log.info("Latest status = %s" % (status))

            if status == "start":
                pass

            elif status != "complete":
                nextStep = int(status.split("_")[0])
                if status.find("complete") != -1:
                    nextStep += 1
                log.info("Next step to do = %s" % (nextStep))

            else:
                ## already Done. just exit
                log.info("Completed %s: %s", SCRIPTNAME, fastq)
                exit(0)
        else:
            checkpoint_step_wrapper("start")
            status = get_status(CFG["status_file"], log)
            log.info("Latest status = %s" % (status))

    else:
        log.error("- %s not found, aborting!", fastq)
        exit(2)



    done = False
    cycle = 0

    filesFile = CFG["files_file"]
    statsFile = CFG["stats_file"]


    if status == "complete":
        log.info("Status is complete")
        done = True


    while not done:

        cycle += 1

        if nextStep == 1 or status == "start":
            status = roi(settingsXmlFile, inputXmlFile, outputPath, tempPath, log)

        if status == "1_roi failed":
            done = True


        if nextStep == 2 or status == "1_roi complete":
            status = step_2(log)

        if status == "2_step_2 failed":
            done = True


        if nextStep == 3 or status == "2_step_2 complete":
            # status = do_cleanup(log)
            status = "3_cleanup complete"

        if status == "3_cleanup failed":
            done = True

        if status == "3_cleanup complete":
            status = "complete" ## FINAL COMPLETE!
            done = True


        ## don't cycle more than 10 times ...
        if cycle > 10:
            done = True



    if status != "complete":
        log.info("Status %s", status)

    else:
        log.info("\n\nCompleted %s", SCRIPTNAME)
        checkpoint_step_wrapper("complete")
        log.info("Done.")
        print "Done."


## EOF
