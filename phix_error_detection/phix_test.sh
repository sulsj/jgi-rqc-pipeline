#!/bin/bash -l

#set -e

# submits 3 qsub jobs for Phix Error Detection testing

folder="/global/projectb/sandbox/rqc/analysis/qc_user/phix"
DATE=`date +"%Y%m%d"`

# prod
#cmd="/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/phix_error_detection/phix_error_detection.py"

#dev for Bryce
cmd="/global/homes/b/brycef/git/jgi-rqc-pipeline/phix_error_detection/phix_error_detection.py"

# MiSeq: Flowcell AEBCY, lane 1 (Pass, Fail)
qsub -b y -j y -m n -w e -l ram.c=5.25g,h_rt=43199 -N phix-t1 -P gentech-rqc.p -js 501 -o $folder/phix-t1.log "$cmd -b --fastq $folder/8972.1.116740.UNKNOWN.fastq.gz --output-path $folder/phix-t1-$DATE"

# HiSeq-2500 1TB: Flowcell C6G2BANXX, lane 2 (Pass, Pass)
qsub -b y -j y -m n -w e -l ram.c=5.25g,h_rt=43199 -N phix-t3 -P gentech-rqc.p -js 501 -o $folder/phix-t1.log "$cmd -b --fastq $folder/8936.2.116238.UNKNOWN.fastq.gz --output-path $folder/phix-t2-$DATE"

# HiSeq Rapid: HF273ADXX, Lane 1 (Pass, Fail)
qsub -b y -j y -m n -w e -l ram.c=5.25g,h_rt=43199 -N phix-t3 -P gentech-rqc.p -js 501 -o $folder/phix-t1.log "$cmd -b --fastq $folder/8891.1.114446.UNKNOWN.fastq.gz --output-path $folder/phix-t3-$DATE"


# /global/projectb/sandbox/rqc/analysis/qc_user/phix/phix-t2-20150417,/global/projectb/sandbox/rqc/analysis/qc_user/phix/phix-t2-20150420
