#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
PhiX Error Detection
- runs on PHiX (control) lanes
- normally lane 8 on Illumina HiSeq, library name starts with "phix"
- logic from Chris Daum on pass or fail for the run (December 2013)

External Dependencies:
bbtools


1.3  - 2014
- replaced gnuplot with matplotlib
- skip steps if the output file exists
- works with gzip files (no need to unzip)

1.4 - 2016-08-17
- use envmodule to run bbtools
- use gzipped fastq for output
- removed duplicate PhiX separation code
- set default to use bbduk to recover Phix to true (-b flag)
- remove adapters after subsampling with bbduk (Alex Spunde's params)

1.5 - 2017-04-14
- got rid of EnvironmentModules & mpld3 imports
- removed gnuplot function (not used)
- added "#bbtools;" to bbtools commands and switched to run_cmd to use on genepool or denovo
- updated to use shifter if on a slurm node (denovo flag)

1.5.1 - 2017-05-11
- fix for "too many cycles"

1.5.2 - 2018-05-08
- fix for incomplete subsample or extracted phix (create tmp.fastq first, then mv if done)

bbmap.sh
The amount of memory bbmap needs depends on the size of the organism.
For phiX or any bacteria, it will use less than 1GB and run on the cluster with any settings.
If you use one of the 16-core cluster nodes, mapping 11 million reads to phiX should take a couple minutes.
On a gpint, it will take longer, but I don't recommend doing mapping on gpints.

recent test case:
$ ./phix_error_detection.py -f /global/projectb/scratch/brycef/phix/10731.8.176252.UNKNOWN.fastq.gz -o /global/projectb/scratch/brycef/phix/t1


Test case:
7348.8.68143.fastq.gz - single read
$ cd /global/projectb/scratch/brycef/tmp2
$ module load pigz
$ unpigz -c /global/dna/dm_archive/sdm/illumina/00/73/48/7348.8.68143.fastq.gz > 7348.8.68143.fastq &
$ ~/code/my-qsub.sh phix1 "/global/homes/b/brycef/git/jgi-rqc-pipeline/phix_error_detection/phix_error_detection.py --output-path /projectb/scratch/brycef/phix/PHIX1 --fastq /global/projectb/scratch/brycef/tmp2/7348.8.68143.fastq"

7508.8.75285.fastq.gz - paired ended
$ unpigz -c /global/dna/dm_archive/sdm/illumina/00/75/08/7508.8.75285.fastq.gz > 7508.8.75285.fastq
$ ~/code/my-qsub.sh phix2 "/global/homes/b/brycef/git/jgi-rqc-pipeline/phix_error_detection/phix_error_detection.py --output-path /projectb/scratch/brycef/phix/PHIX2 --fastq /global/projectb/scratch/brycef/tmp2/7508.8.75285.fastq"

8327.4.98152.UNKNOWN.fastq - Hiseq-2000 1T
$ ~/code/my-qsub.sh phix3 "/global/homes/b/brycef/git/jgi-rqc-pipeline/phix_error_detection/phix_error_detection.py -b --output-path /projectb/scratch/brycef/phix/PHIX3 --fastq /global/projectb/scratch/brycef/tmp2/8327.4.98152.UNKNOWN.fastq"


$ ~/code/my-qsub.sh phix4 "/global/homes/b/brycef/git/jgi-rqc-pipeline/phix_error_detection/phix_error_detection.py -b --output-path /projectb/scratch/brycef/phix/PHIX4 --fastq /global/dna/dm_archive/sdm/illumina/00/93/65/9365.7.130772.UNKNOWN.fastq.gz"

DENOVO 2017-04-14:
# ssh denovo ...
salloc -n 1 -t 30 --mem=3G /bin/bash

PS1='\u@\h:\w\$ '

export PATH=/global/homes/b/brycef/miniconda2/bin:$PATH
source activate qc

cd ~/git/jgi-rqc-pipeline/phix_error_detection
./phix_error_detection.py --fastq $SDM_FASTQ/01/14/88/11488.1.208132.UNKNOWN.fastq.gz --output-path /global/projectb/scratch/brycef/phix/11488

genepool test:
./phix_error_detection.py --fastq $SDM_FASTQ/01/14/88/11488.1.208132.UNKNOWN.fastq.gz --output-path /global/projectb/scratch/brycef/phix/11488g

# need bash script because loading conda qc, export path ...
~/phix.sh
sbatch -t 60 -n 16 --mem=3G --job-name phix-test  ~/phix.sh
-t = time in minutes
-n = cpus
--mem = whole numbers only

sacct = what ran
squeue = cached version of current reservations, handles 20k requests per second

-b yes -j yes -m n -w e -terse -l exclusive.c=1 -l ram.c=3.5G,h_vmem=3.5G,h_rt=18000,s_rt=17995 -pe pe_32 32 -N phix-371038 -P gentech-rqc.p -o $RQC_SCRATCH/pipelines/phix/in-progress/02/94/85/32/phix-2948532.log -js 90

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import numpy
import glob # find files
import getpass

import matplotlib
matplotlib.use("Agg") ## This needs to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import mpld3

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, get_status, run_cmd, checkpoint_step, append_rqc_file, append_rqc_stats, get_read_count_fastq, human_size, get_subsample_rate
from rqc_constants import RQCReferenceDatabases


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions


'''
Subsample fastq to 1% or 250k reads (if less than 250k reads then use all reads)

@param fastq: /path/to/uncompressed/fastq
@param seq_unit_name: fastq name
@param output_path: /path/to/output_folder
@param read_count: number of reads in the fastq (optional)
@param subsample_rate: subsamping rate to use

@return status: new status


'''
def do_subsample(fastq, seq_unit_name, output_path, read_count = 0, subsample_rate = 0.01):

    log.info("do_subsample: %s", seq_unit_name)

    status = "subsample in progress"
    checkpoint_step(status_log, status)

    if read_count == 0:
        read_count = get_read_count_fastq(fastq, log)

    # use read count to determine subsampling rate
    # - if sample_rate * read_count < 250k then calculate a new_subsample_rate
    if read_count > 0:
        new_subsample_rate = get_subsample_rate(read_count)
        if new_subsample_rate != subsample_rate:
            subsample_rate = new_subsample_rate

            log.info("- adjusted subsample rate to %s based on read count", "{:.2%}".format(subsample_rate))
    else:

        status = "subsample failed"
        checkpoint_step(status_log, status)
        log.error("- read count == 0!  Game over man!")
        return status, None


    subsample_fastq = os.path.join(output_path, "%s.subsample.fastq.gz" % (seq_unit_name))
    subsample_tmp_fastq = os.path.join(output_path, "%s.subsample.tmp.fastq.gz" % (seq_unit_name))
    subsample_adapter_fastq = subsample_fastq.replace(".fastq.gz", ".no-adapter.fastq.gz")

    err_log = os.path.join(output_path, "subsample.err")

    exit_code = 0
    if os.path.isfile(subsample_fastq):
        log.info("- skipping subsample, file exists")
    else:

        # new subsampler from BBTOOLS
        # without qin=33 then it uses auto detect, Illumina is phread64 but we need to convert to phred33
        cmd = "#bbtools;reformat.sh in=%s out=%s samplerate=%s qin=33 qout=33 overwrite > %s 2>&1" % (fastq, subsample_tmp_fastq, subsample_rate, err_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        cmd = "mv %s %s" % (subsample_tmp_fastq, subsample_fastq)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        # trim adapters from the ends
        log.info("- trimming adapters from each end")

        frag_adapter = RQCReferenceDatabases.FRAG_ADAPTERS
        stats_file = os.path.join(output_path, "adapter_stats.txt")
        log_file = os.path.join(output_path, "bbduk-adapter.log")

        # bbduk trim params from Alex Spunde - 2016-08-16
        cmd = "#bbtools;bbduk.sh in=%s ref=%s out=%s stats=%s ktrim=r k=23 mink=11 hdist=1 tbo tpe > %s 2>&1" % (subsample_fastq, frag_adapter, subsample_adapter_fastq, stats_file, log_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)




    subsample_read_count = 0

    status = "subsample failed"

    if exit_code == 0:
        status = "subsample complete"

        subsample_size = os.path.getsize(subsample_adapter_fastq)
        log.info("- subsample fastq file size: %s", human_size(subsample_size))

        subsample_read_count = get_read_count_fastq(subsample_adapter_fastq, log)

        # real subsample rate
        subsample_rate = subsample_read_count / float(read_count)

        log.info("- real subsample rate = %s/%s = %s", subsample_read_count, read_count, "{:.2%}".format(subsample_rate))


        append_rqc_stats(rqc_stats_log, "raw read count", read_count)
        append_rqc_stats(rqc_stats_log, "subsample rate", subsample_rate)
        append_rqc_stats(rqc_stats_log, "subsample read count", subsample_read_count)


    else:
        log.error("- subsample failed!")
        subsample_fastq = None

    checkpoint_step(status_log, status)

    return status, subsample_adapter_fastq


'''
Run bbmap and check for output

@param fastq: /path/to/uncompressed/fastq
@param seq_unit_name: fastq name
@param output_path: /path/to/output_folder

@return status: new status

'''
def do_bbmap(fastq, seq_unit_name, output_path):

    log.info("do_bbmap: %s", seq_unit_name)

    status = "bbmap in progress"
    checkpoint_step(status_log, status)

    # output files
    mhist_file = "%s.phixerror.mhist.txt" % (seq_unit_name)
    bbmap_log = os.path.join(output_path, "bbmap-%s.log" % (seq_unit_name))

    # bbmap command
    mhist_file = get_mhist_file(output_path, seq_unit_name)
    exit_code = 0
    if os.path.isfile(mhist_file):
        log.info("- skipping bbmap, found mhist_file")
    else:


        #phix_ref = RQCReferenceDatabases.PHIX_REF
        # Alex S recommends we use a circularlized PhiX ref
        phix_ref = RQCReferenceDatabases.PHIX_CIRCLE_REF

        cmd = "#bbtools;bbmap.sh ref=%s in=%s qin=33 matchhistogram=%s > %s 2>&1"
        cmd = cmd % (phix_ref, fastq, mhist_file, bbmap_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    if exit_code == 0:

        if os.path.isfile(os.path.join(output_path, mhist_file)):
            status = "bbmap complete"

            append_rqc_file(rqc_file_log, "phixerror.mhist.txt", mhist_file)

            append_rqc_stats(rqc_stats_log, "reference", RQCReferenceDatabases.PHIX_REF)

        else:
            status = "bbmap failed"
            log.error("- do_bbmap failed to create %s", mhist_file)

    else:
        status = "bbmap failed"
        log.error("- do_bbmap failure, exit code != 0")


    checkpoint_step(status_log, status)

    return status



'''
create 4 plots

@param seq_unit_name: fastq name
@param output_path: /path/to/output_folder
@param paired_flag: single read = false, paired read = true
@param rqc_file_log: /path/to/file-output-log.tmp

@return new status
'''
def do_plots(seq_unit_name, output_path, paired_flag):

    log.info("do_plots: %s", seq_unit_name)

    status = "plots in progress"
    checkpoint_step(status_log, status)

    plot_cnt = 0 # should have 4 plots


    for i in [1,2,3,4]:


        png_file, html_file, plot_title = make_alignment_plot(seq_unit_name, output_path, paired_flag, i)

        if png_file:
            append_rqc_file(rqc_file_log, plot_title,  png_file)
            plot_cnt += 1

        if html_file:
            append_rqc_file(rqc_file_log, plot_title + "_html",  html_file)


    if plot_cnt == 4:
        status = "plots complete"
    else:
        status = "plots failed"


    checkpoint_step(status_log, status)

    return status



'''
Create the base vs match, sub, ins, del plots for read1 & 2

'''
def make_alignment_plot(seq_unit_name, output_path, paired_flag, chart_id):

    log.info("make_alignment_plot #%s: %s", chart_id, seq_unit_name)

    if chart_id == 1:
        title = "Percent Match vs. Base Position"
        y_label = "Percent Match"
        c1 = 1
        c2 = 7

    if chart_id == 2:
        title = "Percent Substitution vs. Base Position"
        y_label = "Percent Substitution"
        c1 = 2
        c2 = 8

    if chart_id == 3:
        title = "Percent Deletion vs. Base Position"
        y_label = "Percent Deletion"
        c1 = 3
        c2 = 9

    if chart_id == 4:
        title = "Percent Insertion vs. Base Position"
        y_label = "Percent Insertion"
        c1 = 4
        c2 = 10


    plot_title = "%s: %s" % (title, seq_unit_name)

    mhist_file = get_mhist_file(output_path, seq_unit_name)
    png_file = None

    if os.path.isfile(mhist_file):

        # 6 for unpaired
        raw_data = numpy.loadtxt(mhist_file, comments='#') #, usecols=(0,12))

        fig, ax = plt.subplots()

        marker_size = 5.0
        line_width = 1.5



        p1 = ax.plot(raw_data[:,0], raw_data[:,c1]*100, 'r', marker='o', markersize = marker_size, linewidth = line_width, alpha = 0.5, label = 'Read 1')
        if paired_flag == True:
            p2 = ax.plot(raw_data[:,0], raw_data[:,c2]*100, 'g', marker='x', markersize = marker_size, linewidth = line_width, alpha = 0.5, label = 'Read 2')


        plt.title(plot_title)

        font_prop = FontProperties()
        font_prop.set_size("small")
        font_prop.set_family("Bitstream Vera Sans")
        ax.legend(loc=1, prop=font_prop)

        ax.set_xlabel("Base Number", fontsize=12, alpha=0.5)
        ax.set_ylabel(y_label, fontsize=12, alpha=0.5)

        ax.grid(color="gray", linestyle=':')



        png_file = os.path.join(output_path, "%s-%s.png" % (seq_unit_name, chart_id))


        log.info("- created %s", os.path.basename(png_file))

        html_file = png_file.replace(".png", ".html")
        mpld3.save_html(fig, html_file)

        ## Save Matplotlib plot in png format
        plt.savefig(png_file, dpi=fig.dpi)



    return png_file, html_file, title



'''
Calculate avg, stdev for alignment mapping using numpy

@param seq_unit_name: fastq name
@param output_path: /path/to/output_folder
@param paired_flag: single read = false, paired read = true

'''
def do_alignment_stats(seq_unit_name, output_path):

    log.info("do_alignment_stats: %s", seq_unit_name)


    status = "alignment stats in progress"
    checkpoint_step(status_log, status)

    # dictionary of column ids -> definition
    key_dict = {
        1: "Match Read 1",
        2: "Sub Read 1",
        3: "Del Read 1",
        4: "Ins Read 1",
        5: "N Read 1",
        6: "Other Read 1",

        7: "Match Read 2",
        8: "Sub Read 2",
        9: "Del Read 2",
        10: "Ins Read 2",
        11: "N Read 2",
        12: "Other Read 2",
    }


    mhist_file = get_mhist_file(output_path, seq_unit_name)
    if os.path.isfile(mhist_file):
        pass
    else:
        log.info("- missing %s!", mhist_file)
        sys.exit(1)

    align_data = numpy.loadtxt(open(mhist_file, "rb"))

    # get average per column into a list
    avg_list = numpy.average(align_data, 0)

    # determine paired if there are more than 7 columns (1..7 = read1, 8..14 = read2)
    paired_flag = False
    if len(avg_list) > 7:
        paired_flag = True
        log.info("- fastq is paired-ended")

    # zip(*align_data) creates lists out of the columns
    std_list = zip(*align_data)




    # Save averages per column
    cnt = 0
    for avg in avg_list:
        cnt += 1

        if cnt > 1:
            key = "Avg %s" % (key_dict[cnt-1])
            append_rqc_stats(rqc_stats_log, key, avg)

            #ddof = degrees of freedom (1 = population std dev, 0 = sample std dev)
            stdev = numpy.std(std_list[cnt-1], ddof = 1)
            #print "%s = %s" % (cnt, stdev)
            key = "Stdev %s" % (key_dict[cnt-1])

            append_rqc_stats(rqc_stats_log, key, stdev)



    status = "alignment stats complete"
    checkpoint_step(status_log, status)

    return status, paired_flag


'''
Determine if the PhiX run was good or not.
Chris Daum's conditions (2013-12-17)
-Percent Match is < 96% for more than 5 bases, or is < 85% for a single base
-Percent Substitution is > 1% for more than 5 bases, or is > 5% for a single base

@param seq_unit_name: fastq name
@param output_path: /path/to/output_folder
@param paired_flag: single read = false, paired read = true

@return: new status
'''
def check_qc(seq_unit_name, output_path, paired_flag):

    log.info("check_qc: %s", seq_unit_name)

    status = "check qc in progress"
    checkpoint_step(status_log, status)

    # settings for limits
    base_limit = 5

    # cols 1,7
    match_limit = 0.96 # < match_limit for 6+ bases
    match_one_limit = 0.85 # < match_one_limit for single base

    # cols 2,8
    sub_limit = 0.01 # > sub_limit for 6+ bases
    sub_one_limit = 0.05 # > sub_one_limit for single base

    # cols 3,9
    del_limit = 0.01
    del_one_limit = 0.05

    # cols 4,10
    ins_limit = 0.01
    ins_one_limit = 0.05


    mhist_file = get_mhist_file(output_path, seq_unit_name)
    if os.path.isfile(mhist_file):
        pass
    else:
        log.info("- missing %s!", mhist_file)
        sys.exit(1)


    align_data = numpy.loadtxt(open(mhist_file, "rb"))

    align_data_list = zip(*align_data)


    # check percent match column for read 1 (col 1)
    read1_match_bad_cnt, read1_match_pass_flag = check_list(list(align_data_list[1]), ">", match_limit, match_one_limit, base_limit)
    append_rqc_stats(rqc_stats_log, "read1_match_bad_count", read1_match_bad_cnt)


    # check percent substitution for read 1 (col 2)
    read1_sub_bad_cnt, read1_sub_pass_flag = check_list(list(align_data_list[2]), "<", sub_limit, sub_one_limit, base_limit)
    append_rqc_stats(rqc_stats_log, "read1_substitution_bad_count", read1_sub_bad_cnt)

    # check percent deletion for read 1 (col 3)
    read1_del_bad_cnt, read1_del_pass_flag = check_list(list(align_data_list[3]), "<", del_limit, del_one_limit, base_limit)
    append_rqc_stats(rqc_stats_log, "read1_deletion_bad_count", read1_del_bad_cnt)

    # check percent insertion for read 1 (col 4)
    read1_ins_bad_cnt, read1_ins_pass_flag = check_list(list(align_data_list[4]), "<", ins_limit, ins_one_limit, base_limit)
    append_rqc_stats(rqc_stats_log, "read1_insertion_bad_count", read1_ins_bad_cnt)


    # if match or substitution failed, then read1 qc check = FAIL
    if read1_match_pass_flag == False or read1_sub_pass_flag == False or read1_del_pass_flag == False or read1_ins_pass_flag == False:
        append_rqc_stats(rqc_stats_log, "read1_qc_check", "FAIL")
        log.info("- READ 1 QC: FAIL")
    else:
        append_rqc_stats(rqc_stats_log, "read1_qc_check", "PASS")
        log.info("- READ 1 QC: PASS")

    if paired_flag == True:

        # check percent match column for read 2 (col 7)
        read2_match_bad_cnt, read2_match_pass_flag = check_list(list(align_data_list[7]), ">", match_limit, match_one_limit, base_limit)
        append_rqc_stats(rqc_stats_log, "read2_match_bad_count", read2_match_bad_cnt)

        # check percent substitution for read 2 (col 8)
        read2_sub_bad_cnt, read2_sub_pass_flag = check_list(list(align_data_list[8]), "<", sub_limit, sub_one_limit, base_limit)
        append_rqc_stats(rqc_stats_log, "read2_substitution_bad_count", read2_sub_bad_cnt)

        # check percent deletion for read 1 (col 9)
        read2_del_bad_cnt, read2_del_pass_flag = check_list(list(align_data_list[9]), "<", del_limit, del_one_limit, base_limit)
        append_rqc_stats(rqc_stats_log, "read2_deletion_bad_count", read2_del_bad_cnt)

        # check percent insertion for read 1 (col 10)
        read2_ins_bad_cnt, read2_ins_pass_flag = check_list(list(align_data_list[10]), "<", ins_limit, ins_one_limit, base_limit)
        append_rqc_stats(rqc_stats_log, "read2_insertion_bad_count", read2_ins_bad_cnt)



        # if match or substitution failed, then read1 qc check = FAIL
        if read2_match_pass_flag == False or read2_sub_pass_flag == False or read2_del_pass_flag == False or read2_ins_pass_flag == False:
            append_rqc_stats(rqc_stats_log, "read2_qc_check", "FAIL")
            log.info("- READ 2 QC: FAIL")
        else:
            append_rqc_stats(rqc_stats_log, "read2_qc_check", "PASS")
            log.info("- READ 2 QC: PASS")


    status = "check qc complete"
    checkpoint_step(status_log, status)


    return status

'''
Go through a list of numbers and count how many are below (or above) a certain limit

@param my_list: list of floats
@param mode: ">" (greater), "<" (less than)
@param num_limit: limit to be < or > (float)
@param one_limit: pass_flag = False if one in my_list is < or > this (float)
@param base_limit: pass_flag = False if bad_cnt > base_limit

'''
def check_list(my_list, mode, num_limit, one_limit, base_limit):

    mode = mode.lower()

    bad_cnt = 0 # count of items that do not meet the criteria
    bad_one = 0 # count of items that are less than the "one_limit"
    pass_flag = False # False = this list has base_limit bad values or one_limit bad value, True = all values are good
    cnt = 0 # number of items in the list

    # make sure its a list
    if isinstance(my_list, basestring) == False:

        for i in my_list:
            cnt += 1

            # don't count 1st or last read
            if cnt == 1:
                continue
            if cnt == len(my_list):
                continue

            #if not str(i).isdigit():
            #    log.info("- %s is not a digit!", i)
            #    continue # pass to next record

            # good values are greater than num_limit
            if mode == ">":
                if float(i) < num_limit:
                    bad_cnt += 1

                if float(i) < one_limit:
                    bad_one += 1

            # good values are less than num_limit
            if mode == "<":
                if float(i) > num_limit:
                    bad_cnt += 1

                if float(i) > one_limit:
                    bad_one += 1

    #log.info("- total items checked: %s", cnt)
    log.info("- values not %s %s: %s, not %s %s: %s, total: %s", mode, num_limit, bad_cnt, mode, one_limit, bad_one, cnt)

    # must have more than 5 (base_limit) bad values, or one value that is beyond the threshold
    if bad_cnt > base_limit or bad_one > 0:
        pass_flag = False
    else:
        pass_flag = True

    return bad_cnt, pass_flag



'''
Returns full path of match histogram file
'''
def get_mhist_file(output_path, seq_unit_name):

    return os.path.join(output_path, "%s.phixerror.mhist.txt" % (seq_unit_name))

'''
Purge intermediate files
- subsample file only

'''
def do_purge(output_path):

    log.info("do_purge: %s", output_path)

    file_cnt = 0

    file_list = glob.glob(os.path.join(output_path, "*.fastq"))
    file_list = file_list + glob.glob(os.path.join(output_path, "*.fq"))
    file_list = file_list + glob.glob(os.path.join(output_path, "*.fastq.gz"))

    for purge_file in file_list:
        cmd = "rm %s" % (purge_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        file_cnt += 1


    log.info("- purged %s files", file_cnt)
    return file_cnt


'''
For 1T runs, remove the PhiX from UNKNOWN and then use that
- only with the -b option
'''
def separate_phix(fastq, output_path):

    log.info("separate_phix: %s", fastq)


    bbduk_matched_outfile = None
    bbduk_unmatched_outfile = None

    new_fastq = None

    ## RQC-468
    log.info("- Use bbduk to recover the PhiX from the unknown fastq.")

    status = "bbduk in progress"
    checkpoint_step(status_log, status)


    phix_ref = RQCReferenceDatabases.PHIX_REF
    bbduk_matched_outfile = os.path.join(output_path, seq_unit_name + "_matched.fastq.gz")
    bbduk_matched_tmp_outfile = os.path.join(output_path, seq_unit_name + "_matched.tmp.fastq.gz")
    bbduk_unmatched_outfile = os.path.join(output_path, seq_unit_name + "_unmatched.fastq.gz")

    if os.path.isfile(bbduk_matched_outfile):
        log.info("- skipping bbduk splitting step, found file: %s", bbduk_matched_outfile)

    else:
        cmd = "#bbtools;bbduk.sh in=%s ref=%s outm=%s outu=%s" % (fastq, phix_ref, bbduk_matched_tmp_outfile, bbduk_unmatched_outfile)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        cmd = "mv %s %s" % (bbduk_matched_tmp_outfile, bbduk_matched_outfile)
        std_out, std_err, exit_code = run_cmd(cmd, log)
    
    if exit_code == 0:
        ## replace the original fastq with bbduk'd matched fastq
        new_fastq = bbduk_matched_outfile

        status = "bbduk complete"
        checkpoint_step(status_log, status)

    else:
        log.info("bbduk failed!")
        status = "bbduk failed"
        checkpoint_step(status_log, status)
        sys.exit(2)


    return status, new_fastq




## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "RQC Phix Error Detection"
    version = "1.5.2"


    # Parse options
    usage = "prog [options]\n"
    usage += "* PHiX Error Detection, version %s\n" % (version)

    parser = ArgumentParser(usage = usage)

    parser.add_argument("-f", "--fastq", dest="fastq", type=str, help="uncompressed fastq file (full path to fastq)", required = True)
    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-s", "--subsample-rate", dest="subsample", help = "Subsampling rate (default = 0.01 = 1%%)", type = float)
    parser.add_argument("-rc", "--read-count", dest="read_count", help = "read count of fastq", type = int)
    parser.add_argument("-n", "--no-purge", dest="nopurge", default = False, action="store_true", help = "do not remove intermediate files")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument('-v', '--version', action='version', version=version)

    # output path = cwd if no output path (pwd = present working directory = cwd)

    output_path = None
    log_level = "INFO"
    fastq = None
    subsample_rate = 0.01
    read_count = 0
    purge_flag = True
    paired_flag = True # determined in do_alignment_stats step
    uname = getpass.getuser() # for debugging
    print_log = False
    
    args = parser.parse_args()

    print_log = args.print_log

    if args.subsample:
        try:
            subsample_rate = float(args.subsample)
        except:
            subsample_rate = 0.1

    if args.read_count:
        read_count = args.read_count

    if args.output_path:
        output_path = args.output_path

    if args.fastq:
        fastq = args.fastq


    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()

    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    # initialize my logger
    log_file = os.path.join(output_path, "phix_error_detection.log")


    # log = logging object
    log = None

    log = get_logger("phix", log_file, log_level, print_log) 
    

    log.info("%s", 80 * "~")
    log.info("Starting %s: %s", script_name, fastq)


    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "fastq", fastq)
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "subsample_rate", subsample_rate)
    log.info("%25s      %s", "read_count", read_count)
    log.info("%25s      %s", "purge_flag", purge_flag)


    # RQC memory env var - debugging
    rqcmem = os.environ.get('RQCMEM', 0)
    log.info("BBTOOLS RQCMEM = %smb", rqcmem)


    ## Validate fastq exists
    if not os.path.isfile(fastq):
        log.error("Cannot find fastq: %s!", fastq)
        sys.exit(2)

    # create a name by stripping .fastq from the fastq name
    seq_unit_name = os.path.basename(fastq)
    seq_unit_name = seq_unit_name.replace(".gz", "")
    seq_unit_name = seq_unit_name.replace(".fastq", "")

    rqc_file_log = os.path.join(output_path, "rqc-files.tmp")
    rqc_stats_log = os.path.join(output_path, "rqc-stats.tmp")

    # for a seq unit, create 4 charts from Brian Bushnell's tool
    status_log = os.path.join(output_path, "status.log")

    status = get_status(status_log)


    # testing ...
    #check_qc("7508.8.75285", "/global/projectb/scratch/brycef/tmp2/PHIX2", True)
    #make_alignment_plot(seq_unit_name, output_path, True, 1)
    #exit(0)

    done = False
    cycle = 0


    if status == "complete":
        log.info("Status is complete, not processing.")
        done = True

    elif status.endswith("failed"):
        status = "start"


    while done == False:

        cycle += 1

        if status == "start":
            # extract PhiX from unknown library
            status, fastq = separate_phix(fastq, output_path)

            # subsampling and adapter trimming
            status, subsample_fastq = do_subsample(fastq, seq_unit_name, output_path, read_count, subsample_rate)

        if status == "bbduk complete":
            status, subsample_fastq = do_subsample(fastq, seq_unit_name, output_path, read_count, subsample_rate)
            
        if status == "subsample complete":
            status = do_bbmap(subsample_fastq, seq_unit_name, output_path)


        if status == "bbmap complete":
            status, paired_flag = do_alignment_stats(seq_unit_name, output_path)


        if status == "alignment stats complete":
            status = check_qc(seq_unit_name, output_path, paired_flag)

        if status == "check qc complete":
            status = do_plots(seq_unit_name, output_path, paired_flag)



        if status == "plots complete":
            status = "complete"
            done = True

        if status.endswith("failed"):
            done = True

        if cycle > 15:
            done = True
            log.info("- stopping - too many cycles")



    if status == "complete":

        #checkpoint_step(status_log, status)

        # move rqc-files.tmp to rqc-files.txt
        rqc_new_file_log = os.path.join(output_path, "rqc-files.txt")

        cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        rqc_new_stats_log = os.path.join(output_path, "rqc-stats.txt")

        cmd = "mv %s %s" % (rqc_stats_log, rqc_new_stats_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        if purge_flag == True:
            #do_purge(output_path)
            pass

    elif status.endswith("failed"):

        status = "failed"

        checkpoint_step(status_log, status)

        # remove intermediate files (none)
        # do_purge(output_path)

    checkpoint_step(status_log, status)

    log.info("Completed %s: %s", script_name, fastq)

    sys.exit(0)

"""


    ##     ##
   ##  @@@  ##
  ##  @@@@@  ##
  #  @@ @ @@  #
  #@@@@@ @@@@@#
  #@@@@ @ @@@@#
  ##  @@@@@  ##
   ##  @@@  ##
    ##     ##

   Bryce was here ...

"""