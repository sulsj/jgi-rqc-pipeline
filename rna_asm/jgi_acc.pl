#!/usr/bin/env perl
use strict;
use warnings;

use Getopt::Long;
use JGI::Bio::Rnnotator::CalcACC;
use JGI::Bio::Utils::Fastq;
use Bio::SeqIO;
use Pod::Usage;
use POSIX qw/strftime/;

our $VERSION = '1.4.4';

=head1 NAME

jgi_acc.pl

=head1 SYNOPSIS

    jgi_acc.pl [OPTIONS] <contigs.fa>

    OPTIONS:
        -g STR     : genome in FASTA or 2bit format (default: no check for accuracy)
        -o STR     : output directory (default: .)
        -l STP	   : log file name (default: acc.log)
        -n INT     : number of threads (default: 8)
        -m INT     : max. intron size (default: 50000)
        -t STR     : transcripts in FASTA format (default: none)
        -a STR     : gene annotation file (default: none)
        -s INT     : number of nodes to use on the cluster (default: 1)
    
=head2 DESCRIPTION

Computes the accuracy, completeness, and contiguity of a de novo transcriptome assembly.

Accuracy:   % of assembled contigs matching the reference overall.
            % of assembled contigs with > 95% of the assembled bases matching the reference.

Completeness: % of the ref genes that are assembled at > 80% of their length.

Contiguity:  % of the ref genes that are assembled at > 80% of their length continuously.

=head1 CHANGE LOG
V1.4.4 (03-14-2016) XM
    - per Anna, added one more stat line to summarize % mapped contigs overall in the log
V1.4.3 (08-10-2015) XM
    - Updated the summary of contig stats 
V1.4.2 (06-18-2014) XM
    - Removed the --weight option 
    --weight   : flag indicating that accuracy calculations should be weighted by seq. length
V1.4.1 (06-16-2014) XM
    - Calculate accumulated accuracy% if a contig hit multiple genomic contigs for fragmented genome
v1.4 (08-27-2013) XM
    - Minor changes in output
v1.3 (08-7-2013) XM
    - Added an option to output a log file
v1.2.5 (03-1-2013) XM
    - Added codes to create out dir if it doesn't exist, and check if transcript file or gene annotation file exists 
v1.2.4 (11-27-2012) XM
    - Set the floating point precision to 2 digits
v1.2.3 (10-15-2012) XM
    - Added check to make sure ref is available.

=head1 AUTHOR

Jeff Martin <jamartin@lbl.gov>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2011-2013 by JGI

=cut

&run(@ARGV);

sub run {
    my $weighted_accur = 0;
    my $outdir         = '.';
    my $n_threads      = 8;
    my $max_intron     = 50000;
    my $genes_tab      = "";
    my $transcripts_fa = "";
    my $aligner        = "bwa";
    my $mismatches     = 2;
    my $num_nodes      = 1;
    my $genome         = "";
    my $log_file       = "acc.log";
    
    GetOptions(
        "g=s"    => \$genome,
        "m=i"    => \$max_intron,
        "n=i"    => \$n_threads,
        "s=i"    => \$num_nodes,
        "a=s"    => \$aligner,
        "o=s"    => \$outdir,
        "t=s"    => \$transcripts_fa,
        "a=s"    => \$genes_tab,
        "weight" => \$weighted_accur,
        "l=s"    => \$log_file,
    ) || pod2usage();
    my ($contigs_fa) = @ARGV;
    (defined($contigs_fa)) || pod2usage();

    # check if genome exists
    if (length($genome)>0) {
	die("Unable to open genome file: $genome.\n") unless (-e $genome);
    }

    # check if transcript exists
    if (length($transcripts_fa)>0) {
	die("Unable to open transcript file: $transcripts_fa.\n") unless (-e $transcripts_fa);
    }

    # check if gene annotation file exists
    if (length($genes_tab)>0) {
	die("Unable to open gene annotation file file: $genes_tab.\n") unless (-e $genes_tab);
    }
    # make out dir if does not exist
    if ( $outdir ne ".") {
	system("mkdir -p $outdir") unless (-e $outdir);
     }
     
    my $log_fh; 
    (open($log_fh, ">$log_file") || die("Unable to open log file: $log_file\n")) if (length($log_file) > 0);
     
     
    writeToLog($log_fh, "jgi_acc.pl Version: $VERSION\n");
    writeToLog($log_fh,
        "******************************************************************************\n");
    writeToLog($log_fh, "Analysis Parameters:\n");
    writeToLog($log_fh, "Assembly Contig file: $contigs_fa\n");
    writeToLog($log_fh, "Number of threads: $n_threads\n");
    writeToLog($log_fh, "Output directory: $outdir\n");
    writeToLog($log_fh, "Max. intron size: $max_intron\n");
    writeToLog($log_fh, "Accuracy calculations is weighted by sequence length: yes\n") if ($weighted_accur);
    writeToLog($log_fh, "Genome file: $genome\n") if (length($genome)>0);
    writeToLog($log_fh, "Transcript file: $transcripts_fa\n") if (length($transcripts_fa)>0);
    writeToLog($log_fh, "Gene annotation file: $genes_tab\n") if (length($genes_tab)>0);
    writeToLog($log_fh, "Number of nodes to use on the cluster: $num_nodes\n") if ($num_nodes>1);
  
     
    # output files
    my $accur_psl         = "$outdir/accuracy.psl";
    my $comp_psl          = "$outdir/completeness.psl";
    my $cont_psl          = "$outdir/contiguity.psl";
    my $accur_txt         = "$outdir/accuracy.txt";
    my $comp_txt          = "$outdir/completeness.txt";
    my $cont_txt          = "$outdir/contiguity.txt";
    my $gene_overlaps_txt = "$outdir/gene_overlaps.txt";

    # run
    my $cAcc = new JGI::Bio::Rnnotator::CalcACC(
        contigs         => $contigs_fa,
        transcripts     => $transcripts_fa,
        genome          => $genome,
        gene_annotation => $genes_tab,
        max_intron      => $max_intron,
        nThreads        => $n_threads,
        outdir          => $outdir,
        weighted_accur  => $weighted_accur,
        num_nodes       => $num_nodes,
    );

    # stats
    my ($num_seqs, $total_length, $n50, $min, $max, $short_cnt, $med_cnt, $long_cnt, $median_len) =
      calcContigStatsMinLen($contigs_fa, 0);
      
    writeToLog($log_fh, "Contig stats:\n");
    
    writeToLog($log_fh, "\tnumber of sequences: $num_seqs\n");
    writeToLog($log_fh, "\tnumber of total bases: $total_length\n");
    writeToLog($log_fh, "\tN50: $n50\n");
    writeToLog($log_fh, "\tmedian length: $median_len\n");
    writeToLog($log_fh, "\tshortest contig: $min\n");
    writeToLog($log_fh, "\tlongest contig: $max\n");
    writeToLog($log_fh, "\t# of long contigs (>= 1000 bp): $long_cnt\n");
    writeToLog($log_fh, "\t# of medium contigs (>= 500 bp and < 1000 bp): $med_cnt\n");
    writeToLog($log_fh, "\t# of short contigs (< 500 bp): $short_cnt\n");
    
    my ($num_seqs_tmp, $num_bases_tmp, $num_seqs_with_n, $num_n_bases) = getNStats($contigs_fa);
    writeToLog($log_fh, "\tnumber of contigs with N: $num_seqs_with_n\n");
    writeToLog($log_fh, "\tnumber of N bases: $num_n_bases\n");

    # acc
    if (length($genome) > 0) {
        my ($accuracy_good, $total_contigs) = $cAcc->accuracy($accur_psl, $accur_txt);
        if ($weighted_accur) {
            writeToLog($log_fh, "Weighted accuracy of contigs: ", $accuracy_good, " %\n");
        }
        else {
	    writeToLog($log_fh, "Accuracy - Fraction of contigs mapping to the genome : $total_contigs/$num_seqs = ");
	    printf($log_fh " %.2f%%\n", $total_contigs / $num_seqs * 100);
            writeToLog($log_fh, "Accuracy - Fraction of contigs mapping to the genome with high accuracy (thresh = 95 % of contig): $accuracy_good/$total_contigs = ");
	    printf($log_fh " %.2f%%\n", $accuracy_good / $total_contigs * 100);	    
        }
    }
    if (length($transcripts_fa) > 0) {
        my ($completeness_good, $total_genes) = $cAcc->completeness($comp_psl, $comp_txt);
        writeToLog($log_fh, "Completeness - Fraction of genes completely covered by contigs (thresh = 80 % of gene): $completeness_good/$total_genes = ");
 	printf($log_fh "%.2f%%\n", $completeness_good / $total_genes * 100);

        my ($contiguity_good, $total_genes2) = $cAcc->contiguity($cont_psl, $cont_txt);
        writeToLog($log_fh, "Contiguity - Fraction of genes completely covered by a single contig (thresh = 80 % of gene): $contiguity_good/$total_genes2 = ");
        printf($log_fh "%.2f%%\n", $contiguity_good / $total_genes2 * 100);
    }
	
    if (length($genes_tab) > 0) {
        my ($num_no_gene_contigs, $num_multi_gene_contigs) =
          $cAcc->genesPerContig($accur_txt, $gene_overlaps_txt);
          writeToLog($log_fh, "Number of contigs containing less than 50% of a gene: $num_no_gene_contigs\n");
          writeToLog($log_fh, "Number of contigs containing more than 50% of multiple genes: $num_multi_gene_contigs\n");
    }
    
    #close the log file
    close($log_fh);
}


sub getNStats {
    my ($fa) = @_;

    my $num_seqs        = 0;
    my $num_bases       = 0;
    my $num_seqs_with_n = 0;
    my $num_n_bases     = 0;
    my $in_ob           = Bio::SeqIO->new(-file => $fa, '-format' => 'Fasta');
    while (my $seq = $in_ob->next_seq()) {
        $num_seqs++;
        $num_bases += length($seq->seq());

        my $num_n = ($seq->seq() =~ tr/nN//);
        $num_n_bases += $num_n;
        $num_seqs_with_n += ($num_n ? 1 : 0);
    }

    return ($num_seqs, $num_bases, $num_seqs_with_n, $num_n_bases);
}

# Writes the current time and a given text string to the log file.
# 	@param $text - the text string
# 	@returns - (nothing)
sub writeToLog($$) {
    my ($log_ref, $text) = @_;
    print $log_ref strftime('%d-%b-%Y %H:%M', localtime), " $text" if (defined($log_ref));
}


