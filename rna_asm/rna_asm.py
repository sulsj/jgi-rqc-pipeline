#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
RNA seq assembly pipeline
https://issues.jgi-psf.org/browse/RQC-745
MUST use RQC filtered fastq as the fastq input!

command line example:
qsub -cwd -b y -w e -V -j y -m beas -M ALipzen@lbl.gov -l high.c,ram.c=30G,h_rt=120:00:00 -pe pe_slots 4 -N DicRnnAssy -P gentech-rna.p 'module unload gaag; module unload samtools; module load /global/homes/x/xmeng/modules/rnnotator/rnnotator; rnnotator.pl -strP 195 /global/dna/dm_archive/rqc/filtered_seq_unit/00/00/97/04/9704.1.141517.ACAGTG.anqrpht.fastq.gz --keep_rundir -n 4 -trim off -adapter off -low_qual off -g /global/dna/shared/rqc/sequence_repository/Dichomitus_squalens_CBS463.89/Dichomitus_squalens_CBS463.89.reference_genome_draft.1075372
~~~~~~~~~~~~~~~~~~~~~~~~~~


    Version 1.1.0
    Shijie Yao
    11/28/2015 - initial implementation; Trinity v2.1.1
    03/24/2016 - use logger to switch log to file or stdout; change tab to 4 spaces; deal with genome='None' case
    10/10/2016 - update email : rm timestamp; add seq_proj_name / library_name
    02/24/2017 - use Trinity v2.3.2 - revert back to 2.1.1 as 2.3.2 didn't pass test
    12/08/2017 - works on denovo (qc_user, qaqc conda env)
    05/22/2018 - if Trinity fail, then entropy-filter the input and rerun Trinity

    run examples:
        rna_asm.py -o test -f /global/dna/shared/rqc/filtered_seq_unit/00/01/08/98/10898.8.183776.TGACCA.filter-RNA.fastq.gz -a trinity -e syao
'''


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
import shutil
from argparse import ArgumentParser
import MySQLdb

# custom libs in '../lib/'
CUR_DIR = os.path.dirname(os.path.realpath(__file__))   # so will work even file is linked
sys.path.append(os.path.join(CUR_DIR, '../lib'))
sys.path.append(os.path.join(CUR_DIR, '../tools'))

from mail import send_email
from common import get_logger, get_status, run_command, human_size, checkpoint_step, append_rqc_stats, get_run_path, get_read_count_fastq

from rqc_fastq import read_length_from_file
from rqc_constants import RQCCommands, RQCConstants
from db_access import jgi_connect_db

## TODO: set DEBUG to False for production release!!
DEBUG = False

PIPE_START = 'start'

INIT_START = 'start initialization'
INIT_END = 'end initialization'
ASM_START = 'start assembly'
ASM_END = 'end assembly'
FILTER_START = 'start filter'
FILTER_END = 'end filter'
POST_START = 'start postprocessing'
POST_END = 'end postprocessing'
JAMO_START = 'start jamo submission prep'
JAMO_END = 'end jamo submission prep'

PIPE_COMPLETE = 'complete'

STEP_ORDER = {PIPE_START : 0,
              INIT_START : 10,
              INIT_END : 20,
              ASM_START : 50,
              ASM_END : 60,
              FILTER_START : 70,
              FILTER_END : 80,
              POST_START : 90,
              POST_END : 100,
              JAMO_START : 110,
              JAMO_END : 120,
              PIPE_COMPLETE : 1000}

# the following constatns are specific to THIS pipeline
STATUS_LOG = 'status.log'
PURGE_ITEMS = []
JOB_STATS = {}

DOCKER_TRINITY='registry.services.nersc.gov/jgi/trinity'
DOCKER_EXT='Trinity'

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

def get_db():
    db = None
    if DEBUG:
        print('DEBUG DB: rqc_dump.')
        db = jgi_connect_db("rqc-dev")
    else:
        print('PROD DB: rqc.')
        db = jgi_connect_db("rqc")

    return db

def add_stats(kname, value):
    'save stats to file and the dictionary'
    append_rqc_stats(RQC_STATS_LOG, kname, value)
    JOB_STATS[kname] = value

def load_stats():
    'load content in the job stats file into the JOB_STATS dictionaly'
    if os.path.isfile(RQC_STATS_LOG):
        with open(RQC_STATS_LOG, 'r') as fileh:
            for line in fileh:
                line = line.strip()
                tok = line.split('=')
                if len(tok) == 2:
                    JOB_STATS[tok[0].strip()] = tok[1].strip()

def write_stats():
    'write data in JOB_STATS dictionary to the job stats file'
    with open(RQC_STATS_LOG, 'w') as fileh:
        for kname in JOB_STATS.keys():
            buffer = '%s = %s\n' % (kname, JOB_STATS[kname])
            fileh.write(buffer)

# checkpoint logging
def checkpoint(step, last_step=PIPE_START):
    'update the pipeline check point log with the new step (step). last_step is the last pipeline step from the pipeline checkpoint file'
    if step == PIPE_START or STEP_ORDER[step] > STEP_ORDER[last_step]:
        checkpoint_step(STATUS_LOG, step)

'''
Assemble with rnnotator

@param output_path: path to write all output for the pipeline
@param fastqs: list of /path/to/fastq
@param refgenome: reference genome fasta file
@param insert: fragment library insert size

@return status: new status

'''
def do_assembly(last_step, oput_path, fastqs, refgenome, insert=195):
    'assemble input fastq(s)'
    do_log(ASM_START.upper())

    err = None

    if STEP_ORDER[last_step] <= STEP_ORDER[ASM_START]:
        checkpoint(ASM_START, last_step)
        last_step = ASM_START

        do_log('do_assembly on file(s) : %s' % ', '.join(fastqs))
        if refgenome:
            do_log('ref genome : %s' % refgenome)

        acmd = 'module unload gaag;module unload samtools;module load /global/homes/x/xmeng/modules/rnnotator/rnnotator;'
        acmd += 'rnnotator.pl -strP %d %s --keep_rundir -n 4 -trim off -adapter off -low_qual off -o %s' % (insert, ' '.join(fastqs), oput_path)
        if refgenome:
            acmd += ' -g %s' % refgenome

        do_log('  - run cmd: %s' % acmd)

        std_out, std_err, exit_code = run_command(acmd, True)

        if exit_code == 0:
            #TODO: anything need to do with successful run
            do_log('  - cmd SUCCESS')
            pass
        else:
            err = 'assembly failed'

        if not err:
            checkpoint(ASM_END, last_step)
            last_step = ASM_END
            do_log(last_step.upper())
        else:
            do_log(err, True)
    else:
        do_log('  - no need to perform this step')

    return err, last_step

'''
Assemble with trinity

@param oput_path: path to write all output for the pipeline
@param infiles: list of /path/to/files

@return status: new status

'''
def do_trinity(last_step, oput_path, infiles, refgenome):
    'assemble input fastq(s)'
    do_log(ASM_START.upper())

    FASTA_SUFIX = ['fasta', 'fna', 'fa']

    err = None
    contigs = None

    if STEP_ORDER[last_step] <= STEP_ORDER[ASM_START]:
        checkpoint(ASM_START, last_step)
        last_step = ASM_START
        do_log('do_assembly on file(s) : %s' % ', '.join(infiles))

        mem_max = 10    # jellyfish max mem     ## TODO - to finalize the value
        dtype = 'fq'
        mpath = 95      # min percent identity for two paths to be merged into single paths; default 98
        paired = True   ## TODO - should we check the input for this?
        cpu = 8         ## TODO - to finalize the value

        if infiles[0].split('.')[-1] in FASTA_SUFIX:
            dtype = 'fa'

        host = os.environ.get('NERSC_HOST', 'unknown')
        acmd = None
        if host == 'genepool':
            grid_exec = '/global/projectb/sandbox/rnaseq/projects/qex'
            acmd = 'module unload bowtie samtools samtools trinity; module load bowtie2/2.0.2 samtools/1.2 trinity/2.3.2;'
            aopt = '--grid_exec %s --max_memory %dG --jaccard_clip --seqType %s' % (grid_exec, mem_max, dtype)
            if paired:
                aopt += ' --run_as_paired'
            aopt += ' --CPU %d --min_per_id_same_path %d' % (cpu, mpath)
            aopt += ' --bflyHeapSpaceMax 1G --bflyHeapSpaceInit 128M --bflyGCThreads 1 --grid_node_max_memory 1G --normalize_reads --single'

            acmd += 'Trinity %s %s' % (aopt, ','.join(infiles))
        elif host in ['denovo', 'cori']:
            grid_exec = os.path.join(os.path.dirname(__file__), 'qex_SLURM')

            ##- has to use conda qaqc of qc_user! samtools and bowtie2 are installed there
            acmd = 'module unload perl; module load trinity/2.3.2;'
            aopt = '--grid_exec %s --max_memory %dG --jaccard_clip --seqType %s' % (grid_exec, mem_max, dtype)
            if paired:
                aopt += ' --run_as_paired'
            aopt += ' --CPU %d --min_per_id_same_path %d' % (cpu, mpath)
            #aopt += ' --bflyHeapSpaceMax 1G --bflyHeapSpaceInit 128M --bflyGCThreads 1 --grid_node_max_memory 1G --normalize_reads --single'
            # 20180409 - match up the update in the config file (/global/dna/shared/rqc/utils/grid_conf_SLURM.tx)
            aopt += ' --bflyHeapSpaceMax 2G --bflyHeapSpaceInit 128M --bflyGCThreads 1 --grid_node_max_memory 10G --normalize_reads --single'

            acmd += 'Trinity %s %s' % (aopt, ','.join(infiles))

        if acmd:
            do_log('  - run cmd: %s' % acmd)

            std_out, std_err, exit_code = run_command(acmd, True)
            # 20161110 - keep Trinity run log into a file
            fname = os.path.join(oput_path, 'trinity_run.log')
            fh = open(fname, 'a')
            fh.write('Trinity run output:\n')
            fh.write(std_out)
            fh.write('\n\n')
            fh.close()

            if exit_code == 0:
                contigs = os.path.join(oput_path, 'trinity_out_dir/Trinity.fasta')  ## TODO: the trinity 2.3.2 produced final contig file
            else:
                err = 'assembly failed: [err=%s]' % (std_err)
        else:
            err = 'Unsupported host : %s' % host

        if not err:
            checkpoint(ASM_END, last_step)
            last_step = ASM_END
            do_log(last_step.upper())
    else:
        do_log('  - no need to perform this step')
        contigs = os.path.join(oput_path, 'trinity_out_dir/Trinity.fasta')  ## TODO: the trinity 2.3.2 produced final contig file
        if not os.path.isfile(contigs):
            err = 'trinity contigs files [%s] not found' % contigs

    return err, last_step, contigs


'''
'''
def do_filter(last_step, oput_path):
    'do filter'
    do_log(FILTER_START.upper())
    checkpoint(FILTER_START, last_step)

    err = None
    #oput_path = os.path.abspath(oput_path)

    if STEP_ORDER[last_step] <= STEP_ORDER[FILTER_START]:
        last_step = FILTER_START
        do_log('  - do filter')

        # TODO: add code to do filter here
        if not err:
            checkpoint(FILTER_END, last_step)
            last_step = FILTER_END
            do_log(last_step.upper())
        else:
            do_log(err, True)
    else:
        do_log('  - no need to perform this step')
        last_step = FILTER_END

    return err, last_step

'''
'''
def do_post(last_step, oput_path, contigs, refgenome):
    'do post assembly'
    do_log(POST_START.upper())


    err = None

    ## TODO: this is only valid for trinity assembler, need to check and set for rnnotator?
    acclog = os.path.join(oput_path, 'acc.log')
    accura = os.path.join(oput_path, 'accuracy.txt')

    if STEP_ORDER[last_step] <= STEP_ORDER[POST_START]:

        checkpoint(POST_START, last_step)
        last_step = POST_START
        do_log('  - do post processing')

        do_log('    - Run acc on trinity contig ...')

        if contigs:
            if os.path.isfile(contigs):
                do_log('     - contigs file : %s' % contigs)

                host = os.environ.get('NERSC_HOST', 'unknown')
                acmd = ''
                if host == 'genepool':
                    acmd = 'module load jgibio; module load blat; '
                    if refgenome:
                        do_log('     - check contig quality and genome coverage')
                        acmd += 'jgi_acc.pl -g %s %s' % (refgenome, contigs)
                    else:
                        do_log('     - check contig quality')
                        acmd += 'jgi_acc.pl %s' % contigs

                    do_log('     - run cmd: %s' % acmd)
                    std_out, std_err, exit_code = run_command(acmd, True)
                    if exit_code != 0:
                        err = 'jgi_acc.pl failed: [err=%s][out=%s]' % (std_err, std_out)
                elif host in ['denovo', 'cori']:
                    # JGI::Bio module is in qc_user's qaqc now!
                    # $ export PATH=~qc_user/miniconda/miniconda2/bin:$PATH
                    # $ source activate qaqc

                    acmd_list = [
                                #'export PERL5LIB=$PERL5LIB:/global/homes/s/syao/src/jgi-rqc-pipeline/rna_asm/perl5',
                                 'module unload perl',
                                 'source activate qaqc',
                                ]
                    acc_cmd = os.path.join(os.path.dirname(__file__), 'jgi_acc.pl')
                    if refgenome:
                        do_log('     - check contig quality and genome coverage')
                        # need blat, which is in the qc_user qaqc env. produce both acc.log and rqc_files.txt
                        acmd += '%s -g %s %s' % (acc_cmd, refgenome, contigs)
                    else:
                        do_log('     - check contig quality')
                        acmd = '%s %s' % (acc_cmd, contigs) # only produce acc.log
                    acmd_list.append(acmd)

                    acmd = ';'.join(acmd_list)
                    do_log('     - run cmd: %s' % acmd)
                    std_out, std_err, exit_code = run_command(acmd, True)
                    if exit_code != 0:
                        err = 'CMD [%s] failed: [err=%s][out=%s]' % (acmd, std_err, std_out)
            else:
                do_log(' - contigs file not found : %s' % contigs)
        else:
            err = 'contigs file not passed in!'



        ##-- move un-needed files/dir to the temp dir. The following list is file that not moved (v2.3.2)
        keepfile = ['trinity_run.log', 'assembly.log', 'final_contigs.fa', 'status.log', 'rqc_rnaseq_asm.log', 'trinity_out_dir', 'acc.log', 'accuracy.txt', 'rqc_files.txt']

        dst = os.path.join(oput_path, 'assembly_supplementary')
        if not os.path.isdir(dst):
            os.mkdir(dst)

        files = os.listdir(oput_path)
        for f in files:
            if not f in keepfile:
                try:
                    shutil.move('%s/%s' % (oput_path, f), dst)
                except:
                    do_log('failed to move file %s to %s' % (f, dst))


        if not err:
            # create the file-list file
            fname = os.path.join(oput_path, 'rqc_files.txt')
            fh = open(fname, 'w')
            if os.path.isfile(acclog):
                fh.write('accLog=%s\n' % acclog)
            if os.path.isfile(accura):
                fh.write('accuracyLog=%s' % accura)
            fh.close()

            checkpoint(POST_END, last_step)
            last_step = POST_END
            do_log(last_step.upper())
        else:
            do_log(err, True)
    else:
        do_log('  - no need to perform this step')

    return err, last_step, acclog

def do_log(msg, err=False):
    'log the msg to the log file, and also print to std out. if log is an error msg, exit.'

    if not err:
        LOG.info(msg)
    else:
        msg = 'Error: %s' % msg
        LOG.error(msg)

    print(msg)
    if err:
        sys.exit(1)

def rm_cmd(items):
    'delete the given items [string] from disk'
    acmd = 'rm -rf %s' % items
    do_log('  - cmd = %s' % acmd)
    std_out, std_err, exit_code = run_command(acmd, True, LOG)

    if not items or exit_code > 0:
        do_log('  - remove failed')
        return False
    else:
        do_log('  - remove completed')
        return True

def entropy_filter(fastqs, output_path):
    do_log('Do entropy filter on input file : ')
    oflist = []
    for fq in fastqs:
        ofile = os.path.basename(fq)
        tok = ofile.split('.')
        pos = -1
        if tok[-1] == 'gz':
            pos = -2
        
        tok.insert(pos, 'entropy-filtered')
        ofile = os.path.join(output_path, '.'.join(tok))
        oflist.append(ofile)
        
        cmd = 'shifter --image=bryce911/bbtools:latest bbduk.sh in=%s out=%s entropy=0.44 entropyk=2 entropywindow=40' % (fq, ofile)
        do_log(cmd)
        std_out, std_err, exit_code = run_command(cmd, True, LOG)
        if exit_code != 0:
            do_log('  - failed to entropy filter %s' % fq)
            
    return oflist

def driver(args, output_path, tag='Initial Run'):
    'the driver funtion'
    # variable initialization
    fastq = None # list of /full/path/to/fastq files
    fastq_name = None # simple fastq name
    insert_size = 195

    acctxt = ''
    if args.fastq:
        fastq = args.fastq

    ## Validate output dir and fastq
    if not args.fastq:
        do_log('Missing fastq file(s)!', True)
    else:
        fastq = args.fastq.strip().split(',')
        fastq_name = []

    for i, ifile in enumerate(fastq):
        if not os.path.isfile(ifile):
            do_log('Cannot find fastq: %s!' % ifile, True)
        if os.path.islink(ifile):
            fastq[i] = os.readlink(ifile)
        else:
            fastq[i] = os.path.abspath(ifile)
        fastq_name.append(os.path.basename(fastq[i]))

    if args.insert:
        try:
            subsampinsert_size = int(args.insert)
        except:
            do_log('Wrong insert size param []!', True)

    DASH_LEN = 100
    do_log('Job settings:')
    do_log('-' * DASH_LEN)
    do_log('%20s      %s' % ('fastq', fastq[0]))
    if len(fastq) > 1:
        for f in fastq[1:]:
            do_log('%20s      %s' % ('', f))
    do_log('%20s      %s' % ('output_path', output_path))
    do_log('%20s      %s' % ('assembler', args.assembler))
    if args.assembler == 'rnnotator':
        do_log('%20s      %s' % ('fragment size', insert_size))

    if args.genome:
        do_log('%20s      %s' % ('genome_ref', args.genome))
    else:
        do_log('%20s      %s' % ('genome_ref', 'None'))

    do_log('%20s      %s' % ('do entropy-filter', args.entropy))
    do_log('%20s      %s' % ('run type', tag))
    do_log('-' * DASH_LEN)
    do_log('')

    last_step = get_status(STATUS_LOG, LOG)

    done = False
    cycle = 0 # number of steps ran
    cycle_max = 1
    infastq = fastq

    # set umask
    umask = os.umask(RQCConstants.UMASK)

    if last_step == 'complete':
        do_log('Status is already in complete state, do nothing.')
        sys.exit(0)

    #load_stats()    # in case of a rerun
    do_log('SARTING step @ %s' % last_step)
    gref = args.genome
    while done == False:
        cycle += 1
        do_log('[ Attempt %d ]' % cycle)

        if cycle > 1:
            last_step = get_status(STATUS_LOG, LOG)

        if last_step != PIPE_COMPLETE:
            if gref:
                if gref == 'None':
                    gref = None
                elif not os.path.isfile(gref):
                    do_log('the given reference genome does not exist [%s]' % gref, True)

            if args.assembler == 'rnnotator':
                error, last_step = do_assembly(last_step, output_path, fastq, gref, insert_size)
            elif args.assembler == 'trinity':
                if args.entropy:
                    fastq = entropy_filter(fastq, output_path)
                error, last_step, contigs = do_trinity(last_step, output_path, fastq, gref)

            if args.assembler == 'trinity' and not args.entropy and error:
                # try entropy filter and then run trinity
                do_log('  - trinity (without entropy filter) failed : %s' % error)
                trinitydir = os.path.join(output_path, 'trinity_out_dir')
                if os.path.isdir(trinitydir):   # must be true!
                    do_log('  - delete the trinity run folder (%s) so we can have a clean rerun.' % trinitydir)
                    shutil.rmtree(trinitydir)
                do_log('  - turn on entropy filter and rerun')
                args.entropy = True
                driver(args, output_path, 'Self Rerun')
                
                # try entropy filter and then run trinity
                # do_log('  - trinity (without entropy filter) failed : %s' % error)
                # 
                # trinitydir = os.path.join(output_path, 'trinity_out_dir')
                # trinitydir2 = os.path.join(output_path, 'trinity_out_dir_failed')
                # #shutil.rmtree(trinitydir)
                # os.rename(trinitydir, trinitydir2)
                # do_log('  - renamed the failed trinity dir %s to %s, and entropy filter the raw input and then rerun Trinity ...' % (trinitydir, trinitydir2))
                # 
                # fastq = entropy_filter(fastq, output_path)
                # error, last_step, contigs = do_trinity(last_step, output_path, fastq, gref)
                
            # post processing
            if not error:
                if args.assembler == 'rnnotator':
                    error, last_step, acclog = do_post(last_step, output_path, None, None)
                else:
                    error, last_step, acclog = do_post(last_step, output_path, contigs, gref)
            else:
                do_log('  -%s' % error, True)

            if not error:
                done = True
                last_step = PIPE_COMPLETE
                #write_stats()  # re-generate the stats file - in case of rerun

        # don't cycle more than cycle_max times ...
        if not done and cycle >= cycle_max:
            do_log('[ Meet the max number of attempts allowed (%d). Quit ]' % cycle_max)
            done = True

    if last_step == PIPE_COMPLETE:
        lines = ['ran_asm pipeline on %s has been completed.\n' % ','.join(fastq_name)]
        # database connection
        db = get_db()

        if db == None:
            lines('Failed to open RQC database connection\n')
        else:
            sth = db.cursor(MySQLdb.cursors.DictCursor)
            # use 1st seq unit for seq proj name and library name
            sql = '''SELECT
                        L.seq_proj_name,
                        L.seq_proj_id,
                        S.library_name
                    FROM seq_units S INNER JOIN library_info L ON S.library_name=L.library_name
                        INNER JOIN seq_unit_file F on F.seq_unit_id = S.seq_unit_id
                    WHERE F.file_name = %s
            '''
            #print('DEBUG sql=%s' % (sql % fastq_name[0]))
            sth.execute(sql, (fastq_name[0], ))
            rs = sth.fetchone()
            spid = 'NOT IN RQC DB'
            seqprojname = 'NOT IN RQC DB'
            libname = 'NOT IN RQC DB'
            if rs:
                spid = rs['seq_proj_id']
                seqprojname = rs['seq_proj_name']
                libname = rs['library_name']

            lines.append('Sequencing Project ID : %s' % spid)
            lines.append('Sequencing Project Name : %s' % seqprojname)
            lines.append('Library Name : %s\n' % libname)
            lines.append('Reference Genome : %s\n' % str(gref))

        if os.path.isfile(acclog):
            # acc.log lines in format of "08-Oct-2016 11:14   median length: 1466"; remove the leading 18 chars
            RUN_PATH = '/global/projectb/scratch/qc_user/rqc/prod/pipelines/rna_asm/in-progress'
            ARC_PATH = '/global/dna/shared/rqc/pipelines/rna_asm/archive'
            with open(acclog, 'r') as fh:
                for lin in fh:
                    lin = lin.rstrip()
                    if lin.find(RUN_PATH) > -1:
                        lin = lin.replace(RUN_PATH, ARC_PATH)   # points to the final archive location

                    if len(lin) > 18:
                        lin = lin[18:]
                    lines.append(lin)


        ebody = '\n'.join(lines)
        send_notification_email(args.eaddr, ','.join(fastq_name), ebody)

        do_log('Pipeline Completed')
        checkpoint(PIPE_COMPLETE)

    else:
        do_log('Failed %s: %s' % (__file__, fastq_name))

    os.umask(umask)

    sys.exit(0)

def send_notification_email(addresses, subtxt, ebody):
    if addresses:
        enames = addresses.split(',')
        toaddr = []
        for sendTo in enames:
            if sendTo.find('@') == -1:
                sendTo = '%s@lbl.gov' % sendTo
                toaddr.append(sendTo)
            subj = 'rna asm pipeline done - %s' % subtxt
            send_email(sendTo, subj, ebody)

        do_log('Notification sent to %s' % ';'.join(toaddr))

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program
'''
required params:
--fastq RQC filtered fastq file
--type :
'''


if __name__ == '__main__':

    NAME = 'RNASeq Assembler with Trinity 2.3.2'
    VERSION = '1.1.0'


    # Parse options
    USAGE = 'prog [options]\n'
    USAGE += '* RNASeq Assembly Pipeline, version %s : %s\n' % (VERSION, NAME)

    # JOB_CMD += '/global/homes/b/brycef/git/jgi-rqc/framework/test/pipeline_test.py --fastq %s --output-path %s'
    JOB_CMD = ' '.join(sys.argv)
    PARSER = ArgumentParser(usage=USAGE)

    ASM_TYPE = ['trinity', 'rnnotator']
    PARSER.add_argument('-f', '--fastq', dest='fastq', type=str, help='RQC filtered fastq files (csv) (full path to fastq)', required=True)
    PARSER.add_argument('-o', '--output-path', dest='output_path', help='Output path to write to, uses pwd if not set')
    PARSER.add_argument('-g', '--genome-ref', dest='genome', help='genome reference fasta files')
    PARSER.add_argument('-a', '--assembler', dest='assembler', choices=ASM_TYPE, help='assembler to use', required=True)
    PARSER.add_argument('-e', '--email', dest='eaddr', help='email adress for lbl.gov user with only the name part. csv')
    PARSER.add_argument('-is', '--insert-size', dest='insert', default=195, help='The fragment insert size for rnnotator assembler. Default to 195')
    PARSER.add_argument('-n', '--no-purge', dest='nopurge', default=False, action='store_true', help='do not remove intermediate files')
    PARSER.add_argument('-entropy', '--entropy-filter', dest='entropy', default=False, action='store_true', help='do entropy filtering on the input reads')
    PARSER.add_argument('-v', '--version', action='version', version=VERSION)


    ARGS = PARSER.parse_args()
    # use current directory if no output path
    if ARGS.output_path:
        OUTPUT_PATH = ARGS.output_path
    else:
        OUTPUT_PATH = os.getcwd()

    OUTPUT_PATH = os.path.abspath(OUTPUT_PATH)

    # create output_directory if it doesn't exist
    if not os.path.isdir(OUTPUT_PATH):
        #do_log('Cannot find %s, creating as new', OUTPUT_PATH)
        os.makedirs(OUTPUT_PATH)

    # create a name by stripping .fastq from the fastq name
    #RQC_STATS_LOG = os.path.join(OUTPUT_PATH, 'rqc-stats.txt')

    # initialize my logger
    log_level = 'INFO'
    LOG_FILE = os.path.join(OUTPUT_PATH, 'rqc_rnaseq_asm.log')
    LOG = get_logger('rna_asm', LOG_FILE, log_level, DEBUG)

    # move into the working dir
    os.chdir(OUTPUT_PATH)
    
    do_log(80 * '~')
    do_log('Command : %s' % JOB_CMD)
    do_log('')
    
    driver(ARGS, OUTPUT_PATH)
