#!/bin/bash

usage(){
echo "
scripts to run rna_asm.py on denovo/cori
Last modified March 27, 2018
"
}

pushd . > /dev/null
DIR="${BASH_SOURCE[0]}"
while [ -h "$DIR" ]; do
  cd "$(dirname "$DIR")"
  DIR="$(readlink "$(basename "$DIR")")"
done

cd "$(dirname "$DIR")"
DIR="$(pwd)"
popd > /dev/null

set=0

#if [ -z "$1" ] || [[ $1 == -h ]] || [[ $1 == --help ]]; then
	#usage
	#exit
#fi

# set up qc_user's qaqc conda
export PATH=/global/projectb/sandbox/rqc/qc_user/miniconda/miniconda2/bin:$PATH
source activate qaqc

CMD="$DIR/rna_asm.py $@"
$CMD 
