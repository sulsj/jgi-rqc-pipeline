#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
LMP Insert Size
- using all reads to watch that the "innies" (inward reads) at the 250bp region do not get too excessive
- Vasanth uses this pipeline's output

Dependencies
bbtools
megahit

1.0.0 - initial version
1.0.1 - added median, mode per Alicia
1.0.2 - tight insert calculation for tight insert libraries (400 or 800 bp)
1.1 - changed from gnuplot to matplotlib (nicer)
1.1.1 - tight insert use peak: how much within +/- 15% of mode, mode should be 10% of target
1.2 - add insert_size as parameter, updated charts to show shaded regions & annotations, removed alt tight insert calculation

v 1.2.1: 2016-06-29
- need clean subpath for megahit
- removed output_path from function definitions

v 1.2.2: 2016-10-14
- fix for getting the biggest contig.  Was using the wrong field in stats.sh

v 1.2.3: 2017-06-05
- updated to work on Cori
- removed older code & blast code (not used)

$ qsub -b yes -j yes -m as -now no -w e -N lmp-bazaz -l ram.c=3.5G,h_rt=43100 -l exclusive.c=1 -pe pe_32 32 -P gentech-rqc.p -o /global/projectb/scratch/brycef/lmp/lmp-bazaz.log -js 501 "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py -f /global/dna/shared/rqc/filtered_seq_unit/00/01/06/33/10633.1.171440.GGCTAC.anqdpht.fastq.gz -o /global/projectb/scratch/brycef/lmp/BAZAZ"

To Do:
- use new bbmap which includes innies and outies and anything else (not ready yet)



Get coverage histogram
khist.sh in=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY.filter.fastq khist=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY.filter.khist.txt
- get past hump

normalize using min_cov, max_cov
bbnorm.sh in=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY.fastq  out=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY-bbnorm.fastq min=25 max=200

assemble normalized fastq with megahit
megahit -r /global/projectb/scratch/brycef/tmp2/clrs/AGGZY-bbnorm40.fastq -l 155 -m 100e9 --cpu-only --k-max=123 --no-low-local --no-bubble -o /global/projectb/scratch/brycef/tmp2/clrs/AGGZY40

convert assembly into only the one with the biggest contig (> 8kb hopefully)
reformat.sh in=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY/contigs.fa out=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY/bigcontigs.fa minlength=10000

get insert size histogram (and average)
- look at everything > 2kb for the average, st dev
bbmap.sh ref=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY/contigs.fa path=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY in=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY-bbnorm.fastq  ihist=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY/ihist.txt rcs=f out=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY/map.txt > /global/projectb/scratch/brycef/tmp2/clrs/AGGZY/map.log 2>&1


Testing:
AGGZY (7032, Shoudan = 7296 +/-635)

$ rm -rf /global/projectb/scratch/brycef/tmp2/clrs/AGGZY-lmp
$ ~/code/my-qsub-exc.sh AGGZY "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/projectb/scratch/brycef/tmp2/clrs/AGGZY.filter.fastq -o /global/projectb/scratch/brycef/tmp2/clrs/AGGZY-lmp"
$ ~/code/my-qsub8.sh AGGZY "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/projectb/scratch/brycef/tmp2/clrs/AGGZY.filter.fastq -o /global/projectb/scratch/brycef/tmp2/clrs/AGGZY-lmp"
$ ./lmp_insert.py --fastq /global/projectb/scratch/brycef/tmp2/clrs/AGGZY.filter.fastq -o /global/projectb/scratch/brycef/tmp2/clrs/AGGZY-lmp

2016-10-16:
BGWZA
$ qsub -b yes -j yes -m n -w e -l ram.c=5.25g,h_vmem=5.25g,h_rt=43200,s_rt=43195 -pe pe_16 16 -N lmp-bgzwa -o /global/projectb/scratch/brycef/qsub/lmp-bgzwa.log  "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/dna/shared/rqc/filtered_seq_unit/00/01/08/79/10879.1.182879.filter-PLANT-2X250.fastq.gz --insert-size 0 --output-path /global/projectb/scratch/brycef/lmp/BGWZA"

2017-06-02
/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/dna/dm_archive/rqc/analyses/AUTO-54523/11438.1.206604.TTAGGC.filter-PLANT-2X250.fastq.gz --insert-size 400 --output-path /global/projectb/scratch/brycef/lmp/BTASS
sbatch -C haswell ~/lmp-btass.sh


~/code/my-qsub-exc.sh AGGZY "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/projectb/scratch/brycef/tmp2/clrs/AGGZY.filter.fastq -o /global/projectb/scratch/brycef/tmp2/clrs/AGGZY-lmp"
~/code/my-qsub-exc.sh AGGZX "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/projectb/scratch/brycef/tmp2/clrs/AGGZX.filter.fastq -o /global/projectb/scratch/brycef/tmp2/clrs/AGGZX-lmp"
~/code/my-qsub-exc.sh AGGZZ "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/projectb/scratch/brycef/tmp2/clrs/AGGZZ.filter.fastq -o /global/projectb/scratch/brycef/tmp2/clrs/AGGZZ-lmp"
~/code/my-qsub-exc.sh AGGZZ0 "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/dna/dm_archive/rqc/filtered_seq_unit/00/00/89/38/8938.1.116555.TTAGGC.anqdspt.fastq.gz -o /global/projectb/scratch/brycef/tmp2/clrs/AGGZZ0-lmp"
~/code/my-qsub-exc.sh AGGZZ1 "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/dna/dm_archive/rqc/filtered_seq_unit/00/00/91/02/9102.1.122074.TTAGGC.anqdspt.fastq.gz -o /global/projectb/scratch/brycef/tmp2/clrs/AGGZZ1-lmp"
~/code/my-qsub-exc.sh ANOXP "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/projectb/scratch/brycef/tmp2/clrs/ANOXP.filter.fastq -o /global/projectb/scratch/brycef/tmp2/clrs/ANOXP-lmp"
# 4kb test for Vasanth
~/code/my-qsub-exc.sh AACZY "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/dna/dm_archive/rqc/filtered_seq_unit/00/00/91/02/9102.1.122074.GTGAAA.anqdspt.fastq.gz -o /global/projectb/scratch/brycef/tmp2/clrs/AACZY-lmp"

# fungal test - have reference!
~/code/my-qsub-exc.sh AGAUO "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/dna/dm_archive/rqc/filtered_seq_unit/00/00/89/65/8965.6.117383.ATGTCA.anqdspt.fastq.gz -o /global/projectb/scratch/brycef/tmp2/clrs/AGAUO-lmp"



~/code/my-qsub-exc.sh AGAUO "/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py --fastq /global/dna/dm_archive/rqc/filtered_seq_unit/00/00/89/65/8965.6.117383.ATGTCA.anqdspt.fastq.gz -o /global/projectb/scratch/brycef/tmp2/clrs/AGAUO-lmp"

- merged fragment reads - very scattered, probably an artifact from the lab
bbmerge.sh in=/global/projectb/scratch/brycef/tmp2/clrs/AGGZZ.filter.fastq ihist=/global/projectb/scratch/brycef/tmp2/clrs/AGGZZ-lmp/aggzz-ihist.txt reads=1m > /global/projectb/scratch/brycef/tmp2/clrs/AGGZZ-lmp/bbmerge.log 2>&1


"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
import numpy
from scipy import stats # mode
import glob
from argparse import ArgumentParser

import matplotlib
matplotlib.use("Agg") ## This needs to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import AutoMinorLocator, MultipleLocator, FormatStrFormatter
import mpld3
import getpass

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, get_status, checkpoint_step, append_rqc_file, append_rqc_stats, run_cmd
from rqc_constants import RQCConstants


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Run khist.sh to get the coverage histogram
- use the 55% - 85% coverage level to determine min and max coverage
'''
def get_coverage(fastq):

    log.info("get_coverage: %s", os.path.basename(fastq))

    min_cov = 0
    max_cov = 0
    status_name = "coverage"
    status = "%s in progress" % status_name

    checkpoint_step(status_log, status)

    cov_path = os.path.join(output_path, "coverage")
    if not os.path.isdir(cov_path):
        os.makedirs(cov_path)



    #khist.sh in=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY.filter.fastq khist=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY.filter.khist.txt
    khist_file = os.path.join(cov_path, "coverage.txt")
    khist_log = os.path.join(cov_path, "coverage.log")

    if not os.path.isfile(khist_file):
        cmd = "#bbtools;khist.sh in=%s khist=%s ow=t > %s 2>&1" % (fastq, khist_file, khist_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    else:
        log.info("- skipping khist, output file exists")



    # how big is the original fastq?  how many reads are we subsampling to?

    if os.path.isfile(khist_file):

        cnt = 0

        raw_list = []
        sum_cnt = 0
        max_raw_cnt = 0
        my_max_raw = 0 # anything after the first 5 rows because low coverage regions (1..4) have a high count normally
        # read the khist file to figure out the min, max coverage to use
        # - only use 1st 500 values
        fh = open(khist_file, "r")


        for line in fh:
            cnt += 1
            if line.startswith("#"):
                continue


            if cnt > 500:
                break

            arr = line.split()
            raw_cnt = int(arr[1])
            if raw_cnt > max_raw_cnt:
                max_raw_cnt = raw_cnt

            if cnt > 5 and raw_cnt > my_max_raw:
                my_max_raw = raw_cnt

            sum_cnt += raw_cnt
            raw_list.append(sum_cnt)

        fh.close()


        # to get the min and max coverage, use 55% to 85% of the data from the khist results
        cnt = 0

        for i in raw_list:
            cnt += 1
            pct = i / float(sum_cnt)

            if pct > 0.55 and min_cov == 0:
                min_cov = cnt

            if pct > 0.85 and max_cov == 0:
                max_cov = cnt

            #print cnt, pct, min_cov, max_cov




        log.info("- coverage: %s, %s", min_cov, max_cov)
        if min_cov > 0 and max_cov > 0 and max_cov > min_cov:

            append_rqc_stats(rqc_stats_log, "minimum_coverage", min_cov)
            append_rqc_stats(rqc_stats_log, "maximum_coverage", max_cov)
            append_rqc_file(rqc_file_log, "khist", khist_file)

            # plot khist_file [0..100]

            # matplotlib chart
            raw_data = numpy.loadtxt(khist_file, comments='#', usecols=(0,1))

            fig, ax = plt.subplots()

            marker_size = 5.0
            line_width = 1.5

            ax.plot(raw_data[:,0], raw_data[:,1], 'g', marker='o', markersize=marker_size, linewidth=line_width, alpha=0.5)

            if min_cov > 0 and max_cov > 0:
                ax.fill_between(raw_data[:,0], 0, raw_data[:,1], where = raw_data[:,0] > min_cov,  facecolor='#99FF99')
                # to make it look like a certain range, fill in tight_upper+ with white
                # damn I'm smooth!
                ax.fill_between(raw_data[:,0], 0, raw_data[:,1], where = raw_data[:,0] > max_cov,  facecolor='#FFFFFF')
                annotation = "Shaded coverage area from %s to %s\n used for assembly" % (int(min_cov), int(max_cov))
                #my_max = np.max(raw_data[:1])
                #my_max = numpy.max(raw_data[:1])

                ax.annotate(annotation, xy=(min_cov, my_max_raw),  xycoords='data', xytext=(30, -45), textcoords='offset points', arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"))


            plt.xlim(0, 100)
            plt.title("Coverage Profile")

            # annotate region from min_cov to max_cov

            ax.set_xlabel("Depth", fontsize=12, alpha=0.5)
            ax.set_ylabel("Raw Count", fontsize=12, alpha=0.5)

            ax.grid(color="gray", linestyle=':')


            png_file = os.path.join(cov_path, "coverage.png")
            html_file = png_file.replace(".png", ".html")

            mpld3.save_html(fig, html_file)

            ## Save Matplotlib plot in png format
            plt.savefig(png_file, dpi=fig.dpi)
            append_rqc_file(rqc_file_log, "coverage_png", png_file)
            append_rqc_file(rqc_file_log, "coverage_html", html_file)


            if os.path.isfile(khist_log):
                # grab read count from coverage_log
                read_count = 0
                #Total reads in:                 82845204        100.000% Kept
                fh = open(khist_log, "r")
                for line in fh:
                    if line.startswith("Total reads in:"):
                        try:
                            read_count = line.strip().split()[3]
                        except:
                            read_count = 0
                            log.error("- cannot get read count from %s", khist_log)
                fh.close()


                log.info("- read count: %s", "{:,}".format(int(read_count)))
                append_rqc_stats(rqc_stats_log, "read_count", read_count)



            status = "%s complete" % status_name
        else:
            status = "%s failed" % status_name

    else:
        status = "%s failed" % status_name




    checkpoint_step(status_log, status)

    return min_cov, max_cov, status


'''
Use bbnorm and min,max coverage to create a new fastq of the mito/chloroplast

'''
def normalize_fastq(fastq, min_cov, max_cov):

    log.info("normalize_fastq: %s [%s, %s]", os.path.basename(fastq), min_cov, max_cov)

    status_name = "normalization"
    status = "%s in progress" % status_name

    checkpoint_step(status_log, status)

    normalize_path = os.path.join(output_path, "bbnorm")
    if not os.path.isdir(normalize_path):
        os.makedirs(normalize_path)


    new_fastq_name = os.path.basename(fastq).replace(".fastq", "-bbnorm.fastq")

    normalized_fastq = os.path.join(normalize_path, new_fastq_name)
    bbnorm_log = os.path.join(normalize_path, "bbnorm.log")

    if not os.path.isfile(normalized_fastq):

        cmd = "#bbtools;bbnorm.sh in=%s out=%s min=%s max=%s > %s 2>&1" % (fastq, normalized_fastq, min_cov, max_cov, bbnorm_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    else:
        log.info("- skipping bbnorm, output file exists")



    # how many reads were used?
    #Total reads in:                 79433044        93.407% Kept
    #Total reads in:                 74196272        29.926% Kept

    if os.path.isfile(normalized_fastq):

        norm_read_count = 0 # normalized read count
        read_count_log = os.path.join(normalize_path, "read_count.log")
        if not os.path.isfile(read_count_log):
            if normalized_fastq.endswith(".gz"):
                cmd = "zcat %s | wc -l > %s 2>&1" % (normalized_fastq, read_count_log)
            else:
                cmd = "wc -l %s > %s 2>&1" % (normalized_fastq, read_count_log)

            std_out, std_err, exit_code = run_cmd(cmd, log)


        if os.path.isfile(read_count_log):
            fh = open(read_count_log, 'r')
            for line in fh:
                try:
                    norm_read_count = int(line.split()[0])/4
                except:
                    norm_read_count = 0
                    log.error("- cannot get read count from %s", read_count_log)
            fh.close()

            log.info("- normalized read count: %s", "{:,}".format(int(norm_read_count)))
            append_rqc_stats(rqc_stats_log, "normalized_read_count", norm_read_count)


            # subsample to 10m reads if > 10m reads
            #max_reads = 10 * 1000000
            max_reads = 10 * 1000000
            if int(norm_read_count) > max_reads:
                log.info("- read count > max (%s > %s), subsampling", "{:,}".format(norm_read_count), "{:,}".format(max_reads))

                reformat_log = os.path.join(normalize_path, "reformat.log")
                new_fastq_name = os.path.basename(normalized_fastq).replace("-bbnorm.fastq", "-bbnorm-subsample.fastq")
                subsampled_fastq = os.path.join(normalize_path, new_fastq_name)

                if not os.path.isfile(subsampled_fastq):

                    cmd = "#bbtools;reformat.sh in=%s out=%s ow=t samplereadstarget=%s > %s 2>&1" % (normalized_fastq, subsampled_fastq, max_reads, reformat_log)
                    std_out, std_err, exit_code = run_cmd(cmd, log)


                normalized_fastq = subsampled_fastq
                append_rqc_stats(rqc_stats_log, "normalized_subsampled_read_count", max_reads)




        status = "%s complete" % status_name
    else:
        status = "%s failed" % status_name



    checkpoint_step(status_log, status)

    return normalized_fastq, status


'''
Use megahit to assemble the normalized fastq
- stats.sh to check if the assembly is okay
'''
def do_assembly(fastq):

    log.info("do_assembly: %s", os.path.basename(fastq))

    assembly = None


    status_name = "assembly"
    status = "%s in progress" % status_name
    checkpoint_step(status_log, status)
    # assembly size, max contig

    asm_path = os.path.join(output_path, "asm")
    if not os.path.isdir(asm_path):
        os.makedirs(asm_path)


    # read the 1st line of the fastq to determine the read size
    read_length = 155
    k_max = 123 # kmer max size (Alex C, Vasanth) - 80% of read length, but must be odd - keep it at 123 (max for megahit anyways)

    # 80%, 0dd
    zcat_head_log = os.path.join(asm_path, "zcat_head.log")
    cat_cmd = "cat"
    if fastq.endswith(".gz"):
        cat_cmd = "zcat"
    cmd = "%s %s | head -4 > %s 2>&1" % (cat_cmd, fastq, zcat_head_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)


    if os.path.isfile(zcat_head_log):
        fh = open(zcat_head_log, "r")
        line_cnt = 0
        for line in fh:
            line_cnt += 1
            if line_cnt == 2:
                read_length = len(line)
        fh.close()



        log.info("- read_length: %s, k_max: %s", read_length, k_max)


    memory = "100e9" # 100,000,000,000 = 100G

    asm_log = os.path.join(output_path, "megahit.log")
    asm_subpath = os.path.join(asm_path, "mh") # need clean path for megahit assembly
    assembly = os.path.join(asm_subpath, "final.contigs.fa") # megahit output


    if not os.path.isfile(assembly):
        # 2016-06-29: megahit 1.0.06 exits with error:
        # megahit: Output directory /global/projectb/scratch/brycef/lmp/BAZAZ/asm/ already exists, please change the parameter -o to another value to avoid overwriting.
        if os.path.isdir(asm_subpath):
            cmd = "rm -rf %s" % asm_subpath
            std_out, std_err, exit_code = run_cmd(cmd, log)

        #shifter --image foster505/megahit:v1.1.1-2-g02102e1 megahit --help
        # v 1.1.1 - use --low-local-ration 0.001 --bubble-level 0 instead of --no-local-ratio --no-bubble
        cmd = "#megahit-1.1.1;megahit -r %s -l %s -m %s --cpu-only --k-max=%s --low-local-ratio 0.001 --bubble-level 0 -o %s > %s 2>&1" % (fastq, read_length, memory, k_max, asm_subpath, asm_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    else:
        log.info("- skipping megahit, output exists")



    if os.path.isfile(assembly):

        status = "%s complete" % status_name
        # check file size > 0? or fail

    else:
        status = "%s failed" % status_name

    checkpoint_step(status_log, status)

    return assembly, status


'''
Use reformat to create a new assembly using only the largest contig(s)
'''
def get_biggest_contig(fasta):

    log.info("get_biggest_contig: %s", os.path.basename(fasta))

    status_name = "biggest contig"
    status = "%s in progress" % status_name
    checkpoint_step(status_log, status)

    asm_path = os.path.join(output_path, "asm")
    if not os.path.isdir(asm_path):
        os.makedirs(asm_path)


    # do this twice: once for reporting (format=1), once for parsing (format=3)
    stats_log = os.path.join(asm_path, "stats_long.log")
    cmd = "#bbtools;stats.sh format=1 in=%s > %s 2>&1" % (fasta, stats_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    append_rqc_file(rqc_file_log, "asm_stats", stats_log)

    # determine the largest contig(s), hopefully > 10kb!
    # format=3 only show 2 lines for info
    stats_log = os.path.join(asm_path, "stats.log")
    cmd = "#bbtools;stats.sh format=3 in=%s > %s 2>&1" % (fasta, stats_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    # grap a couple of numbers from stat.sh
    scaffold_cnt = 0 # number of scaffolds
    scaffold_bp = 0 # total bp
    scaffold_max = 0 # 9, biggest scaffold in bp

    if os.path.isfile(stats_log):
        fh = open(stats_log, "r")
        for line in fh:
            if line.startswith("n_scaffolds"):
                continue

            arr = line.strip().split()
            scaffold_cnt = int(arr[0])
            scaffold_bp = int(arr[2])
            scaffold_max = int(arr[13]) # was 9
            #print arr


        fh.close()

    log.info("- scaffolds: %s", "{:,}".format(scaffold_cnt))
    log.info("- assembly size: %s bp", "{:,}".format(scaffold_bp))
    log.info("- biggest scaffold: %s bp", "{:,}".format(scaffold_max))


    append_rqc_stats(rqc_stats_log, "scaffolds", scaffold_cnt)
    append_rqc_stats(rqc_stats_log, "scaffold_bp", scaffold_bp)
    append_rqc_stats(rqc_stats_log, "biggest_scaffold", scaffold_max)



    scaffold_size = 10000 # round to nearest 10k

    if scaffold_max < scaffold_size:
        log.error("- biggest scaffold less than 10kbp!")
        #status = "%s failed" % status_name
        #checkpoint_step(status_log, status)
        #sys.exit(11)
        scaffold_size = 1000 # try to round to nearest 1000bp

    # round biggest scaffold to lowest 10kb increment
    scaffold_limit = int(scaffold_max / float(scaffold_size)) * scaffold_size


    #reformat.sh in=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY/contigs.fa out=/global/projectb/scratch/brycef/tmp2/clrs/AGGZY/bigcontigs.fa minlength=10000
    big_assembly = fasta.replace(".fa", ".biggest.fa")
    log_file = os.path.join(asm_path, "reformat-big.log")


    if not os.path.isfile(big_assembly):
        cmd = "#bbtools;reformat.sh in=%s out=%s minlength=%s > %s 2>&1" % (fasta, big_assembly, scaffold_limit, log_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)


    else:
        log.info("- skipping reformat, biggest contig exists")



    if os.path.isfile(big_assembly):
        # stats.sh to check?
        status = "%s complete" % status_name

        # consider blasting the head and tail (600 bases) for the big fasta

    else:
        status = "%s failed" % status_name

    # hopefully > 8kb
    checkpoint_step(status_log, status)

    return big_assembly, status


'''
Use bbmap to generate insert size histogram
- calc averages for < 2000bp and > 2000bp
- plot the histogram up to 10kbp
- insert_size = expected insert size from ITS
'''
def get_insert_size(fasta, fastq, insert_size):

    log.info("get_insert_size: %s", os.path.basename(fasta))

    status_name = "insert size"
    status = "%s in progress" % status_name
    checkpoint_step(status_log, status)

    isize_path = os.path.join(output_path, "isize")
    if not os.path.isdir(isize_path):
        os.makedirs(isize_path)



    insert_histogram = os.path.join(isize_path, "ihist.txt")
    map_log = os.path.join(isize_path, "map.log")
    map_file = os.path.join(isize_path, "map.txt")

    if not os.path.isfile(insert_histogram):


        cmd = "#bbtools;bbmap.sh ref=%s in=%s path=%s ihist=%s out=%s rcs=f machineout > %s 2>&1" % (fasta, fastq, isize_path, insert_histogram, map_file, map_log)
        # rcs=f = require correct strand = Forbid pairing of reads without correct strand orientation

        # use the first one because we want to see the small fragments that are merging around 270bp
        #cmd = "bbmap.sh ref=%s in=%s path=%s ihist=%s out=%s rcomp=t machineout > %s 2>&1" % (fasta, fastq, isize_path, insert_histogram, map_file, map_log)
        # rcomp=t = use reverse compliment

        std_out, std_err, exit_code = run_cmd(cmd, log)


    else:
        log.info("- skipping bbmap, output file exists")


    if os.path.isfile(insert_histogram):

        # read file to calc insert size
        fh = open(insert_histogram, "r")
        max_x = 0
        i_list = []       # all values
        i_list_2k = []    # all values with insert size greater than 2k
        i_list_lt_2k = [] # all values with insert size less than 2k

        hist_dict = {} # histogram dictionary
        hbin_size = 100 # tight insert should be 10
        max_x_axis = 10000


        for line in fh:
            if line.startswith("#"):

                arr = line.strip().replace("#", "").split()

                if str(arr[0]).lower().startswith("mode"):
                    i_mode = arr[1]
                    if int(i_mode) < 1000:
                        hbin_size = 10  # tight insert
                        max_x_axis = 1000

            else:
                arr = line.strip().split()
                if len(arr) > 1:
                    isize = int(arr[0])
                    cnt = int(arr[1])

                    hbin = int(hbin_size * int(isize / float(hbin_size)))
                    bucket = str(hbin).zfill(6)
                    if bucket in hist_dict:
                        hist_dict[bucket] += cnt
                    else:
                        hist_dict[bucket] = cnt


                    if isize > max_x:
                        max_x = isize
                    for _ in range(cnt):
                        i_list.append(isize)

                        if isize > 2000:
                            i_list_2k.append(isize)
                        else:
                            i_list_lt_2k.append(isize)

        fh.close()


        # could use numpy.average(i_list, i_weights) where i_list = column[0], weights = column[1]
        # but there is no way to get the standard deviation this way so I threw it all into a list

        i_all_avg = numpy.average(i_list)
        i_all_std = numpy.std(i_list)
        i_all_median = numpy.median(i_list)
        i_all_mode = stats.mode(i_list)[0][0]
        i_max = numpy.max(i_list)

        # average, std for everything > 2000 bases
        i_2k_avg = 0
        i_2k_std = 0
        i_2k_median = 0
        i_2k_mode = 0

        if len(i_list_2k) > 0:
            i_2k_avg = numpy.average(i_list_2k)
            i_2k_std = numpy.std(i_list_2k)
            i_2k_median = numpy.median(i_list_2k)
            i_2k_mode = stats.mode(i_list_2k)[0][0]


        # average, std for everything <= 2000 bases
        i_lt_2k_avg = 0
        i_lt_2k_std = 0
        i_lt_2k_median = 0
        i_lt_2k_mode = 0

        if len(i_list_lt_2k) > 0:
            i_lt_2k_avg = numpy.average(i_list_lt_2k)
            i_lt_2k_std = numpy.std(i_list_lt_2k)
            i_lt_2k_median = numpy.median(i_list_lt_2k)
            i_lt_2k_mode = stats.mode(i_list_lt_2k)[0][0]


        # save stats

        log.info("- insert size: %s +/- %s", i_all_avg, i_all_std)
        log.info("- insert size (> 2kb): %s +/- %s", i_2k_avg, i_2k_std)
        log.info("- insert size (<= 2kb): %s +/- %s", i_lt_2k_avg, i_lt_2k_std)

        append_rqc_stats(rqc_stats_log, "insert_avg", i_all_avg)
        append_rqc_stats(rqc_stats_log, "insert_std", i_all_std)
        append_rqc_stats(rqc_stats_log, "insert_median", i_all_median)
        append_rqc_stats(rqc_stats_log, "insert_mode", i_all_mode)


        # use mode if insert size not passed
        if insert_size == 0:
            insert_size = i_all_mode

        tight_lower = 0
        tight_upper = 0
        tight_peak = 0.0
        if insert_size < 1500: # tight insert = 400, 800.  CLRS/LMP = 2000 - 8000
            tight_lower, tight_upper, tight_peak = tight_insert(i_all_mode, insert_size, insert_histogram)


        insert_histogram_bucket = os.path.join(isize_path, "isize_bucket.txt")
        fh = open(insert_histogram_bucket, "w")
        for k in sorted(hist_dict.iterkeys()):
            fh.write("%s %s\n" % (int(k), hist_dict[k]))

        fh.close()

        append_rqc_file(rqc_file_log, "insert_histogram_bucket", insert_histogram_bucket)

        max_x = min(max_x_axis, max_x)


        if len(hist_dict) > 0:
            # matplotlib chart
            raw_data = numpy.loadtxt(insert_histogram_bucket, comments='#', usecols=(0,1))
            #raw_data = numpy.loadtxt(ihist_file, comments='#', usecols=(0,1))
            fig, ax = plt.subplots()

            marker_size = 5.0
            line_width = 1.5

            ax.plot(raw_data[:,0], raw_data[:,1], 'r', marker='o', markersize=marker_size, linewidth=line_width, alpha=0.5)

            # if tight insert criteria, fill in range
            if tight_lower > 0 and tight_upper > 0:
                ax.fill_between(raw_data[:,0], 0, raw_data[:,1], where = raw_data[:,0] > tight_lower,  facecolor='#99FF99')
                # to make it look like a certain range, fill in tight_upper+ with white
                # damn I'm smooth!
                ax.fill_between(raw_data[:,0], 0, raw_data[:,1], where = raw_data[:,0] > tight_upper,  facecolor='#FFFFFF')
                annotation = "Shaded area from %s to %s\n is %s of total histogram" % (int(tight_lower), int(tight_upper), "{:.2%}".format(tight_peak))
                ax.annotate(annotation, xy=(i_all_mode, i_max),  xycoords='data', xytext=(30, -45), textcoords='offset points', arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.2"))

            # legend
            textstr = '$\mu=%.2f$\n$\mathrm{median}=%s$\n$\sigma=%.2f$\n$mode=%s$' % (i_all_avg, int(i_all_median), i_all_std, i_all_mode)
            props = dict(boxstyle='round', facecolor='#DDDDDD', alpha=0.5)

            # place a text box in upper left in axes coords with textstr
            ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12, verticalalignment='top', bbox=props)


            plt.xlim(0, max_x)
            plt.title("Insert Size (%sbp bin size)" % (hbin_size))

            #n, bins, patches = plt.hist(x, 50, normed=1, facecolor='g', alpha=0.75)

            ax.set_xlabel("Insert Size (bp)", fontsize=12, alpha=0.5)
            ax.set_ylabel("Number of Reads", fontsize=12, alpha=0.5)

            ax.grid(color="gray", linestyle=':')


            png_file = os.path.join(isize_path, "insert_size_histogram.png")
            html_file = png_file.replace(".png", ".html")

            mpld3.save_html(fig, html_file)
            ## Save Matplotlib plot in png format
            plt.savefig(png_file, dpi=fig.dpi)

            append_rqc_file(rqc_file_log, "insert_histogram_png", png_file)
            append_rqc_file(rqc_file_log, "insert_histogram_html", html_file)

        else:
            log.error("- no data for insert histogram!")



        append_rqc_stats(rqc_stats_log, "insert_2k_avg", i_2k_avg)
        append_rqc_stats(rqc_stats_log, "insert_2k_std", i_2k_std)
        append_rqc_stats(rqc_stats_log, "insert_2k_median", i_2k_median)
        append_rqc_stats(rqc_stats_log, "insert_2k_mode", i_2k_mode)

        append_rqc_stats(rqc_stats_log, "insert_lt_2k_avg", i_lt_2k_avg)
        append_rqc_stats(rqc_stats_log, "insert_lt_2k_std", i_lt_2k_std)
        append_rqc_stats(rqc_stats_log, "insert_lt_2k_median", i_lt_2k_median)
        append_rqc_stats(rqc_stats_log, "insert_lt_2k_mode", i_lt_2k_mode)

        append_rqc_file(rqc_file_log, "insert_histogram_txt", insert_histogram)

        # parse mapping data
        append_rqc_file(rqc_file_log, "mapping_log", map_log)
        fh = open(map_log, "r")
        for line in fh:
            line = line.strip()

            if line.startswith("Exec") or line.startswith("java") or line.startswith("Set "):
                continue


            # key=value
            arr = line.split("=")
            if len(arr) > 1:
                k = str(arr[0]).lower()
                v = arr[1]

                append_rqc_stats(rqc_stats_log, k, v)

                #print "[%s] [%s]" % ( k, v)
                # save stats
        fh.close()



        status = "%s complete" % status_name

    else:
        status = "%s failed" % status_name

    checkpoint_step(status_log, status)
    return status

# calculates the percent below the cutoff for tight_insert libraries using the ihist.txt file
# - if there is a long tail on the right then the library will probably fail anyways
# - need to pass library_info.rqc_insert_size (insert_size param) to get correct values
def tight_insert(mode, insert_size, insert_histogram):

    log.info("tight_insert: %s", mode)


    if os.path.isfile(insert_histogram):

        sum_all = 0 # sum of all

        fh = open(insert_histogram, "r")
        for line in fh:

            if line.startswith("#"):
                continue

            arr = line.split()
            if len(arr) == 2:
                i = int(arr[0]) # insert size
                cnt = int(arr[1])

                sum_all += cnt

        fh.close()

        # old criteria:
        # if 400bp insert size: 75% BETWEEN 360 - 440
        # if 800bp insert size: 90% BETWEEN 720 - 880

        # new criteria: 2015-07-30
        # if 400bp insert size: 90% BETWEEN [40 - mode + 40]
        # if 800bp insert size: 75% BETWEEN [80 - mode + 80]

        hill_sum = 0 # amount of the insert size peak
        hill_lower = mode - (mode * 0.15)
        hill_upper = mode + (mode * 0.15)
        hill_criteria = 0.9 # 90% of data should be between hill_lower and hill_upper

        # mode should be between mode_lower and mode_upper for QC
        mode_lower = insert_size - insert_size * 0.1
        mode_upper = insert_size + insert_size * 0.1

        if insert_size > 550: # 800bp - assumption (only 400bp and 800bp tight insert libs)
            hill_criteria = 0.75


        mode_qc = "FAIL"
        if mode >= mode_lower and mode <= mode_upper:
            mode_qc = "PASS"

        log.info("- mode: %s <= %s <= %s ? %s", mode_lower, mode, mode_upper, mode_qc)


        fh = open(insert_histogram, "r")
        for line in fh:

            if line.startswith("#"):
                continue

            arr = line.split()
            if len(arr) == 2:
                i = int(arr[0]) # insert size
                cnt = int(arr[1])

                if i >= hill_lower and i <= hill_upper:
                    hill_sum += cnt

        fh.close()


        peak = 0  # amount that is insert size +/- 10%
        if sum_all > 0:

            peak = hill_sum / float(sum_all)

        hill_qc = "FAIL"
        if peak >= hill_criteria:
            hill_qc = "PASS"


        log.info("- peak = %s/%s for read insert size between [%s, %s], mode = %s, qc = %s", "{:.2%}".format(peak), "{:.2%}".format(hill_criteria), hill_lower, hill_upper, mode, hill_qc)


        # new stats
        append_rqc_stats(rqc_stats_log, "hill_lower", hill_lower)
        append_rqc_stats(rqc_stats_log, "hill_upper", hill_upper)
        append_rqc_stats(rqc_stats_log, "peak", peak)
        append_rqc_stats(rqc_stats_log, "mode_qc", mode_qc)
        append_rqc_stats(rqc_stats_log, "hill_qc", hill_qc)

        # return ranges for histogram plot
        return hill_lower, hill_upper, peak




'''
Remove intermediate files ...
a test run was 42GB and could be greatly reduced!
- purge off temporarily
'''
def do_purge(purge_flag):

    log.info("do_purge: %s", purge_flag)


    status_name = "purge"
    status = "%s in progress" % status_name

    checkpoint_step(status_log, status)

    # what to remove?
    # bbnorm = 16G
    # isize = 25G
    # asm = 797m

    file_list = []
    file_list.append(os.path.join(output_path, "asm", "tmp"))
    file_list.append(os.path.join(output_path, "asm", "*.fa"))
    file_list.append(os.path.join(output_path, "bbnorm", "*.fastq"))
    file_list.append(os.path.join(output_path, "isize", "map.txt"))
    file_list.append(os.path.join(output_path, "isize", "ref"))

    f_cnt = 0 # number of files & folders removed

    for f in file_list:
        if os.path.isdir(f):
            cmd = "rm -rf %s" % (f)
            log.info("- cmd: %s", cmd)
            if purge_flag == True:
                std_out, std_err, exit_code = run_cmd(cmd, log)

            f_cnt += 1

        else:
            my_list = glob.glob(f)
            for ff in my_list:
                cmd = "rm %s" % ff
                log.info("- cmd: %s", cmd)
                if purge_flag == True:
                    std_out, std_err, exit_code = run_cmd(cmd, log)

                f_cnt += 1


    status = "%s complete" % status_name
    checkpoint_step(status_log, status)

    log.info("- removed %s files and folders", f_cnt)

    return status


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# coverage, normalize, assemble, biggest contig, insert size
def get_next_step(status):

    log.info("get_next_step: %s", status)

    next_status = None

    status_dict = {
        "start" : "coverage",
        "coverage complete" : "normalize",
        "normalization complete" : "assembly",
        "assembly complete" : "biggest contig",
        "biggest contig complete" : "insert size",
        "insert size complete" : "purge",
        "purge complete" : "complete",

        "failed" : "failed",
        "complete" : "complete",
    }



    if status:
        status = status.lower()

        if status in status_dict:
            next_status = status_dict[status]
        else:
            if status.endswith("failed"):
                next_status = "failed"
            else:
                next_status = "failed: next step missing for '%s'" % (status)
                log.error("Cannot get the next step for %s", status)
                sys.exit(12)

    log.info("- next status = %s", next_status)
    return next_status



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "LMP Insert Size"
    version = "1.2.3"



    # Parse options
    usage = "prog [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-f", "--fastq", dest="fastq", type=str, help="fastq file (full path to fastq), better to use filtered fastq")
    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-i", "--insert-size", dest="insert_size", type=int, help = "Expected insert size in bp (400,800,4000,8000)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)

    fastq = None # fastq on file system

    output_path = None
    log_level = "INFO"
    insert_size = 0
    purge_flag = False
    print_log = False
    
    uname = getpass.getuser()
    if uname == "qc_user":
        purge_flag = True

    # parse args
    args = parser.parse_args()

    print_log = args.print_log

    if args.fastq:
        fastq = args.fastq

    if args.output_path:
        output_path = args.output_path

    if args.insert_size:
        insert_size = args.insert_size

    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)


    # initialize my logger
    log_file = os.path.join(output_path, "lmp_insert_size.log")



    # log = logging object
    log = get_logger("lmp", log_file, log_level, print_log)

    log.info("%s", 80 * "~")

    log.info("Starting %s (%s %s)", script_name, my_name, version)
    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "fastq", fastq)
    log.info("%25s      %s bp", "insert_size", insert_size)
    log.info("%25s      %s", "output_path", output_path)

    log.info("")


    if not fastq:
        log.error("- fastq not specified!")
        sys.exit(2)

    if not os.path.isfile(fastq):
        log.error("- fastq does not exist on file system!  %s", fastq)
        sys.exit(2)



    # create a name by stripping .fastq from the fastq name
    rqc_file_log = os.path.join(output_path, "lmp-files.tmp")
    rqc_stats_log = os.path.join(output_path, "lmp-stats.tmp")


    # RQC System log
    status_log = os.path.join(output_path, "lmp_status.log")

    # set umask
    umask = os.umask(RQCConstants.UMASK)
    status = get_status(status_log)


    # testing
    if uname == "brycef":
        #get_coverage(fastq, output_path)
        #norm_fastq, status = normalize_fastq(fastq, 18, 29, output_path)
        #fasta, status = do_assembly(norm_fastq, output_path)
        #fasta, status = get_biggest_contig(fasta, output_path)

        #fasta = os.path.join(output_path, "asm", "final.contigs.biggest.fa")
        #do_purge(output_path)
        #get_insert_size(fasta, fastq, insert_size, output_path)
        #insert_histogram = os.path.join(output_path, "isize", "ihist.txt")

        #sys.exit(44)
        pass

    checkpoint_step(status_log, "## New Run")

    if status == "complete":
        log.info("Status is complete for %s, exiting.", fastq)
        sys.exit(0)

    else:
        status = "start"
        checkpoint_step(status_log, status)



    done = False
    cycle = 0

    min_cov = 0
    max_cov = 0
    norm_fastq = None # normalized fastq
    fasta = None # assembly, and ''reformatted'' to get largest contigs

    while done == False:

        cycle += 1


        # determine the next step from the current status
        status = get_next_step(status)


        if status in ("start", "coverage"):

            min_cov, max_cov, status = get_coverage(fastq)

        if status == "normalize":

            norm_fastq, status = normalize_fastq(fastq, min_cov, max_cov)

        if status == "assembly":

            fasta, status = do_assembly(norm_fastq)

        if status == "biggest contig":

            fasta, status = get_biggest_contig(fasta)

        if status == "insert size":

            status = get_insert_size(fasta, fastq, insert_size)

        if status == "purge":

            status = do_purge(purge_flag)

        if cycle > 20:
            done = True

        if status == "complete":
            done = True

        if status == "failed":
            done = True




    if status == "complete":
        # move rqc-files.tmp to rqc-files.txt
        rqc_new_file_log = os.path.join(output_path, "lmp-files.txt")

        cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        rqc_new_stats_log = os.path.join(output_path, "lmp-stats.txt")

        cmd = "mv %s %s" % (rqc_stats_log, rqc_new_stats_log)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        checkpoint_step(status_log, status)

    if status.find("failed") > -1:
        status = "failed"
        checkpoint_step(status_log, status)

    os.umask(umask)

    log.info("Completed %s: %s", script_name, fastq)

    sys.exit(0)



"""

        /|   /|
       / |  / |
      /  | /  |
     /___|/___|
    /    \/   \
    | .  |  . |
   \\____/\___//
   --(.: V  :.)--
    /// ___ // \
     // |_| //
     //     //
     //     //
   ((___M____//


  Greetings my children
  Come with me now
  And discover the magic
  That lies beyond the sea of dreams.
  And if you have the courage
  To cross the rainbow bridge
  You may find the Sacred Heart.

"""
