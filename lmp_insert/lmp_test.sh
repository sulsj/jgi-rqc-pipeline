#!/bin/bash -l

#set -e

# Long mate pair testing (CLRS)

folder="/global/projectb/sandbox/rqc/analysis/qc_user/clrs"
DATE=`date +"%Y%m%d"`


#dev for Bryce
cmd="/global/homes/b/brycef/git/jgi-rqc-pipeline/lmp_insert/lmp_insert.py"


# AGAUO 4kb  (insert size from reference = 4427 +/- 1554)
qsub -b yes -j yes -m as -now no -w e -N lmp-AGAUO -l exclusive.c -P gentech-rqc.p -js 401 -o $folder/lmp-AGAUO.log "$cmd -o $folder/AGAUO-lmp-$DATE -f $folder/AGAUO-filter.fastq"
 

# AGGZY 8kb (insert size from Shoudan's qc = 7296 +/- 635)
qsub -b yes -j yes -m as -now no -w e -N lmp-AGGZY -l exclusive.c -P gentech-rqc.p -js 400 -o $folder/lmp-AGGZY.log "$cmd -o $folder/AGGZY-lmp-$DATE -f $folder/AGGZY-filter.fastq"

# AATHS - 400bp tight insert
qsub -b yes -j yes -m as -now no -w e -N lmp-AATHS -l exclusive.c -P gentech-rqc.p -js 403 -o $folder/lmp-AATHS.log "$cmd -o $folder/AATHS-lmp-$DATE -f $folder/AATHS-filter.fastq"

# unpigz -c /global/dna/dm_archive/rqc/filtered_seq_unit/00/00/89/65/8965.6.117383.ATGTCA.anqdspt.fastq.gz > AGAUO-filter.fastq


# ~/git/jgi-rqc/rqc_system/test/compare.py -p lmpins -rf /global/projectb/sandbox/rqc/analysis/qc_user/clrs/AGAUO-lmp-20150806,/global/projectb/sandbox/rqc/analysis/qc_user/clrs/AGAUO-lmp-20150706
