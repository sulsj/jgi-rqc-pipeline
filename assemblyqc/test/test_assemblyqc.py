#!/usr/bin/env python


import sys
import os
import hashlib
from nose import *


dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir,'../'))       ## rqc-pipeline/assemblyqc/lib
sys.path.append(os.path.join(dir,'../lib'))    ## rqc-pipeline/lib
sys.path.append(os.path.join(dir,'../tools'))  ## rqc-pipeline/tools
sys.path.append(os.path.join(dir,'../../lib'))    ## rqc-pipeline/lib
sys.path.append(os.path.join(dir,'../../tools'))  ## rqc-pipeline/tools

from common import *

from assemblyqc_utils import *
from assemblyqc_constants import *
from assemblyqc_report import *
from assemblyqc import *

from time import time, strftime
import shutil

def setup_module(module):
    print ("") # this is to get a newline after the dots
    print ("setup_module before anything in this file")

def teardown_module(module):
    print ("teardown_module after everything in this file")

def my_setup_function():
    print ("my_setup_function")

def my_teardown_function():
    print ("my_teardown_function")


###For each of the cases below, run assemblyqc on the specific sequence and write to the output directory the results.
###Then open the rqc_stats file in the output directory for each:
###compare the contents of the rqc_stats file to a golden standard file that is under test/
###Then open the rqc_files file in the output directory for each:
###compare the contents of the rqc_files file to a golden standard file that is under test/ but also check if the file exists with non-zero size.


###Fungal


###Metagenomic


###Microbial
###fungal, microbial single cell, microbial isolate
###test_fastqs = ['/global/dna/dm_archive/sdm/illumina/00/79/53/7953.2.88059.GCCAAT.fastq.gz',
###'/global/dna/dm_archive/sdm/illumina/00/80/69/8069.1.89378.GGCTAC.fastq.gz',
###'/global/dna/dm_archive/sdm/illumina/00/79/45/7945.1.87371.GCCAA.fastq.gz',
test_fastqs = [
'/global/dna/dm_archive/sdm/illumina/00/80/44/8044.5.89797.CGGAAT.fastq.gz',
'/global/dna/dm_archive/sdm/illumina/00/80/44/8044.4.89792.TAGCTT.fastq.gz',
'/global/dna/dm_archive/sdm/illumina/00/80/82/8082.1.89732.TTCAACAGGTG.fastq.gz',
'/global/dna/dm_archive/sdm/illumina/00/80/82/8082.1.89732.AGCGTGTCCAA.fastq.gz',
'/global/dna/dm_archive/sdm/illumina/00/80/44/8044.4.89792.GTTTCG.fastq.gz',
'/global/dna/dm_archive/sdm/illumina/00/80/82/8082.1.89732.GACGCTAGTTC.fastq.gz',
'/global/dna/dm_archive/sdm/illumina/00/79/91/7991.1.87974.GCACT.fastq.gz',
'/global/dna/dm_archive/sdm/illumina/00/79/89/7989.1.87940.GAGTG.fastq.gz',
'/global/dna/dm_archive/sdm/illumina/00/78/46/7846.5.85464.ACATCT.fastq.gz']



'''
'/global/dna/dm_archive/sdm/illumina/00/79/54/7954.2.88067.ACAGTG.fastq.gz',
'''



@with_setup(my_setup_function, my_teardown_function)
def test_assembly():
    timestamp = strftime("%m%d%Y-%H%M%S")
    for fastq in test_fastqs:
    
        my_name = "TEST " + fastq
        base = os.path.basename(fastq)
        output_path = os.path.join( os.getcwd() , base )
        
        ###Read the golden standard files
        golden_path = os.path.join( os.getcwd() , "gold_" + base )
        assert os.path.exists(golden_path)
        assert os.path.isdir(golden_path)
        gold_rqc_file_log = os.path.join(golden_path, "rqc-files.txt")
        gold_rqc_stats_log = os.path.join(golden_path, "rqc-stats.txt")
        assert os.path.exists(gold_rqc_file_log)
        assert os.path.isfile(gold_rqc_file_log)
        assert os.path.exists(gold_rqc_stats_log)
        assert os.path.isfile(gold_rqc_stats_log)
    
        log_level = "INFO"
        max_subsampl_reads = "5000000"
        
        if os.path.isdir(output_path) and os.path.exists(output_path):
            shutil.move(output_path, output_path + "_" + timestamp)
            
        # create output_directory if it doesn't exist
        if not os.path.isdir(output_path):
            os.makedirs(output_path)	
    
        # initialize my logger
        log_file = os.path.join(output_path, "assemblyqc.log")
        #print "Started phix_error pipeline, writing log to: %s" % (log_file)
        
        # log = logging object
        log = get_logger("assemblyqc", log_file, log_level)
    
    
        log.info("Starting %s: %s", my_name, fastq)
    
        
        ## Validate output dir and fastq
        
        # create output_directory if it doesn't exist
        if not os.path.isdir(output_path):
            log.info("Cannot find %s, creating as new", output_path)
            os.makedirs(output_path)
        
        assert os.path.isdir(output_path)
        
        assert os.path.isfile(fastq)
            
        # Create the status log file location.
        status_log = os.path.join(output_path, "status.log")
        
        # create the file log and stats log locations.
        rqc_file_log = os.path.join(output_path, "rqc-files.txt")
        rqc_stats_log = os.path.join(output_path, "rqc-stats.txt")
        
        # cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        # log.info("- cmd: %s", cmd)
        
        
    
        log.info( "The fastq file is " + str(fastq) )
        kmer = 0
        read_length = 0
        read_length_1 = 0
        read_length_2 = 0
        is_pe = True
    
        (read_length, read_length_1, read_length_2, is_pe) = get_working_read_length(fastq, log);
        assert read_length > 0
    
        #Save the read_lengths and pairedness to the statistics
        if is_pe == True:
            append_rqc_stats(rqc_stats_log, AssemblyStats.ILLUMINA_READ_LENGTH_1, read_length_1)
            append_rqc_stats(rqc_stats_log, AssemblyStats.ILLUMINA_READ_LENGTH_2, read_length_2)
        else:
            append_rqc_stats(rqc_stats_log, AssemblyStats.ILLUMINA_READ_LENGTH_1, read_length)
    
        kmer = 63
        maxkmer = 63
        kmer = read_length_to_kmer(read_length, maxkmer); #KMER_TEST
    
        log.info("kmer " + str(kmer))
    
        status, dest_fastq = illumina_subsampling(fastq, output_path, status_log, max_subsampl_reads, log)
        assert status.endswith("start")
        # The last True is for skipping megan because in testing it may cause problems.
        status = illumina_assemble_velvet(kmer, fastq, output_path, status_log, rqc_stats_log, rqc_file_log, log, 16, True)
        assert status.endswith("success") or status.endswith("complete")
        status = illumina_assembly_level_report(output_path, status_log, rqc_stats_log, rqc_file_log, kmer, read_length_1, log, True)
        assert status.endswith("success") or status.endswith("complete")
    
        compare(rqc_stats_log, gold_rqc_stats_log)
        
        compare(rqc_file_log, gold_rqc_file_log)
    
        print "Status: " + status
        
        checkpoint_step(status_log, status)
    
        if status.find("failed") > -1:
            checkpoint_step(status_log, "failed")
    
        log.info("Completed %s: %s", my_name, fastq)
