#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
    2nd Generation RQC Assembly pipeline.
    Use an assembler based on project types (RQC-1075)
    [subsample] -> [error correctoin] -> sketch -> assembly -> report data generation -> done!

    Depends on:
        assemblyqc_constants
        assemblyqc_utils
        common
        matplotlib
          \-pyplot
        numpy
        os_utility
        rqc_constants
        rqc_fastq
        * assembler executables

    20180507 6.0.0: SY - use different assemblers for different project types (RQC-1075)
    1) MegaHit for metagenome / metatranscriptome (flags are in rqc_setup.py)
        metagenome pipeline: metagenome_flag = 1
            seq_prod_name in ('metagenome minimal draft', 'metagenome improved draft', 'metagenome standard draft')
            seq_prod_name in ('metagenome viral improved draft', 'metagenome viral standard draft')
            if str(rs['final_deliverable_prod_name']).lower().startswith('cell enrichment, unamplified')

        metatranscriptome : metatranscriptome_flag = 1
            seq_prod_name in ('metagenome metatranscriptome', 'eukaryote community metatranscriptome'):
    2) Spades for microbial projects:
        sag : sag_asm_flag = 1
            seq_prod_name in ('microbial minimal draft, single cell', 'microbial improved draft, single cell')
        iso : microbe_isolate_flag = 1 ?
        ce : microbe_cell_enrichment_flag = 1:
            seq_prod_name == 'metagenome viral minimal draft'
            str(rs['final_deliverable_prod_name']).lower().startswith('cell enrichment, amplified')
        sps : microbe_sps_flag = 1
            seq_prod_name == 'single particle sort, 16s negative':


    Update started: May, 21 2018
    Shijie Yao (syao@lbl.gov)

Revision:


'''


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
import argparse
from collections import defaultdict
import operator
import re
import time
import shutil
import numpy as np
import matplotlib
matplotlib.use('Agg') ## to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
import mpld3
from threading import Thread
import threading

# custom libs in '../lib/'
srcDir = os.path.dirname(__file__)
libDir = os.path.join(srcDir, 'lib')
tooDir = os.path.join(srcDir, '../tools')

sys.path.append(libDir)       ## rqc-pipeline/assemblyqc/lib
sys.path.append(os.path.join(srcDir, '../lib'))    ## rqc-pipeline/lib
sys.path.append(tooDir)  ## rqc-pipeline/tools
sys.path.append(os.path.join(srcDir, os.path.pardir, 'sag'))

from assemblyqc_utils import checkpoint_step, calculate_line_total, get_cat_cmd
from assemblyqc_constants import get_tool_path, AssemblyStats
from rqc_fastq import get_working_read_length
from common import append_rqc_stats, append_rqc_file, get_logger, get_status, run_cmd
from os_utility import make_dir
from rqc_constants import RQCReferenceDatabases
from micro_lib import make_asm_stats, save_asm_stats, save_file

VERSION = '0.5.0'

LOGGER = None
DEBUG = False

TIME_S = time.time()

SUBSAMPLE = 'sub sampled file'
ERRCORRECTION = 'error correction file file'

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## pipeline step control constant
PIPE_START = 'pipeline start'
INIT_START = 'start init'
INIT_END = 'end init'
SAMPLE_START = 'start subsample'
SAMPLE_END = 'end subsample'
BFEC_START = 'start bloom-filter based error correction'
BFEC_END = 'end bloom-filter based error correction'
ASSEMBLE_START = 'start assemble'
ASSEMBLE_END = 'end assemble'
SKETCH_START = 'start sketching'
SKETCH_END = 'end sketching'
POST_ASSEMBLE_START = 'start post assemble'
POST_ASSEMBLE_END = 'end post assemble'

PIPE_COMPLETE = 'pipeline complete'

STEP_ORDER = {'start' : 0,
    PIPE_START : 0,
    INIT_START : 10,
    INIT_END : 20,
    SAMPLE_START : 30,
    SAMPLE_END : 40,
    BFEC_START : 50,
    BFEC_END : 52,
    ASSEMBLE_START : 70,
    ASSEMBLE_END : 72,
    POST_ASSEMBLE_START : 90,
    POST_ASSEMBLE_END : 100,
    SKETCH_START : 110,
    SKETCH_END : 120,
    PIPE_COMPLETE : 1000
}

## stats / file keys that used in multiple places
STATS_FILE_TAG = 'stats file'
FILES_FILE_TAG = 'files file'
CHECKPOINT_FILE_TAG = 'checkpoint file'
SKETCH_VS_NT_FILE_TAG = 'sketch vs nt output'
SKETCH_VS_REFSEQ_FILE_TAG = 'sketch vs refseq output'
SKETCH_VS_SILVA_FILE_TAG = 'sketch vs silva output'

ASM_LEVEL_TOP_5_CONTIG_FILE_TAG = 'assembly level top 5 contig file'
ASM_LEVEL_GC_HIST_IMG_FILE_TAG = 'assembly level GC content hist HTML image file'
ASM_LEVEL_GC_HIST_FILE_TAG = 'assembly level GC content text hist file'

PAIRED_VALUE = 'paired'
READ_LENGTH_VALUE = 'read length'
READ_LENGTH_1_VALUE = 'read1 length'
READ_LENGTH_2_VALUE = 'read2 length'

## blast DB constants
REF_DB = {
    'nt' : RQCReferenceDatabases.NT_maskedYindexedN_BB,
    'LSSURef': RQCReferenceDatabases.LSSU_REF,
    'LSURef' : RQCReferenceDatabases.LSU_REF,
    'SSURef' : RQCReferenceDatabases.SSU_REF,
    'refseq.archaea' : RQCReferenceDatabases.REFSEQ_ARCHAEA,
    'refseq.bacteria' : RQCReferenceDatabases.REFSEQ_BACTERIA,
    'refseq.mitochondrion' : RQCReferenceDatabases.REFSEQ_MITOCHONDRION,
    'refseq.plant' : RQCReferenceDatabases.REFSEQ_PLANT,
    'refseq.plasmid' : RQCReferenceDatabases.REFSEQ_PLASMID,
    'refseq.plastid' : RQCReferenceDatabases.REFSEQ_PLASTID,
    'refseq.fungi' : RQCReferenceDatabases.REFSEQ_FUNGI,
    'refseq.viral' : RQCReferenceDatabases.REFSEQ_VIRAL,
    'green_genes16s.insa_gg16S' : RQCReferenceDatabases.GREEN_GENES,
    'JGIContaminants' : RQCReferenceDatabases.CONTAMINANTS,
    'collab16s' : RQCReferenceDatabases.COLLAB16S,
}


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions
def run_thread(target_f, opts):
    t = Thread(target=target_f, args=opts)
    t.start()
    #print('Active thread cound = %d' % threading.active_count())
    return t

def print_clock(idx=0):
    symb = r'-\|/'
    sys.stdout.write('\b%s' % symb[idx])
    sys.stdout.flush()
    idx += 1
    if idx > len(symb)-1:
        idx = 0
    return idx

'''
    return executable
    make it easy to use different excutables (e.g. module vs shifter)
'''
def get_exec(progname, bbtools=False):
    asm_map = {
        'tadpole'   : '#bbtools;tadpole.sh',
        'spades'    : '#bioboxes/spades;spades.py',
        'megahit'   : '#megahit-1.1.1;megahit',
        'velveth'    : '#biobox/velveth;velveth',
        'velvetg'    : '#biobox/velvetg;velvetg',
    }

    if bbtools:
        cmd = '#bbtools;%s' % progname
    else:
        cmd = asm_map.get(progname)
    return cmd

def checkpoint(step, lastStep=PIPE_START):
    'update the pipeline check point log with the new step (step). lastStep is the last pipeline step from the pipeline checkpoint file'

    global TIME_S
    if step.upper().startswith('START'):
        TIME_S = time.time()
    if step.upper().startswith('END'):
        dt = time.time() - TIME_S
        LOGGER.info('  TIME ELAPSED : %.2f', dt)

    LOGGER.info(' [ %s ] ', step.upper())
    if step.upper().startswith('END'):
        LOGGER.info('\n')

    if step == PIPE_START or STEP_ORDER[step] > STEP_ORDER[lastStep]:
        checkpoint_step(ASMMETA[CHECKPOINT_FILE_TAG], step)

#
def init(fastq, assembler, ksize, kmax, lastStep):
    'initialization step : gather reads info'
    #print('DEBUG init')
    status = lastStep
    readLength = None
    paired = None
    kmer = None

    ##= local helper : nearest odd kmer value that's 80% of the read length
    def read_length_to_kmer(readLen, maxKmer):
        ## take 80% of the read length, and take the nearest odd number => 2 x round( ( x -1 ) / 2 ) + 1
        readLen = readLen * 0.80
        kmer = (readLen - 1) / 2
        kmer = int(kmer + 0.5)
        kmer = (2 * kmer) + 1

        ## The max kmer length is a parameter to this function with default of 63.
        ## The max allowed kmer is 63 so set it to 63 if the returned value is greater.
        ## The max can also be user specified to any value, for example maybe the user chooses 71.
        if kmer > maxKmer:
            kmer = maxKmer
        return kmer

    ##= local helper: called in 2 places
    def initialize(fastq, assembler, kmax):
        (readLength, readLengthR1, readLengthR2, paired) = get_working_read_length(fastq, LOGGER)
        kmer = None
        if readLength > 0:
            if assembler == 'velvet':   # determine the kmer size
                if ksize:
                    kmer = int(kmer)
                else:
                    ## Maximum kmer may be used if it is specified.
                    if kmax:
                        kmax = int(kmax)
                    else:
                        kmax = 63

                    kmer = read_length_to_kmer(readLength, kmax) ## KMER_TEST
            ASMMETA[PAIRED_VALUE] = paired
            if paired:
                ASMMETA[READ_LENGTH_1_VALUE] = readLengthR1
                ASMMETA[READ_LENGTH_2_VALUE] = readLengthR2
            else:
                ASMMETA[READ_LENGTH_VALUE] = readLength
        return readLength, kmer, paired
    ##= local helper


    if STEP_ORDER[status] <= STEP_ORDER[INIT_START]:
        status = INIT_START
        checkpoint(status, lastStep)
        readLength, kmer, _ = initialize(fastq, assembler, kmax)

        if readLength == 0:
            LOGGER.info('init failure - bad read length: %d', readLength)
        else:
            status = INIT_END
            checkpoint(status, lastStep)
    else:
        readLength, kmer, paired = initialize(fastq, assembler, kmax)

    LOGGER.info('  -Read length found : %s', readLength)
    LOGGER.info('              Paired : %s', paired)

    return status, paired, readLength, kmer


def do_subsample(fastq, opath, maxReads, paired, lastStep):
    'do subsample with bbtool'
    status = lastStep

    rmax = int(maxReads)
    if paired:
        rmax /= 2
    bname = os.path.basename(fastq).replace('.gz', '').replace('.fastq', '')
    destinationFastq = os.path.join(opath, '%s.subsampled_%s.fastq' % (bname, maxReads))
    sourceFastqStats = os.path.join(opath, '%s.subsampled_%s.stats' % (bname, maxReads))

    if not os.path.isfile(destinationFastq):
        checkpoint(status, lastStep)

        LOGGER.info('do subsampling on file : %s; maxReads=%s', fastq, maxReads)

        cmds = [
            '#bbtools;reformat.sh',
            'in=%s' % fastq,
            'out=%s' % destinationFastq,
            'samplereadstarget=%d ow=t > %s 2>&1' % (rmax, sourceFastqStats)
        ]
        cmd = ' '.join(cmds)
        LOGGER.info(' RUN CMD : %s', cmd)
        stdOut, stdErr, exitCode = run_cmd(cmd)

        if exitCode == 0:
            status = SAMPLE_END
            checkpoint(status, lastStep)
            fastq = destinationFastq
            ASMMETA[SUBSAMPLE] = destinationFastq
        else:
            LOGGER.info(' cmd failed : out=%s, err=%s, exitcode=%s', stdOut, stdErr, exitCode)
            fastq = None
    else:
        LOGGER.info('  - no need to perform subsampling : have been done already\n')
        ASMMETA[SUBSAMPLE] = destinationFastq

    return status

def do_bloomf_err_correction(opath, fastq, lastStep):
    'do bloom-filter error correction'
    status = lastStep
    bname = os.path.basename(fastq).replace('.gz', '').replace('.fastq', '')
    ecfastq = os.path.join(opath, '%s.bfec.fastq' % bname)
    logfile = os.path.join(opath, '%s.bfec.log' % bname)

    if not os.path.isfile(ecfastq):
        ## if subsumpled, use subsumpled seq, else, use raw input
        if SUBSAMPLE in ASMMETA:
            infile = ASMMETA[SUBSAMPLE]
        else:
            infile = fastq

        status = BFEC_START

        checkpoint(status, lastStep)
        if os.path.isfile(infile):
            cmds = [
                '#bbtools;bbcms.sh',
                'in=%s' % infile,
                'out=%s' % ecfastq,
                'mincount=2 highcountfraction=0.6 ow=t > %s' % logfile
            ]

            cmd = ' '.join(cmds)
            LOGGER.info(' RUN CMD : %s', cmd)
            _, stdErr, exitCode = run_cmd(cmd)

            if exitCode == 0:
                status = BFEC_END
                checkpoint(status, lastStep)
                ASMMETA[ERRCORRECTION] = ecfastq
            else:
                LOGGER.info(' - cmd failed : %s', stdErr)
    else:
        LOGGER.info('  - no need to perform bloom-filter based error correction : have been done already\n')
        ASMMETA[ERRCORRECTION] = ecfastq

    return status

def asm_cmd(assembler, fastq, outputPath, paired):
    'return assembler command based on the assembler type, with full execution params'
    cmd = get_exec(assembler)
    contigs = None
    # if cmd and os.path.isfile(fastq):
    if cmd:
        asmdir = os.path.join(outputPath, assembler)
        asmopts = ASMPARAM.get(assembler, '')
        if assembler == 'tadpole':
            if not os.path.isdir(asmdir):
                os.mkdir(asmdir)
            contigs = os.path.join(asmdir, 'contigs.fasta')
            cmd = '%s in=%s out=%s %s' % (cmd, fastq, contigs, asmopts)
        elif assembler == 'spades':
            contigs = os.path.join(asmdir, 'contigs.fasta')
            # opts = '--cov-cutoff auto --phred-offset 33 -t 4 -m 40 --careful -k 25,55,95'
            cmd = '%s %s -o %s' % (cmd, asmopts, asmdir)
            if paired:
                cmd += ' --12 %s' % fastq
            else:
                cmd += ' -s %s' % fastq
        elif assembler == 'megahit':
            contigs = os.path.join(asmdir, 'final.contigs.fa')
            # opts = '--k-list 23,43,63,83,103,123 --continue'
            cmd = '%s %s -o %s' % (cmd, asmopts, asmdir)
            if paired:
                cmd += ' --12 %s' % fastq
            else:
                cmd += ' --read %s' % fastq
    else:
        LOGGER.error('unsupported assembler : %s', assembler)

    if not fastq:
        cmd = None

    return cmd, contigs


def do_assemble(assembler, fastq, paired, outputPath, lastStep):
    'assemble the input reads using non-velvet assembler'

    status = lastStep
    cmd, contigs = asm_cmd(assembler, fastq, outputPath, paired)

    if STEP_ORDER[status] <= STEP_ORDER[ASSEMBLE_START]:
        status = ASSEMBLE_START
        checkpoint(status, lastStep)

        if cmd:
            LOGGER.info('RUN CMD : %s', cmd)
            _, stdErr, exitCode = run_cmd(cmd)
            if exitCode == 0:
                if os.path.isfile(contigs) and os.path.getsize(contigs) > 0:
                    LOGGER.info('%s complete', assembler)
                    status = ASSEMBLE_END
                    checkpoint(status, lastStep)
                else:
                    LOGGER.error('assembly failed to produce the contig file [%s] : %s', contigs, stdErr)
            else:
                LOGGER.error('assembly failed %s', stdErr)
        else:
            LOGGER.error('failed to construct assembler command for %s', assembler)

    elif os.path.isfile(contigs):
        LOGGER.info('  - no need to perform assemble : have been done already\n')
    else:
        LOGGER.info('  - assemble has been done before, but the produced contig file is missing [%s] !\n', contigs)
        contigs = None

    return status, contigs


def do_velvet_assemble(kmer, fastq, outputPath, lastStep):
    'assemble the input reads using velvet'
    status = lastStep
    assemDirName = 'velvet'
    assemDir = os.path.join(outputPath, assemDirName)
    gchist = os.path.join(assemDir, 'GC.contigs.fa.BBDUK.hist')
    contigs = os.path.join(assemDir, 'contigs.fa')

    if STEP_ORDER[status] <= STEP_ORDER[ASSEMBLE_START]:
        status = ASSEMBLE_START
        checkpoint(status, lastStep)

        if os.path.isfile(fastq):
            zcatCmd, exitCode = get_cat_cmd(fastq, LOGGER)

            if exitCode == 0:
                cmds = [
                    '%s %s' % (zcatCmd, fastq),
                    '%s ow in=stdin.fq out=stdout.fq int=t qtrim=rl trimq=5 minlength=35 maxns=2 gchist=%s' % (get_exec('bbduk.sh', bbtools=True), gchist),
                    '%s %s %d -fastq -' % (get_exec('velveth'), assemDir, kmer)
                ]
                cmd = ' | '.join(cmds)

                LOGGER.info('RUN velveth CMD : %s', cmd)
                _, stdErr, exitCode = run_cmd(cmd)

                if exitCode == 0:
                    if os.path.isfile(os.path.join(assemDir, 'Roadmaps')):
                        LOGGER.info('velveth complete')
                        cmd = '%s %s -cov_cutoff 1 -min_contig_lgth 100' % (get_exec('velvetg'), assemDir)
                        LOGGER.info('RUN velvetg CMD : %s', cmd)
                        _, stdErr, exitCode = run_cmd(cmd)
                        if exitCode == 0:
                            if os.path.isfile(contigs):
                                LOGGER.info('velvetg complete')

                                status = ASSEMBLE_END
                                checkpoint(status, lastStep)
                            else:
                                LOGGER.error('velvetg output not produced : %s', stdErr)
                        else:
                            status = 'velvetg failed'
                            LOGGER.error('velvetg failed, exit code != 0 : %s', stdErr)
                    else:
                        LOGGER.error('velveth output checking failed : %s', stdErr)
                else:
                    LOGGER.error('velveth failed, exit code != 0 : %s', stdErr)
            else:
                LOGGER.error('get_cat_cmd failure, exit code != 0')
    else:
        LOGGER.info('  - no need to perform velvet assemble : have been done already\n')
        if not os.path.isfile(contigs):
            LOGGER.info('  - BUT assembly file missing : \n', contigs)
            contigs = None

    return status, contigs


def do_post_process(assembler, contigs, wdir, skipblast, kmer, readlength, lastStep):
    'generate plots and anaysis data, and collect needed data/files into standard files for archiving'
    status = lastStep

    asmdir = os.path.join(wdir, assembler)
    postdir = os.path.join(wdir, 'post')

    ok = True
    fqlink = None
    if STEP_ORDER[lastStep] <= STEP_ORDER[POST_ASSEMBLE_START]:
        status = POST_ASSEMBLE_START
        checkpoint(status, lastStep)

        statsFile = ASMMETA[STATS_FILE_TAG]
        filesFile = ASMMETA[FILES_FILE_TAG]
        if os.path.isfile(statsFile):
            LOGGER.info(' - delete the existing stats file : %s', statsFile)
            os.remove(statsFile)
        if os.path.isfile(filesFile):
            LOGGER.info(' - delete the existing file list file : %s', filesFile)
            os.remove(filesFile)

        ##=
        if ASMMETA[PAIRED_VALUE]:
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_READ_LENGTH_1, ASMMETA[READ_LENGTH_1_VALUE])
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_READ_LENGTH_2, ASMMETA[READ_LENGTH_2_VALUE])
        else:
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_READ_LENGTH_1, ASMMETA[READ_LENGTH_VALUE])

        ##= local helper
        def makelink(destdir, contigs, dolink=True):
            # contig file into the uniform post/contigs.fa
            slink = os.path.join(destdir, 'contigs.fa')

            if dolink:
                if os.path.isfile(slink):
                    os.remove(slink)

                LOGGER.info(' - create symlink : %s %s', contigs, slink)
                os.symlink(contigs, slink)
            return slink

        if not os.path.isdir(postdir):
            os.mkdir(postdir)
        if not os.path.islink(contigs):
            fqlink = makelink(postdir, contigs)
        else:
            fqlink = makelink(postdir, contigs, False)

        ok = assembly_summary(wdir, postdir, assembler, fqlink, kmer)

        if ok:
            ##- move into the postdir so tools/ill_velvet_stats2.sh produced file (current dir) will be postdir
            os.chdir(postdir)

            ##== create GC.contigs.fa and GC.contigs.fa.hist.gp
            if assembler == 'velvet':
                ## Velvet assembly summary is in 'Log' file. We will change this name to other meanful name later.
                summaryFile = os.path.join(asmdir, 'Log')
                LOGGER.info(' Parse velvet summary log : %s', summaryFile)

                cmd = 'tail -1 %s' % os.path.join(asmdir, summaryFile)
                LOGGER.info('  RUN comd : %s', cmd)
                stdOut, stdErr, exitCode = run_cmd(cmd)

                if exitCode == 0:
                    match = re.match(r'.+\/(\d+)', stdOut)

                    if match:
                        totalReads = match.group(1)
                        append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_READ_USED, totalReads)
                    else:
                        LOGGER.error(' - unexpected format in last line of the velvet [%s]', stdOut.strip())
                else:
                    LOGGER.error('  - cmd fail : %s', stdErr)

                ##== create GC.contigs.fa; GC.contigs.fa.hist and GC.contigs.fa.hist.gp, and the depth.* files, in post/
                LOGGER.info(' - create velvet contig stats')
                stats_sh = os.path.join(srcDir, 'tools/ill_velvet_stats2.sh')
                cmd = ' '.join([stats_sh, asmdir, postdir])
                LOGGER.info('   RUN CMD : %s', cmd)
                stdOut, stdErr, exitCode = run_cmd(cmd)

                if exitCode != 0:
                    LOGGER.error(' - cmd failed : %s', stdErr)
                    ok = False
                else:
                    LOGGER.info(' - create velvet contig stats complete')
                    ok = assembly_depth_plot(asmdir, postdir, kmer, readlength)    ## work on the post/dpeth.*
            else:
                # ~/src/jgi-rqc-pipeline/assemblyqc/tools/GCcontent.pl velvet/contigs.fa 1 > GC.contigs.fa
                # run tools/gc2gcplot.sh on GC.contigs.fa to produce GC.contigs.fa.hist and GC.contigs.fa.hist.gp
                cmd = '%s %s 1 > %s' % (os.path.join(srcDir, 'tools/GCcontent.pl'), fqlink, os.path.join(postdir, 'GC.contigs.fa'))
                LOGGER.info(' - RUN CMD : %s', cmd)
                stdOut, stdErr, exitCode = run_cmd(cmd)
                if exitCode == 0:
                    cmd = '%s GC.contigs.fa' % os.path.join(srcDir, 'tools/gc2gcplot.sh')
                    LOGGER.info(' - RUN CMD : %s', cmd)
                    stdOut, stdErr, exitCode = run_cmd(cmd)
                    if exitCode != 0:
                        LOGGER.error(' - gc2gcplot.sh failed : %s', stdErr)
                        ok = False
                else:
                    LOGGER.error(' - GCcontent.pl failed : %s', stdErr)
                    ok = False

        gchistFileName = os.path.join(postdir, 'GC.contigs.fa.hist')    # produced by tools/ill_velvet_stats2.sh for velvet, or by tools/gc2gcplot.sh for other assemblers

        if ok:
            if os.path.isfile(gchistFileName):
                ok = assembly_gc_plot(wdir, gchistFileName)
            else:
                LOGGER.error(' - %s : not produced by the previous step. ', gchistFileName)
                ok = False
        if ok:
            ok = assembly_gc_mean(gchistFileName)   ## parse GC.contigs.fa.hist

        ##== create tetramer frequency analysis files
        if ok:
            tetramerOutputFile = os.path.join(postdir, 'contigs.kmers')
            tetramerLogFile = os.path.join(postdir, 'contigs.kmers.log')
            cmd = '%s in=%s out=%s window=2000 ow> %s 2>&1' % (get_exec('tetramerfreq.sh', bbtools=True), fqlink, tetramerOutputFile, tetramerLogFile)
            LOGGER.info(' - tetramer plot freq : %s', cmd)
            stdOut, stdErr, exitCode = run_cmd(cmd)

            if exitCode == 0 and os.path.isfile(tetramerOutputFile):
                LOGGER.info(' - tetramer plot freq complete: %s', tetramerOutputFile)
                ## TODO : need to add file/stats to file-list??
            else:
                LOGGER.error(' - tetramer plot freq error : %s', stdErr)
                ok = False
        ##==

        ##== generate the tetramer PCA plot
        if ok:
            tetramerPlotFile = os.path.join(postdir, 'tetramer_assembly')
            tetramerPlotLog = os.path.join(postdir, 'show_kmer_bin.log')
            rscriptCmd = get_tool_path('Rscript', 'R')
            showKmerBinLoc = '/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/showKmerBin.R'
            cmd = '%s --vanilla %s --input %s --output %s --jpgurl bryce --doturl brycedot --label "Principal Component Analysis" > %s' %\
                                    (rscriptCmd, showKmerBinLoc, tetramerOutputFile, tetramerPlotFile, tetramerPlotLog)
            LOGGER.info(' - tetramer plot file : %s', cmd)
            stdOut, stdErr, exitCode = run_cmd(cmd)

            tplotfile = '%s.jpg' % tetramerPlotFile
            if exitCode == 0:
                if os.path.isfile(tplotfile):
                    LOGGER.info(' - tetramer plot file generated: %s', tplotfile)
                    desc = "contig level tetramer plot"
                    append_rqc_file(filesFile, desc, tplotfile, LOGGER)
                    append_rqc_stats(statsFile, desc, os.path.basename(tplotfile), LOGGER)
                else:
                    LOGGER.error(' - cmd run ok but failed to generate tetramer plot file: %s [%s]', tplotfile, stdOut)
                    ok = False
            else:
                LOGGER.error(' - cmd failed: [%s]', stdErr.strip())
                ok = False
        ##==

        ##== run blast
        tobeUsedDBNames = None  # for reporting
        if ok:
            if skipblast: ## skip refseq and nt
                tobeUsedDBNames = ['LSSURef', 'LSURef', 'SSURef', 'JGIContaminants', 'collab16s']
                refDatabases = []
                for dbname in tobeUsedDBNames:
                    refDatabases.append(REF_DB[dbname])
            else:
                tobeUsedDBNames = REF_DB.keys()
                refDatabases = REF_DB.values()

            startTime = time.time()

            for db in refDatabases:
                LOGGER.info(' - RUN blastn vs %s', db)

                ok, megablastOutputFile = run_blastplus_py(fqlink, db)

                if not ok:
                    if megablastOutputFile is None:
                        LOGGER.error('failed to run blastn against %s. ', db)
                        break
                    elif megablastOutputFile == -143:
                        LOGGER.warning('Blast overtime. Skip the search against %s.', db)
                else:
                    LOGGER.info('Successfully ran blastn of reads against %s', db)
                    LOGGER.info('Run blast %s sequentially against the refseq_db %s produced megablast_file %s', fqlink, db, megablastOutputFile)

            runtime = str(time.time() - startTime)
            LOGGER.info('Runtime serial for %d dbs (except nt) was %s', len(refDatabases), runtime)
        ##==

        if ok:
            for db in tobeUsedDBNames:
                LOGGER.info(' - assembly megablast hits for %s', db)
                ok = assembly_megablast_hits(postdir, db)

                if not ok:
                    LOGGER.error(' - assembly megablast hits for %d failed', db)
                    break

        if ok:
            status = POST_ASSEMBLE_END
            checkpoint(status, lastStep)
    else:
        LOGGER.info('  - no need to perform post assemble process : have been done already\n')
        fqlink = os.path.join(postdir, os.path.basename(contigs))

    return status, fqlink, ok

def contig_gc_plot_matplotlib(wdir, odir):
    'Same functionality as illumina_contig_gc_plot above, except this function uses matplotlib for making an html and png file plot.'

    LOGGER.info('Creating GC plot of contigs')
    gchistFileName = os.path.join(wdir, 'post/GC.contigs.fa.hist')

    if not os.path.isfile(gchistFileName):
        LOGGER.warning('Cannot locate contig GC text histogram. gnuplot contig GC plot not made: %s', gchistFileName)
        return None, None, None

    gchistPngPlotFileName = os.path.join(odir, 'GC.contigs.fa.hist.png')
    gchistHtmlPlotFileName = os.path.join(odir, 'GC.contigs.fa.hist.html')

    ## NEW 09.21.2015 sulsj
    ##
    rawDataMatrix = []
    with open(gchistFileName, 'r') as gcHistFH:
        for l in gcHistFH:
            match = re.match(r'^\|\w*\s+([\d\.]+)\s-\s([\d\.]+)\s:\s\[\s([\d]+)\s([\d\.e\+\-]+)\s([\d\.e\+\-]+)', l)
            if match:
                rawDataMatrix.append([float(match.group(1)), int(match.group(3))])

    rawDataMatrix = np.array(rawDataMatrix)

    fig, ax = plt.subplots()

    markerSize = 5.0
    lineWidth = 1.5

    p1 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 1], 'r', marker='o', markersize=markerSize, linewidth=lineWidth, alpha=0.5)

    ax.set_xlabel('%GC', fontsize=12, alpha=0.5)
    ax.set_ylabel('Contigs count', fontsize=12, alpha=0.5)
    ax.grid(color='gray', linestyle=':')

    ## Add tooltip
    mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p1[0], labels=list(rawDataMatrix[:, 1])))

    ## Save D3 interactive plot in html format
    mpld3.save_html(fig, gchistHtmlPlotFileName)

    ## Save Matplotlib plot in png format
    plt.savefig(gchistPngPlotFileName, dpi=fig.dpi)

    if os.path.isfile(gchistPngPlotFileName):
        LOGGER.info('GC plot of contigs successfully genearted: %s %s', gchistPngPlotFileName, gchistHtmlPlotFileName)
    else:
        LOGGER.warning('Failed to generate GC plot of contigs.')
        gchistPngPlotFileName = None
        gchistHtmlPlotFileName = None

    return gchistPngPlotFileName, gchistHtmlPlotFileName, gchistFileName


'''
run_blastplus_py

Call run_blastplus.py
'''
def run_blastplus_py(queryFastaFile, db):
    outDir = os.path.dirname(queryFastaFile)
    queryFastaFileBaseName = os.path.basename(queryFastaFile)
    dbFileBaseName = os.path.basename(db)

    runBlastnCmd = os.path.join(tooDir, 'run_blastplus_taxserver.py')
    blastOutFileNamePrefix = os.path.join(outDir, 'megablast.%s.vs%s' % (queryFastaFileBaseName, dbFileBaseName))
    timeoutCmd = get_tool_path('timeout', 'timeout')
    cmd = '%s 21600s %s -d %s -o %s -q %s -s > %s.log 2>&1 ' % (timeoutCmd, runBlastnCmd, db, outDir, queryFastaFile, blastOutFileNamePrefix)

    LOGGER.info('run_blastplus command: %s', cmd)
    _, stdErr, exitCode = run_cmd(cmd)

    ## Added timeout to terminate blast run manually after 6hrs
    ## If exitCode == 124 or exitCode = 143, this means the process exits with timeout.
    ## Timeout exits with 128 plus the signal number. 143 = 128 + 15 (SGITERM)
    ## Ref) http://stackoverflow.com/questions/4189136/waiting-for-a-command-to-return-in-a-bash-script
    ##      timeout man page ==> If the command times out, and --preserve-status is not set, then exit with status 124.
    ##
    if exitCode in (124, 143):
        ## BLAST timeout
        ## Exit with succuss so that the blast step can be skipped.
        LOGGER.info('##################################')
        LOGGER.info('BLAST TIMEOUT. JUST SKIP THE STEP.')
        LOGGER.info('##################################')
        return False, -143

    elif exitCode != 0:
        LOGGER.error('failed to run blastplus. [%s]', stdErr)
        return False, None

    else:
        LOGGER.info('run blastplus vs %s complete.', db)

    return True, blastOutFileNamePrefix

def do_sketch(wdir, contig, lastStep):
    'sketch the read file (raw or subsumpled) vs nt, refseq, silva'
    status = lastStep
    ok = True
    skip = False

    if STEP_ORDER[lastStep] <= STEP_ORDER[SKETCH_START]:
        status = SKETCH_START
        checkpoint(status, lastStep)

        stime = time.time()
        sketchOutPath = os.path.join(wdir, 'sketch')
        if os.path.isdir(sketchOutPath):
            shutil.rmtree(sketchOutPath)
        make_dir(sketchOutPath)
        # change_mod(sketchOutPath, '0755')

        seqUnitName = os.path.basename(contig)
        seqUnitName = seqUnitName.replace('.gz', '').replace('.fastq', '').replace('.fasta', '')

        comOptions = 'ow=t colors=f printtaxa=t unique2'
        sendSketchShCmd = get_exec('sendsketch.sh', bbtools=True)

        ##= local helper function
        def sketch_helper(dbName, tagName):
            sketchOutFile = os.path.join(sketchOutPath,  '%s.sketch_vs_%s.txt' % (seqUnitName, dbName))
            cmd = '%s in=%s out=%s %s %s' % (sendSketchShCmd, contig, sketchOutFile, comOptions, dbName)
            LOGGER.info(' RUN CMD : %s', cmd)
            _, stdErr, exitCode = run_cmd(cmd)
            if exitCode == 0:
                append_rqc_file(ASMMETA[FILES_FILE_TAG], tagName, sketchOutFile, LOGGER)
                return True

            LOGGER.error(' - cmd failed : %s', stdErr)
            return False

        ## NT ##########################
        ok = sketch_helper('nt', SKETCH_VS_NT_FILE_TAG)

        ## Refseq ##########################
        if ok:
            ok = sketch_helper('refseq', SKETCH_VS_REFSEQ_FILE_TAG)

        ## Silva ##########################
        if ok:
            ok = sketch_helper('silva', SKETCH_VS_SILVA_FILE_TAG)

        if ok:
            status = SKETCH_END
            checkpoint(status, lastStep)
    else:
        LOGGER.info('  - no need to perform sketching : have been done already\n')

    return status, ok

def assmbler_version(assembler):
    'return assembler version.'
    version = 'unknown'
    if assembler == 'velvet':
        cmd = get_exec('velveth')   # run velveth to get version for velvet
    else:
        cmd = get_exec(assembler)
    if assembler == 'megahit':
        version = cmd.split('megahit:')[1].split()[0]
    else:
        if assembler == 'tadpole':
            cmd = get_exec('bbversion.sh', bbtools=True)
        if cmd:
            LOGGER.info(' check assember version')
            LOGGER.info(' RUN CMD : %s' % cmd)
            stdOut, stdErr, exitCode = run_cmd(cmd)
            if exitCode == 0:
                if assembler == 'velvet':
                    for line in stdOut.split('\n'):
                        if line.startswith("Version"):
                            version = line
                            break
                elif assembler == 'tadpole':
                    version = stdOut.strip()
                elif assembler == 'spades':
                    version = stdErr.split('\n')[0].strip()
    return version

def assembly_summary(wdir, postdir, assembler, contigs, kmer):
    'to gernate report of assembly summary: kmer, contig cutoff, total nodes, n50 nodes'
    LOGGER.info('do assembly summary')
    statsFile = ASMMETA[STATS_FILE_TAG]
    filesFile = ASMMETA[FILES_FILE_TAG]

    ok = True

    append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_CONTIGS_FILESIZE, os.path.getsize(contigs), LOGGER)
    append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_ASSEMBLER, assembler, LOGGER)
    append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_ASSEMBLER_VERSION, assmbler_version(assembler), LOGGER)
    append_rqc_stats(statsFile, 'assembly assembler run params', ASMPARAM.get(assembler, ''), LOGGER)

    ##== calculate contig summary
    LOGGER.info(' use sag/micro_lib.make_asm_stats to do contig summary')
    asm_stats, asm_tsv = make_asm_stats(contigs, 'contig', postdir, filesFile, LOGGER)
    save_asm_stats(asm_tsv, 'contig', statsFile, LOGGER)

    ##== velvet-specific
    if assembler == 'velvet':
        ## Velvet assembly summary is in 'Log' file. We will change this name to other meanful name later.
        asmdir = os.path.join(wdir, assembler)
        velvetLog = os.path.join(asmdir, 'Log')
        cmd = 'tail -1 %s' % velvetLog
        LOGGER.info(' get velvet Log info')
        LOGGER.info(' RUN CMD : %s', cmd)
        stdOut, stdErr, exitCode = run_cmd(cmd)

        if exitCode == 0:
            if len(stdOut) < 1:
                LOGGER.error('velvet assembler Log tail failure : empty file %s', velvetLog)
                ok = False
        else:
            LOGGER.error('velvet assembler Log tail failure : %s', stdErr)
            ok = False

        if ok:
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_KMER, kmer, LOGGER)

            match = re.match(r'Final graph has (\d+)', stdOut)
            if match:
                totalnodes = match.group(1)

            match = re.match(r'.+n50 of (\d+)', stdOut)
            if match:
                n50 = match.group(1)

            match = re.match(r'.+max (\d+)', stdOut)
            if match:
                maxcontiglen = match.group(1)

            match = re.match(r'.+total (\d+)', stdOut)
            if match:
                totalnodeslen = match.group(1)

            match = re.match(r'.+\/(\d+)', stdOut)
            if match:
                totalreads = match.group(1)

            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_NODE_NUMBER, totalnodes, LOGGER)
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_N50, n50, LOGGER)
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_MAX_CONTIG_LENGTH, maxcontiglen, LOGGER)
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_NODE_LENGTH, totalnodeslen, LOGGER)
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_READ_NUMBER, totalreads, LOGGER)

    return ok


def assembly_gc_mean(gcfile):
    'generates average GC content % and its standard deviation and put them into database.'
    LOGGER.info('do assembly gc mean')
    statsFile = ASMMETA[STATS_FILE_TAG]

    hline = None
    LOGGER.info(' Read 1st line of file to get GC info : %s', gcfile)
    with open(gcfile, 'r') as fh:
        for hline in fh:
            hline = hline.strip()   # just get the 1st line of the file
            break

    if hline:
        match = re.match(r'.*<(\S+)\s+\+\/\-\s+(\S+)>.*', hline)
        if match:
            mean = match.group(1)
            stdev = match.group(2)
            mean = float(mean) * 100
            mean = str("%.2f" % mean)
            stdev = float(stdev) * 100
            stdev = str("%.2f" % stdev)
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_GC_MEAN, mean, LOGGER)
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_GC_STD, stdev, LOGGER)
        else:
            LOGGER.error(' wrong line format : %s', hline)
    else:
        LOGGER.error(' failed in reading file : %s', gcfile)

    return True

def assembly_gc_plot(wdir, histfile):
    'generates GC plot PNG image put it into database.'
    LOGGER.info('do assembly gc plot')
    statsFile = ASMMETA[STATS_FILE_TAG]
    filesFile = ASMMETA[FILES_FILE_TAG]

    odir = os.path.dirname(histfile)
    pngFile, htmlFile, textFile = contig_gc_plot_matplotlib(wdir, odir)

    if not pngFile or not htmlFile or not textFile:
        return False

    append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_GC_PLOT, os.path.basename(pngFile), LOGGER)

    append_rqc_file(filesFile, ASM_LEVEL_GC_HIST_IMG_FILE_TAG, htmlFile, LOGGER)
    append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_GC_PLOT, os.path.basename(htmlFile), LOGGER)

    append_rqc_file(filesFile, ASM_LEVEL_GC_HIST_FILE_TAG, textFile, LOGGER)
    append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_GC_TEXT, os.path.basename(textFile), LOGGER)

    cmd = 'grep \"^#Found\" %s' % textFile
    LOGGER.info('   RUN CMD : %s', cmd)
    stdOut, stdErr, exitCode = run_cmd(cmd)

    if exitCode == 0:
        match = re.match(r'.*(\d+\.\d+) \+\/\- (\d+\.\d+).*', stdOut)
        if match:
            mean = float(match.group(1)) * 100
            mean = str("%.2f" % mean)
            stdev = float(match.group(2)) * 100
            stdev = str("%.2f" % stdev)
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_GC_TEXT_AVG, mean, LOGGER)
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_GC_TEXT_STD, stdev, LOGGER)

    else:
        LOGGER.error(' cmd failure : %s', stdErr)
        return False

    return True

def genome_size_info(textHistFile):
    'parse the 1st line of depth hist text file and get estimated genome size, mean depth and std'
    if DEBUG:
        print(' >> DEBUG genome_size_info: textHistFile=%s' % textHistFile)

    gline = None
    bline = None
    LOGGER.info(' Read 1st and 3rd lines of file to get genome size info : %s', textHistFile)
    with open(textHistFile, 'r') as fh:
        for (idx, lin) in enumerate(fh):
            if idx == 0:
                gline = lin.strip() # 1st line in velvet output depth.* file; #Found 4991838 total values totalling 423801475.0027. <84.898884 +/- 46.135422>
            elif idx == 2:
                bline = lin.strip() # 3rd line in velvet output depth.* file; #Most likely bin: [ 70 - 80 ] 1201177 counts
                break
    gsize = None
    mean = None
    stdev = None
    binn = None
    if gline and bline:
        myArray = re.findall(r'(\d+)', gline)   ###($str =~ m/(\d+)/g)
        if myArray and len(myArray) > 6:
            gsize = myArray[0]
            mean = '%.2f' % float('%s.%s' % (myArray[3], myArray[4]))
            stdev = '%.2f' % float('%s.%s' % (myArray[5], myArray[6]))

        match = re.match(r'.*\[ (.+) \].*', bline)
        if match:
            binn = abs(eval(match.group(1)))    # causing pylint "eval-used" warning!

    return (gsize, mean, stdev, binn)

def assembly_depth_plot(asmdir, postdir, kmer, readLen):
    'only for velvet assemble - generates GC plot PNG image, text hist put them into database.'

    LOGGER.info('do assembly depth plot')
    statsFile = ASMMETA[STATS_FILE_TAG]
    filesFile = ASMMETA[FILES_FILE_TAG]

    if asmdir.find('velvet') == -1:
        # not velvet, try to collect genome size from bbtool stats.sh output
        LOGGER.info(' - not velvet run. skip')
        return True

    ##
    ## To find all depth.***** files
    ##
    depthFileList = []
    for filename in os.listdir(postdir):
        match = re.match(r'(depth.\S*\d)$', filename)
        if match:
            depthFile = match.group(1)
            depthFileList.append(os.path.join(postdir, depthFile))

    fileCount = len(depthFileList)

    if fileCount != 6:
        LOGGER.error("The right number of assembly depth hist text files were not found in folder %s.", postdir)
        # print(depthFileList)
        return False

    for depthFile in depthFileList:
        ##== get the range of depth, the last number in the file name
        strDepFile = depthFile.strip()
        myArray = re.findall(r'(\d+)', strDepFile)
        rangeArray = myArray[-1]

        ret = genome_size_info(depthFile)
        retCount = len(ret)
        binnn = 0

        if retCount >= 4:
            append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_GENOME_SIZE, rangeArray), ret[0], LOGGER)
            depthMean = ret[1]
            append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_DEPTH_MEAN, rangeArray), depthMean, LOGGER)
            depthStd = ret[2]
            append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_DEPTH_STD, rangeArray), depthStd, LOGGER)

            #These statistics would be good to move to the assemblyqc piepline code, but the kmer and read length1 and depth mean/std are all computed in different places of the pipeline.
            if kmer and depthMean and depthStd and readLen:
                bpdepth = float(depthMean) * readLen / (readLen - kmer + 1)
                bpstd = float(depthStd) * readLen / (readLen - kmer + 1)
                bpdepth = '%.2f' % (float(bpdepth))
                bpstd = '%.2f' % (float(bpstd))

                append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_BPDEPTH_MEAN, rangeArray), bpdepth, LOGGER)
                append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_BPDEPTH_STD, rangeArray), bpstd, LOGGER)

            append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_DEPTH_BIN, rangeArray), ret[3], LOGGER)
            binnn = ret[3]

        gnuplotPngFile = '%s.png' % depthFile

        ## shifter --image=bryce911/bbtools gnuplot
        gnuplotTool = get_tool_path("gnuplot", "gnuplot")

        gnuplotPngFile = os.path.join(postdir, os.path.basename(gnuplotPngFile))
        cmds = [
            "echo \"set title 'Contig Length Weighted Depth 0-%sx'" % rangeArray,
            "set xlabel 'Kmer Depth bin %d'" % binnn,
            "set ylabel 'Assembled Bases'; set output '%s'" % gnuplotPngFile,
            "set terminal png; set grid",
            "plot '%s' u 2:7 w lp title 'Kmer Depth'; replot;\" | %s;" % (depthFile, gnuplotTool)
        ]
        cmd = '; '.join(cmds)

        LOGGER.info(' RUN CMD : %s', cmd)
        _, stdErr, exitCode = run_cmd(cmd)

        if exitCode != 0:
            LOGGER.error(' CMD failed : %s ', stdErr)


        depFileBasename = os.path.basename(depthFile)

        append_rqc_file(filesFile, 'assembly level depth text hist file 0-%s' % rangeArray, depthFile, LOGGER)
        append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_DEPTH_TEXT, rangeArray), depFileBasename, LOGGER)

        pngPlotFileBasename = os.path.basename(gnuplotPngFile)

        append_rqc_file(filesFile, 'assembly level depth hist PNG image file 0-%s' % rangeArray, gnuplotPngFile, LOGGER)
        append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_DEPTH_PLOT, rangeArray), pngPlotFileBasename, LOGGER)

    return True

def assembly_megablast_hits(blastdir, dbName):
    'generates tophit list of megablast against different databases.'

    LOGGER.info(' - parse assembly megablast hits against %s', dbName)
    statsFile = ASMMETA[STATS_FILE_TAG]
    filesFile = ASMMETA[FILES_FILE_TAG]

    ## To find the matching hits
    numMatchingHits = 0
    parsedFileName = ''
    parsedFileAbspath = ''
    for filename in os.listdir(blastdir):
        match = re.match('(megablast\.\S+\.%s\S*\.parsed)' % dbName, filename)
        if match:
            parsedFileName = match.group(1)

    if parsedFileName != '':
        parsedFileAbspath = os.path.join(blastdir, parsedFileName)
        numMatchingHits, _ = calculate_line_total(parsedFileAbspath, LOGGER)
        # LOGGER.info("numMatchingHits: " + str(numMatchingHits) + " parsedFileAbspath " + str(parsedFileAbspath))

    if type(numMatchingHits) is int and numMatchingHits >= 2:
        numMatchingHits = numMatchingHits - 2

    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_MATCHING_HITS, dbName), numMatchingHits, LOGGER)
    append_rqc_file(filesFile, 'assembly level .parsed file of %s database' % dbName, parsedFileAbspath, LOGGER)
    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_PARSED_FILE, dbName), parsedFileName, LOGGER)

    ##== To find the top hits
    numTophits = 0
    tophitsFileName = ''
    tophitsFileAbspath = ''

    for filename in os.listdir(blastdir):
        match = re.match('(megablast\.\S+\.%s\S*\.parsed\.tophit)' % dbName, filename)
        if match:
            tophitsFileName = match.group(1)

    if tophitsFileName != '':
        tophitsFileAbspath = os.path.join(blastdir, tophitsFileName)
        numTophits, _ = calculate_line_total(tophitsFileAbspath, LOGGER)
        # log.info("numTophits: " + str(numTophits) + " tophitsFileAbspath " + str(tophitsFileAbspath))

    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TOP_HITS, dbName), numTophits, LOGGER)
    append_rqc_file(filesFile, 'assembly level top hit file of %s database' % dbName, tophitsFileAbspath, LOGGER)
    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TOPHIT_FILE, dbName), tophitsFileName, LOGGER)


    ##== To find the top 100 hits
    numTop100hits = 0
    top100hitsFileName = ''
    top100hitsFileAbspath = ''

    for filename in os.listdir(blastdir):
        match = re.match('(megablast\.\S+\.%s\S*\.parsed\.top100hit)' % dbName, filename)
        if match:
            top100hitsFileName = match.group(1)

    if top100hitsFileName != '':
        top100hitsFileAbspath = os.path.join(blastdir, top100hitsFileName)
        numTop100hits, _ = calculate_line_total(top100hitsFileAbspath, LOGGER)
        # log.info("numTop100hits: " + str(numTop100hits) + " top100hitsFileAbspath " + str(top100hitsFileAbspath))

    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TOP_100_HITS, dbName), numTop100hits, LOGGER)
    append_rqc_file(filesFile, 'assembly level top 100 hit file of %s database' % dbName, top100hitsFileAbspath, LOGGER)
    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TOP100HIT_FILE, dbName), top100hitsFileName, LOGGER)


    ##== To find the taxonomic species
    numSpecies = 0
    taxlistFileName = ""
    taxlistFileAbspath = ""

    for filename in os.listdir(blastdir):
        match = re.match("(megablast\.\S+\.%s\S*\.parsed\.taxlist)" % dbName, filename)
        if match:
            taxlistFileName = match.group(1)

    if taxlistFileName != "":
        taxlistFileAbspath = os.path.join(blastdir, taxlistFileName)
        numSpecies, _ = calculate_line_total(taxlistFileAbspath, LOGGER)
        # log.info("numSpecies: " + str(numSpecies) + " taxlistFileAbspath " + str(taxlistFileAbspath))

    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TAX_SPECIES, dbName), numSpecies, LOGGER)
    append_rqc_file(filesFile, 'assembly level taxlist file of %s database' % dbName, taxlistFileAbspath, LOGGER)
    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TAXLIST_FILE, dbName), taxlistFileName, LOGGER)

    return True

def cleanup_assemblyqc(wdir, assembler):
    # in wdir:
    subsample = ['*.subsampled*.fastq']
    bfecfile = ['*subsampled*.bfec.*']

    # assembler specific files
    asmfiles = {'velvet' : ['Graph', 'Sequences', 'Roadmaps'],
                'megahit' : ['intermediate_contigs'],
                'spades' : ['K*', 'assembly_graph*', 'before_rr.fasta', 'corrected', 'mis*', 'tmp', 'split_input'],
                'tadpole' : []
    }

    # in post dir
    postfiles = ['GC.contigs.fa', 'contigs.kmers']

    # core file in wDir, assembler dir, post dir
    def rm_from_dir(alist, adir):
        for fname in alist:
            fpath = os.path.join(adir, fname)
            cmd = '/bin/rm -rf %s' % fpath
            LOGGER.info('Removing file : %s', cmd)
            run_cmd(cmd)

    rm_from_dir(asmfiles[assembler], os.path.join(wdir, assembler))
    rm_from_dir(postfiles, os.path.join(wdir, 'post'))
    wdirfiles = subsample + bfecfile
    rm_from_dir(wdirfiles, wdir)

'''
    config file contain lines in form of
    KEY = VALUE
'''
def read_asm_opts(wdir):
    'copy the default config file into run dir, and readin the assembler run options'
    src = os.path.join(srcDir, 'config/assembler.cfg')
    dst = os.path.join(wdir, os.path.basename(src))
    if not os.path.isfile(dst): #
        shutil.copyfile(src, dst)
    opts = {}
    with open(dst) as fh:
        for line in fh:
            line = line.strip()
            if not line or line.startswith('#'):
                continue

            tok = line.split('=')   # no "=" is allowed in key and value strings
            if len(tok) > 1:
                opts[tok[0].strip()] = tok[1].strip()
    return opts
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program
if __name__ == '__main__':
    PROG_NAME = 'RQC assembly-qc pipeline'

    PROG_START_TIME = time.time()

    JOB_CMD = ' '.join(sys.argv)

    PARSER = argparse.ArgumentParser(description='Asseblyqc v2')

    ASSEMBLER = ['tadpole', 'spades', 'megahit', 'velvet']
    HELPS = {
        'nocleanup' : 'Skip temporary file cleanup',
        'nec'   : 'Do bloom filter based error correction before assemble',
        'kmer' : 'Set k-mer size may be user-specified. Has precedence over the -m option. ' +
                'If -k is unspecified, then usually 80 percent of the read length will be used as kmer size, ' +
                'up to a maximum value as specified with -m if specified',
        'maxkmer' : 'Set maximum k-mer size. Only has utility if the kmer size is not specified with the -k ' +
                    'option. If -k is unspecified but -m is specified, then 80 percent of the read length will ' +
                    'be used as kmer size, up to a maximum value as specified with -m',
    }

    PARSER.add_argument('-b', '--skip-blast', dest='skipBlastFlag', action='store_true', help='Skip Blast search', default=False)
    PARSER.add_argument('-c', '--skip-cleanup', dest='skipCleanup', action='store_true', default=False, help=HELPS['nocleanup'])
    PARSER.add_argument('-f', '--fastq', dest='fastq', help='Set input Fastq file (full path to fastq)', required=True)
    PARSER.add_argument('-k', '--kmer', dest='kmer', help=HELPS['kmer'], required=False)
    PARSER.add_argument('-m', '--max-kmer', dest='maxKmer', help=HELPS['maxkmer'], required=False)
    PARSER.add_argument('-o', '--output-path', dest='outputPath', help='Set output path to write to', required=True)
    PARSER.add_argument('-r', '--max-reads', dest='maxReads', help='Set maximum number of reads to subsample', required=False)
    PARSER.add_argument('-nec', '--error-correction', dest='bfec', action='store_false', default=True, help=HELPS['nec'])
    PARSER.add_argument('-a', '--assembler', dest='assembler', help='assembler name', choices=ASSEMBLER, default=ASSEMBLER[0])
    PARSER.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = 'print log to screen')
    PARSER.add_argument("-mt", "--multi-thread", dest="mthread", default = False, action = "store_true", help = 'run multithread version')
    PARSER.add_argument('-v', '--version', action='version', version=VERSION)

    ARGS = PARSER.parse_args()

    SEQFILE = None

    if ARGS.fastq:
        FASTQ = ARGS.fastq

    ## use current directory if no output path
    if not ARGS.outputPath:
        ARGS.outputPath = os.getcwd()

    ARGS.outputPath = os.path.realpath(ARGS.outputPath)

    ## create output_directory if it doesn't exist
    if not os.path.isdir(ARGS.outputPath):
        print('Cannot find %s, creating as new' % ARGS.outputPath)
        os.makedirs(ARGS.outputPath)

    ## initialize my logger
    LOG_FILE = os.path.join(ARGS.outputPath, 'assemblyqc.log')
    LOG_LEVEL = 'DEBUG'
    LOGGER = get_logger('assemblyqc', LOG_FILE, LOG_LEVEL, ARGS.print_log)

    print('Started assemblyqc pipeline, writing log to: %s' % LOG_FILE)

    if not os.path.isfile(FASTQ):
        LOGGER.error('Cannot find fastq: %s!', FASTQ)
        sys.exit(2)

    ## Create the standard files:
    ASMMETA = {}
    ASMMETA[FILES_FILE_TAG] = os.path.join(ARGS.outputPath, 'rqc-files.txt')
    ASMMETA[STATS_FILE_TAG] = os.path.join(ARGS.outputPath, 'rqc-stats.txt')
    ASMMETA[CHECKPOINT_FILE_TAG] = os.path.join(ARGS.outputPath, 'status.log')

    STATUS = get_status(ASMMETA[CHECKPOINT_FILE_TAG], LOGGER)
    if STATUS == 'complete':
        LOGGER.info('Status is complete, not processing.')
        sys.exit(0)

    LOGGER.info('The fastq file is %s', FASTQ)

    NERSC_HOST = os.environ['NERSC_HOST']

    LOGGER.info('CMD : %s', JOB_CMD)
    LOGGER.info('=================================================================')
    LOGGER.info('Assembly Qc Analysis (version %s)', VERSION)
    LOGGER.info('   %20s : %s', 'outdir', ARGS.outputPath)
    LOGGER.info('   %20s : %s', 'assembler', ARGS.assembler)
    LOGGER.info('   %20s : %s', 'skip blast', ARGS.skipBlastFlag)
    LOGGER.info('   %20s : %s', 'skip cleanup', ARGS.skipCleanup)
    LOGGER.info('   %20s : %s', 'kmer size', ARGS.kmer)
    LOGGER.info('   %20s : %s', 'max kmer size', ARGS.maxKmer)
    LOGGER.info('   %20s : %s', 'subsample size', ARGS.maxReads)
    LOGGER.info('   %20s : %s', 'do err correction', ARGS.bfec)
    LOGGER.info('   %20s : %s', 'running on', ARGS.bfec)
    LOGGER.info('=================================================================')

    LOGGER.info('Starting %s on %s', PROG_NAME, FASTQ)

    if ARGS.maxReads and int(ARGS.maxReads) < 1:
        LOGGER.error('subsample reads must be > 1 [%s]. Can not continue!', ARGS.maxReads)
        sys.exit(2)

    if STATUS == 'start':
        STATUS = PIPE_START
        checkpoint(STATUS)
    elif STATUS == PIPE_COMPLETE:
        LOGGER.warning('Pipeline has been completed before. Do nothing!')
        sys.exit(0)
    else:
        LOGGER.info('\nLast step -- [ %s ]', STATUS.upper())

    STATUS, PAIRED, RLENGTH, KSIZE = init(FASTQ, ARGS.assembler, ARGS.kmer, ARGS.maxKmer, STATUS)

    if not RLENGTH or RLENGTH < 1:
        sys.exit(2)

    ASMPARAM = read_asm_opts(ARGS.outputPath)

    if ARGS.maxReads:
        ## subsample file will be stored in ASMMETA[SUBSAMPLE]
        STATUS = do_subsample(FASTQ, ARGS.outputPath, ARGS.maxReads, PAIRED, STATUS)
    else:
        LOGGER.info('No subsampling is requested by user.\n')

    if ARGS.bfec:
        ## error correction file will be stored in ASMMETA[ERRCORRECTION]
        STATUS = do_bloomf_err_correction(ARGS.outputPath, FASTQ, STATUS)
    else:
        LOGGER.info('No error correction is requested by user.\n')

    if ERRCORRECTION in ASMMETA:
        SEQFILE = ASMMETA[ERRCORRECTION]
    elif SUBSAMPLE in ASMMETA:
        SEQFILE = ASMMETA[SUBSAMPLE]
    else:
        SEQFILE = FASTQ

    LOGGER.info('Assembly input file : %s' % SEQFILE)

    CONTIGS = None
    if SEQFILE:
        if ARGS.assembler == 'velvet':
            ## TODO : command pipe does not work with the run_cmd() !!
            STATUS, CONTIGS = do_velvet_assemble(KSIZE, SEQFILE, ARGS.outputPath, STATUS)
        else:
            STATUS, CONTIGS = do_assemble(ARGS.assembler, SEQFILE, PAIRED, ARGS.outputPath, STATUS)

    if CONTIGS:
        STATUS, CONTIG, OK = do_post_process(ARGS.assembler, CONTIGS, ARGS.outputPath, ARGS.skipBlastFlag, KSIZE, RLENGTH, STATUS)
        if not OK:
            LOGGER.error('postprocessing failed!')
            sys.exit(1)
        else:
            STATUS, OK = do_sketch(ARGS.outputPath, CONTIGS, STATUS)

        if not OK:
            LOGGER.error('Pipeline failed!')
            sys.exit(1)
        else:
            if not ARGS.skipCleanup:
                LOGGER.info(' == Do clean up ==')
                cleanup_assemblyqc(ARGS.outputPath, ARGS.assembler)
            else:
                LOGGER.info(' == Skip clean up! ==')

            checkpoint(PIPE_COMPLETE, STATUS)

    ##== run time info
    LOGGER.info('Total wallclock runtime: %s', str(time.time() - PROG_START_TIME))

## EOF
