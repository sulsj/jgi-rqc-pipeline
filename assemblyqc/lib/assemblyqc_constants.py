#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Constants specific for RQC pipelines.
This file holds specific values for the assemblyQC pipeline.
Global constants are defined in a file under the directory lib/rqc_constants.py .
"""

import os

from common import get_run_path
from os_utility import get_tool_path


class RQCAssemblyQcConfig:
    CFG = {}
    CFG["output_path"] = ""
    CFG["status_file"] = ""
    CFG["files_file"] = ""
    CFG["stats_file"] = ""
    CFG["log_file"] = ""
    CFG["no_cleanup"] = ""
    CFG["run_path"] = ""


# Profiling constants
# class RQCProfilingAssembly:
#     RQC_PROFILING_MEGABLAST_MULTITHREADED = 0
#     RQC_PROFILING_LOCALIZED_FILES = 0
#     RQC_PROFILING = 0


# The locations of different directories are hard-coded
class AssemblyQCPaths:
    RQC_SW_BIN_DIR = os.path.join(get_run_path(), "assemblyqc/tools")


# Velvet specific constants for assemblyQC
class RQCVelvet:
    VELVETG = "module load velvet; velvetg-k96_c2 " ### NOJGITOOLS /jgi/tools/bin/velvetg "
    VELVETH = "velveth-k96_c2 " ### NOJGITOOLS /jgi/tools/bin/velveth-k96 "
    VELVETH_WRAPPER = AssemblyQCPaths.RQC_SW_BIN_DIR + "/velvet_assembly.sh "
    ILL_VELVET_STATS_SH = AssemblyQCPaths.RQC_SW_BIN_DIR + "/ill_velvet_stats.sh "
    FASTQ_TRIMMER = get_tool_path('fastqTrimmer', 'qaqc')

    BBDUK_TRIMMER = "bbduk.sh "
    TETRAMER_CMD = "module load jgitools; module load qaqc; module load R; /global/projectb/sandbox/rqc/prod/pipelines/jigsaw/DEFAULT/bin/tetramerKMWrap.pl " ### /house/groupdirs/QAQC/scripts.moved/versions/DEFAULT/bin/tetramerKMWrap.pl " ### NOJGITOOLS


class AssemblyStats:
    ILLUMINA_ASSEMBLY = "assembly level"
    ILLUMINA_ASSEMBLY_ASSEMBLER = "assembly assembler"
    ILLUMINA_ASSEMBLY_ASSEMBLER_VERSION = "assembly assembler version"
    ILLUMINA_ASSEMBLY_KMER = "assembly kmer"
    ILLUMINA_ASSEMBLY_COV_CUTOFF = "assembly coverage cutoff"
    ILLUMINA_ASSEMBLY_MIN_CONTIG_LENGTH = "assembly min contig length"
    ILLUMINA_ASSEMBLY_NODE_NUMBER = "assembly node number"
    ILLUMINA_ASSEMBLY_NODE_LENGTH = "assembly all node length"
    ILLUMINA_ASSEMBLY_N50 = "assembly n50"
    ILLUMINA_ASSEMBLY_MAX_CONTIG_LENGTH = "assembly max contig length"
    ILLUMINA_ASSEMBLY_READ_NUMBER = "assembly read number"
    ILLUMINA_ASSEMBLY_CONTIG_STATS_SUMMARY = "assembly contig stats summary"
    ILLUMINA_ASSEMBLY_TOP5_CONTIG_FILE = "assembly top 5 contig file"
    ILLUMINA_ASSEMBLY_GC_MEAN = "assembly GC mean"
    ILLUMINA_ASSEMBLY_GC_STD = "assembly GC std"
    ILLUMINA_ASSEMBLY_GC_PLOT = "assembly GC plot"
    ILLUMINA_ASSEMBLY_GC_TEXT = "assembly GC text hist"
    ILLUMINA_ASSEMBLY_GC_TEXT_AVG = "assembly GC text hist avg"
    ILLUMINA_ASSEMBLY_GC_TEXT_STD = "assembly GC text hist std"
    ILLUMINA_ASSEMBLY_DEPTH_PLOT = "assembly depth hist plot"
    ILLUMINA_ASSEMBLY_DEPTH_TEXT = "assembly depth text hist"
    ILLUMINA_ASSEMBLY_GENOME_SIZE = "assembly genome size"
    ILLUMINA_ASSEMBLY_DEPTH_MEAN = "assembly depth mean"
    ILLUMINA_ASSEMBLY_DEPTH_STD = "assembly depth std"
    ILLUMINA_ASSEMBLY_BPDEPTH_MEAN = "assembly bpdepth mean"
    ILLUMINA_ASSEMBLY_BPDEPTH_STD = "assembly bpdepth std"
    ILLUMINA_ASSEMBLY_DEPTH_BIN = "assembly depth bin"
    ILLUMINA_ASSEMBLY_CONTIG_NUMBER = "assembly contig number"
    ILLUMINA_ASSEMBLY_MATCHING_HITS = "assembly matching hits of"
    ILLUMINA_ASSEMBLY_TOP_HITS = "assembly top hits of"
    ILLUMINA_ASSEMBLY_TOP_100_HITS = "assembly top 100 hits of"
    ILLUMINA_ASSEMBLY_TAX_SPECIES = "assembly tax species of"
    ILLUMINA_ASSEMBLY_PARSED_FILE = "assembly parsed file of"
    ILLUMINA_ASSEMBLY_TAXLIST_FILE = "assembly taxlist file of"
    ILLUMINA_ASSEMBLY_TOPHIT_FILE = "assembly tophit file of"
    ILLUMINA_ASSEMBLY_TOP100HIT_FILE = "assembly top 100 hit file of"
    ILLUMINA_ASSEMBLY_READ_USED = "assembly read used"
    ILLUMINA_READ_LENGTH_1 = "read length 1"
    ILLUMINA_READ_LENGTH_2 = "read length 2"
    ILLUMINA_CONTIGS_FILESIZE = "assembly contigs filesize"


## EOF
