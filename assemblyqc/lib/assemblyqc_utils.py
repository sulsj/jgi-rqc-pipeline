#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re

from common import checkpoint_step
from os_utility import run_sh_command, get_tool_path
from rqc_constants import RQCCommands, RQCExitCodes, RQCReferenceDatabases
from rqc_utility import safe_dirname, safe_basename, get_cat_cmd


"""
For testing

Compare the results from two separate runs

"""
def compare(outFile, goldFile):
    outFileDict = {}

    ## read outFile into dictionary
    with open(outFile, 'r') as outFH:
        for line in outFH:
            if line == '':
                break
            else:
                kv = line.split(' = ')
                outFileDict[str(kv[0])] = str(kv[1])

    outFileDictKeys = outFileDict.keys()

    goldFileDict = {}

    ## read goldFile into dictionary
    with open(goldFile, 'r') as goldFH:
        for line in goldFH:
            if line == '':
                break
            else:
                kv = line.split(' = ')
                goldFileDict[str(kv[0])] = str(kv[1])

    goldFileDictKeys = goldFileDict.keys()

    assert len(set(outFileDictKeys)) >= len(set(goldFileDictKeys)) - 3

    for o in outFileDict.keys():
        ovalue = outFileDict.get(o)
        gvalue = goldFileDict.get(o)
        print "Considering key %s : out value %s vs. gold value %s " % (o, ovalue, gvalue)

        if is_number(ovalue) and is_number(gvalue):
            if float(ovalue) > 0 and float(gvalue) > 0 and o.find(" hits ") < 0 and o.find("tax spec") < 0:
                assert (float(gvalue) == float(ovalue)) or abs(float(gvalue) - float(ovalue)) <= abs(max(float(ovalue), float(gvalue)) * 3)

        if os.path.isdir(ovalue) and os.path.isdir(gvalue):
            assert os.path.exists(ovalue)
            assert os.path.exists(gvalue)
            assert os.path.getsize(ovalue) > 0
            assert os.path.getsize(gvalue) > 0


def is_number(s):
    try:
        float(s)
        return True
    except Exception:
        return False


## TODO Put in a list and iterate. Also the *Graph files should be removed. Also reads-*.fasta , reads-*GC
def cleanup_assemblyqc(workingDir, log):
    toBeDeleted = ["*Graph", "reads-*.fasta", "reads-*GC", "*fastq.velvet.k*/megablast.*W45", "*fastq.velvet.k*/Sequences", "*fastq.velvet.k*/Roadmaps", "*fastq.velvet.k*/*.rma", "*.subsampled*.fastq", "core", "*fastq.velvet.k*/core", "*fastq.velvet.k*/GC.contigs.fa"]

    for toDel in toBeDeleted:
        ## Delete unneeded files that take too much space
        toDelPath = os.path.join(workingDir, toDel)
        cmd = "/bin/rm " + toDelPath
        log.info("Removing files: " + cmd)
        run_sh_command(cmd, True, log)


"""
Performs the subsampling of the fastq file passed in.
Returns a destination fastq file dest_fastq.
"""
def illumina_subsampling(sourceFastq, outputPath, status_log, maxSubsReads, isFqPaired, log):
    ## Check if maxSubsReads makes sense, else exit with failure
    if int(maxSubsReads) < 1:
        status = "illumina_subsampling maxSubsReads failed"
        log.error(status + " " + str(maxSubsReads))

        return "illumina_subsampling failed", None

    else:
        status = "subsampling"
        log.info(status + " after max_subsampl_reads in fastq " + str(maxSubsReads))
        checkpoint_step(status_log, status)


    ## If lib is pair-ended then need to divide by 2 because reformat subsamples the number of fragments, not reads. set_max_allowed_reads("ZZZZZZZZZ", log)
    readForAssembly = int(maxSubsReads)

    if isFqPaired:
        readForAssembly = int(maxSubsReads) / 2

    samplingOption = " samplereadstarget=" + str(readForAssembly)

    ## fastq is a full path name so get the basename of the fastq file to use as part of the other filenames,
    ## such as sourceFastqStats and destinationFastq.
    subsampledFastq, exitCode = safe_basename(sourceFastq, log)
    subsampledFastq = subsampledFastq.replace(".gz", "")
    subsampledFastq = subsampledFastq.replace(".fastq", "")
    sourceFastqStats = outputPath + "/" + subsampledFastq + ".stats"
    destinationFastq = outputPath + "/" + subsampledFastq + ".subsampled" + str(maxSubsReads) + ".fastq"

    bbtoolsReformatSh = get_tool_path("reformat.sh", "bbtools")
    # subsamplingCmd = RQCCommands.BBTOOLS_REFORMAT_CMD + " in=" + sourceFastq + " out=" + destinationFastq + " " + samplingOption + " ow=t > " + sourceFastqStats + " 2>&1"
    subsamplingCmd = "%s in=%s out=%s %s ow=t > %s 2>&1" % (bbtoolsReformatSh, sourceFastq, destinationFastq, samplingOption, sourceFastqStats)
    log.info("Subsample command is: " + subsamplingCmd)

    _, _, exitCode = run_sh_command(subsamplingCmd, True, log, True)

    if exitCode == 0:
        status = "subsamplingCmd complete"
        checkpoint_step(status_log, status)
    else:
        status = "subsamplingCmd failed"
        log.error("- subsamplingCmd failed, exit code != 0")
        checkpoint_step(status_log, status)
        return "illumina_subsampling failed", None


    ## Check if the source fastq stats make sense.
    basecountTotalRef = None
    readTotalRef = None

    with open(sourceFastqStats, 'r') as statFH:
        for line in statFH:
            if re.match("(O|o)utput:(.*)", line):
                lineref = line.strip().split("\t")
                readTotalRef = lineref[1].strip().split(" ")[0]
                basecountTotalRef = lineref[2].strip().split(" ")[0]

    if basecountTotalRef is None or readTotalRef is None or readTotalRef < 1 or basecountTotalRef < 1:
        status = "sourceFastq illumina_subsampling readCount failed"
        log.error(status + ": " + str(sourceFastqStats) + " has " + str(basecountTotalRef) + " - " + str(readTotalRef))  #Unable to open the source FASTQ stats file for reading!
        checkpoint_step(status_log, status)
        return "illumina_subsampling failed", None
    else:
        #status = "sourceFastq illumina_subsampling readCount"
        log.info("Total Base Count = " + basecountTotalRef)
        log.info("Total Read = " + readTotalRef)


    ## Check if the destination fastq stats(read count) make sense.
    readCount, exitCode = calculate_read_total_fastq(destinationFastq, log) ## `$zcatCmd $sourceFastq | grep "^@" | wc -l`;

    if exitCode != RQCExitCodes.JGI_SUCCESS or readCount < 1:
        status = "dest_fastq illumina_subsampling readCount failed"
        log.error(status + ": " + str(readCount) + " for " + str(destinationFastq))
        checkpoint_step(status_log, status)
        return "illumina_subsampling failed", None
    else:
        status = "dest_fastq illumina_subsampling readCount"
        log.info(status + " "  + str(readCount) + " for " + str(destinationFastq))


    return "start", destinationFastq



"""
calculate_line_total
Returns: number of lines in the input text file. The cat command runs on the file by default.
In case of failure returns non-digit (or negative number) , exitCode
"""
def calculate_line_total(inFile, log):
    ##
    ## Construct and run the read total system command.
    ##
    # cmd = "cat " + inFile + " | wc -l | awk \"{print $1}\""
    cmd = "grep -v '^#' " + inFile + " | wc -l "
    stdOut, _, exitCode = run_sh_command(cmd, True, log, True)

    if exitCode == 0:
        stdOut = stdOut.strip()
        if stdOut.isdigit():
            status = "cat_file complete"
            log.info(status)
        else:
            status = "cat_file failed. The calculated line total was not an integer!"
            log.error("- cat_file failed. The calculated line total was not an integer!")
            log.info(status)

            return status, RQCExitCodes.JGI_FAILURE
    else:
        status = "cat_file failed"
        log.error("- cat_file failure, Unable to run the line total command! exit code != 0")
        log.info(status)

        return status, RQCExitCodes.JGI_FAILURE

    ##
    ## If one gets here, everything must have worked, so record the elapsed
    ## time and return success.
    ##
    status = "calculate_line_total complete"
    log.info(status)


    return stdOut, RQCExitCodes.JGI_SUCCESS


"""
calculate_read_total_fastq
Returns: number of lines in fastq file
In case of failure returns non-digit (or negative number) , exitCode
"""
def calculate_read_total_fastq(fastqFile, log=None):
    ##
    ## Open the source FASTQ file for reading, and the destination one
    ## for writing.
    ##
    zcatCmd, exitCode = get_cat_cmd(fastqFile, log)

    ##
    ## Construct and run the read total system command.
    ##
    readCountCmd = zcatCmd + " " + fastqFile + " | grep \"^@\" | wc -l | awk \"{print $1}\""

    stdOut, _, exitCode = run_sh_command(readCountCmd, True, log, True)

    if exitCode == 0:
        stdOut = stdOut.strip()
        if stdOut.isdigit():
            status = "cat_fastq complete"
        else:
            status = "cat_fastq failed. The calculated read total was not an integer!"
            if log:
                log.error("- cat_fastq failed. The calculated read total was not an integer!")

            return status, RQCExitCodes.JGI_FAILURE
    else:
        status = "cat_fastq failed"
        if log:
            log.error("- cat_fastq failure, Unable to run the read total command! exit code != 0")

        return status, RQCExitCodes.JGI_FAILURE

    ##
    ## If one gets here, everything must have worked, so record the elapsed
    ## time and return success.
    ##
    status = "calculate_read_total_fastq complete"
    log.info(status)


    return stdOut, RQCExitCodes.JGI_SUCCESS


"""
calculate_read_total_fasta
Returns: number of lines in fasta file
In case of failure returns non-digit (or negative number) , exitCode
"""
def calculate_read_total_fasta(inFile, log):
    ##
    ## Construct and run the read total system command.
    ## This function is only meant for fasta files, so the cmd uses ">" to find headers.
    ##
    catCmd, exitCode = get_cat_cmd(inFile, log)

    if exitCode != 0:
        status = "get_cat_cmd failed"
        log.error("- get_cat_cmd failure, Unable to run the read total command! exit code != 0")

        return status, RQCExitCodes.JGI_FAILURE

    patt = ""
    if inFile.split(".")[-1] == "fasta" or inFile.split(".")[-2] == "fasta" or inFile.split(".")[-1] == "fa" or inFile.split(".")[-2] == "fa":
        patt = "^>"
    elif inFile.split(".")[-1] == "fastq" or inFile.split(".")[-2] == "fastq":
        patt = "^@"
    else:
        log.error("Unknown file type: %s" % (inFile))
        return -1, RQCExitCodes.JGI_FAILURE

    cmd = catCmd + " " + inFile + " | grep \"" + patt + "\" | wc -l | awk \"{print $1}\""
    stdOut, _, exitCode = run_sh_command(cmd, True, log, True)

    if exitCode == 0:
        stdOut = stdOut.strip()
        if stdOut.isdigit():
            status = "cat_fasta complete"
        else:
            status = "cat_fasta failed. The calculated read total was not an integer!"
            log.error("- cat_fasta failed. The calculated read total was not an integer!")

            return status, RQCExitCodes.JGI_FAILURE
    else:
        status = "cat_fasta failed"
        log.error("- cat_fasta failure, Unable to run the read total command! exit code != 0")

        return status, RQCExitCodes.JGI_FAILURE

    ##
    ## If one gets here, everything must have worked, so record the elapsed
    ## time and return success.
    ##
    status = "calculate_read_total_fasta complete"
    log.info(status)


    return stdOut, RQCExitCodes.JGI_SUCCESS


## EOF

