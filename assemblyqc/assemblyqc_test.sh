#!/bin/bash -l

#set -e

# Assembly qc pipeline tests

# How to compare the results: jgi-rqc/rqc_system/test/compare.py -rf [list of output folders] -p [pacbio|raed|filter|assemblyqc]


OUTDIR="/global/projectb/sandbox/rqc/analysis/qc_user/assemblyqc"
DATE=`date +"%Y%m%d"`

# dev for sulsj
cmd="/global/homes/s/sulsj/work/bitbucket-repo/jgi-rqc-pipeline/assemblyqc/assemblyqc.py"

SEQUNIT="7365.2.69553.AGTTCC"
qsub -cwd -b yes -j yes -m as -now no -w e -N assemblyqc-test-$SEQUNIT -l h_rt=43195 -P gentech-rqc.p -o $OUTDIR/assemblyqc-$SEQUNIT-$DATE.log "$cmd --output-path $OUTDIR/$SEQUNIT-$DATE --fastq $OUTDIR/data/$SEQUNIT.fastq -r 100000000000"

SEQUNIT="7378.1.69281.CGATG"
qsub -cwd -b yes -j yes -m as -now no -w e -N assemblyqc-test-$SEQUNIT -l h_rt=43195 -P gentech-rqc.p -o $OUTDIR/assemblyqc-$SEQUNIT-$DATE.log "$cmd --output-path $OUTDIR/$SEQUNIT-$DATE --fastq $OUTDIR/data/$SEQUNIT.fastq -r 100000000000"

SEQUNIT="7257.1.64419.CACATTGTGAG"
qsub -cwd -b yes -j yes -m as -now no -w e -N assemblyqc-test-$SEQUNIT -l h_rt=43195 -P gentech-rqc.p -o $OUTDIR/assemblyqc-$SEQUNIT-$DATE.log "$cmd --output-path $OUTDIR/$SEQUNIT-$DATE --fastq $OUTDIR/data/$SEQUNIT.fastq -r 100000000000"
