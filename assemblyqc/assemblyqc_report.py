#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re

from common import get_logger, get_status, checkpoint_step
from os_utility import run_sh_command, get_tool_path
from assemblyqc_utils import *
from rqc_constants import RQCExitCodes
from common import append_rqc_stats, append_rqc_file, set_colors
from assemblyqc_constants import AssemblyStats, RQCVelvet, RQCAssemblyQcConfig

from rqc_utility import safe_basename

color = {}
color = set_colors(color, True)


"""
 illumina_assembly_level_report

 Title      : illumina_assembly_level_report
 Function   : This function generates an alpha version of a flatfile-backed,
              web-based report for the specified Illumina sequence unit.
 Usage      : illumina_assembly_level_report(kmer, readLen1, log)
 Args       : kmer, readLen1
 Returns    : JGI_SUCCESS: Illumina read level report could be successfully generated.
              JGI_FAILURE: Illumina read level report could not be generated.
 Comments   : This function is intended to be called at the very end of the illumina read level data processing script.
"""
def illumina_assembly_level_report(kmer, readLen1, log, skip_megan=False):
    log.info("\n\n%s - RUN illumina_assembly_level_report <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    status = "illumina_assembly_level_report in progress"

    ASM_STATS_FILE = RQCAssemblyQcConfig.CFG["stats_file"]
    ASM_STATUS_FILE = RQCAssemblyQcConfig.CFG["status_file"]
    ASM_OUTPUT_PATH = RQCAssemblyQcConfig.CFG["output_path"]

    checkpoint_step(ASM_STATUS_FILE, status)

    velvetOuputPath = None
    velvetOuputPath2 = None

    for p in os.listdir(ASM_OUTPUT_PATH):
        m = re.match(".+velvet\.k(\d+)$", p)
        if p.endswith(".velvet.k63"):
            velvetOuputPath2 = p

    if m is not None and len(m.groups()) > 0:
        velvetOuputPath = p
    elif m is None and velvetOuputPath2 is not None: ## just in case when match() fails
        velvetOuputPath = velvetOuputPath2
    else:
        log.error("Velvet output file not found: %s, %s", ASM_OUTPUT_PATH, p)
        status = "summaryFilePath failed"
        checkpoint_step(ASM_STATUS_FILE, status)
        return status

    summaryFilePath = os.path.join(ASM_OUTPUT_PATH, velvetOuputPath.strip())
    log.info("The velvet assembly directory is " + summaryFilePath)

    if not os.path.exists(summaryFilePath):
        log.error("The summaryFilePath directory (" + summaryFilePath + ") does not exist.")
        status = "summaryFilePath failed"
        log.error("summaryFilePath failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return status

    contigs = os.path.join(summaryFilePath, "contigs.fa")
    log.info("Current contigs file: " + contigs)

    ## Check if the contigs.fa has size 0 or is too small, if yes then exit with success.
    contigsFileSize, exitCode = calculate_read_total_fasta(contigs, log)

    if exitCode != RQCExitCodes.JGI_SUCCESS or contigsFileSize < 1:
        log.error("WARNING! Size " + str(contigsFileSize) + " for file " + contigs + " . AssemblyQC is exiting with failure.")
        status = "contigsFileSize failed"
        checkpoint_step(ASM_STATUS_FILE, status)
        return status


    exitCode = assembly_summary(summaryFilePath, log)

    if exitCode == 0:
        status = "assembly_summary complete"
        checkpoint_step(ASM_STATUS_FILE, status)
    else:
        status = "assembly_summary failed"
        log.error("assembly_summary failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return status


    exitCode = assembly_gc_mean(summaryFilePath, log)

    if exitCode == 0:
        status = "assembly_gc_mean complete"
        checkpoint_step(ASM_STATUS_FILE, status)
    else:
        status = "assembly_gc_mean failed"
        log.error("assembly_gc_mean failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return status


    exitCode = assembly_gc_plot(summaryFilePath, log)

    if exitCode == 0:
        status = "assembly_gc_plot complete"
        checkpoint_step(ASM_STATUS_FILE, status)
    else:
        status = "assembly_gc_plot failed"
        log.error("assembly_gc_plot failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return status


    exitCode = assembly_depth_plot(summaryFilePath, kmer, readLen1, log)

    if exitCode == 0:
        status = "assembly_depth_plot complete"
        checkpoint_step(ASM_STATUS_FILE, status)
    else:
        status = "assembly_depth_plot failed"
        log.error("assembly_depth_plot failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return status


    exitCode = assembly_megablast(summaryFilePath, log)

    if exitCode == 0:
        status = "assembly_megablast complete"
        checkpoint_step(ASM_STATUS_FILE, status)
    else:
        status = "assembly_megablast failed"
        log.error("assembly_megablast failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return status


    if not skip_megan:
        exitCode = assembly_megan_plots(summaryFilePath, log)
    else:
        megan_cmd = "echo 'megan skipped'"
        log.info("Megan skipped!")
        _, _, exitCode = run_sh_command(megan_cmd, True, log)

    if exitCode == 0:
        status = "assembly_megan_plots complete"
        checkpoint_step(ASM_STATUS_FILE, status)
    else:
        status = "assembly_megan_plots failed"
        log.error("assembly_megan_plots failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)


    # if not skip_megan:
    #     exitCode = assembly_tetramer_plots(summaryFilePath, log)
    # else:
    #     tetramer_plot_cmd = "echo 'tetramer_plot skipped'"
    #     log.info("Megan skipped!")
    #     _, _, exitCode = run_sh_command(tetramer_plot_cmd, True, log)
    exitCode = assembly_tetramer_plots(summaryFilePath, log)

    if exitCode == 0:
        status = "assembly_tetramer_plots complete"
        checkpoint_step(ASM_STATUS_FILE, status)
    else:
        status = "assembly_tetramer_plots failed"
        log.error("assembly_tetramer_plots failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)

    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY, "completed", log)
    status = "illumina_assembly_level_report complete"
    checkpoint_step(ASM_STATUS_FILE, status)


    return status



"""
 assembly_summary

 Title      : assembly_summary
 Function   : This function to gernate report of assembly summary:
              kmer, contig cutoff, total nodes, n50 nodes, .
 Usage      : assembly_summary(summaryFilePath, log)
 Args       : 1) The checkpointing file absolute path
              2) The assembly working folder
 Returns    : JGI_SUCCESS:
              JGI_FAILURE:
 Comments   :
"""
def assembly_summary(summaryFilePath, log):
    ASM_STATS_FILE = RQCAssemblyQcConfig.CFG["stats_file"]
    ASM_FILES_FILE = RQCAssemblyQcConfig.CFG["files_file"]
    ASM_STATUS_FILE = RQCAssemblyQcConfig.CFG["status_file"]

    if summaryFilePath is None:
        log.error("Assembly folder is not available for reporting the megablast results of assembly contigs")
        status = "summaryFilePath failed"
        log.error("summaryFilePath failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return RQCExitCodes.JGI_FAILURE

    ##
    ## hard-coded assembler
    ##
    assembler = "Velvet"
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_ASSEMBLER, assembler, log)

    ##
    ## test the version of velvet
    ##
    # velveth = "module load velvet; " + RQCVelvet.VELVETH + " | grep Version" ###NOJGITOOLS
    velvethCmd = get_tool_path("velveth", "velvet")
    velveth = "%s" % (velvethCmd)
    stdOut, _, exitCode = run_sh_command(velveth, True, log)

    if exitCode == 0:
        for l in stdOut.split('\n'):
            if l.startswith("Version"):
                stdOut = l

        status = "velveth_version complete"
        log.info(status)
    else:
        status = "velveth_version failed"
        log.error("velveth_version failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return RQCExitCodes.JGI_FAILURE

    m = re.match("Version (\d+)\.(\d+)\.(\d+)", stdOut)

    if not m is None and len(m.groups()) > 2:
        hv = m.groups()[0] + "." + m.groups()[1] + "." + m.groups()[2] ###"$1.$2.$3"
        append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_ASSEMBLER_VERSION, hv, log)

    ##
    ## Velvet assembly summary is in 'Log' file. We will change this name to other meanful name later.
    ##
    summaryFile = "Log"
    logname = "tail -1 " + os.path.join(summaryFilePath, summaryFile) #`cat $summaryFilePath/$summaryFile`

    stdOut, _, exitCode = run_sh_command(logname, True, log)

    if exitCode == 0:
        if len(stdOut) < 1:
            status = "summary LOG failed"
            log.error("summary LOG failure, exit code != 0")
            checkpoint_step(ASM_STATUS_FILE, status)
            return RQCExitCodes.JGI_FAILURE
        else:
            status = "logname complete"
            log.info(status)
    else:
        status = "logname failed"
        log.error("logname failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return RQCExitCodes.JGI_FAILURE

    m = re.match(".+\.k(\d+)", summaryFilePath)
    if len(m.groups()) > 0:
        kmer = m.groups()[0]
        append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_KMER, kmer, log)

    m = re.match("Final graph has (\d+)", stdOut)
    if len(m.groups()) > 0:
        totalnodes = m.groups()[0]

    m = re.match(".+n50 of (\d+)", stdOut)
    if len(m.groups()) > 0:
        n50 = m.groups()[0]

    m = re.match(".+max (\d+)", stdOut)
    if len(m.groups()) > 0:
        maxcontiglen = m.groups()[0]

    m = re.match(".+total (\d+)", stdOut)
    if len(m.groups()) > 0:
        totalnodeslen = m.groups()[0]

    m = re.match(".+\/(\d+)", stdOut)
    if len(m.groups()) > 0:
        totalreads = m.groups()[0]

    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_NODE_NUMBER, totalnodes, log)
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_N50, n50, log)
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_MAX_CONTIG_LENGTH, maxcontiglen, log)
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_NODE_LENGTH, totalnodeslen, log)
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_READ_NUMBER, totalreads, log)

    ##
    ## calculate contig summary
    ##
    contigSummaryFile = os.path.join(summaryFilePath, "contigs.fa.stats.txt")
    statsShCmd = get_tool_path("stats.sh", "bbtools")
    # contigStatsCmd = "module load bbtools; stats.sh in=" + os.path.join(summaryFilePath, "contigs.fa") + " > " + contigSummaryFile
    contigStatsCmd = "%s in=%s > %s" % (statsShCmd, os.path.join(summaryFilePath, "contigs.fa"), contigSummaryFile)

    stdOut, _, exitCode = run_sh_command(contigStatsCmd, True, log, True)

    if exitCode == 0:
        status = "contigs_stats complete"
        log.info(status)
        contigStatsGet = "cat " + contigSummaryFile

        _, _, exitCode = run_sh_command(contigStatsGet, True, log, True)

        if exitCode == 0:
            log.info("contig_stats ok from " + str(contigSummaryFile))
            log.info(status)
            append_rqc_file(ASM_FILES_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_CONTIG_STATS_SUMMARY, str(contigSummaryFile), log)

    else:
        status = "contigs_stats failed"
        log.error("contigs_stats failure. Unable to run fasta_stats.linux to get contig stats. exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return RQCExitCodes.JGI_FAILURE


    ##
    ## add the top 5 contig file
    ##
    top5File = None
    for filename in os.listdir(summaryFilePath):
        m = re.match("(top5_contigs.fa)", filename)
        if not m is None and len(m.groups()) > 0:
            top5File = m.groups()[0]

    if top5File is None:
        log.error("Failed to add top 5 assembly contig file: " + summaryFilePath)
        return RQCExitCodes.JGI_FAILURE
    else:
        log.info("top5File is " + str(top5File))

        append_rqc_file(ASM_FILES_FILE, "assembly level top 5 assembly contigs file", os.path.join(summaryFilePath, top5File), log)
        append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_TOP5_CONTIG_FILE, top5File, log) # "assembly top 5 contig file"


    return RQCExitCodes.JGI_SUCCESS



"""
=head2 assembly_gc_mean

 Title      : assembly_gc_mean
 Function   : This function generates average GC content % and its standard deviation and put them into database.
 Usage      : assembly_gc_mean(summaryFilePath, log)
 Args       : 1) The checkpointing file absolute path
              2) The assembly working folder
 Returns    : JGI_SUCCESS:
              JGI_FAILURE:
 Comments   :
"""
def assembly_gc_mean(summaryFilePath, log):
    ASM_STATS_FILE = RQCAssemblyQcConfig.CFG["stats_file"]
    ASM_STATUS_FILE = RQCAssemblyQcConfig.CFG["status_file"]

    if summaryFilePath is None:
        log.error("Assembly folder is not available for reporting the assembly level mean GC and standard deviation")
        status = "assembly_gc_mean failed"
        log.error("assembly_gc_mean failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)

        return RQCExitCodes.JGI_FAILURE

    gc_file = "nofile"

    for filename in os.listdir(summaryFilePath):
        m = re.match("(GC.contigs.fa.hist)", filename)
        if not m is None and len(m.groups()) > 0:
            gc_file = m.groups()[0]


    meanLineCmd = "head -1 " + os.path.join(summaryFilePath, gc_file)

    stdOut, _, exitCode = run_sh_command(meanLineCmd, True, log)

    if exitCode == 0:
        status = "meanLineCmd complete"
        log.info(status)

        meanSd = "0"
        m = re.match(".*<(.+)>.*", stdOut)
        if not m is None and len(m.groups()) > 0:
            meanSd = m.groups()[0]
            (mean, stdev) = re.sub('\s+', ' ', meanSd.replace("+/-", " ")).strip().split(" ")
            mean = float(mean) * 100
            mean = str("%.2f" % mean)
            stdev = float(stdev) * 100
            stdev = str("%.2f" % stdev)
            append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_GC_MEAN, mean, log)
            append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_GC_STD, stdev, log)

    else:
        status = "meanLineCmd failed"
        log.error("meanLineCmd failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return RQCExitCodes.JGI_FAILURE


    return RQCExitCodes.JGI_SUCCESS



"""
 assembly_gc_plot

 Title      : assembly_gc_plot
 Function   : This function generates GC plot PNG image put it into database.
 Usage      : assembly_gc_plot (summaryFilePath, log)
 Args       : 1) The checkpointing file absolute path
              2) The assembly working folder
 Returns    : JGI_SUCCESS:
              JGI_FAILURE:
 Comments   :
"""
def assembly_gc_plot(summaryFilePath, log):
    ASM_STATS_FILE = RQCAssemblyQcConfig.CFG["stats_file"]
    ASM_FILES_FILE = RQCAssemblyQcConfig.CFG["files_file"]
    ASM_STATUS_FILE = RQCAssemblyQcConfig.CFG["status_file"]

    if summaryFilePath is None:
        log.error("Assembly folder is not available for reporting the assembly level GC plot")
        status = "assembly_gc_plot failed"
        log.error("assembly_gc_plot failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return RQCExitCodes.JGI_FAILURE

    ## Read the files first in case anything is missing.
    pngFile = "none"
    for filename in os.listdir(summaryFilePath):
        m = re.match("(\S+fa.hist.png)", filename)
        if not m is None and len(m.groups()) > 0:
            pngFile = m.groups()[0]

    if pngFile == "none":
        log.error("The assembly GC content PNG file was not found in folder " + summaryFilePath)
        return RQCExitCodes.JGI_FAILURE

    htmlFile = "none"
    for filename in os.listdir(summaryFilePath):
        m = re.match("(\S+fa.hist.html)", filename)
        if not m is None and len(m.groups()) > 0:
            htmlFile = m.groups()[0]

    if htmlFile == "none":
        log.error("The assembly GC content HTML file was not found in folder " + summaryFilePath)
        return RQCExitCodes.JGI_FAILURE

    textFile = "none"
    for filename in os.listdir(summaryFilePath):
        m = re.match("(GC.contigs.fa.hist)", filename)
        if not m is None and len(m.groups()) > 0:
            textFile = m.groups()[0]

    if textFile == "none":
        log.error("The assembly GC content hist text file was not found in folder " + summaryFilePath)
        return RQCExitCodes.JGI_FAILURE

    append_rqc_file(ASM_FILES_FILE, "assembly level GC content hist PNG image file", os.path.join(summaryFilePath, pngFile), log)
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_GC_PLOT, pngFile, log)

    append_rqc_file(ASM_FILES_FILE, "assembly level GC content hist HTML image file", os.path.join(summaryFilePath, htmlFile), log)
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_GC_PLOT, htmlFile, log)

    append_rqc_file(ASM_FILES_FILE, "assembly level GC content text hist file", os.path.join(summaryFilePath, textFile), log)
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_GC_TEXT, textFile, log)

    gc_avg_std_cmd = 'grep \"^#Found\" %s' % os.path.join(summaryFilePath, textFile)
    stdOut, _, exitCode = run_sh_command(gc_avg_std_cmd, True, log, True)

    if exitCode == 0:
        match = re.match(r'.*(\d+\.\d+) \+\/\- (\d+\.\d+).*', stdOut)
        if match:
            mean = float(match.group(1)) * 100
            mean = str("%.2f" % mean)
            stdev = float(match.group(2)) * 100
            stdev = str("%.2f" % stdev)
            append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_GC_TEXT_AVG, mean, log)
            append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_GC_TEXT_STD, stdev, log)

    else:
        status = "gc_avg_std_cmd failed"
        log.error("gc_avg_std_cmd failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return RQCExitCodes.JGI_FAILURE


    return RQCExitCodes.JGI_SUCCESS



"""
 assembly_depth_plot

 Title      : assembly_depth_plot
 Function   : This function generates GC plot PNG image, text hist put them into database.
 Usage      : assembly_depth_plot(summaryFilePath, kmer, readLen, log)
 Args       : 1) The checkpointing file absolute path
              2) The assembly working folder
 Returns    : JGI_SUCCESS:
              JGI_FAILURE:
 Comments   :
"""
def assembly_depth_plot(summaryFilePath, kmer, readLen, log):
    ASM_STATS_FILE = RQCAssemblyQcConfig.CFG["stats_file"]
    ASM_FILES_FILE = RQCAssemblyQcConfig.CFG["files_file"]
    ASM_STATUS_FILE = RQCAssemblyQcConfig.CFG["status_file"]

    if summaryFilePath is None:
        log.error("Assembly folder is not available for reporting the assembly level GC plot")
        status = "assembly_gc_plot failed"
        log.error("assembly_gc_plot failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return RQCExitCodes.JGI_FAILURE


    ##
    ## To find all depth.***** files
    ##
    depthFileList = []
    for filename in os.listdir(summaryFilePath):
        m = re.match("(depth.\S*\d)$", filename)
        if not m is None and len(m.groups()) > 0:
            depthFile = m.groups()[0]
            depthFileList.append(os.path.join(summaryFilePath, depthFile))

    fileCount = len(depthFileList)

    if fileCount != 6:
        log.error("The right number of assembly depth hist text files were not found in folder " + summaryFilePath + ".")

        return RQCExitCodes.JGI_FAILURE

    for depthFile in depthFileList:
        ##
        ## get the range of depth, the last number in the file name
        ##
        strDepFile = depthFile.strip()
        myArray = re.findall("(\d+)", strDepFile)
        rangeArray = myArray[-1]

        ret = parse_genome_size_average_depth(depthFile, log)
        retCount = len(ret)
        binnn = 0

        if retCount >= 4:
            append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_GENOME_SIZE + " " + rangeArray, ret[0], log)
            depthMean = ret[1]
            append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_DEPTH_MEAN + " " + rangeArray, depthMean, log)
            depthStd = ret[2]
            append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_DEPTH_STD + " " + rangeArray, depthStd, log)

            #These statistics would be good to move to the assemblyqc piepline code, but the kmer and read length1 and depth mean/std are all computed in different places of the pipeline.
            if kmer and depthMean and depthStd and readLen:
                bpdepth = float(depthMean) * readLen / (readLen - kmer + 1)
                bpstd = float(depthStd) * readLen / (readLen - kmer + 1)
                bpdepth = "%.2f" % (float(bpdepth))
                bpstd = "%.2f" % (float(bpstd))

                append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_BPDEPTH_MEAN + " " + rangeArray, bpdepth, log)
                append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_BPDEPTH_STD + " " + rangeArray, bpstd, log)

            append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_DEPTH_BIN + " " + rangeArray, ret[3], log)
            binnn = ret[3]

        gnuplotPngFile = depthFile + ".png"
        times = 'x'
        # gnuplotCmd = "echo \"set title 'Contig Length Weighted Depth 0-" + rangeArray + times + "'; set xlabel 'Kmer Depth bin " +\
        #              str(binnn) + "'; set ylabel 'Assembled Bases'; set output '" + gnuplotPngFile +\
        #             "'; set terminal png; set grid; plot '" + depthFile + "' u 2:7 w lp title 'Kmer Depth'; replot;\" | /usr/bin/gnuplot;"

        ## shifter --image=bryce911/bbtools gnuplot
        gnuplotTool = get_tool_path("gnuplot", "gnuplot")
        gnuplotCmd = "echo \"set title 'Contig Length Weighted Depth 0-" + rangeArray + times + "'; set xlabel 'Kmer Depth bin " +\
                     str(binnn) + "'; set ylabel 'Assembled Bases'; set output '" + gnuplotPngFile +\
                    "'; set terminal png; set grid; plot '" + depthFile + "' u 2:7 w lp title 'Kmer Depth'; replot;\" | %s;" % (gnuplotTool)

        log.info("gnuplotCmd is  " + gnuplotCmd)

        stdOut, stdErr, exitCode = run_sh_command(gnuplotCmd, True, log)

        if exitCode != 0:
            status = "gnuplotCmd error"
            log.error("gnuplotCmd error, exit code " + str(exitCode) + " , stdOut " + str(stdOut) + " , stdErr " + str(stdErr))
            checkpoint_step(ASM_STATUS_FILE, status)


        depFileBasename, exitCode = safe_basename(depthFile, log)

        append_rqc_file(ASM_FILES_FILE, "assembly level depth text hist file 0-" + rangeArray, depthFile, log)
        append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_DEPTH_TEXT + " " + rangeArray, depFileBasename, log)

        pngPlotFileBasename, exitCode = safe_basename(gnuplotPngFile, log)

        append_rqc_file(ASM_FILES_FILE, "assembly level depth hist PNG image file 0-" + rangeArray, gnuplotPngFile, log)
        append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_DEPTH_PLOT + " " + rangeArray, pngPlotFileBasename, log)

        ##
        ## delete the png file after putting it into database.
        ##
        ## unlink($gnuplotPngFile)


    return RQCExitCodes.JGI_SUCCESS



"""
 parse_genome_size_average_depth

 Title      : parse_genome_size_average_depth
 Function   : This function parse the first line of depth hist text file and get estimated genome size, mean depth and std
 Usage      : parse_genome_size_average_depth(textHistFile, log)
 Args       : 1) the file of depth text hist file
 Returns    : array contains three numbers: estimated genome size, mean depth and std.
 Comments   :
"""
def parse_genome_size_average_depth(textHistFile, log):
    genome = 0
    mean = 0.0
    stdev = 0.0
    binn = 0

    line = "head -1 " + textHistFile
    stdOut, _, exitCode = run_sh_command(line, True, log)

    if exitCode == 0:
        myArray = re.findall("(\d+)", stdOut)   ###($str =~ m/(\d+)/g)
        if myArray and len(myArray) > 0:
            genome = myArray[0]

    meanSd = "0"
    m = re.match(".*<(.+)>.*", stdOut)

    if not m is None and len(m.groups()) > 0:
        meanSd = m.groups()[0]
        (mean, stdev) = re.sub('\s+', ' ', meanSd.replace("+/-", " ")).strip().split(" ")
        mean = float(mean)
        # mean = str("%.2f" % mean)
        stdev = float(stdev)
        # stdev = str("%.2f" % stdev)

    lines = "head -3 " + str(textHistFile)

    stdOut, _, exitCode = run_sh_command(lines, True, log)

    toks = stdOut.split("\n")
    lines_num = len(toks)
    if lines_num >= 3:
        line3 = toks[2]
        m = re.match(".*\[ (.+) \].*", line3)
        if not m is None and len(m.groups()) > 0:
            binn = abs(eval(m.groups()[0]))

    ret = (genome, str("%.2f" % mean), str("%.2f" % stdev), binn)


    return ret



"""
 assembly_megablast

 Title      : assembly_megablast
 Function   : This function to gernate report of contig megablast results.
 Usage      : assembly_megablast(summaryFilePath, log)
 Args       : 1) The checkpointing file absolute path
              2) The assembly working folder
 Returns    : JGI_SUCCESS:
              JGI_FAILURE:
 Comments   :
"""
def assembly_megablast(summaryFilePath, log):
    ASM_STATS_FILE = RQCAssemblyQcConfig.CFG["stats_file"]
    ASM_STATUS_FILE = RQCAssemblyQcConfig.CFG["status_file"]

    if summaryFilePath is None:
        log.error("Assembly folder is not available for reporting the megablast results of assembly contigs")
        status = "summaryFilePath failed"
        log.error("summaryFilePath failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return RQCExitCodes.JGI_FAILURE


    ##
    ## To find the total contig number generated from velvet
    ##
    contigs = summaryFilePath + "/contigs.fa"
    log.info("Current contigs file: " + contigs)

    ## Check if the contigs.fa has size 0 or is too small, if yes then exit with success.
    contigsFileSize, exitCode = calculate_read_total_fasta(contigs, log)

    if exitCode != RQCExitCodes.JGI_SUCCESS or contigsFileSize < 1:
        log.error("WARNING! Size " + str(contigsFileSize) + " for file " + contigs + " . AssemblyQC is exiting with failure.")
        status = "contigsFileSize failed"
        checkpoint_step(ASM_STATUS_FILE, status)

        return RQCExitCodes.JGI_FAILURE

    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_CONTIG_NUMBER, contigsFileSize, log)

    dbList = ["nt", "LSSURef", "LSURef", "SSURef", "refseq.archaea", "refseq.bacteria",
              "refseq.mitochondrion", "refseq.plant", "refseq.plasmid", "refseq.plastid",
              "refseq.fungi", "refseq.viral", "green_genes16s.insa_gg16S",
              "JGIContaminants", "collab16s"]

    for db in dbList:
        exitCode = assembly_megablast_hits(summaryFilePath, db, log)

        if exitCode == 0:
            status = "assembly_megablast " + str(db) + " complete"
            checkpoint_step(ASM_STATUS_FILE, status)
        else:
            status = "assembly_megablast " + str(db) + " failed"
            log.error("assembly_megablast " + str(db) + " failure, exit code != 0")
            checkpoint_step(ASM_STATUS_FILE, status)
            return RQCExitCodes.JGI_FAILURE


    return RQCExitCodes.JGI_SUCCESS



"""
 assembly_megablast_hits

 Title      : assembly_megablast_hits
 Function   : This function generates tophit list of megablast against different databases.
 Usage      : assembly_megablast_hits(summaryFilePath, dbName, log)
 Args       : 1) The checkpointing file absolute path
              2) The assembly working folder
              3) Megablast reference sequence database name
 Returns    : JGI_SUCCESS:
              JGI_FAILURE:
 Comments   :
"""
def assembly_megablast_hits(summaryFilePath, dbName, log):
    ASM_STATS_FILE = RQCAssemblyQcConfig.CFG["stats_file"]
    ASM_FILES_FILE = RQCAssemblyQcConfig.CFG["files_file"]
    ASM_STATUS_FILE = RQCAssemblyQcConfig.CFG["status_file"]

    if summaryFilePath is None:
        log.error("Assembly folder is not available for reporting the megablast results of assembly contigs")
        status = "summaryFilePath failed"
        log.error("summaryFilePath failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return RQCExitCodes.JGI_FAILURE

    if dbName is None:
        log.error("Assembly folder is not available for reporting the megablast results of assembly contigs")
        status = "summaryFilePath failed"
        log.error("summaryFilePath failure, exit code != 0")
        checkpoint_step(ASM_STATUS_FILE, status)
        return RQCExitCodes.JGI_FAILURE


    ##
    ## To find the matching hits
    ##
    numMatchingHits = 0
    parsedFileName = ""
    parsedFileAbspath = ""
    for filename in os.listdir(summaryFilePath):
        m = re.match("(megablast\.\S+\." + str(dbName) + "\S*\.parsed)", filename)
        if not m is None and len(m.groups()) > 0:
            parsedFileName = m.groups()[0]

    if parsedFileName != "":
        parsedFileAbspath = os.path.join(summaryFilePath, parsedFileName)
        numMatchingHits, _ = calculate_line_total(parsedFileAbspath, log)
        log.info("numMatchingHits: " + str(numMatchingHits) + " parsedFileAbspath " + str(parsedFileAbspath))

    if type(numMatchingHits) is int and numMatchingHits >= 2:
        numMatchingHits = numMatchingHits - 2


    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_MATCHING_HITS + " " + dbName, numMatchingHits, log)
    append_rqc_file(ASM_FILES_FILE, "assembly level .parsed file of " + dbName + " database", parsedFileAbspath, log)
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_PARSED_FILE + " " + dbName, parsedFileName, log)


    ##
    ## To find the top hits
    ##
    numTophits = 0
    tophitsFileName = ""
    tophitsFileAbspath = ""

    for filename in os.listdir(summaryFilePath):
        m = re.match("(megablast\.\S+\." + str(dbName) + "\S*\.parsed\.tophit)", filename)
        if not m is None and len(m.groups()) > 0:
            tophitsFileName = m.groups()[0]

    if tophitsFileName != "":
        tophitsFileAbspath = os.path.join(summaryFilePath, tophitsFileName)
        numTophits, _ = calculate_line_total(tophitsFileAbspath, log)
        log.info("numTophits: " + str(numTophits) + " tophitsFileAbspath " + str(tophitsFileAbspath))

    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_TOP_HITS + " " + dbName, numTophits, log)
    append_rqc_file(ASM_FILES_FILE, "assembly level top hit file of " + dbName + " database", tophitsFileAbspath, log)
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_TOPHIT_FILE + " " + dbName, tophitsFileName, log)


    ##
    ## To find the top 100 hits
    ##
    numTop100hits = 0
    top100hitsFileName = ""
    top100hitsFileAbspath = ""

    for filename in os.listdir(summaryFilePath):
        m = re.match("(megablast\.\S+\." + str(dbName) + "\S*\.parsed\.top100hit)", filename)
        if not m is None and len(m.groups()) > 0:
            top100hitsFileName = m.groups()[0]

    if top100hitsFileName != "":
        top100hitsFileAbspath = os.path.join(summaryFilePath, top100hitsFileName)
        numTop100hits, _ = calculate_line_total(top100hitsFileAbspath, log)
        log.info("numTop100hits: " + str(numTop100hits) + " top100hitsFileAbspath " + str(top100hitsFileAbspath))

    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_TOP_100_HITS + " " + dbName, numTop100hits, log)
    append_rqc_file(ASM_FILES_FILE, "assembly level top 100 hit file of " + dbName + " database", top100hitsFileAbspath, log)
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_TOP100HIT_FILE + " " + dbName, top100hitsFileName, log)


    ##
    ## To find the taxonomic species
    ##
    numSpecies = 0
    taxlistFileName = ""
    taxlistFileAbspath = ""

    for filename in os.listdir(summaryFilePath):
        m = re.match("(megablast\.\S+\." + str(dbName) + "\S*\.parsed\.taxlist)", filename)
        if not m is None and len(m.groups()) > 0:
            taxlistFileName = m.groups()[0]

    if taxlistFileName != "":
        taxlistFileAbspath = os.path.join(summaryFilePath, taxlistFileName)
        numSpecies, _ = calculate_line_total(taxlistFileAbspath, log)
        log.info("numSpecies: " + str(numSpecies) + " taxlistFileAbspath " + str(taxlistFileAbspath))

    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_TAX_SPECIES + " " + dbName, numSpecies, log)
    append_rqc_file(ASM_FILES_FILE, "assembly level taxlist file of " + dbName + " database", taxlistFileAbspath, log)
    append_rqc_stats(ASM_STATS_FILE, AssemblyStats.ILLUMINA_ASSEMBLY_TAXLIST_FILE + " " + dbName, taxlistFileName, log)


    return RQCExitCodes.JGI_SUCCESS



"""
 assembly_megan_plots

 Title      : assembly_megan_plots
 Function   : This function retrieves the images created by running Megan and R
              and adds them to the analysis table.
 Usage      : assembly_megan_plots(meganDir, log)
 Returns    : JGI_SUCCESS:
              JGI_FAILURE:
 Comments   :
"""
def assembly_megan_plots(meganDir, log):
    ASM_STATS_FILE = RQCAssemblyQcConfig.CFG["stats_file"]
    ASM_FILES_FILE = RQCAssemblyQcConfig.CFG["files_file"]

    ##
    ## Look for the megan output files.
    ## Image files may contain the following keys.
    ## The value=description of file.
    ##
    imageDescriptions = {
        "taxa.jpg" : "contig level MEGAN: Taxa Distribution of contigs vs. NT",
        "phylo.jpg" : "contig level MEGAN: Phylogeny of contigs vs NT",
        "phylo2.jpg" : "contig level MEGAN: Phylogeny of contigs vs NT filtered by class",
        "megan.jpg" : "contig level GC Histogram of contigs"
    }

    ##
    ## Get image files generated by Megan and R.
    ##
    numMeganImages = 0
    for image in os.listdir(meganDir):
        megaFile = os.path.join(meganDir, image)
        if image.endswith(".jpg"):
            toks = image.split(".")
            rootName = toks[-2] + "." + toks[-1]

            if rootName and imageDescriptions.has_key(rootName):
                desc = imageDescriptions.get(rootName)

                append_rqc_file(ASM_FILES_FILE, desc, megaFile, log)
                append_rqc_stats(ASM_STATS_FILE, desc, image, log)

                numMeganImages += 1
                log.info("contig level MEGAN_ANALYSIS: image_file=" + megaFile + ", rootName=" + rootName)
                log.info("contig level MEGAN_ANALYSIS_STATS: adding " + desc + ", image " + image)

    if numMeganImages == 0:
        log.error("Failed to add contig level megan image file into database.")

        return RQCExitCodes.JGI_FAILURE


    return RQCExitCodes.JGI_SUCCESS



"""
 assembly_tetramer_plots

 Title      : assembly_tetramer_plots
 Function   : This function retrieves the images created by running the tetramer plot
              (based on velvet) and adds them to the analysis table.
 Usage      : assembly_tetramer_plots(tetramerDir, log)
 Returns    : JGI_SUCCESS:
              JGI_FAILURE:
 Comments   :
"""
def assembly_tetramer_plots(tetramerDir, log):
    ASM_STATS_FILE = RQCAssemblyQcConfig.CFG["stats_file"]
    ASM_FILES_FILE = RQCAssemblyQcConfig.CFG["files_file"]

    tetramerPlotFileName = "tetramer_velvet_assembly.jpg"
    tetramerFile = os.path.join(tetramerDir, tetramerPlotFileName)

    if os.path.exists(tetramerFile):
        desc = "contig level tetramer plot"
        append_rqc_file(ASM_FILES_FILE, desc, tetramerFile, log)
        append_rqc_stats(ASM_STATS_FILE, desc, tetramerPlotFileName, log)
        # print "contig level TETRAMER: adding " + desc + ", " + tetramerPlotFileName + ", " + tetramerFile
        log.info("contig level TETRAMER: adding " + desc + ", " + tetramerPlotFileName + ", " + tetramerFile)

    else:
        log.error("Failed to add contig level TETRAMER file into database.")
        return RQCExitCodes.JGI_FAILURE


    return RQCExitCodes.JGI_SUCCESS


## EOF


