#!/bin/bash

SCRIPT=$(readlink -f $0)
SCRIPT_PATH=`dirname $SCRIPT`

NAWK=`which gawk`
SORT=`which sort` 

cd $1;

HIST="$SCRIPT_PATH/histogram2.pl"

if [ "$NERSC_HOST" == "denovo" ] || [ "$NERSC_HOST" == "cori" ]
then
    GNUPLOT="shifter --image=bryce911/bbtools gnuplot"
else
    GNUPLOT=`module load gnuplot; which gnuplot`
fi

function gc2gcplot {
    # it'd be nice to be able to use as a filter!!
    # plot output of GCcontent.pl

    local F=${*:?"Need one or more input files"}
    local BASE=$(for f in $(echo $F) ; do basename $f; done | $SORT -u | tr '\n' '.' | sed 's/.$//')
    GPFILE="$BASE.hist.gp"
    echo "# BASE=$BASE"
	echo "$5"
	echo "$6"
	echo "$F" 
    $NAWK '{print $5+$6}' $F | $HIST - 1 0.005 > $BASE.hist ;

    printf "#\041$GNUPLOT -persist\nset title 'GC Content %s'; set xlabel '%%GC'; set xrange [0:1]; set ylabel 'Count';\n" $BASE >$GPFILE
    printf "plot \"%s.hist\" u (\$2>0?\$2:1/0):7 w lp title \"%s\"\n" $BASE $BASE >>$GPFILE

    # strip tailing comma...
    #echo $PLOTCMD | sed 's/,$//' >> $GPFILE
    chmod +x $GPFILE
}

gc2gcplot $2 
