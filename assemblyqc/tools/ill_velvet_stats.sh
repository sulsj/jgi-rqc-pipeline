#!/bin/bash
set -e
set -o pipefail

#NAWK=`which nawk`
#if [ -n $NAWK ]
#then
NAWK=`which gawk`
#fi

SCRIPT=$(readlink -f $0)
SCRIPT_PATH=`dirname $SCRIPT`
ILLU_PROTOTYPE_DIR="$SCRIPT_PATH";

##source "$SCRIPT_PATH/qctools.sh"

HIST="$SCRIPT_PATH/histogram2.pl"
function hist() { $HIST $* ; }

pwd;
DIR=$1;
cd $1;
NAME=$2;

# check if file size is non-zero
if [ -s contigs.fa ] ; then
    tail -n 1 Log > "$NAME.assem.summary"
    hist stats.txt 6 1 > "$NAME.depth.out"
    $ILLU_PROTOTYPE_DIR/GCcontent.pl contigs.fa 1 > GC.contigs.fa
    
    #$NAWK '$2>=100' stats.txt > s
    #sed 's/NODE_//;s/_length_/ /;s/_cov_/ /' GC.contigs.fa | NAWK 'BEGIN{print "ID lgth cov gc"} {print $1, $2, $3, $7+$8}' > g
    #join -1 1 -2 1 s g > gc+stats

    ## Gen depth.*.png
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 1 2 6 3 90 > depth.`echo 6 1 2 6 3 90 | sed -e"s/ /_/g"` ; cat depth*90;
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 .5 2 6 1 6 > depth.`echo 6 h 2 6 1 6 | sed -e"s/ /_/g"` ; cat depth*6;
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 10 2 6 4 240 > depth.`echo 6 10 2 6 4 240 | sed -e"s/ /_/g"` ; cat depth*240;
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 10 2 6 4 400 > depth.`echo 6 10 2 6 4 400 | sed -e"s/ /_/g"` ; cat depth*400;
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 10 2 6 4 800 > depth.`echo 6 10 2 6 4 800 | sed -e"s/ /_/g"` ; cat depth*800;
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 10 2 6 4 1500 > depth.`echo 6 10 2 6 4 1500 | sed -e"s/ /_/g"` ; cat depth*1500;
    
    ## Gen GC.contigs.fa.hist.gp
    $ILLU_PROTOTYPE_DIR/gc2gcplot.sh GC.contigs.fa
    
    #$ILLU_PROTOTYPE_DIR/megablast.sh contigs.fa $REFSEQ_MICROBIAL &
    #$ILLU_PROTOTYPE_DIR/megablast.sh contigs.fa $REFSEQ_MITOCHONDRION &
    #$ILLU_PROTOTYPE_DIR/megablast.sh contigs.fa $REFSEQ_PLANT &
    #$ILLU_PROTOTYPE_DIR/megablast.sh contigs.fa $REFSEQ_PLASMID &
    #$ILLU_PROTOTYPE_DIR/megablast.sh contigs.fa $REFSEQ_PLASTID &
    
    #$ILLU_PROTOTYPE_DIR/megablast.sh contigs.fa $LSURef &
    #$ILLU_PROTOTYPE_DIR/megablast.sh contigs.fa $SSURef &
    #$ILLU_PROTOTYPE_DIR/megablast.sh contigs.fa $LSSURef &
    #$ILLU_PROTOTYPE_DIR/megablast.sh contigs.fa $NT
    
    #for FN in *parsed 
    #  do
    #  $ILLU_PROTOTYPE_DIR/blast2taxlist.sh $FN ;
    #done
fi

#echo "Megablast complete";
