#!/bin/bash
set -e
set -o pipefail

#NAWK=`which nawk`
#if [ -n $NAWK ]
#then
NAWK=`which gawk`
#fi

SCRIPT=$(readlink -f $0)
SCRIPT_PATH=`dirname $SCRIPT`
ILLU_PROTOTYPE_DIR="$SCRIPT_PATH";

##source "$SCRIPT_PATH/qctools.sh"

HIST="$SCRIPT_PATH/histogram2.pl"
function hist() { $HIST $* ; }

pwd;
DIR=$1;
cd $1;
ODIR=`realpath $2`;

# check if file size is non-zero
if [ -s contigs.fa ] ; then
    tail -n 1 Log > $ODIR/assem.summary
    hist stats.txt 6 1 > $ODIR/depth.out
    $ILLU_PROTOTYPE_DIR/GCcontent.pl contigs.fa 1 > $ODIR/contigs.gc

    #$NAWK '$2>=100' stats.txt > s
    #sed 's/NODE_//;s/_length_/ /;s/_cov_/ /' GC.contigs.fa | NAWK 'BEGIN{print "ID lgth cov gc"} {print $1, $2, $3, $7+$8}' > g
    #join -1 1 -2 1 s g > gc+stats

    ## Gen depth.*.png
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 1 2 6 3 90 > $ODIR/depth.`echo 6 1 2 6 3 90 | sed -e"s/ /_/g"`
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 .5 2 6 1 6 > $ODIR/depth.`echo 6 h 2 6 1 6 | sed -e"s/ /_/g"`
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 10 2 6 4 240 > $ODIR/depth.`echo 6 10 2 6 4 240 | sed -e"s/ /_/g"`
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 10 2 6 4 400 > $ODIR/depth.`echo 6 10 2 6 4 400 | sed -e"s/ /_/g"`
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 10 2 6 4 800 > $ODIR/depth.`echo 6 10 2 6 4 800 | sed -e"s/ /_/g"`
    $ILLU_PROTOTYPE_DIR/weighted-hist.pl stats.txt 6 10 2 6 4 1500 > $ODIR/depth.`echo 6 10 2 6 4 1500 | sed -e"s/ /_/g"`

    ## Gen GC.contigs.fa.hist.gp
    $ILLU_PROTOTYPE_DIR/gc2gcplot2.sh $ODIR contigs.gc
fi

#echo "Megablast complete";
