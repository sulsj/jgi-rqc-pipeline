#!/bin/bash

set -e
set -o pipefail

module load velvet
module load bbtools
#NOJGITOOLS

###$1 $2 | $3 -b 5 -a 5 -l 35 -n 2 | $4 $5 $6 -fastq - 

$1 $2 | $3 ow in=stdin.fq out=stdout.fq qtrim=rl trimq=5 minlength=35 maxns=2 gchist=$5/GC.contigs.fa.BBDUK.hist | $4 $5 $6 -fastq -

#echo "exit status = $?"

