#!/usr/bin/env perl
#
# merSampler.pl by Jarrod Chapman <jchapman@lbl.gov> Wed Feb 25 09:43:20 PST 2009
# Copyright 2009 Jarrod Chapman. All rights reserved.
#

use strict;
use warnings;
use Getopt::Std;

my %opts = ();
my $validLine = getopts('f:m:e:v', \%opts);
my @required = ("f","m","e");
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);
if ($validLine != 1) {
    print "Usage: ./merSampler.pl <-m merSize> <-f fastaFile> <-e reportFreq>\n";
    exit;
}

my $verbose ||= $opts{"v"} ;
my $reportFreq = $opts{"e"};
my $merSize = $opts{"m"};
my $fastaFile = $opts{"f"};

my %uniqueMers = ();
my $nUnique = 0;
my %uniqueStartMers = ();
my $nUniqueStarts = 0;
my $nSeq = 0;
my $lSeq = 0 ;
my $gc_ct = 0 ;
my $reportNU = 0;
my $reportNUS = 0;

my $currentEntry = undef;
my $seq = "";

# header
if ($verbose) {
	print STDERR "# nSeq\tnStrtUniMer\tfracStartUniMer\tnRandUniMer\tfracRandUniMer\tspos\tgc\n";
} else {
	print "# nSeq\tnStrtUniMer\tfracStartUniMer\tnRandUniMer\tfracRandUniMer\n";
}

open (F,$fastaFile) || die "Couldn't open $fastaFile\n";
while (my $line = <F>) {
    chomp $line;
    if ($line =~ /^>/) {
        if (defined($currentEntry)) {
            processSequence($currentEntry,$seq);
        }
        $seq = "";
        ($currentEntry) = $line =~ />(\S+)/;
    } else {
        $seq .= $line;
    }
}
close F;

if (defined($currentEntry)) {
    processSequence($currentEntry,$seq);
}

sub processSequence {
    my ($name,$seq) = @_;
    my $seqLen = length($seq);
    
    return unless ($seqLen > $merSize) ;

    if ($verbose) { 
	$lSeq+=length($seq) ; 
	$gc_ct += ($seq=~tr/[gcGC]//) ; 
    } 

    $nSeq++;
    my $pos = 0;
    my $smer = substr($seq,$pos,$merSize);
    
    if (exists($uniqueStartMers{$smer})) {
	$uniqueStartMers{$smer}++;
    } else {
	$uniqueStartMers{$smer} = 1;
	$nUniqueStarts++;
    }
    if ( $verbose ) {
	    print STDERR "$nUniqueStarts $uniqueStartMers{$smer} $smer $pos " ;
    }
    
    $pos = int(rand($seqLen-$merSize));
    my $mer = substr($seq,$pos,$merSize);
    
    if (exists($uniqueMers{$mer})) {
	$uniqueMers{$mer}++;
    } else {
	$uniqueMers{$mer} = 1;
	$nUnique++;
    }	

    if ( $verbose ) {
	    print STDERR "$nUnique $uniqueMers{$mer} $mer $pos\n";
    }

    if ($nSeq%$reportFreq == 0) {
	my $pU = ($nUnique - $reportNU)/$reportFreq;
	$reportNU = $nUnique;
	my $pUS = ($nUniqueStarts - $reportNUS)/$reportFreq;
	$reportNUS = $nUniqueStarts;

	if ($verbose) {
		print "$nSeq\t$nUniqueStarts\t$pUS\t$nUnique\t$pU\t0\t$smer\t$pos\t$mer\t",$gc_ct/$lSeq,"\n" ;
	} else {
		print "$nSeq\t$nUniqueStarts\t$pUS\t$nUnique\t$pU\n";
	}
    }
}
