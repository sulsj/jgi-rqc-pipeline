#!/usr/bin/env perl

###################################################################
# POD Block                                                       #
###################################################################

#
# POD Notes:
#
# Spacing can be critical to the correct presentation of the text within
# the POD block.
# The options information under SYNOPSIS must be indented.
# The script verion and author must be indented.
# No other lines in the POD Block need to be indented
#

=head1 NAME

fq_trim_seq.pl

=head1 SYNOPSIS

fq_trim_seq.pl

  Options:
    -fq <file>          The fastq file to be trimmed.
    -r1b <int>		Trim <int> bases from the beginning of read 1 
    -r1e <int>          Trim <int> bases from the end of read 1
    -r2b <int>          Trim <int> bases from the beginning of read 2
    -r2e <int>          Trim <int> bases from the end of read 2
    -b <int>		Trim <int> bases from the beginning of both reads. 
                        Overrides -r1b and -r2b
    -e <int>		Trim <int> bases from the end of both reads. 
                        Overrides -r1e and -r2e

=head1 OPTIONS

=over 8

=item B<fq>

The fastq file to be trimmed.

=item B<r1b>

Trim <int> bases from the beginning of read 1 

=item B<r1e>          

Trim <int> bases from the end of read 1

=item B<r2b>

Trim <int> bases from the beginning of read 2

=item B<r2e>

Trim <int> bases from the end of read 2

=item B<b>		

Trim <int> bases from the beginning of both reads. Overrides -r1b and -r2b
=item B<e> 

Trim <int> bases from the end of both reads. Overrides -r1e and -r2e

=back

=head1 DESCRIPTION

This script trims from beginning or end of single line fastq files 
sequences can also be trimmmed at a single location . Input must be a 
fastq file (zipped or unzipped).  At least 1 option is required.  

=cut

###################################################################
# Pragma Inclusions                                               #
###################################################################

#
# Enforce good programming style.  Note that warnings are turned on with
# the invocation of Perl itself.

use strict;
use warnings;

###################################################################
# Module Inclusions                                               #
###################################################################


use Getopt::Long;
use Pod::Usage;

#
# The location of the Rolling QC modules.  This will be replaced with the
# appropriate "use lib" directory during the installation process.
#
####REPLACE####;

#
# Include the appropriate Rolling QC modules.
#
use JGI_Constants;


###################################################################
# Constant Definitions                                            #
###################################################################

# A string to identify this script.
my $SCRIPT_ID = "fq_trim_seq.pl";

# A string to identify this script's version.
my $VERSION_ID = "Version 4/12/2012";

# A string to identify the author of this script.
my $AUTHOR_ID = "James Han";

# A string to identify this function.
my $function_id = "main";


###################################################################
# Variable Declarations                                           #
###################################################################

# The "use strict" directive requires all variables to be
# declared before use.
my ($help, $man, $version);

my $fastq_file;

my ($opt_r1_beg, $opt_r1_end, $opt_r2_beg, $opt_r2_end, 
    $opt_glob_beg, $opt_glob_end);

my (@opts_check, $current_opt, $opt_count);

my ($r1_id, $r1_seq, $r1_junk, $r1_qual, $r2_id, $r2_seq, $r2_junk, $r2_qual);

my ($r1_len, $r2_len, $r1_keep, $r2_keep );

###################################################################
# The Main Script                                                 #
###################################################################


#
# Load in the command-line arguments.
#
GetOptions("fq=s" => \$fastq_file,
           "r1b=i" => \$opt_r1_beg,
	   "r1e=i" => \$opt_r1_end,
	   "r2b=i" => \$opt_r2_beg,
	   "r2e=i" => \$opt_r2_end,
	   "b=i" => \$opt_glob_beg,
	   "e=i" => \$opt_glob_end,
	   help => \$help,
	   man => \$man,
	   version => \$version,
    )
    or pod2usage({-verbose => 0, -exit => JGI_FAILURE});

#
# Handle the standard options -h, -m  and -v
#
if ($help)
{
    pod2usage(-verbose => 1, -exit => JGI_SUCCESS);
}
elsif ($man)
{
    pod2usage(-verbose => 2, -exit => JGI_SUCCESS);
}
elsif ($version)
{
    #
    # A call to script_version will exit with JGI_SUCCESS.
    #
    script_version($VERSION_ID, $AUTHOR_ID);
}

# Set all the options
@opts_check = ($opt_r1_beg, $opt_r1_end, $opt_r2_beg, $opt_r2_end, 
	       $opt_glob_beg, $opt_glob_end);

$opt_count = 0;
foreach $current_opt (@opts_check)
{
    if (defined($current_opt))
    {
	$opt_count++;
    }
}

if ( (! defined($fastq_file)) || ($opt_count == 0) )
{
    pod2usage(-verbose => 1, -exit => JGI_SUCCESS);
}

# Unless the fastq_file is piped from stdin, check if file exists
if ($fastq_file ne "-")
{  
    if (! -f $fastq_file)
    {
	print "Fastq does not exist! ($fastq_file)\n";
	exit JGI_FAILURE;
    }
}
if (! open(FASTQ_FH, $fastq_file))
{
    print "Could not open file handle for reading!\n" .
	"Fastq_file: $fastq_file";
    exit JGI_FAILURE;
}


# Override if global beg or end given
if ( $opt_glob_beg ) 
{
  $opt_r1_beg = $opt_glob_beg;
  $opt_r2_beg = $opt_glob_beg;
}
if ( $opt_glob_end ) 
{
  $opt_r1_end = $opt_glob_end;
  $opt_r2_end = $opt_glob_end;
}

unless ( $opt_r1_beg ) 
{
  $opt_r1_beg = 0;
}
unless ( $opt_r2_beg ) 
{
  $opt_r2_beg = 0;
}

while ($r1_id = <FASTQ_FH> ) 
{
    $r1_seq = <FASTQ_FH>;
    $r1_junk = <FASTQ_FH>;
    $r1_qual = <FASTQ_FH>;

    $r2_id = <FASTQ_FH>;
    $r2_seq = <FASTQ_FH>;
    $r2_junk = <FASTQ_FH>;
    $r2_qual = <FASTQ_FH>;

    chomp ($r1_seq, $r2_seq );

    # get rl, assume read length and qual len are the same
    $r1_len = length ( $r1_seq );
    $r2_len = length ( $r2_seq );

    if ( $opt_r1_end ) 
    {
	$r1_keep = $r1_len - $opt_r1_end;
    }
    else 
    {
	$r1_keep = $r1_len;
    }

    if ( $opt_r2_end ) 
    {
	$r2_keep = $r2_len - $opt_r2_end;
    } 
    else 
    {
	$r2_keep = $r2_len;
    }

    $r1_seq = substr( $r1_seq, $opt_r1_beg, $r1_keep );
    $r2_seq = substr( $r2_seq, $opt_r2_beg, $r2_keep );
    
    $r1_qual = substr( $r1_qual, $opt_r1_beg, $r1_keep );
    $r2_qual = substr( $r2_qual, $opt_r2_beg, $r2_keep );

    print $r1_id. "$r1_seq\n" . $r1_junk . "$r1_qual\n" .
	$r2_id. "$r2_seq\n" . $r2_junk . "$r2_qual";

} #end while

close ($fastq_file);

###################################################################
# Function Definitions                                            #
###################################################################

sub script_version
{
    # Please refer to the above function description for details of
    # what these variables are.

    my ($VERSION_ID, $AUTHOR_ID, $script_status) = @_;

    print "$SCRIPT_ID ($VERSION_ID)\n";
    print "Author: $AUTHOR_ID\n";

    #
    # If the script status isn't defined, or is set to something other
    # than failure, exit the script with success.
    #

    if (!defined $script_status || $script_status ne JGI_FAILURE)
    {
        exit(JGI_SUCCESS);
    }
} # End of the script_version function.
