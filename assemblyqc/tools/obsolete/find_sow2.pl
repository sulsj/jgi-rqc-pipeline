#!/usr/bin/env perl

# depreciated: Bryce - LIMS_data doesn't work
exit(0);

use strict;
use warnings;

use FindBin '$RealBin';
use lib "$RealBin/../lib";


# data service package
use LIMS_data;

unless ( @ARGV ) {
 print <<USAGE;
 
  usage: find_sow2.pl <lib | lib project id | bioclass >
   
  Finds SOW for any library name, library project id, or bioclassfication

USAGE

 exit;
}

foreach my $q ( @ARGV ) {
  chomp $q;
  my @sow = LIMS_data::lib_sowb ( $q );
  
  map { 
	my @lps  = split( ':_:', $_ );
	print "$lps[3]"; } @sow;

}
