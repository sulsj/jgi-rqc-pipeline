#!/usr/bin/env perl

#########################################################################
#  Performs a standard set of QC's for an Illumina fq file. 
#  processes 1 fastq file on the sge cluster
#  Run id must be greater than 425 and seq file must be fastq.bz2 or
#  fastq.gz format. 
#########################################################################

use strict;
use warnings;

use File::Copy;
use File::Basename;
use Cwd qw(realpath);
use Getopt::Long;
use Pod::Usage;
use Time::HiRes;

use FindBin '$RealBin';
use lib "$RealBin/../lib";

use JGI_Analysis;
use JGI_Analysis_Manager;
use JGI_Analysis_Utility_Illumina;
use JGI_Commands;
use JGI_Constants;
use JGI_Lib_Info;
use JGI_Log;
use JGI_QC_Utility;
use JGI_Seq_Unit;
use JGI_Utility;


my ($script_name, $script_path);
$script_name = 'ill_sge_qc_job.pl';
$script_path = $0;

#
# Declare GetOption variables
#
my ($opt_max_retries,
    $opt_outdir,
    $opt_seq_unit,
    $help);

my $opt_assembly = CONFIG_FALSE;
my $opt_read = CONFIG_FALSE;

GetOptions('assembly'	       => \$opt_assembly,
	   'max-retries=i'     => \$opt_max_retries,
	   'outdir=s'          => \$opt_outdir,
	   'read'              => \$opt_read,
	   'seq-unit=s'        => \$opt_seq_unit,
	   'help!'             => \$help,
	   ) or pod2usage(2);
pod2usage(1) if $help;

my $start_time = [Time::HiRes::gettimeofday()];
my $log = Log::Log4perl->get_logger();

&Main;

exit_success();


sub Main {
    if (!defined($opt_seq_unit) || $opt_seq_unit eq '') {
	if (!defined($ARGV[0]) || $ARGV[0] eq '') {
	    $log->error("No sequence unit name provided");
	    exit_failure();
	} else {
	    $opt_seq_unit = $ARGV[0];
	}
    }

    chomp($opt_seq_unit);

    my $seq_unit_base;

    if (get_seq_unit_base($opt_seq_unit, \$seq_unit_base, $log) != JGI_SUCCESS) {
	exit_failure();
    }

    my $analysis_type;

    if ($opt_assembly == CONFIG_FALSE && $opt_read == CONFIG_FALSE) {
	print STDERR "Processing aborted: Must specify either -assembly or -read\n";

	$log->error("Processing aborted: Must specify either -assembly or -read");
    	exit_failure();
    } elsif ($opt_assembly == CONFIG_TRUE && $opt_read == CONFIG_TRUE) {
	print STDERR "Processing aborted: Must specify either -assembly or -read but not both\n";

	$log->error("Processing aborted: Must specify either -assembly or -read but not both");
    	exit_failure();
    } else {
	if ( $opt_assembly == CONFIG_TRUE ) {
	    $analysis_type = JGI_ANALYSIS_TYPE_ILLUMINA_ASSEMBLY_LEVEL;
	}
	
	if ( $opt_read == CONFIG_TRUE ) {
	    $analysis_type = JGI_ANALYSIS_TYPE_ILLUMINA_READ_LEVEL;
	}	
    }

    my $cwd;
    
    if(safe_cwd(\$cwd, $log) != JGI_SUCCESS) {
	$log->error("Cannot identify current working directory (safe_cwd).");
	exit_failure();
    }

    if ( !defined($opt_outdir) || !(-d $opt_outdir) )  {
	unless (mkdir($opt_outdir))
	{
	    $log->error("Unable to create the output directory! " .
			"($opt_outdir)");
	    exit_failure();
	}
    }

    my $scratch_dir;

    if (create_scratch_dir($seq_unit_base, \$scratch_dir, $log) != JGI_SUCCESS) {
	exit_failure();
    }
    
    if (!chdir($scratch_dir)) {
	$log->error("Cannot chdir to $scratch_dir: $!");
	exit_failure();
    } else {
	$log->info("chdir to $scratch_dir succeeded");
    }
    
    my $symlink;

    if (!defined($opt_max_retries) || $opt_max_retries == 0) {
	$opt_max_retries = 3;
    }

	my $analysis = prepare_analysis_object($seq_unit_base, $scratch_dir, $analysis_type);
	
    if (illumina_create_symlink($seq_unit_base, \$symlink, $opt_max_retries, $log) != JGI_SUCCESS) {
    $analysis->stop(JGI_ANALYSIS_STATUS_DATA_UNAVAILABLE);
	exit_success();
    }
	
    my $is_pair_ended = CONFIG_TRUE;
    my $read_length = 0;

    if (get_working_read_length($symlink, \$is_pair_ended, \$read_length, $analysis, $log) 
	!= JGI_SUCCESS) {
    $analysis->stop(JGI_ANALYSIS_STATUS_FAILED);	
	exit_failure();
    }

    if ($opt_read == CONFIG_TRUE) {
		generate_read_qc_data($symlink, $is_pair_ended, $read_length, $analysis, $log);
    } elsif ($opt_assembly == CONFIG_TRUE) {
    	#filter out metagenome sequences
    	#if ( is_metagenome($opt_seq_unit) == FALSE ) {    
			generate_assembly_qc_data($symlink, $read_length, $analysis, $log);
    	#} else {
		#	$analysis->stop(JGI_ANALYSIS_STATUS_FILTERED);	
		#	exit_success();
    	#}
    }

    if (!chdir($cwd)) {
	$log->error("Cannot chdir back to $cwd: $!");
    $analysis->set_analysis_status(JGI_ANALYSIS_STATUS_FAILED);
    $analysis->sync_attributes();	
	exit_failure();
    } else {
	$log->info("chdir back to $cwd succeeded");
    }
    
    move_output($scratch_dir, $opt_outdir, $log);
    
    $log->info("ANALYSIS DIRECTORY: $opt_outdir\n");
    $log->info("ANALYSIS COMPLETE");
}

sub is_metagenome
{
	my ($seq_unit_name) = @_;
	my $ret = FALSE;
	
	my ($seq_unit, $lib_info, $library_name, $project_type);
	if (defined($seq_unit = JGI_Seq_Unit->new()))
	{
		if ($seq_unit->retrieve_data( (JGI_SEQ_UNIT_NAME), $seq_unit_name) == JGI_SUCCESS)
		{
    		if (!defined($library_name = $seq_unit->get_data( (JGI_SEQ_UNIT_LIBRARY_NAME) )))
    		{
				$log->error("Undefined library name for seq_unit ($seq_unit_name).");
    		}
    		else
    		{
				if (!defined($lib_info = JGI_Lib_Info->new()))
				{
	    			$log->error("Unable to create the JGI_Lib_Info object!");
				}
				else
				{
	    			if ($lib_info->add_library_set($library_name) != JGI_SUCCESS)
	    			{
						$log->error("Unable to retrieve the library information ($library_name).");
	    			}
	    			else
	    			{
						if ( defined($project_type = $lib_info->get_data(JGI_LIB_INFO_PROJECT_TYPE, $library_name, JGI_LIB_INFO_LIBRARY_NAME)))
						{
		    				if ( $project_type eq 'Metagenome' )
		    				{
		    					$ret = TRUE;
		    				}
						}
					}

	    		} # End of the library information retrieval block
			} # End of the defined JGI_Lib_Info block
        } # End of library name retrieval block		
	} # End of defining new JGI_Seq_Unit object

	return $ret;
}


sub prepare_analysis_object {
    my ($seq_unit_base, $scratch_dir, $analysis_type) = @_;

    my $sequence_unit_name = $seq_unit_base . ".srf";
    
    #
    # Check In_Progress, Failure, SUCCESSFULLY status for reprocessing
    #
    my $analysis_manager = JGI_Analysis_Manager->new();
    my @analysis_list = $analysis_manager->get_unsuccessed_analysis_by_seq_unit($sequence_unit_name, $analysis_type);
    my $analysis_num = @analysis_list;
    if ( $analysis_num == 0 ) {
	@analysis_list = $analysis_manager->get_successed_analysis_by_seq_unit($sequence_unit_name, $analysis_type);
	$analysis_num = @analysis_list;
    }
    
    my $analysis;

    if ( $analysis_num > 0 ) {
	$analysis = pop @analysis_list;
    } else {
    	$analysis = JGI_Analysis->new();
    	# the seq unit has to be added for a new analysis object
    	# and the analysis has to be registered.
    	$analysis->add_seq_unit($sequence_unit_name);
    	$analysis->register();
    }
	
    $analysis->start($analysis_type);	
    $analysis->set_data(JGI_ANALYSIS_DIR, $scratch_dir);
    $analysis->sync_attributes();

    return $analysis;
}


sub get_working_read_length {
    my ($symlink, $ref_is_pair_ended, $ref_read_length, $ref_analysis, $log) = @_;

    my $read_length_1 = 0, my $read_length_2 = 0;

    if (read_length_from_file($symlink, $ref_is_pair_ended, \$read_length_1, \$read_length_2) 
	!= JGI_SUCCESS) {
	$log->error("Unable to process $symlink and determine its read length");
	return JGI_FAILURE;
    }

    if ($$ref_is_pair_ended == CONFIG_FALSE) {
	$$ref_read_length = $read_length_1;
	$ref_analysis->set_stats_data(ILLUMINA_READ_LENGTH_1, $$ref_read_length);
    } else {
	$ref_analysis->set_stats_data(ILLUMINA_READ_LENGTH_1, $read_length_1);
	$ref_analysis->set_stats_data(ILLUMINA_READ_LENGTH_2, $read_length_2);

	if ($read_length_1 != $read_length_2) {
	    $log->warn("The lengths of read 1 ($read_length_1) and read 2 ($read_length_2) are not equal");
	}

	if ($read_length_1 < $read_length_2) {
	    $$ref_read_length = $read_length_1;
	} else {
	    $$ref_read_length = $read_length_2;
	}
    }

    if ($$ref_read_length < 10) {
	$log->error("Read length is less than 10 bps");
	return JGI_FAILURE;
    }

    return JGI_SUCCESS;
}

sub generate_read_qc_data {
    my ($seq_unit, $is_pair_ended, $read_length, $analysis_ref, $log) = @_;
    
    my @seq_unit = ( $seq_unit );
    my $sub_fastq = $seq_unit;

    # generate subsampled fasta file from fastq file
    my $sampl_per = ILLUMINA_SAMPLE_PERCENTAGE;
    
    my $read_total = 0, my $subsample_total = 0, my $basecount_total = 0;
    
    $sub_fastq =~ s/\.(bz2|gz)//;
    $sub_fastq =~ s/\.fastq//;
    $sub_fastq = "${sub_fastq}.s${sampl_per}.fastq";
    
    if (subsample_fastq_sequences($seq_unit, $sub_fastq, $sampl_per, \$basecount_total,
				  \$read_total, \$subsample_total, $log) == JGI_FAILURE) {
	$log->error("Error: Subsampling failed");
    } else {
	$log->info("Total Base Count = $basecount_total");
	$log->info("Total Read = $read_total");
	$log->info("Subsample Total = $subsample_total");
    }
    
    # sample 20 mers
    if (write_unique_20_mers($read_total, \@seq_unit, $log) != JGI_SUCCESS) {
	$log->warn("Cannot write unique 20 mers");
    }
    
    # generate read GC histograms
    illumina_read_gc($sub_fastq, $read_length, $log);
    
    if ($is_pair_ended == CONFIG_TRUE) {
	write_one_two_read_quality_stats($sub_fastq, $log);
	write_one_two_read_base_comp($sub_fastq, $log); 
    } else {
	write_single_end_read_quality_stats($sub_fastq, $log);
	write_single_end_read_base_comp($sub_fastq, $log);
    }
    
    illumina_count_q_score ($sub_fastq, $log);
    illumina_calculate_average_quality($sub_fastq, $log);
    illumina_find_common_motifs($sub_fastq, $log);
    illumina_run_bwa($sub_fastq, $log);
    illumina_run_tagdust($sub_fastq, $log);
    
    illumina_read_level_report(\$analysis_ref);
		
    unlink($sub_fastq);
}


sub generate_assembly_qc_data {
    my ($seq_unit, $read_length, $analysis_ref, $log) = @_;

    my @seq_unit = ( $seq_unit );

    if (illumina_assemble_velvet(\@seq_unit, $read_length, $log) != JGI_SUCCESS) {
	$log->error("Velvet run failed");
	$analysis_ref->stop(JGI_ANALYSIS_STATUS_FAILED);
    } else {
	illumina_assembly_level_report(\$analysis_ref);
    }
}


sub move_output {
    my ($scratch_dir, $outdir, $log) = @_;

    my $return_value;

    my $scratch_dir_base;

    if (safe_basename($scratch_dir, \$scratch_dir_base, $log) != JGI_SUCCESS) {
        $log->error("Cannot extract basename from $scratch_dir");
        return JGI_FAILURE;
    }

    my $cwd;
    
    if (safe_cwd(\$cwd, $log) != JGI_SUCCESS) {
	$log->error("Cannot identify current working directory (safe_cwd)");
	return JGI_FAILURE;
    }

    if (!chdir("$scratch_dir/..")) {
	$log->error("Cannot chdir to $scratch_dir/..: $!");
	exit_failure();
    } else {
	$log->info("chdir to $scratch_dir/.. succeeded");
    }

    if (run_local_cmd(TAR_GZIP_CMD . " ${scratch_dir_base}.tar.gz $scratch_dir_base", \$return_value, $log) != JGI_SUCCESS) {
	$log->error("Cannot tar-gzipped output ${scratch_dir_base}tar.gz");
	return JGI_FAILURE;
    }

    if (run_local_cmd(MV_CMD . " ${scratch_dir_base}.tar.gz $outdir", \$return_value, $log) != JGI_SUCCESS) { 
	$log->warn("Cannot move ${scratch_dir_base}.tar.gz");
    }

    if (!chdir($cwd)) {
	$log->error("Cannot chdir back to $cwd: $!");
	return JGI_FAILURE;
    } else {
	$log->info("chdir back to $cwd succeeded");
    }    
    
    # clean up the tmp directory in case something didn't happen correctly
    if ( -d $scratch_dir ) {
	if (!rmtree($scratch_dir)) {
	    $log->warn("Cannot remove exisiting $scratch_dir");
	} else {
	    $log->info("Successfully removed exisiting $scratch_dir");
	}
    }
}


sub create_scratch_dir {
    my ($seq_unit, $ref_scratch_dir, $log) = @_;

    my $username = `whoami`;
    chomp($username);
    my $tmp_dir = (RQC_CLUSTER_ANALYSIS_DIR) . "/$username";

    my $return_value;

    unless ( -d $tmp_dir ) {
	if (!mkdir($tmp_dir)) {
	    $log->error("Cannot mkdir $tmp_dir: $!");
	    return JGI_FAILURE;
	} else {
	    $log->info("mkdir $tmp_dir succeeded");
	    
	    if (run_local_cmd("chmod 755 $tmp_dir", \$return_value, $log) != JGI_SUCCESS) { 
		$log->error("Cannot change the permission of $tmp_dir");
		return JGI_FAILURE;
	    }
	}
    }

    if ($opt_assembly == (CONFIG_TRUE)) {
	$tmp_dir .= "/${seq_unit}_assembly_$$";
    } else {
	$tmp_dir .= "/${seq_unit}_read_$$";
    }

    unless ( -d $tmp_dir ) {
	if (!mkdir($tmp_dir)) {
	    $log->error("Cannot mkdir $tmp_dir: $!");
	    return JGI_FAILURE;
	} else {
	    $log->info("mkdir $tmp_dir succeeded");
	    
	    if (run_local_cmd("chmod 755 $tmp_dir", \$return_value, $log) != JGI_SUCCESS) { 
		$log->error("Cannot change the permission of $tmp_dir");
		return JGI_FAILURE;
	    }
	}
    }

    $$ref_scratch_dir = $tmp_dir;

    return JGI_SUCCESS;
}


sub get_seq_unit_base {
    my ($seq_unit_name, $ref_seq_unit_base, $log) = @_;

    if ($seq_unit_name =~ /(^\d{3,4}\.\d{1,2}\.\d+)\.srf/) {
	$$ref_seq_unit_base = $1;

	$log->info("Sequnece Unit Name = $seq_unit_name");
	$log->info("Sequnece Unit Base = $$ref_seq_unit_base");

	return JGI_SUCCESS;
    } else {
	$log->error("No sequence unit base extracted from $opt_seq_unit");

	return JGI_FAILURE;
    }
}


#
# Title    : exit_failure
# Function : This function provides a standard way of failing out of
#            the script.
# Usage    : exit_failure ( )
# Args     : None; relies on all variables defined in the main body of
#            the script being accessible.
# Returns  : Nothing; exits with JGI_FAILURE.
# Comments : None.
#
sub exit_failure
{
    exit(JGI_FAILURE);
} # End of the exit_failure function.



#
# Title    : exit_success
# Function : This function provides a standard way of exiting the
#            script with success.
# Usage    : exit_success ( )
# Args     : None; relies on all variables defined in the main body of
#            the script being accessible.
# Returns  : Nothing; exits with JGI_SUCCESS.
# Comments : None.
#
sub exit_success
{
    my $total_time = Time::HiRes::tv_interval($start_time);
    $log->info("Successfully ran $script_name . ($total_time seconds)");

    exit(JGI_SUCCESS);
} # End of the exit_success function.


=head1 NAME

       ill_sge_qc_job.pl

=head1 SYNOPSIS

	Usage: ill_sge_qc_job.pl [-options] <seq_unit>

=head COPYRIGHT

        Copyright (C) 2010 University of California. All rights reserved.

=head1 AUTHOR

        Jimmy Y. Huang (jhuang@lbl.gov)

=cut


