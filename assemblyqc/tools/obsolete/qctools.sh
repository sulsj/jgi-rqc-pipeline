#########################################################################
# GLOBALS
#########################################################################
OK=0
# Error codes
E_BADARG=65
E_NOFILE=66

SCRIPT=$(readlink -f $0)
SCRIPT_PATH=`dirname $SCRIPT`
JGI_TOOLS="/jgi/tools"
JGI_TOOLS_BIN="$JGI_TOOLS/bin"
ILLU_PROTOTYPE_DIR="$SCRIPT_PATH";

REFSEQ_MICROBIAL='/home/blast_db2_admin/ncbi/refseq.genomic/microbial/2009-03-25/refseq.microbial'
REFSEQ_MITOCHONDRION='/home/blast_db2_admin/ncbi/refseq.genomic/mitochondrion/2009-03-25/refseq.mitochondrion'
REFSEQ_PLANT='/home/blast_db2_admin/ncbi/refseq.genomic/plant/2008-01-30/refseq.plant'
REFSEQ_PLASMID='/home/blast_db2_admin/ncbi/refseq.genomic/plasmid/2009-03-25/refseq.plasmid'
REFSEQ_PLASTID='/home/blast_db2_admin/ncbi/refseq.genomic/plastid/2008-12-22/refseq.plastid'

LSURef='/house/blast_db/Silva/release_102.13-Feb-2010/LSURef_102_tax_silva.fasta'
SSURef='/house/blast_db/Silva/release_102.13-Feb-2010/SSURef_102_tax_silva.fasta'
LSSURef='/house/blast_db/Silva/release_102.13-Feb-2010/LSSURef_102_tax_silva.fasta'

NT='/home/blast_db2_admin/ncbi/nt/2009-03-18/nt'

SGE_LOGS="$SCRIPT_PATH/../sge_logs"

# System Commands
CUT="/usr/bin/cut"
GREP="/jgi/tools/bin/grep"
JOIN="/usr/bin/join"

#NAWK="/usr/bin/nawk"
NAWK=`which nawk`
if [ -n $NAWK ]
then
	NAWK=`which gawk`
fi

PERL="/usr/bin/env perl"
SORT="/usr/bin/sort"
TAIL="/usr/bin/tail"


# Program Commands
BLASTPARSER="$SCRIPT_PATH/blastParser_P.pl"
EFETCH_PL="$SCRIPT_PATH/efetch.pl"
HIST="$SCRIPT_PATH/histogram2.pl"
ILL_SGE_QC_PL="$SCRIPT_PATH/ill_sge_qc.pl"
MEGABLAST="$JGI_TOOLS_BIN/megablast"
NRGREP="$SCRIPT_PATH/nrgrep"

# This program depends on JGI::QC::OraJGI and JGI::QC::QCTool.
P2O="$SCRIPT_PATH/proj2organism.pl"
T2T_AWK="$SCRIPT_PATH/t2t.awk"


export PATH=${PATH}:${JGI_TOOLS}/aligners/ncbi-blast+/DEFAULT/bin
export BLASTMAT="$SCRIPT_PATH/BLAST/matrix"
export BLASTDB=/house/blast_db/public_db
export BLASTFILTER="$SCRIPT_PATH/BLAST"


function hist() { $HIST $* ; }


function gcat() {
# generic cat - handle compressed or uncmpressed input
	local IF=${*:?"Need one or more input files"}
	local EXT=$(echo $IF | $GREP -o '[^.]*$'  | sort -u) 
	if [ $(echo $EXT | wc -l) -gt 1 ] ; then echo "Files must have same extension."; return ; fi  
	local PRE=""

	case $EXT in 
		bz2) PRE="bzcat $IF " ;;
		fastq.gz|fq.gz) PRE="gzcat $IF " ;;
		fastq|fq) PRE="cat $IF";; 
		fa|fasta) PRE="cat $IF " ;;
		fa.gz|fasta.gz) PRE="gzcat $IF " ;;
		sff) PRE="cat $IF " ;;
		sff.gz) PRE="gzcat $IF " ;;
		*) echo "(gcat) Don't know how to handle input format $EXT Exiting"; return ;;
	esac
	#echo 2> "# $PRE"
	eval $PRE
}


function checkIllQualLANLfq() {
	local QF=${1:?"Specify illumina qual file in fq.bz2 or fq.gz format"} 
	if [ ! -s $QF ] ; then echo "Can't open input file $QF"; return ; fi

	# Now handles both bz2 and gz format
	local OUT=""
        OUT=${QF/.bz2};
        OUT=${OUT/.gz}.qhist ; 

	gcat $QF | $PERL -e '
	my %AverageQ; 
	while(<>){
		next if /^@/ ;
		if($.%4==0) { 
			chomp;
			# unpack C* ?
			for (split(//)) {
				# according to bfast
				# my $Q = ord($_)-64; $Total += chr(($Q<=93? $Q : 93) + 33);
				$Total += int(10 * log(1 + 10**((ord()-64)/10.0)) / 2.30); 
				$na++ ;
			}
			$AVG = int($Total /$na) ;
			$Total = 0; 
			$na=0 ;
			$AverageQ{$AVG}++;
		}
	}  
	$Key=-5; 
	while ($Key <= 40) {
		$allReads +=  $AverageQ{$Key} if(defined $AverageQ{$Key}); 
		++$Key;
	} 
	$Key=-5; 
	print "# Qavg nrd@Qavg percent\n" ;
	while ($Key <= 40){
		# col1=avg Q for read # col2=numer reads with avg Q # col3=percentage
		print "$Key\t", $AverageQ{$Key}, "\t", int($AverageQ{$Key} / $allReads * 10000) / 100, "\n" if(defined $AverageQ{$Key}); 
		++$Key;
	}' > $OUT  ;
}


function countIt() {
    local FLD=${1:?Specify one more more fields (1-based) to count} ;
    shift; # leave only file or stream in $*
    # combining more than one col into key is also doable
    $PERL -ane 'BEGIN{$e=shift;$e--}
        next if /^\s*$|^#/;
        $h{$F[$e]}++;
        END{print "$_ $h{$_}\n" foreach (sort keys %h)}' $FLD $* | $SORT -k 2,2n ;
}


function gc2gcplot {
        # it'd be nice to be able to use as a filter!!
        # plot output of GCcontent.pl

        local F=${*:?"Need one or more input files"}
        local BASE=$(for f in $(echo $F) ; do basename $f; done | $SORT -u | tr '\n' '.' | sed 's/.$//')
        GPFILE="$BASE.hist.gp"
        echo "# BASE=$BASE"
        $NAWK '{print $5+$6}' $F | $HIST - 1 0.005 > $BASE.hist ;
        printf "#\041/$JGI_TOOLS_BIN/gnuplot -persist\nset title 'GC Content %s'; set xlabel '%%GC'; set xrange [0:1]; set ylabel 'Count';\n" $BASE >$GPFILE
        printf "plot \"%s.hist\" u (\$2>0?\$2:1/0):7 w lp title \"%s\"\n" $BASE $BASE >>$GPFILE

        # strip tailing comma...
        #echo $PLOTCMD | sed 's/,$//' >> $GPFILE
        chmod +x $GPFILE
}


function run_megablast() {
    local FF=${1:?Need fasta file} ; shift ;
    local DB=${1:?"Specify a blastdb or another fasta file"} ; shift ;
    local ARGS=$* ;
    local DEFAULTS='-F "m L" -D 2 -a 1 -p 98 -e 1e-30 -J F -f T -I T' ;
    local BLASTDIR=`dirname $DB` ;

    # doesn't work for -l "file"
    local EXT=`echo $DEFAULTS $ARGS | $PERL -pne 's#-l\s+/.*/(\S+)#l$1#g;s/-e 1e\([-0-9]*\)/-E \1/g;s/-o\s+(\S+)//g;s/[ -]//g;s/"//g'`;
    OUT=${OUT:="megablast.`basename $FF`.v.`basename $DB`.$EXT"} ;

    # Log cmdd to output file
    local CMD="$MEGABLAST $DEFAULTS $ARGS -i $FF -d $DB 2>$OUT.log " ;
    echo "# $CMD" > $OUT ;
    date >> $OUT ;

    eval $CMD >> $OUT ;

    if [ ! -s $OUT ] ; then
        echo "Blast failed" 1>&2 ;
        return ;
    fi

    # parse output if D2 format
    $BLASTPARSER $OUT ;

    local LN=`$GREP -c . $OUT.parsed || echo 0`;

    if [ $LN -lt 1 ] ; then
        echo "# $BLASTPARSER failed" 1>&2 ;
    fi
}


function tax2list() {
            local TAXPROG="$EFETCH_PL -report=docsum -database=taxonomy" ;
            local ORGN;
            local ID;

	    # $# contains the number of command-line arguments that were given

            while [ $# -gt 0 ] ; do
                    ID=$(echo $1 | tr ' ' '_' | sed 's/[()]//g');
                    shift ;

                    case $ID in
                        # JGI project id
                        [2-5][0-9][0-9][0-9][0-9]+)
                            ORGN=$($P2O $ID | $NAWK '{printf("%s[lineage]\n",$2)}') ;;
                        [a-zA-Z_]*)
                            ORGN="${ID}[lineage]" ;;
                        [#]*) 
                            echo "# didn't handle id $ID" ; continue ;;
                    esac

                    CMD="$TAXPROG $ORGN | $NAWK -vi=$ID -f $T2T_AWK"
		    echo $CMD
                    eval $CMD
            done
}


function processInput() {
    # gather up input from STDIN, ARGV or FILE then run __cmd over it
    # __cmd is a function which must be defined in the caller
    local ARG
    declare -Ff __cmd 2>/dev/null 1>/dev/null || ( echo 1>&2 "(processInput) Need to define __cmd " ;return; )
    if [ $# -eq 0 ] || [ "x$1" = "x-" ]  ; then
        # read from stdin
        $SORT - | __cmd ;
    else
        for ARG in $*
        do

            if [ -s $ARG ] ; then  
		#    echo "# read from file" 
               cat $ARG | $SORT | __cmd  ;

            # read from ARGV
            elif [ "x$ARG" != "x" ] ; then
	    	#echo "# read from argv" 
               echo $ARG | $PERL -pne 's/\s+/\n/g' | $SORT | __cmd ;

            else
		    echo "(processInput) Invalid argument: '$ARG' is neither a string, file, nor stdin" ; return ;
            fi
        done
    fi
	unset -f __cmd ;
}


function tophit() {
	#  1      2      3      4       5    6    7        8       9      10        11     12      13       14
	# query subject score expect length %id q_start q_stop q_length q_strand s_start s_stop s_length s_strand
	#
	local TAB=$(echo -e "\t") ; 
	function __cmd() {
        #       sort -t"$TAB" -k1,1 -k3,3n | \
        #       nawk -F'[\t]' '{print $1,gensub(" ","  ",1,$2),$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14}'|\
        #       tr ' ' '_' | sed 's/|_/| /' | \
        #       nawk -F'[\t]' '!/#/{a[$1]=$2" "$4" "$5" "$6" "$9" "$13}END{for(e in a) print e,a[e]}' |sort -k2,2n ; 
		$SORT -t"$TAB" -k1,1 -k3,3n | \
	        $NAWK -F'[\t]' '!/^#/{
			s=gensub(" ","  ",1,$2); # creates new column:=3
			t=gensub(" ","_","g",s)
			a[$1]=t" "$4" "$5" "$6" "$9" "$13}
			END{ for(e in a) print e,a[e] }' | $SORT -k 4,4n -k 6,6n  ; 
	}
	processInput $* ;
}


