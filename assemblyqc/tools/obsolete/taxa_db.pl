#!/usr/bin/env perl

use strict;
use DBI;
use MIME::Base64;

my $gi2tax_file = "/house/homedirs/a/andreopo/test/RQC-200/rqc_tax/gi_taxid_nucl.dmp";
#my $gi2tax_file = '/house/groupdirs/QAQC/databases/tax/gi_taxid_nuclI.dmp';

#my $db = DBI->connect("dbi:SQLite:taxtwo.db", "", "", {RaiseError => 1, AutoCommit => 0});
my $db_pwd = "dHJvd3QudGFYZXMub0Jsb25nLjIyMzs=";
my $db_user = "rqc_prod";
my $db_name = "rqc_tax";
my $db_host = "scidb1.nersc.gov";

$db_pwd = "b1Jpb24uVGhyeWxsLnJhdDo5OQ=="; # dev
$db_user = "rqc_dev";
$db_name = "bryce_tax";
$db_host = "draw.jgi-psf.org";

$db_pwd = decode_base64($db_pwd);
my $db = DBI->connect("DBI:mysql:database=".$db_name.";host=".$db_host.";port=3306;", $db_user, $db_pwd, {RaiseError => 1, AutoCommit => 1});


print "Creating tables:\n";
for my $i (0..99)
{

    my $dbnum = sprintf("%02d", $i);
    my $sql = "drop table if exists gi_to_tax_".$dbnum;
    $db -> do($sql);
    $sql = "create table gi_to_tax_".$dbnum." (gi INT unsigned, taxid INT unsigned, PRIMARY KEY (gi)) engine=innodb";
    print $sql."\n";
    $db -> do($sql);
}

print "Processing $gi2tax_file\n";
open(FH, "<", $gi2tax_file);

my $i = 0;
my %sql_cnt_hash = ();
my %sql_hash = ();
while (my $line = <FH>)
{

    chomp $line;
    $i++;
    #last if ($i > 100_000); # temp
    
    my @fields = split(/\s+/, $line);
    my $gi_last = substr($fields[0], -2);
    $gi_last = sprintf("%02d", $gi_last);
    
    #my $sql = "insert into gi_to_tax_".$gi_last." (gi, taxid) values (".$fields[0].", ".$fields[1].")";
    my $sql = "(".$fields[0].", ".$fields[1]."),";
    $sql_cnt_hash{$gi_last}++;
    $sql_hash{$gi_last} .= $sql;
    
    if ($sql_cnt_hash{$gi_last} >= 100)
    {
        $sql = "insert into gi_to_tax_".$gi_last." (gi, taxid) values ";
        $sql .= $sql_hash{$gi_last};
        chop($sql); # remove trailing ,
        
        #print $sql."\n";
        $db->do($sql);        
        $sql_cnt_hash{$gi_last} = 0;
        $sql_hash{$gi_last} = "";
    }
    
    print "- processed line $i: ".localtime()."\n" if ($i % 10_000 == 0);

}


close(FH);

print "Creating indexes\n";
for my $i (0..99)
{
    my $db_num = sprintf("%02d", $i);
    my $sql = "create index gi_index_".$db_num." on gi_to_tax_".$db_num."(gi)";
    print $sql."\n";
    $db ->do($sql);
}

   


for my $i (0..99)
{


    my $table_name = "gi_to_tax_".sprintf("%02d", $i);
    my $sql = "select count(*) as cnt from ".$table_name;
    my $sth = $db->prepare($sql);
    $sth->execute();
    my ($cnt) = $sth->fetchrow_array();
    
    print "Table: $table_name = $cnt\n";
    

}

$db->disconnect();

exit(0);

__END__

insert into gi_to_tax_84 (gi, taxid) values (8786, 7227), (8788, 7227), (8790, 7227), (8791, 7227), (8792, 7227);
insert into gi_to_tax_86 (gi, taxid) values 
insert into gi_to_tax_88 (gi, taxid) values 
insert into gi_to_tax_90 (gi, taxid) values 
insert into gi_to_tax_91 (gi, taxid) values 
insert into gi_to_tax_92 (gi, taxid) values 
insert into gi_to_tax_94 (gi, taxid) values (8794, 7227)
insert into gi_to_tax_96 (gi, taxid) values (8796, 7227)
insert into gi_to_tax_98 (gi, taxid) values (8798, 7227)
insert into gi_to_tax_00 (gi, taxid) values (8800, 7227)
insert into gi_to_tax_01 (gi, taxid) values (8801, 7227)
insert into gi_to_tax_04 (gi, taxid) values (8804, 7227)
insert into gi_to_tax_09 (gi, taxid) values (8809, 7227)
insert into gi_to_tax_11 (gi, taxid) values (8811, 7227)
insert into gi_to_tax_13 (gi, taxid) values (8813, 7227)
insert into gi_to_tax_15 (gi, taxid) values (8815, 7227)



