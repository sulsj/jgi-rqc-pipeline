#!/bin/bash


SCRIPT=$(readlink -f $0)
SCRIPT_PATH=`dirname $SCRIPT`

source "$SCRIPT_PATH/qctools.sh"


###################################
#
# Two hard code paths at line 43 and 45  willdo
#
##################################

# this version uses a user specieid substr from the 5' prefix to identify duplicates with 0 0r 1 mismatch

###

function usage() {
	printf "$0 [OPTIONS] <fa> [len]\n
	
	len		sampled seq length. Default=50
	-h help		print usage
	-v verbose 	leave intermediate files. Default=rm intermediate files
	-d debug 	only print commands, don't execute
\n\n" ;
	exit 1 ;
}

function cleanup() {
	if [ $CLEANUP -eq 1 ] ; then
		rm $SAI $FE.{amb,ann,bwt,pac,rbwt,rpac,rsa,sa} $FA.fastaidx 2>/dev/null 1>/dev/null
		# rm $FE $SAM 2>/dev/null 1>/dev/null
	fi
}
function run_or_print() {
	local CMD=$* ;
	echo "# $CMD" ;
	if  [ $DEBUG -ne 1 ] ; then 
		eval $CMD
	fi
}


CLEANUP=1;
DEBUG=0;
while getopts ":dvh" options; do
   case $options in
     d) DEBUG=1 
	     ;;
     v) CLEANUP=1 
	     ;; 
     h)  usage
	     ;;
     \?) usage
	   ;;
     * ) usage
	     ;;
   esac
done
# to support post-arg args
shift $((OPTIND-1)); OPTIND=1


FA=${1:?"Specify fa file to find dupes in"}
LEN=${2:-50}
shopt -s extglob
FF=$(basename ${FA%%.fa?(sta)})
FE=$FF.ends.fa
FQ=$FF.ends.fq
SAI=$FF.sai
SAM=$FF.sam
BAM=$FF.bam


let MAX=$(egrep -c '^[@>]' $FA)
let MAX=$MAX-1

### 
# main

# TODO: should check then fail if not formatted correclty
# this assumes we have length=N tags in the fa header ie. 454 reads !!!
# TODO: check above assumption

# 1. make fasta index to allow rapidly making pseudoreads
# for this application cut would work just as well
CMD="$CUT -c1-$LEN $FA > $FE"
run_or_print $CMD 

# 1a. fix up read names for bwa
$PERL -i -pne 's/^([>@])(.*)/\1$./' $FE

# 2. index the pseudoreads
CMD="$JGI_TOOLS_BIN/bwa index $FE"
run_or_print $CMD

# 3. bwa align reads against index
# -t 4 <threads>
# -k 2 <max edit dist in seed>
# -d 5 <disallow long deln 5 bp from end>
# -R proceed with suboptimal alignment even w/ repeat - semantics changed in bwa as of 9/2009
# -n 4 max number diffs
CMD="$JGI_TOOLS_BIN/bwa aln -t 4 -k 8 $FE $FE > $SAI" 
run_or_print $CMD

# 4. convert bwa align output to text (sam format)
#
# TODO: should we use sampe ????
# -n 100 <report up to 100 aligments> -- unfortunately changes the output format to column-wise brief format
CMD="$JGI_TOOLS_BIN/bwa samse  $FE $SAI $FE > $SAM"
run_or_print $CMD

# 5. dump self-aligns and align w >2 mismatches which corresponds to 95% accuracy in a 40bp seed
# TODO -- make sure we don't toss out all copies...
MAXERR=2
CMD="$NAWK '\$1!=\$3 && \$3!~/\*/ && NF>3' $SAM > reads.$FF.dup.hits"
run_or_print $CMD

# 6. make brief output of passing aligns
#print just read ids
CMD="$CUT -d' ' -f1 reads.$FF.dup.hits > reads.$FF.dup"
run_or_print $CMD 


VDUP=$( ${NRGREP} -c . reads.$FF.dup | ${CUT} -d' ' -f2) 

CMD="$NAWK -vtot=$MAX -vdup=$VDUP 'BEGIN{print \"# dup tot dup/tot\"} END{print dup,tot,dup/tot}' /dev/null > $FF.bwa.summary"
run_or_print $CMD

cat $FF.bwa.summary

# 7. cleanup
cleanup
