#!/usr/bin/env perl

##################
# takes single line fastq file and prints count of index reads. 
# jhan 03.2010
##################
use strict;
use warnings;
use File::Basename;

unless ( $ARGV[0] ) { 
  print <<"USAGE";
usage: $0 <fastq file or '-'>

Converts a fastq file ocntaining indexes into a fastq file of indexes

USAGE

  exit;
};

my $input_file = $ARGV[0];

unless ( $input_file eq '-' || -f $input_file ) {
  print "cannot find file: $ARGV[0]\n";
}

open ( my $in_fh, $input_file );

while ( my $line = <$in_fh> ) {
  # print "A\n";
  next unless $line =~ /^@/ && $line =~ /Q=/;
  # print "B\n";

  chomp $line;
  # get rid of read identifier if it exists
  my $line_strand_rm = $line =~ s/\/1//;
  # parse out index seq ($1) and index qual ($2)
  $line =~ /.+#(.+)\sQ=(.+)/;
  my $iseq = uc ( $1 );
  my $iqual = $2;

  print "$line\n$iseq\n+\n$iqual\n";

}

close ( $in_fh );



