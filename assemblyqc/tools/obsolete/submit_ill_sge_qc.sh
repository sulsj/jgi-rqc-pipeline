#!/bin/bash

SCRIPT=$(readlink -f $0)
SCRIPT_PATH=`dirname $SCRIPT`

source "$SCRIPT_PATH/qctools.sh"

export HOST="hyperion"
source "/opt/sge/$HOST/common/settings.sh"
cd $SGE_LOGS

$PERL $ILL_SGE_QC_PL
