#!/usr/bin/env perl

###################################################################################
#
#       Name: 		seq_file_find.pl
#       Function:	Displays information about each next-gen seq. files 
#			See -h option for details.
#
#       Author:		James Han	
#       Date:		2009.05.11
#
##################################################################################

use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;

use FindBin '$RealBin';
use lib "$RealBin/../lib";

use JGI_Constants;


# variables to hold script name and path used for logging
my ( $script_name, $script_path );
$script_name = 'seq_file_find3.pl';
$script_path = $0;

my $FFF_SFF_DUMP_TXT = (FFF_SFF_DUMP_TXT);
my $FFF_SFF_DUMP_NO_VALIDATE_TXT = (FFF_SFF_DUMP_NO_VALIDATE_TXT);
my $ILL_FQ_DUMP_TXT = (ILL_FQ_DUMP_TXT);
my $ILL_FQ_DUMP_NO_VALIDATE_TXT = (ILL_FQ_DUMP_NO_VALIDATE_TXT);


#
# Declare GetOption variables
#
my ( $opt_display_454, 
     $opt_display_illumina, 
     $opt_display_all, 
     $opt_lib,
     $opt_bio,
     $opt_sid,
     $opt_pmo,
     $opt_pid,
     $opt_run,
     $opt_delete_header, 
     $opt_no_validate,
     $help 
   ) = ();

GetOptions(
        '454_only'			=> \$opt_display_454,
	'illumina_only'			=> \$opt_display_illumina,
	'all_platforms'			=> \$opt_display_all,
	'lib'				=> \$opt_lib,
	'bio'				=> \$opt_bio,
	'sid'				=> \$opt_sid,
	'pmo'				=> \$opt_pmo,
	'pid'				=> \$opt_pid,
	'run'				=> \$opt_run,
	'header'			=> \$opt_delete_header,
	'no'				=> \$opt_no_validate,
	'help!'                 	=> \$help,
	) or pod2usage(2);
	pod2usage(1) if $help;

#
# If no platform specific options are given, then all platforms
# are searched.
#
if ( !defined($opt_display_454) && !defined($opt_display_illumina)  ) {
	$opt_display_all = 'all_platforms';
}


####################################################################
#
# START - MAIN PROGRAM
#
####################################################################

# print column names unless -no_column_header option is given
unless ( $opt_delete_header ) {
  my $header =            '#bioclass'
                        . ', source_dna_name'
                        . ', pmo_proj_id'
                        . ', source_dna_number'
                        . ', proj_id'
                        . ', lib_name'
                        . ', lib_type'
                        . ', cdna_type'
                        . ', run_date'
                        . ', run_id'
                        . ', lane'
                        . ', lane_size'
                        . ', basecall_event'
                        . ', approx_Mbp'
                        . ', approx_reads'
                        . ', read_length'
                        . ', qc_date'
                        . ', qc_result'
                        . ', file_path';

  print "$header\n";
}
#
# If search term is provided as an argument then pass search term
# to the platform specific searches. Else, dump all data according
# to any platform specific options that were given.
#
if ( $ARGV[0] ) {
  foreach my $search_term ( @ARGV ) {
	if ( $opt_display_454 || $opt_display_all ) {
          Display_454_info ( $search_term );
  	}

  	if ( $opt_display_illumina || $opt_display_all ) {
	  Display_illumina_info ( $search_term );
  	}
  }
} else {             
  	
	if ( $opt_display_454 || $opt_display_all ) {
	  Display_454_info ( );
	}

	if ( $opt_display_illumina || $opt_display_all ) {
	  Display_illumina_info ( );
	}
}

####################################################################
#
# END - MAIN PROGRAM
#
####################################################################


#
# Subroutine  - Display 454 information. Can be called by passing 
# 		a search string that will be used to search and
# 		limit results.
#
sub Display_454_info {
	my $search_term_454 = shift;
   #
   # 454 information is pulled from LIMS_data.pm data dump
   #
   my @four_five_four_info = `grep -v '#' $FFF_SFF_DUMP_TXT`;
   if ( $opt_no_validate ) {
	@four_five_four_info = `grep -v '#' $FFF_SFF_DUMP_NO_VALIDATE_TXT`;
   }

   foreach my $four_five_four_row ( @four_five_four_info ) {

	#
	# Split ':_:' delimited record into separate fields.
	#
	my ( $bioclass,
        $dna_name,
        $pmo_proj_id,
        $dna_num,
        $proj_id,
        $library,
        $library_type,
        $cdna_type,
        $run_date,
        $run_id,
        $region_number,
        $region_size,
        $analysis_id,
        $bases,
        $reads,
        $read_length,
        $qc_date,
        $qc_status,
        $sff_name ) = split ( /:_:/ , $four_five_four_row );

	#
	# Print all rows if search term is not defined.
	#
	if ( !defined ($search_term_454) ) {
	  $four_five_four_row =~ s/:_:/,/g;
	  print "$four_five_four_row";

	#
	# If search term is given, print only the rows with string matching query term
	#
	} elsif ( $bioclass =~ /$search_term_454/i 
		|| $pmo_proj_id eq $search_term_454
		|| $dna_num eq $search_term_454
		|| $proj_id eq $search_term_454
		|| $library eq $search_term_454
		|| $library_type =~ /$search_term_454/
		|| $run_id eq $search_term_454
		|| $region_number eq $search_term_454
		|| $sff_name =~ /$search_term_454/ 
		|| $qc_status =~ /$search_term_454/i ) {

		$four_five_four_row =~ s/:_:/,/g;

		if ( $opt_lib ) {
		  print "$four_five_four_row" if $library eq $search_term_454;
		} elsif ( $opt_bio ) {
		  print "$four_five_four_row" if $bioclass =~ /$search_term_454/i;
		} elsif ( $opt_sid ) {
		  print "$four_five_four_row" if $dna_num eq $search_term_454;
		} elsif ( $opt_pmo ) {
		  print "$four_five_four_row" if $pmo_proj_id eq $search_term_454;
		} elsif ( $opt_pid ) {
		  print "$four_five_four_row" if $proj_id eq $search_term_454;
		} elsif ( $opt_run ) {
 		  print "$four_five_four_row" if $run_id eq $search_term_454;
		} else {
		  print "$four_five_four_row";
		}
	}
   }	

}

#
# Subroutine - Display illumina information. Can be called by passing
# 	       a search string that will be used to search and limit
# 	       results.
#
sub Display_illumina_info {
	
	my $search_term_illumina = shift;
   #
   # illumina information is pulled from LIMS_data.pm data dump
   #
   my @illumina_info = `grep -v '#' $ILL_FQ_DUMP_TXT`;
   if ( $opt_no_validate ) {
        @illumina_info = `grep -v '#' $ILL_FQ_DUMP_NO_VALIDATE_TXT`;
   }

   foreach my $illumina_row ( @illumina_info ) {
   	
	#
	# Split ':_:' delimited record into separate fields.
	#
	   
   my ( $bioclass,
        $dna_name,
        $pmo_proj_id,
        $dna_num,
        $proj_id,
        $library,
        $library_type,
        $cdna_type,
        $run_date,
        $run_id,
        $lane_number,
        $lane_size,
        $analysis_id,
        $bases,
        $reads,
        $read_length,
        $qc_date,
        $qc_status,
        $illumina_file_path ) = split ( /:_:/ , $illumina_row );

	#
	# Print all rows if search term is not defined.
	# Option added to limit output fields to make the output more
	# readable.
	#
	if ( !defined ( $search_term_illumina ) ) {
	  $illumina_row =~ s/:_:/,/g;
	  print "$illumina_row";

	#
	# If search term is given, print only the rows with string matching query term 
	#
	} elsif ( $bioclass =~ /$search_term_illumina/i
	     || $pmo_proj_id eq $search_term_illumina
             || $dna_num eq $search_term_illumina
	     || $proj_id eq $search_term_illumina
	     || $library eq $search_term_illumina
	     || $library_type =~ /$search_term_illumina/
	     || $run_id eq $search_term_illumina
	     || $lane_number eq $search_term_illumina
	     || $illumina_file_path =~ /$search_term_illumina/
	     || $qc_status =~ /$search_term_illumina/i ) {
	  
		$illumina_row =~ s/:_:/,/g;	
                
	 	if ( $opt_lib ) {
                  print "$illumina_row" if $library eq $search_term_illumina;
                } elsif ( $opt_bio ) {
                  print "$illumina_row" if $bioclass =~ /$search_term_illumina/i;
                } elsif ( $opt_sid ) {
                  print "$illumina_row" if $dna_num eq $search_term_illumina;
                } elsif ( $opt_pmo ) {
                  print "$illumina_row" if $pmo_proj_id eq $search_term_illumina;
                } elsif ( $opt_pid ) {
                  print "$illumina_row" if $proj_id eq $search_term_illumina;
                } elsif ( $opt_run ) {
		  print "$illumina_row" if $run_id eq $search_term_illumina;
		} else {
                  print "$illumina_row";
                }
	}
   }
}


=head1 NAME

        seq_file_find.pl

=head1 SYNOPSIS

	Usage: seq_file_find.pl [-options] [search_term...]

	Searches the Production Informatics database and sequence directories to locate 
	metadata and sequence file paths. Paths to FASTQ files are located for Illumina and
	SFF files for 454. 

	Columns displayed:
		bioclassification
                source dna name
                pmo project id
                source_dna_number
                project id
                library name
                library type
                cdna type
                run date
                run id
                region/lane
                region/lane_size
                basecall event
                approximate Mbp
                approximate reads
                average read length/run format
                qc date
                qc_result
                file_path
	
	Multiple arguments are accepted but processed as separate searches. Each arugment
	should be separated by a space. Search terms containing a space should be quoted. 
	
	The search is performed as a string search across selected fields
	(see below for match criteria).

	Options:
	
	-454_only 			Search and display results for 454 platform only. 
					Whenever possible, specifying a platform option is recommended
					to prevent unwanted search term hits from the other platform.

	-illumina_only			Search and display results for illumina platform only.
					Whenever possible, specifying a platform option is recommended
                                        to prevent unwanted search term hits from the other platform.
					The run id must be >425. 

	-lib				Match on library name only

	-bio				Match on bioclassification only

	-sid				Match on sample id only

	-pmo				Match on PMO project id only

	-pid				Match on project id only

	-run				Match on run id only
	
	-header				Remove header from output. 

	-help				Display this message

	Examples: 

		-search for GCUW library 
		(OK) seq_file_find.pl GCUW
			or
		(BETTER) seq_file_find.pl -lib GCUW

		-search for 'ecoli' bioclassification projects on 
		the illumina platform only
		(OK) seq_file_find.pl -i coli
			or
		(BETTER) seq_file_find.pl -i -bio coli

                -search for 'ecoli' bioclassification projects on
                the illumina platform only and remove column names
		from the output.
                (OK) seq_file_find.pl -i -d coli
			or
		(BETTER) seq_file_find.pl -i -d -bio coli

		-search for project id '4086913' on all platforms
		(OK) seq_file_find.pl 4086913
			or
		(BETTER) seq_file_find.pl -pid 4086913

		-search for project id '4086913' on 454 only
		(OK) seq_file_find.pl -4 4086913
			or
		(BETTER) seq_file_find.pl -4 -pid 4086913

		-search for illumina files that have passed QC
		seq_file_find.pl -i pass

		-search for files without a QC
		seq_file_find.pl null

		-search for 454 run id '11070'
		(OK) seq_file_find.pl -4 11070
			or 
		(BETTER) seq_file_find.pl -4 -run 11070

                -search for 'Synechococcus sp. PCC 7502'on the Illumina platform
                (OK) seq_file_find.pl -i 'Synechococcus sp. PCC 7502'
			or 
		(BETTER) seq_file_find.pl -i -bio 'Synechococcus sp. PCC 7502'

 		-search for GGHT, GGBA, and GBBN libraries
		seq_file_find.pl GGHT GGBA GBBN

		-search for 'Chlamydomonas reinhardtii CC-1021' bioclassification projects 
		and library on both platforms.
		seq_file_find.pl 'Chlamydomonas reinhardtii CC-1021' GGIG

		-dump all illumina data
		seq_file_find.pl -i

		-dump all data
		seq_file_find.pl

	Search Methods:
		-Fields requiring exact match:
			project id
			library name
			run id
			region number (454)
			lane number (illumina)
			pmo project id
			sample id

		-Fields matched with case-sensitive regex
			library type
			sff path (454)
			fastx path (illumina)
		
		-Fields matched with case-insensitive regex
			bioclassification
			qc status


=head1 COPYRIGHT

	Copyright (C) 2009 University of California. All rights reserved.

=head1 AUTHOR

        James Han (jkhan@lbl.gov)

=cut

#
# VERION HISTORY
#
# V1.0 2009.05.11 jhan - orignal version
# V1.1 2009.05.19 jhan - added readable option for easy viewing of data
# V1.2 2009.05.20 jhan - changed field delimiter to '::' b/c some bioclass
# 			 info contained a ','
# V1.3 2009.05.25 jhan - added -first and -last analysis options for 454
# 			 platform
# V1.4 2009.10.10 jhan - now uses LIMS_data.pm to get data. delimiter is ':_:'
#
# V1.5 2010.01.22 jhan - -d, -fq, -fa options added
# v1.6 2010.05.03 jhan - -readable removed, more accurate search results for illumina
