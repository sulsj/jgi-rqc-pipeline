#!/usr/bin/env perl
#NOJGITOOLS

use strict;
use warnings;

my @bases = qw ( A T G C);
my $mismatches = $ARGV[1];
$mismatches = 1 if ( !defined($mismatches) || $mismatches <= 0 );
my $total = 0;
my %mismatch_seqs = ( $ARGV[0] => 0 );

  for ( my $j = 1; $j<=$mismatches; $j++ ) {
    foreach my $seq ( keys %mismatch_seqs ) {
    my $seq_len = length ( $seq );
    my $i = 0;
    for ( 1..$seq_len ){
      my @split_bases = split ('', $seq );
      foreach my $b ( @bases ) {
        my @working_bases = @split_bases;
        $working_bases[$i] = $b;
        my $join_work = join ('', @working_bases);
        next if defined( $mismatch_seqs{ $join_work } );
        $total++;
        $mismatch_seqs{ $join_work } = $j;
        #print "$total $i $join_work $mismatches \n";
      }
     
      $i++;
    }}
  }
  my $c =0;
foreach my $s ( sort keys %mismatch_seqs ) {
$c++;
print "$s\n"   
}



