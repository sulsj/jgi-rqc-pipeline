#!/usr/bin/env perl
#NOJGITOOLS

##################
# takes fastq lines from stdin and prints fasta to stdout
# jhan 03.2010
##################
use strict;
use warnings;

while(my $header = <STDIN>) {
      my $read = <STDIN>;
      my $plus = <STDIN>;
      my $qual = <STDIN>;
      $header =~ s/^@/>/; 
      print $header . $read;
}

#pre 2012.08 code
#while ( my $line = <STDIN> ) {
#  if ( $line =~ /^\@/ ) {
#     $line =~ s/^\@/>/;
#     print "$line";
#     $line = <STDIN>;
#     print "$line";
#  }
#}
