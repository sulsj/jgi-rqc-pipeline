#!/usr/bin/env perl
#NOJGITOOLS

#
# Tool for removing cre-lox adatpers from illumina fq files.
# Crelox adatper can occur anywhere in the read.
# Errors are allowed
# Uses supplied kmer size in the interior of the read.
# Uses kmer sizes smaller than the supplied kmer size at the ends of the reads.
# For example, if the supplied kmer size is 20bp, it tries to find a match of 20bp (minus error). 
# At the ends of the reads, say last 15bp, it will attempt to find a match of 15bp (minus error).
# Artifacts sequences shorter than the kmer are skipped
# 2011.05 jhan

use strict;
use warnings;
use Getopt::Long;

use Data::Dumper;
$| = 1;

# get option variables
my ( $opt_kmer, $opt_debug, $opt_max_errors, $opt_min_len);

GetOptions(
  'l=i'       => \$opt_min_len,
  'k=i'       => \$opt_kmer,
  'd'         => \$opt_debug,
  's=i'       => \$opt_max_errors
);


my $p_artifact_db = $ARGV[0];
my $p_in_file = $ARGV[1];

my $usage = <<"USAGE";
  removes linker or artifact sequences from fastq files

  $0 [options] <linker/artifact.fa> <fastq or - or STDIN>

  options:
    -k <int>	kmer length. match kmers of <int> length. kmers at the ends of reads can be shorter. default = 25 
    -s <int>	max number of errors allowed (Hamming). default 1 
   
USAGE

unless ( @ARGV ) {
  print $usage;
  exit;
}  

# default max errors to 1
if ( !defined($opt_max_errors) || $opt_max_errors == 0 ) {
  $opt_max_errors = 1;
}

# default kmer to 25
my $p_kmer;
if ( $opt_kmer ) {
  $p_kmer = $opt_kmer;
} else {
  $p_kmer = 25;
}

# default minimum length to 45
my $p_min_len;
if ( $opt_min_len ) {
  $p_min_len = $opt_min_len;
} else {
  $p_min_len = 45;
}

#
# Create uniq kmer database of artifact sequences
#

# artifacts hash  key = kmer , value 
my %artifact_kmer;

foreach my $kmer ( 1..$p_kmer ) {
  open ( my $artifact_db_fh, $p_artifact_db ) or die $!;
  while ( my $header = <$artifact_db_fh> ) {
    next unless $header =~ /^>/;
    my $seq = <$artifact_db_fh>;
    chomp ( $header, $seq );
    my $seq_len = length ( $seq );
  
    # warn and skip sequence if kmer is greater than a read length
    unless ( $p_kmer <= $seq_len ) {
      print STDERR "WARNING: a artifact sequence was shorter than the specified kmer length!\n$header\n";
      next;
    }

    #print "$header $seq_len\n$seq\n";
    my $start_index = 0;
    # initialize end point
    my $end = $start_index + $kmer;

    # grab kmers until until the end point reaches the read length
    while ( $end <= $seq_len ) {
      my $substring = substr ( $seq, $start_index, $kmer );
      my $substring_rev_comp = $substring;
      $substring_rev_comp =~ tr/ATGC/TACG/;
      $substring_rev_comp = reverse $substring_rev_comp;
    #  print "$header $end\n$substring\n";
      # increment starting point by 1bp
      $start_index++;
      # increment end point
      $end++; 
        #my @substring_permutes = `/house/groupdirs/QAQC/scripts/permute_seq2.pl $substring $opt_max_errors`;
        my @substring_permutes = permute_seq2($substring, $opt_max_errors);
        foreach my $permuted_strings ( @substring_permutes ) {
 	  chomp $permuted_strings;
	  unless ( $artifact_kmer{$permuted_strings} ){
	    $artifact_kmer{$permuted_strings} = 1;
	  }
	}
        #my @substring_revcomp_permutes = `/house/groupdirs/QAQC/scripts/permute_seq2.pl $substring_rev_comp $opt_max_errors`;
        my @substring_revcomp_permutes = permute_seq2($substring_rev_comp, $opt_max_errors);
	foreach my $permuted_strings_rev_comp ( @substring_revcomp_permutes ) {
	  chomp $permuted_strings_rev_comp;
	  unless ( $artifact_kmer{$permuted_strings_rev_comp} ) {
            $artifact_kmer{$permuted_strings_rev_comp} = 1;
          }
        }

    print STDERR "Loading kmer db: $opt_max_errors error kmers of $substring $substring_rev_comp loaded\r"; 
    }

  }
  close ( $artifact_db_fh ) or die $!;
}

#
# Open fastq file and trim sequences
# Take the start mer of a read and calculate hamming dist with 20mer artifact database
# Trim it and everything after, if the hamming diatance is less than or equal to the allowed number of errors.
# Otherwise, slide 1 nucleotide and repeat. When the kmer from the read is less than the specified kmer value
# compare using artifact kmers that are the same length as the kmer from the read. 
# 

open ( my $p_in_file_fh, $p_in_file ) or die $!;

while ( my $r1_header = <$p_in_file_fh> ) {
  chomp $r1_header;
  next unless $r1_header;
  my $r1_seq = <$p_in_file_fh>;
  my $r1_plus = <$p_in_file_fh>;
  my $r1_qual = <$p_in_file_fh>;
  my $r2_header = <$p_in_file_fh>;
  my $r2_seq = <$p_in_file_fh>;
  my $r2_plus = <$p_in_file_fh>;
  my $r2_qual = <$p_in_file_fh>;
  
  last unless ($r1_header || $r1_seq || $r1_plus || $r1_qual || $r2_header || $r2_seq || $r2_plus || $r2_qual);

  chomp ( $r1_header, $r1_seq, $r1_plus, $r1_qual, $r2_header, $r2_seq, $r2_plus, $r2_qual );
  
  my ( $new_r1_seq, $new_r1_qual, $new_r2_seq, $new_r2_qual ) = '';
  ( $new_r1_seq, $new_r1_qual ) = trim_read ( $r1_seq, $r1_qual );
  ( $new_r2_seq, $new_r2_qual ) = trim_read ( $r2_seq, $r2_qual );

  if ( $new_r1_qual && $new_r2_qual && $new_r1_seq && $new_r2_seq && length($new_r1_seq) >= $p_min_len && length($new_r2_seq) >= $p_min_len) {
     print "$r1_header\n$new_r1_seq\n$r1_plus\n$new_r1_qual\n";
     print "$r2_header\n$new_r2_seq\n$r2_plus\n$new_r2_qual\n";

  }
}

close ( $p_in_file_fh ) or die $!;

# function:
# given a read and corresponding qual stings tries to trim the read and qual based on the stated rules
# returns (trimmed) read and qual

sub trim_read {
  my $read = $_[0];
  my $qual = $_[1];
   
  my $rl = length ( $read );

  # begin looking at the beginning for the read
  my $start_index = 0;

  # save original read for debugging
  my $trimmed_read = $read;
  my $trimmed_qual = $qual;


  while ( $start_index < $rl ) {
    
    my $read_kmer = substr ( $read, $start_index, $p_kmer );
    if ( $artifact_kmer{$read_kmer} ) {
       if ( $start_index == 0 ) {
       # kmer is at beginning of read
	 $trimmed_read = '';
   	 $trimmed_qual = '';
       } else {
       # not at start
	 $trimmed_read = substr ( $read, 0, $start_index );  
         $trimmed_qual = substr ( $trimmed_qual, 0, $start_index );
       }
 
       if ( $trimmed_read && $trimmed_qual ) {
	  return ( $trimmed_read, $trimmed_qual );
       } 
     }
       
    $start_index++;
    }
}
sub permute_seq2 {
	my $mismatches = $_[1];
	my @bases = qw ( A T G C);
	
	$mismatches = 1 if ( !defined($mismatches) || $mismatches <= 0 );
	my $total = 0;
	my %mismatch_seqs = ( $_[0] => 0 );

	for ( my $j = 1; $j<=$mismatches; $j++ ) {
    	foreach my $seq ( keys %mismatch_seqs ) {
    		my $seq_len = length ( $seq );
    		my $i = 0;
    		for ( 1..$seq_len ){
      			my @split_bases = split ('', $seq );
      			foreach my $b ( @bases ) {
        			my @working_bases = @split_bases;
        			$working_bases[$i] = $b;
        			my $join_work = join ('', @working_bases);
        			next if defined( $mismatch_seqs{ $join_work } );
        			$total++;
        			$mismatch_seqs{ $join_work } = $j;
        			#print "$total $i $join_work $mismatches \n";
      			} 
      			$i++;
    		}
    	}
	}
	my @ret = ();
	my $c =0;
	foreach my $s ( sort keys %mismatch_seqs ) {
		$c++;
		#print "$s\n";
		push (@ret, "$s\n");
	}
	
	return @ret;
}
