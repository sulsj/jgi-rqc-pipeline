#!/usr/bin/env perl
# ===========================================================================
#
#                            PUBLIC DOMAIN NOTICE
#               National Center for Biotechnology Information
#
#  This software/database is a "United States Government Work" under the
#  terms of the United States Copyright Act.  It was written as part of
#  the author's official duties as a United States Government employee and
#  thus cannot be copyrighted.  This software/database is freely available
#  to the public for use. The National Library of Medicine and the U.S.
#  Government have not placed any restriction on its use or reproduction.
#
#  Although all reasonable efforts have been taken to ensure the accuracy
#  and reliability of the software and data, the NLM and the U.S.
#  Government do not and cannot warrant the performance or results that
#  may be obtained by using this software or data. The NLM and the U.S.
#  Government disclaim all warranties, express or implied, including
#  warranties of performance, merchantability or fitness for any particular
#  purpose.
#
#  Please cite the author in any work or product based on this material.
#
# ===========================================================================
#
# Author:  Oleg Khovayko http://olegh.spedia.net
#
# File Description: eSearch/eFetch calling example
#  
# ---------------------------------------------------------------------------

# ---------------------------------------------------------------------------
# Define library for the 'get' function used in the next section.
# $utils contains route for the utilities.
# $db, $query, and $report may be supplied by the user when prompted; 
# if not answered, default values, will be assigned as shown below.

use strict;
use warnings;

use LWP::Simple;
use Getopt::Long ;
use Pod::Usage ;
my $utils = "http://www.ncbi.nlm.nih.gov/entrez/eutils";

my ($DB,$REPORT,$retmax,$retmode,$DEBUG,$HELP,$COUNT,$SUMMARY,$MAXFETCH,$HISTORY);
$COUNT=0 ;
$DEBUG=0 ;
$SUMMARY=0 ;
$MAXFETCH=499 ;   # max number of queries per try
$DB="nucleotide" ;# nucleotide popset genome protein sequences taxonomy literature
$retmode="text" ; # text xml html asn.1
$REPORT="native" ;# literature: docsum brief abstract citation medline asn.1 mlasn1 uilist sgml gen
                  # nucleotide: native fasta gb gbwithparts est gss gp uilist
$retmax=1  ;
GetOptions(
	"database=s"    => \$DB,
        "report=s"      => \$REPORT,
        "retmode=s"   => \$retmode,
        "retmax=i"    => \$retmax,
        "count!"    => \$COUNT,
        "history!"    => \$HISTORY,
	"debug!"    => \$DEBUG,
        "summary!"  => \$SUMMARY,
        "batch=i"  => \$MAXFETCH,
	"help!"     => \$HELP, 
) or pod2usage(2) ;
pod2usage(1) if ( $HELP ) ;


#my $query  = "NC_002759" ;#"Pseudomonas syringae" ;#ask_user("Query",    "zanzibar");
my @query = @ARGV if @ARGV ; 
chomp(@query = <STDIN>) unless @query ; 
pod2usage(2) unless @query ;



# ---------------------------------------------------------------------------
# $esearch contains the PATH & parameters for the ESearch call
# $esearch_result containts the result of the ESearch call
# the results are displayed and parsed into variables 
# $Count, $QueryKey, and $WebEnv for later use and then displayed.

my $esearch = "$utils/esearch.fcgi?" .  "db=$DB&retmax=$retmax&usehistory=y&term=";

# don't submit more than 500 at a time
for (my $i=0; $i<scalar(@query); $i+=$MAXFETCH) {
    my $j = $i+$MAXFETCH-1 ;
    $j = $#query  if ( $j > $#query ) ;# Don't walk off end of input
    my $query=join(',',@query[$i .. $j]) ;
    my $esearch_result = get($esearch . $query);

    print "db=$DB report=$REPORT query=$query\n" if $DEBUG ;

    if ( $SUMMARY ) {
        print "\nESEARCH RESULT: $esearch_result\n"  ;
        exit ;
    }

    if (!defined($esearch_result)) {
	next;
    }

    $esearch_result =~ 
      m|<Count>(\d+)</Count>.*<QueryKey>(\d+)</QueryKey>.*<WebEnv>(\S+)</WebEnv>|s;

    my $Count    = $1;
    my $QueryKey = $2;
    my $WebEnv   = $3;


    print "Count = $Count; QueryKey = $QueryKey; WebEnv = $WebEnv\n" if $DEBUG ;

    if ( $COUNT  ) {
        print "$Count\n" ; 
        exit ;
    }

    my $efetch = "$utils/efetch.fcgi?rettype=$REPORT&retmode=$retmode&" .
                   "db=$DB&query_key=$QueryKey&WebEnv=$WebEnv";
    my $efetch_result = get($efetch);

    if (defined($efetch_result)) { 
	print "$efetch_result\n" ;
	print "WebEnv=$WebEnv\n" if $HISTORY;
    }
}

__END__


=head1 NAME

efetch.pl 

=head1 SYNOPSIS

  efetch.pl [OPTIONS]

  Options:

  --database 	Specify database [default=nucleotide,popset,genome,protein,sequences,taxonomy,literature]
  --report 	Report type [default=native]
  		literature database: docsum,brief,abstract,citation,medline,asn.1,mlasn1,uilist,sgml,gen 
		nucleotide database: native,fasta,gb,gbwithparts,est,gss,gp,uilist
  --retmode	Return mode [default=text,xml,html,asn.1]
  --retmax 	Number of records to return [default=1]
  --count	Just return number of records found
  --debug	Print debug output
  --summary	Print only summary of results
  --help	Print this message


=head1 DESCRIPTION

 Fetch data from Entrez

=head1 COPYRIGHT

Copyright (C) 2003 Univ. of California. All rights reserved.

=head1 AUTHOR

Alex Copeland (accopeland@lbl.gov)

=cut
