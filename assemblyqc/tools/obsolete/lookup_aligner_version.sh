#!/bin/bash

# set -e
# set -o pipefail

# echo "Z"
# /house/groupdirs/QAQC/rqc_archive/jgi_home/copeland/local/IX86/bin/blat-3.0
# /house/groupdirs/QAQC/rqc_archive/jgi_home/copeland/local/IX86/bin/blat
module load blat
blat | egrep "^blastall|^blat|^megablast"
#NOJGITOOLS /jgi/tools/bin/blat
# - | grep -E "^blastall|^blat|^megablast"
# echo "exit status " $?
# echo "GGGG" | grep G
# echo "A"
# echo "exit status " $?
# echo "B"
