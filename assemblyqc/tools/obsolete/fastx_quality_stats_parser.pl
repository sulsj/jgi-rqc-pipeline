#!/usr/bin/env perl


use strict;
use warnings;

# parses base count info from illumina-pipeline/bin/fastx_quality_stats
# to create percentages


  print "#id count a_count a_per t_count t_per g_count g_per c_count c_per n_count n_per\n";

while ( <STDIN> ) {
  chomp;
  my @d = split;
  
  my $id = $d[0];
  my $tot = $d[1];

  my $a_tot = $d[12];
  my $a_per = ($a_tot/$tot)*100;
  
  my $c_tot = $d[13];
  my $c_per = ($c_tot/$tot)*100;

  my $g_tot = $d[14];
  my $g_per = ($g_tot/$tot)*100;

  my $t_tot = $d[15];   
  my $t_per = ($t_tot/$tot)*100;
 
  my $n_tot = $d[16];
  my $n_per = ($n_tot/$tot)*100;

  print "$id $tot $a_tot $a_per $t_tot $t_per $g_tot $g_per $c_tot $c_per $n_tot $n_per\n";
}



