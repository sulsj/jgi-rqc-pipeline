#!/usr/bin/env perl 
#
# proj2organsim.pl -- lookup organism given project id
#
# -- Copyright (C) 2003 Univ. of California --
# $Id: proj2organism.pl,v 1.2 2010-12-11 00:15:10 hjshapiro Exp $
# *041111-jam, updated sql to use the bio_class_genus_species table and ids as
#              genus_species_id is deprecated
# *041111-jam, refactored almost entirely and switched to OraJGI for a demo;
#
use strict;
use warnings;
use Carp;
use Getopt::Long;
use Pod::Usage;
use JGI::QC::OraJGI;
use JGI::QC::QCtool;

my ( $help, $brief, $strain);

my $PASSTHRU = 0;

my $bsnym    = 1;  # change to use bio_classification_name by default.
my $debug    = 0;
my $tewl     = new JGI::QC::QCtool;
my $db       = new OraJGI;

GetOptions(
    "debug!"  => \$debug,
    "pass!"   => \$PASSTHRU,
    "brief!"  => \$brief,
    "strain!" => \$strain,
    "bcsnym!" => \$bsnym,
    "help!"   => \$help,
 ) or pod2usage(2);
pod2usage(1) if $help;

#
# MAIN:
#

my @idTmp = ();

#
# read from argv or stdin,
#

@idTmp = $tewl->getInput;

pod2usage(2) unless @idTmp;
#
# if libraries are supplied return libraries 
# but if any non-[A-Za-z] supplied try and extract
# project ids as by default this is a projid based tool...
#
#

my $process_as_libs = is_libs_only(\@idTmp);

my @ids =  $process_as_libs ? @idTmp : $tewl->filterProjId( @idTmp );

if ( $debug ) {
    print "Proccessing : ", join("\t", @ids), "\n";
}

sub is_libs_only { 
    my ( $ref ) = @_;

    my $libs_only = 1;

    for my $to_test ( @$ref ) {
        if ( $debug ) { 
	     print "CHECkING if lib $to_test\n";
	
	}
        if ( ! ( $to_test =~ /^[A-Za-z]+$/ ) ) {
	    if ( $debug ){
	        print "NOT a lib\n";
            }
	    $libs_only = 0;
	    last;
	}
    }
    return( $libs_only ) ;
}

die "No data: $!" unless @ids;

# set columns , hopefully db will decide where the info will be / stay

my $maincolumn = $bsnym ? 'bio_classification_name' : 'genus_species';

#if ($strain) { $strain = ', c.strain ' }

if ($strain) { $strain = ', strain ' }

if (!defined($strain)) {
    $strain = "";
}

$db->login;

# new sql 070416
my $sql_proj_id = <<EOSQL;
SELECT distinct(source_dna_number),
       $maincolumn $strain
FROM   projects.dt_gpts_dna_samples
WHERE  source_dna_number = :1
EOSQL

my $sql_lib_name = <<EOSQL;
SELECT distinct(source_dna_number), 
       $maincolumn $strain
FROM   libraries2.dt_pts_libraries
WHERE  library_name = :1
EOSQL

my $sql = $process_as_libs ? $sql_lib_name : $sql_proj_id;

if ( $debug ) {
    print "SQL:\n$sql\n";
}
#SELECT 	 c.$maincolumn $strain
#FROM 	 projects.seq_project a, 
#	 projects.source_dna b, 
#	 projects.bio_class_genus_species c
#WHERE 	 a.source_dna_id = b.source_dna_id
#  AND 	 b.bio_classification_id = c.bio_classification_id
#  AND 	 a.project_number = :1
#ORDER BY genus_species
#ENDOFSQL

$db->open($sql);
foreach my $id (@ids) {
    next if ( $id =~ /^\s*$/ );

    my $is_id_in_db = 0;

    $db->bind($id);
    while ( my (@gs) = $db->fetch ) {
        $is_id_in_db++;
        if ($brief) {
	    shift @gs;
            print join(' ', @gs );
        }
        else {
            print join(' ', @gs), "\n";
        }
    }
    print "$id NULL\n" unless ($is_id_in_db);
}
$db->close;
$db->logoff;

__END__


=head1 NAME

proj2organism.pl 

=head1 SYNOPSIS

  USAGE:
  proj2organsim 2662193 

  Options:
  --pass        Pass incoming data to output and append organism
  --brief       Print just the organism
  --bcsnym	Print whatevers in bio_classification_name, whatever that is (recommended)
  --strain	Print strain info as well (what's in strain column of db)

=head1 DESCRIPTION

 proj2organsim - return the organism name for one or more projects. 

=head1 COPYRIGHT

Copyright (C) 2003 Univ. of California. All rights reserved.

=head1 AUTHOR

Alex Copeland (accopeland@lbl.gov)

=cut
