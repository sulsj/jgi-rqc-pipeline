#!/usr/bin/env perl

use strict;
use warnings;

use File::Copy;
use File::Basename;
use Cwd qw(realpath);
use Getopt::Long;

use FindBin '$RealBin';
use lib "$RealBin/../lib";

use JGI_Commands;
use JGI_Constants;
use JGI_Job_Set;
use JGI_Log;
use JGI_QC_Utility;
use JGI_Seq_Unit;
use JGI_Seq_Unit_Manager;
use JGI_Utility;

my ($start_time, $total_time);

$start_time = [Time::HiRes::gettimeofday()];
my $previous_umask = umask(QC_UMASK);

my $seq_unit_name;

GetOptions(
    "seq-unit=s" => \$seq_unit_name,
    );

my $script_name = 'ill_demultiplex_files.pl';
my $script_path = $0;

my $log =  Log::Log4perl->get_logger();
my $seq_unit_manager = JGI_Seq_Unit_Manager->new();

if (!defined($seq_unit_name)) {
    $log->error("No seq unit name was given");
    exit_failure($seq_unit_manager);
}

my @seq_unit_names = ();
push(@seq_unit_names, $seq_unit_name);

my $retrieval_params = {
    (JGI_SEQ_UNIT_MANAGER_NAME) => \@seq_unit_names,
};

if ($seq_unit_manager->retrieve_data($retrieval_params) != JGI_SUCCESS) {
    $log->error("Unable to retrieve data using JGI_Seq_Unit_Manager object!");
    exit_failure($seq_unit_manager);
}

my $seq_unit = $seq_unit_manager->get_seq_unit((JGI_SEQ_UNIT_MANAGER_NAME), $seq_unit_name);

if (!$seq_unit) {
    if ($seq_unit_manager->retrieve_external_data(\@seq_unit_names, (RQC_PLATFORM_ILLUMINA)) != JGI_SUCCESS) {
	$log->error("Unable to retrieve external data using JGI_Seq_Unit_Manager object!");
	exit_failure($seq_unit_manager);
    }

    $seq_unit = $seq_unit_manager->get_seq_unit((JGI_SEQ_UNIT_MANAGER_NAME), $seq_unit_name);

    if (!$seq_unit) {
	$log->error("Data retrieval on $seq_unit_name was not complete, since \$seq_unit was not initialized properly");
	exit_failure($seq_unit_manager);
    }
}

if (initialize_seq_unit($seq_unit, $log) != JGI_SUCCESS) {
    exit_failure($seq_unit_manager);
}

my $is_multiplexed = TRUE;
my %index_sequence_to_library_map;

if (illumina_initialize_index_sequence_to_library_map
    ($seq_unit, \%index_sequence_to_library_map, \$is_multiplexed, $log) != JGI_SUCCESS) {

    if ($is_multiplexed == TRUE) {
	$log->error("Index Sequence to Library Map data structure initialization encountered errors");
	exit_failure($seq_unit_manager);
    } else {
	if ($seq_unit->set_data( (JGI_SEQ_UNIT_IS_MULTIPLEX), (FALSE) ) != JGI_SUCCESS) {
	    $log->error("Failed to set is_multiplex for seq unit ($seq_unit_name) status to FALSE");
	    return JGI_FAILURE;
	}

	if (finalize_seq_unit($seq_unit, $log) != JGI_SUCCESS) {
	    $log->error("Index Sequence to Library Map data structure initialization encountered errors");
	    exit_failure($seq_unit_manager);
	}
	
	exit_success();
    }
}

if (add_demultiplexed_libraries(\%index_sequence_to_library_map, $log) != JGI_SUCCESS) {
    exit_failure($seq_unit_manager);
}

if (add_demultiplexed_seq_units($seq_unit, \%index_sequence_to_library_map, 
				$seq_unit_manager, $log) != JGI_SUCCESS) {
    exit_failure($seq_unit_manager);
}

if (illumina_demultiplex_files
    ($seq_unit_name, \%index_sequence_to_library_map, $log) != JGI_SUCCESS) {
    $log->error("File demultiplexing encountered errors");
    exit_failure($seq_unit_manager);
}

# Sync seq unit manager only after the files have been demultiplexed.
if ($seq_unit_manager->sync_data() != JGI_SUCCESS) {
    $log->error("Seq unit manager database sync'ing encountered errors");
    exit_failure($seq_unit_manager);
}

if (finalize_seq_unit($seq_unit, $log) != JGI_SUCCESS) {
    exit_failure($seq_unit_manager);
}

exit_success();


#                        
# Title    : exit_failure
# Function : This function provides a standard way of failing out of the script.
# Usage    : exit_failure($seq_unit_manager)
# Args     : None; relies on all variables defined in the main body of
#            the script being accessible.
# Returns  : Nothing; exits with JGI_FAILURE.                                                                               
# Comments : None.
#

sub exit_failure
{
    my ($seq_unit_manager) = @_;

#    if ($seq_unit_manager->sync_data() != JGI_SUCCESS) {
#	$log->error("Seq unit manager database sync'ing encountered errors");
#    }

    #
    # Change back to the original umask.
    if (defined($previous_umask))
    {
	umask($previous_umask);
    }

    #                                                                                                                        
    # Record the total wallclock time.                                                                                       
    #                                                                                                                        
    $total_time = Time::HiRes::tv_interval($start_time);
    $log->info("Unable to update the sequence units! ($total_time " .
               "seconds)");

    #                                                                                                                        
    # Exit with failure.                                                                                                     
    #
    exit(JGI_FAILURE);
} # End of the exit_failure function.


#
# Title    : exit_success
# Function : This function encapsulates everything that needs to be done
#            before exiting the script with success.
# Usage    : exit_success ( )
# Args     : None; relies on all variables defined in the main body of
#            the script being accessible.
# Returns  : Nothing; exits with JGI_SUCCESS
# Comments : None.
#
sub exit_success
{
    #
    # Change back to the original umask.
    #
    if (defined($previous_umask))
    {
	umask($previous_umask);
    }

    #
    # Record the total wallclock time.
    #
    $total_time = Time::HiRes::tv_interval($start_time);
    $log->info("Successfully updated the sequence units. ($total_time " .
	       "seconds)");
    
    #
    # Exit with success.
    #
    exit(JGI_SUCCESS);
} # End of the exit_success function.


sub initialize_seq_unit {
    my ($seq_unit, $log) = @_;

    my $seq_unit_name = $seq_unit->get_data((JGI_SEQ_UNIT_NAME));

    if ($seq_unit->set_data( (JGI_SEQ_UNIT_RQC_STATUS_NAME), 
				  (JGI_SEQ_UNIT_STATUS_ON_HOLD) ) != JGI_SUCCESS) {
	$log->error("Failed to set multiplexed seq unit ($seq_unit_name) status to " .
		    (JGI_SEQ_UNIT_STATUS_ON_HOLD));
	return JGI_FAILURE;
    }

    if ($seq_unit->set_data( (JGI_SEQ_UNIT_IS_MULTIPLEX), (TRUE) ) != JGI_SUCCESS) {
	$log->error("Failed to set is_multiplex for seq unit ($seq_unit_name) status to TRUE");
	return JGI_FAILURE;
    }	    

    if ($seq_unit->sync_data() != JGI_SUCCESS) {
	$log->error("Multiplexed seq unit database sync'ing encountered errors");
	return JGI_FAILURE;
    }

    return JGI_SUCCESS;
}


sub add_demultiplexed_libraries {
    my ($ref_index_sequence_to_library_map, $log) = @_;

    my @demultiplexed_libraries;

    my $lib_info = JGI_Lib_Info->new();

    if (illumina_collect_individual_libraries
	($ref_index_sequence_to_library_map, \@demultiplexed_libraries, $log) != JGI_SUCCESS) {
	$log->error("Failed to collect individual library information for " .
		    "the multiplexed seq unit ($seq_unit_name)");
	return JGI_FAILURE;
    }

    my $num_libs = scalar @demultiplexed_libraries;

    if ($lib_info->update_add_library_set(\@demultiplexed_libraries) != JGI_SUCCESS) {
	$log->error("Failed to update and add information for individual libraries");
	return JGI_FAILURE;
    }
    
    if (illumina_sync_lib_info
	($lib_info, $ref_index_sequence_to_library_map, $log) != JGI_SUCCESS) {
	$log->error("Library info database sync'ing encountered errors");
	return JGI_FAILURE;
    }

    return JGI_SUCCESS;
}


sub add_demultiplexed_seq_units {
    my ($seq_unit, $ref_index_sequence_to_library_map, 
	$seq_unit_manager, $log) = @_;

    my @demultiplexed_seq_units;
    
    if (illumina_collect_individual_seq_units
	($seq_unit, $ref_index_sequence_to_library_map, 
	 \@demultiplexed_seq_units, $log) != JGI_SUCCESS) {
	$log->error("Failed to generate individual seq unit information for ".
		    "the demultiplexed seq units");
	return JGI_FAILURE;
    }
    
    if (illumina_update_and_add_seq_units($seq_unit_manager, \@demultiplexed_seq_units, 
					  $log) != JGI_SUCCESS) {
	$log->error("Failed to update and add information for individual seq units");
	return JGI_FAILURE;
    }

    return JGI_SUCCESS;
}


sub finalize_seq_unit
{
    my ($seq_unit, $log) = @_;

    my $seq_unit_name = $seq_unit->get_data( (JGI_SEQ_UNIT_NAME) );

    if (!defined($seq_unit_name)) {
	$log->error("Failed access seq unit name!");
	return JGI_FAILURE;
    }

    if ($seq_unit->set_data( (JGI_SEQ_UNIT_RQC_STATUS_NAME), 
			     (JGI_SEQ_UNIT_STATUS_REGISTERED) ) != JGI_SUCCESS) {
	$log->error("Failed to set multiplexed seq unit ($seq_unit_name) status to " .
		    (JGI_SEQ_UNIT_STATUS_REGISTERED));
	return JGI_FAILURE;	
    }

    if ($seq_unit->sync_data() != JGI_SUCCESS) {
	$log->error("Multiplexed seq unit database sync'ing encountered errors");
	return JGI_FAILURE;	
    }

    if ($seq_unit->retrieve_data
	( (JGI_SEQ_UNIT_NAME), $seq_unit_name ) != JGI_SUCCESS) {
	$log->error("Failed to retrieve fresh seq unit data from database");
	return JGI_FAILURE;
    }

    my $rqc_status = $seq_unit->get_data( (JGI_SEQ_UNIT_RQC_STATUS_NAME) );

    if ($rqc_status ne (JGI_SEQ_UNIT_STATUS_REGISTERED) ) {
	$log->error("Failed to set multiplexed seq unit ($seq_unit_name) status to " .
		    (JGI_SEQ_UNIT_STATUS_REGISTERED));
	return JGI_FAILURE;	
    }

    return JGI_SUCCESS;
}
