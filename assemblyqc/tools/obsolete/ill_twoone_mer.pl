#!/usr/bin/env perl

##################################################################################
# Name: Illumina_QC_processes.pl
# Function: perform Illumina qc for a given Illumina fastq file
# Description: For a given fastq file, a set of predefined analysis are performed, for example
#    quality trimming, gc content, dupication analysis, average quality per cycle, 21 mer plot, etc.
#   Normally the fastq file of Illumina is compressed in storage. To reduce the multiple uncompression, 
#   all these analsyses are performed simultaniously. But this increases the complexity of the programming.
#  To reduce the complexity and increase the management of the software, we define for inital procedure,
#   and process procedure, and finalize and report procedure for each tool. For example, gc analysis can have 
#  three procedure, init_gc, gc_analysis, and gc_report. The main problem mainly processes
#   the sequence file and calls the defferent tool.
#
# Author: Mingkun Li
# Date:  March 23, 10.
#####################################################################################    

use strict;
use warnings;

use Getopt::Std;
# declare the sub
sub get_a_read;
sub gc_analysis;
sub gc_report;
sub qual_analysis;
sub qual_report;
sub duplicate_analysis;
sub duplicate_report;

# command line parameter
my ($seqFile, $sampleRate,  $binSize, $reportFreq);
# read information
my ($readname, $readnumber, $readseq, $readqual);
my (%totalReads, %processedReads);
my ($bProcess, $prevReadname);
# GC content variables
my (%gccnt, %atcnt, %ncnt,  %gchist);
# seq quality analysis variables
my @e_tbl ;
for (0..128) {
    $e_tbl[$_] = 1.0 - 10**( -($_-64)/10.0);
}
my %qualable = ();
my %avgQual;
my $trueP = .50;
my $merSize = 21;
my %truePerRead = ();
my %totalMers;
my %totalExpTrue;
my (%totalBases, %totalQ20, %totalQ30); 
# duplication variables
my $duplMerSize = 20;
my %uniqueRandMers;
my %nUniqueRand;
my %uniqueStartMers;
my %nUniqueStarts;
my %prevUniqStartMers;
my %prevUniqRandMers;

# get input parameters
my %opts = ();
my $validLine = getopts('s:R:b:e:h', \%opts);

#print $validLine, "\n", $opts{s}, "\t",  $opts{R},  "\t", $opts{h}, "\n";


if ($opts{h}) {
    print "Usage: ./Illumina_QC_processes.pl -s fastqFile -h help\n";
    exit;
}
# fastq file name or stand input
if ( $opts{s}) {
	$seqFile = $opts{"s"};
} else {
	$seqFile = '-';
}
# sample rate
if (exists($opts{"R"})) {
    $sampleRate = $opts{"R"};
}else {
	$sampleRate = 1;
}
# gc bin size, should be the number of cycle of the run
if (exists($opts{"b"})) {
	$binSize = $opts{"b"};
} else {
	$binSize = 20;
}

if (exists($opts{"e"})) {
	$reportFreq = $opts{"e"};
} else {
	$reportFreq = 25000;
}
#  test options
#print "fastq: $seqFile \t Sample rate: $sampleRate \t binsize $binSize \n";
# initialize the tools


# read the sequence file and perform analysis
open FFAQ, $seqFile;

#$totalReads = 0;
$prevReadname = "";
while (($readname, $readnumber, $readseq, $readqual) = get_a_read) {
 # 		print $readname, "\t $readnumber \n", 
 # 		      $readseq, "\n", print $readqual, "\n";
    # count total reads.
 	$totalReads{$readnumber}++;
 	# sampling
 	if ($readnumber <= 1 && rand() < $sampleRate) {
 		$bProcess = 1;
 		$prevReadname = $readname;
 	} elsif ( $readnumber > 1 && $readname eq $prevReadname  ) {
 		$bProcess = 1;
 	} else {
 		$bProcess = 0;
 	}
 	# process sampled reads
 	if ($bProcess) {
 		# processed reads count
 		$processedReads{$readnumber}++;
  		#print $readname, "\t $readnumber \n", 
  		#      $readseq, "\n",  $readqual, "\n";		
  		# gc analysis
  	#	gc_analysis($readname, $readnumber, $readseq);  
  		# quality analysis
  		qual_analysis($readname, $readnumber, $readqual );    
 	}
 	# duplication analysis
 	#duplicate_analysis($readname, $readnumber, $readseq); 
 	
}
close(FFAQ);

# post process and report the results.
# duplicate report.
#duplicate_report;
# gc_report
#gc_report;
# quality report
qual_report;

exit(0);

#####################################################################################################
# Sub: get_a_read
# Function: read a read from a fastq file, return read name, read number, read base sequence, and read
#           quality sequence. The file should be opened through the file handle <FFAQ>
# Description: the subroutine should be called sequentially. So that the start position should be a new read 
#         starting with "@". The subroutine would return null if it reaches the end of the file.
# Author: Mingkun Li
# Date:   March 24, 2010
########################################################################################################
sub get_a_read {
	my ($readname, $readnumber, $readseq, $readqual);
	my $line;
	
   # head line for a read
   while ( $line = <FFAQ>) {
   	  chomp $line;
   	  if ($line =~ /^@/) {
   	  	last;
   	  }
   }
   # end of file, return
   if ( ! defined($line) ) {
   	  return;
   }
#   $line =~ /^@([\d|:]+)\/?(\d)?/;
   $line =~ /^@([\w|:]+)#?[^\/ ]*\/?(\d)?/;
   $readname = $1;
   if (defined $2) {
        $readnumber = $2;
   }else{
        $readnumber = 0;
   }
   #  sequence line
   $line = <FFAQ>;
   chomp $line;
   $readseq = $line;
   # + line
   $line = <FFAQ>;
  # qualitiy value line
  $line = <FFAQ>;
  chomp $line;
  $readqual = $line;
 
  return ($readname, $readnumber, $readseq, $readqual); 
	
}

#####################################################################################################
# Sub: gc_analysis
# Function: Given a read, count gc, at, and n bases, these values are saved in the global variables %gccnt,
#         %atcnt, %ncnt. The function also calculates the gc histogram uisng the global variable $binSize.
#        The resulted histogram is saved in the global variable %gchist.   
# 
#        The hash key is the read number. The first and second read of paired reads are counted separately.
#  
# Description: The main program should declare the global variables %gccnt, %atcnt, %ncnt, %gchist, and initialize
#        the number of bin size variable  $binSize.
#        The subroutine should be called sequentially for each read in sequence file.
#        The gc_report subroutine need be called after all the reads are processed to report the gc information
# Author: Mingkun Li
# Date:   March 25, 2010
########################################################################################################
sub gc_analysis {
	my ($readname, $readnumber, $readseq) = @_;
	my ($gcrd, $atrd, $nrd, $gcPctRd);
	
	# gc count
	 $gcrd = ( $readseq =~ tr/GC/gc/);
     $gccnt{$readnumber} += $gcrd;
     
     # at count
     $atrd = ($readseq =~ tr/AT/at/);
     $atcnt{$readnumber} += $atrd;
     # n count
     $nrd = ( $readseq =~ tr/N/n/);
     $ncnt{$readnumber} += $nrd;
      
      $gcPctRd =  $gcrd + $atrd;
      if ( $gcPctRd > 20) {
           $gcPctRd = int($binSize*$gcrd / $gcPctRd);   
	       $gchist{$readnumber}[$gcPctRd]++;
      }
}

#####################################################################################################
# Sub: gc_report
# Function: summarize and report the gc content and histogram calculate in the gc_analysis.
#        The count of gc, at, and n bases are saved in the global variables %gccnt, %atcnt, %ncnt. 
#        The histogram is saved in the global variable %gchist. The gc histogram bin size 
#           is saved in   uisng the global variable $binSize.
#        The hash key is the read number. The first and second read of paired reads are counted separately.
#  
# Description: The main program should declare the global variables %gccnt, %atcnt, %ncnt, %gchist, and initialize
#        the number of bin size variable  $binSize.
#        The subroutine gc_analysis should be called sequentially for each read in sequence file.
#        The gc_report subroutine need be called after all the reads are processed to report the gc information
# Author: Mingkun Li
# Date:   March 25, 2010
########################################################################################################
sub gc_report {
	my ($rn, $gc, $np);
   # print out avg gc content for each read
   foreach $rn (sort keys %atcnt) {
  	  $gc = $gccnt{$rn}/($gccnt{$rn}+$atcnt{$rn});
  	  $np = $ncnt{$rn}/($gccnt{$rn} + $atcnt{$rn} + $ncnt{$rn});
      #print "#Read $rn \n";
#	  print "# GC count: $gccnt{$rn}, AT count: $atcnt{$rn}, N count: $ncnt{$rn} \n";
#	  print "#GC rate: $gc \n",
#          "# No call rate: $np \n";
	}
# print out gc histogram
# gc plot
	my ($ntotalReads, $i, $gcPct);
	foreach $rn ( sort keys %gchist ) {
    	$ntotalReads = 0;
		for ($i=0; $i<=$binSize; $i++) {
			if (defined $gchist{$rn}[$i]) {
  				$ntotalReads += $gchist{$rn}[$i];
			}
		}
		#print "#total Reads in GC plot of read $rn: $ntotalReads\n";
		#print "#GC\tNum_Reads\tFrequency\n";
		for ($i=0; $i<=$binSize;$i++) {
			if (defined $gchist{$rn}) {
   				$gcPct = sprintf("%.4f", $gchist{$rn}[$i]/$ntotalReads);
   				#print $i/$binSize, "\t$gchist{$rn}[$i]\t$gcPct\n";
			}
		}
		#print "\n\n";
	}
}
######################################################################################################
# Sub qual_analysis
# Function: perform quality analysis for a given Solexa quality sequence. Currently, 21mer plot, 
#           average quality per cycle, and total bases, q20 and q30 bases are counted.
# Description: The subroutine should be called sequentially for read qual from main program. It save its
#        results in global variables %qualable, %truePerRead, %totalExpTrue, %avgQual, %totalBases,
#        %totalQ20, and %Q30. The hash key is read number.
#      For 21mer plot,  
#
# Author: Mingkun Li
# Date: March 25, 2010
#######################################################################################################
sub qual_analysis {
	my ($readname, $readnumber, $quals) = @_;
    my ($i, $loc);
    my @q = split(//,$quals);
    my $n = scalar(@q);
    my @e = map { $e_tbl[ord($_)] } @q ;
    my $p = 1.0;
    my $expTrue = 0;

    for ($i = 0; $i < $n; $i++) {
          $loc = $i-$merSize+1;
	      $p *= $e[$i];
	      if ($loc > 0 && $e[$loc-1] != 0) {
	          $p *= 1.0 / $e[$loc-1];
	      }
	      if ($loc > -1) {
	         $qualable{$readnumber}{$loc} += $p;
	          $totalMers{$readnumber}++;
	          if ($p > $trueP) {
	           $expTrue++;
	         }
	      }
	      # average error rate for each position
	      $avgQual{$readnumber}[$i] += $e[$i];
	      # count q20 and q30 bases
	      $totalBases{$readnumber}++;
	      if ( $e[$i] >= 0.99 ) {
	      	$totalQ20{$readnumber}++;
	      }
	      if ( $e[$i] >= 0.999 ) {
	      	$totalQ30{$readnumber}++;
	      }	      
	 }
     $truePerRead{$readnumber}{$expTrue}++;
     $totalExpTrue{$readnumber} += $expTrue;	
} 
######################################################################################################
# Sub qual_report
# Function: aggregate and report quality analysis results after qual_analysis. Currently, 21mer plot, 
#           average quality per cycle, and total bases, q20 and q30 bases are counted.
# Description: The subroutine should be called after quality analysis is performed. 
#        It uses results in global variables %qualable, %truePerRead, %totalExpTrue, %avgQual, %totalBases,
#        %totalQ20, and %Q30. The hash key is read number.
#      
# Author: Mingkun Li
# Date: March 25, 2010
#######################################################################################################
sub qual_report {
	my $rn;
	my ($i, $j, $temp);
	# print out avg q20 and q30.
	foreach $rn (sort keys %totalBases) {
		#print "#read $rn, total bases: $totalBases{$rn} total q20 bases: $totalQ20{$rn} total Q30 bases: $totalQ30{$rn}\n";
		# Q20 percentage
		if (exists $totalQ20{$rn}) {
		   $temp = sprintf("%.4f", $totalQ20{$rn}/$totalBases{$rn});	
		} else{
			$temp = "0";
		}
		#print "Q20 base percentage: $temp\n";
		# Q30 percentage
		if (exists $totalQ30{$rn}) {
		   $temp = sprintf("%.4f", $totalQ30{$rn}/$totalBases{$rn});	
		} else{
			$temp = "0";
		}
		#print "Q30 base percentage: $temp\n";		
	}

	# print out average quality value.
	#print "#Average accuracy along each base position \n";
	$j = 0;
	my ($posAvg, $overallAvg, $pqa, $qa);
	foreach $rn ( sort keys %avgQual) {
	#	print "#read $rn \n";
		$overallAvg = 0;
    	for ( $i = 0; $i <= $#{$avgQual{$rn}}; $i++) {
    		 $posAvg = $avgQual{$rn}[$i] /$processedReads{$rn};
      	 	$pqa = sprintf("%.4f", $posAvg);
        #  	print $i + $j, "\t$pqa\t", $processedReads{$rn}, "\n";
          
           $overallAvg += $posAvg;
    	}
    	$j = $j + $i ;   
    
   		$pqa = sprintf("%.4f", $overallAvg/($#{$avgQual{$rn}}+1) );
    	#print "#Avereage accuracy for read $rn is $pqa\n\n";
	}

# plot out correct 21 mer along read location.

# use 2 \n so we can use gnuplot index command to plot separately
	my %eTrue;
	#print "#Expected number of correct $merSize-mers by coordinate:\n";
	$j = 0;
	foreach $rn ( sort keys %qualable) {
     	my @q = sort {$a <=> $b} keys(%{$qualable{$rn}});
        $eTrue{$rn} = 0;
        for ( $i = $q[0]; $i <= $q[-1]; $i++) {
           $qa = int($qualable{$rn}{$i}) ;
           $pqa = 100 * $qa /$processedReads{$rn} ;
            #print "$i\t$qualable{$i}\t$nReads\n";
	    my $readpos = $i + $j + 1;
           #print $i + $j, "\t$qa\t", $processedReads{$rn}, "\t$pqa\n";
           print $readpos, "\t$qa\t", $processedReads{$rn}, "\t$pqa\n";
	   $eTrue{$rn} += $qualable{$rn}{$i};
        }
       $j = $j + $i + $merSize - 1;
   	print "\n"; 
    }
# print overall correct 21mers.
	foreach $rn (sort keys %eTrue) {
    	my $fTrue = sprintf("%.2f",100*$eTrue{$rn}/$totalMers{$rn});
    	#print "# $fTrue\% of $merSize-mers are expected to be correct for read $rn \n";

    	my $dTrue = sprintf("%.2f",100*$totalExpTrue{$rn}/$totalMers{$rn});
     	#print "# $dTrue\% of $merSize-mers have Qtrue > 0.5 for read $rn\n";
	}

# print out correct 21mer histogram 
	#print "\n#Number of QTrue > 0.5 $merSize-mers per read:\n";

	foreach $rn (sort keys %truePerRead) {
	#	print "# read $rn \n";
    	my @t = sort {$b <=> $a} keys(%{ $truePerRead{$rn}});

     	my $cumulReads = 0;

     	for ( $i = $t[0]; $i >= $t[-1]; $i--) {
        	if (exists($truePerRead{$rn}{$i})) {
           		$cumulReads += $truePerRead{$rn}{$i};
           		my $fReads = sprintf("%.2f",$cumulReads/$processedReads{$rn});
         #  		print "$i\t", $truePerRead{$rn}{$i}, "\t$fReads\n" ;
     		} else {
        #		print "$i\t0\n";
     		}
   		}
   	#	print "\n\n"
	}	
}

######################################################################################################
# Sub: duplicate_analysis
# Description: this function is copied from  Jarrod Chapman's script.
#    It calculates unique a start k-mer and a random k-mer for each reads and outputs the unique
#     start k-mers and random k-mers every M reads.
#   The script uses the global variable %uniqueStartMers, %uniqueRandMers, %nUniqueStarts, %nUniqueRands, 
#   where the key of the hashes are read number. The scripts also uses the global variable $DuplMerszie and 
#     for the calculation. The main program should declare these variables.
#
#   The sub should be called sequentially to process each read and duplicate_report should be called later to 
#    report the dulication stat.
#
# Date: March 26, 2110
##############################################################################################################  
sub duplicate_analysis {
	my ($readname, $readnumber, $readseq) = @_;
	my $seqLen = length($readseq);
	#
	unless ($seqLen > $duplMerSize) {
		return;
    }
    # unique start mers
    my $pos = 0;
    my $mer = substr($readseq,$pos,$duplMerSize);
    
    if (exists($uniqueStartMers{$readnumber}{$mer})) {
		$uniqueStartMers{$readnumber}{$mer}++;
    } else {
		$uniqueStartMers{$readnumber}{$mer} = 1;
		$nUniqueStarts{$readnumber}++;
    }
    # random mers
    $pos = int(rand($seqLen-$duplMerSize));
    $mer = substr($readseq,$pos,$duplMerSize);
    
    if (exists($uniqueRandMers{$readnumber}{$mer})) {
		$uniqueRandMers{$readnumber}{$mer}++;
    } else {
		$uniqueRandMers{$readnumber}{$mer} = 1;
		$nUniqueRand{$readnumber}++;
    }	

    # write out the unique mers if in report frequency.
    my ($pU, $pUS);
    if ($totalReads{$readnumber}%$reportFreq == 0) {
    	if (defined $prevUniqRandMers{$readnumber} ) {
		  $pU = ($nUniqueRand{$readnumber} - $prevUniqRandMers{$readnumber})/$reportFreq;
    	} else {
    		$pU = $nUniqueRand{$readnumber} /$reportFreq; # first time sampling
    	}
		$prevUniqRandMers{$readnumber} = $nUniqueRand{$readnumber};
		
		$pU = sprintf("%.4f", $pU);
		
		if (defined $prevUniqStartMers{$readnumber}) {
			$pUS = ($nUniqueStarts{$readnumber} - $prevUniqStartMers{$readnumber})/$reportFreq;
		}else {
			$pUS = $nUniqueStarts{$readnumber}/$reportFreq;
		}
		$prevUniqStartMers{$readnumber} = $nUniqueStarts{$readnumber};
		
		$pUS = sprintf("%.4f", $pUS);

		print "$totalReads{$readnumber} \t $readnumber \t $nUniqueStarts{$readnumber} \t $pUS ",
		      "\t $nUniqueRand{$readnumber} \t$pU\n";
    }
	
}

######################################################################################################
# Sub: duplicate_report
# Description: the sub calculates the duplicated rate resulted from duplicate_analysis.
#   The script uses the global variable %uniqueStartMers, %uniqueRandMers, %nUniqueStarts, %nUniqueRands, 
#   where the key of the hashes are read number. 
#
#
# Date: March 26, 2110
##############################################################################################################  
sub duplicate_report {
	my ($pU, $pUS);
	my $readnumber;
	
	foreach $readnumber (keys %nUniqueStarts) {
	    $pU = 	sprintf("%.4f", $nUniqueRand{$readnumber}/$totalReads{$readnumber});
	    $pUS = sprintf("%.4f", $nUniqueStarts{$readnumber}/$totalReads{$readnumber} );
	    
	    print "# Read $readnumber, Unique Start $duplMerSize - Mer: $pUS, Unique Random  $duplMerSize - Mer: $pU\n";
		
	}
	print "\n\n";
}
