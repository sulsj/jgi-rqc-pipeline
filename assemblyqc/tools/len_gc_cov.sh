#!/bin/bash
# create a gnuplot for contig length vs GC % vs Aligned Read Converage.
# input is the gc_cov.py produced gccovstats.txt (with --mode fungal option)
# usage : len_gc_cov.sh gccovstats.txt OUT.png LIBNAME
# Shijie Yao
# 7/18/2018
#

module purge; module load gnuplot

echo "set terminal png
set output '$2'
set title '$3 Contig Length vs GC % vs Aligned Reads Coverage'
set xlabel 'Contig Length (bp)'
set ylabel 'GC %'
set y2label 'Coverage'
set cbrange[0:200]
plot '$1' u 3:4:2 w points lt 1 pt 20 lc palette t 'contig';
" | gnuplot