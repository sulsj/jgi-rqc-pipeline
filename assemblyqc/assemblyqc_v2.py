#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
    2nd Generation RQC Assembly pipeline.
    Use an assembler based on project types (RQC-1075)
    [subsample] -> [error correctoin] -> assembly -> sketch -> report data generation -> done!

    Depends on:
        assemblyqc_constants
        assemblyqc_utils
        common
        matplotlib
          \-pyplot
        numpy
        os_utility
        rqc_constants
        rqc_fastq
        * assembler executables

    20180507 6.0.0: SY - use different assemblers for different project types (RQC-1075)
    1) MegaHit for metagenome / metatranscriptome (flags are in rqc_setup.py)
        metagenome pipeline: metagenome_flag = 1
            seq_prod_name in ('metagenome minimal draft', 'metagenome improved draft', 'metagenome standard draft')
            seq_prod_name in ('metagenome viral improved draft', 'metagenome viral standard draft')
            if str(rs['final_deliverable_prod_name']).lower().startswith('cell enrichment, unamplified')

        metatranscriptome : metatranscriptome_flag = 1
            seq_prod_name in ('metagenome metatranscriptome', 'eukaryote community metatranscriptome'):
    2) Spades for microbial projects:
        sag : sag_asm_flag = 1
            seq_prod_name in ('microbial minimal draft, single cell', 'microbial improved draft, single cell')
        iso : microbe_isolate_flag = 1 ?
        ce : microbe_cell_enrichment_flag = 1:
            seq_prod_name == 'metagenome viral minimal draft'
            str(rs['final_deliverable_prod_name']).lower().startswith('cell enrichment, amplified')
        sps : microbe_sps_flag = 1
            seq_prod_name == 'single particle sort, 16s negative':
    3) Tadpole for the rest:



    Update started: May, 21 2018
    Shijie Yao (syao@lbl.gov)

Revision:


'''


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use
from __future__ import print_function
import os
import sys
import argparse
import re
import time
import shutil
import platform
import matplotlib
import numpy as np
matplotlib.use('Agg') ## to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
import mpld3

# custom libs in '../lib/'
SRCDIR = os.path.dirname(__file__)
LIBDIR = os.path.join(SRCDIR, 'lib')
TOOLDIR = os.path.join(SRCDIR, '../tools')

sys.path.append(LIBDIR)       ## rqc-pipeline/assemblyqc/lib
sys.path.append(os.path.join(SRCDIR, '../lib'))    ## rqc-pipeline/lib
sys.path.append(TOOLDIR)  ## rqc-pipeline/tools
sys.path.append(os.path.join(SRCDIR, os.path.pardir, 'sag'))

from assemblyqc_utils import checkpoint_step, calculate_line_total, get_cat_cmd
from assemblyqc_constants import get_tool_path, AssemblyStats
from rqc_fastq import get_working_read_length
from common import append_rqc_stats, append_rqc_file, get_logger, get_status, run_cmd
from os_utility import make_dir
from rqc_constants import RQCReferenceDatabases
from micro_lib import make_asm_stats, save_asm_stats

VERSION = '0.5.0'

LOGGER = None
DEBUG = False

TIME_S = time.time()

ASSEMBER_OPT = 'assembler options'
INPUT_FASTQ = 'input fastq'
SUB_SAMPLE = 'sub sampled file'
ERR_CORRECTION = 'error correction file'
FOR_ASM = 'assembly input file'
ASM_COVERAGE = 'assembly converage'

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## pipeline step control constant
PIPE_START = 'pipeline start'
INIT_START = 'start init'
INIT_END = 'end init'
SUBSAMPLE_START = 'start subsample'
SUBSAMPLE_END = 'end subsample'
BFEC_START = 'start bloom-filter based error correction'
BFEC_END = 'end bloom-filter based error correction'
ASSEMBLE_START = 'start assemble'
ASSEMBLE_END = 'end assemble'
SKETCH_START = 'start sketching'
SKETCH_END = 'end sketching'
POST_ASSEMBLE_START = 'start post assemble'
POST_ASSEMBLE_END = 'end post assemble'

PIPE_COMPLETE = 'pipeline complete'

STEP_ORDER = {
    'start' : 0,
    PIPE_START : 0,
    INIT_START : 10,
    INIT_END : 20,
    SUBSAMPLE_START : 30,
    SUBSAMPLE_END : 40,
    BFEC_START : 50,
    BFEC_END : 52,
    ASSEMBLE_START : 70,
    ASSEMBLE_END : 72,
    POST_ASSEMBLE_START : 90,
    POST_ASSEMBLE_END : 100,
    SKETCH_START : 110,
    SKETCH_END : 120,
    PIPE_COMPLETE : 1000
}

## stats / file keys that used in multiple places
STATS_FILE_TAG = 'stats file'
FILES_FILE_TAG = 'files file'
CHECKPOINT_FILE_TAG = 'checkpoint file'
SKETCH_VS_NT_FILE_TAG = 'sketch vs nt output'
SKETCH_VS_REFSEQ_FILE_TAG = 'sketch vs refseq output'
SKETCH_VS_SILVA_FILE_TAG = 'sketch vs silva output'

ASM_LEVEL_GC_HIST_IMG_FILE_TAG = 'assembly level GC content hist image file'
ASM_LEVEL_GC_HIST_HTML_FILE_TAG = 'assembly level GC content hist HTML file'
ASM_LEVEL_GC_HIST_FILE_TAG = 'assembly level GC content text hist file'

PAIRED_VALUE = 'paired'
READ_LENGTH_VALUE = 'read length'
READ_LENGTH_1_VALUE = 'read1 length'
READ_LENGTH_2_VALUE = 'read2 length'

CONTIG_FILE_TAG = 'contig file'
RAW_CONTIG_FILE_TAG = 'raw contigs file'


## blast DB constants
REF_DB = {
    'nt' : RQCReferenceDatabases.NT_maskedYindexedN_BB,
    'LSSURef': RQCReferenceDatabases.LSSU_REF,
    'LSURef' : RQCReferenceDatabases.LSU_REF,
    'SSURef' : RQCReferenceDatabases.SSU_REF,
    'refseq.archaea' : RQCReferenceDatabases.REFSEQ_ARCHAEA,
    'refseq.bacteria' : RQCReferenceDatabases.REFSEQ_BACTERIA,
    'refseq.mitochondrion' : RQCReferenceDatabases.REFSEQ_MITOCHONDRION,
    'refseq.plant' : RQCReferenceDatabases.REFSEQ_PLANT,
    'refseq.plasmid' : RQCReferenceDatabases.REFSEQ_PLASMID,
    'refseq.plastid' : RQCReferenceDatabases.REFSEQ_PLASTID,
    'refseq.fungi' : RQCReferenceDatabases.REFSEQ_FUNGI,
    'refseq.viral' : RQCReferenceDatabases.REFSEQ_VIRAL,
    'green_genes16s.insa_gg16S' : RQCReferenceDatabases.GREEN_GENES,
    'JGIContaminants' : RQCReferenceDatabases.CONTAMINANTS,
    'collab16s' : RQCReferenceDatabases.COLLAB16S,
}


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions
'''
    return executable
    make it easy to use different excutables (e.g. module vs shifter)
'''
def get_exec(progname, bbtools=False):
    amap = {
        'tadpole'   : '#bbtools;tadpole.sh',
        'spades'    : '#spades/3.10.1;spades.py',
        'megahit'   : '#megahit-1.1.1;megahit',
        'velveth'    : '#biobox/velvet;velveth',
        'velvetg'    : '#biobox/velvet;velvetg',
    }

    if bbtools:
        cmd = '#bbtools;%s' % progname
    else:
        cmd = amap.get(progname)
    return cmd

def checkpoint(step, lastStep=PIPE_START):
    'update the pipeline check point log with the new step (step). lastStep is the last pipeline step from the pipeline checkpoint file'

    global TIME_S
    if step.upper().startswith('START'):
        TIME_S = time.time()
    if step.upper().startswith('END'):
        dt = time.time() - TIME_S
        LOGGER.info('  TIME ELAPSED : %.2f', dt)

    LOGGER.info(' [ %s ] ', step.upper())
    if step.upper().startswith('END'):
        LOGGER.info('\n')

    if step == PIPE_START or STEP_ORDER[step] > STEP_ORDER[lastStep]:
        # print('>>>>DEBUG step=%d; lastStep=%d' % (STEP_ORDER[step], STEP_ORDER[lastStep]))
        checkpoint_step(ASMMETA[CHECKPOINT_FILE_TAG], step)

#
def init(fastq, assembler, ksize, kmax, lastStep):
    'initialization step : gather reads info'
    #print('DEBUG init')
    status = lastStep
    readLength = None
    paired = None
    kmer = None

    ##= local helper : nearest odd kmer value that's 80% of the read length
    def read_length_to_kmer(readLen, maxKmer):
        ## take 80% of the read length, and take the nearest odd number => 2 x round( ( x -1 ) / 2 ) + 1
        readLen = readLen * 0.80
        kmer = (readLen - 1) / 2
        kmer = int(kmer + 0.5)
        kmer = (2 * kmer) + 1

        ## The max kmer length is a parameter to this function with default of 63.
        ## The max allowed kmer is 63 so set it to 63 if the returned value is greater.
        ## The max can also be user specified to any value, for example maybe the user chooses 71.
        if kmer > maxKmer:
            kmer = maxKmer
        return kmer

    ##= local helper: called in 2 places
    def initialize(fastq, assembler, kmax):
        (readLength, readLengthR1, readLengthR2, paired) = get_working_read_length(fastq, LOGGER)
        kmer = None
        if readLength > 0:
            if assembler == 'velvet':   # determine the kmer size
                if ksize:
                    kmer = int(kmer)
                else:
                    ## Maximum kmer may be used if it is specified.
                    if kmax:
                        kmax = int(kmax)
                    else:
                        kmax = 63

                    kmer = read_length_to_kmer(readLength, kmax) ## KMER_TEST
            ASMMETA[PAIRED_VALUE] = paired
            if paired:
                ASMMETA[READ_LENGTH_1_VALUE] = readLengthR1
                ASMMETA[READ_LENGTH_2_VALUE] = readLengthR2
            else:
                ASMMETA[READ_LENGTH_VALUE] = readLength
        return readLength, kmer, paired
    ##= local helper


    if STEP_ORDER[status] <= STEP_ORDER[INIT_START]:
        status = INIT_START
        checkpoint(status, lastStep)
        readLength, kmer, _ = initialize(fastq, assembler, kmax)

        if readLength == 0:
            LOGGER.info('init failure - bad read length: %d', readLength)
        else:
            status = INIT_END
            checkpoint(status, lastStep)
    else:
        readLength, kmer, paired = initialize(fastq, assembler, kmax)

    LOGGER.info('  -Read length found : %s', readLength)
    LOGGER.info('              Paired : %s', paired)

    return status, paired, readLength, kmer


def do_subsample(fastq, opath, maxReads, paired, lastStep):
    'do subsample with bbtool'
    status = lastStep

    rmax = int(maxReads)
    if paired:
        rmax /= 2
    bname = os.path.basename(fastq).replace('.gz', '').replace('.fastq', '')
    destinationFastq = os.path.join(opath, '%s.subsampled_%s.fastq' % (bname, maxReads))
    sourceFastqStats = os.path.join(opath, '%s.subsampled_%s.stats' % (bname, maxReads))

    checkpoint(SUBSAMPLE_START, lastStep)
    err = None
    if not os.path.isfile(destinationFastq):
        cmds = [
            '#bbtools;reformat.sh',
            'in=%s' % fastq,
            'out=%s' % destinationFastq,
            'samplereadstarget=%d ow=t > %s 2>&1' % (rmax, sourceFastqStats)
        ]
        cmd = ' '.join(cmds)
        LOGGER.info(' RUN CMD : %s', cmd)
        stdOut, stdErr, exitCode = run_cmd(cmd)
        if exitCode != 0:
            err = ' cmd failed : out=%s, err=%s, exitcode=%s', stdOut, stdErr, exitCode

    if not err:
        checkpoint(SUBSAMPLE_END, lastStep)
        fastq = destinationFastq
        ASMMETA[SUB_SAMPLE] = destinationFastq
        if STEP_ORDER[SUBSAMPLE_END] > STEP_ORDER[lastStep]:
            status = SUBSAMPLE_END
    else:
        LOGGER.error(err)

    return status, ASMMETA.get(SUB_SAMPLE)

def do_bloomf_err_correction(opath, fastq, lastStep):
    'do bloom-filter error correction'
    status = lastStep
    bname = os.path.basename(fastq).replace('.gz', '').replace('.fastq', '')
    ecfastq = os.path.join(opath, '%s.bfec.fastq' % bname)
    logfile = os.path.join(opath, '%s.bfec.log' % bname)

    checkpoint(BFEC_START, lastStep)

    err = None
    if not os.path.isfile(ecfastq):
        ## if subsumpled, use subsumpled seq, else, use raw input
        if SUB_SAMPLE in ASMMETA:
            infile = ASMMETA[SUB_SAMPLE]
        else:
            infile = fastq

        if os.path.isfile(infile):
            cmds = [
                '#bbtools;bbcms.sh',
                'in=%s' % infile,
                'out=%s' % ecfastq,
                'mincount=2 highcountfraction=0.6 ow=t > %s' % logfile
            ]

            cmd = ' '.join(cmds)
            LOGGER.info(' RUN CMD : %s', cmd)
            stdOut, stdErr, exitCode = run_cmd(cmd)

            if exitCode != 0:
                err = ' cmd failed : out=%s, err=%s, exitcode=%s', stdOut, stdErr, exitCode
        else:
            err = ' - failed to find input file : %s', infile

    if not err:
        checkpoint(BFEC_END, lastStep)
        ASMMETA[ERR_CORRECTION] = ecfastq
        if STEP_ORDER[BFEC_END] > STEP_ORDER[lastStep]:
            status = BFEC_END
    else:
        LOGGER.error(err)

    return status, ASMMETA.get(ERR_CORRECTION)

def asm_cmd(assembler, fastq, outputPath, paired):
    'return assembler command based on the assembler type, with full execution params'
    cmd = get_exec(assembler)
    contigs = None
    # if cmd and os.path.isfile(fastq):
    if cmd:
        asmdir = os.path.join(outputPath, assembler)
        asmopts = ASMMETA[ASSEMBER_OPT]
        if assembler == 'tadpole':
            if not os.path.isdir(asmdir):
                os.mkdir(asmdir)
            contigs = os.path.join(asmdir, 'contigs.fasta')
            cmd = '%s in=%s out=%s %s' % (cmd, fastq, contigs, asmopts)
        elif assembler == 'spades':
            contigs = os.path.join(asmdir, 'contigs.fasta')
            cmd = '%s %s -o %s' % (cmd, asmopts, asmdir)
            if paired:
                cmd += ' --12 %s' % fastq
            else:
                cmd += ' -s %s' % fastq
        elif assembler == 'megahit':
            contigs = os.path.join(asmdir, 'final.contigs.fa')
            cmd = '%s %s -o %s' % (cmd, asmopts, asmdir)
            if paired:
                cmd += ' --12 %s' % fastq
            else:
                cmd += ' --read %s' % fastq
    else:
        LOGGER.error('unsupported assembler : %s', assembler)

    if not fastq:
        cmd = None

    return cmd, contigs

'''
    return just the expected trimmed contig file path (when justname = True), or
    do contig trimming and then return the trimmed contig file path
'''
def trim_contig(contigs, libname, justname=False):
    wdir = os.path.dirname(contigs)
    trimmedContigFile = os.path.join(wdir, 'contigs.trim.fasta')

    if justname:
        return trimmedContigFile
    else:
        # trimName = os.path.join(wdir, 'contigs.trim.unnamed.fasta')
        cmd = '%s in=%s out=%s underscore minlength=1000 ow=t' % (get_exec('reformat.sh', bbtools=True), contigs, trimmedContigFile)
        LOGGER.info('RUN : %s' % cmd)
        _, stdErr, exitCode = run_cmd(cmd)

        if exitCode != 0:
            trimmedContigFile = None
            LOGGER.error(' -reformat.sh failed : %s' % stdErr)

    return trimmedContigFile

def do_assemble(assembler, fastq, paired, outputPath, libname, lastStep):
    'assemble the input reads using non-velvet assembler'

    status = lastStep
    cmd, contigs = asm_cmd(assembler, fastq, outputPath, paired)

    checkpoint(ASSEMBLE_START, lastStep)
    err = None
    if not os.path.isfile(contigs):
        if cmd:
            LOGGER.info('RUN CMD : %s', cmd)
            if assembler == 'megahit':  # megahit rerun with existing run foler can fail
                asmdir = os.path.join(outputPath, assembler)
                if os.path.isdir(asmdir):
                    LOGGER.warning(' Remove the existing megahit job dir before re-run : %s' % asmdir)
                    shutil.rmtree(asmdir)

            _, stdErr, exitCode = run_cmd(cmd)
            if exitCode == 0:
                if os.path.isfile(contigs) and os.path.getsize(contigs) > 0:
                    LOGGER.info('%s complete', assembler)
                    ASMMETA[RAW_CONTIG_FILE_TAG] = contigs
                    contigs = trim_contig(contigs, libname)
                else:
                    err = 'assembly failed to produce the contig file [%s] : %s', contigs, stdErr
            else:
                err = 'assembly failed %s', stdErr
        else:
            err = 'failed to construct assembler command for %s', assembler
    else:
        LOGGER.info('  - no need to perform assemble : have been done already\n')
        ASMMETA[RAW_CONTIG_FILE_TAG] = contigs
        contigs = trim_contig(contigs, libname, True)

    if err:
        LOGGER.error(err)
        contigs = None
    else:
        checkpoint(ASSEMBLE_END, lastStep)
        if STEP_ORDER[ASSEMBLE_END] > STEP_ORDER[lastStep]:
            status = ASSEMBLE_END

    return status, contigs


def do_velvet_assemble(kmer, fastq, outputPath, libname, lastStep):
    'assemble the input reads using velvet'
    status = lastStep
    assemDirName = 'velvet'
    assemDir = os.path.join(outputPath, assemDirName)
    gchist = os.path.join(assemDir, 'GC.contigs.fa.BBDUK.hist')
    contigs = None #os.path.join(assemDir, 'contigs.fa')

    if STEP_ORDER[status] <= STEP_ORDER[ASSEMBLE_START]:
        status = ASSEMBLE_START
        checkpoint(status, lastStep)

        if os.path.isfile(fastq):
            zcatCmd, exitCode = get_cat_cmd(fastq, LOGGER)
            if exitCode == 0:
                ## the use of pipe breaks the #bbtool notation of run_cmd; use shifter directly
                bbtools = 'shifter --image=bryce911/bbtools'
                velvet = 'shifter --image=bioboxes/velvet'
                duk = '%s bbduk.sh' % bbtools
                velveth = '%s velveth' % velvet
                cmds = [
                    '%s %s' % (zcatCmd, fastq),
                    '%s ow in=stdin.fq out=stdout.fq int=t qtrim=rl trimq=5 minlength=35 maxns=2 gchist=%s' % (duk, gchist),
                    '%s %s %d -fastq -' % (velveth, assemDir, kmer)
                ]
                cmd = ' | '.join(cmds)

                LOGGER.info('RUN velveth CMD : %s', cmd)
                _, stdErr, exitCode = run_cmd(cmd)

                if exitCode == 0:
                    if os.path.isfile(os.path.join(assemDir, 'Roadmaps')):
                        LOGGER.info('velveth complete')
                        cmd = '%s %s -cov_cutoff 1 -min_contig_lgth 100' % (get_exec('velvetg'), assemDir)
                        LOGGER.info('RUN velvetg CMD : %s', cmd)
                        _, stdErr, exitCode = run_cmd(cmd)
                        if exitCode == 0:
                            if os.path.isfile(os.path.join(assemDir, 'contigs.fa')):
                                LOGGER.info('velvetg complete')
                                contigs = os.path.join(assemDir, 'contigs.fa')
                                ASMMETA[RAW_CONTIG_FILE_TAG] = contigs
                                contigs = trim_contig(contigs, libname)
                                if contigs:
                                    status = ASSEMBLE_END
                                    checkpoint(status, lastStep)
                            else:
                                LOGGER.error('velvetg output not produced : %s', stdErr)
                        else:
                            status = 'velvetg failed'
                            LOGGER.error('velvetg failed, exit code != 0 : %s', stdErr)
                    else:
                        LOGGER.error('velveth output checking failed : %s', stdErr)
                else:
                    LOGGER.error('velveth failed, exit code != 0 : %s', stdErr)
            else:
                LOGGER.error('get_cat_cmd failure, exit code != 0')
    else:
        LOGGER.info('  - no need to perform velvet assemble : have been done already\n')
        contigs = os.path.join(assemDir, 'contigs.fa')
        ASMMETA[RAW_CONTIG_FILE_TAG] = contigs
        contigs = trim_contig(contigs, libname, True)
        if not os.path.isfile(contigs):
            LOGGER.info('  - BUT assembly file missing : \n', contigs)
            contigs = None

    return status, contigs

'''
    The rqc-files.txt and rqc-stats.txt file will be re-created and populated by this function.
'''
def do_post_process(assembler, contigs, wdir, kmer, readlength, libname, lastStep):
    'generate plots and anaysis data, and collect needed data/files into standard files for archiving'
    status = lastStep

    asmdir = os.path.join(wdir, assembler)
    postdir = os.path.join(wdir, 'post')

    ok = True
    fqlink = None
    if STEP_ORDER[lastStep] <= STEP_ORDER[POST_ASSEMBLE_START]:
        status = POST_ASSEMBLE_START
        checkpoint(status, lastStep)

        statsFile = ASMMETA[STATS_FILE_TAG]
        filesFile = ASMMETA[FILES_FILE_TAG]
        if os.path.isfile(statsFile):
            LOGGER.info(' - delete the existing stats file : %s', statsFile)
            os.remove(statsFile)
        if os.path.isfile(filesFile):
            LOGGER.info(' - delete the existing file list file : %s', filesFile)
            os.remove(filesFile)


        ##=
        if ASMMETA[PAIRED_VALUE]:
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_READ_LENGTH_1, ASMMETA[READ_LENGTH_1_VALUE])
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_READ_LENGTH_2, ASMMETA[READ_LENGTH_2_VALUE])
        else:
            append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_READ_LENGTH_1, ASMMETA[READ_LENGTH_VALUE])

        ##= local helper
        def makelink(destdir, contigs, dolink=True):
            # contig file into the uniform post/contigs.fa
            slink = os.path.join(destdir, 'contigs.fa')

            if dolink:
                if os.path.isfile(slink):
                    os.remove(slink)

                LOGGER.info(' - create symlink : %s %s', contigs, slink)
                os.symlink(contigs, slink)
            return slink

        if not os.path.isdir(postdir):
            os.mkdir(postdir)
        if not os.path.islink(contigs):
            fqlink = makelink(postdir, contigs)
        else:
            fqlink = makelink(postdir, contigs, False)

        ASMMETA[CONTIG_FILE_TAG] = fqlink

        append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_CONTIGS_FILESIZE, os.path.getsize(contigs), LOGGER)
        append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_ASSEMBLER, assembler, LOGGER)
        append_rqc_stats(statsFile, AssemblyStats.ILLUMINA_ASSEMBLY_ASSEMBLER_VERSION, assmbler_version(assembler), LOGGER)
        append_rqc_stats(statsFile, 'assembly assembler run params', ASMMETA[ASSEMBER_OPT], LOGGER)


        ok = do_sketch_vs_db(fqlink, postdir)

        ## stats on the untrimmed contig
        if ok:
            ok = raw_contig_stats(postdir)

        if ok:
            ## create %GC vs contig count plot
            assembly_gc_plot(postdir)

            ##== calculate contig summary: produce contigs.trim.* files : contigs.trim.tsv AND contigs.trim.txt -> [stats.sh output]
            LOGGER.info(' use sag/micro_lib.make_asm_stats to do contig summary')
            asm_stats, asm_tsv = make_asm_stats(contigs, 'contigs', postdir, filesFile, LOGGER)
            save_asm_stats(asm_tsv, 'contig', statsFile, LOGGER)

            if assembler == 'velvet':
                append_rqc_stats(statsFile, 'velvet %s' % AssemblyStats.ILLUMINA_ASSEMBLY_KMER, kmer, LOGGER)
                ok = velvet_assembly_summary(wdir)

            if ok:
                os.chdir(postdir)
                ## run sag/contig_gc.py to generate plots :  -f contigs.trim.fasta -l XYZAB -o ./
                ## generate gc.* | contig.gc | contig_gc.* | contig_hist.* | contig_sum.* files
                contigqccmd = 'contig_gc.py'
                cmd = '%s/../sag/%s -f %s -l %s -o ./' % (SRCDIR, contigqccmd, fqlink, libname)
                LOGGER.info('  RUN comd : %s', cmd)
                _, stdErr, exitCode = run_cmd(cmd)
                if exitCode == 0:
                    # add contig_gc.png, contig_hist.png, contig_vs_size.png to stats file list
                    append_rqc_stats(filesFile, 'contig gc histogram', os.path.join(postdir, 'contig_gc.png'))
                    append_rqc_stats(filesFile, 'contig size and gc percent', os.path.join(postdir, 'contig_hist.png'))
                    append_rqc_stats(filesFile, 'contig size vs percent of genome', os.path.join(postdir, 'genome_vs_size.png'))

                else:
                    LOGGER.error(' - %s  failed : %s' % (contigqccmd, stdErr))
                    ok = False

                ## run tools/asm2stats.pl to produce assembly quality JSON data file for report
                if ok:
                    fname = os.path.join(postdir, '%s.assembly-stats.json' % libname)
                    asmstatscmd = 'asm2stats.pl'
                    cmd = '%s/tools/%s %s > %s' % (SRCDIR, asmstatscmd, fqlink, fname)
                    LOGGER.info(' RUN %s' % cmd)
                    _, stdErr, exitCode = run_cmd(cmd)
                    if exitCode == 0:
                        append_rqc_stats(filesFile, 'assembly stats json', fname)
                    else:
                        LOGGER.error(' - %s  failed : %s' % (asmstatscmd, stdErr))
                        ok = False

        ##== create tetramer frequency analysis files. USE contigs, not scaffolds!
        if ok:
            tetramerOutputFile = os.path.join(postdir, 'contigs.kmers')
            tetramerLogFile = os.path.join(postdir, 'contigs.kmers.log')
            cmd = '%s in=%s out=%s window=2000 ow> %s 2>&1' % (get_exec('tetramerfreq.sh', bbtools=True), fqlink, tetramerOutputFile, tetramerLogFile)
            LOGGER.info(' - tetramer plot freq : %s', cmd)
            stdOut, stdErr, exitCode = run_cmd(cmd)

            if exitCode == 0 and os.path.isfile(tetramerOutputFile):
                LOGGER.info(' - tetramer plot freq complete: %s', tetramerOutputFile)
            else:
                LOGGER.error(' - tetramer plot freq error : %s', stdErr)
                ok = False
        ##==

        ##== generate the tetramer PCA plot
        if ok:
            tetramerPlotFile = os.path.join(postdir, 'assembly_tetramer')
            tetramerPlotLog = os.path.join(postdir, 'show_kmer_bin.log')
            rscriptCmd = get_tool_path('Rscript', 'R')
            showKmerBinLoc = '/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/showKmerBin.R'
            cmd = '%s --vanilla %s --input %s --output %s --jpgurl bryce --doturl brycedot --label "Principal Component Analysis" > %s' %\
                                    (rscriptCmd, showKmerBinLoc, tetramerOutputFile, tetramerPlotFile, tetramerPlotLog)
            LOGGER.info(' - tetramer plot file : %s', cmd)
            stdOut, stdErr, exitCode = run_cmd(cmd)

            tplotfile = '%s.jpg' % tetramerPlotFile
            if exitCode == 0:
                if os.path.isfile(tplotfile):
                    LOGGER.info(' - tetramer plot file generated: %s', tplotfile)
                    desc = "contig level tetramer plot"
                    append_rqc_file(filesFile, desc, tplotfile, LOGGER)
                else:
                    LOGGER.error(' - cmd run ok but failed to generate tetramer plot file: %s [%s]', tplotfile, stdOut)
                    ok = False
            else:
                LOGGER.error(' - cmd failed: [%s]', stdErr.strip())
                ok = False
        ##==

        # gc_cov
        if ok:
            ok = do_gc_cov(postdir, libname, fqlink)

        if ok:
            status = POST_ASSEMBLE_END
            checkpoint(status, lastStep)
    else:
        LOGGER.info('  - no need to perform post assemble process : have been done already\n')
        fqlink = os.path.join(postdir, os.path.basename(contigs))

    return status, fqlink, ok

def do_sketch_vs_db(contigs, postdir):
    ok = True
    DB_MAP = {
                'refseq' : '',
                'nt' : '',
                'silva' : 'local'
            }
    filesFile = ASMMETA[FILES_FILE_TAG]
    for dbn, opt in DB_MAP.iteritems():
        sketchOutName = 'sketch_vs_%s.txt' % dbn
        sketchLogName =  sketchOutName.replace('.txt', '.log')
        sketchOut = os.path.join(postdir, sketchOutName)
        sketchLog = os.path.join(postdir, sketchLogName)
        cmd = '%s in=%s out=%s %s %s ow > %s' % (get_exec('sendsketch.sh', bbtools=True), contigs, sketchOut, dbn, opt, sketchLog)
        LOGGER.info(' - RUN %s', cmd)
        stdOut, stdErr, exitCode = run_cmd(cmd)

        if exitCode == 0:
            LOGGER.info(' - sketch vs %s completed' % dbn)
            append_rqc_stats(filesFile, 'stetch vs %s' % dbn, sketchOut, LOGGER)
        else:
            LOGGER.error(' - sketch vs %s failed : %s' % (dbn, stdErr))
            ok = False
            break

    return ok

def do_gc_cov(postdir, libname, contigs):
    LOGGER.info(' RUN gc_cov')
    reads = None
    if ERR_CORRECTION in ASMMETA:
        reads = ASMMETA[ERR_CORRECTION]
    elif SUB_SAMPLE in ASMMETA:
        reads = ASMMETA[SUB_SAMPLE]
    else:
        reads = ASMMETA[INPUT_FASTQ]
    gccovdir = os.path.join(postdir, 'gc_cov')
    gccovcmd = 'gc_cov.py'
    cmd = '%s/../sag/%s -f %s -fq %s -l %s -o %s --mode fungal --sketch' % (SRCDIR, gccovcmd, contigs, reads, libname, gccovdir)
    LOGGER.info('  RUN comd : %s', cmd)
    _, stdErr, exitCode = run_cmd(cmd)
    # exitCode = 0
    if exitCode != 0:
        LOGGER.error('  -gc_cov failed : %s' % stdErr)
        return False
    else:
        # add *.png files to files list
        filesFile = ASMMETA[FILES_FILE_TAG]
        pngfiles = [f for f in os.listdir(gccovdir) if os.path.isfile(os.path.join(gccovdir, f)) and (f.endswith('.png') or f.endswith('.txt'))]
        for fname in pngfiles:
            keyname = fname[:-4]
            append_rqc_stats(filesFile, keyname, os.path.realpath(fname))

        ## create the asm len/gc/aligned read cov gnuplot
        gccovstats = os.path.join(gccovdir, 'gccovstats.txt')
        if os.path.isfile(gccovstats):
            sh = os.path.join(SRCDIR, 'tools/len_gc_cov.sh')
            gplot = os.path.join(gccovdir, '%s_asm_len_gc_cov.png' % libname)
            cmd = '%s %s %s %s' % (sh, gccovstats, gplot, libname)
            _, stdErr, exitCode = run_cmd(cmd)
            if exitCode != 0:
               LOGGER.info(' - cmd failed : %s' % stdErr)
               return False
            else:
                append_rqc_stats(filesFile, 'asm_len_gc_cov', gplot)

    return True

def raw_contig_stats(odir):
    'untrimmed contigs stats'
    ok = True

    statsFile = ASMMETA[STATS_FILE_TAG]

    asmInput = ASMMETA[FOR_ASM]
    cmd = '%s in=%s format=2 | grep "    All" | awk \'{ print $5 }\'' % (get_exec('stats.sh', bbtools=True), asmInput)
    stdOut, stdErr, exitCode = run_cmd(cmd)
    inputBaseCnt = 0
    if exitCode == 0:
        inputBaseCnt = int(stdOut.strip())
        append_rqc_stats(statsFile, 'total base in assembly input', inputBaseCnt, LOGGER)
    else:
        LOGGER.error('failed to count base total in assembly input file %s' % asmInput)
        return False

    rawContigs = ASMMETA[RAW_CONTIG_FILE_TAG]
    rawContigStats = os.path.join(odir, 'raw_contig_stats.txt')
    cmd = '%s in=%s format=2 > %s' % (get_exec('stats.sh', bbtools=True), rawContigs, rawContigStats)
    LOGGER.info('RUN %s' % cmd)
    _, stdErr, exitCode = run_cmd(cmd)
    if exitCode == 0:
        with open(rawContigStats, 'r') as fh:
            for idx, line in enumerate(fh):
                if idx > 0:
                    if idx == 1:
                        toks = line.split()
                        if len(toks) > 8:
                            gcmean = toks[7]
                            gcstd = toks[8]
                            append_rqc_stats(statsFile, 'contig_gc_avg_raw', gcmean, LOGGER)
                            append_rqc_stats(statsFile, 'contig_gc_std_raw', gcstd, LOGGER)
                    else:
                        toks = line.split(':')
                        if line.startswith('Main genome contig total'):
                            append_rqc_stats(statsFile, 'contig_n_contigs_raw', toks[1].strip(), LOGGER)
                        elif line.startswith('Main genome scaffold sequence total'):
                            append_rqc_stats(statsFile, 'contig_scaf_bp_raw', toks[1].strip(), LOGGER)
                        elif line.startswith('Main genome contig sequence total'):  # toks[1] = "4960255         0.000% gap"
                            toks = toks[1].split()
                            ctgBaseCnt = int(toks[0].strip())
                            append_rqc_stats(statsFile, 'contig_contig_bp_raw', ctgBaseCnt, LOGGER)
                            append_rqc_stats(statsFile, 'contig_gap_pct_raw', toks[1].strip()[:-1], LOGGER)
                            coverage = float(inputBaseCnt) / float(ctgBaseCnt)
                            ASMMETA[ASM_COVERAGE] = '%.1f' % coverage
                            # append_rqc_stats(statsFile, 'assembly coverage', '%.1f' % coverage, LOGGER)
                        elif line.startswith('Main genome scaffold N/L50'):
                            toks = toks[1].split('/')
                            append_rqc_stats(statsFile, 'contig_scaf_N50_raw', toks[0].strip(), LOGGER)
                            append_rqc_stats(statsFile, 'contig_scaf_L50_raw', toks[1].strip(), LOGGER)
                        elif line.startswith('Main genome contig N/L50'):
                            toks = toks[1].split('/')
                            append_rqc_stats(statsFile, 'contig_ctg_N50_raw', toks[0].strip(), LOGGER)
                            append_rqc_stats(statsFile, 'contig_ctg_L50_raw', toks[1].strip(), LOGGER)
                        elif line.startswith('Main genome scaffold N/L90'):
                            toks = toks[1].split('/')
                            append_rqc_stats(statsFile, 'contig_scaf_N90_raw', toks[0].strip(), LOGGER)
                            append_rqc_stats(statsFile, 'contig_scaf_L90_raw', toks[1].strip(), LOGGER)
                        elif line.startswith('Main genome contig N/L90'):
                            toks = toks[1].split('/')
                            append_rqc_stats(statsFile, 'contig_ctg_N90_raw', toks[0].strip(), LOGGER)
                            append_rqc_stats(statsFile, 'contig_ctg_L90_raw', toks[1].strip(), LOGGER)
                        elif line.startswith('Max scaffold length'):
                            append_rqc_stats(statsFile, 'contig_scaf_max_raw', toks[1].strip(), LOGGER)
                        elif line.startswith('Max contig length'):
                            append_rqc_stats(statsFile, 'contig_ctg_max_raw', toks[1].strip(), LOGGER)
                        elif line.startswith('Number of scaffolds > 50 KB'):
                            append_rqc_stats(statsFile, 'contig_scaf_n_gt50K_raw', toks[1].strip(), LOGGER)
                        elif line.startswith('% main genome in scaffolds > 50 KB'):
                            append_rqc_stats(statsFile, 'contig_scaf_pct_gt50K_raw', toks[1].strip()[:-1], LOGGER)
    else:
        LOGGER.error(' stats.sh failed on raw contig file : %s' % stdErr)
        ok = False

    return ok

def assembly_gc_plot(wdir):
    'uses matplotlib to make an html and png file plot.'

    rawDataMatrix = []
    gchistPngPlotFilePath = None
    gchistHtmlPlotFilePath = None
    gchistFilePath = None

    LOGGER.info('Creating %GC vs contig count plot')
    contigs = ASMMETA[CONTIG_FILE_TAG]
    gchistFilePath = os.path.join(wdir, 'contigs_gc.txt')
    cmd = '%s in=%s gchist=%s ow' % (get_exec('stats.sh', bbtools=True), contigs, gchistFilePath)
    LOGGER.info('RUN %s' % cmd)
    _, stdErr, exitCode = run_cmd(cmd)

    if exitCode == 0:
        with open(gchistFilePath, 'r') as fh:
            for line in fh:
                if line.startswith('#'):
                    continue

                line = line.strip()
                toks = line.split()
                binhead = float(toks[0])
                contigcnt = int(toks[1])
                if contigcnt > 0:
                    rawDataMatrix.append([binhead, contigcnt])

        rawDataMatrix = np.array(rawDataMatrix)
        fig, ax = plt.subplots()

        markerSize = 5.0
        lineWidth = 1.5

        p1 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 1], 'r', marker='o', markersize=markerSize, linewidth=lineWidth, alpha=0.5)

        ax.set_xlabel('%GC', fontsize=12, alpha=0.5)
        ax.set_ylabel('Contigs count', fontsize=12, alpha=0.5)
        ax.grid(color='gray', linestyle=':')

        ## Add tooltip
        mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p1[0], labels=list(rawDataMatrix[:, 1])))


        baseName = 'contigs_gc.hist'

        ## Save D3 interactive plot in html format
        gchistHtmlPlotFilePath = os.path.join(wdir, '%s.html' % baseName)
        mpld3.save_html(fig, gchistHtmlPlotFilePath)

        ## Save Matplotlib plot in png format
        gchistPngPlotFilePath = os.path.join(wdir, '%s.png' % baseName)
        plt.savefig(gchistPngPlotFilePath, dpi=fig.dpi)

        if os.path.isfile(gchistPngPlotFilePath):
            LOGGER.info('GC plot of contigs successfully genearted: %s %s', gchistPngPlotFilePath, gchistHtmlPlotFilePath)
            statsFile = ASMMETA[STATS_FILE_TAG]
            filesFile = ASMMETA[FILES_FILE_TAG]
            append_rqc_file(filesFile, ASM_LEVEL_GC_HIST_HTML_FILE_TAG, gchistHtmlPlotFilePath, LOGGER)
            append_rqc_file(filesFile, ASM_LEVEL_GC_HIST_IMG_FILE_TAG, gchistPngPlotFilePath, LOGGER)
            append_rqc_file(filesFile, ASM_LEVEL_GC_HIST_FILE_TAG, gchistFilePath, LOGGER)

        else:
            LOGGER.warning('Failed to generate GC plot of contigs.')
            gchistPngPlotFilePath = gchistHtmlPlotFilePath = gchistFilePath = None

    return gchistPngPlotFilePath, gchistHtmlPlotFilePath, gchistFilePath


'''
run_blastplus_py

Call run_blastplus.py
'''
def run_blastplus_py(queryFastaFile, db):
    outDir = os.path.dirname(queryFastaFile)
    queryFastaFileBaseName = os.path.basename(queryFastaFile)
    dbFileBaseName = os.path.basename(db)

    runBlastnCmd = os.path.join(TOOLDIR, 'run_blastplus_taxserver.py')
    blastOutFileNamePrefix = os.path.join(outDir, 'megablast.%s.vs%s' % (queryFastaFileBaseName, dbFileBaseName))
    timeoutCmd = get_tool_path('timeout', 'timeout')
    cmd = '%s 21600s %s -d %s -o %s -q %s -s > %s.log 2>&1 ' % (timeoutCmd, runBlastnCmd, db, outDir, queryFastaFile, blastOutFileNamePrefix)

    LOGGER.info('run_blastplus command: %s', cmd)
    _, stdErr, exitCode = run_cmd(cmd)

    ## Added timeout to terminate blast run manually after 6hrs
    ## If exitCode == 124 or exitCode = 143, this means the process exits with timeout.
    ## Timeout exits with 128 plus the signal number. 143 = 128 + 15 (SGITERM)
    ## Ref) http://stackoverflow.com/questions/4189136/waiting-for-a-command-to-return-in-a-bash-script
    ##      timeout man page ==> If the command times out, and --preserve-status is not set, then exit with status 124.
    ##
    if exitCode in (124, 143):
        ## BLAST timeout
        ## Exit with succuss so that the blast step can be skipped.
        LOGGER.info('##################################')
        LOGGER.info('BLAST TIMEOUT. JUST SKIP THE STEP.')
        LOGGER.info('##################################')
        return False, -143

    elif exitCode != 0:
        LOGGER.error('failed to run blastplus. [%s]', stdErr)
        return False, None

    else:
        LOGGER.info('run blastplus vs %s complete.', db)

    return True, blastOutFileNamePrefix

def do_sketch(wdir, contig, lastStep):
    'sketch the read file (raw or subsumpled) vs nt, refseq, silva'
    status = lastStep
    ok = True
    skip = False

    if STEP_ORDER[lastStep] <= STEP_ORDER[SKETCH_START]:
        status = SKETCH_START
        checkpoint(status, lastStep)

        stime = time.time()
        sketchOutPath = os.path.join(wdir, 'sketch')
        if os.path.isdir(sketchOutPath):
            shutil.rmtree(sketchOutPath)
        make_dir(sketchOutPath)
        # change_mod(sketchOutPath, '0755')

        seqUnitName = os.path.basename(contig)
        seqUnitName = seqUnitName.replace('.gz', '').replace('.fastq', '').replace('.fasta', '')

        comOptions = 'ow=t colors=f printtaxa=t unique2'
        sendSketchShCmd = get_exec('sendsketch.sh', bbtools=True)

        ##= local helper function
        def sketch_helper(dbName, tagName):
            sketchOutFile = os.path.join(sketchOutPath, '%s.sketch_vs_%s.txt' % (seqUnitName, dbName))
            cmd = '%s in=%s out=%s %s %s' % (sendSketchShCmd, contig, sketchOutFile, comOptions, dbName)
            LOGGER.info(' RUN CMD : %s', cmd)
            _, stdErr, exitCode = run_cmd(cmd)
            if exitCode == 0:
                append_rqc_file(ASMMETA[FILES_FILE_TAG], tagName, sketchOutFile, LOGGER)
                return True

            LOGGER.error(' - cmd failed : %s', stdErr)
            return False

        ## NT ##########################
        ok = sketch_helper('nt', SKETCH_VS_NT_FILE_TAG)

        ## Refseq ##########################
        if ok:
            ok = sketch_helper('refseq', SKETCH_VS_REFSEQ_FILE_TAG)

        ## Silva ##########################
        if ok:
            ok = sketch_helper('silva', SKETCH_VS_SILVA_FILE_TAG)

        if ok:
            status = SKETCH_END
            checkpoint(status, lastStep)
    else:
        LOGGER.info('  - no need to perform sketching : have been done already\n')

    return status, ok

def assmbler_version(assembler):
    'return assembler version.'
    version = 'unknown'
    if assembler == 'velvet':
        cmd = get_exec('velveth')   # run velveth to get version for velvet
    else:
        cmd = get_exec(assembler)

        if assembler == 'tadpole':
            cmd = get_exec('bbversion.sh', bbtools=True)
        elif assembler in ['megahit', 'spades']:
            cmd = '%s --version' % cmd
        if cmd:
            LOGGER.info(' check assember version')
            LOGGER.info(' RUN CMD : %s' % cmd)
            stdOut, stdErr, exitCode = run_cmd(cmd)
            if exitCode == 0:
                if assembler == 'velvet':
                    for line in stdOut.split('\n'):
                        if line.startswith("Version"):
                            version = line
                            break
                elif assembler == 'tadpole':
                    version = stdOut.strip()
                elif assembler == 'spades':
                    version = stdErr.strip().split()[-1]
                elif assembler == 'megahit':
                    version = stdOut.split()[1].strip()
    return version

def velvet_assembly_summary(wdir):
    'to gernate velvet specific assembly summary'
    ## Velvet assembly summary is in 'Log' file. We will change this name to other meanful name later.

    statsFile = ASMMETA[STATS_FILE_TAG]
    filesFile = ASMMETA[FILES_FILE_TAG]
    ok = True

    asmdir = os.path.join(wdir, 'velvet')
    velvetLog = os.path.join(asmdir, 'Log')
    cmd = 'tail -1 %s' % velvetLog
    LOGGER.info(' get velvet Log info')
    LOGGER.info(' RUN CMD : %s', cmd)
    stdOut, stdErr, exitCode = run_cmd(cmd)

    if exitCode == 0:
        if len(stdOut) < 1:
            LOGGER.error('velvet assembler Log tail failure : empty file %s', velvetLog)
            ok = False
    else:
        LOGGER.error('velvet assembler Log tail failure : %s', stdErr)
        ok = False

    if ok:
        # "Final graph has 15386 nodes and n50 of 2363, max 13943, total 5252334, using 0/9660736 reads"
        match = re.match(r'Final graph has (\d+) nodes and n50 of (\d+), max (\d+), total (\d+), using \d+/(\d+) reads', stdOut)
        if match:
            totalnodes = match.group(1)
            append_rqc_stats(statsFile, 'velvet %s' % AssemblyStats.ILLUMINA_ASSEMBLY_NODE_NUMBER, totalnodes, LOGGER)

            n50 = match.group(2)
            append_rqc_stats(statsFile, 'velvet %s' % AssemblyStats.ILLUMINA_ASSEMBLY_N50, n50, LOGGER)

            maxcontiglen = match.group(3)
            append_rqc_stats(statsFile, 'velvet %s' % AssemblyStats.ILLUMINA_ASSEMBLY_MAX_CONTIG_LENGTH, maxcontiglen, LOGGER)

            totalnodeslen = match.group(4)
            append_rqc_stats(statsFile, 'velvet %s' % AssemblyStats.ILLUMINA_ASSEMBLY_NODE_LENGTH, totalnodeslen, LOGGER)

            totalreads = match.group(1)
            append_rqc_stats(statsFile, 'velvet %s' % AssemblyStats.ILLUMINA_ASSEMBLY_READ_USED, totalreads, LOGGER)
        else:
            ok = False
    return ok

def assembly_megablast_hits(blastdir, dbName):
    'generates tophit list of megablast against different databases.'

    LOGGER.info(' - parse assembly megablast hits against %s', dbName)
    statsFile = ASMMETA[STATS_FILE_TAG]
    filesFile = ASMMETA[FILES_FILE_TAG]

    ## To find the matching hits
    numMatchingHits = 0
    parsedFileName = ''
    parsedFileAbspath = ''
    for filename in os.listdir(blastdir):
        match = re.match(r'(megablast\.\S+\.%s\S*\.parsed)' % dbName, filename)
        if match:
            parsedFileName = match.group(1)

    if parsedFileName != '':
        parsedFileAbspath = os.path.join(blastdir, parsedFileName)
        numMatchingHits, _ = calculate_line_total(parsedFileAbspath, LOGGER)

    if type(numMatchingHits) is int and numMatchingHits >= 2:
        numMatchingHits = numMatchingHits - 2

    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_MATCHING_HITS, dbName), numMatchingHits, LOGGER)
    append_rqc_file(filesFile, 'assembly level parsed file of %s database' % dbName, parsedFileAbspath, LOGGER)
    # append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_PARSED_FILE, dbName), parsedFileName, LOGGER)

    ##== To find the top hits
    numTophits = 0
    tophitsFileName = ''
    tophitsFileAbspath = ''

    for filename in os.listdir(blastdir):
        match = re.match(r'(megablast\.\S+\.%s\S*\.parsed\.tophit)' % dbName, filename)
        if match:
            tophitsFileName = match.group(1)

    if tophitsFileName != '':
        tophitsFileAbspath = os.path.join(blastdir, tophitsFileName)
        numTophits, _ = calculate_line_total(tophitsFileAbspath, LOGGER)
        # log.info("numTophits: " + str(numTophits) + " tophitsFileAbspath " + str(tophitsFileAbspath))

    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TOP_HITS, dbName), numTophits, LOGGER)
    append_rqc_file(filesFile, 'assembly level top hit file of %s database' % dbName, tophitsFileAbspath, LOGGER)
    # append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TOPHIT_FILE, dbName), tophitsFileName, LOGGER)


    ##== To find the top 100 hits
    numTop100hits = 0
    top100hitsFileName = ''
    top100hitsFileAbspath = ''

    for filename in os.listdir(blastdir):
        match = re.match(r'(megablast\.\S+\.%s\S*\.parsed\.top100hit)' % dbName, filename)
        if match:
            top100hitsFileName = match.group(1)

    if top100hitsFileName != '':
        top100hitsFileAbspath = os.path.join(blastdir, top100hitsFileName)
        numTop100hits, _ = calculate_line_total(top100hitsFileAbspath, LOGGER)
        # log.info("numTop100hits: " + str(numTop100hits) + " top100hitsFileAbspath " + str(top100hitsFileAbspath))

    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TOP_100_HITS, dbName), numTop100hits, LOGGER)
    append_rqc_file(filesFile, 'assembly level top 100 hit file of %s database' % dbName, top100hitsFileAbspath, LOGGER)
    # append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TOP100HIT_FILE, dbName), top100hitsFileName, LOGGER)


    ##== To find the taxonomic species
    numSpecies = 0
    taxlistFileName = ""
    taxlistFileAbspath = ""

    for filename in os.listdir(blastdir):
        match = re.match("(megablast\.\S+\.%s\S*\.parsed\.taxlist)" % dbName, filename)
        if match:
            taxlistFileName = match.group(1)

    if taxlistFileName != "":
        taxlistFileAbspath = os.path.join(blastdir, taxlistFileName)
        numSpecies, _ = calculate_line_total(taxlistFileAbspath, LOGGER)
        # log.info("numSpecies: " + str(numSpecies) + " taxlistFileAbspath " + str(taxlistFileAbspath))

    append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TAX_SPECIES, dbName), numSpecies, LOGGER)
    append_rqc_file(filesFile, 'assembly level taxlist file of %s database' % dbName, taxlistFileAbspath, LOGGER)
    # append_rqc_stats(statsFile, '%s %s' % (AssemblyStats.ILLUMINA_ASSEMBLY_TAXLIST_FILE, dbName), taxlistFileName, LOGGER)

    return True

def cleanup_assemblyqc(wdir, assembler, skip=False):
    if skip:
        LOGGER.info(' == Skip clean up! ==')
        return
    else:
        LOGGER.info(' == Do clean up ==')
    # in wdir:
    subsample = ['*.subsampled_*.fastq']
    bfecfile = ['*.bfec.fastq', '*.bfec.log']

    # assembler specific files
    asmfiles = {
        'velvet' : ['Graph', 'Sequences', 'Roadmaps'],
        'megahit' : ['intermediate_contigs'],
        'spades' : ['K*', 'assembly_graph*', 'before_rr.fasta', 'corrected', 'mis*', 'tmp', 'split_input'],
        'tadpole' : []
    }

    # in post dir
    postfiles = ['GC.contigs.fa', 'contigs.kmers', ]

    # core file in wDir, assembler dir, post dir
    def rm_from_dir(alist, adir):
        for fname in alist:
            fpath = os.path.join(adir, fname)
            cmd = '/bin/rm -rf %s' % fpath
            LOGGER.info('Removing file : %s', cmd)
            run_cmd(cmd)

    rm_from_dir(asmfiles[assembler], os.path.join(wdir, assembler))
    rm_from_dir(postfiles, os.path.join(wdir, 'post'))
    wdirfiles = subsample + bfecfile
    rm_from_dir(wdirfiles, wdir)

'''
    config file contain lines in form of
    KEY = VALUE
'''
def assembler_opts(wdir):
    'copy the default config file into run dir, and readin the assembler run options'
    src = os.path.join(SRCDIR, 'config/assembler.cfg')
    dst = os.path.join(wdir, os.path.basename(src))
    if not os.path.isfile(dst): #
        shutil.copyfile(src, dst)
    opts = {}
    with open(dst) as fh:
        for line in fh:
            line = line.strip()
            if not line or line.startswith('#'):
                continue

            tok = line.split('=')   # no "=" is allowed in key and value strings
            if len(tok) > 1:
                opts[tok[0].strip()] = tok[1].strip()
    return opts
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program
if __name__ == '__main__':
    PROG_NAME = 'RQC assembly-qc pipeline'

    PROG_START_TIME = time.time()

    JOB_CMD = ' '.join(sys.argv)

    PARSER = argparse.ArgumentParser(description='Asseblyqc v2')

    ASSEMBLER = ['tadpole', 'spades', 'spades-sag', 'megahit', 'velvet']
    # ASSEMBLER = ['tadpole', 'spades', 'spades-sag', 'megahit']
    HELPS = {
        'nocleanup' : 'Skip temporary file cleanup',
        'nec'   : 'No bloom filter based error correction. Default is to perform the error correction before assembly',
        'kmer' : 'Set k-mer size may be user-specified. Has precedence over the -m option. ' +
                 'If -k is unspecified, then usually 80 percent of the read length will be used as kmer size, ' +
                 'up to a maximum value as specified with -m if specified',
        'maxkmer' : 'Set maximum k-mer size. Only has utility if the kmer size is not specified with the -k ' +
                    'option. If -k is unspecified but -m is specified, then 80 percent of the read length will ' +
                    'be used as kmer size, up to a maximum value as specified with -m',
        'aopts' : 'assembler options, in additional to these defined in the config file. use quotes. For a single option, ' +
                  'leave a space char at the beginnig, or end of the entire quoted options string [" --sc", or "--sc "]'
    }

    PARSER.add_argument('-c', '--skip-cleanup', dest='skipCleanup', action='store_true', default=False, help=HELPS['nocleanup'])
    PARSER.add_argument('-f', '--fastq', dest='fastq', help='Set input Fastq file (full path to fastq)', required=True)
    PARSER.add_argument('-l', '--lib', dest='library', help='Llibrary Name', required=True)
    PARSER.add_argument('-k', '--kmer', dest='kmer', help=HELPS['kmer'], type=int, required=False)
    PARSER.add_argument('-m', '--max-kmer', dest='maxKmer', help=HELPS['maxkmer'], type=int, required=False)
    PARSER.add_argument('-o', '--output-path', dest='outputPath', help='Set output path to write to', required=True)
    PARSER.add_argument('-r', '--max-reads', dest='maxReads', type=int, help='Set maximum number of reads to subsample', required=False)
    PARSER.add_argument('-nec', '--error-correction', dest='bfec', action='store_false', default=True, help=HELPS['nec'])
    PARSER.add_argument('-a', '--assembler', dest='assembler', help='assembler name', choices=ASSEMBLER, default=ASSEMBLER[0])
    PARSER.add_argument("-pl", "--print-log", dest="print_log", default=False, action="store_true", help='print log to screen')
    PARSER.add_argument('-v', '--version', action='version', version=VERSION)

    PARSER.add_argument('-ao', '--assembler-options', dest='assmblerOptions', help=HELPS['aopts'])
    PARSER.add_argument('-mem', '--mem', dest='memory', help='memory in G. For spades only.', type=int, required=False)

    ARGS = PARSER.parse_args()

    ASM_INPUT = None

    if ARGS.fastq:
        FASTQ = ARGS.fastq

    ## use current directory if no output path
    if not ARGS.outputPath:
        ARGS.outputPath = os.getcwd()

    ARGS.outputPath = os.path.realpath(ARGS.outputPath)

    ## create output_directory if it doesn't exist
    if not os.path.isdir(ARGS.outputPath):
        print('Cannot find %s, creating as new' % ARGS.outputPath)
        os.makedirs(ARGS.outputPath)

    ## initialize my logger
    LOG_FILE = os.path.join(ARGS.outputPath, 'assemblyqc.log')
    LOG_LEVEL = 'DEBUG'
    LOGGER = get_logger('assemblyqc', LOG_FILE, LOG_LEVEL, ARGS.print_log)

    print('Started assemblyqc pipeline, writing log to: %s' % LOG_FILE)

    if not os.path.isfile(FASTQ):
        LOGGER.error('Cannot find fastq: %s!', FASTQ)
        sys.exit(2)

    ## Create the standard files:
    ASMMETA = {}
    ASMMETA[INPUT_FASTQ] = FASTQ
    ASMMETA[FILES_FILE_TAG] = os.path.join(ARGS.outputPath, 'rqc-files.txt')
    ASMMETA[STATS_FILE_TAG] = os.path.join(ARGS.outputPath, 'rqc-stats.tmp')
    ASMMETA[CHECKPOINT_FILE_TAG] = os.path.join(ARGS.outputPath, 'status.log')

    STATUS = get_status(ASMMETA[CHECKPOINT_FILE_TAG], LOGGER)
    if STATUS == 'complete':
        LOGGER.info('Status is complete, not processing.')
        sys.exit(0)

    LOGGER.info('The fastq file is %s', FASTQ)

    HOST_NAME = platform.node()

    LOGGER.info('CMD : %s', JOB_CMD)
    LOGGER.info('=================================================================')
    LOGGER.info('Assembly Qc Analysis (version %s)', VERSION)
    LOGGER.info('   %30s : %s', 'Library', ARGS.library)
    LOGGER.info('   %30s : %s', 'outdir', ARGS.outputPath)
    LOGGER.info('   %30s : %s', 'assembler', ARGS.assembler)
    LOGGER.info('   %30s : %s', 'skip cleanup', ARGS.skipCleanup)
    # LOGGER.info('   %30s : %s', 'kmer size', ARGS.kmer)
    # LOGGER.info('   %30s : %s', 'max kmer size', ARGS.maxKmer)
    LOGGER.info('   %30s : %s', 'subsample size', ARGS.maxReads)
    LOGGER.info('   %30s : %s', 'do err correction', ARGS.bfec)
    LOGGER.info('   %30s : %s', 'extra assembler options', ARGS.assmblerOptions)
    LOGGER.info('   %30s : %s', 'running on', HOST_NAME)
    LOGGER.info('=================================================================')

    LOGGER.info('Starting %s on %s', PROG_NAME, FASTQ)

    if ARGS.maxReads and ARGS.maxReads != '' and int(ARGS.maxReads) < 1:
        LOGGER.error('subsample reads must be > 1 [%s]. Can not continue!', ARGS.maxReads)
        sys.exit(2)

    if STATUS == 'start':
        STATUS = PIPE_START
        checkpoint(STATUS)
    elif STATUS == PIPE_COMPLETE:
        LOGGER.warning('Pipeline has been completed before. Skip processing!')
        cleanup_assemblyqc(ARGS.outputPath, ARGS.assembler, skip=ARGS.skipCleanup)
        sys.exit(0)
    else:
        LOGGER.info('\nLast step -- [ %s ]', STATUS.upper())

    STATUS, PAIRED, RLENGTH, KSIZE = init(FASTQ, ARGS.assembler, ARGS.kmer, ARGS.maxKmer, STATUS)

    if not RLENGTH or RLENGTH < 1:
        sys.exit(2)

    extraOpt = None
    if ARGS.assembler == 'spades-sag':
        extraOpt = '--sc'
        ARGS.assembler = 'spades'

    assemblerOpt = assembler_opts(ARGS.outputPath)[ARGS.assembler]
    if extraOpt:
        assemblerOpt = '%s %s' % (assemblerOpt, extraOpt)

    if ARGS.assmblerOptions:
        assemblerOpt = '%s %s' % (assemblerOpt, ARGS.assmblerOptions.strip().replace('\\', ''))

    # print('DEBUG %s' % ARGS.memory)
    if ARGS.assembler == 'spades' and ARGS.memory:
        match = re.match(r'.* (-m\s+\d+)', assemblerOpt)
        if match:
            assemblerOpt = assemblerOpt.replace(match.group(1), '-m %s' % ARGS.memory)
    ASMMETA[ASSEMBER_OPT] = assemblerOpt

    if ARGS.maxReads:
        ## subsample file will be stored in ASMMETA[SUB_SAMPLE]
        STATUS, SUB_FILE = do_subsample(FASTQ, ARGS.outputPath, ARGS.maxReads, PAIRED, STATUS)
        if not SUB_FILE:
            sys.exit(1)
    else:
        LOGGER.info('No subsampling is requested by user.\n')

    if ARGS.bfec:
        ## error correction file will be stored in ASMMETA[ERR_CORRECTION]
        STATUS, EC_FILE = do_bloomf_err_correction(ARGS.outputPath, FASTQ, STATUS)
        if not EC_FILE:
            sys.exit(1)
    else:
        LOGGER.info('No error correction is requested by user.\n')

    if ERR_CORRECTION in ASMMETA:
        ASM_INPUT = ASMMETA[ERR_CORRECTION]
    elif SUB_SAMPLE in ASMMETA:
        ASM_INPUT = ASMMETA[SUB_SAMPLE]
    else:
        ASM_INPUT = ASMMETA[INPUT_FASTQ]

    LOGGER.info('Assembly input file : %s' % ASM_INPUT)

    CONTIGS = None
    if ASM_INPUT:
        ASMMETA[FOR_ASM] = ASM_INPUT
        if ARGS.assembler == 'velvet':
            STATUS, CONTIGS = do_velvet_assemble(KSIZE, ASM_INPUT, ARGS.outputPath, ARGS.library, STATUS)
        else:
            STATUS, CONTIGS = do_assemble(ARGS.assembler, ASM_INPUT, PAIRED, ARGS.outputPath, ARGS.library, STATUS)

    if CONTIGS:
        STATUS, CONTIG, OK = do_post_process(ARGS.assembler, CONTIGS, ARGS.outputPath, KSIZE, RLENGTH, ARGS.library, STATUS)
        if not OK:
            LOGGER.error('postprocessing failed!')
            sys.exit(1)
        else:
            # save_asm_stats created the "contig_coverage_x = 0.0" in stats file, replace it with the true value
            with open(ASMMETA[STATS_FILE_TAG], 'r') as fh:
                # the final stats file: finalize the assembly stat keynames:
                fstat = os.path.join(ARGS.outputPath, os.path.basename(ASMMETA[STATS_FILE_TAG]).replace('tmp', 'txt'))
                with open(fstat, 'w') as fh2:
                    for line in fh:
                        if line.startswith('contig_coverage_x = 0.0') and ASM_COVERAGE in ASMMETA:
                            line = 'coverage_x = %s\n' % ASMMETA[ASM_COVERAGE]
                        elif line.startswith('contig_n_contigs'):
                            line = line.replace('contig_n_contigs', 'contigs_n', 1)
                        elif line.startswith('contig_n_scaffolds'):
                            line = line.replace('contig_n_scaffolds', 'scaffolds_n', 1)
                        elif line.startswith('contig_ctg'):
                            line = line.replace('contig_ctg', 'contig', 1)
                        elif line.startswith('contig_'):
                            line = line.replace('contig_', '', 1)
                        fh2.write(line)

            os.remove(ASMMETA[STATS_FILE_TAG])  # remove the tmp file

            STATUS, OK = do_sketch(ARGS.outputPath, CONTIGS, STATUS)

        if not OK:
            LOGGER.error('Pipeline failed!')
            sys.exit(1)
        else:
            cleanup_assemblyqc(ARGS.outputPath, ARGS.assembler, skip=ARGS.skipCleanup)
            checkpoint(PIPE_COMPLETE, STATUS)

    ##== run time info
    LOGGER.info('Total wallclock runtime: %s', str(time.time() - PROG_START_TIME))

## EOF
