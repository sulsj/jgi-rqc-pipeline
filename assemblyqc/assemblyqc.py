#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
 How to run the assemblyQC pipeline for development testing:

 As andreopo@gpint24:

 cd /global/projectb/scratch/andreopo/test_rqc_pipeline.6397.5.44056.TATAAT.srf_B

 export PYTHONPATH=/house/homedirs/a/andreopo/src/bitbucket/jgi-rqc-pipeline/assemblyqc/lib/:/house/homedirs/a/andreopo/src/bitbucket/jgi-rqc-pipeline/readqc/lib/:/house/homedirs/a/andreopo/src/bitbucket/jgi-rqc-pipeline/lib/:/house/homedirs/a/andreopo/src/bitbucket/jgi-rqc-pipeline/tools/
 export PYTHONPATH=/global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/assemblyqc/lib/:/global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/readqc/lib/:/global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/lib/:/global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/tools/:/global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc/

 /global/projectb/sandbox/rqc/andreopo/src/bitbucket/jgi-rqc-pipeline/assemblyqc/assemblyqc.py -f /house/sdm/prod/illumina/seq_data/fastq/00/00/63/97/6397.5.44056.TATAAT.fastq.gz -o /global/projectb/scratch/andreopo/test_rqc_pipeline.6397.5.44056.TATAAT.srf_B/ -r 5000000
optional parameter:   -k 63 -m 71




Update started: Sept 23 2015
sulsj (ssul@lbl.gov)

Revision:

    20150923 4.0.0: Start updating for removing legacy codes
    20151021 4.0.0: Released! c52e0b8

    20151109 4.1.0: Updated to do the Blast search against refseq.archaea and refseq.bacteria instead of refseq.microbial
    20151111 4.1.0: Released! 0585e15
    20151201 4.1.1: Removed the access to the tax db; Replaced run_blastn with run_blastplus
    20151214 4.1.2: Skip Megan

    20151217 5.0.0: Cleanup

    20160329 5.1.0: Replaced Jigsaw's run_blast.pl with run_blastplus.py
    20160427 5.1.1: Updated assembly_megablast_hits() and calculate_line_total() for run_blastplus.py
    20160501 5.1.2: Added top 100 hit file creation
    20160516 5.1.3: Fixed assembly qc for blast timeout
    20160803 5.1.4: isFqPaired.1.8.py not found! Changed it to isFqPaired.1.8.py
    20160829 5.1.5: Passed isPaired to illumina_subsampling()
    20160831 5.1.6: Updated with new localize_dir2()
    20170302 5.1.7: Fixed velvet output path detection routine

    20170512 5.2.0: Added denovo and cori support

    20170531 5.3.0: Updated to use run_blastplus_taxserver.py

    20170908 5.4.0: Added sketch vs refseq, nt, silva
    20170928 5.4.1: Added "printtaxa=t" for sendsketch.sh

    20171101 5.4.2: Removed green genes from Blasting
    20180105 5.4.3: Updated to exit 0 even if gc hist plot cannot be created
    20180125 5.4.4: Add int=t to bbduk.sh

    20180501 5.5.0: Updated to use sulsj/rocker:latest Rocker container for Rscript

"""


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
import argparse
from collections import defaultdict
import operator
import re
from time import time

# custom libs in "../lib/"
srcDir = os.path.dirname(__file__)
sys.path.append(os.path.join(srcDir, 'lib'))       ## rqc-pipeline/assemblyqc/lib
sys.path.append(os.path.join(srcDir, '../lib'))    ## rqc-pipeline/lib
sys.path.append(os.path.join(srcDir, '../tools'))  ## rqc-pipeline/tools

from assemblyqc_utils import *
from assemblyqc_constants import *
from assemblyqc_report import *
from rqc_fastq import get_working_read_length
from common import append_rqc_stats, append_rqc_file, set_colors
from os_utility import run_sh_command, make_dir, change_mod
from rqc_utility import safe_dirname, safe_basename
from rqc_constants import RQCExitCodes, RQCReferenceDatabases

import numpy as np
import matplotlib
matplotlib.use('Agg') ## This needs to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
import mpld3


VERSION = "5.5.0"
color = {}
color = set_colors(color, True)
NERSC_HOST = os.environ['NERSC_HOST']
LOG_LEVEL = "DEBUG"
# LOG_LEVEL = "INFO"


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

"""
illumina_assemble_velvet

Run illumina_assemble_velvet and check for output
Assume sequnitName contains no prefix of srf or fastq.gz.

 Title:        illumina_assemble_velvet
 Function:     Takes path(s) to bz2 zipped or gzipped fastq file
               and uses Velvet to assemble reads. Makes gc plot of
               contigs, and megablast contigs.

               * The input is subsampled fastq

 Usage:        illumina_assemble_velvet(kmer, fastq, outputPath, log)
 Returns:      JGI_SUCCESS
               JGI_FAILURE
 Comments:     None.

"""
def illumina_assemble_velvet(kmer, fastq, outputPath, statusLogFile, rqc_stats_log, log, skip_megan=False):
    log.info("\n\n%s - RUN illumina_assemble_velvet <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    status = "illumina_assemble_velvet in progress"
    checkpoint_step(statusLogFile, status)

    sequnitName, exitCode = safe_basename(fastq, log)
    log.info("fastq_base sequnitName is: %s", str(sequnitName))

    ## If not found fastq then return failure.
    if os.path.isfile(fastq):
        log.info("fastq file found: %s", fastq)
    else:
        status = "fastq checking failed"
        log.error("fastq file not found")
        return status

    assemDirName = str(sequnitName) + ".velvet.k" + str(kmer)
    assemDir = os.path.join(outputPath, assemDirName)

    # mkdirCmd = "mkdir -p %s" % (assemDir)
    # _, _, _ = run_sh_command(mkdirCmd, True, log, True)

    zcatCmd, exitCode = get_cat_cmd(fastq, log)

    if exitCode != 0:
        status = "get_cat_cmd failed"
        log.error("get_cat_cmd failure, exit code != 0")
        return status


    ##############################
    ## velveth command
    cmd = RQCVelvet.VELVETH_WRAPPER  + " \"" + zcatCmd + "\" " + fastq + " " + RQCVelvet.BBDUK_TRIMMER + " " + RQCVelvet.VELVETH + " " + str(assemDir) + " " + str(kmer)

    if NERSC_HOST != "genepool":
        ## $1 $2 | $3 ow in=stdin.fq out=stdout.fq qtrim=rl trimq=5 minlength=35 maxns=2 gchist=$5/GC.contigs.fa.BBDUK.hist | $4 $5 $6 -fastq -
        velvethCmd = get_tool_path("velveth", "velvet")
        bbdukCmd = get_tool_path("bbduk.sh", "bbtools")
        cmd = "%s %s | %s ow in=stdin.fq out=stdout.fq int=t qtrim=rl trimq=5 minlength=35 maxns=2 gchist=%s | %s %s %s -fastq -" %\
              (zcatCmd, fastq, bbdukCmd, os.path.join(assemDir, "GC.contigs.fa.BBDUK.hist"), velvethCmd, assemDir, str(kmer))

    log.info("\n\n%s - RUN velveth <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    log.info("cmd: %s", cmd)

    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)

    if exitCode == 0:
        if os.path.isfile(assemDir + "/Roadmaps"):
            status = "velveth complete"
            log.info(status)
        else:
            status = "velveth output checking failed"
            log.error("velveth failed %s", stdErr)
            return status
    else:
        status = "velveth failed"
        log.error("velveth failure, exit code != 0")
        return status


    ##############################
    ## velvetg command
    ## Do NOT change the velvetg parameters. Low coverage cutoff is intended to detect contamination.
    cmd = RQCVelvet.VELVETG + " " + assemDir + " -cov_cutoff 1 -min_contig_lgth 100"

    if NERSC_HOST != "genepool":
        ## $1 $2 | $3 ow in=stdin.fq out=stdout.fq qtrim=rl trimq=5 minlength=35 maxns=2 gchist=$5/GC.contigs.fa.BBDUK.hist | $4 $5 $6 -fastq -
        velvetgCmd = get_tool_path("velvetg", "velvet")
        cmd = "%s %s -cov_cutoff 1 -min_contig_lgth 100" % (velvetgCmd, assemDir)

    log.info("\n\n%s - RUN velvetg <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    log.info("cmd: %s", cmd)

    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)

    contigs = assemDir + "/contigs.fa"

    if exitCode == 0:
        if os.path.isfile(contigs):
            status = "velvetg complete"
            log.info(status)
        else:
            status = "velvetg output checking failed"
            log.error("velvetg failed %s", stdErr)
            return status
    else:
        status = "velvetg failed"
        log.error("velvetg failure, exit code != 0")
        return status


    log.info("\n\n%s - RUN ill_velvet_stats <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    ## Print the stat values to file.
    ## The ill_velvet_stats.sh command will output: GC.contigs.fa.hist, GC.contigs.fa.hist.gp, depth.*, g, s, gc+stats, 8212.2.93516.ACTGAT.subsampled5000000.fastq.velvet.k63.depth.out, 8212.2.93516.ACTGAT.subsampled5000000.fastq.velvet.k63.assem.summary .
    ## Later illumina_contig_gc_plot will be called to output GC.contigs.fa.hist.parse and GC.contigs.fa.hist.png image plot files.
    cmd = RQCVelvet.ILL_VELVET_STATS_SH + " " + assemDir + " " + assemDirName

    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)
    gchistFileName = os.path.join(assemDir, "GC.contigs.fa.hist")

    # if exitCode == 0 and os.path.isfile(gchistFileName):
    if exitCode == 0:
        status = "velvet_stats complete"
        log.info(status)
    else:
        status = "velvet_stats failed"
        if not os.path.isfile(gchistFileName):
            log.error("Failed to run ill_velvet_stats and create gchist file")
        log.error("velvet_stats failure, exit code != 0")
        return status


    ##
    ## Velvet assembly summary is in 'Log' file. We will change this name to other meanful name later.
    ##
    summaryFile = "Log"
    tailCmd = "tail -1 " + os.path.join(assemDir, summaryFile) #`cat $summary_file_dir/$summaryFile`

    stdOut, stdErr, exitCode = run_sh_command(tailCmd, True, log, True)

    if exitCode == 0:
        if len(stdOut) < 1:
            status = "assemblyqc summary log file checking failed"
            log.error("summary log file checking failure, exit code != 0")
            return status
        else:
            status = "assemblyqc log file checking complete"
            log.info(status)
    else:
        status = "assemblyqc logname failed"
        log.error("assemblyqc logname failure, exit code != 0")
        return status


    m = re.match(".+\/(\d+)", stdOut)

    if len(m.groups()) > 0:
        totalReads = m.groups()[0]
        append_rqc_stats(rqc_stats_log, AssemblyStats.ILLUMINA_ASSEMBLY_READ_USED, totalReads)


    ## Check if the contigs.fa has size 0 or is too small, if yes then exit with success.
    contigsFileSize, exitCode = calculate_read_total_fasta(contigs, log)

    log.info("File size is %s for file %s", str(contigsFileSize), str(contigs))

    append_rqc_stats(rqc_stats_log, AssemblyStats.ILLUMINA_CONTIGS_FILESIZE, contigsFileSize)

    if exitCode != 0 or int(contigsFileSize) < 1:
        status = "size is " + str(contigsFileSize) + " for file " + str(contigs) + ", assemblyQC cannot continue, exiting with success"
        log.info(status)
        status = "assemblyqc velveth failed"
        return status



    log.info("\n\n%s - RUN TETRAMER <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    ## Compute the tetramers for the jigsaw plot
    ## Need to set the env variables properly.
    # os.environ['SHOW_KMER_BIN_R'] = '/projectb/sandbox/rqc/prod/pipelines/external_tools/tetramerK/20120823/showKmerBin.R '
    # os.environ['KMER_FREQUENCIES_JAR'] = '/projectb/sandbox/rqc/prod/pipelines/external_tools/tetramerK/20120823/KmerFrequencies.jar '
    os.environ['SHOW_KMER_BIN_R'] = '/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/showKmerBin.R'
    os.environ['KMER_FREQUENCIES_JAR'] = '/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/KmerFrequencies.jar'
    kmerConf = '/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/KmerFrequencies.conf'

    ## cp log4j conf file to working dir
    kmerCpCmd = "cp %s %s" % (kmerConf, os.environ['PWD'])
    _, _, _ = run_sh_command(kmerCpCmd, True, log, True)

    ## OLD
    # tetramerCmd = RQCVelvet.TETRAMER_CMD + " -od " + assemDir + " -w 100 -label \"contigs.fa\" " + contigs + " tetramer_velvet_assembly.jpg"
    # stdOut, stdErr, exitCode = run_sh_command(tetramerCmd, True, log, True)

    ## NEW
    ## TODO: remove tetramer_plotting step
    tetramerOutputFile = os.path.join(assemDir, "tetramer_velvet_assembly.kmers")
    tetramerLogFile = os.path.join(assemDir, "tetramer_velvet_assembly.log")
    kmerFreqJarLoc = os.environ['KMER_FREQUENCIES_JAR']
    cmd = "java -jar %s -n -i %s -o %s -w 2000 > %s 2>&1" % (kmerFreqJarLoc, contigs, tetramerOutputFile, tetramerLogFile)

    _, _, exitCode = run_sh_command(cmd, True, log, True)

    if exitCode == 0 and os.path.isfile(tetramerOutputFile):
        log.info("tetramer_cmd complete: %s", tetramerOutputFile)
    else:
        log.error("tetramer_cmd error, let it move to the next step.")

    tetramerPlotFile = os.path.join(assemDir, "tetramer_velvet_assembly")
    tetramerPlotLog = os.path.join(assemDir, "show_kmer_bin.log")
    showKmerBinLoc = os.environ['SHOW_KMER_BIN_R']

    ## Related with INC0118781
    ## source activation sometimes clears the permission of ldpaths
    ## thus, try to manually set the permission every time
    ##
    # checkCmd = "which Rscript"
    # so, se, ec = run_sh_command(checkCmd, True, log, True)
    # log.debug("Rscript check: %s %s %s", so, se, ec)
    # chPermCmd = "chmod 777 /global/projectb/sandbox/rqc/qc_user/miniconda/miniconda2/envs/qaqc2/lib/R/etc/ldpaths"
    # so, se, ec = run_sh_command(chPermCmd, True, log, True)
    # log.debug("ldpaths permission change: %s %s %s", so, se, ec)

    rscriptCmd = get_tool_path("Rscript", "R")
    cmd = "%s --vanilla %s --input %s --output %s --jpgurl bryce --doturl brycedot --label 'Principal Component Analysis' > %s 2>&1" %\
          (rscriptCmd, showKmerBinLoc, tetramerOutputFile, tetramerPlotFile, tetramerPlotLog)

    _, _, exitCode = run_sh_command(cmd, True, log, True)

    if exitCode == 0 and os.path.isfile(tetramerPlotFile+".jpg"):
        log.info("tetramer_plotting_cmd complete: %s", tetramerPlotFile+".jpg")
    else:
        log.error("tetramer_plotting_cmd error, let it move to the next step.")


    # log.info("\n\n%s - RUN write_top_5_largest_contigs <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    ## write the 5 largest contigs
    stdOut, exitCode = write_5_largest_contigs(contigs, log)

    if exitCode != 0:
        status = "write_5_largest_contigs failed"
        log.error("write_5_largest_contigs failure, exit code != 0")
        return status


    ##
    ## RUN BLAST
    ##
    refseqArchaea = RQCReferenceDatabases.REFSEQ_ARCHAEA
    refseqBacteria = RQCReferenceDatabases.REFSEQ_BACTERIA
    refseqMitoch = RQCReferenceDatabases.REFSEQ_MITOCHONDRION
    refseqPlant = RQCReferenceDatabases.REFSEQ_PLANT
    refseqPlasmid = RQCReferenceDatabases.REFSEQ_PLASMID
    refseqPlastid = RQCReferenceDatabases.REFSEQ_PLASTID
    refseqFungi = RQCReferenceDatabases.REFSEQ_FUNGI
    refseqViral = RQCReferenceDatabases.REFSEQ_VIRAL
    refGreenGenes = RQCReferenceDatabases.GREEN_GENES
    refContam = RQCReferenceDatabases.CONTAMINANTS
    ref16s = RQCReferenceDatabases.COLLAB16S
    refNT = RQCReferenceDatabases.NT_maskedYindexedN_BB

    ## silva
    refLSU = RQCReferenceDatabases.LSU_REF
    refSSU = RQCReferenceDatabases.SSU_REF
    refLSSU = RQCReferenceDatabases.LSSU_REF

    # refDatabases = [refseqArchaea, refseqBacteria, refseqMitoch, refseqPlant, refseqPlasmid, refseqPlastid, refseqFungi, refseqViral, refGreenGenes, refLSU, refSSU, refLSSU, refContam, ref16s, refNT]
    refDatabases = [refseqArchaea, refseqBacteria, refseqMitoch, refseqPlant, refseqPlasmid, refseqPlastid, refseqFungi, refseqViral, refLSU, refSSU, refLSSU, refContam, ref16s, refNT]

    if skipBlastFlag: ## skip refseq and nt
        # refDatabases = [refGreenGenes, refLSU, refSSU, refLSSU, refContam, ref16s] ## using sketch for other refs
        refDatabases = [refLSU, refSSU, refLSSU, refContam, ref16s] ## using sketch for other refs

    startTime = time()

    for db in refDatabases:
        log.info("\n\n%s - RUN blastn vs %s <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], db, color[''])

        ## contigs = contigs.fa from velvet
        retCode, megablastOutputFile = run_blastplus_py(contigs, db, log)

        if retCode != RQCExitCodes.JGI_SUCCESS:
            if megablastOutputFile is None:
                log.error("failed to run blastn against %s. Ret = %s", db, retCode)
                status = "run_blastplus against %s failed" % (db)
                return status
            elif megablastOutputFile == -143:
                log.warning("Blast overtime. Skip the search against %s.", db)
                status = "run_blastplus against %s timeout." % (db)
                log.info(status)
        else:
            log.info("Successfully ran blastn of reads against %s", db)
            log.info("Run blast " + str(contigs) + " sequentially against the refseq_db " + str(db) + " produced megablast_file " + str(megablastOutputFile))


    runtime = str(time() - startTime)
    log.info("Runtime serial for " + str(len(refDatabases)) + " dbs (except nt) was " + runtime)


    ## If fails then pipeline failure.
    stdOut, exitCode = illumina_contig_gc_plot_matplotlib(assemDir, log)

    if exitCode != 0:
        status = "illumina_contig_gc_plot failed"
        log.error("illumina_contig_gc_plot failure, exit code != 0")
        return status




    ## MEGAN. If fails then pipeline failure.
    log.info("\n\n%s - RUN MEGAN <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    meganDir = assemDir
    log.info("megan output dir: %s", str(meganDir))

    label = str(sequnitName)  ###safe_basename(formatted_fastq, log)
    log.info("megan label " + str(label))
    log.info("megan contigs " + str(contigs))
    log.info("megan megablast_file " + str(megablastOutputFile))

    if not skip_megan:
        meganCmd = RQCBlast.MEGAN_CMD + " -f " +  str(contigs) + " -b " + str(megablastOutputFile) + " -t megablast -w " + meganDir + " -l " + label
    else:
        meganCmd = "echo 'megan skipped'"

    stdOut, stdErr, exitCode = run_sh_command(meganCmd, True, log, True, timeoutSec=30)

    if exitCode in [124, 143]:
        ## Megan timeout
        ## Exit with succuss so that run_blastpl can be skipped.
        log.info("##################################")
        log.info("MEGAN TIMEOUT. JUST SKIP THE STEP.")
        log.info("##################################")
        status = "illumina_assemble_velvet complete"

    elif exitCode == 0:
        if skip_megan:
            status = "illumina_assemble_velvet complete"
            log.info("megan skipped")
        elif os.path.isfile(megablastOutputFile):
            status = "illumina_assemble_velvet complete"
            log.info("megan success: %s", megablastOutputFile)
        else:
            status = "illumina_assemble_velvet failed"
            log.error("megan failed to create %s", megablastOutputFile)
    else:
        status = "illumina_assemble_velvet failed"
        log.error("megan_obj.run_megan failure, exit code != 0")


    if status == "illumina_assemble_velvet failed":
        if stdOut:
            log.error("illumina_assemble_velvet failed, stdOut: %s", stdOut)
        if stdErr:
            log.error("illumina_assemble_velvet failed, stdErr: %s", stdErr)


    return status



"""
write_5_largest_contigs

 Title:        write_5_largest_contigs
 Function:     Given the contigs file, generate the text file with the 5 largest contigs
 Usage:        write_5_largest_contigs(contigs, log)
 Args:         1) The contigs.fa file absolute path
 Returns:      JGI_SUCCESS
               JGI_FAILURE
"""
def write_5_largest_contigs(contigsFile, log):
    log.info("\n\n%s - RUN write_top_5_largest_contigs <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    baseName, _ = safe_basename(contigsFile, log)
    outDir, _ = safe_dirname(contigsFile, log)

    contigsInfoDict = defaultdict(dict)
    sequence = ""
    idcontig = ""

    with open(contigsFile, 'r') as contigsFH:
        for line in contigsFH:
            line = line.strip()
            m = re.match("(>\S+)\_length\_\d+\_cov\_(\S+)", line)

            if line.startswith(">") and line.find("_length_") > -1 and line.find("_cov_") > -1 and len(m.groups()) > 1:

                idcontig = m.groups()[0]
                #cov = m.groups()[1]

                if sequence != "":
                    contigsInfoDict[idcontig]['len'] = len(sequence)
                    contigsInfoDict[idcontig]['seq'] = sequence
                    sequence = ""

            else:
                sequence += line

    contigsInfoDict[idcontig]['len'] = len(sequence)
    contigsInfoDict[idcontig]['seq'] = sequence

    output = os.path.join(str(outDir), "top5_" + str(baseName))
    log.info("output file: " + output)

    with open(output, 'w') as top5ContigsFH:
        sortedContigsInfoDict = sorted(contigsInfoDict.iteritems(), key=operator.itemgetter(1), reverse=True)

        ## print top 3 seqs
        numSeqs = 5
        i = 0

        for idConfig, key in sortedContigsInfoDict:
            i += 1
            top5ContigsFH.write(str(idConfig) + "_length_" + str(key['len']) + "\n" + str(key['seq']) + "\n")
            if i == numSeqs:
                break


    if not os.path.isfile(output) or i < 1:
        log.error("write_5_largest_contigs failed, did not print top 5 contigs i = %s", str(i))
        return "write_5_largest_contigs failed", RQCExitCodes.JGI_FAILURE

    log.info("5_largest_contigs output successfully generated: %s", output)

    ## record top5 contig file with 'assembly level top 5 assembly contigs file' and "assembly top 5 contig file"
    filesFile = RQCAssemblyQcConfig.CFG["files_file"]
    append_rqc_file(filesFile, "assembly level top 5 assembly contigs file", output, log)
    append_rqc_file(filesFile, "assembly top 5 contig file", output, log)

    return "write_5_largest_contigs success", RQCExitCodes.JGI_SUCCESS



"""
Same functionality as illumina_contig_gc_plot above, except this function uses matplotlib for making an html and png file plot.
"""
def illumina_contig_gc_plot_matplotlib(assemDir, log):
    log.info("\n\n%s - RUN contig_gc_plot_matplotlib <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    log.info("Making GC plot of contigs")

    gchistFileName = os.path.join(assemDir, "GC.contigs.fa.hist")

    if not os.path.isfile(gchistFileName):
        # log.error("Cannot locate contig GC text histogram. gnuplot contig GC plot not made: %s" % (gchistFileName))
        # return None, RQCExitCodes.JGI_FAILURE
        log.warning("Cannot locate contig GC text histogram. gnuplot contig GC plot not made: %s" % (gchistFileName))
        return None, RQCExitCodes.JGI_SUCCESS

    m = re.match("(\S+)\.velvet\.k.+", assemDir)

    gchistPngPlotFileName = os.path.join(assemDir, "GC.contigs.fa.hist.png")
    gchistHtmlPlotFileName = os.path.join(assemDir, "GC.contigs.fa.hist.html")

    ## NEW 09.21.2015 sulsj
    ##
    rawDataMatrix = []
    with open(gchistFileName, "r") as gcHistFH:
        for l in gcHistFH:
            m = re.match("^\|\w*\s+([\d\.]+)\s-\s([\d\.]+)\s:\s\[\s([\d]+)\s([\d\.e\+\-]+)\s([\d\.e\+\-]+)", l)
            if m:
                rawDataMatrix.append([float(m.groups()[0]), int(m.groups()[2])])

    rawDataMatrix = np.array(rawDataMatrix)


    fig, ax = plt.subplots()

    markerSize = 5.0
    lineWidth = 1.5

    p1 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 1], 'r', marker='o', markersize=markerSize, linewidth=lineWidth, alpha=0.5)

    ax.set_xlabel('%GC', fontsize=12, alpha=0.5)
    ax.set_ylabel('Contigs count', fontsize=12, alpha=0.5)
    ax.grid(color='gray', linestyle=':')

    ## Add tooltip
    mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p1[0], labels=list(rawDataMatrix[:, 1])))

    ## Save D3 interactive plot in html format
    mpld3.save_html(fig, gchistHtmlPlotFileName)

    ## Save Matplotlib plot in png format
    plt.savefig(gchistPngPlotFileName, dpi=fig.dpi)

    if os.path.isfile(gchistPngPlotFileName):
        log.info("GC plot of contigs successfully genearted: %s %s", gchistPngPlotFileName, gchistHtmlPlotFileName)
    else:
        log.warning("Failed to generate GC plot of contigs.")

    return "illumina_contig_gc_plot success", RQCExitCodes.JGI_SUCCESS



"""
read_length_to_kmer

 Title:        read_length_to_kmer
 Function:     Takes read length and returns nearest odd kmer value that's 80% of the read length
 Usage:        read_length_to_kmer(readLength, maxKmer)
 Args:         1) Read length.
               2) The maximum kmer size.
 Returns:      None.
 Comments:     If the calculated kmer value is greater than the max allowed kmer, it's
               set to the max allowed value.
"""
def read_length_to_kmer(readLen, maxKmer):
    log.info("\n\n%s - RUN read_length_to_kmer <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    ## take 80% of the read length, and take the nearest odd number => 2 x round( ( x -1 ) / 2 ) + 1
    readLen = readLen * 0.80
    kmer = (readLen - 1) / 2
    kmer = int(kmer + 0.5)
    kmer = (2 * kmer) + 1

    ## The max kmer length is a parameter to this function with default of 63.
    ## The max allowed kmer is 63 so set it to 63 if the returned value is greater.
    ## The max can also be user specified to any value, for example maybe the user chooses 71.
    if kmer > maxKmer:
        kmer = maxKmer


    return kmer



"""
run_blastplus_py

Call run_blastplus.py
"""
def run_blastplus_py(queryFastaFile, db, log):
    outDir, exitCode = safe_dirname(queryFastaFile, log)
    queryFastaFileBaseName, exitCode = safe_basename(queryFastaFile, log)
    dbFileBaseName, exitCode = safe_basename(db, log)

    # runBlastnCmd = "/global/homes/s/sulsj/work/bitbucket-repo/jgi-rqc-pipeline/tools/run_blastplus.py" ## debug
    # runBlastnCmd = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/run_blastplus.py"
    # runBlastnCmd = "/global/homes/s/sulsj/work/bitbucket-repo/jgi-rqc-pipeline/tools/run_blastplus_taxserver.py" ## debug
    runBlastnCmd = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/run_blastplus_taxserver.py"

    blastOutFileNamePrefix = outDir + "/megablast." + queryFastaFileBaseName + ".vs." + dbFileBaseName
    timeoutCmd = get_tool_path("timeout", "timeout")

    cmd = "%s 21600s %s -d %s -o %s -q %s -s > %s.log 2>&1 " % (timeoutCmd, runBlastnCmd, db, outDir, queryFastaFile, blastOutFileNamePrefix)

    log.debug("run_blastplus command: %s", cmd)
    _, _, exitCode = run_sh_command(cmd, True, log, True)

    ## Added timeout to terminate blast run manually after 6hrs
    ## If exitCode == 124 or exitCode = 143, this means the process exits with timeout.
    ## Timeout exits with 128 plus the signal number. 143 = 128 + 15 (SGITERM)
    ## Ref) http://stackoverflow.com/questions/4189136/waiting-for-a-command-to-return-in-a-bash-script
    ##      timeout man page ==> If the command times out, and --preserve-status is not set, then exit with status 124.
    ##
    if exitCode in (124, 143):
        ## BLAST timeout
        ## Exit with succuss so that the blast step can be skipped.
        log.info("##################################")
        log.info("BLAST TIMEOUT. JUST SKIP THE STEP.")
        log.info("##################################")
        return RQCExitCodes.JGI_FAILURE, -143

    elif exitCode != 0:
        log.error("failed to run_blastplus_py. Exit code != 0")
        return RQCExitCodes.JGI_FAILURE, None

    else:
        log.info("run_blastplus_py complete.")


    return RQCExitCodes.JGI_SUCCESS, blastOutFileNamePrefix


"""
STEP6 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

sketch_vs_nt_refseq_silva

input: original fastq file

"""
def sketch_vs_nt_refseq_silva(fastq, statusLogFile, log):
    log.info("\n\n%sRun sketch vs nt, refseq, silva <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    status = "sketch_vs_nt_refseq_silva in progress"
    checkpoint_step(statusLogFile, status)

    sketchOutDir = "sketch"
    sketchOutPath = os.path.join(outputPath, sketchOutDir)
    make_dir(sketchOutPath)
    change_mod(sketchOutPath, "0755")

    seqUnitName, exitCode = safe_basename(fastq, log)
    seqUnitName = file_name_trim(seqUnitName)

    filesFile = RQCAssemblyQcConfig.CFG["files_file"]

    ## sendsketch examples
    ## sendsketch.sh in=file.fa out=results.txt colors=f nt
    ## sendsketch.sh in=file.fa out=results.txt colors=f refseq
    ## sendsketch.sh in=file.fa out=results.txt colors=f silva

    comOptions = "ow=t colors=f printtaxa=t unique2"

    ## NT ##########################
    sketchOutFile = os.path.join(sketchOutPath, seqUnitName + ".sketch_vs_nt.txt")
    sendSketchShCmd = get_tool_path("sendsketch.sh", "bbtools")
    cmd = "%s in=%s out=%s %s nt" % (sendSketchShCmd, fastq, sketchOutFile, comOptions)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)
    if exitCode != 0:
        log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
        status = "sketch_vs_nt_refseq_silva failed"
        checkpoint_step(statusLogFile, status)
        return status

    append_rqc_file(filesFile, "sketch_vs_nt_output", sketchOutFile, log)

    ## Refseq ##########################
    sketchOutFile = os.path.join(sketchOutPath, seqUnitName + ".sketch_vs_refseq.txt")
    cmd = "%s in=%s out=%s %s refseq" % (sendSketchShCmd, fastq, sketchOutFile, comOptions)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)
    if exitCode != 0:
        log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
        status = "sketch_vs_refseq failed"
        checkpoint_step(statusLogFile, status)
        return status

    append_rqc_file(filesFile, "sketch_vs_refseq_output", sketchOutFile, log)

    ## Silva ##########################
    sketchOutFile = os.path.join(sketchOutPath, seqUnitName + ".sketch_vs_silva.txt")
    cmd = "%s in=%s out=%s %s silva" % (sendSketchShCmd, fastq, sketchOutFile, comOptions)
    stdOut, stdErr, exitCode = run_sh_command(cmd, True, log, True)
    if exitCode != 0:
        log.error("Failed to run : %s, stdout : %s, stderr: %s", cmd, stdOut, stdErr)
        status = "sketch_vs_silva failed"
        checkpoint_step(statusLogFile, status)
        return status

    append_rqc_file(filesFile, "sketch_vs_silva_output", sketchOutFile, log)

    status = "sketch_vs_nt_refseq_silva complete"
    log.info(status)
    checkpoint_step(statusLogFile, status)

    return status


'''===========================================================================
    file_name_trim

'''
def file_name_trim(fname):
    return fname.replace(".gz", "").replace(".fastq", "").replace(".fasta", "")



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program
if __name__ == "__main__":
    my_name = "RQC assembly-qc pipeline"

    startruntime = time()

    desc = 'assemblyqc'
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-b", "--skip-blast", dest="skipBlastFlag", action="store_true", help="Skip Blast search", default=False)
    parser.add_argument('-c', '--skip-cleanup', dest="skipCleanup", action='store_true', default=False, help="Skip temporary file cleanup")
    parser.add_argument("-f", "--fastq", dest="fastq", help="Set input Fastq file (full path to fastq)", required=True)
    # parser.add_argument('-g', '--skip-megan', dest="skipMegan", action='store_true', default=False, help = "Do not run Megan")
    parser.add_argument("-k", "--kmer", dest="kmer", help="Set k-mer size may be user-specified. Has precedence over the -m option. \
If -k is unspecified, then usually 80 percent of the read length will be used as kmer size, up to a maximum value as specified with \
-m if specified", required=False)
    parser.add_argument("-m", "--max-kmer", dest="maxKmer", help="Set maximum k-mer size. Only has utility if the kmer size is not \
specified with the -k option. If -k is unspecified but -m is specified, then 80 percent of the read length will be used as kmer size, \
up to a maximum value as specified with -m", required=False)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Set output path to write to", required=True)
    parser.add_argument("-r", "--max-reads", dest="maxReads", help="Set maximum number of reads to subsample", required=False)
    parser.add_argument("-t", "--num-threads", dest="numThreads", help="Set maximum number of threads to be used in a blast execution", required=False)
    parser.add_argument("-v", "--version", action="version", version=VERSION)

    options = parser.parse_args()

    ## output path = cwd if no output path

    outputPath = None
    # log_level = "INFO"
    fastq = None
    numThreads = 8
    skipBlastFlag = False

    if options.outputPath:
        outputPath = options.outputPath

    if options.fastq:
        fastq = options.fastq

    if options.numThreads:
        numThreads = options.numThreads

    if options.skipBlastFlag:
        skipBlastFlag = True

    skipCleanup = False
    if  options.skipCleanup:
        skipCleanup = True
        print "The files will not be cleaned up after run"

    # skipMegan = False
    # if  options.skipMegan:
    #     skipMegan = True
    skipMegan = True ## 12142015 skipping megan

    ## use current directory if no output path
    if not outputPath:
        outputPath = os.getcwd()

    ## create output_directory if it doesn't exist
    if not os.path.isdir(outputPath):
        os.makedirs(outputPath)

    ## initialize my logger
    log_file = os.path.join(outputPath, "assemblyqc.log")

    log = get_logger("assemblyqc", log_file, LOG_LEVEL, False, True)
    print "Started assemblyqc pipeline, writing log to: %s" % (log_file)

    log.info("=================================================================")
    log.info("   Assembly Qc Analysis (version %s)", VERSION)
    log.info("=================================================================")

    log.info("Starting %s: %s", my_name, fastq)

    ## Validate output dir and fastq

    ## create output_directory if it doesn't exist
    if not os.path.isdir(outputPath):
        log.info("Cannot find %s, creating as new", outputPath)
        os.makedirs(outputPath)

    if not os.path.isfile(fastq):
        log.error("Cannot find fastq: %s!", fastq)
        exit(2)

    ## Create the status log file location.
    statusLogFile = os.path.join(outputPath, "status.log")
    lastStatus = get_status(statusLogFile, log)

    ## Create the file log and stats log locations.
    rqc_file_log = os.path.join(outputPath, "rqc-files.tmp")
    rqc_stats_log = os.path.join(outputPath, "rqc-stats.tmp")

    RQCAssemblyQcConfig.CFG["files_file"] = rqc_file_log
    RQCAssemblyQcConfig.CFG["stats_file"] = rqc_stats_log
    RQCAssemblyQcConfig.CFG["status_file"] = statusLogFile
    RQCAssemblyQcConfig.CFG["output_path"] = outputPath

    cycle = 0

    if lastStatus == "complete":
        log.info("Status is complete, not processing.")
        exit(0)

    log.info("The fastq file is " + str(fastq))

    kmer = 0
    readLength = 0
    readLengthR1 = 0
    readLengthR2 = 0
    isPairedEnd = True

    (readLength, readLengthR1, readLengthR2, isPairedEnd) = get_working_read_length(fastq, log)

    if readLength == 0:
        ## Exit with failure
        status = "fastq file read length failed"
        log.info(status + " " + str(readLength) + " " + str(readLengthR1) + " " + str(isPairedEnd))
        checkpoint_step(statusLogFile, status)
        exit(1)


    ##
    ## STEP 1 - determine kmer length ------------------------------------------
    ##

    ## AssemblyQC is moving on, so determine the kmer size
    if options.kmer:
        kmer = options.kmer
    else:
        ## Maximum kmer may be used if it is specified.
        if options.maxKmer:
            maxKmer = options.maxKmer
        else:
            maxKmer = 63

        kmer = read_length_to_kmer(readLength, maxKmer) ## KMER_TEST

    log.info("kmer length: %s", str(kmer))


    ## Save the read_lengths and pairedness to the statistics
    if isPairedEnd:
        append_rqc_stats(rqc_stats_log, AssemblyStats.ILLUMINA_READ_LENGTH_1, readLengthR1)
        append_rqc_stats(rqc_stats_log, AssemblyStats.ILLUMINA_READ_LENGTH_2, readLengthR2)
    else:
        append_rqc_stats(rqc_stats_log, AssemblyStats.ILLUMINA_READ_LENGTH_1, readLength)


    ## If the previous run got stuck at the results gathering, then start from that point, no need to rerun the entire analysis.
    if lastStatus.startswith("illumina_assembly_level_report") or lastStatus.startswith("assembly_summary") or lastStatus == "illumina_assemble_velvet complete":
        #status = illumina_assembly_level_report(outputPath, statusLogFile, rqc_stats_log, rqc_file_log, kmer, readLengthR1, log)
        ## 12142015 Skip megan
        status = illumina_assembly_level_report(kmer, readLengthR1, log, skip_megan=skipMegan)

        if status.endswith("failed"): ## == "illumina_assemble_velvet failed":
            status = "illumina_assembly_level_report failed"
            exit(2)
        elif status == "illumina_assembly_level_report complete":
            status = "complete"

    else:
        status = "start"
        checkpoint_step(statusLogFile, status)

        if lastStatus.startswith("resubmit with more memory"):
            status = "start"
            log.info(status + " maxSubsampledReads not used. Resubmitting with more memory.")
            checkpoint_step(statusLogFile, status)

        ## Determine if subsampling should or should not be done.
        ## Also determine the max number of reads to be used.
        elif options.maxReads:
            maxSubsampledReads = options.maxReads
            if maxSubsampledReads < 1:
                status = "fastq file maxSubsampledReads failed"
                log.info(status + " " + str(maxSubsampledReads))
                checkpoint_step(statusLogFile, status)
                exit(2)
            else:
                status = "subsampling"
                log.info(status + " 'max-reads' reads from fastq " + str(maxSubsampledReads))
                checkpoint_step(statusLogFile, status)
        else:
            ## We have already computed the read_cnt, so even though no subsampling is done, save the number of reads used.
            ## maxSubsampledReads = read_cnt
            status = "start"
            log.info(status + " maxSubsampledReads not used")
            checkpoint_step(statusLogFile, status)


        ##
        ## STEP 2 - subsampling ------------------------------------------------
        ##

        if status == "subsampling":
            log.info("\n\n%s - RUN subsampling <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
            status, fastq = illumina_subsampling(fastq, outputPath, statusLogFile, maxSubsampledReads, isPairedEnd, log)
            assert fastq is not None, "Subsampling failed."
            log.info("After subsampling the destination fastq is " + str(fastq))

        if status.endswith("failed"): ## == "illumina_subsampling failed":
            exit(2)

        elif status == "start":
            ## Start the analysis
            ## TODO move inside pipeline so we donot have to count the reads before the pipeline
            ## Use the velvet output for the number of reads.
            #append_rqc_stats(rqc_stats_log, AssemblyStats.ILLUMINA_ASSEMBLY_READ_USED, maxSubsampledReads)

            log.info("The fastq file is " + str(fastq))

            ##
            ## STEP 3 - illumina_assemble_velvet -------------------------------
            ##   - velevth
            ##   - velvetg
            ##   - ill_velvet_stats
            ##   - TETRAMER
            ##   - write_5_largest_contigs
            ##   - blastn vs. refseq.archaea, refseq.bacteria, refseq.mitochondrion, refseq.plant...
            ##   - contig_gc_plot_matplotlib
            ##   - MEGAN
            ##

            status = illumina_assemble_velvet(kmer, fastq, outputPath, statusLogFile, rqc_stats_log, log, skip_megan=skipMegan)
            checkpoint_step(statusLogFile, status)

            if status.endswith("failed"): ### == "illumina_assemble_velvet failed":
                status = "illumina_assemble_velvet failed"
                checkpoint_step(statusLogFile, status)
                exit(2)

            elif status.endswith("success"):
                ## success means that a step did not fail but we cannot continue either, e.g. in case of 0 contigs.
                status = "complete"

            elif status == "illumina_assemble_velvet complete":
                ##
                ## STEP 4 - illumina_assembly_level_report ---------------------
                ##

                status = illumina_assembly_level_report(kmer, readLengthR1, log, skip_megan=skipMegan)

                if status.endswith("failed"): ### == "illumina_assemble_velvet failed":
                    status = "illumina_assembly_level_report failed"
                    exit(2)

                elif status == "illumina_assembly_level_report complete":
                    status = "complete"

    ##
    ## STEP 5 - Run sketch vs nt, refseq, silva ------------------------------------------------
    ##

    status = sketch_vs_nt_refseq_silva(fastq, statusLogFile, log)

    if status.endswith("failed"):
        status = "sketch_vs_nt_refseq_silva failed"
        checkpoint_step(statusLogFile, status)
        exit(2)

    elif status.endswith("success") or status.endswith("complete"):
        status = "complete"


    ##
    ## STEP 6 - post-processing ------------------------------------------------
    ##

    log.info("\n\n%s - RUN post processing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    ## Move the tmp files to txt files
    log.info("process_log_files: %s, %s", rqc_stats_log, rqc_file_log)

    if os.path.isfile(rqc_stats_log):
        rqcNewStatsLog = "%s/%s" % (outputPath, "rqc-stats.txt")

        cmd = "mv %s %s" % (rqc_stats_log, rqcNewStatsLog)
        log.info("cmd: %s", cmd)

        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)

        if exitCode > 0:
            status = "failed to move %s" % (rqc_stats_log)
            log.error(status)

    if os.path.isfile(rqc_file_log):
        rqc_new_file_log = "%s/%s" % (outputPath, "rqc-files.txt")

        cmd = "mv %s %s" % (rqc_file_log, rqc_new_file_log)
        log.info("cmd: %s", cmd)

        stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)

        if exitCode > 0:
            status = "failed to move %s" % (rqc_file_log)
            log.error(status)


    print "Status: " + status

    if not skipCleanup:
        cleanup_assemblyqc(outputPath, log)

    checkpoint_step(statusLogFile, status)

    if status.find("failed") > -1:
        checkpoint_step(statusLogFile, "failed")

    log.info("\n\nCompleted %s: %s", my_name, fastq)

    totalruntime = str(time() - startruntime)
    log.info("Total wallclock runtime: " + totalruntime)
    print "Total wallclock runtime: " + totalruntime


    exit(0)


## EOF
