#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Meta* Report
- creates pdf reports using templates and the RQC web service to merge into a .tex file and then create a pdf
- RQCSUPPORT-2041

v 1.0: 2018-02-14
- initial version

v 1.1: 2018-02-20
- added more debugging to log
- convert mg-summary to mg-sum.tex (RQCSUPPORT-2041)

v 1.1.1: 2018-02-27
- changed "MISSING" to "N/A" (RQCSUPPORT-2041)


~~~~~~~~~~~~~~~~~~~[ Test Cases ]

./meta_report.py -o t1 -pl

./meta_report.py \
-o /global/projectb/scratch/brycef/report/t1 \
-f /global/projectb/scratch/qc_user/rqc/prod/pipelines/metagenome/in-progress/02/96/81/87/rqc-stats.txt,/global/projectb/scratch/qc_user/rqc/prod/pipelines/metagenome/in-progress//02/96/81/87/rqc-files.txt \
-of /global/projectb/scratch/brycef/report/t1/bzzhy-mt.pdf \
-t mg-sum.tex -i BZZHY -pl

/global/projectb/scratch/brycef/git/jgi-rqc-pipeline/report/meta_report.py -t mg-summary.tex -i ACOSU,AUSNY -pl \
-f /global/projectb/sandbox/gaag/bfoster/metaG/combined/GAA-3486/rqc-stats.txt,/global/projectb/sandbox/gaag/bfoster/metaG/combined/GAA-3486/rqc-files.txt \
-of /global/projectb/scratch/brycef/report/ACOSU/rqc-stats.pdf \
-o /global/projectb/scratch/brycef/report/ACOSU

9289.1.128215.AGGCAGA-CTCTCTA.fastq.gz,10107.8.148890.CTTGTA.fastq.gz

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import re
import getpass
import json


# custom libs in "../lib/"
my_path = os.path.dirname(__file__)
sys.path.append(my_path)
sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, get_colors, run_cmd, get_msg_settings
from curl3 import Curl, CurlHttpException
#from rqc_report import get_page, do_pdf

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions

'''
Get list of files for a library and sets merge_dict values
/api/rqcws/lib2su/bzzhy
'''
def get_input_list(input_name):
    log.info("get_input_list: %s", input_name)
    #input_list = []
    spid_list = []
    sp_name_list = []
    proposal_id_list = []

    rqc_api = "/api/rqcws/lib2su/%s" % input_name
    log.info("- source: %s%s", RQC_URL, rqc_api)
    response = get_page(RQC_URL, rqc_api)

    log.info("- received %s bytes", len(str(response)))

    #print type(response) # comes back as tuple for some reason ...

    for su in response[0]:

        if 'seq_unit_name' in su:
            input_list.append(su['seq_unit_name'])
            spid_list.append(str(su['spid'])) # join only likes str
            sp_name_list.append(su['sp_name'])
            proposal_id_list.append(str(su['proposal_id']))

            log.info("- su: %s, spid: %s, sp_name: %s", su['seq_unit_name'], su['spid'], su['sp_name'])


    spid = ", ".join(list(set(spid_list)))
    sp_name = ", ".join(list(set(sp_name_list)))
    su_names = ", ".join(input_list)
    proposal_ids = ", ".join(list(set(proposal_id_list)))

    log.info("- spid: %s, sp_name: %s", spid, sp_name)

    report_metadata['library_info:seq_proj_id'] = spid
    report_metadata['library_info:seq_proj_name'] = sp_name
    report_metadata['seq_unit:seq_unit_name'] = su_names
    report_metadata['library_info:proposal_id'] = proposal_ids

    return



'''
Get web page
'''
def get_page(my_url, my_api):

    response = None
    exit_code = 0

    if debug_flag:
        log.info("%s%s/%s%s", color['cyan'], my_url, my_api, color[''])

    if my_url and my_api:

        curl = Curl(my_url)

        try:
            response = curl.get(my_api)

        except CurlHttpException as e:
            log.error("- Failed HTTPS Call: %s/%s, %s: %s", my_url, my_api, e.code, e.response)
            #code = e.code
            response = e.response
            exit_code = 10

        except Exception as e:
            log.error("- Failed API Call: %s/%s, %s", my_url, my_api, e.args)
            exit_code = 10


    if response:
        if debug_flag:
            log.info(color['green'])
            log.info(json.dumps(response, indent=4, sort_keys=True))
            log.info(color[''])
            #print

    return response, exit_code


'''
Create a pdf report from the latex file
- sometimes exits with exit code 1 but still creates a normal pdf
'''
def do_pdf(output_filename):
    log.info("do_pdf: %s", output_filename)

    pdf_file = output_filename.replace(".tex", ".pdf")

    # get rid of old one to make sure the new one is the right one
    if os.path.isfile(pdf_file):
        cmd = "rm %s" % (pdf_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    pdflatex_log = os.path.join(output_path, "pdflatex.log")
    cmd = "#texlive;pdflatex -interaction=nonstopmode -output-directory=%s %s > %s 2>&1" % (output_path, os.path.basename(output_filename), pdflatex_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(pdf_file):
        log.info("- created pdf report: %s  %s", pdf_file, msg_ok)
    else:
        log.error("- failed to make pdf report: %s  %s", pdf_file, msg_fail)



'''
characters have special meanings in latex, translate to correct values
'''
def trans_latex(my_string):

    the_string = str(my_string)
    # add \ to beginning
    trans_dict = {
        #"\\" : "textbackslash ", # chr(92)
        "$" : "$",
        "%" : "%",
        "_" : "_",
        "{" : "{",
        "}" : "}",
        "&" : "&",
        "#" : "#",
        "^" : "textasciicircum",
        "~" : "textasciitilde",
        "*" : "textasteriskcentered ",
        "/" : "slash ",
        "|" : "textbar ",
        "-" : "textendash ",
        ">" : "textgreater ",
        "<" : "textless ",
    }

    # first replace \, then replace everything else
    the_string = the_string.replace("\\", "%stextbackslash " % chr(92))

    for t in trans_dict:
        the_string = the_string.replace(t, "%s%s" % (chr(92), trans_dict[t]))

    return the_string


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "Meta* Reporter"
    version = "1.1.1"

    RQC_URL = "https://rqc.jgi-psf.org"

    uname = getpass.getuser() # get the user

    MISSING_VAL = "N/A"


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)

    rqc_report_cmd = "[my_path]/rqc_report.py"
    rqc_report_cmd = os.path.realpath(rqc_report_cmd.replace("[my_path]", my_path))




    # Parse options
    usage = "meta_report.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
to do
    """


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-of", "--output-file", dest="output_file", help = "Output filename (optional)")
    parser.add_argument("-f", "--file", dest="input_files", help = "csv list of files containing input data")
    parser.add_argument("-t", "--template", dest="template_name", help = "Name of reporting template (or path to template)")
    parser.add_argument("-i", "--input", dest="input_name", help = "input name (seq unit, library name) for rqcws/dump api lookup")
    parser.add_argument("-d", "--debug", dest="debug", default = False, action = "store_true", help = "extra output in the logs (e.g. show key = value for merging)")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)

    output_path = None
    input_name = None
    input_file_list = []
    template_name = "mg-sum.tex" # default
    su_template_name = "mg-seq-unit.tex" # seq unit template to combine em all
    output_file = None
    debug_flag = False
    print_log = False


    # parse args
    args = parser.parse_args()

    debug_flag = args.debug
    print_log = args.print_log

    if args.output_path:
        output_path = args.output_path


    if args.output_file:
        output_file = args.output_file

    if args.input_files:
        input_file_list = args.input_files.split(",")

    if args.input_name:
        input_name = args.input_name

    if args.template_name:
        template_name = args.template_name
        # RQCSUPPORT-2041 - force mg-sum or mt-sum
        if template_name.startswith("mg-sum"):
            template_name = "mg-sum.tex"
        if template_name.startswith("mt-sum"):
            tempalte_name = "mt-sum.tex"
            
        if template_name.startswith("mg-"):
            su_template_name = "mg-seq-unit.tex"
        elif template_name.startswith("mt-"):
            su_template_name = "mt-seq-unit.tex"

    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/report"

        if output_path == "t1":
            template_name = "mg-sum.tex"
            su_template_name = "mg-seq-unit.tex"
            input_name = "bzzhy"
            output_file = "bzzhy-mt.pdf"
            # note: rqc-files files point to scratch/in-progress so the files are not there unless you rsync them back
            # $ cd /global/projectb/scratch/qc_user/rqc/prod/pipelines/metagenome/in-progress/02/96/81/
            # $ mkdir 87
            # $ cd 87
            # $ rsync -av /global/dna/shared/rqc/pipelines/metagenome/archive/02/96/81/87/* .
            input_file_list = [ '/global/dna/shared/rqc/pipelines/metagenome/archive/02/96/81/87/rqc-stats.txt', '/global/dna/shared/rqc/pipelines/metagenome/archive/02/96/81/87/rqc-files.txt' ]

        output_path = os.path.join(test_path, output_path)


    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)



    if output_file:
        if not output_file.startswith("/"):
            output_file = os.path.join(output_path, output_file)

    else:
        output_file = "%s-%s" % (input_name, template_name)
        output_file = os.path.join(output_path, output_file)


    # initialize my logger
    log_file = os.path.join(output_path, "meta_report.log")



    # log = logging object
    log_level = "INFO"

    log = get_logger("meta_report", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "input_name", input_name)
    log.info("%25s      %s", "input_file_list", input_file_list)
    log.info("%25s      %s", "template_name", template_name)
    log.info("%25s      %s", "su_template_name", su_template_name)
    log.info("%25s      %s", "output_file", output_file)

    log.info("")



    if not input_name:
        log.error("missing input name!  %s", msg_fail)
        sys.exit(2)

    if not template_name:
        log.error("missing template name!  %s", msg_fail)
        sys.exit(2)

    input_list = []
    report_metadata = {} # key : value (spid: 1210210)

    
    
    get_input_list(input_name)

    
    # get list of input seq units
    output_file_list = [] # list of seq units to merge into our output_file

    for input_file in input_list:
        temp_output_file = input_file.replace(".fastq.gz", ".pdf")
        temp_output_file = os.path.join(output_path, temp_output_file)

        # expect to fail - there is no header
        cmd = "%s -t %s -i %s -np -of %s -o %s --no-pdf" % (rqc_report_cmd, su_template_name, input_file, temp_output_file, output_path)
        std_out, std_err, exit_code = run_cmd(cmd, log)

        temp_output_file = temp_output_file.replace(".pdf", ".tex")
        if os.path.isfile(temp_output_file):
            output_file_list.append(temp_output_file)
        else:
            log.error("- failed to make %s  %s", temp_output_file, msg_fail)
            sys.exit(2)


    # create intermediate assembly report, expect missing values

    temp_output_file = os.path.join(output_path, "%s.tex" % input_name)
    cmd = "%s -t %s -nm -of %s -f %s -o %s --no-pdf" % (rqc_report_cmd, template_name, temp_output_file, ",".join(input_file_list), output_path)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    #temp_output_file = temp_output_file.replace(".pdf", ".tex")
    if not os.path.isfile(temp_output_file):
        log.error("- failed to make %s  %s", temp_output_file, msg_fail)
        sys.exit(2)

    # format values
    for my_key in report_metadata:
        report_metadata[my_key] = trans_latex(report_metadata[my_key])


    # do merge - similar to rqc_report but simplier
    search = r"\__(.*?)__" # regex to look for all occurences of __key_name__ in the template

    tex_output_file = output_file.replace(".pdf", ".tex")
    log.info("- creating file tex: %s", tex_output_file)
    fhw = open(tex_output_file, "w")
    fh = open(temp_output_file, "r")

    for line in fh:
        arr = re.findall(search, line)
        new_line = line

        if len(arr) > 0:
            for merge_field in arr:
                my_key = merge_field
                my_val = MISSING_VAL


                if my_key in ("template:mg-seq-unit.tex", "", "template:mt-seq-unit.tex"):

                    for output_su in output_file_list:
                        log.info("- integrating file: %s", output_su)
                        fhi = open(output_su, "r")
                        for su_line in fhi:
                            fhw.write(su_line)
                        fhi.close()

                else:
                    # merge simple values
                    if my_key in report_metadata:
                        my_val = report_metadata[my_key]

                    my_val = str(my_val)
                    my_find = "__%s__" % merge_field

                    # re.sub replaces "\textendash " with "    endash " so we use replace
                    #new_line = re.sub(my_find, my_val, new_line)
                    new_line = new_line.replace(my_find, my_val)

                    fhw.write(new_line)
        else:
            fhw.write(new_line)


    fhw.close()
    fh.close()

    if output_file.endswith(".pdf"):
        do_pdf(tex_output_file)
        sys.exit(2)

    log.info("Completed %s", script_name)

    sys.exit(0)



"""

                      .-.
         .-._    _.../   `,    _.-.
         |   `'-'    \     \_'`   |
         \            '.__,/ `\_.--,
          /                '._/     |    Water, water everywhere
         /                    '.    /    And all the boards did shrink;
        ;   _                  _'--;
     '--|- (_)       __       (_) -|--'  Water, water everywhere
     .--|-          (__)          -|--.  Lets all take a drink!
      .-\-                        -/-.
     '   '.              o        .'  `   Rime of the Ancient Mariner/Homer Simpson - Boy-Scoutz 'n the Hood - episode 89
           '-._              _.-'
               `""--....--""`


"""
