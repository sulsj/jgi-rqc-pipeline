#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
RQC Report
- creates pdf reports using templates and the RQC web service to merge into a .tex file and then create a pdf
- can work with text files and other formats too
- replaces jgi-rqc-pipeline/tools/rqc_pdf_report.py (Bill 2014)
- sometimes pdflatex exits with exit code 1 but still produces a pdf file
- Alex invented using __key__ for his replace values in the 1990s

* Simon, May 10 2017

v 1.0: 2017-07-06
- initial version

v 1.1: 2017-07-07
- handle bmp files, metagenome reporting
- handle "missing" files from rqcws/dump api
- custom reporting functions ...

v 1.1.1: 2017-07-10
- exit(8) if anything missing

v 1.2: 2017-07-21
- import text file (key=value)
- import json file (flattens it)
- included sample.txt template for testing

v 1.2.1: 2017-07-26
- added debug feature to list keys
- convert *.bmp to *.png for pdf files

v 1.2.2: 2017-08-14
- unicode fix for library BXTTB

v 1.2.3: 2017-09-13
- updated to work with metatranscriptome pipeline


v 1.2.4: 2017-09-21
- don't translate image files (get_input_data)

v 1.2.5: 2017-12-01
- fix bug if using full path for template name, strange issue with ~ in second path

v 1.2.6: 2018-02-13
- fix bug if more than one seq unit in rqcdump  (RQC-1079)
- fixed cannot convert '5953.0' to int
- no purge flag for multiple-seq unit reports, missing replacement value flag (RQCSUPPORT-2041)

v 1.2.7: 2018-02-23
- fix for smRNA 20bp adapter in do_custom_metadata

v 1.2.8: 2018-02-27
- request to change "MISSING" to "N/A" (RQCSUPPORT-2041)

To do:
- print key=value for debugging to create templates ...
- bug - fails if more than one seq unit in a libary with rqcws/dump (BTYPS)
- single read qc report still has "Insert Size" plot - need to replace with a single read template or value


text -> latex safe text
|p# = percent, # digits precision, assumes value is 0.0 to 1.0
|i = format with ,'s
|f2 = float with 2 decimals
|f = float 0 decimals



~~~~~~~~~~~~~~~~~~~[ Test Cases ]


./rqc_report.py -o t1
1147040 - BWAON

cp /global/projectb/scratch/brycef/report/t1/bryce.pdf ~/gc/.

./rqc_report.py -o t2
BWOAH = 1x101 - test for __SKIP__Q20 Read Length, Read 2: & __stats:read_qc:read_q20_read2__ bp \\

./rqc_report.py -o t3
BWTTA


./rqc_report.py -f simple_data2.txt,simple_data.json -t simple.txt -of s.txt

./rqc_report.py -t mg-sum2.tex -i bgbgs \
-f /global/projectb/sandbox/gaag/bfoster/metaG/viral_metagenome/swift/1114564/rqc-stats.txt,/global/projectb/sandbox/gaag/bfoster/metaG/viral_metagenome/swift/1114564/rqc-files.txt \
-of mg-bgbgs.pdf \
-o /global/projectb/scratch/brycef/report/BGBGS

./rqc_report.py -i 11788.1.219580.GTTTCG.fastq.gz -o $S/report -of 11788.1.219580.GTTTCG.QC.pdf

metagenome:
./rqc_report.py -t mg-summary.tex -i bgbgs \
-f /global/projectb/sandbox/gaag/bfoster/metaG/viral_metagenome/swift/1114564/rqc-stats.txt,/global/projectb/sandbox/gaag/bfoster/metaG/viral_metagenome/swift/1114564/rqc-files.txt \
-of mg-bgbgs.pdf \
-o /global/projectb/scratch/brycef/report/BGBGS -pl

metatranscriptome:
./rqc_report.py -t mt-summary.tex -i bzhca \
-f /global/dna/shared/rqc/pipelines/metatranscriptome_aa/archive/02/96/62/00/rqc_stats.txt \
-of mt-bzhca.pdf \
-o /global/projectb/scratch/brycef/report/BZHCA -pl

BYWCT
./rqc_report.py -t mt-summary.tex -i bywct \
-of mt-bywct.pdf \
-f /global/dna/shared/rqc/pipelines/metatranscriptome_aa/archive/02/96/61/67/rqc_stats.txt \
-o /global/projectb/scratch/brycef/report/BYWCT -pl

2017-12-07: newest bbtools docker image does not have seqsplit latex library, commented out in mt-summary.txt


/global/projectb/scratch/brycef/git/jgi-rqc-pipeline/report/rqc_report.py -pl \
-i 12187.1.241803.GCGATAG-ACTATCG.fastq.gz \
-o /global/projectb/scratch/brycef/report/t4 \
-of 12187.1.241803.GCGATAG-ACTATCG.QC.pdf

"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use

import os
import sys
from argparse import ArgumentParser
import re
import getpass
import time
import json
import glob

# custom libs in "../lib/"
my_path = os.path.dirname(__file__)

sys.path.append(os.path.join(my_path, '../lib'))

from common import get_logger, get_colors, run_cmd, get_msg_settings
from curl3 import Curl, CurlHttpException

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## function definitions
'''
https://medium.com/@amirziai/flattening-json-objects-in-python-f5343c794b10
flatten a json structure into key = value
"Iron Maiden" : { "Vocals" : "Bruce Dickinson", "Bass Guitar" : "Steve Harris", "Lead Guitar" : ["Dave Murrary", "Adrian Smith"] },
becomes
Iron_Maiden:Bass_Guitar = Steve Harris
Iron_Maiden:Lead_Guitar:1 = Adrian Smith
Iron_Maiden:Lead_Guitar:0 = Dave Murrary
Iron_Maiden:Vocals = Bruce Dickinson

'''
def flatten_json(input_json):
    flat_json = {}

    def flatten(x, the_name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], the_name + a + ':')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, the_name + str(i) + ':')
                i += 1
        else:
            the_name = filter_key(the_name)
            flat_json[the_name[:-1]] = x

    flatten(input_json)
    return flat_json


'''
Import a json file

2017-12-01
 ./rqc_report.py -f ~/git/jgi-rqc-pipeline/report/simple_data2.txt,~/git/jgi-rqc-pipeline/report/simple_data.json -t ~/git/jgi-rqc-pipeline/report/templates/simple.txt -of out.txt
 - second file not found, python not translating ~/ properly
'''
def get_json_data(input_file):
    log.info("get_json_data: %s", input_file)

    #print "if: '%s'" % os.path.realpath(input_file)
    #input_file = os.path.realpath(input_file.strip())
    #print input_file, os.path.realpath(input_file)
    #print type(input_file)

    if os.path.isfile(input_file):

        with open(input_file) as json_data:
            my_json = json.load(json_data)

        #print my_json

        flat_json = flatten_json(my_json)
        #for j in flat_json:
        #    print "%s = %s" % (j, flat_json[j])
        #sys.exit(44)

        key_cnt = 0
        for j in flat_json:

            my_key = j
            my_val = flat_json[j]

            report_metadata[my_key] = my_val
            key_cnt += 1

        log.info("- imported %s keys", key_cnt)

    else:
        print "file missing: %s" % input_file
        log.error("- input file missing: %s!  %s", input_file, msg_fail)
        sys.exit(2)


'''
Read data from a file
format:
# comment line
# blank lines are skipped

key1 = value1
key2 = value2=3
key3 =  *
key1 = value4 **

* this key will not be added
** key1 will only contain "value4", not "value1"

if the value ends with .bmp then try to convert to png

'''
def get_input_data(input_file):
    log.info("get_input_data: %s", input_file)

    if os.path.isfile(input_file):
        key_cnt = 0

        fh = open(input_file, "r")
        for line in fh:
            if line.startswith("#"):
                continue


            my_list = line.split("=", 1) # split on first occurrence only

            my_key = ""
            my_val = ""
            if len(my_list) == 2:
                my_key = str(my_list[0]).strip()
                my_val = str(my_list[1]).strip()
                key_cnt += 1

                my_key = filter_key(my_key)
                if my_val.endswith(".png") or my_val.endswith(".bmp") or my_val.endswith(".jpg"):
                    # dont translate images to .tex
                    pass
                else:
                    my_val = trans_latex(my_val)

                report_metadata[my_key] = my_val
        fh.close()

        log.info("- loaded %s key=value pairs", key_cnt)

    else:
        log.error("- input file missing: %s!  %s", input_file, msg_fail)
        sys.exit(2)

def filter_key(the_key):

    new_key = re.sub(r'[^A-Za-z0-9\-\+\=\_\/\.\,\$\%\:\;\{\}\[\]]+', '_', the_key)
    return new_key



'''
Return the metadata from RQC api: api/rqcws/dump

'''
def get_rqc_dump_metadata(input_name):
    log.info("get_rqc_dump_metadata: %s", input_name)

    SHARED_PATH = "/global/dna/shared/rqc/"
    zip_dict = {} # flag if parent folder is unzipped

    # use rqc data dump service ...
    if 1==1:

        rqc_api = "/api/rqcws/dump/%s" % input_name
        log.info("- source: %s%s", RQC_URL, rqc_api)
        response = get_page(RQC_URL, rqc_api)

        log.info("- received %s bytes", len(str(response)))


        # flatten the metadata
        for data_type in ['files','library_info','seq_unit','stats']:
            if data_type in response[0]:
                for my_field in response[0][data_type]:
                    #print data_type, my_field

                    if type(response[0][data_type]) == list:
                        log.info("-- %s is a list", data_type)
                        continue

                    my_val = response[0][data_type][my_field]

                    my_key = my_field

                    if data_type == "files":


                        if my_val.lower().startswith("missing"):

                            # find the zip file
                            # unzip the file to a "tmp" folder
                            # change my_val to point to tmp folder
                            # purge folder later

                            my_val = my_val.replace("missing:", "")
                            if str(my_val).startswith("$RQC_SHARED"):
                                my_val = my_val.replace("$RQC_SHARED", SHARED_PATH)

                            #print my_val
                            #print os.path.dirname(my_val)

                            # check if we've unzipped this already
                            this_path = os.path.dirname(my_val)
                            if this_path not in zip_dict:
                                zip_list = glob.glob(os.path.join(this_path, "*.zip"))
                                zip_file = ""
                                for z in zip_list:
                                    zip_file = z

                                if zip_file:
                                    cmd = "unzip -o %s -d %s" % (zip_file, tmp_path)
                                    # -d = unzip to this path
                                    # -o = don't ask to overwrite, just do it
                                    std_out, std_err, exit_code = run_cmd(cmd, log)

                                    zip_dict[this_path] = "unzipped"

                            new_file = os.path.join(tmp_path, os.path.basename(my_val))
                            if os.path.isfile(new_file):
                                my_val = new_file


                        elif str(my_val).startswith("$RQC_SHARED"):
                            my_val = my_val.replace("$RQC_SHARED", SHARED_PATH)


                    if latex_flag:
                        if data_type == "files":
                            pass

                        else:

                            # BXTTB
                            my_val = unicode(my_val).encode('utf-8')

                            my_val = trans_latex(my_val)


                    new_key = "%s:%s" % (data_type, my_key)
                    new_key = new_key.lower()
                    report_metadata[new_key] = my_val



    #print report_metadata['stats:microbe_iso:scaffolds_n_contigs'] # 34
    #print report_metadata['files:microbe_iso:iso_bwaon_txt']
    #print report_metadata['library_info:library_created_dt']


'''
Convert bmp files to png files
- only for pdf files

remove .png from .png files, template needs to be:
{{__files:read_qc:read_gc_plot__}.png}
'''
def fix_images():

    log.info("fix_images")

    for my_key in report_metadata:


        if str(report_metadata[my_key]).endswith(".bmp"):
            # convert to png in output folder ...
            if os.path.isfile(report_metadata[my_key]):
                png_file = os.path.join(output_path, os.path.basename(report_metadata[my_key]).replace(".bmp", ".png"))
                cmd = "convert %s %s" % (report_metadata[my_key], png_file)
                std_out, std_err, exit_code = run_cmd(cmd, log)

                #./rqc_report.py -t mg-summary.tex -i bgbgs -f /global/projectb/sandbox/gaag/bfoster/metaG/viral_metagenome/swift/1114564/rqc-stats.txt,/global/projectb/sandbox/gaag/bfoster/metaG/viral_metagenome/swift/1114564/rqc-files.txt -of mg-bgbgs.pdf -o /global/projectb/scratch/brycef/report/BGBGS
                report_metadata[my_key] = png_file


        # might need to handle .gif, .jpg too? No, latex okay with those
        if str(report_metadata[my_key]).endswith(".png"):
            report_metadata[my_key] = report_metadata[my_key].replace(".png", "")
            log.info("* %s", report_metadata[my_key])



'''
Read template file
ignore lines starting with "#" (comment lines)

'''
def get_template_data(template_name):

    log.info("get_template_data: %s", template_name)

    template_file = None

    if template_name.startswith("/"):
        template_file = template_name
    else:
        template_file = os.path.realpath(os.path.join(my_path, "templates", template_name))

    if not os.path.isfile(template_file):
        log.error("missing template file: %s  %s", template_file, msg_fail)
        sys.exit(2)

    template = "" # contents of the file

    fh = open(template_file, "r")
    for line in fh:
        if line.strip().startswith("#"):
            continue
        template += line
    fh.close()

    log.info("- loaded %s bytes", len(template))

    return template


'''
Constants that can be used in the templates as __keys__
'''
def add_metadata_constants():

    report_metadata['timestamp'] = time.strftime("%m/%d/%Y %H:%M:%S") # 7/06/2017 10:56:31
    report_metadata['jgi_url'] = "http://jgi.doe.gov"

    # images in report/images/...
    report_metadata['jgi_stacked_logo'] = os.path.realpath(os.path.join(my_path, "images/JGI_logo_stacked_DOEtag_RGB.jpg"))
    report_metadata['jgi_logo'] = os.path.realpath(os.path.join(my_path, "images/jgi_logo.jpg"))


'''
Update report_metadata with values for some reports
- this isn't a good idea, the pipelines/data source should have this data
'''
def do_custom_metadata():

    report_metadata['stats:metagenome:custom_read_mapped_pct'] = "Not avail" # 0.0 -> 100.0
    report_metadata['stats:metagenome:custom_read_mapped_10kb_pct'] = "Not avail" # 0.0 -> 100.0

    # smrna
    if 'stats:read_qc:illumina_read_percent_contamination_artifact_20bp_seal' in report_metadata:
        report_metadata['stats:read_qc:illumina_read_percent_contamination_artifact_50bp_seal'] = report_metadata['stats:read_qc:illumina_read_percent_contamination_artifact_20bp_seal']

    # tmp fix for RQC-1084
    if 'stats:read_qc:read_q20_read2' in report_metadata and 'stats:read_qc:read_q20_read1' not in report_metadata:
        report_metadata['stats:read_qc:read_q20_read1'] = "0"

    # for megahit ...
    if 'stats:metagenome:outputs_assembly_assembler_parameters' in report_metadata:
        arr = report_metadata['stats:metagenome:outputs_assembly_assembler_parameters'].split()

        report_metadata['stats:metagenome:custom_kmer_list'] = arr[4]

    if 'stats:metatranscriptome_aa:outputs_metatranscriptome_assembly_assembler_parameters' in report_metadata:
        arr = report_metadata['stats:metatranscriptome_aa:outputs_metatranscriptome_assembly_assembler_parameters'].split()

        report_metadata['stats:metatranscriptome:custom_kmer_list'] = arr[4]

    #if 'stats:filter:subsample_rate' in report_metadata:
    #
    #    try:
    #        report_metadata['stats:filter:subsample_pct'] = float(report_metadata['stats:filter:subsample_rate']) * 100.0
    #    except:
    #        report_metadata['stats:filter:subsample_pct'] = 1.0 # normally 1% by default

    if 'stats:metatranscriptome_aa:outputs_metatranscriptome_alignment_num_aligned_reads' in report_metadata:

        report_metadata['stats:metatranscriptome:aligned_pct'] = float(report_metadata['stats:metatranscriptome_aa:outputs_metatranscriptome_alignment_num_aligned_reads']) / \
                                                                 float(report_metadata['stats:metatranscriptome_aa:outputs_metatranscriptome_alignment_num_input_reads']) * 100
        report_metadata['stats:metatranscriptome:custom_read_mapped_pct'] = "Not avail"

    if 'outputs.metatranscriptome_alignment.num_input_reads' in report_metadata:
        report_metadata['custom_mtaa:aligned_pct'] = float(report_metadata['outputs.metatranscriptome_alignment.num_aligned_reads']) / \
                                                    float(report_metadata['outputs.metatranscriptome_alignment.num_input_reads'])

    # need to split on MEGAHIT_TMP_DIR
    if 'outputs.metatranscriptome_assembly.assembler_parameters' in report_metadata:
        #print report_metadata['outputs.metatranscriptome_assembly.assembler_parameters']
        if "MEGAHIT" in report_metadata['outputs.metatranscriptome_assembly.assembler_parameters']:
            report_metadata['outputs.metatranscriptome_assembly.assembler_parameters'] = str(report_metadata['outputs.metatranscriptome_assembly.assembler_parameters']).replace("MEGAHIT", "\\\\ &MEGAHIT")
            #print report_metadata['outputs.metatranscriptome_assembly.assembler_parameters']


'''
Get all the keys, look up values, format values, report missing values
'''
def merge_template(template_name, input_name, output_file):
    log.info("merge_template: %s-%s", input_name, template_name)


    fhw = open(output_file, "w")

    search = r"\__(.*?)__" # regex to look for all occurences of __key_name__ in the template

    missing_list = [] # list of keys with missing values

    for line in template.split("\n"):


        # if line starts with __SKIP__ then don't include it if any field is MISSING
        optional_flag = False
        if line.strip().startswith("__SKIP__"):
            optional_flag = True
            line = line.replace("__SKIP__", "")

        arr = re.findall(search, line)
        new_line = line

        if len(arr) > 0:
            for merge_field in arr:
                my_key = merge_field
                my_val = MISSING_VAL
                format_type = None

                # look for formatting on the end of a key, e.g. seq_unit:raw_reads_count|i = format this field as int with thousands separator
                arr2 = my_key.split("|")
                if len(arr2) > 1:
                    format_type = arr2.pop() # pop off last element
                    my_key = "".join(arr2) # my key = put everything else together like before (e.g.  find_this|key|here|i -> find_this|key|here )

                if my_key in report_metadata:
                    my_val = report_metadata[my_key]
                else:
                    if not optional_flag:
                        missing_list.append(merge_field)

                # try to format the numbers
                if format_type:
                    my_val = format_val(my_val, format_type)

                my_val = str(my_val)
                my_find = "__%s__" % merge_field

                # re.sub replaces "\textendash " with "    endash " so we use replace
                #new_line = re.sub(my_find, my_val, new_line)

                if my_val == MISSING_VAL:
                    if missing_flag:
                        new_line = new_line.replace(my_find, my_val)
                else:
                    new_line = new_line.replace(my_find, my_val)


                #print my_key, my_val

        #print new_line
        if optional_flag:
            #print new_line
            if "MISSING" in new_line:
                log.info("- line has missing values, not including in report  %s", msg_warn)
            else:
                fhw.write(new_line + "\n")
        else:
            fhw.write(new_line + "\n")


    fhw.close()

    if os.path.isfile(output_file):
        log.info("- created: %s  %s", output_file, msg_ok)
    else:
        log.info("- failed to create: %s  %s", output_file, msg_fail)
        sys.exit(2)

    if len(missing_list) > 0:
        log.error("- %s fields missing  %s", len(missing_list), msg_fail)
        for missing_key in missing_list:
            log.info("-- missing key: %s  %s", missing_key, msg_warn)
        # missing might be okay, like 1x100 runs
        log.error("- exiting, cannot create report")
        sys.exit(8)
    else:
        log.info("- no missing keys in template  %s", msg_ok)


'''
Create a pdf report from the latex file
- sometimes exits with exit code 1 but still creates a normal pdf
'''
def do_pdf(output_filename):
    log.info("do_pdf: %s", output_filename)

    pdf_file = output_filename.replace(".tex", ".pdf")

    # get rid of old one to make sure the new one is the right one
    if os.path.isfile(pdf_file):
        cmd = "rm %s" % (pdf_file)
        std_out, std_err, exit_code = run_cmd(cmd, log)

    pdflatex_log = os.path.join(output_path, "pdflatex.log")
    cmd = "#texlive;pdflatex -interaction=nonstopmode -output-directory=%s %s > %s 2>&1" % (output_path, os.path.basename(output_filename), pdflatex_log)
    std_out, std_err, exit_code = run_cmd(cmd, log)

    if os.path.isfile(pdf_file):
        log.info("- created pdf report: %s  %s", pdf_file, msg_ok)
    else:
        log.error("- failed to make pdf report: %s  %s", pdf_file, msg_fail)



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ helper functions

'''
Format values
i = int, comma separated thousands
f# = float, # digits precision, comma separated thousands
p = percent, # digits precision, assumes value is 0.0 to 1.0
'''
def format_val(my_val, format_type):

    my_new_val = my_val
    err_cnt = 0

    my_test_val = str(my_val).replace("-","").replace(".","")

    if format_type == "i":
        if my_test_val.isdigit():
            try:
                my_new_val = "{:,}".format(int(float(my_val)))
            except:
                err_cnt += 1
        else:
            err_cnt += 1

    elif format_type.startswith("f"):
        p = format_type.replace("f", "") # 2 = 2 decimal places
        if not p:
            p = "0"
        format_style = "{0:,.%sf}" % (p)

        try:
            my_new_val = format_style.format(float(my_val))
        except:
            err_cnt += 1


    elif format_type.startswith("p"):
        p = format_type.replace("p", "") # 2 = 2 decimal places
        if not p:
            p = "0"
        format_style = "{0:.%s%%}" % (p)
        #format_style = "{0:,.%sf}" % (p)

        try:
            my_new_val = format_style.format(float(my_val)) #+ "\\%"
            my_new_val = my_new_val.replace("%", "\\%")
        except:
            err_cnt += 1

    if err_cnt > 0:
        log.error("- cannot convert '%s' using %s  %s", my_val, format_type, msg_warn)

    return my_new_val

'''
characters have special meanings in latex, translate to correct values
'''
def trans_latex(my_string):

    the_string = str(my_string)
    # add \ to beginning
    trans_dict = {
        #"\\" : "textbackslash ", # chr(92)
        "$" : "$",
        "%" : "%",
        "_" : "_",
        "{" : "{",
        "}" : "}",
        "&" : "&",
        "#" : "#",
        "^" : "textasciicircum",
        "~" : "textasciitilde",
        "*" : "textasteriskcentered ",
        "/" : "slash ",
        "|" : "textbar ",
        "-" : "textendash ",
        ">" : "textgreater ",
        "<" : "textless ",
    }

    # first replace \, then replace everything else
    the_string = the_string.replace("\\", "%stextbackslash " % chr(92))

    for t in trans_dict:
        the_string = the_string.replace(t, "%s%s" % (chr(92), trans_dict[t]))

    return the_string


'''
Get web page
'''
def get_page(my_url, my_api):

    response = None
    exit_code = 0

    if debug_flag:
        log.info("%s%s/%s%s", color['cyan'], my_url, my_api, color[''])

    if my_url and my_api:

        curl = Curl(my_url)

        try:
            response = curl.get(my_api)

        except CurlHttpException as e:
            log.error("- Failed HTTPS Call: %s/%s, %s: %s", my_url, my_api, e.code, e.response)
            #code = e.code
            response = e.response
            exit_code = 10

        except Exception as e:
            log.error("- Failed API Call: %s/%s, %s", my_url, my_api, e.args)
            exit_code = 10


    if response:
        if debug_flag:
            log.info(color['green'])
            log.info(json.dumps(response, indent=4, sort_keys=True))
            log.info(color[''])
            #print

    return response, exit_code

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

script_name = __file__

if __name__ == "__main__":


    my_name = "RQC Reporter"
    version = "1.2.8"

    RQC_URL = "https://rqc.jgi-psf.org"

    uname = getpass.getuser() # get the user

    MISSING_VAL = "N/A"


    color = get_colors()
    msg_ok, msg_fail, msg_warn = get_msg_settings(color)


    # Parse options
    usage = "rqc_report.py [options]\n"
    usage += "* %s, version %s\n" % (my_name, version)
    usage += """
Creates reports based on a template, if the report template ends with .tex then it makes a pdf.

To create a report using the qc.report.tex template writing to the t2 folder for library bxbwz
and creating a file called bxbwz.pdf in the output folder:
$ ./rqc_report.py -t qc.report.tex -o /global/projectb/scratch/brycef/report/t2 -i bxbwz -of BXBWZ.pdf

$ ./rqc_report.py -t qc.report.tex -o /global/projectb/scratch/brycef/report/t2 -of 11752.UNK.pdf -i 11752.1.216713.UNKNOWN.fastq.gz
    """


    parser = ArgumentParser(usage = usage)

    parser.add_argument("-o", "--output-path", dest="output_path", help = "Output path to write to, uses pwd if not set")
    parser.add_argument("-of", "--output-file", dest="output_file", help = "Output filename (optional)")
    parser.add_argument("-f", "--file", dest="input_files", help = "csv list of files containing input data")
    parser.add_argument("-t", "--template", dest="template_name", help = "Name of reporting template (or path to template)")
    parser.add_argument("-i", "--input", dest="input_name", help = "input name (seq unit, library name) for rqcws/dump api lookup")
    parser.add_argument("-d", "--debug", dest="debug", default = False, action = "store_true", help = "extra output in the logs (e.g. show key = value for merging)")
    parser.add_argument("-np", "--no-purge", dest="no_purge", default = True, action = "store_false", help = "purge temp files (default to purge files)")
    parser.add_argument("-nm", "--no-missing", dest="no_missing", default = True, action = "store_false", help = "do not replace missing value with '%s'" % MISSING_VAL)
    parser.add_argument("-npdf", "--no-pdf", dest="no_pdf", default = True, action = "store_false", help = "do not render pdf file")
    parser.add_argument("-pl", "--print-log", dest="print_log", default = False, action = "store_true", help = "print log to screen")
    parser.add_argument("-v", "--version", action="version", version=version)

    output_path = None
    input_name = None
    input_file_list = []
    template_name = "qc.report.tex" # default (used in rqc pipelines)
    output_file = None
    debug_flag = False
    purge_flag = True
    print_log = False
    missing_flag = True # False = don't replace value with MISSING_VAL
    make_pdf_flag = True # False = don't make pdf - used with meta_report.py

    # parse args
    args = parser.parse_args()

    debug_flag = args.debug
    print_log = args.print_log
    purge_flag = args.no_purge
    missing_flag = args.no_missing
    make_pdf_flag = args.no_pdf

    if args.output_path:
        output_path = args.output_path


    if args.output_file:
        output_file = args.output_file

    if args.input_files:
        input_file_list = args.input_files.split(",")

    if args.input_name:
        input_name = args.input_name

    if args.template_name:
        template_name = args.template_name

    # testing (-o t1, -o t2)
    if output_path and output_path.startswith("t"):
        test_path = "/global/projectb/scratch/brycef/report"

        if output_path == "t1":
            #template_name = "qc.report.tex"
            input_name = "bwaon"
            output_file = "bryce.pdf"

        elif output_path == "t2":
            #template_name = "qc.report.tex"
            input_name = "bwoah"
            output_file = "bwoah.pdf"

        elif output_path == 't3':
            input_name = "BWTTA"
            template_name = "mg-sum.report.tex"
            output_file = "11721.8.216484.TTGTCGG-ACCGACA.mg.pdf"


        elif output_path == 't4':
            input_name = "BHOZN" # purged
            template_name = "mg-sum.report.tex"
            output_file = "BHOZN.mg.pdf"

        elif output_path == 't5':
            input_name = "BTYPS" # 2 seq units, causes problems with combining data ...
            input_name = "11706.1.215706.AGTCTCA-GTGAGAC.fastq.gz"
            template_name = "mt-sum.report.tex"
            output_file = "BTYPS.mt.pdf"

        output_path = os.path.join(test_path, output_path)


    # use current directory if no output path
    if not output_path:
        output_path = os.getcwd()


    # create output_directory if it doesn't exist
    if not os.path.isdir(output_path):
        os.makedirs(output_path)



    if output_file:
        if not output_file.startswith("/"):
            output_file = os.path.join(output_path, output_file)

    else:
        output_file = "%s-%s" % (input_name, template_name)
        output_file = os.path.join(output_path, output_file)


    # initialize my logger
    log_file = os.path.join(output_path, "rqc_report.log")



    # log = logging object
    log_level = "INFO"

    log = get_logger("rqc_report", log_file, log_level, print_log)

    log.info("%s", 80 * "~")
    log.info("Starting %s (%s %s)", script_name, my_name, version)

    log.info("")

    log.info("Run settings:")
    log.info("%25s      %s", "output_path", output_path)
    log.info("%25s      %s", "input_name", input_name)
    log.info("%25s      %s", "input_file_list", input_file_list)
    log.info("%25s      %s", "template_name", template_name)
    log.info("%25s      %s", "output_file", output_file)
    log.info("%25s      %s", "missing_flag", missing_flag)
    log.info("%25s      %s", "make_pdf_flag", make_pdf_flag)
    log.info("%25s      %s", "purge_flag", purge_flag)
    log.info("")



    #if not input_name:
    #    log.error("missing input name!  %s", msg_fail)
    #    sys.exit(2)

    if not template_name:
        log.error("missing template name!  %s", msg_fail)
        sys.exit(2)


    # special pdf handling ...
    latex_flag = False
    if template_name.endswith(".tex"):
        latex_flag = True

    # only needed to unzip "missing" files
    tmp_path = os.path.join(output_path, "tmp")
    if not os.path.isdir(tmp_path):
        os.makedirs(tmp_path)


    # get report template
    report_metadata = {}


    # pull the template file into memory
    template = get_template_data(template_name)

    # setup metadata
    if input_name:
        get_rqc_dump_metadata(input_name)

    # load data from each file
    for input_file in input_file_list:
        if input_file.endswith(".json"):
            get_json_data(input_file)
        else:
            get_input_data(input_file)


    # add timestamp, etc
    add_metadata_constants()

    # custom calcs
    do_custom_metadata()

    # debug - dump metadata ...
    if debug_flag:
        log.info("key dump:")
        for my_key in report_metadata:
            log.info("--- '%s' = '%s'", my_key, report_metadata[my_key])
    #print report_metadata['jgi_logo']
    # not supporting nested json to merge with a template, e.g.  replace __victim.of.changes__ with report_metadata['victim']['of']['changes']


    output_file = output_file.replace(".pdf", ".tex") # first make .tex file if they want a pdf

    if template_name.endswith(".tex"):
        fix_images()

    merge_template(template_name, input_name, output_file)


    if template_name.endswith(".tex"):
        if make_pdf_flag:
            do_pdf(output_file)
    else:
        log.info("- created report: %s  %s", output_file, msg_ok)


    if purge_flag:
        cmd = "rm -rf %s" % tmp_path
        std_out, std_err, exit_code = run_cmd(cmd, log)


    log.info("Completed %s", script_name)

    sys.exit(0)



"""

                      .-.
         .-._    _.../   `,    _.-.
         |   `'-'    \     \_'`   |     Frodo Baggins: I wish the ring had never come to me,
         \            '.__,/ `\_.--,                   I wish none of this had happened.
          /                '._/     |
         /                    '.    /
        ;   _                  _'--;     Gandalf: So do all who live to see such times,
     '--|- (_)       __       (_) -|--'           but that is not for us to decide.
     .--|-          (__)          -|--.           All we have to decide is what to do with the time that's been given us.
      .-\-                        -/-.
     '   '.                       .'  `
           '-._              _.-'
               `""--....--""`


"""
