#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Blastn on-demand

Calls run_blastplus.py and collect output files

Command
    $ blast_ondemand.py -q <query_file> -d <ref> -o <output_dir> [-s <num_reads_to_sample>]


Created: Sep 21 2017

sulsj (ssul@lbl.gov)


Revisions:

    09212017 0.0.9: init
    10262017 1.0.0: Added "all" option to run it against all default reference databases
    06112018 1.0.1: Removed nr



"""

import os
import sys
import time
import argparse

## custom libs in "../lib/"
ROOT = os.path.dirname(__file__)
sys.path.append(os.path.join(ROOT, '../lib'))    ## rqc-pipeline/lib
sys.path.append(os.path.join(ROOT, '../tools'))  ## rqc-pipeline/tools

from common import get_run_path, get_logger, get_status, append_rqc_stats, append_rqc_file
from rqc_utility import *
from rqc_constants import *
from db_access import *
from os_utility import make_dir, change_mod, run_sh_command, make_dir_p


SCRIPT_NAME = __file__
VERSION = "1.0.1"
CFG = {}
#LOG_LEVEL = "INFO"
LOG_LEVEL = "DEBUG"
SUCCESS = 0
FAILURE = -1


"""
STEP 1 Pre-processing ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This step is reserved for future needs.

"""
def do_preprocessing(query, log):
    log.info("\n\nSTEP1 - Preprocessing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n")
    status = "1_preprocessing in progress"
    checkpoint_step_wrapper(status)

    log.info("Query file: %s", query)

    retCode = SUCCESS

    if retCode != SUCCESS:
        log.info("1_preprocessing failed.")
        status = "1_preprocessing failed"
        checkpoint_step_wrapper(status)

    else:
        log.info("1_preprocessing complete.")
        status = "1_preprocessing complete"
        checkpoint_step_wrapper(status)


    return status


"""
STEP 2 Run run_blastplus ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_run_blastplus(query, db, outpath, numReads, log):
    log.info("\n\nSTEP2 - Run run_blastplus <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n")
    status = "2_run_blastplus in progress"
    checkpoint_step_wrapper(status)

    log.info("Query file: %s", query)

    ## Set the default number of reads to sample. If numReads is set as 0, do subsampling up to 50k reads.
    numReads = 50000 if numReads == 0 else numReads
    # sampled = safe_basename(query, log)[0] + "_" + str(numReads) + "_sampled.fasta"
    sampled = safe_basename(query, log)[0] + "_sampled.fasta"
    sampled = os.path.join(outpath, sampled)

    reformatshCmd = get_tool_path("reformat.sh", "bbtools")
    reformatLogFile = os.path.join(outpath, "blast_reformat.log")

    cmd = "%s in=%s out=%s samplereadstarget=%s qin=33 qout=33 ow=t > %s 2>&1 " % (reformatshCmd, query, sampled, numReads, reformatLogFile)
    
    if os.path.isfile(sampled): ## skip subsampling if found
        log.info("Subsampled query file found: %s" % (sampled))

    else:
        _, _, retCode = run_sh_command(cmd, True, log)

        if retCode != SUCCESS:
            log.error("Subsampling failed.")
            return FAILURE

        ## get the real sampled query read num for fasta has lower read num than 50000
        if os.path.isfile(reformatLogFile):
            with open(reformatLogFile) as STAT_FH:
                for l in STAT_FH.readlines():
                    if l.startswith("Output:"):
                        numReads = int(l.split()[1])

    statsFile = CFG["stats_file"]
    append_rqc_stats(statsFile, "blast_query_read_cnt", numReads, log) ## record query read count
    append_rqc_stats(statsFile, "dbs", db, log) ## record db list
    filesFile = CFG["files_file"]
    append_rqc_stats(filesFile, "blast_query_file", sampled, log) ## record sampled query file

    ###############################################################################################
    ## Blast_ondemand.py is called with "-d all" by default. So that all the refs will be aligned.
    ###############################################################################################
    
    dbList = []    
    if db.find(',') != -1: ## if multiple ref dbs are set as CSV format, split the line by ','
        dbList = db.split(',')
    elif db == "all":
        dbList = ["refseq.archaea",
                  "refseq.bacteria",
                  "nt",
                  "refseq.fungi",
                  "refseq.mitochondrion",
                  "refseq.plant",
                  "refseq.plasmid",
                  "refseq.plastid",
                  "refseq.viral",
                  "green_genes",
                  "ssu",
                  "lsu",
                  "lssu",
                  "collab16s",
                  "jgicontam"]
    else:
        dbList.append(db)

    for d in dbList:
        log.info("\n\n - RUN blastn vs %s <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n", d)
        newOutputPath = os.path.join(outpath, d)

        try:
            os.makedirs(newOutputPath)
        except OSError:
            pass

        if os.path.isfile(os.path.join(newOutputPath, "done")) or os.path.isfile(os.path.join(newOutputPath, "timeout")):
            log.info("%s already done." % d)
            continue

        retCode, blastOutFileNamePrefix = run_blastplus_py(sampled, d, newOutputPath, log)

        if retCode != SUCCESS:
            if blastOutFileNamePrefix is None:
                log.error("Failed to run blastn against %s. Ret = %s", d, retCode)
                return "failed"
            elif blastOutFileNamePrefix == -143:
                log.error("Blast overtime. Skip the search against %s.", d)
                # return -143 ## blast overtime
                cmd = "touch %s" % (os.path.join(newOutputPath, "timeout"))
                run_sh_command(cmd, True, log)
                continue

        else:
            log.info("Successfully ran blastn of reads against %s", d)

            ret2 = read_megablast_hits(d, newOutputPath, log)

            if ret2 != SUCCESS:
                log.error("Errors in read_megablast_hits()")
                log.error("2_run_blastplus reporting failed.")
                status = "2_run_blastplus failed"
            else:
                log.info("2_run_blastplus complete.")
                status = "2_run_blastplus complete"
                cmd = "touch %s" % (os.path.join(newOutputPath, "done"))
                run_sh_command(cmd, True, log)


    return status



"""
STEP 3 Post-processing ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_postprocessing(outputPath, log):
    log.info("\n\nSTEP3 - Post-processing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n")
    status = "3_postprocessing in progress"
    checkpoint_step_wrapper(status)

    ## move rqc-files.tmp to rqc-files.txt
    newFilesFile = os.path.join(outputPath, "blast_ondemand_files.txt")
    newStatsFile = os.path.join(outputPath, "blast_ondemand_stats.txt")

    cmd = "mv %s %s " % (CFG["files_file"], newFilesFile)
    log.info("mv cmd: %s", cmd)
    run_sh_command(cmd, True, log)

    cmd = "mv %s %s " % (CFG["stats_file"], newStatsFile)
    log.info("mv cmd: %s", cmd)
    run_sh_command(cmd, True, log)

    log.info("3_postprocessing complete.")
    status = "3_postprocessing complete"
    checkpoint_step_wrapper(status)


    return status



"""
STEP 4 Cleanup ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

"""
def do_cleanup(outpath, log):
    log.info("\n\nSTEP4 - Cleanup <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n")
    status = "4_cleanup in progress"
    checkpoint_step_wrapper(status)

    cmd = "rm -rf %s" % (os.path.join(outpath, "*_sampled.fasta"))
    _, _, exitCode = run_sh_command(cmd, True, log)

    if exitCode != 0:
        log.error("Failed to execute %s; may be already purged.", cmd)

    log.info("4_cleanup complete.")
    status = "4_cleanup complete"
    checkpoint_step_wrapper(status)


    return status


'''===========================================================================
    checkpoint_step_wrapper

'''
def checkpoint_step_wrapper(status):
    assert(CFG["status_file"])
    checkpoint_step(CFG["status_file"], status)



"""
run_blastplus_py

Call run_blastplus.py
"""
def run_blastplus_py(query, db, outDir, log):
    queryBaseName, exitCode = safe_basename(query, log)
    dbFileBaseName, exitCode = safe_basename(db, log)

    log.debug("outDir = %s", outDir)
    log.debug("queryBaseName = %s", queryBaseName)
    log.debug("dbFileBaseName = %s", dbFileBaseName)

    # runBlastnCmd = "/global/homes/s/sulsj/work/bitbucket-repo/jgi-rqc-pipeline/tools/run_blastplus_taxserver.py" ## debug
    runBlastnCmd = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/run_blastplus_taxserver.py" ## production
    # runBlastnCmd = get_tool_path("run_blastplus_taxserver.py", "run_blastplus_taxserver.py") ## production

    blastOutFileNamePrefix = outDir + "/megablast." + queryBaseName + ".vs." + dbFileBaseName
    timeoutCmd = get_tool_path("timeout", "timeout")

    ## 6hrs timeout
    # cmd = "%s 21600s %s -d %s -o %s -q %s -s > %s.log 2>&1 " % (timeoutCmd, runBlastnCmd, db, outDir, query, blastOutFileNamePrefix)
    ## 12hrs timeout
    cmd = "%s 43200s %s -d %s -o %s -q %s -s > %s.log 2>&1 " % (timeoutCmd, runBlastnCmd, db, outDir, query, blastOutFileNamePrefix)

    _, _, exitCode = run_sh_command(cmd, True, log, True)

    ## Added timeout to terminate blast run manually after 6hrs
    ## If exitCode == 124 or exitCode = 143, this means the process exits with timeout.
    ## Timeout exits with 128 plus the signal number. 143 = 128 + 15 (SGITERM)
    ## Ref) http://stackoverflow.com/questions/4189136/waiting-for-a-command-to-return-in-a-bash-script
    ##      timeout man page ==> If the command times out, and --preserve-status is not set, then exit with status 124.
    ##
    if exitCode in (124, 143):
        ## BLAST timeout
        ## Exit with succuss so that the blast step can be skipped.
        log.info("##################################")
        log.info("BLAST TIMEOUT. JUST SKIP THE STEP.")
        log.info("##################################")
        return FAILURE, -143

    elif exitCode != 0:
        log.error("failed to run_blastplus_py. Exit code != 0")
        return FAILURE, None

    else:
        log.info("run_blastplus_py complete.")


    return SUCCESS, blastOutFileNamePrefix




"""

 Title      : read_megablast_hits
 Function   : This function generates tophit list of megablast against different databases.
 Usage      : read_megablast_hits(db_name, log)
 Args       : blast db name or full path
 Returns    : SUCCESS
              FAILURE
 Comments   :

"""
def read_megablast_hits(db, megablastPath, log):
    db = safe_basename(db, log)[0]
    statsFile = CFG["stats_file"]
    filesFile = CFG["files_file"]

    ## Change file search keyword b/c the db names from the option parameter are different with the actual db name
    keyword = db
    ## ssu | lsu | lssu | collab16s | green_genes | sagtam | jgicontam
    if db == "ssu":
        keyword = "SSURef"
    elif db == "lsu":
        keyword = "LSURef"
    elif db == "lssu":
        keyword = "LSSURef"
    elif db == "jgicontam":
        keyword = "JGIContaminants"
    # elif db == "sagtam":
    #     keyword = "sagtaminants"

    ##
    ## Process blast output files
    ##
    matchings = 0
    hitCount = 0
    parsedFile = os.path.join(megablastPath, "megablast.*.%s*.parsed" % (keyword))

    ## count the line number in parsed file
    matchings, _, exitCode = run_sh_command("grep -v '^#' %s 2>/dev/null | wc -l " % (parsedFile), True, log)

    if exitCode == 0: ## if a parsed file is found and also one or more hits found from the file
        t = matchings.split()

        if len(t) == 1 and t[0].isdigit():
            hitCount = int(t[0])

        append_rqc_stats(statsFile, "READ_MATCHING_HITS_" + keyword, hitCount, log)

        ##
        ## add .parsed file
        ##
        parsedFileFound, _, exitCode = run_sh_command("ls %s" % (parsedFile), True, log)

        if parsedFileFound:
            parsedFileFound = parsedFileFound.strip()
            append_rqc_file(filesFile, "READ_PARSED_FILE_" + keyword, os.path.join(megablastPath, parsedFileFound), log)
        else:
            log.error("- Failed to add megablast parsed file of %s." % (keyword))
            return FAILURE

        ##
        ## wc the top hits
        ##
        topHit = 0
        tophitFile = os.path.join(megablastPath, "megablast.*.%s*.parsed.tophit" % (keyword))
        tophits, _, exitCode = run_sh_command("grep -v '^#' %s 2>/dev/null | wc -l " % (tophitFile), True, log)

        t = tophits.split()

        if len(t) == 1 and t[0].isdigit():
            topHit = int(t[0])

        append_rqc_stats(statsFile, "READ_TOP_HITS_" + keyword, topHit, log)

        ##
        ## wc the taxonomic species
        ##
        spe = 0
        taxlistFile = os.path.join(megablastPath, "megablast.*.%s*.parsed.taxlist" % (keyword))
        species, _, exitCode = run_sh_command("grep -v '^#' %s 2>/dev/null | wc -l " % (taxlistFile), True, log)

        t = species.split()

        if len(t) == 1 and t[0].isdigit():
            spe = int(t[0])

        append_rqc_stats(statsFile, "READ_TAX_SPECIES_" + keyword, spe, log)

        ##
        ## wc the top 100 hit
        ##
        top100hits = 0
        top100hitFile = os.path.join(megablastPath, "megablast.*.%s*.parsed.top100hit" % (keyword))
        species, _, exitCode = run_sh_command("grep -v '^#' %s 2>/dev/null | wc -l " % (top100hitFile), True, log)

        t = species.split()

        if len(t) == 1 and t[0].isdigit():
            top100hits = int(t[0])

        append_rqc_stats(statsFile, "READ_TOP_100HITS_" + keyword, top100hits, log)

        ##
        ## Find and add taxlist file
        ##
        taxListFound, _, exitCode = run_sh_command("ls %s" % (taxlistFile), True, log)
        taxListFound = taxListFound.strip()

        if taxListFound:
            append_rqc_file(filesFile, "READ_TAXLIST_FILE_" + keyword, os.path.join(megablastPath, taxListFound), log)
        else:
            log.error("- Failed to add megablast taxlist file of %s." % (keyword))
            return FAILURE

        ##
        ## Find and add tophit file
        ##
        tophitFound, _, exitCode = run_sh_command("ls %s" % (tophitFile), True, log)
        tophitFound = tophitFound.strip()

        if tophitFound:
            append_rqc_file(filesFile, "READ_TOPHIT_FILE_" + keyword, os.path.join(megablastPath, tophitFound), log)
        else:
            log.error("- Failed to add megablast tophit file of %s." % (keyword))
            return FAILURE

        ##
        ## Find and add top100hit file
        ##
        top100hitFound, _, exitCode = run_sh_command("ls %s" % (top100hitFile), True, log)
        top100hitFound = top100hitFound.strip()

        if top100hitFound:
            append_rqc_file(filesFile, "READ_TOP100HIT_FILE_" + keyword, os.path.join(megablastPath, top100hitFound), log)
        else:
            log.error("- Failed to add megablast top100hit file of %s." % (keyword))
            return FAILURE

    else:
        log.info("- No blast hits for %s." % (keyword))


    return SUCCESS


## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program

if __name__ == "__main__":
    desc = "RQC Pacbio Reads-of-insert Pipeline"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-d", "--db", dest="dbFile", help="Blast reference db, \
Full path to BLAST db or [refseq.archaea | refseq.bacteria \
| nt | refseq.fungi | refseq.mitochondrion | refseq.plant | refseq.plasmid \
| refseq.plastid | refseq.viral | green_genes | ssu | lsu | lssu | collab16s \
| jgicontam] or 'all'", required=True)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Target path to write output files to")
    parser.add_argument("-q", "--query", dest="queryFile", help="Input query file (full path to fastq/fasta or gzipped fastq/fasta)", required=True)
    parser.add_argument("-s", "--max-reads", dest="maxReads", help="Set the number of reads to subsample", required=False, default=0, type=int)
    parser.add_argument('-v', '--version', action='version', version=VERSION)

    options = parser.parse_args()

    ## Mandatory options
    queryFile = options.queryFile
    dbFile = options.dbFile

    outputPath = None
    if options.outputPath:
        outputPath = options.outputPath
        make_dir_p(outputPath)
    else:
        outputPath = os.getcwd()

    maxReads = options.maxReads

    CFG["status_file"] = os.path.join(outputPath, "blast_ondemand_status.log")
    CFG["files_file"] = os.path.join(outputPath, "blast_ondemand_files.tmp")
    CFG["stats_file"] = os.path.join(outputPath, "blast_ondemand_stats.tmp")
    CFG["log_file"] = os.path.join(outputPath, "blast_ondemand.log")


    print "Started blast on-demand pipeline, writing log to: %s" % (CFG["log_file"])

    log = get_logger("blast-ondemand", CFG["log_file"], LOG_LEVEL)

    log.info("=================================================================")
    log.info("   Blast On-Demand (version %s)" % (VERSION))
    log.info("=================================================================")

    log.info("Starting %s...", SCRIPT_NAME)


    status = None
    nextStep = 0


    ## check for query file
    if os.path.isfile(queryFile):
        log.info("Found %s, starting processing.", queryFile)

        if os.path.isfile(CFG["status_file"]):
            status = get_status(CFG["status_file"], log)
            log.info("Latest status = %s", status)

            if status == "start":
                pass

            elif status != "complete":
                nextStep = int(status.split("_")[0])
                if status.find("complete") != -1:
                    nextStep += 1
                log.info("Next step to do = %s", nextStep)

            else:
                ## already bDone. just exit
                log.info("Completed %s: %s", SCRIPT_NAME, queryFile)
                exit(0)
        else:
            checkpoint_step_wrapper("start")
            status = get_status(CFG["status_file"], log)
            log.info("Latest status = %s", status)

    else:
        log.error("%s not found, aborting!", queryFile)
        exit(2)



    done = False
    cycle = 0

    filesFile = CFG["files_file"]
    statsFile = CFG["stats_file"]


    if status == "complete":
        log.info("Status is complete")
        done = True


    while not done:

        cycle += 1

        if nextStep == 1 or status == "start":
            status = do_preprocessing(queryFile, log)

        if status == "1_preprocessing failed":
            done = True


        if nextStep == 2 or status == "1_preprocessing complete":
            status = do_run_blastplus(queryFile, dbFile, outputPath, maxReads, log)

        if status == "2_run_blastplus failed":
            done = True


        if nextStep == 3 or status == "2_run_blastplus complete":
            status = do_postprocessing(outputPath, log)

        if status == "3_postprocessing failed":
            done = True


        if nextStep == 4 or status == "3_postprocessing complete":
            status = do_cleanup(outputPath, log)

        if status == "4_cleanup failed":
            done = True


        if status == "4_cleanup complete":
            status = "complete" ## FINAL COMPLETE!
            done = True


        ## don't cycle more than 10 times ...
        if cycle > 10:
            done = True


    if status != "complete":
        log.info("Status %s", status)

    else:
        log.info("\n\nCompleted %s", SCRIPT_NAME)
        checkpoint_step_wrapper("complete")
        log.info("Done.")
        print "Done."

## EOF
