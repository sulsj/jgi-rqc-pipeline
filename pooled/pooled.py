#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Pooled analysis pipeline

Command
    $ pooled.py --fastq 6145.5.40009.fastq --output-path in-progress/00/00/00/05/

Outputs
  - stats_log = pooled_stats.txt
  - file_log = pooled_files.txt
  - status_log = pooled_status.log


Created: Oct 17 2016

sulsj (ssul@lbl.gov)


Revision:

    10172016 1.0.0: init
    10172016 1.0.0: Separated out from read qc

    11222016 1.1.0: Added reference analysis step
    12052016 1.1.1: Added trimming in the reference analysis step

    12132016 1.2.0: Added plotting

    03022017 1.3.0: Added fragment analysis

    06052017 1.4.0: Updated to run on Cori
    09222017 1.4.1: Decreased the data size in plotting multiplex data

    10122017 1.5.0: Skip reference analysis

    03052018 1.6.0: Added kapa spike-ins analysis
    04092018 1.6.1: Fixed jamo call for kapa spikein analysis
    06052018 1.6.2: Updated kapa spike-ins table creation


"""

## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## libraries to use
import os
import sys
import argparse
import matplotlib
import numpy as np
import glob

srcDir = os.path.dirname(__file__)
sys.path.append(os.path.join(srcDir, "../lib"))    ## rqc-pipeline/lib
sys.path.append(os.path.join(srcDir, "../tools"))  ## rqc-pipeline/tools

from pooled_constants import RQCPooledConfig
from pooled_utils import *
from common import get_run_path, get_logger, get_status, append_rqc_stats, append_rqc_file, set_colors
from rqc_fastq import get_working_read_length
from os_utility import get_tool_path
from db_access import *

matplotlib.use("Agg")  ## This needs to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import mpld3
from matplotlib.ticker import MultipleLocator, FormatStrFormatter


VERSION = "1.6.2"
LOG_LEVEL = "DEBUG"
# LOG_LEVEL = "INFO"
SCRIPT_NAME = __file__
# NUM_KAPA_WORKER = 2 ## number of tfmq workers for processing kapa spikeins

color = {}
color = set_colors(color, True)


"""
STEP1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_multiplex_statistics

Demultiplexing analysis for pooled lib

"""
def do_multiplex_statistics(fastq, log):
    log.info("\n\n%sSTEP1 - Multiplex statistics analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])

    filesFile = RQCPooledConfig.CFG["files_file"]

    status = "1_multiplex_statistics in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    log.debug("fastq file: %s", fastq)

    retCode, demultiplexStatsFile, detectionPngPlotFile, detectionHtmlPlotFile = generate_index_sequence_detection_plot(fastq, log)

    if retCode != RQCExitCodes.JGI_SUCCESS:
        status = "1_multiplex_statistics failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        status = "1_multiplex_statistics complete"
        log.info(status)
        checkpoint_step_wrapper(status)

        if demultiplexStatsFile is not None:
            append_rqc_file(filesFile, "pooled demultiplex stats text", demultiplexStatsFile, log)

        if detectionPngPlotFile is not None:
            append_rqc_file(filesFile, "pooled demultiplex stats plot", detectionPngPlotFile, log)

        if detectionHtmlPlotFile is not None:
            append_rqc_file(filesFile, "pooled demultiplex stats d3 plot", detectionHtmlPlotFile, log)


    return status


"""
STEP2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_pooled_reference_mapping_analysis

"""
def do_pooled_reference_mapping_analysis(fastq, ref, outputPath, log):
    log.info("\n\n%sSTEP2 - pooled_reference_mapping_analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "2_pooled_reference_mapping_analysis in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    if ref is None:
        status = "2_pooled_reference_mapping_analysis complete"
        log.info("pooled_reference_mapping_analysis skipped.")
        checkpoint_step_wrapper(status)

    else:
        filesFile = RQCPooledConfig.CFG["files_file"]
        statsFile = RQCPooledConfig.CFG["stats_file"]

        log.debug("fastq file: %s", fastq)
        log.debug("reference file: %s", ref)

        refanalDir = "ref-analysis"
        refAnalPath = os.path.join(outputPath, refanalDir)
        make_dir(refAnalPath)
        change_mod(refAnalPath, "0755")

        sequnitFileName, exitCode = safe_basename(fastq, log)
        sequnitFileName = sequnitFileName.replace(".fastq", "").replace(".gz", "")

        trimmedFile = os.path.join(refAnalPath, sequnitFileName + ".trimmed.fq.gz")
        mappedFile = os.path.join(refAnalPath, sequnitFileName + ".mapped.sam.gz")
        variantsTxtFile = os.path.join(refAnalPath, sequnitFileName + ".variants.txt")
        trimmedFile2 = os.path.join(refAnalPath, sequnitFileName + ".trimmed2.fq.gz")

        mhistTxtFile = os.path.join(refAnalPath, sequnitFileName + ".mhist.txt") ## Histogram of match, sub, del, and ins rates by read location
        qhistTxtFile = os.path.join(refAnalPath, sequnitFileName + ".qhist.txt") ## Quality histogram by position
        qahistTxtFile = os.path.join(refAnalPath, sequnitFileName + ".qahist.txt") ## Quality accuracy histogram of error rates versus quality score
        aqhistTxtFile = os.path.join(refAnalPath, sequnitFileName + ".aqhist.txt") ## Histogram of average read quality


        ## https://issues.jgi-psf.org/browse/RQC-887
        # $ bbduk.sh in=reads.fq.gz out=trimmed.fq.gz ref=/global/projectb/sandbox/gaag/bbtools/data/adapters.fa ktrim=r tpe tbo hdist=1 k=23 mink=11
        # $ bbmap.sh ref=ref.fasta nodisk in=trimmed.fq.gz out=mapped.sam.gz
        # $ callvariants.sh in=mapped.sam.gz out=variants.txt minscore=20
        # $ bbduk.sh in=mapped.sam.gz variants=variants.txt mhist=mhist.txt qhist=qhist.txt qahist=qahist.txt aqhist=aqhist.txt

        bbdukSh = get_tool_path("bbduk.sh", "bbtools")
        bbdukCmd = "%s in=%s out=%s ref=/global/projectb/sandbox/gaag/bbtools/data/adapters.fa ktrim=r tpe tbo hdist=1 k=23 mink=11 ow=t" %\
                   (bbdukSh, fastq, trimmedFile)

        _, _, exitCode = run_sh_command(bbdukCmd, True, log, True)

        if exitCode != 0 or not os.path.isfile(trimmedFile):
            log.error("Failed to run bbduk.sh cmd, %s", bbdukCmd)
            exit(1)

        bbmapSh = get_tool_path("bbmap.sh", "bbtools")
        bbmapCmd = "%s ref=%s nodisk in=%s out=%s ow=t" % (bbmapSh, ref, trimmedFile, mappedFile)

        _, _, exitCode = run_sh_command(bbmapCmd, True, log, True)

        if exitCode != 0 or not os.path.isfile(mappedFile):
            log.error("Failed to run bbmap.sh cmd, %s", bbmapCmd)
            exit(1)

        callVariantsSh = get_tool_path("callvariants.sh", "bbtools")
        collvariantsCmd = "%s in=%s out=%s ref=%s minscore=20 ow=t" % (callVariantsSh, mappedFile, variantsTxtFile, ref)

        _, _, exitCode = run_sh_command(collvariantsCmd, True, log, True)

        if exitCode != 0 or not os.path.isfile(variantsTxtFile):
            log.error("Failed to run callvariants.sh cmd, %s", collvariantsCmd)
            exit(1)

        # bbdukCmd2 = "module load bbtools; bbduk.sh in=%s out=%s variants=%s mhist=%s qhist=%s qahist=%s aqhist=%s ow=t" %\
        bbdukCmd2 = "%s in=%s out=%s variants=%s mhist=%s qhist=%s qahist=%s aqhist=%s ow=t" %\
                   (bbdukSh, mappedFile, trimmedFile2, variantsTxtFile, mhistTxtFile, qhistTxtFile, qahistTxtFile, aqhistTxtFile)

        _, _, exitCode = run_sh_command(bbdukCmd2, True, log, True)

        if exitCode != 0 or not os.path.isfile(mhistTxtFile):
            log.error("Failed to run bbduk.sh cmd, %s", bbdukCmd2)
            exit(1)

        else:
            status = "2_pooled_reference_mapping_analysis complete"
            log.info(status)
            checkpoint_step_wrapper(status)

            readLength = 0
            readLenR1 = 0
            readLenR2 = 0
            isPairedEnd = None

            readLength, readLenR1, readLenR2, isPairedEnd = get_working_read_length(fastq, log)
            log.info("Read length = %s, and is paired? = %s", readLength, isPairedEnd)

            if isPairedEnd:
                append_rqc_stats(statsFile, "pooled read length 1", readLenR1, log)
                append_rqc_stats(statsFile, "pooled read length 2", readLenR2, log)

            else:
                append_rqc_stats(statsFile, "pooled read length 1", readLength, log)

            append_rqc_file(filesFile, "pooled ref database", ref, log)

            if trimmedFile is not None:
                append_rqc_file(filesFile, "pooled ref trimmed fastq", trimmedFile, log)
            if mappedFile is not None:
                append_rqc_file(filesFile, "pooled ref mapped fastq", mappedFile, log)
            if variantsTxtFile is not None:
                append_rqc_file(filesFile, "pooled ref variants text", variantsTxtFile, log)
            if trimmedFile2 is not None:
                append_rqc_file(filesFile, "pooled ref trimmed fastq2", trimmedFile2, log)


            ## Checking outputs and generating plots
            if mhistTxtFile is not None:
                append_rqc_file(filesFile, "pooled ref match rate hist mhist text", mhistTxtFile, log) ## Histogram of match, sub, del, and ins rates by read location

                matchRatePng, matchRateD3 = gen_match_rate_hist_plot(fastq, mhistTxtFile, isPairedEnd, refAnalPath, log)
                log.debug("Outputs: %s %s %s", mhistTxtFile, matchRatePng, matchRateD3)

                if matchRatePng is not None:
                    append_rqc_file(filesFile, "pooled ref match rate hist plot", matchRatePng, log)
                    append_rqc_file(filesFile, "pooled ref match rate hist d3 plot", matchRateD3, log)
                else:
                    log.error("Failed to generate base position quality histogram plot using %s", mhistTxtFile)

            if qhistTxtFile is not None:
                append_rqc_file(filesFile, "pooled ref read qual pos qhist text", qhistTxtFile, log) ## Quality histogram by position

                basePosQualPng, basePosQualD3 = gen_avg_qual_by_position_plot(fastq, qhistTxtFile, isPairedEnd, refAnalPath, log)
                log.debug("Outputs: %s %s %s", qhistTxtFile, basePosQualPng, basePosQualD3)

                if basePosQualPng is not None:
                    append_rqc_file(filesFile, "pooled ref read qual pos plot", basePosQualPng, log)
                    append_rqc_file(filesFile, "pooled ref read qual pos d3 plot", basePosQualD3, log)
                else:
                    log.error("Failed to generate base position quality histogram plot using %s", qhistTxtFile)

            if qahistTxtFile is not None:
                append_rqc_file(filesFile, "pooled ref qual accuracy qahist text", qahistTxtFile, log) ## Quality accuracy histogram of error rates versus quality score

                qualAccuPng, qualAccuD3 = gen_qual_accuracy_plot(fastq, qahistTxtFile, refAnalPath, log)
                log.debug("Outputs: %s %s %s", qahistTxtFile, qualAccuPng, qualAccuD3)

                if qualAccuPng is not None:
                    append_rqc_file(filesFile, "pooled ref qual accuracy plot", qualAccuPng, log)
                    append_rqc_file(filesFile, "pooled ref qual accuracy d3 plot", qualAccuD3, log)
                else:
                    log.error("Failed to generate average read quality histogram plot using %s", qahistTxtFile)

            if aqhistTxtFile is not None:
                append_rqc_file(filesFile, "pooled ref avg read qual aqhist text", aqhistTxtFile, log) ## Histogram of average read quality

                readQualPng, readQualD3 = gen_fraction_reads_by_qual_plot(fastq, aqhistTxtFile, isPairedEnd, refAnalPath, log)
                log.debug("Outputs: %s %s %s", aqhistTxtFile, readQualPng, readQualD3)

                if readQualPng is not None:
                    append_rqc_file(filesFile, "pooled ref avg read qual plot", readQualPng, log)
                    append_rqc_file(filesFile, "pooled ref avg read qual d3 plot", readQualD3, log)
                else:
                    log.error("Failed to generate average read quality histogram plot using %s", aqhistTxtFile)


    return status




"""
STEP3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_fragment_analysis

"""
def do_fragment_analysis(fastq, outputPath, log):
    log.info("\n\n%sSTEP3 - fragment analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "3_fragment_analysis in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    statsFile = RQCPooledConfig.CFG["stats_file"]
    filesFile = RQCPooledConfig.CFG["files_file"]

    imgOut = "fragment"
    imgOutputDir = os.path.join(outputPath, imgOut)
    make_dir(imgOutputDir)
    change_mod(imgOutputDir, "0755")

    # retCode = fragment_analysis(fastq, outputPath, log)

    ## Check run_id < 30000
    sequnitName, exitCode = safe_basename(fastq, log)
    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)
    sql = """select run_id from seq_units where seq_unit_name = "%s" """
    sth.execute(sql % (sequnitName))
    rs = sth.fetchone()
    assert rs
    if int(rs['run_id']) > 30000:
        log.info("Detected run_id > 30000. Skip the fragment analysis.")
        status = "3_fragment_analysis complete"
        log.info(status)
        checkpoint_step_wrapper(status)

    else:
        ## ex)
        ## module load jgi-rqc; clarity_crawler.py -s /global/projectb/scratch/sulsj/data2/pooled/9398.7.131285.fastq.gz -o /global/projectb/scratch/sulsj/2016.10.17-pooled-test/out/9398.7.131285/fragment
        clarityCrawler = get_tool_path("clarity_crawler.py", "clarity_crawler.py")
        ccCmd = "%s -s %s -o %s" % (clarityCrawler, fastq, imgOutputDir)
        _, _, retCode = run_sh_command(ccCmd, True, log, True)
    
        if retCode != RQCExitCodes.JGI_SUCCESS:
            status = "3_fragment_analysis failed"
            log.error(status)
            checkpoint_step_wrapper(status)
    
        else:
            status = "3_fragment_analysis complete"
            log.info(status)
            checkpoint_step_wrapper(status)
    
            n = 1
            for f in os.listdir(imgOutputDir):
                log.debug("Downloaded gel image file(s): %s", f)
                if f.endswith(".png") or f.endswith(".bmp"):
                    append_rqc_file(filesFile, "fragment_analysis_gel_plot_" + str(n), os.path.join(imgOutputDir, f), log)
                    n += 1
    
            append_rqc_stats(statsFile, "fragment_analysis_gel_plot_cnt", n - 1, log)


    return status


"""
STEP4 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_kapa_spikein_analysis

"""
def do_kapa_spikein_analysis(fastq, outputPath, log):
    log.info("\n\n%sSTEP4 - kapa spike-in analysis <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "4_kapa_analysis in progress"
    log.info(status)
    checkpoint_step_wrapper(status)

    statsFile = RQCPooledConfig.CFG["stats_file"]
    filesFile = RQCPooledConfig.CFG["files_file"]

    statOut = "kapa"
    kapaOutputDir = os.path.join(outputPath, statOut)
    make_dir(kapaOutputDir)
    change_mod(kapaOutputDir, "0755")


    ## Get the lib name
    sequnitName, exitCode = safe_basename(fastq, log)
    db = jgi_connect_db("rqc")
    sth = db.cursor(MySQLdb.cursors.DictCursor)
    sql = """select library_name from seq_units where seq_unit_name = "%s" """
    # log.debug("sql= %s, seq_unit_name = %s", sql, sequnitName)
    sth.execute(sql % (sequnitName))
    rs = sth.fetchone()
    assert rs
    log.debug("Parent library name: %s", rs['library_name'])


    ## Get the fastq list for "fastq"
    ## CNSYH (78) BGPAT (82)

    ## 12233.7.246491.fastq.gz ==? 12233.7.
    sequnitPrefix = sequnitName.split('.')[0]

    jamoTool = get_tool_path("jamo", "jamo")
    # cmd = "module load jamo; jamo report select metadata.library_name, file_path, file_name where metadata.fastq_type = sdm_normal and metadata.parent_library_name = %s" \
    cmd = "%s report select metadata.library_name, file_path, file_name where metadata.fastq_type = sdm_normal and metadata.parent_library_name = %s and file_name like %s" \
          % (jamoTool, rs['library_name'], sequnitPrefix + '%gz')
    stdOut, _, retCode = run_sh_command(cmd, True, log, True)
    assert retCode == 0, "Failed to run %s" % (cmd)

    candidateFiles = []
    fpath = ""
    # numTasks = 0

    ## Check each fastq in the pool and restore if needed
    for l in stdOut.split('\n'):
        ## ['BZOXP', '/global/dna/dm_archive/sdm/illumina/01/18/85', '11885.4.225459.TGTACAC-GGTGTAC.fastq.gz']
        t = l.split('\t')
        if len(t) < 3:
            continue

        # cmd = "module load jamo; jamo fetch filename %s" % (t[2])
        cmd = "%s fetch filename %s" % (jamoTool, t[2])
        ## /global/dna/dm_archive/sdm/illumina/01/18/85/11885.4.225459.AGTGCAG-ACTGCAC.fastq.gz PURGED 59b4edcc7ded5e2f18676a06
        ## /global/dna/dm_archive/sdm/illumina/01/18/85/11885.4.225459.AGTGCAG-ACTGCAC.fastq.gz RESTORE_IN_PROGRESS 59b4edcc7ded5e2f18676a06
        ## /global/dna/dm_archive/sdm/illumina/01/18/85/11885.4.225459.AGTGCAG-ACTGCAC.fastq.gz RESTORED 59b4edcc7ded5e2f18676a06
        stdOut, _, retCode = run_sh_command(cmd, True, log, stdoutPrint=False)
        assert retCode == 0

        if stdOut.split(' ')[1] == "PURGED":
            log.info("Purged sequnit found: %s", l)
            while stdOut.split(' ')[1] in ("PURGED", "RESTORE_IN_PROGRESS"):
                log.info("Wait for %s to be restored...", t[2])
                time.sleep(60)
                stdOut, _, retCode = run_sh_command(cmd, True, log, stdoutPrint=False)
                assert retCode == 0
        else:
            assert os.path.isfile(os.path.join(t[1], t[2])), "File not found, %s" % (l)
            # if not os.path.isfile(os.path.join(t[1], t[2])):
            #     log.warning("Kapa spike-ins analysis: File not found, %s", (l))
            #     continue

            temp = {}
            temp['library_name'] = t[0]
            temp['seq_unit_name'] = t[2]
            # temp['fpath'] = t[1]
            # temp['outpath'] = kapaOutputDir
            fpath = t[1]

            sql = """select well_id from library_info l inner join seq_units s on s.rqc_library_id = l.library_id where s.seq_unit_name = "%s";"""
            sth.execute(sql % (t[2]))
            rs = sth.fetchone()
            # assert rs is not None, "Failed to get well id: %s" % (sql % t[2])
            if rs is None or rs["well_id"] is None:
                log.warning("Failed to get well id: %s" % (sql % t[2]))
                ## if well_id does not exist, set it as "NA"
                temp["well_id"] = "NA"
            else:
                temp["well_id"] = rs["well_id"]

            candidateFiles.append(temp)
            # numTasks += 1


    ## run bbduk against kapa ref
    bbdukTool = get_tool_path("bbduk.sh", "bbtools")
    # bbdukCmd = "%s in=%s out=/dev/null stats=%s ref=kapa k=31 edist=1 overwrite=t"
    bbdukCmd = "%s in=%s out=null stats=%s ref=kapa k=31 edist=1 overwrite=t"


    ############################################################################
    ## Serial run
    ## CNSYH => 78 sequnits ==> 30min
    ############################################################################
    for f in candidateFiles:
        statFile = os.path.join(kapaOutputDir, f['seq_unit_name'] + "." + f['library_name'] + "." + f['well_id'] + ".kapa")
        cmd = bbdukCmd % (bbdukTool, os.path.join(fpath, f['seq_unit_name']), statFile)
        _, _, retCode = run_sh_command(cmd, True, log, True)
        assert retCode == 0, "Failed to run: %s" % (cmd)
        assert os.path.isfile(statFile), "Failed to find kapa stat file: %s" % (statFile)


    ############################################################################
    ## tfmq version
    ############################################################################
    # tfmqFile = os.path.join(kapaOutputDir, fastq + ".kapa.tfmq")
    # tfmqTemplate = "%s::0\n"
    #
    # with open(tfmqFile, 'w') as TfmqF:
    #     for f in candidateFiles:
    #         statFile = os.path.join(kapaOutputDir, f['seq_unit_name'] + "." + f['library_name'] + ".kapa")
    #         cmd = bbdukCmd % (bbdukTool, os.path.join(fpath, f['seq_unit_name']), statFile)
    #         TfmqF.write(tfmqTemplate % (cmd))
    #
    # if os.path.isfile(tfmqFile):
    #     cmd = ""
    #     cmd = "(tfmq-worker -q kapa -ld %s &) && (sleep 2) && "
    #     for _ in range(NUM_KAPA_WORKER - 1):
    #         cmd += "(tfmq-worker -q kapa -ld %s &) && (sleep 2) && " % (kapaOutputDir)
    #     cmd += "(tfmq-client -d -i %s -q kapa -ld %s)" % (tfmqFile, kapaOutputDir)
    #     _, _, retCode = run_sh_command(cmd, True, log, True)
    # else:
    #     log.error("Failed to create tfmq input file.")
    #     exit(1)


    ############################################################################
    ## process pool version
    ############################################################################
    # import subprocess as sp
    # import multiprocessing as mp
    #
    # def work(f):
    #     # s = d['seq_unit_name']
    #     # l = d['library_name']
    #     # f = os.path.join(fpath, f)
    #     # statFile = os.path.join(kapaOutputDir, s + "." + l + ".kapa")
    #     # print " ".join([bbdukTool, 'in=%s' % (f), 'out=/dev/null', 'stats=%s' % (statFile), 'ref=kapa', 'k=31', 'edist=1', 'overwrite=t'])
    #     # print " ".join([bbdukTool, 'in=%s' % (f), 'out=/dev/null', 'stats=%s' % (f + ".kapa"), 'ref=kapa', 'k=31', 'edist=1', 'overwrite=t'])
    #     # sp.call([bbdukTool, 'in=%s' % (f), 'out=/dev/null', 'stats=%s' % (f + ".kapa"), 'ref=kapa', 'k=31', 'edist=1', 'overwrite=t'])
    #     print f
    #     return 0
    #
    # count = mp.cpu_count()
    # # pool = mp.Pool(processes=count)
    # pool.map(work, candidateFiles)


    ## parse output files
    searchPatt = os.path.join(kapaOutputDir, "*.kapa")
    outKapaFileList = glob.glob(searchPatt)

    ## TODO: prepare for bigger plate
    rowDict = {"A": 0, "B": 1, "C": 2, "D": 3, "E": 4, "F": 5, "G": 6, "H": 7}
    kakaTagDict = {"001": "A1", "002": "B1", "003": "C1", "004": "D1", "005": "E1", "006": "F1", "007": "G1", "008": "H1",
                   "009": "A2", "010": "B2", "011": "C2", "012": "D2", "013": "E2", "014": "F2", "015": "G2", "016": "H2",
                   "017": "A3", "018": "B3", "019": "C3", "020": "D3", "021": "E3", "022": "F3", "023": "G3", "027": "H3",
                   "029": "A4", "030": "B4", "031": "C4", "032": "D4", "033": "E4", "034": "F4", "035": "G4", "036": "H4",
                   "037": "A5", "038": "B5", "039": "C5", "040": "D5", "041": "E5", "042": "F5", "043": "G5", "044": "H5",
                   "045": "A6", "046": "B6", "047": "C6", "102": "D6", "049": "E6", "050": "F6", "051": "G6", "052": "H6",
                   "053": "A7", "054": "B7", "055": "C7", "056": "D7", "057": "E7", "058": "F7", "059": "G7", "060": "H7",
                   "061": "A8", "062": "B8", "063": "C8", "064": "D8", "065": "E8", "066": "F8", "067": "G8", "068": "H8",
                   "069": "A9", "070": "B9", "071": "C9", "072": "D9", "073": "E9", "074": "F9", "075": "G9", "076": "H9",
                   "077": "A10", "078": "B10", "079": "C10", "080": "D10", "081": "E10", "082": "F10", "083": "G10", "084": "H10",
                   "085": "A11", "086": "B11", "087": "C11", "088": "D11", "089": "E11", "090": "F11", "091": "G11", "092": "H11",
                   "093": "A12", "094": "B12", "095": "C12", "096": "D12", "097": "E12", "099": "F12", "104": "G12", "101": "H12"
    }

    plate = [[0 for i in xrange(12)] for i in xrange(8)]
    plateNoSpikein = [[0 for i in xrange(12)] for i in xrange(8)]
    plateLib = [[0 for i in xrange(12)] for i in xrange(8)]

    kapaSummaryFile = os.path.join(kapaOutputDir, "kapa.summary")
    kapaSpikeinFile = os.path.join(kapaOutputDir, "kapa.spikein")
    kapaNonspikeinFiile = os.path.join(kapaOutputDir, "kapa.nonspikein")
    kapaLibFiile = os.path.join(kapaOutputDir, "kapa.library")

    sumOuputfile = open(kapaSummaryFile, 'w')
    spikeinOuputfile = open(kapaSpikeinFile, 'w')
    nonspikeOuputfile = open(kapaNonspikeinFiile, 'w')
    libTableFile = open(kapaLibFiile, 'w')

    sumOuputfile.write("#sequnit_name library_ame well_id tag_name matched_reads_num matched_reads_perc sum_others_read_num sum_others_perc\n")

    for idx, i in enumerate(outKapaFileList):
        ## store to files.txt
        append_rqc_file(filesFile, "kapa_spikein_bbduk_outfile_" + str(idx + 1), i, log)

        eachSequnit = ""
        eachReadCnt = ""

        firstKapaTagName = "n/a"
        matched_reads_cnt = 0
        matched_reads_perc = 0.0

        othersRead = []
        others = []
        bFoundFirst = False
        sumOthersReadNum = 0 ## sum for hits without top hit
        sumOthersPerc = 0.0 ## sum for hits without top hit

        ## spikein output file ex)
        # #File    /global/dna/dm_archive/sdm/illumina/01/08/19/10819.1.180667.CAAGGTC-AGACCTT.fastq.gz
        # #Total    305992
        # #Matched    3787    1.23761%
        # #Name    Reads    ReadsPct
        # tag096    3782    1.23598%
        # tag094    2    0.00065%
        # ...

        with open(i, 'r') as inputf:
            libraryName = i.split('.')[-3]
            wellId = i.split('.')[-2]
            eachSequnit = ".".join(safe_basename(i, log)[0].split('.')[:-3])

            for l2 in inputf:
                t = l2.strip().split()
                # if l2.startswith("#File"):
                #     eachSequnit = l2.strip().split()[1]
                if l2.startswith("#Total"):
                    eachReadCnt = t[1]
                elif l2.startswith("tag"):
                    if not bFoundFirst:
                        firstKapaTagName = t[0]
                        matched_reads_cnt = t[1]
                        matched_reads_perc = t[2].replace('%', '')
                        bFoundFirst = True
                    else:
                        othersRead.append(t[1])
                        others.append(t[2].replace('%', ''))

        for j in othersRead:
            sumOthersReadNum += int(j)

        for j in others:
            sumOthersPerc += float(j)

        sumOuputfile.write("%s,%s,%s,%s,%s,%s,%s,%s\n" % (eachSequnit, libraryName, wellId, firstKapaTagName, matched_reads_cnt, matched_reads_perc, sumOthersReadNum, sumOthersPerc))

        ## OLD
        # if wellId != "NA":
        #     rowNum = rowDict[wellId[:1]]
        #     colNum = int(wellId[1:]) - 1
        #     plate[rowNum][colNum] = matched_reads_perc
        #     plateNoSpikein[rowNum][colNum] = sumOthersPerc
        #     plateLib[rowNum][colNum] = libraryName

        ## NEW 06042018
        ## NOTE: sumOthersReadNum and sumOthersPerc are not used!
        ## If the tag number's cell id is matched, show the spike-ins value in the Primary Kapa Tag Concentration table
        ## If not, show the value in the Non-Primary Kapa Tag Concentration table
        ##
        if wellId != "NA" and firstKapaTagName != "n/a":
            rowNum = rowDict[wellId[:1]]
            colNum = int(wellId[1:]) - 1
            if kakaTagDict[firstKapaTagName.replace("tag", "")] == wellId:
                plate[rowNum][colNum] = matched_reads_perc
            else:
                plateNoSpikein[rowNum][colNum] = matched_reads_perc
            plateLib[rowNum][colNum] = libraryName


    for i in range(8):
        for j in range(12):
            c = ',' if j < 11 else '\n'
            try:
                spikeinOuputfile.write(plate[i][j] + c)
            except:
                spikeinOuputfile.write("n/a" + c)

            try:
                nonspikeOuputfile.write(plateNoSpikein[i][j] + c)
            except:
                nonspikeOuputfile.write("n/a" + c)

            try:
                libTableFile.write(plateLib[i][j] + c)
            except:
                libTableFile.write("" + c)


    sumOuputfile.close()
    spikeinOuputfile.close()
    nonspikeOuputfile.close()
    libTableFile.close()


    ## Record files
    assert os.path.isfile(kapaSummaryFile)
    assert os.path.isfile(kapaSpikeinFile)
    assert os.path.isfile(kapaNonspikeinFiile)
    assert os.path.isfile(kapaLibFiile)

    append_rqc_file(filesFile, "kapa spikein summary csv file", kapaSummaryFile, log)
    append_rqc_file(filesFile, "kapa spikein table file", kapaSpikeinFile, log)
    append_rqc_file(filesFile, "kapa nonspikein table file", kapaNonspikeinFiile, log)
    append_rqc_file(filesFile, "kapa library table file", kapaLibFiile, log)


    status = "4_kapa_analysis complete"
    log.info(status)
    checkpoint_step_wrapper(status)

    return status


"""
STEP6 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

do_cleanup_pooled

"""
def do_cleanup_pooled(log):
    log.info("\n\n%sSTEP6 - Cleanup <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
    status = "6_cleanup_pooled in progress"
    checkpoint_step_wrapper(status)

    ## Get option
    skipCleanup = RQCPooledConfig.CFG["skip_cleanup"]

    if not skipCleanup:
        retCode = cleanup_pooled(log)
    else:
        log.warning("File cleaning is skipped.")
        retCode = RQCExitCodes.JGI_SUCCESS

    if retCode != RQCExitCodes.JGI_SUCCESS:
        status = "6_cleanup_pooled failed"
        log.error(status)
        checkpoint_step_wrapper(status)

    else:
        status = "6_cleanup_pooled complete"
        log.info(status)
        checkpoint_step_wrapper(status)


    return status



"""
Create average quality by position plot

input: qhist text data file generated by bbduk
output: png and d3 interactive plots

"""
def gen_avg_qual_by_position_plot(fastq, qhistFile, bIsPaired, outPath, log):
    sequnitFileName, _ = safe_basename(fastq, log)
    sequnitFileNamePrefix = sequnitFileName.replace(".fastq", "").replace(".gz", "")

    qualDir = "qual"
    qualPath = os.path.join(outPath, qualDir)
    make_dir(qualPath)
    change_mod(qualPath, "0755")

    log.debug("qhist file: %s", qhistFile)

    ## Gen Average Base Position Quality Plot
    if not os.path.isfile(qhistFile):
        log.error("Qhist file not found: %s", qhistFile)
        return None, None

    pngFile = None
    htmlFile = None

    ## New data format
    ## Load data from txt
    rawDataMatrix = np.loadtxt(qhistFile, delimiter='\t', comments='#')
    assert len(rawDataMatrix[0][:]) == 7 or len(rawDataMatrix[0][:]) == 4

    ## data (paired)
    # #Deviation    3.0574  8.3610
    # #BaseNum  Read1_linear    Read1_log   Read1_measured  Read2_linear    Read2_log   Read2_measured
    # 1 26.793  13.947  6.873   27.132  10.897  5.628
    # 2 26.723  13.738  6.812   29.086  15.890  5.873
    # 3 24.445  13.266  6.824   29.221  15.446  5.954
    # 4 26.073  13.294  7.118   29.523  15.777  5.522
    # 5 26.289  13.023  7.429   29.629  15.802  6.420

    fig, ax = plt.subplots()

    markerSize = 3.5
    lineWidth = 1.0

    p1 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 2], 'r', marker='o', markersize=markerSize, linewidth=lineWidth, label='read1_log')
    p3 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 3], 'b', marker='o', markersize=markerSize, linewidth=lineWidth, label='read1_measured')

    if bIsPaired:
        p2 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 5], 'g', marker='d', markersize=markerSize, linewidth=lineWidth, label='read2_log')
        p4 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 6], 'm', marker='d', markersize=markerSize, linewidth=lineWidth, label='read2_measured')

    ax.set_xlabel("Read Position", fontsize=12, alpha=0.5)
    ax.set_ylabel("Average Quality Score", fontsize=12, alpha=0.5)
    # ax.set_ylim([0, 45])
    # ax.set_ylim([0, 60])
    fontProp = FontProperties()
    fontProp.set_size("small")
    fontProp.set_family("Bitstream Vera Sans")
    ax.legend(loc=1, prop=fontProp)
    ax.grid(color="gray", linestyle=':')

    ## Add tooltip
    mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p1[0], labels=list(rawDataMatrix[:, 2])))
    mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p3[0], labels=list(rawDataMatrix[:, 3])))

    if bIsPaired:
        mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p2[0], labels=list(rawDataMatrix[:, 5])))
        mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p4[0], labels=list(rawDataMatrix[:, 6])))

    pngFile = os.path.join(qualPath, sequnitFileNamePrefix + ".read_basepos_qual_hist.png")
    htmlFile = os.path.join(qualPath, sequnitFileNamePrefix + ".read_basepos_qual_hist.html")

    ## Save D3 interactive plot in html format
    mpld3.save_html(fig, htmlFile)

    ## Save Matplotlib plot in png format
    plt.savefig(pngFile, dpi=fig.dpi)


    return pngFile, htmlFile



"""
Create average quality histogram plot

input: aqhistFile text data file generated by bbduk
output: png and d3 interactive plots

"""
def gen_fraction_reads_by_qual_plot(fastq, aqhistFile, bIsPaired, outPath, log):
    sequnitFileName, _ = safe_basename(fastq, log)
    sequnitFileNamePrefix = sequnitFileName.replace(".fastq", "").replace(".gz", "")

    qualDir = "qual"
    qualPath = os.path.join(outPath, qualDir)
    make_dir(qualPath)
    change_mod(qualPath, "0755")

    log.debug("aqhistFile file: %s", aqhistFile)

    rawDataMatrix = np.loadtxt(aqhistFile, delimiter='\t', comments='#')
    assert len(rawDataMatrix[0][:]) == 5

    # #Quality  count1  fraction1   count2  fraction2
    # 0 0   0.00000 0   0.00000
    # 1 20  0.00005 1925    0.00524
    # 2 44658   0.12149 14985   0.04077

    fig, ax = plt.subplots()

    markerSize = 3.5
    lineWidth = 1.5

    p1 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 2], 'r', marker='o', markersize=markerSize, linewidth=lineWidth, alpha=0.5, label='Read1')

    if bIsPaired:
        p2 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 4], 'g', marker='*', markersize=markerSize, linewidth=lineWidth, alpha=0.5, label='Read2')

    ax.set_xlabel("Average Read Quality", fontsize=12, alpha=0.5)
    ax.set_ylabel("Fraction of Reads", fontsize=12, alpha=0.5)
    fontProp = FontProperties()
    fontProp.set_size("small")
    fontProp.set_family("Bitstream Vera Sans")
    ax.legend(loc=1, prop=fontProp)
    ax.grid(color="gray", linestyle=':')

    ## Add tooltip
    mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p1[0], labels=list(rawDataMatrix[:, 2])))

    if bIsPaired:
        mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p2[0], labels=list(rawDataMatrix[:, 4])))

    pngFile = os.path.join(qualPath, sequnitFileNamePrefix + ".avg_read_qual_hist.png")
    htmlFile = os.path.join(qualPath, sequnitFileNamePrefix + ".avg_read_qual_hist.html")

    ## Save D3 interactive plot in html format
    mpld3.save_html(fig, htmlFile)

    ## Save Matplotlib plot in png format
    plt.savefig(pngFile, dpi=fig.dpi)


    return pngFile, htmlFile


"""
Create match rate histogram plot

input: mhistFile text data file generated by bbduk
output: png and d3 interactive plots

"""
def gen_match_rate_hist_plot(fastq, mhistFile, bIsPaired, outPath, log):
    sequnitFileName, _ = safe_basename(fastq, log)
    sequnitFileNamePrefix = sequnitFileName.replace(".fastq", "").replace(".gz", "")

    qualDir = "qual"
    qualPath = os.path.join(outPath, qualDir)
    make_dir(qualPath)
    change_mod(qualPath, "0755")

    log.debug("mhistFile file: %s", mhistFile)

    rawDataMatrix = np.loadtxt(mhistFile, delimiter='\t', comments='#')
    assert len(rawDataMatrix[0][:]) == 13

    # #BaseNum  Match1  Sub1    Del1    Ins1    N1  Other1  Match2  Sub2    Del2    Ins2    N2  Other2
    # 1 0.77714 0.20000 0.00000 0.00000 0.00190 0.02095 0.67654 0.24074 0.00000 0.00000 0.04198 0.04074
    # 2 0.77619 0.20381 0.00000 0.00000 0.00095 0.01905 0.71481 0.24815 0.00000 0.00000 0.00247 0.03457
    # 3 0.77714 0.20000 0.00000 0.00286 0.00190 0.01810 0.71852 0.23827 0.00000 0.00494 0.00247 0.03580
    # 4 0.79238 0.18190 0.00190 0.00571 0.00190 0.01810 0.69753 0.25802 0.00370 0.00741 0.00247 0.03457
    # 5 0.80571 0.16476 0.00286 0.00857 0.00190 0.01905 0.75185 0.20617 0.00370 0.01111 0.00000 0.03086
    # 6 0.83333 0.14286 0.00286 0.00857 0.00095 0.01429 0.77160 0.19012 0.00494 0.01111 0.00000 0.02716

    fig, ax = plt.subplots()

    markerSize = 3.5
    lineWidth = 1.0

    p1 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 1], 'r', marker='o', markersize=markerSize, linewidth=lineWidth, label='Match1', alpha=0.5)
    p2 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 2], 'g', marker='s', markersize=markerSize, linewidth=lineWidth, label='Sub1', alpha=0.5)
    p3 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 3], 'b', marker='*', markersize=markerSize, linewidth=lineWidth, label='Del1', alpha=0.5)
    p4 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 4], 'm', marker='d', markersize=markerSize, linewidth=lineWidth, label='Ins1', alpha=0.5)
    p5 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 5], 'c', marker='v', markersize=markerSize, linewidth=lineWidth, label='N1', alpha=0.5)
    p6 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 6], 'k', marker='x', markersize=markerSize, linewidth=lineWidth, label='Other1', alpha=0.5)

    if bIsPaired:
        p7 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 7], 'r', linestyle='--', marker='o', markersize=markerSize, linewidth=lineWidth, label='Match2', alpha=0.5)
        p8 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 8], 'g', linestyle='--', marker='s', markersize=markerSize, linewidth=lineWidth, label='Sub2', alpha=0.5)
        p9 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 9], 'b', linestyle='--', marker='*', markersize=markerSize, linewidth=lineWidth, label='Del2', alpha=0.5)
        p10 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 10], 'm', linestyle='--', marker='d', markersize=markerSize, linewidth=lineWidth, label='Ins2', alpha=0.5)
        p11 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 11], 'c', linestyle='--', marker='v', markersize=markerSize, linewidth=lineWidth, label='N2', alpha=0.5)
        p12 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 12], 'k', linestyle='--', marker='x', markersize=markerSize, linewidth=lineWidth, label='Other2', alpha=0.5)

    ax.set_xlabel("Base Number", fontsize=12, alpha=0.5)
    ax.set_ylabel("Fraction", fontsize=12, alpha=0.5)
    fontProp = FontProperties()
    fontProp.set_size("small")
    fontProp.set_family("Bitstream Vera Sans")
    ax.legend(loc=5, prop=fontProp)
    ax.grid(color="gray", linestyle=':')

    pngFile = os.path.join(qualPath, sequnitFileNamePrefix + ".match_rate_hist.png")
    htmlFile = os.path.join(qualPath, sequnitFileNamePrefix + ".match_rate_hist.html")

    ## Save D3 interactive plot in html format
    mpld3.save_html(fig, htmlFile)

    ## Save Matplotlib plot in png format
    plt.savefig(pngFile, dpi=fig.dpi)


    return pngFile, htmlFile



"""
Create quality accuracy plot

input: qahistTxtFile text data file generated by bbduk
output: png and d3 interactive plots

"""
def gen_qual_accuracy_plot(fastq, qahistTxtFile, outPath, log):
    sequnitFileName, _ = safe_basename(fastq, log)
    sequnitFileNamePrefix = sequnitFileName.replace(".fastq", "").replace(".gz", "")

    qualDir = "qual"
    qualPath = os.path.join(outPath, qualDir)
    make_dir(qualPath)
    change_mod(qualPath, "0755")

    log.debug("qahistTxtFile file: %s", qahistTxtFile)

    # rawDataMatrix = np.loadtxt(qahistTxtFile, delimiter='\t', comments='#')
    rawDataMatrix = np.genfromtxt(qahistTxtFile, delimiter='\t', comments='#')
    assert len(rawDataMatrix[0][:]) >= 5 and len(rawDataMatrix[0][:]) <= 7

    # #Deviation    21.902
    # #DeviationSub 21.579
    # #Quality  Match   Sub Ins Del TrueQuality TrueQualitySub
    # 0 0   0   0   0
    # 1 0   0   0   0
    # 2 4632    1006    56  28  7.20    7.55
    # 3 0   0   0   0
    # 4 0   0   0   0
    # 5 0   0   0   0
    # 6 0   0   0   0

    fig, ax = plt.subplots()

    markerSize = 3.5
    lineWidth = 1.5

    p1 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 5], 'r', marker='o', linestyle='None', alpha=0.5, label='TrueQuality')
    p2 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 6], 'g', marker='o', linestyle='None', alpha=0.5, label='TrueQualitySub')
    p3 = ax.plot(rawDataMatrix[:, 0], rawDataMatrix[:, 0], 'b', marker='d', markersize=markerSize, linewidth=lineWidth, alpha=0.5, label='Ideal')

    ax.set_xlabel("Claimed Quality", fontsize=12, alpha=0.5)
    ax.set_ylabel("Observed Quality", fontsize=12, alpha=0.5)
    fontProp = FontProperties()
    fontProp.set_size("small")
    fontProp.set_family("Bitstream Vera Sans")
    ax.legend(loc=1, prop=fontProp)
    ax.grid(color="gray", linestyle=':')

    ## Add tooltip
    mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p1[0], labels=list(rawDataMatrix[:, 5])))
    mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p2[0], labels=list(rawDataMatrix[:, 6])))

    pngFile = os.path.join(qualPath, sequnitFileNamePrefix + ".qual_accuracy.png")
    htmlFile = os.path.join(qualPath, sequnitFileNamePrefix + ".qual_accuracy.html")

    ## Save D3 interactive plot in html format
    mpld3.save_html(fig, htmlFile)

    ## Save Matplotlib plot in png format
    plt.savefig(pngFile, dpi=fig.dpi)


    return pngFile, htmlFile



## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Main Program
if __name__ == "__main__":
    desc = "RQC ReadQC Pipeline"

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-f", "--fastq", dest="fastq", help="Set input fastq file (full path to fastq)", required=True)
    parser.add_argument("-o", "--output-path", dest="outputPath", help="Set output path to write to")
    parser.add_argument("-r", "--ref", dest="refDb", help="Set reference db and enable reference analysis", required=False)
    parser.add_argument("-v", "--version", action="version", version=VERSION)

    ## For running on Crays
    # parser.add_argument("-l", "--lib-name", dest="libName", help="Library name", required=False)
    # parser.add_argument("-r", "--is-rna", dest="isRna", help="RNA", required=False)

    ## Toggle options
    parser.add_argument("-c", "--skip-cleanup", action="store_true", help="Skip file cleanup", dest="skipCleanup", default=False, required=False)
    # parser.add_argument("-z", "--skip-localization", action="store_true", help="Skip db localization", dest="skipDbLocalization", default=False, required=False)

    options = parser.parse_args()

    outputPath = None # output path, defaults to current working directory
    fastq = None # full path to input fastq.gz
    refDb = None

    status = ""
    nextStepToDo = 0

    if options.outputPath:
        outputPath = options.outputPath
    else:
        outputPath = os.getcwd()

    if options.fastq:
        fastq = options.fastq

    if options.refDb:
        refDb = options.refDb

    ## create output_directory if it doesn't exist
    if not os.path.isdir(outputPath):
        os.makedirs(outputPath)


    ## Set pooled config
    RQCPooledConfig.CFG["status_file"] = os.path.join(outputPath, "pooled_status.log")
    RQCPooledConfig.CFG["files_file"] = os.path.join(outputPath, "pooled_files.tmp")
    RQCPooledConfig.CFG["stats_file"] = os.path.join(outputPath, "pooled_stats.tmp")
    RQCPooledConfig.CFG["output_path"] = outputPath
    RQCPooledConfig.CFG["skip_cleanup"] = options.skipCleanup
    RQCPooledConfig.CFG["run_path"] = get_run_path()
    # RQCPooledConfig.CFG["skip_localization"] = options.skipDbLocalization
    RQCPooledConfig.CFG["log_file"] = os.path.join(outputPath, "pooled.log")


    print "Started Pooled pipeline, writing log to: %s" % (RQCPooledConfig.CFG["log_file"])

    log = get_logger("pooled", RQCPooledConfig.CFG["log_file"], LOG_LEVEL, False, True)
    log.info("=================================================================")
    log.info("   Pooled Analysis (version %s)", VERSION)
    log.info("=================================================================")

    log.info("Starting %s with %s", SCRIPT_NAME, fastq)

    if options.refDb:
        log.info("Reference: %s", refDb)

    ## check for fastq file
    if os.path.isfile(fastq):
        log.info("Found %s, starting processing.", fastq)
        if os.path.isfile(RQCPooledConfig.CFG["status_file"]):
            status = get_status(RQCPooledConfig.CFG["status_file"], log)
            log.info("Latest status = %s", status)

            if status == "start":
                pass

            elif status != "complete":
                nextStepToDo = int(status.split("_")[0])
                if status.find("complete") != -1:
                    nextStepToDo += 1
                log.info("Next step to do = %s", nextStepToDo)

            else:
                ## already bDone. just exit
                log.info("Completed %s: %s", SCRIPT_NAME, fastq)
                exit(0)
        else:
            checkpoint_step_wrapper("start")
            status = get_status(RQCPooledConfig.CFG["status_file"], log)
            log.info("Latest status = %s", status)

    else:
        log.error("%s not found, aborting!", fastq)
        exit(2)


    bDone = False
    cycle = 0
    totalReadNum = 0
    firstSubsampledFastqFileName = ""
    secondSubsampledFastqFile = ""
    totalReadCount = 0
    subsampledReadNum = 0
    bIsPaired = False
    readLength = 0

    if status == "complete":
        log.info("Status is complete, not processing.")
        bDone = True


    while not bDone:

        cycle += 1

        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 1. multiplex_statistics
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 1 or status == "start":
            status = do_multiplex_statistics(fastq, log)

        if status == "1_multiplex_statistics failed":
            bDone = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 2. Reference analysis
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 2 or status == "1_multiplex_statistics complete":
            ## 10122017 skip this step!
            refDb = None
            status = do_pooled_reference_mapping_analysis(fastq, refDb, outputPath, log)

            if status == "2_pooled_reference_mapping_analysis failed":
                bDone = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 3. Fragment analysis (RQC-925)
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 3 or status == "2_pooled_reference_mapping_analysis complete":
            status = do_fragment_analysis(fastq, outputPath, log)

            if status == "3_fragment_analysis failed":
                bDone = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 4. kapa spike-in analysis
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 4 or status == "3_fragment_analysis complete":
            status = do_kapa_spikein_analysis(fastq, outputPath, log)

            if status == "4_kapa_analysis failed":
                bDone = True


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 5. post-processing & reporting
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 5 or status == "4_kapa_analysis complete":
            log.info("\n\n%sSTEP4 - pooled_report_postprocess: mv rqc-*.tmp to rqc-*.txt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", color['pink'], color[''])
            status = "5_pooled_report_postprocess in progress"
            checkpoint_step_wrapper(status)

            ## move rqc-files.tmp to rqc-files.txt
            rqcNewFilesLog = "%s/%s" % (outputPath, "pooled_files.txt")
            rqcNewStatsLog = "%s/%s" % (outputPath, "pooled_stats.txt")

            cmd = " mv %s %s " % (RQCPooledConfig.CFG["files_file"], rqcNewFilesLog)
            log.info("mv cmd: %s", cmd)
            run_sh_command(cmd, True, log)

            cmd = " mv %s %s " % (RQCPooledConfig.CFG["stats_file"], rqcNewStatsLog)
            log.info("mv cmd: %s", cmd)
            run_sh_command(cmd, True, log)

            status = "5_pooled_report_postprocess complete"
            log.info(status)
            checkpoint_step_wrapper(status)


        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ## 6. Cleanup
        ##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if nextStepToDo == 6 or status == "5_pooled_report_postprocess complete":
            status = do_cleanup_pooled(log)

        if status == "6_cleanup_pooled failed":
            bDone = True

        if status == "6_cleanup_pooled complete":
            status = "complete" ## FINAL COMPLETE!
            bDone = True

        ## don't cycle more than 10 times ...
        if cycle > 10:
            bDone = True


    if status != "complete":
        log.info("Status %s", status)

    else:
        log.info("\n\nCompleted %s: %s", SCRIPT_NAME, fastq)
        checkpoint_step_wrapper("complete")
        log.info("Done.")
        print "Done."


    exit(0)


## EOF

