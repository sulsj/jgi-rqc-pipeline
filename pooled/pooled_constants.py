#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Constants for RQC Pooled analysis pipeline
Global constants are defined in a file under the directory lib/rqc_constants.py .

Created: 10172016

sulsj (ssul@lbl.gov)

"""

import os
# from common import get_run_path
# from os_utility import get_tool_path


# global configuration variables
class RQCPooledConfig:
    CFG = {}
    CFG["output_path"] = ""
    CFG["status_file"] = ""
    CFG["files_file"] = ""
    CFG["stats_file"] = ""
    CFG["log_file"] = ""
    CFG["no_cleanup"] = ""
    CFG["run_path"] = ""


# class RQCPooledCommands:
#     ## STEP1
#     ## Needs readqc/tools/readSeq.py
#     KMERCOUNT_POS_CMD = get_tool_path("kmercount_pos.py", "kmercount_pos.py")


## Constants for reporting
# class PooledStats:
#     ## step 1
#     POOLED_DEMULTIPLEX_STATS = "demultiplex stats"
#     POOLED_DEMULTIPLEX_STATS_PLOT = "demultiplex stats plot"
#     POOLED_DEMULTIPLEX_STATS_D3_HTML_PLOT = "POOLED_DEMULTIPLEX_STATS_D3_HTML_PLOT"


## EOF