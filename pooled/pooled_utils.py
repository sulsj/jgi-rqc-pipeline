#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
 Pooled analysis utilities

 Created: 10172016
 sulsj (ssul@lbl.gov)

"""

import os
import subprocess
import matplotlib

from common import checkpoint_step
from os_utility import make_dir, change_mod, run_sh_command
from pooled_constants import RQCPooledConfig
from rqc_constants import RQCExitCodes
from rqc_utility import safe_basename, get_cat_cmd

matplotlib.use("Agg")  ## This needs to skip the DISPLAY env var checking
import matplotlib.pyplot as plt
# from matplotlib.font_manager import FontProperties
import mpld3
# from matplotlib.ticker import MultipleLocator, FormatStrFormatter


""" STEP1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Generate_index_sequence_detection_plot

"""
# def generate_index_sequence_detection_plot(fastq, log, isMultiplexed=True):
def generate_index_sequence_detection_plot(fastq, log):
    if not os.path.isfile(fastq):
        log.error("Failed to find the input fastq file, %s" % (fastq))
        return RQCExitCodes.JGI_FAILURE, None, None, None
    else:
        log.info("fastq file for index sequence analysis: %s" % (fastq))

    sequnitFileName, exitCode = safe_basename(fastq, log)
    sequnitFileNamePrefix = sequnitFileName.replace(".fastq", "").replace(".gz", "")

    demultiplexStatsFile = None
    demultiplexPlotDataFile = None
    detectionPlotPngFile = None
    storedDemultiplexStatsFile = None

    log.info("Multiplexed - start analyzing...")

    outputPath = RQCPooledConfig.CFG["output_path"]
    demul_dir = "demul"
    demulPath = os.path.join(outputPath, demul_dir)
    make_dir(demulPath)
    change_mod(demulPath, "0755")

    ## This version is sorted by percent for readability of stats
    demultiplexStatsFile = os.path.join(demulPath, sequnitFileNamePrefix + ".demultiplex_stats")

    ## This version has index column and sort by index for plot
    demultiplexPlotDataFile = os.path.join(demulPath, sequnitFileNamePrefix + ".demultiplex_stats.tmp")

    ## This path is relative to final qual location to be stored in analysis obj.
    detectionPlotPngFile = os.path.join(demulPath, sequnitFileNamePrefix + ".index_sequence_detection.png")

    storedDemultiplexStatsFile = os.path.join(demulPath, sequnitFileNamePrefix + ".demultiplex_stats")

    if not os.path.isfile(demultiplexStatsFile):
        indexSeq = None
        line = None
        header = None
        indexSeqCounter = {}

        catCmd, exitCode = get_cat_cmd(fastq, log)

        if fastq.endswith(".gz"):
            catCmd = "zcat"  ## pigz does not work with subprocess

        seqCount = 0

        try:
            proc = subprocess.Popen([catCmd, fastq], bufsize=2 ** 16, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

            while 1:
                line = proc.stdout.readline()
                if not line:
                    break

                ## First line is header
                line.strip()
                header = line

                ## Get second line of record - sequence
                line = proc.stdout.readline()

                ## Get third line - junk
                line = proc.stdout.readline()

                ## Get the final line (4th line) - quality
                line = proc.stdout.readline()

                ## Parse the header
                headerFields = header.split(":")

                ## The last index is the index
                indexSeq = headerFields[-1].strip()
                assert indexSeq

                ## Increment the counts and store the index
                if indexSeq in indexSeqCounter:
                    indexSeqCounter[indexSeq] += 1
                else:
                    indexSeqCounter[indexSeq] = 1

                seqCount += 1

        except Exception as e:
            if log:
                log.error("Exception in file reading: %s" % e)
                log.error("Failed to read the given fastq file [%s]" % fastq)
                log.error("Fastq header doesn't have the index sequence: %s" % header)
                log.error("Index sequence analysis is skipped!")

            return RQCExitCodes.JGI_SUCCESS, None, None, None

        ## Open the output file handles for writing
        log.info("demultiplexPlotDataFile = %s" % (demultiplexPlotDataFile))
        log.info("detectionPlotPngFile = %s" % (detectionPlotPngFile))
        log.info("demultiplexStatsFile = %s" % (demultiplexStatsFile))
        log.info("storedDemultiplexStatsFile = %s" % (storedDemultiplexStatsFile))

        plotDataFH = open(demultiplexPlotDataFile, "w")
        statsFH = open(demultiplexStatsFile, "w")

        ## Count the total number of indexes found
        numIndexesFound = len(indexSeqCounter)

        ## Store the data header information for printing
        reportHeader = """# Demultiplexing Summary
#
# Seq unit name: %s
# Total sequences: %s
# Total indexes found: %s
# 1=indexSeq 2=index_sequence_count 3=percent_of_total
#
""" % (sequnitFileName, seqCount, numIndexesFound)

        statsFH.write(reportHeader)

        ## Sort by value, descending
        for indexSeq in sorted(indexSeqCounter, key=indexSeqCounter.get, reverse=True):
            perc = float(indexSeqCounter[indexSeq]) / float(seqCount) * 100
            l = "%s\t%s\t%.6f\n" % (indexSeq, indexSeqCounter[indexSeq], perc)
            statsFH.write(l)

        ## Sort by index and add id column for plotting
        i = 1
        for indexSeq in sorted(indexSeqCounter.iterkeys()):
            perc = float(indexSeqCounter[indexSeq]) / float(seqCount) * 100
            ## to decrease the number of data to plot (mem overflow)
            ## ex) 11921.4.227916.fastq.gz ==> 11921.4.227916.demultiplex_stats.tmp: file size 22MB --> 4.8MB
            if perc > 0.000002:
                l = "%s\t%s\t%s\t%.6f\n" % (i, indexSeq, indexSeqCounter[indexSeq], perc)
                plotDataFH.write(l)
                i += 1

        plotDataFH.close()
        statsFH.close()

    ## matplotlib plotting
    # data
    # Index_seq_id Index_seq indexSeqCounter percent
    # 1 AAAAAAAAAAAA    320 0.000549
    # 2 AAAAAAAAAAAC    16  0.000027
    # 3 AAAAAAAAAAAG    8   0.000014
    # 4 AAAAAAAAAACA    4   0.000007
    # 5 AAAAAAAAAACG    2   0.000003
    # 6 AAAAAAAAAAGA    6   0.000010
    # 7 AAAAAAAAAATA    6   0.000010

    # rawDataMatrix = np.loadtxt(demultiplexPlotDataFile, delimiter='\t', comments='#')
    # assert len(rawDataMatrix[1][:]) == 4

    ## For a textfile with 4000x4000 words this is about 10 times faster than loadtxt.
    ## http://stackoverflow.com/questions/14985233/load-text-file-as-strings-using-numpy-loadtxt
    def load_data_file(fname):
        data = []
        with open(fname, 'r') as FH:
            for line in FH:
                data.append(line.replace('\n', '').split('\t'))
        return data

    def column(matrix, i, opt):
        if opt == "int":
            return [int(row[i]) for row in matrix]
        elif opt == "float":
            return [float(row[i]) for row in matrix]
        else:
            return [row[i] for row in matrix]

    rawDataMatrix = load_data_file(demultiplexPlotDataFile)
    fig, ax = plt.subplots()

    markerSize = 6.0
    lineWidth = 1.5

    ## This becomes a bottleneck if rawDataMatrix is getting bigger
    p1 = ax.plot(column(rawDataMatrix, 0, "int"), column(rawDataMatrix, 3, "float"), 'r', marker='o', markersize=markerSize, linewidth=lineWidth, label='N', alpha=0.5)

    ax.set_xlabel("Index Sequence ID", fontsize=12, alpha=0.5)
    ax.set_ylabel("Fraction", fontsize=12, alpha=0.5)
    ax.grid(color="gray", linestyle=':')

    ## Add tooltip
    labels = ["%s" % i for i in column(rawDataMatrix, 1, "str")]

    ## Show index_seq in the plot
    for i in rawDataMatrix:
        ax.text(i[0], float(i[3]) + .2, "%s" % i[1])

    mpld3.plugins.connect(fig, mpld3.plugins.PointLabelTooltip(p1[0], labels=labels))

    detectionPlotHtml = os.path.join(demulPath, sequnitFileNamePrefix + ".index_sequence_detection.html")

    ## Save D3 interactive plot in html format
    mpld3.save_html(fig, detectionPlotHtml)

    ## Save Matplotlib plot in png format
    plt.savefig(detectionPlotPngFile, dpi=fig.dpi)

    if exitCode != 0:
        log.error("Failed to create demulplex plot")
        return RQCExitCodes.JGI_FAILURE, None, None, None

    log.info("demulplex stats and plot generation completed!")


    if detectionPlotPngFile is not None and storedDemultiplexStatsFile is not None and os.path.isfile(detectionPlotPngFile) and os.path.isfile(storedDemultiplexStatsFile):
        return RQCExitCodes.JGI_SUCCESS, demultiplexStatsFile, detectionPlotPngFile, detectionPlotHtml

    else:
        return RQCExitCodes.JGI_FAILURE, None, None, None



""" STEP3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

cleanup_pooled

Cleaning up the Pooled analysis directory with unwanted files.

@param log
@return retCode: always return success

"""
def cleanup_pooled(log):
    log.info("Cleaning up temp files...")
    # outputPath = RQCPooledConfig.CFG["output_path"]
    # cmd = " %s %s/%s/megablast*v*FfTITW45 " % (RQCCommands.RM_CMD, outputPath, "megablast")
    # stdOut, stdErr, exitCode = run_sh_command(cmd, True, log)
    # if exitCode != 0:
    #     log.error("Failed to execute %s; may be already purged." % (cmd))

    return RQCExitCodes.JGI_SUCCESS


## ===========================================================================================================================



""" For STEP1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

multiplex stats analysis

get_multiplex_info

"""
# def get_multiplex_info(sequnitFileName, log):
#     log.debug("get_multiplex_info: %s", sequnitFileName)
#     isMultiplexed = -1
#
#     if sequnitFileName:
#         srfSequnitFileName = sequnitFileName + ".srf"
#         gzSequnitFileName = sequnitFileName + ".fastq.gz"
#
#         ## read only connection
#         db = jgi_connect_db("rqc")
#         sth = db.cursor(MySQLdb.cursors.DictCursor)
#
#         sql = """
# select is_multiplex
# from seq_units
# where seq_unit_name = %s;"""
#
#         log.debug("sql= %s. sequnitFileName = %s" % (sql, gzSequnitFileName))
#         sth.execute(sql, gzSequnitFileName)
#         rs = sth.fetchone()
#
#         ## Try to get lib info again using *.fastq.gz
#         if not rs:
#             log.debug("sql = %s. sequnitFileName = %s" % (sql, srfSequnitFileName))
#             sth.execute(sql % (srfSequnitFileName))
#             rs = sth.fetchone()
#
#         if rs:
#             if rs["is_multiplex"]:
#                 isMultiplexed = 1 if rs["is_multiplex"] == 1 else 0
#             else:
#                 isMultiplexed = -1
#         else:
#             log.error("cannot find is_multiplex information from the seq_units table for %s", sequnitFileName)
#             isMultiplexed = -1
#
#     db.close()
#
#     return isMultiplexed



"""===========================================================================
    checkpoint_step_wrapper

"""
def checkpoint_step_wrapper(status):
    assert RQCPooledConfig.CFG["status_file"]
    checkpoint_step(RQCPooledConfig.CFG["status_file"], status)


"""===========================================================================
    get the file content for post-processing
"""
# def get_analysis_file_contents(full_path):
#     retCode = ""
#
#     if os.path.isfile(full_path):
#         with open(full_path, "r") as FH:
#             retCode = FH.readlines()
#         return retCode
#     else:
#         return "file not found"


## EOF
